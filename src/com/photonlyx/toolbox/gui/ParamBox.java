package com.photonlyx.toolbox.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.photonlyx.toolbox.param.ParBool;
import com.photonlyx.toolbox.param.ParDouble;
import com.photonlyx.toolbox.param.ParInt;
import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;

public abstract class ParamBox 
{
	public static boolean autoUpdate=true;

	private int w=180,h=30;	
	//private boolean enable;

	public abstract Component getComponent();
	public abstract void update();
	public abstract String getParamName();
	public abstract Param getParam();
	protected boolean triggerEnabled=true;
	private Font font=Global.font;
	public abstract void setEditable(boolean b);
	public abstract void setEnabled(boolean b);
	public abstract void setFieldLength(int l);

	
	public static ParamBox getParamBoxInstance(Object paramOwner,String paramName,String paramLabel,
			String paramUnit,TriggerList tl,int w,int h)
	{
		return ParamBox.getParamBoxInstance(paramOwner, paramName, paramLabel, paramUnit, tl, w, h, true);	
	}

	public static ParamBox getParamBoxInstance(Object paramOwner,String paramName,String paramLabel,
			String paramUnit,TriggerList tl,int w,int h,boolean useCombos)

	{
		Class typec=Util.getType( paramOwner, paramName);
		if (typec==null) return null;
		String type=typec.getSimpleName();
		//System.out.println("--------------------------paramName:  "+paramName+ " owner class: "+paramOwner.getClass().getSimpleName()+" type: "+type);
		if ((type.compareTo("boolean")==0 )  ||(type.compareTo("Boolean")==0 ))
		{
			ParBoolBox b=new ParBoolBox(tl,new ParBool(paramOwner,paramName),w,h,paramLabel,paramUnit);
			return b;
		}
		if ((type.compareTo("double")==0 ) ||(type.compareTo("Double")==0 ))
		{
			double[] options=null;
			Field field=null;
			try
			{
				field=paramOwner.getClass().getField(paramName+"Options");
				if (field!=null) options= (double[])field.get(paramOwner);
			}
			catch (Exception e)
			{
				//System.out.println(paramOwner.getClass()+ "  no field of name :   "+paramName+"Options");
			};


			if ((options==null)||(!useCombos))//put a text box
			{
				ParDouble par=new ParDouble(paramOwner,paramName);
				par.setFormat("0.000");
				ParDoubleBox b=new ParDoubleBox(tl,par,w,h,paramLabel,paramUnit);
				return b;
			}
			else// put a combo with the options put as String[] %name%Options=new String{"options2","option2"}
			{
				ParDouble par=new ParDouble(paramOwner,paramName);
				ParDoubleOptionsBox b=new ParDoubleOptionsBox(tl,par,options,w,h,paramLabel,paramUnit);
				return b;
			}

		}
		if ((type.compareTo("int")==0 )  ||(type.compareTo("Integer")==0 ))
		{
			int[] options=null;
			Field field=null;

			//try to find a field of type String[] with name paramName+"Options"
			try
			{
				field=paramOwner.getClass().getField(paramName+"Options");
				if (field!=null) options= (int[])field.get(paramOwner);
			}
			catch (Exception e){};

			if (field==null)
			{
				//try to find a method that return	 String[] with name paramName+"Options"
				Method method=null;
				try
				{
					method=paramOwner.getClass().getMethod(paramName+"Options", (Class[])null);
					if (method!=null) 
					{
						options=(int[])method.invoke(paramOwner, (Object[])null);
					}
				}
				catch (Exception e){};
			}


			if ((options==null)||(!useCombos))//put a int box
			{
				ParInt par=new ParInt(paramOwner,paramName);
				ParIntBox b=new ParIntBox(tl,par,w,h,paramLabel,paramUnit);
				//b.setSize(180,30);
				b.setSize(w,h);
				return b;
			}
			else // put a combo with the options put as int[] %name%Options=new int{"options2","option2"}
			{
				ParInt par=new ParInt(paramOwner,paramName);
				ParIntOptionsBox b=new ParIntOptionsBox(tl,par,options);
				if (paramLabel==null) b.setLabel(paramName);else b.setLabel(paramLabel);
				if (paramUnit!=null) b.setUnit(paramUnit);
				//b.setSize(180,30);
				b.setSize(w,h);
				return b;
			}



		}

		if (type.compareTo("String")==0 )  
		{
			String[] options=null;
			Field field=null;

			//try to find a field of type String[] with name paramName+"Options"
			try
			{
				field=paramOwner.getClass().getField(paramName+"Options");
				if (field!=null) options= (String[])field.get(paramOwner);
			}
			catch (Exception e){};

			if (field==null)
			{
				//try to find a method that return	 String[] with name paramName+"Options"
				Method method=null;
				try
				{
					method=paramOwner.getClass().getMethod(paramName+"Options", (Class[])null);
					if (method!=null) 
					{
						options=(String[])method.invoke(paramOwner, (Object[])null);
					}
				}
				catch (Exception e){};
			}

			if ((options==null)||(!useCombos))//put a text box
			{
				ParString par=new ParString(paramOwner,paramName);
				ParStringBox b=new ParStringBox(tl,par,w,h,paramLabel,paramUnit);
				//b.setSize(180,30);
				b.setSize(w,h);
				return b;
			}
			else  // put a combo with the options put as String[] %name%Options=new String{"options2","option2"}
			{
				ParString par=new ParString(paramOwner,paramName);
				ParStringOptionsBox b=new ParStringOptionsBox(tl,par,options,w,h,paramLabel,paramUnit);
				return b;
			}
		}
		Messager.messErr(paramName+" of "+paramOwner.getClass().getSimpleName()+
				" (type "+Util.getType( paramOwner, paramName).getSimpleName()+
				") is not boolean nor double nor int nor String");

		return null;

	}



	public int getW()
	{
		return w;
	}
	public void setW(int w)
	{
		this.w = w;
		//init();
	}
	public int getH()
	{
		return h;
	}
	public void setH(int h)
	{
		this.h = h;
		//init();
	}

	public Font getFont()
	{return font;}

	public void setFont(Font f)
	{
		this.font=f;
		//init();
	}

	public Color getBackground(){return Global.background;}


	public void setTriggerEnabled(boolean b)
	{
		triggerEnabled=b;

	}
	public boolean isTriggerEnabled()
	{
		return triggerEnabled;
	}




}
