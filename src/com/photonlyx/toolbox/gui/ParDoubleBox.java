package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.*;
import javax.swing.event.*;

import com.photonlyx.toolbox.mysql.MysqlRow;
import com.photonlyx.toolbox.param.ParDouble;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;


/**
permits to show and edit a param (name,value,unit) and trig objects when value changed

 */
public class ParDoubleBox  extends ParamBox implements DocumentListener,KeyListener,MouseWheelListener
{
	private Color valbackground=Color.lightGray;
	private boolean editable=true;
	private int fieldLength=5;

	private TriggerList triggerList;//list of object to update when this parameter is changed
	private ParDouble p;
	private JTextField val;
	private JLabel name,labelUnit;
	private boolean actualiseOnMouseWheel=true;
	//private boolean wheelMultiply=true;
	private boolean wheelAdd=false;
	private double wheelCoef=0.01; //one mouse wheel step multiply by this if wheelMultiply==true or add by this if  wheelAdd==true
	private double maxLimit=Double.MAX_VALUE,minLimit=-Double.MAX_VALUE;


	private JPanel jp=new JPanel();

	/**
	 * 
	 * @param tl can be null
	 * @param p
	 */
	public ParDoubleBox(TriggerList tl,ParDouble p,int w,int h,String label,String unit)
	{
		triggerList=tl;	
		this.p=p;
		this.setW(w);
		this.setH(h);

		jp.removeAll();
		jp.setSize(new Dimension(getW(),getH()));
		jp.setPreferredSize(new Dimension(getW(),getH()));
		jp.setBackground(Global.background);

		name=new JLabel();
		name.setFont(this.getFont());
		name.setBackground(this.getBackground());
		if (label!=null) name.setText(label);else name.setText(p.getParamName());
		jp.add(name);

		val=new JTextField(fieldLength);

		jp.add(val);
		val.getDocument().addDocumentListener(this);
		val.addKeyListener(this);
		val.addMouseWheelListener(this);
		val.setOpaque(true);
		val.setEditable(editable);
		val.setFont(this.getFont());
		if (editable) val.setBackground(this.getBackground());
		else val.setBackground(new Color(200,200,200));

		if (unit!=null) 
		{
			labelUnit=new JLabel("");
			labelUnit.setFont(this.getFont());
			labelUnit.setBackground(this.getBackground());
			jp.add(labelUnit);
			labelUnit.setText(unit);
		}

		val.doLayout();

		//		JPanel jpminibuttons=new JPanel();
		//		jpminibuttons.setBackground(Global.background);
		//		int dx=6;int dy=10;int col=15;
		//		jpminibuttons.setPreferredSize(new Dimension(dx,this.getH()));
		//		FlowLayout fl=new FlowLayout();
		//		fl.setVgap(1);
		//		jpminibuttons.setLayout(fl);

		//		{
		//			//button to up value :
		//			TriggerList tl_button=new TriggerList();
		//			tl_button.add(new Trigger(this,"upValue"));
		//			CButton cButton=new CButton(tl_button);
		//			cButton.setText("");
		//			cButton.setPreferredSize(new Dimension(dx,dy));
		//			cButton.setBorderPainted(false);
		//			Color c=Global.background;
		//			cButton.setBackground(c);
		//			jpminibuttons.add(cButton);
		//		}
		//		{
		//			//button to down value :
		//			TriggerList tl_button=new TriggerList();
		//			tl_button.add(new Trigger(this,"downValue"));
		//			CButton cButton=new CButton(tl_button);
		//			cButton.setText("");
		//			cButton.setPreferredSize(new Dimension(dx,dy));
		//			cButton.setBorderPainted(false);
		//			Color c=Global.background;
		//			cButton.setBackground(c);
		//			jpminibuttons.add(cButton);
		//		}
		//jp.add(jpminibuttons);



		update();
	}




	public void setForeground(String color) 
	{
		Color foreground=ColorUtil.getColor(color);
		jp.setForeground(foreground);
		name.setForeground(foreground);
		val.setForeground(foreground);
		labelUnit.setForeground(foreground);
	}

	public void setBackground(String color) 
	{
		Color background=ColorUtil.getColor(color);
		jp.setBackground(background);
		name.setBackground(background);
		val.setBackground(background);
		labelUnit.setBackground(background);
	}

	public void setValBackground(String color) 
	{
		Color background=ColorUtil.getColor(color);
		val.setBackground(background);
	}

	public TriggerList getTriggerList() {
		return triggerList;
	}


	public void setTriggerList(TriggerList triggerList) {
		this.triggerList = triggerList;
	}


	public JTextField getVal() {
		return val;
	}


	public void setVal(JTextField val) {
		this.val = val;
	}


	public boolean isEditable() {
		return editable;
	}


	public void setEditable(boolean editable) {
		this.editable = editable;
		if (editable) val.setBackground(this.getBackground());
		else val.setBackground(new Color(200,200,200));
		val.setEditable(editable);
	}


	public void setFieldLength(int fieldLength) {
		this.fieldLength = fieldLength;
		val.setColumns(fieldLength);
	}


	public void setActualiseOnMouseWheel(boolean actualiseOnMouseWheel) {
		this.actualiseOnMouseWheel = actualiseOnMouseWheel;
	}


	public void setWheelCoef(double wheelCoef) {
		this.wheelCoef = wheelCoef;
	}



	public void setWheelAdd(boolean wheelAdd) {
		this.wheelAdd = wheelAdd;
	}


	public void update()
	{
		//System.out.println(getClass()+" update "+p.getParamName()+"to: "+p.stringVal());
		val.getDocument().removeDocumentListener(this);
		val.setText(p.stringVal());
		val.getDocument().addDocumentListener(this);
	}




	//***********interface DocumentListener ***************

	//Gives notification that an attribute or set of attributes changed.
	public  void changedUpdate(DocumentEvent e)
	{
		//System.out.println(getClass()+" val box changed ");
		actualiseParamFromBox();
		//System.out.println(getClass()+" has changed:"+p.hasChanged());
	}

	//Gives notification that there was an insert into the document.
	public   void insertUpdate(DocumentEvent e)
	{
		//System.out.println(getClass()+" inserted ");
		actualiseParamFromBox();
	}
	public   void removeUpdate(DocumentEvent e)
	{
		//System.out.println(getClass()+" remove ");
		actualiseParamFromBox();
	}
	//***********end interface DocumentListener ***************


	//**********interface keyListener**********************
	public void keyPressed(KeyEvent e)
	{
		switch (e.getKeyCode())
		{
		case KeyEvent.VK_ENTER :
			jp.repaint();
			break;
		}


	}

	public void keyTyped(KeyEvent e){}
	public void keyReleased(KeyEvent e){}

	//**********end interface keyListener**********************



	public void mouseWheelMoved(MouseWheelEvent e)
	{
		if (!editable) return;
		int nbClicks=-e.getWheelRotation();
		val.getDocument().removeDocumentListener(this);

		ParDouble pd=(ParDouble)p;
		double v;
		v=pd.val();
		double newv;
		if (wheelAdd) newv=v+wheelCoef*nbClicks; 
		else if (v==0.0) newv=v+nbClicks;else newv=v*(1+wheelCoef*nbClicks);
		//System.out.println(v+" "+newv);
		//pd.setVal(newv);
		NumberFormat nf=new DecimalFormat(p.getFormat(),new DecimalFormatSymbols(Locale.ENGLISH));
		val.setText(nf.format(newv));

		if (actualiseOnMouseWheel) actualiseParamFromBox();
		val.getDocument().addDocumentListener(this);
	}


	public void downValue()
	{
		if (this.editable) this.changeValue(-1);
	}

	public void upValue()
	{
		if (this.editable) this.changeValue(+1);	
	}

	private void changeValue(int nb)
	{
		ParDouble pd=(ParDouble)p;
		double v;
		v=pd.val();
		double newv;
		if (wheelAdd) newv=v+wheelCoef*nb; 
		else if (v==0.0) newv=v+nb;else newv=v*(1+wheelCoef*nb);
		//System.out.println(v+" "+newv);
		pd.setVal(newv);
		val.setText(pd.stringVal());	
	}



	private void actualiseParamFromBox()
	{
		if (p==null) return;
		//System.out.println(getClass()+" actualiseParamFromBox : "+val.getText());
		{
			boolean ok;
			double value=0;
			String s=val.getText();
			try
			{
				value=new Double(s.replace(',','.')).doubleValue();
				ok=true;
			}
			catch (NumberFormatException e)
			{
				ok=false;
			}	
			if (ok) 
			{
				if ((value>maxLimit)||(value<minLimit)) ok=false;
			}
			if (ok)
			{
				((ParDouble)p).setVal(value);
				val.setBackground(valbackground);

			}
			else
			{
				val.setBackground(Color.red);
				//System.out.println("red: "+value);
			}

		}
		if (triggerList!=null) if (triggerEnabled) triggerList.trig();
	}


	public Component getComponent(){return jp;}

	public String getParamName()
	{
		return p.getParamName();
	}


	public void setEnabled(boolean b)
	{
		this.setEditable(b);
		val.setEnabled(b);
		name.setEnabled(b);
		labelUnit.setEnabled(b);
	}

	public Param getParam(){return p;}


	public double getMaxLimit()
	{
		return maxLimit;
	}


	public void setMaxLimit(double maxLimit)
	{
		this.maxLimit = maxLimit;
	}


	public double getMinLimit()
	{
		return minLimit;
	}


	public void setMinLimit(double minLimit)
	{
		this.minLimit = minLimit;
	}

}