package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JPanel;

import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;


public class PalettePanel    implements PaletteInterface, KeyListener,MouseListener
{

/**
	 * 
	 */
private static final long serialVersionUID = 1L;
 
private double vmin=0,vmax=1;//min and max values 
private int offsetHue=0;//hue value corresponding to the 0 value (0 to 100%)
private int ampHue=100;//hue amplitude used by the palette (0 to 100%)
private String format;//format of numbers written in the palette
private float hue;//for COLOR_N_WHITE
private int type=0;
public final static int GRAY_SCALE=0;
public final static int COLOR_N_WHITE=1;
public final static int COLOR=2;
private Vector<PaletteChangedListener> listeners=new Vector(); 


private JPanel jp=new JPanel()//panel to visualize the palette
{
	public void paintComponent(Graphics g)
	{
	for (int i=0;i<getSize().height;i++)
		{
		double val=(1-(float)i/(float)getSize().height)*(vmax-vmin)+vmin;
		g.setColor(new Color(getRGB(val)));
		g.drawLine(0,i,getSize().width,i);
		}
	//incrust the min and max values
	g.setColor(Color.black);
	g.setFont(Global.font);
	NumberFormat nf;
	if (format==null) nf=new DecimalFormat("0.00E0");
	else nf=new DecimalFormat(format);
	g.drawString(""+nf.format(vmax),1,10);
	g.drawString(""+nf.format(vmin),1,getSize().height-10);
	}
};


//test app
public static void main(String[] args) 
{
//create frame 
CJFrame cjf=new CJFrame();
cjf.getContentPane().setLayout(new BorderLayout());
cjf.setSize(200, 400);
cjf.setVisible(true);	
cjf.setTitle("Palette");
cjf.setFont(Global.font);
cjf.add(new PalettePanel().getVisualisation());
cjf.setVisible(true);
}


/**
 * construct a  palette
 */
public PalettePanel()
{
jp.setPreferredSize(new Dimension(50,200));
jp.addKeyListener(this);
jp.addMouseListener(this);
}

/**
 * construct a  palette
 */
public PalettePanel(int type)
{
jp.setPreferredSize(new Dimension(50,200));
jp.addKeyListener(this);
jp.addMouseListener(this);
this.type=type;
}



public void setVmin(double d){vmin=(float)d;jp.repaint();}
public void setVmax(double d){vmax=(float)d;jp.repaint();}


/**return the RGB with colours  for a  value*/
private  int getRGBcoul(double val)
{
int coul;
float r,g,b;
if (val<vmin) val=vmin;
if (val>vmax) val=vmax;
// float hue=(float)( 0.12 +val*(0.8)/(vmax.val-vmin.val));
double offset,amp;
offset=offsetHue/100.;
amp=ampHue/100.;
float hue=(float)( offset +(val-vmin)/(vmax-vmin)*amp);
if (hue>1) hue-=1;
coul=Color.HSBtoRGB(hue,1,1);
if (val==0) coul=Color.black.getRGB();
return coul;
}

/**return the RGB code grey for a value*/
private int getRGBnb(double val)
{
int coul;
if (val<vmin) val=vmin;
if (val>vmax) val=vmax;
double offset,amp;
offset=offsetHue/100.;
amp=ampHue/100.;
float coef=(float)( offset +(val-vmin)/(vmax-vmin)*amp);
if (coef<0) coef=0;
if (coef>1) coef=1;
coul=new Color(coef,coef,coef).getRGB();
return coul;
}

/**return the RGB code grey for a value*/
private int getRGBcolorNwhite(double val)
{
int coul;
if (val<vmin) val=vmin;
if (val>vmax) val=vmax;
double offset,amp;
offset=offsetHue/100.;
amp=ampHue/100.;
float coef=(float)( offset +(val-vmin)/(vmax-vmin)*amp);
float h=hue;
float s=coef;
float b=1;
coul=Color.HSBtoRGB(h,s,b);
return coul;
}

/**return the RGB code for a  value*/
public int getRGB(double val)
{
int c=PalettePanel.GRAY_SCALE;
switch (type)
	{
	case PalettePanel.GRAY_SCALE  :
		c=getRGBnb(val);
	break;
	case PalettePanel.COLOR_N_WHITE  :
		c=getRGBcolorNwhite(val);
	break;
	case PalettePanel.COLOR  :
		c=getRGBcoul(val);
	break;
	}
return c;
}

public void update()
{
jp.repaint();
for (PaletteChangedListener l:listeners) l.onPaletteChanged();
}


public void addListener(PaletteChangedListener l)
{
	listeners.add(l);
}

public void keyPressed(KeyEvent e) 
{
boolean allowed=true;
char keychar=e.getKeyChar();
if (allowed)
	{
	switch (keychar)
		{
		case 'c'  :
			setType(getType()+1);
			update();
		break;
		case 'o'  ://more options
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"update"));
			String[] names1={"AmpHue","OffsetHue","Type"};
			ParamsBox paramsBox1=new ParamsBox(this,tl,names1);
			CJFrame cjf=new CJFrame();
			cjf.setTitle("Palette");
			cjf.getContentPane().setLayout(new FlowLayout());
			cjf.getContentPane().add(paramsBox1.getComponent());
			cjf.pack();
			cjf.setExitAppOnExit(false);
			cjf.setAutoCenter(true);
			cjf.setVisible(true);
		break;
		}
	}
	
}



public void keyReleased(KeyEvent arg0) {
	
	
}



public void keyTyped(KeyEvent arg0) {
	
	
}



public void mouseClicked(MouseEvent arg0) {
	
	
}



public void mouseEntered(MouseEvent arg0) 
{
//if (takeFocusOnMouseEnter == true)
		{
		jp.requestFocus();
		}
}
	




public void mouseExited(MouseEvent arg0) {
	
	
}



public void mousePressed(MouseEvent arg0) {
	
	
}



public void mouseReleased(MouseEvent arg0) {
	
	
}






public Component getVisualisation() {
	return jp;
}



public int getType()
{
	return type;
}

/**
 * Palette.GRAY_SCALE    Palette.COLOR_N_WHITE=0    Palette.COLOR=0
 * @param type
 */
public void setType(int t)
{
if (t>2) t=0;
if (t<0) t=2;
type=t;

if (t==2)
{
	offsetHue=20;
	ampHue=80;
}
else 
{
	offsetHue=0;
	ampHue=100;
}
}




public float getHue()
{
	return hue;
}

/**
 * hue used for the case Palette.COLOR_N_WHITE
 * @param hue
 */
public void setHue(float hue)
{
	this.hue = hue;
}

/**
 * hue used for the case Palette.COLOR_N_WHITE
 * @param hue
 */
public int getOffsetHue()
{
	return offsetHue;
}


public void setOffsetHue(int offsetHue)
{
	this.offsetHue = offsetHue;
}


public int getAmpHue()
{
	return ampHue;
}


public void setAmpHue(int ampHue)
{
	this.ampHue = ampHue;
}


public double getVmin()
{
	return vmin;
}


public double getVmax()
{
	return vmax;
}	


/**
 * 
 * @param w
 * @param h
 * @return
 */
public CImage getPaletteAsImage(int w,int h,double min,double max,NumberFormat nf,Font font)
{
//CImage cim=new CImage(w,h);
CImageProcessing cimp=new CImageProcessing(w,h);
CImage cim=cimp.getCImage();
Graphics g=cim.getImage().getGraphics( );
for (int i=0;i<h;i++)
	{
	double val=(1-(float)i/(float)h)*(vmax-vmin)+vmin;
	g.setColor(new Color(getRGB(val)));
	g.drawLine(0,i,w,i);
	}
//incrust the min and max values
g.setColor(Color.black);
g.setFont(font);
g.drawString(""+nf.format(max),1,20);
g.drawString(""+nf.format(min),1,h-20);
return cim;
}




}
