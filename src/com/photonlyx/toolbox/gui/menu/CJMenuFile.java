

package com.photonlyx.toolbox.gui.menu;

import java.awt.*;
import javax.swing.*;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.CFileManager;
import com.photonlyx.toolbox.io.NewFileAction;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;

import java.awt.event.*;
import java.io.File;


/**
A standard menu for opening a file, save, save as,print, quit...
After loading, open, etc, launch the sons of class WorkButton
*/
public class CJMenuFile  implements ActionListener
{
//links
private CFileManager cfm;
private CFileFilter cff;
private CJFrame fatherWindow;
private TriggerList triggerList;//the buttons that lauch the network depending on the file

//specific
private JMenu menu;
private JMenuItem[] items;


public  CJMenuFile(CFileManager ffm, CFileFilter fff,TriggerList triggerList,CJFrame fatherWindow)
{
this.cfm=ffm;
this.cff=fff;
this.fatherWindow=fatherWindow;
this.triggerList=triggerList;
//s=Messager.getString("File");
int nbmenus=6;
String[] labels=new String[nbmenus];
labels[0]=Messager.getString("New");
labels[1]=Messager.getString("Open_a_file");
labels[2]=Messager.getString("Save");
labels[3]=Messager.getString("Save_as");
labels[4]=Messager.getString("Print");
labels[5]=Messager.getString("Exit");
menu=new JMenu(Messager.getString("File"));

items =new JMenuItem[nbmenus];
for (int i=0;i<nbmenus;i++) items[i]=new JMenuItem(labels[i]);
for (int i=0;i<nbmenus;i++) items[i].addActionListener(this);
for (int i=0;i<nbmenus;i++) menu.add(items[i]);
items[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
items[1].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
items[2].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
items[5].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));

menu.setFont(Global.font);
for (int i=0;i<nbmenus;i++) items[i].setFont(Global.font);
	
//af(fff.getExtension());
ffm.newFile(NewFileAction.getNewFileName(fatherWindow.getApp().getPath(),fff.getExtension()));

}



public JMenu getJMenu()
{
return menu;
}



public void actionPerformed(ActionEvent e)
	{

	Object source = e.getSource();

	if (source==items[0]) //create a new file
		{
		if (!checkIfUserWantedToSave()) return;
		String s;
		cfm.newFile(s=NewFileAction.getNewFileName(fatherWindow.getApp().getPath(),cff.getExtension()));
		fatherWindow.setTitle(s);

		launchTriggerList();
		}
	if (source==items[1]) //open another file
		{
		if (!checkIfUserWantedToSave()) return;
		System.out.println("Open file in path:"+Global.path);
		String ch=Global.path;
		if (ch==null) ch=".";
		JFileChooser df=new JFileChooser(ch);
		df.addChoosableFileFilter(cff/*.getFileFilter()*/);
		df.setFileFilter(cff);
		int returnVal = df.showOpenDialog(fatherWindow);
		if(returnVal == JFileChooser.APPROVE_OPTION)
			{
			Global.path=df.getSelectedFile().getParent()+File.separator;
			String path=df.getSelectedFile().getParent()+File.separator;
			String fileName=df.getSelectedFile().getName();
			cfm.open(path,fileName);
			fatherWindow.setTitle(/*path+*/fileName);
			}
		launchTriggerList();
		}

	if (source==items[2])//save
		{
		boolean ok=cfm.save();
		if (!ok) saveAs();
		}
	if (source==items[3])//save as
		{
		saveAs();
		//launchTriggerList();
		}
	if (source==items[4]) //print the Visible son
		{
		//Component comp=fatherWindow;
		Messager.messErr("Not available yet !");
		//flexo.image.Utils.print(comp);
		}
	if (source==items[5])//quit
		{
//		if (saveAsFileAction==null) 
//			{
//			saveParamsAndClose();
//			return;
//			}
		if (!checkIfUserWantedToSave()) return;

		String answer=Messager.yesNoQuestion(Messager.getString("really_close_the_application")
			,Messager.getString("closing") );
	
		if (answer.compareTo("yes")==0) 
			{
			//TreeNode main=getNode().lookForFatherOfClass("flexo.jolo.MainJolobject",true);
			saveParamsAndClose();
			}
		}


	}

public void saveParamsAndClose()
{
	//TODO
//FApplication app=(FApplication)lookForFatherOfClass("flexo.FApplication",true);
//app.savePersistentParams();
//System.out.println(getClass()+" "+"Persistent parameters saved");
////System.out.println(Global.editionMode);
//if (!Global.editionMode) 
//	if (fatherWindow.exitAppOnClose()) 
//		{
//		System.exit(0);
//		}
fatherWindow.dispose();
}


//lauch the work actions of all the networks (work buttons here)
private void launchTriggerList()
{
if (triggerList!=null) triggerList.trig();
}



//save a file as a new name
private boolean saveAs()
{
String ch=Global.path;
if (ch==null) ch=".";
JFileChooser df=new JFileChooser(ch);
df.addChoosableFileFilter(cff/*.getFileFilter()*/);
df.setFileFilter(cff);
//df.setCurrentDirectory(new File(ch));
df.setSelectedFile(new File(cfm.getPath()+File.separator+cfm.getFileName()));
int returnVal = df.showSaveDialog(fatherWindow);
if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	Global.path=df.getSelectedFile().getParent()+File.separator;
	//System.out.println("Global.chemin:"+Global.chemin);
	String fileName=df.getSelectedFile().getName();
	String extension=cff.getExtension();
	if (!fileName.endsWith(extension)) fileName+="."+extension;
	String path=df.getSelectedFile().getParent()+File.separator;
	File f=new File(path+fileName);
	if(f.exists())
	//if(df.getSelectedFile().exists())
		{
		if (JOptionPane.showConfirmDialog(fatherWindow,Messager.getString("overwrite_file")+" "+fileName+"?"
			,Messager.getString("Confirm"), JOptionPane.YES_NO_OPTION)== JOptionPane.NO_OPTION)
			{
			return false;
			}
		else
			{
			cfm.save(path,fileName);
			fatherWindow.setTitle(/*path+*/fileName);
			}
      		}
	else 
		{
		cfm.save(path,fileName);
		fatherWindow.setTitle(/*path+*/fileName);
		}
	}
else return false;
return true;

}

public boolean checkIfUserWantedToSave()
{
if (!cfm.isSaved())
	{
	String answer1=Messager.yesNoQuestion(Messager.getString("Do_you_want_to_save_the_changes_done_to_your_file"),Messager.getString("closing") );
	System.out.println(getClass()+" "+answer1);
	if (answer1.equals("closed")) 
		{
		return false;
		}
	else if (answer1.equals("yes")) 
		{
		if (!cfm.isNewFile()) cfm.save(); else return saveAs();
		}
	}
return true;
}




}
