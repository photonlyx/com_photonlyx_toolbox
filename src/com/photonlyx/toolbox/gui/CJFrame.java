package com.photonlyx.toolbox.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.StringReader;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.HasPersistentParam;
import com.photonlyx.toolbox.util.Util;


/**
 * 
 * @author laurent
 *
 */
public class CJFrame extends JFrame implements WindowListener,CMessageHolder
{
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private int w=800,h=600;
	private int x=100,y=100;
	private boolean autoCenter=true; 
	//private boolean useStatusBar=true;//if true put a status bar at SOUTH for messages
	private JPanel statusBar=new JPanel();
	private JLabel message=new JLabel("PhotonLyX");//to print the status
	private TriggerList exitTriggerList;
	private boolean exitAppOnExit=true;
	private WindowApp app;


	public  CJFrame()
	{
		init(false);
	}

	public  CJFrame( WindowApp app)
	{
		this.app=app;
		init(false);
	}

	public  CJFrame( WindowApp app,boolean useStatusBar)
	{
		this.app=app;
		init(useStatusBar);
	}

	public  CJFrame( WindowApp app,Component c)
	{
		this.app=app;
		init(false);
		this.getContentPane().add(c);
	}

	private void init(boolean useStatusBar)
	{
		// bug fix 4736093 (see SunMicroSystem website)
		//jf.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ALT, Event.ALT_MASK, false), "repaint");
		this.addWindowListener(this);
		//jf.addKeyListener(this);
		// Set the window layout to BorderLayout
		this.getContentPane().setLayout(new BorderLayout());
		this.setBackground(Global.background);
		if (!autoCenter) setLocation(x,y);
		else 
		{
			Dimension d=getToolkit().getScreenSize();
			//		System.out.println("ScreenSize "+d.width+"x"+d.height);
			//		System.out.println("width="+this.getWidth()+"height="+this.getHeight());
			setLocation(d.width/2-w/2,d.height/2-h/2);
		}

		//add status bar
		if (useStatusBar)
		{
			message.setFont(Global.font);
			statusBar.setLayout(new BorderLayout());
			statusBar.setBackground(Global.background);
			statusBar.add(message,BorderLayout.WEST);
			statusBar.setPreferredSize(new Dimension(100,30));
			this.setSize(w,h);
			getContentPane().add(statusBar,BorderLayout.SOUTH);
			Global.addMessageHolder(this);
		}

		this.setSize(new Dimension(w,h));

		this.setVisible(true);


	}


	public void center()
	{
		//if (!autoCenter) setLocation(x,y);
		//else 
		{
			Dimension d=getToolkit().getScreenSize();
			//	System.out.println("ScreenSize "+d.width+"x"+d.height);
			//	System.out.println("width="+this.getWidth()+"height="+this.getHeight());
			setLocation(d.width/2-this.getWidth()/2,d.height/2-this.getHeight()/2);
			this.validate();
		}
	}



	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public boolean isAutoCenter() {
		return autoCenter;
	}
	public void setAutoCenter(boolean autoCenter) {
		this.autoCenter = autoCenter;
	}


	public JPanel getStatusBar(){return statusBar;}


	public void windowClosed(WindowEvent event){}
	public void windowDeiconified(WindowEvent event){}
	public void windowIconified(WindowEvent event){}
	public void windowActivated(WindowEvent event) {}
	public void windowDeactivated(WindowEvent event){}
	public void windowOpened(WindowEvent event) {}
	public void windowClosing(WindowEvent event)
	{
		if (exitAppOnExit) 
		{
			//save the working directory:
			Global.saveWorkingDirectory(app);
			//save the persistent parameters:
			Global.savePersistentParams(app);
			if (exitTriggerList!=null) exitTriggerList.trig();
			System.exit(0);
		}
	}



	public void setExitTriggerList(TriggerList exitTriggerList)
	{
		this.exitTriggerList = exitTriggerList;
	}




	public void setMessage(String s)
	{
		message.setText(s);
		//System.out.println(s);

	}



	public boolean isExitAppOnExit()
	{
		return exitAppOnExit;
	}



	public void setExitAppOnExit(boolean exitAppOnExit)
	{
		this.exitAppOnExit = exitAppOnExit;
	}


	public void updateWorkingDir(String appName)
	{
		//look for the folder where are stored the user params:
		app.appHome=Util.getOrCreateApplicationUserFolder(appName);
		//read the file for persistence:
		StringBuffer sb=TextFiles.readFile(app.appHome+File.separator+Global.workingDirectoryFileName, false,false);
		String s="";
		if (sb!=null) 
		{
			s=sb.toString();
			XMLElement xml = new XMLElement();
			StringReader reader = new StringReader(s);
			try {	xml.parseFromReader(reader);}
			catch (Exception ex) {ex.printStackTrace();}
			app.path=xml.getStringAttribute("PATH");
		}
	}





	public WindowApp getApp() {
		return app;
	}




}
