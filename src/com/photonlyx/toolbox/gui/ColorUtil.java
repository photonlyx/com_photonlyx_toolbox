// Author Laurent Brunel

package com.photonlyx.toolbox.gui;


import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.photonlyx.toolbox.txt.Definition;



public class ColorUtil
{

private static Pattern pattern = Pattern.compile("^rgb[ 	]+([0-9]+)[ 	]+([0-9]+)[ 	]*([0-9]+)");
private static Pattern pattern2 = Pattern.compile("^rgba[ 	]+([0-9]+)[ 	]+([0-9]+)[ 	]*([0-9]+)[ 	]*([0-9]+)");

public static Color getColor(String color)
{
Color col;
if (color.length()<=2) return Color.white;
//System.out.println("couleur:"+couleur);
color=color.replace('_',' ');
if (color.substring(0,3).compareTo("rgb")==0)
	{
	int r,g,b;
	if (color.substring(0,4).compareTo("rgba")==0)
		{
		Matcher matcher2 = pattern2.matcher(color);
		matcher2.find();
		 r=(int)Double.parseDouble(matcher2.group(1));
		 g=(int)Double.parseDouble(matcher2.group(2));
		 b=(int)Double.parseDouble(matcher2.group(3));
		int a=(int)Double.parseDouble(matcher2.group(4));
		col=new Color(r,g,b,a);
		}
	else
		{
		Matcher matcher = pattern.matcher(color);
		matcher.find();
		r=(int)Double.parseDouble(matcher.group(1));
		g=(int)Double.parseDouble(matcher.group(2));
		b=(int)Double.parseDouble(matcher.group(3));
		col=new Color(r,g,b);
		}
	}
else if (color.compareTo("green")==0) col=Color.green;
else if (color.compareTo("blue")==0) col=Color.blue;
else if (color.compareTo("cyan")==0) col=Color.cyan;
else if (color.compareTo("red")==0) col=Color.red;
else if (color.compareTo("orange")==0) col=Color.orange;
else if (color.compareTo("black")==0) col=Color.black;
else if (color.compareTo("lightGray")==0) col=Color.lightGray;
else if (color.compareTo("gray")==0) col=new Color(150,150,150);
else if (color.compareTo("darkGray")==0) col=Color.darkGray;
else if (color.compareTo("pink")==0) col=Color.pink;
else if (color.compareTo("yellow")==0) col=Color.yellow;
else if (color.compareTo("white")==0) col=Color.white;
else if (color.compareTo("pastelBlue")==0) col=new Color(185,231,244);
else if (color.compareTo("pastelRed")==0) col=new Color(244,189,169);
else if (color.compareTo("panelGray")==0) col=new Color(236,233,216);
else if (color.compareTo("orangePanelPhotonLyX")==0) col=new Color(254,219,153);
else if (color.compareTo("brown")==0) col=new Color(118,80,85);
else col=new Color(0);
return col;
}


public static Color getPanelGray() {return new Color(236,233,216);}


/**
 * 
 * @param couleur
 * @return
 */
public static float[] getHSB(String colorString)
{
Color c=getColor(colorString);
float[] hsb=new float[3];
Color.RGBtoHSB(c.getRed(),c.getGreen(),c.getBlue(),hsb);
return hsb;
}


/**encode the color in a string*/
public static String color2String(Color c)
{
return "rgba_"+c.getRed()+"_"+c.getGreen()+"_"+c.getBlue()+"_"+c.getAlpha();
}

}
