package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.photonlyx.toolbox.mysql.MysqlRow;
import com.photonlyx.toolbox.param.ParBool;
import com.photonlyx.toolbox.param.ParDouble;
import com.photonlyx.toolbox.param.ParInt;
import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;




/**
permits to show and edit a param (name,value,unit) and trig objects when value changed

 */
public class ParBoolBox  extends ParamBox implements ActionListener
{
	private String unit="";
	private String label="";
	private boolean editable=true;

	private TriggerList triggerList;//list of object to update when this parameter is changed
	private ParBool p;
	private JRadioButton val;
	private JLabel name,labelUnit;


	private JPanel jp=new JPanel();

	/**
	 * 
	 * @param tl can be null
	 * @param p
	 */
	public ParBoolBox(TriggerList tl,ParBool p,int w,int h,String label,String unit)
	{
		//System.out.println(getClass()+" contructor. param "+p.getParamName()+"  val:"+p.val());	
		triggerList=tl;	
		this.p=p;
		this.setW(w);
		this.setH(h);

		jp.removeAll();

		//int w=intVal("w",150);
		//int h=intVal("h",30);
		jp.setSize(getW(),getH());
		jp.setPreferredSize(new Dimension(getW(),getH()));
		jp.setBackground(this.getBackground());

		name=new JLabel();
		name.setFont(this.getFont());
		name.setBackground(this.getBackground());
		if (label!=null) name.setText(label);else name.setText(p.getParamName());
		jp.add(name);
		
		
		jp.add(name);

		val=new JRadioButton();

		jp.add(val);
		val.addActionListener(this);
		val.setFont(this.getFont());
		val.setBackground(this.getBackground());

		if (unit!=null) 
		{
			labelUnit=new JLabel("");
			labelUnit.setFont(this.getFont());
			labelUnit.setBackground(this.getBackground());
			jp.add(labelUnit);
			labelUnit.setText(unit);
		}


		val.doLayout();
		update();
	}


	public void setSize(int w, int h)
	{
		super.setW(w);
		super.setH(h);
		//init();
		update();
	}



	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setLabel(String label) {
		this.label = label;
	}


	public void setForeground(String color) 
	{
		Color foreground=ColorUtil.getColor(color);
		jp.setForeground(foreground);
		name.setForeground(foreground);
		val.setForeground(foreground);
		labelUnit.setForeground(foreground);
	}

	public void setBackground(String color) 
	{
		Color background=ColorUtil.getColor(color);
		jp.setBackground(background);
		name.setBackground(background);
		val.setBackground(background);
		labelUnit.setBackground(background);
	}

	public void setValBackground(String color) 
	{
		Color background=ColorUtil.getColor(color);
		val.setBackground(background);
	}

	public TriggerList getTriggerList() {
		return triggerList;
	}


	public void setTriggerList(TriggerList triggerList) {
		this.triggerList = triggerList;
	}



	public boolean isEditable() {
		return editable;
	}


	public void setEditable(boolean editable) {
		this.editable = editable;
		//init();
	}



	public void update()
	{
		val.setSelected(p.val());
	}

	public void actionPerformed(ActionEvent e)
	{
		//System.out.println(getClass()+" val box changed ");
		if (val.getSelectedObjects()==null) 
		{
			p.setVal(false);
		} 
		else 
		{
			p.setVal(true);
		}
//		//if the object that have this parameter is a MysqlLine, update the database:
//		if (p.getParamOwner() instanceof MysqlLine) 
//		{
//			MysqlLine l=(MysqlLine)p.getParamOwner();
//			l.getTable().updateSQL(l,p.getParamName());
//		}

		if (triggerList!=null) if (triggerEnabled) 
		{
			triggerList.trig();
			//System.out.println(getClass()+" param: "+p.getParamName()+" trig");
		}
		//System.out.println(getClass()+" val="+p.val());
	}




	public Component getComponent(){return jp;}

	public String getParamName()
	{
		return p.getParamName();
	}

	public void setEnabled(boolean b)
	{
		val.setEnabled(b);
		//name.setEnabled(b);
		//labelUnit.setEnabled(b);
	}


	public Param getParam(){return p;}


	public void setFieldLength(int fieldLength) {

	}

}