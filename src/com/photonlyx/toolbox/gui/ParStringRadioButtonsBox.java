package com.photonlyx.toolbox.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;

public class ParStringRadioButtonsBox extends ParamBox implements  ActionListener
{
	private JPanel jp=new JPanel(new GridLayout(0, 1));
	private JLabel name;
	private TriggerList triggerList;//list of object to update when this parameter is changed
	private ParString p;
	private Vector<JRadioButton> rbs=new Vector<JRadioButton>();
	private ButtonGroup group = new ButtonGroup();

	public ParStringRadioButtonsBox(TriggerList tl,ParString p)
	{
		this.triggerList=tl;	
		this.p=p;
		init();
	}

	public ParStringRadioButtonsBox(TriggerList tl,ParString p,String label)
	{
		this.triggerList=tl;	
		this.p=p;
		init();
		name.setText(label);
	}

	public void init()
	{
		jp.removeAll();
		jp.setSize(getW(),getH());
		jp.setPreferredSize(new Dimension(getW(),getH()));
		jp.setBackground(this.getBackground());
		jp.setLayout(new GridLayout(0,1));

		name=new JLabel();
		name.setFont(this.getFont());
		name.setBackground(this.getBackground());
		jp.add(name);
	}


	public void addItem(String string)
	{
		JRadioButton jrb=new JRadioButton(string);
		jrb.setFont(this.getFont());
		jrb.setBackground(this.getBackground());
		//jrb.setBackground(Color.red);
		//jp.setBackground(Color.yellow);
		jrb.addActionListener(this);
		group.add(jrb);
		rbs.add(jrb);
		jp.setPreferredSize(new Dimension(getW(),getH()*(int)(rbs.size()*1.0)));
		jrb.setPreferredSize(new Dimension(getW(),getH()));
		jp.add(jrb);
		jp.validate();
		update();
	}


	//select the item corresponding to the param having the selected value
	public void synchronizeToParam()
	{
		for (JRadioButton jrb:rbs) 
		{
			if (jrb.getActionCommand().compareTo(p.stringVal())==0) 
			{
				//System.out.println(s+" match with param:"+p.stringVal());
				jrb.setSelected(true);
			}
		}
	}

	public void setSelected(String s)
	{
		for (JRadioButton jrb:rbs) if (jrb.getActionCommand().compareTo(s)==0) jrb.setSelected(true);
	}

	private JRadioButton getSelected()
	{
		for (JRadioButton jrb:rbs) if (jrb.isSelected())return jrb;
		return null;
	}

	//private void actualiseParamFromBox()
	//{
	//if (p==null) return;
	////System.out.println(getClass()+" actualiseParamFromBox : "+val.getText());
	//if (p instanceof ParString) 
	//	{
	//	((ParString)p).setVal(getSelected().getActionCommand());
	//	}
	//else 	
	//	{
	//	}
	//if (triggerList!=null) triggerList.trig();
	////af("val "+p.val());
	//}
	//

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

	}

	public void actionPerformed(ActionEvent e)
	{
		//System.out.println(e.getActionCommand());
		p.setVal(e.getActionCommand());
		if (triggerList!=null) triggerList.trig();
	}

	@Override
	public Component getComponent()
	{
		return jp;
	}

	@Override
	public void update()
	{
		synchronizeToParam();
	}

	@Override
	public String getParamName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setEnabled(boolean b)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Param getParam()
	{
		// TODO Auto-generated method stub
		return null;
	}


	public void setEditable(boolean editable) {

	}


	public void setFieldLength(int fieldLength) {

	}

}
