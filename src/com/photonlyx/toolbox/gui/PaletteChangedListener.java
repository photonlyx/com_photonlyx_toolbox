package com.photonlyx.toolbox.gui;

public interface PaletteChangedListener 
{
public void onPaletteChanged();
}
