package com.photonlyx.toolbox.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.thread.CTask2;
import com.photonlyx.toolbox.thread.TaskEndListener;
import com.photonlyx.toolbox.triggering.TriggerList;

public class ButtonImageTask extends JPanel implements MouseListener,TaskEndListener
{
	private CTask2 t;
	private BufferedImage im,imStopped,imRunning,imDisable;	
	private TriggerList triggerListBefore ;//to trig before starting
	private TriggerList triggerListAfter ;//to trig before starting
	private Thread thread;
	//private Window tipWindow = new Window(null);
	private boolean disable=false;
	private String tip="";
	private TipWindow tw=null;


	public ButtonImageTask(CTask2 t,String imageFileInResources)
	{
		this.t=t;

		//this.setBackground(Color.RED);
		this.setPreferredSize(new Dimension(20,20));

		Image im0=com.photonlyx.toolbox.util.Util.getImage(imageFileInResources,true);
		CImageProcessing cimp=new CImageProcessing(new CImage(im0));
		//cimp._keepOnlyRedChannel();
		imStopped=cimp.getBufferedImage();

		//take the same size of the image:
		this.setPreferredSize(new Dimension(imStopped.getWidth(),imStopped.getHeight()));

		//running look:
		CImageProcessing cimpRunning=cimp.getAcopy();
		cimpRunning._enhanceRed();
		imRunning=cimpRunning.getBufferedImage();

		//disable look:
		CImageProcessing cimpDisable=cimp.getAcopy();
		cimpDisable._desaturate();
		imDisable=cimpDisable.getBufferedImage();

		t.setTaskEndListener(this);

		this.addMouseListener(this);

		t.setStopped(true);
		this.setStoppedLook();

	}


	public void  paintComponent(Graphics g)
	{
		Graphics2D g2=(Graphics2D)g;
		//setPreferredSize(new Dimension(cim.w(),cim.h()));
		g.drawImage(im, 0, 0, this);
	}


	public void setTip(String tip)
	{
		this.tip=tip;
	}


	public void start()
	{
		//launch sons subnetworks:
		if (triggerListBefore != null)
		{
			triggerListBefore.trig();
		}

		if (thread!=null)
		{
			do
			{
				//System.out.println("Wait!");
			}
			while (thread.isAlive());
		}
		t.setStopped(false);
		//stopped=false;

		setRunningLook();

		thread=new Thread(t);
		//System.out.println("Button stopped="+stopped);
		thread.start();
	}

	public void stop()
	{
		//System.out.println(this.getClass()+"+++++ stop: "+t.getClass());
		t.setStopped(true);
		setStoppedLook();
		if (triggerListAfter != null)
		{
			triggerListAfter.trig();
		}
	}


	public void setRunningLook()
	{
		im=imRunning;
		repaint();
	}

	public void setStoppedLook()
	{
		im=imStopped;
		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) 
	{
		if (arg0.getButton()!=MouseEvent.BUTTON1) return;
		if (tw!=null) tw.setVisible(false);		
		if (disable) return;
		if (t.isStopped())
		{
			start();
		}
		else
		{
			//System.out.println(getClass()+" mouse clicked. Ask stop . (stopped="+t.isStopped()+")");	
			stop();
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) 
	{
		Point p=this.getLocationOnScreen();
		p.translate(20, -im.getHeight());
		//if (tw==null) tw=new TipWindow(this,tip);else tw.showMe();
	}

	@Override
	public void mouseExited(MouseEvent arg0) 
	{
		if (tw!=null) tw.setVisible(false);
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}



	public void setDisable(boolean b)
	{
		disable=b;
		if (disable)	im=imDisable; else im=imStopped; 
		repaint();
	}


	@Override
	public void whenTaskFinished() 
	{
		stop();	
	}









	//	
	//	
	//	class ShowTipWithTimeOut implements Runnable
	//	{
	//		@Override
	//		public void run()
	//		{
	//			for (int i=0;i<100;i++)
	//			{
	//				//System.out.println(getClass());
	//				try { Thread.sleep(10);} catch(Exception e) {}
	//			}
	//			tipWindow.setVisible(false);		
	//		}
	//	}
	//













}
