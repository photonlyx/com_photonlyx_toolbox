package com.photonlyx.toolbox.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;

import javax.swing.JLabel;

import com.photonlyx.toolbox.util.Global;

public class TipWindow extends Window
{
	private Component p;

	public TipWindow(Component p,String tip)
	{
		super(null);
		this.p=p;
		//tipWindow = new Window(jf);
		//tool tip
		Window tipWindow=this;
		tipWindow.setSize(new Dimension(100,20));
		JLabel jl=new JLabel(tip);
		jl.setFont(Global.font);
		jl.setBackground(Global.background);
		tipWindow.add(jl);
		tipWindow.pack();
		tipWindow.setAlwaysOnTop(true);

		tipWindow.setLocation(p.getLocationOnScreen());
		tipWindow.repaint();

		showMe() ;
	}



	public void showMe() 
	{
		//setVisible(true);
		Thread thread0=new Thread(new ShowTipWithTimeOut());
		thread0.start();
	}




	class ShowTipWithTimeOut implements Runnable
	{
		@Override
		public void run()
		{
			for (int i=0;i<20;i++)
			{
				//System.out.println(getClass());
				try { Thread.sleep(10);} catch(Exception e) {}
			}
		setVisible(true);
		for (int i=0;i<100;i++)
			{
				//System.out.println(getClass());
				try { Thread.sleep(10);} catch(Exception e) {}
			}
			setVisible(false);		
		}
	}






}
