package com.photonlyx.toolbox.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.photonlyx.toolbox.util.Global;



public class MultiPanel  implements MouseListener
{
private JPanel jp=new JPanel();
private Vector<MiniPanel> v=new Vector<MiniPanel>();
private MiniPanel expandedPanel=null;

public MultiPanel()
{
jp.setLayout(new FlowLayout());
jp.setBackground(Global.background);
}


public void add(JPanel njp,String title,Color color)
{
MiniPanel minip=new MiniPanel(this,njp,title, color);
jp.add(minip);
v.add(minip);
expandPanel(minip);
}




public Component getComponent()
{
return jp;
}


public void update()
{
if (expandedPanel==null) 
	{
	jp.removeAll();
	jp.setLayout(new FlowLayout());
	for (MiniPanel minip:v) jp.add(minip);
	jp.validate();
	}	
else 
	{
	jp.removeAll();
	jp.setLayout(new BorderLayout());
	jp.add(expandedPanel,BorderLayout.CENTER);
	jp.validate();
	}
}

public void expandPanel(MiniPanel minip)
{
expandedPanel=minip;
update();
}

public void minimizePanels()
{
expandedPanel=null;
update();
}



public MiniPanel getExpandedPanel()
{
return expandedPanel;
}


public void mouseClicked(MouseEvent e)
{
// TODO Auto-generated method stub

}




public void mouseEntered(MouseEvent e)
{
// TODO Auto-generated method stub

}


public void mouseExited(MouseEvent e)
{
// TODO Auto-generated method stub

}


public void mousePressed(MouseEvent e)
{
// TODO Auto-generated method stub

}


public void mouseReleased(MouseEvent e)
{
// TODO Auto-generated method stub

}



public static void main(String[] args)
{
CJFrame cjf=new CJFrame();

MultiPanel mp=new MultiPanel();

cjf.add(mp.getComponent());


JPanel jp1=new JPanel();
jp1.setBackground(Color.red);


JPanel jp2=new JPanel();
jp2.setBackground(Color.blue);

mp.add(jp1, "red",Color.green);
mp.add(jp2, "blue",Color.green);


cjf.validate();

}



class MiniPanel extends JPanel implements MouseListener
{
private MultiPanel mp;
private JLabel label_title;

public MiniPanel (MultiPanel mp,JPanel jp,String title,Color color)
{
this.mp=mp;
this.setBackground(Global.background);
label_title=new JLabel(title);
label_title.setBackground(Global.background);
Font font=Global.font.deriveFont((float)16);
font=font.deriveFont(Font.BOLD);
label_title.setFont(font);
label_title.setForeground(color);
label_title.setAlignmentX(CENTER_ALIGNMENT);
label_title.setAlignmentY(CENTER_ALIGNMENT);
int border=10;
label_title.setBorder(BorderFactory.createMatteBorder(border,border, border, border, Global.background));
this.setLayout(new BorderLayout());
this.add(label_title,BorderLayout.NORTH);
this.add(jp,BorderLayout.CENTER);
this.setPreferredSize(new Dimension(200,200));
this.addMouseListener(this);

}

public void mouseClicked(MouseEvent arg0)
{
//System.out.println(getClass()+" mouseClicked");
//if (arg0.getClickCount()==2) 

if (mp.getExpandedPanel()!=null)	mp.minimizePanels();
else 
	mp.expandPanel(this);

}

public void mouseEntered(MouseEvent arg0)
{
// TODO Auto-generated method stub

}

public void mouseExited(MouseEvent arg0)
{
// TODO Auto-generated method stub

}

public void mousePressed(MouseEvent arg0)
{
// TODO Auto-generated method stub

}

public void mouseReleased(MouseEvent arg0)
{
// TODO Auto-generated method stub

}


}




}
