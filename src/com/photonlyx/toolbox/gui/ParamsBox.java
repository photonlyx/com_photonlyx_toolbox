package com.photonlyx.toolbox.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

public class ParamsBox extends JPanel
{
	private Object paramOwner;
	private TriggerList tl;//triggered whe parameter is changed
	private String[] paramNamesList=null;
	private String[] paramUnitsList=null;
	private String[] paramLabelsList=null;
	private String label;
	//private JPanel jp=new JPanel();
	private Vector<ParamBox> boxesList=new Vector<ParamBox>();
	private JLabel jLabel;
	//public static int defaultBoxHeight=30	;//height of each param box
	//public static int defaultBoxWidth=180	;//width of each param box
	public int boxesHeight=30	;//height of each param box
	public int boxesWidth=180	;//width of each param box
	public static final int VERTICAL=0;
	public static final int HORIZONTAL=1;
	private int orientation=VERTICAL;
	private boolean useCombos=true;//use combo boxes if options are available

	public ParamsBox(Object paramOwner,TriggerList tl,String[] paramNamesList)
	{
		this.paramOwner=paramOwner;
		this.tl=tl;
		this.paramNamesList=paramNamesList;
		this.label=null;
		setgui();

	}

	public ParamsBox(Object paramOwner,TriggerList tl,String label,String[] paramNamesList)
	{
		this.paramOwner=paramOwner;
		this.tl=tl;
		this.paramNamesList=paramNamesList;
		this.label=label;
		setgui();

	}


	/**
	 * 
	 * @param paramOwner
	 * @param tl
	 * @param label
	 * @param paramNamesList
	 * @param orientation VERTICAL or HORIZONTAL
	 */
	public ParamsBox(Object paramOwner,TriggerList tl,String label,String[] paramNamesList,int orientation)
	{
		this.paramOwner=paramOwner;
		this.tl=tl;
		this.paramNamesList=paramNamesList;
		this.label=label;
		this.orientation=orientation;
		setgui();

	}


	public ParamsBox(Object paramOwner,TriggerList tl,String label,String[] paramNamesList,
			String[] paramUnitsList)
	{
		this.paramOwner=paramOwner;
		this.tl=tl;
		this.paramNamesList=paramNamesList;
		this.paramUnitsList=paramUnitsList;
		this.label=label;
		setgui();

	}

	public ParamsBox(Object paramOwner,TriggerList tl,String label,String[] paramNamesList,
			String[] paramLabelsList,String[] paramUnitsList)
	{
		this.paramOwner=paramOwner;
		this.tl=tl;
		this.paramNamesList=paramNamesList;
		this.paramUnitsList=paramUnitsList;
		this.paramLabelsList=paramLabelsList;
		this.label=label;
		setgui();

	}


	public ParamsBox(Object paramOwner,TriggerList tl,String label,String[] paramNamesList,
			String[] paramLabelsList,String[] paramUnitsList,int orientation)
	{
		this.paramOwner=paramOwner;
		this.tl=tl;
		this.paramNamesList=paramNamesList;
		this.paramUnitsList=paramUnitsList;
		this.paramLabelsList=paramLabelsList;
		this.label=label;
		this.orientation=orientation;
		setgui();

	}

	public ParamsBox(Object paramOwner,TriggerList tl,String label,String[] paramNamesList,
			String[] paramLabelsList,String[] paramUnitsList,int orientation,int boxesWidth,int boxesHeight)
	{
		this.paramOwner=paramOwner;
		this.tl=tl;
		this.paramNamesList=paramNamesList;
		this.paramUnitsList=paramUnitsList;
		this.paramLabelsList=paramLabelsList;
		this.label=label;
		this.orientation=orientation;
		this.boxesWidth=boxesWidth;
		this.boxesHeight=boxesHeight;
		setgui();

	}

	public ParamsBox(Object paramOwner,TriggerList tl,String label,String[] paramNamesList,
			String[] paramLabelsList,String[] paramUnitsList,int orientation,int boxesWidth,int boxesHeight,boolean useCombos)
	{
		this.paramOwner=paramOwner;
		this.tl=tl;
		this.paramNamesList=paramNamesList;
		this.paramUnitsList=paramUnitsList;
		this.paramLabelsList=paramLabelsList;
		this.label=label;
		this.orientation=orientation;
		this.boxesWidth=boxesWidth;
		this.boxesHeight=boxesHeight;
		this.useCombos=useCombos;
		setgui();

	}


	private void setgui()
	{
		JPanel jp=this;
		jp.setLayout(new FlowLayout());
		jp.setBackground(Global.background);
		if (label!=null) 
		{
			jp.add(jLabel=new JLabel(label));
			jLabel.setFont(Global.font);
		}
		for (int i=0;i<paramNamesList.length;i++) 
		{
			String name=paramNamesList[i];
			String unit=null,label=null;
			if (paramUnitsList!=null) unit=paramUnitsList[i];
			if (paramLabelsList!=null) label=paramLabelsList[i];
			ParamBox.autoUpdate=false;
			ParamBox pb=ParamBox.getParamBoxInstance(paramOwner,name,label,unit,tl,boxesWidth,boxesHeight,useCombos);
			//pb.getComponent().setBackground(Color.red);
			if (pb!=null) 
			{
				jp.add(pb.getComponent());	
				boxesList.add(pb);
			}
			ParamBox.autoUpdate=true;
		}
		adaptSize();
	}

	/**
	 * 
	 */
	public void adaptSize()
	{
		JPanel jp=this;
		int w=0,h=0;
		//System.out.println(getClass()+" update");
		if (orientation==VERTICAL) 
		{
			w=boxesWidth+5;
			h=boxesHeight*boxesList.size()+10;	
			if (label!=null) h+=boxesHeight+10;
		}
		else //orientation==HORIZONTAL // h is the max height and w is the sum of widths
		{	
			w=boxesWidth*boxesList.size()+50;
			h=boxesHeight+10;
			if (label!=null) w+=jLabel.getWidth();
		}
		jp.setPreferredSize(new Dimension(w,h));
	}



	public void update()
	{
		//System.out.println(getClass()+" update");
		for (ParamBox pb:boxesList) 
		{
			pb.update();	
		}
	}




	public int getOrientation()
	{
		return orientation;
	}

	/**
	 * set as row or column
	 * @param orientation ParamsBox.HORIZONTAL or VERTICAL
	 */
	public void setOrientation(int orientation)
	{
		this.orientation = orientation;
	}


	public void setTriggerEnabled(boolean b)
	{
		for (ParamBox pb:boxesList) pb.setTriggerEnabled(b);
	}


	///**
	// * get the type of param: double, int ,....
	// * @param paramOwner
	// * @param paramName
	// * @return
	// */
	//public Class getType(Object paramOwner,String paramName)
	//{
	//Method[] methods=paramOwner.getClass().getMethods();
	//Class c=null;
	//for (Method m:methods) 
	//	{
	//	String name=m.getName();
	//	//System.out.println(name);
	//	if (  (name.compareTo("get"+paramName)==0)||(name.compareTo("is"+paramName)==0) ) 
	//		{
	//		c=m.getReturnType();
	//		break;
	//		}
	//	}	
	//if (c==null) 
	//	Messager.messErr(paramOwner.getClass().getSimpleName()+" has no get"+paramName+ " or is"+paramName+" method");
	//return c;
	//}

	public Component getComponent(){return this;}
	public JPanel getJPanel(){return this;}



	/**
	 * get the ParamBox of an index
	 * @param index
	 * @return
	 */
	public ParamBox getParamBox(int index) 
	{
		return boxesList.elementAt(index);
	}

	public ParamBox getParamBoxOfParam(String string) {
		for (ParamBox pb:boxesList) if (pb.getParamName().compareTo(string)==0) return pb;
		return null;
	}

	//public void setLocation(int i, int j)
	//{
	//this.setLocation(i, j);
	//	
	//}


	public void setEnabled(boolean b)
	{
		for (ParamBox pb:boxesList) 	pb.setEnabled(b);
	}

	/**
	 * set the width of the box
	 * @param w
	 */
	public void setPreferredWidth(int w)
	{
		int height=boxesHeight*boxesList.size();
		if (jLabel!=null) height+=2*boxesHeight;
		this.setPreferredSize(new Dimension(w, height));
		for (ParamBox pb:boxesList) pb.getComponent().setPreferredSize(new Dimension(w, boxesHeight));
	}


	///**
	// * set the default individual param box height
	// * @param h
	// */
	//public static void setDefaultParamBoxHeight(int h)
	//{
	//defaultBoxHeight=h;
	//}

	/**
	 * set the height of each param box
	 */
	public void setParamBoxHeight(int h)
	{
		JPanel jp=this;
		int height=h*boxesList.size();
		if (jLabel!=null) height+=boxesHeight;
		int w=jp.getPreferredSize().width;
		jp.setPreferredSize(new Dimension(w, height));
		for (ParamBox pb:boxesList) pb.getComponent().setPreferredSize(new Dimension(w, h));
	}


	public void setEditable(boolean b)
	{
		for (ParamBox pb:boxesList) pb.setEditable(b);

	}

	public void removeBox(String name)
	{
		boxesList.remove(getParamBoxOfParam(name));
	}

	public void setAllFieldsLength(int i)
	{
		// TODO Auto-generated method stub
		for (ParamBox pb:boxesList)
		{
			pb.setFieldLength(13);
		}

	}

	public Object getParamOwner() {
		return paramOwner;
	}




}
