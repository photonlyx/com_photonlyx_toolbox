package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.photonlyx.toolbox.param.ParDouble;
import com.photonlyx.toolbox.param.ParInt;
import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;




/**
permits to show and edit a param (name,value,unit) and trig objects when value changed

*/
public class ParIntOptionsBox  extends ParamBox implements  ActionListener
{
private String unit="";
private String label="";
private Color valbackground=Color.lightGray;
private boolean editable=true;
	
private TriggerList triggerList;//list of object to update when this parameter is changed
private ParInt  p;
private JComboBox<String> combo;
private JLabel name,labelUnit;
private int[] options;

private JPanel jp=new JPanel();

/**
 * 
 * @param tl can be null
 * @param p
 */
public ParIntOptionsBox(TriggerList tl,ParInt p,int[] options)
{
this.options=options;
triggerList=tl;	
this.p=p;
init();
}


public void init()
{

jp.removeAll();
	
//int w=intVal("w",150);
//int h=intVal("h",30);
jp.setSize(getW(),getH());
jp.setPreferredSize(new Dimension(getW(),getH()));
jp.setBackground(this.getBackground());

name=new JLabel();
name.setFont(this.getFont());
name.setBackground(this.getBackground());
jp.add(name);

combo=new JComboBox<String>();

jp.add(combo);
//val.setOpaque(true);
//val.setEditable(editable);
combo.setFont(this.getFont());
combo.setBackground(this.getBackground());
combo.addActionListener(this);

labelUnit=new JLabel("");
labelUnit.setFont(this.getFont());
labelUnit.setBackground(this.getBackground());
jp.add(labelUnit);

String save=p.stringVal();
for (int s:options) combo.addItem(s+"");
p.setVal(save);
synchronizeToParam();


System.out.println("param "+p.getParamName()+" val="+p.stringVal());

combo.doLayout();
update();
}


public void setSize(int w, int h)
{
super.setW(w);
super.setH(h);
init();
update();
}



public void setUnit(String unit) {
	this.unit = unit;
}

public void setLabel(String label) {
	this.label = label;
}


public void setForeground(String color) 
{
Color foreground=ColorUtil.getColor(color);
jp.setForeground(foreground);
name.setForeground(foreground);
combo.setForeground(foreground);
labelUnit.setForeground(foreground);
}

public void setBackground(String color) 
{
Color background=ColorUtil.getColor(color);
jp.setBackground(background);
name.setBackground(background);
combo.setBackground(background);
labelUnit.setBackground(background);
}

public void setValBackground(String color) 
{
Color background=ColorUtil.getColor(color);
combo.setBackground(background);
}

public TriggerList getTriggerList() {
	return triggerList;
}


public void setTriggerList(TriggerList triggerList) {
	this.triggerList = triggerList;
}




public boolean isEditable() {
	return editable;
}


public void setEditable(boolean editable) {
	this.editable = editable;
	//init();
}







public void update()
{
labelUnit.setText(unit);
name.setText(label);
//val.setText(p.stringVal());
synchronizeToParam();
}



public Component getComponent(){return jp;}

public String getParamName()
{
return p.getParamName();
}

public void setEnabled(boolean b)
{
combo.setEnabled(b);
name.setEnabled(b);
labelUnit.setEnabled(b);
}


public void addItem(String string)
{
String save=p.stringVal();//seems that add an item change the param value !....;
combo.addItem(string);
p.setVal(save);
update();
}

public void removeAllItems()
{
combo.removeAllItems();
update();
}


/**
 * select the item corresponding to the param having the selected value
 */
public void synchronizeToParam()
{
for (int i=0;i<combo.getItemCount();i++) 
	{
	Object o=combo.getItemAt(i);
	String s=(String)o;
	//System.out.println(i+" check:"+s);
	if (p.stringVal()!=null)
	if (s.compareTo(p.stringVal())==0) 
		{
		//System.out.println(s+" match with param:"+p.stringVal());
		combo.setSelectedItem(o);
		}
	}
}


public void actionPerformed(ActionEvent e)
{
// System.out.println(getClass()+" "+"actionPerformed");
JComboBox<String> cb = (JComboBox<String>)e.getSource();
Object selectedObject = cb.getSelectedItem();
if (!ParamBox.autoUpdate)return;

if (selectedObject instanceof String)
	{
	String s = (String)selectedObject;
	p.setVal(s);
	if (triggerList!=null) triggerList.trig();
	//System.out.println("ComboBox trig, object selected="+s);
	}
//System.out.println("ComboBox trig");
}

public Param getParam(){return p;}

public void setFieldLength(int fieldLength) {

}

}