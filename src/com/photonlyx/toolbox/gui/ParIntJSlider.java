package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.photonlyx.toolbox.param.ParDouble;
import com.photonlyx.toolbox.param.ParInt;
import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;




/**
slider that modify a ParInt

*/
public class ParIntJSlider  extends ParamBox implements  ChangeListener
{
private String unit="";
private String label="";
private boolean editable=true;
	
private TriggerList triggerList;//list of object to update when this parameter is changed
private ParInt p;
private JSlider slider;



private JPanel jp=new JPanel();

/**
 * 
 * @param tl can be null
 * @param p
 */
public ParIntJSlider(TriggerList tl,ParInt p)
{
triggerList=tl;	
this.p=p;
init();
}


public void init()
{
jp.removeAll();
	
//int w=intVal("w",150);
//int h=intVal("h",30);
jp.setSize(getW(),getH());
jp.setPreferredSize(new Dimension(getW(),getH()));
jp.setBackground(this.getBackground());
//jp.setBackground(Color.red);
jp.setLayout(new BorderLayout());

slider=new JSlider();
slider.setSize(new Dimension(this.getW(),this.getH()));
slider.setPreferredSize(new Dimension(this.getW()-100,this.getH()));
//val.setPreferredSize(new Dimension(400,100));

jp.add(slider,BorderLayout.CENTER);

//val.setOpaque(true);
//val.setEditable(editable);
slider.setFont(this.getFont());
slider.setBackground(this.getBackground());
slider.addChangeListener(this);

slider.doLayout();
update();
}


public void setSize(int w, int h)
{
super.setW(w);
super.setH(h);
init();
update();
}



public void setUnit(String unit) {
	this.unit = unit;
}

public void setLabel(String label) {
	this.label = label;
}


public void setForeground(String color) 
{
Color foreground=ColorUtil.getColor(color);
jp.setForeground(foreground);
slider.setForeground(foreground);
}

public void setBackground(String color) 
{
Color background=ColorUtil.getColor(color);
jp.setBackground(background);
slider.setBackground(background);
}

public void setBackground(Color c) 
{
jp.setBackground(c);
slider.setBackground(c);
}



public void setValBackground(String color) 
{
Color background=ColorUtil.getColor(color);
slider.setBackground(background);
}

public TriggerList getTriggerList() {
	return triggerList;
}


public void setTriggerList(TriggerList triggerList) {
	this.triggerList = triggerList;
}




public boolean isEditable() {
	return editable;
}


public void setEditable(boolean editable) {
	this.editable = editable;
	//init();
}







public void update()
{
synchronizeToParam();
}




public Component getComponent(){return jp;}

public String getParamName()
{
return p.getParamName();
}

public void setEnabled(boolean b)
{
slider.setEnabled(b);
}



//select the item corresponding to the param having the selected value
public void synchronizeToParam()
{
//slider.setValue(((ParInt)p).val());
slider.setValue(p.val());

}


public Param getParam(){return p;}


public void stateChanged(ChangeEvent e)
{
//System.out.println("slider val="+e.getClass());
JSlider source = (JSlider)e.getSource();
//if (!source.getValueIsAdjusting()) 
	{
	int s=source.getValue();
	p.setVal(s+"");
	if (triggerList!=null) 
		{
		triggerList.trig();
		}
//System.out.println("slider val="+s);
   }
}


public JSlider getJSlider(){return slider;}

public void setFieldLength(int fieldLength) {

}

}