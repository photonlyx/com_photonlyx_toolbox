// Author Laurent Brunel

package com.photonlyx.toolbox.gui;



import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;



public class TextEditor extends CJFrame implements ActionListener, WindowListener
{
/**
	 * 
	 */
private static final long serialVersionUID = 8721361849802054209L;

private int posx=50,posy=50,width=400,height=500;

//private String title="Text Editor";
private JTextArea txtArea= new JTextArea();
private String fileName;//name of associated txt file
private static JMenuItem[] menus1,menus2,menus3;

public TextEditor() 
{ 
init();
}
public TextEditor(String s) 
{ 
init();
setText(s);
}

public void init() 
{ 
//setTitle(title);
//setLocation(posx,posy);
//setSize(width,height);
addWindowListener(this);
int nbmenus1=5;
int nbmenus2=0;
int nbmenus3=0;

menus1 =new JMenuItem[nbmenus1];
menus2 =new JMenuItem[nbmenus2];
menus3 =new JMenuItem[nbmenus3];

String s1=Messager.getString("File");
String[] labels1=new String[nbmenus1];
labels1[0]=Messager.getString("Reread_the_current_file");
labels1[1]=Messager.getString("Open_a_file");
labels1[2]=Messager.getString("Save");
labels1[3]=Messager.getString("Save_as");
labels1[4]=Messager.getString("Exit");

String s2=Messager.getString("Edit");
String[] labels2=new String[nbmenus2];

String s3=Messager.getString("Help");
String[] labels3=new String[nbmenus3];

JMenuBar barre=new JMenuBar();
//setJMenuBar(barre);
JMenu m1=new JMenu(s1);
JMenu m2=new JMenu(s2);
JMenu m3=new JMenu(s3);
barre.add(m1);


for (int i=0;i<nbmenus1;i++) menus1[i]=new JMenuItem(labels1[i]);
for (int i=0;i<nbmenus2;i++) menus2[i]=new JMenuItem(labels2[i]);
for (int i=0;i<nbmenus3;i++) menus3[i]=new JMenuItem(labels3[i]);

for (int i=0;i<nbmenus1;i++) menus1[i].addActionListener(this);
for (int i=0;i<nbmenus2;i++) menus2[i].addActionListener(this);
for (int i=0;i<nbmenus3;i++) menus3[i].addActionListener(this);

for (int i=0;i<nbmenus1;i++) m1.add(menus1[i]);
for (int i=0;i<nbmenus2;i++) m2.add(menus2[i]);
for (int i=0;i<nbmenus3;i++) m3.add(menus3[i]);

//key shortcuts:
menus1[2].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
menus1[4].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));


txtArea.setFont(Global.font);

JPanel topPanel=new JPanel();
topPanel.setLayout(new BorderLayout());
this.add(topPanel);


//JPanel jp=new JPanel();
//jp.setLayout(new BorderLayout());
//JScrollPane jscrollpane = new JScrollPane(txtArea);
//jp.add(jscrollpane,BorderLayout.CENTER);

//panel to paste the raw data:
JScrollPane jsp=new JScrollPane(txtArea);
//JPanel jp=new JPanel();
//jp.setLayout(new BorderLayout());
//jp.add(jsp,BorderLayout.CENTER);

topPanel.add(jsp,BorderLayout.CENTER);

// show the main window
//pack();
setVisible(true);
validate();
} 




/**return a String containing what is in the text editor*/
public String getText()
{
return txtArea.getText();
}

/**set a new String in the text editor*/
public void setText(String s)
{
txtArea.setText(s);
txtArea.repaint();
}

/**append a new String in the text editor*/
public void appendText(String s)
{
txtArea.append(s);
if (txtArea.getText().length()>10000) setText("");
}


/**
save the text in a text file
*/
public void save(String filename)
{
TextFiles.saveString(filename,txtArea.getText()); 
}


public void actionPerformed(ActionEvent e)
	{

	Object source = e.getSource();


	if (source==menus1[1]) //open a new file
		{
		FileDialog df=new FileDialog(this,"Open",FileDialog.LOAD);
		df.setDirectory(Global.path);
		df.setVisible(true);
		if (df.getFile()==null) return;
		String fileName=df.getDirectory()+df.getFile();
		txtArea.setText(TextFiles.readFile(fileName).toString());

		setTitle(fileName);
		}

	if (source==menus1[2])
		{
		if (fileName!=null) TextFiles.saveString(fileName,txtArea.getText());
		}
	if (source==menus1[3])
		{
		FileDialog df=new FileDialog(this,"Save as",FileDialog.SAVE);
		df.setDirectory(Global.path);
		df.setVisible(true);
		if (df.getFile()!=null) 
			{
			String fileName=df.getDirectory()+df.getFile();
			setTitle(Messager.getString("TextEditor")+" :"+fileName);
			TextFiles.saveString(fileName,txtArea.getText());
			}
		}
	if (source==menus1[4])
		{
		dispose();
		}

	}
public void windowClosed(WindowEvent event) 
{
}
public void windowDeiconified(WindowEvent event) 
{
}
public void windowIconified(WindowEvent event) 
{
}
public void windowActivated(WindowEvent event) 
{
}
public void windowDeactivated(WindowEvent event) 
{
}
public void windowOpened(WindowEvent event) 
{
}
public void windowClosing(WindowEvent event) 
{
dispose();
}

//
//
//public String getTitle() {
//	return title;
//}
//
//
//
//
//public void setTitle(String title) {
//	this.title = title;
//}





}
