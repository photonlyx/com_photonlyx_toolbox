package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.photonlyx.toolbox.mysql.MysqlRow;
import com.photonlyx.toolbox.param.ParDouble;
import com.photonlyx.toolbox.param.ParInt;
import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;




/**
permits to show and edit a param (name,value,unit) and trig objects when value changed

 */
public class ParDoubleOptionsBox  extends ParamBox implements  ActionListener
{
	private Color valbackground=Color.lightGray;
	private boolean editable=true;

	private TriggerList triggerList;//list of object to update when this parameter is changed
	private ParDouble p;
	private JComboBox<String> combo;
	private JLabel name,labelUnit;
	private double[] options;

	private JPanel jp=new JPanel();

	/**
	 * 
	 * @param tl can be null
	 * @param p
	 */
	public ParDoubleOptionsBox(TriggerList tl,ParDouble p,double[] options,int w,int h,String label,String unit)
	{
		this.options=options;
		triggerList=tl;	
		this.p=p;
		this.setW(w);
		this.setH(h);

		jp.removeAll();

		//int w=intVal("w",150);
		//int h=intVal("h",30);
		jp.setSize(getW(),getH());
		jp.setPreferredSize(new Dimension(getW(),getH()));
		jp.setBackground(this.getBackground());

		name=new JLabel();
		name.setFont(this.getFont());
		name.setBackground(this.getBackground());
		if (label!=null) name.setText(label);else name.setText(p.getParamName());
		jp.add(name);


		combo=new JComboBox<String>();

		jp.add(combo);
		//val.setOpaque(true);
		//val.setEditable(editable);
		combo.setFont(this.getFont());
		combo.setBackground(this.getBackground());
		combo.addActionListener(this);

		if (unit!=null) 
		{
			labelUnit=new JLabel("");
			labelUnit.setFont(this.getFont());
			labelUnit.setBackground(this.getBackground());
			jp.add(labelUnit);
			labelUnit.setText(unit);
		}

		String save=p.stringVal();
		for (double s:options) combo.addItem(""+s);
		p.setVal(save);
		synchronizeToParam();


		combo.doLayout();
		update();
	}




	public void setSize(int w, int h)
	{
		super.setW(w);
		super.setH(h);
		//init();
		update();
	}



	public void setForeground(String color) 
	{
		Color foreground=ColorUtil.getColor(color);
		jp.setForeground(foreground);
		name.setForeground(foreground);
		combo.setForeground(foreground);
		labelUnit.setForeground(foreground);
	}

	public void setBackground(String color) 
	{
		Color background=ColorUtil.getColor(color);
		jp.setBackground(background);
		name.setBackground(background);
		combo.setBackground(background);
		labelUnit.setBackground(background);
	}

	public void setValBackground(String color) 
	{
		Color background=ColorUtil.getColor(color);
		combo.setBackground(background);
	}

	public TriggerList getTriggerList() {
		return triggerList;
	}


	public void setTriggerList(TriggerList triggerList) {
		this.triggerList = triggerList;
	}




	public boolean isEditable() {
		return editable;
	}


	public void setEditable(boolean editable) {
		this.editable = editable;
		//init();
	}







	public void update()
	{
		synchronizeToParam();
	}



	public Component getComponent(){return jp;}

	public String getParamName()
	{
		return p.getParamName();
	}

	public void setEnabled(boolean b)
	{
		combo.setEnabled(b);
		name.setEnabled(b);
		labelUnit.setEnabled(b);
	}


	public void addItem(String string)
	{
		String save=p.stringVal();//seems that add an item change the param value !....;
		combo.addItem(string);
		p.setVal(save);
		update();
	}

	public void removeAllItems()
	{
		combo.removeAllItems();
		update();
	}


	//select the item corresponding to the param having the selected value
	public void synchronizeToParam()
	{
		//System.out.println("param value:"+((ParDouble)p).val());
		double pval=((ParDouble)p).val();
		for (int i=0;i<combo.getItemCount();i++) 
		{
			Object o=combo.getItemAt(i);
			String s=(String)o;
			//System.out.println(i+" check:"+s);
			if (p.stringVal()!=null)
				if (new Double(s)==pval) 
				{
					//System.out.println(s+" match with param:"+p.stringVal());
					combo.setSelectedItem(o);
				}
		}
	}


	public void actionPerformed(ActionEvent e)
	{
		// System.out.println(getClass()+" "+"actionPerformed");
		JComboBox<String> cb = (JComboBox<String>)e.getSource();
		Object selectedObject = cb.getSelectedItem();

		if (selectedObject instanceof String)
		{
			String s = (String)selectedObject;
			p.setVal(s);
			//			//if the object that have this parameter is a MysqlLine, update the database:
			//			if (p.getParamOwner() instanceof MysqlLine) 
			//			{
			//				MysqlLine l=(MysqlLine)p.getParamOwner();
			//				l.getTable().updateSQL(l,p.getParamName());
			//			}

			if (triggerList!=null) triggerList.trig();
			//System.out.println("ComboBox trig, object selected="+s);
		}
		//System.out.println("ComboBox trig");
	}

	public Param getParam(){return p;}

	public void setFieldLength(int fieldLength) {

	}

}