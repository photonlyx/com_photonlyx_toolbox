package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;


public class InputDialog
{
protected Component Parent;
protected String title;
protected String fieldLabel;
protected String[] labels;
protected String labelsBorderText;
protected String inputBorderText;
protected Icon icon;
protected String initialFieldValue = null;
protected int action;

public final static int QUESTION_ICON = 1;
public final static int WARNING_ICON = 2;
public final static int INFORMATION_ICON = 3;
public final static int ERROR_ICON = 4;

public final static int OK_ACTION = 1;
public final static int CANCEL_ACTION = 2;

private boolean isHiddenPassword=false;


protected String stringAnswer;

public InputDialog()
{
	
}


public InputDialog setParent( Component Parent )
	{
	this.Parent = Parent;
	return this;
	}

public InputDialog setFieldLabel( String fieldLabel )
	{
	this.fieldLabel = fieldLabel;
	return this;
	}

public InputDialog setLabels( String[] labels )
	{
	this.labels = labels;
	return this;
	}

public InputDialog setTitle( String title )
	{
	this.title = title;
	return this;
	}

public InputDialog setLabelsBorderText( String labelsBorderText )
	{
	this.labelsBorderText = labelsBorderText;
	return this;
	}

public InputDialog setInputBorderText( String inputBorderText )
	{
	this.inputBorderText = inputBorderText;
	return this;
	}


/**
 * 
 * @param iconDef QUESTION_ICON WARNING_ICON  INFORMATION_ICON   ERROR_ICON
 * @return
 */
public InputDialog setIcon (int iconDef)
	{
	icon = getIcon(iconDef);
	
	return this;
	}

public static ImageIcon getIcon ( int iconDef)
	{
	switch (iconDef)
		{
		case QUESTION_ICON:
			return new ImageIcon(Util.getImage("com/photonlyx/toolbox/gui/icons/Question.png"));
		case WARNING_ICON:
			return new ImageIcon(Util.getImage("com/photonlyx/toolbox/gui/icons/Warn.png"));
		case INFORMATION_ICON:
			return new ImageIcon(Util.getImage("com/photonlyx/toolbox/gui/icons/Inform.png"));
		case ERROR_ICON:
			return new ImageIcon(Util.getImage("com/photonlyx/toolbox/gui/icons/Error.png"));
		default :
			return null;
		}
	}

public InputDialog setInitialFieldValue(String initialFieldValue) 
	{
	this.initialFieldValue = initialFieldValue;

	return this;
	}

public String getInitialFieldValue() {return initialFieldValue;}



public void setIsHiddenPassword(boolean b)
{
isHiddenPassword=b;
}


public void show()
	{
	// Create a modal JDialog. The owner can be a Dialog or a Frame (JoloWindow)
	//
	final JDialog dialog;
//	if (Parent instanceof Dialog || Parent instanceof Frame)
//		{
//		if (Parent instanceof Dialog)
//			{
//			dialog = new JDialog((Dialog)Parent);
//			}
//		else
//			{
//			dialog = new JDialog((Frame)Parent);
//			}
//		}
//	else
//		{
//		Messager.messErr("Le Parent n'est ni un Dialog, ni une Frame!");
//		stringAnswer = null;
//		return;
//		}
	
	dialog=new JDialog();

	dialog.setLayout(new GridBagLayout());
	dialog.setTitle(title);
	dialog.setFont(Global.font);
	dialog.setModal(true);

	// Create the icon
	//
	if (icon != null)
		{
		JLabel iconLabel = new JLabel();
		iconLabel.setIcon(icon);

		dialog.add(iconLabel,new Grider(0,0)
				.setGridHeight(2)
				.setWeight(0,1)
				.setAnchor(Grider.NORTH)
				.setInsets(10,10,10,0));
		}
	
	// Add all the text lines contained in the labels String array
	//
	if (labels != null)
		{
		// Create a panel to hold all the text
		//
		JPanel labelPanel = new JPanel();

		if (labelsBorderText != null)
			{
			TitledBorder titleBorder = new TitledBorder(labelsBorderText);
			labelPanel.setBorder(titleBorder);
			}
			
		dialog.add(labelPanel, new Grider(1,0)
				.setGridWidth(2)
				.setFill(Grider.HORIZONTAL)
				.setWeight(1,0)
				.setInsets(15,15,0,5));
				
		labelPanel.setLayout(new GridBagLayout());

		for (int i=0; i < labels.length ; i++)
			{
			JLabel label = new JLabel(labels[i]);
			label.setFont(Global.font);
			labelPanel.add(label, new Grider(0,i)
					.setWeight(1,0)
					.setAnchor(Grider.NORTHWEST));
			}
		}

	// Create a panel to hold the input field
	//
	JPanel fieldPanel = new JPanel();
	fieldPanel.setLayout(new GridBagLayout());

	if (inputBorderText != null)
		{
		TitledBorder titleBorder = new TitledBorder(inputBorderText);
		fieldPanel.setBorder(titleBorder);
		}
		
	dialog.add (fieldPanel, new Grider(1,1)
			.setGridWidth	(2)
			.setWeight		(1,1)
			.setAnchor		(Grider.NORTH)
			.setFill		(Grider.HORIZONTAL)
			.setInsets		(10,15,10,5));
				
	// Eventualy add a description before the input field
	//
	if (fieldLabel != null && !fieldLabel.equals(""))
		{
		JLabel fieldLabelW = new JLabel(fieldLabel);
		fieldLabelW.setFont(Global.font);
		fieldPanel.add(fieldLabelW, new Grider(0,0).setAnchor(Grider.NORTH).setInsets(0,0,0,5));
		}
	
	// create the input field
	//
	final JTextField field ;
	if (isHiddenPassword) 
		{
		field= new JPasswordField(); 
		((JPasswordField)field).setEchoChar('*');
		}
	else field= new JTextField();
	

	field.setFont(Global.font);

	if (initialFieldValue != null)
		{
		field.setText(initialFieldValue);
		}

	fieldPanel.add(field, new Grider(1,0)
			.setWeight(1,0)
			.setFill(Grider.HORIZONTAL));

	// Create the Ok button
	//
	final JButton okButton = new JButton(Messager.getString("OK"));
	okButton.setFont(Global.font);

	dialog.add(okButton, new Grider(1,2)
			.setGridWidth(-2)
			.setWeight(1,0)
			.setAnchor(Grider.EAST)
			.setInsets(0,5,5,5));

	int okW = okButton.getPreferredSize().width;
	if (okW < 80)
		{
		okW = 80;
		}
	okButton.setPreferredSize(new Dimension(okW,okButton.getPreferredSize().height));

	// Create the Cancel button
	//
	final JButton cancelButton = new JButton(Messager.getString("Cancel"));
	cancelButton.setFont(Global.font);

	dialog.add(cancelButton, new Grider(2,2).setInsets(0,0,5,5));

	int cancelW = cancelButton.getPreferredSize().width;
	if (cancelW < 80)
		{
		cancelW = 80;
		}
	cancelButton.setPreferredSize(new Dimension(cancelW,cancelButton.getPreferredSize().height));

	dialog.pack();

	// Set the minimum width to 300 and center the dialog on the screen
	//
	Dimension d=dialog.getToolkit().getScreenSize();
	Dimension dialogDimension = dialog.getPreferredSize();
	int w = dialogDimension.width;
	int h = dialogDimension.height;
	
	if (w < 300)
		{
		w = 300;
		dialog.setPreferredSize(new Dimension(w,h));
		dialog.pack();
		}
	
	dialog.setLocation(d.width/2-w/2,d.height/2-h/2);
	
	// Define the actions performed by the buttons
	//
	class ButtonListener implements ActionListener
		{
		
		//public int action = CANCEL_ACTION;
		
		public void actionPerformed(ActionEvent e)
			{
			if (e.getSource() == cancelButton)
				{
				action = CANCEL_ACTION;
				dialog.dispose();
				}
			else if (e.getSource() == okButton)
				{
				action = OK_ACTION;
				dialog.dispose();
				}
			}
		}
	
	ButtonListener buttonListener = new ButtonListener();
	okButton.addActionListener(buttonListener);
	cancelButton.addActionListener(buttonListener);
	
	// Define the OK button has the default button so that when ENTER is pressed
	// the OK action is performed
	//
	dialog.getRootPane().setDefaultButton(okButton);

	// When the ESC key is pressed, the dialog is closed
	//
	JRootPane rootPane = dialog.getRootPane();
	InputMap iMap = rootPane.getInputMap(                                            
		JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
	iMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escape");

	ActionMap aMap = rootPane.getActionMap();
	aMap.put("escape", new AbstractAction()
        {
			public void actionPerformed(ActionEvent e)
				{
				dialog.dispose();
				}
        }); 

	
	// show the dialog
	//
	dialog.setVisible(true);
	
	if (action == CANCEL_ACTION)
		{
		stringAnswer = null;
		}
	else
		{
		stringAnswer = field.getText();
		}
	}

public String getStringAnswer()
	{
	return stringAnswer;
	}

public int getAction()
	{
	return action;
	}

}
