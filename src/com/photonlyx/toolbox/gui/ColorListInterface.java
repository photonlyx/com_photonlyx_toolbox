
// Author Laurent Brunel


package com.photonlyx.toolbox.gui;

import java.awt.Color;



/**
provide a configurable list of colours
*/
public interface   ColorListInterface 
{

/**
return the color number i modulo the nb of colors
@param i the color index
*/
public Color javaColor(int i);



/**
get a new color (different than last one)
*/
public Color getNewJavaColor();



/**
set the number of colors that will be used
*/
public void setColorNumber(int n);

public String getNextColour();
public String getPreviousColour();
}
