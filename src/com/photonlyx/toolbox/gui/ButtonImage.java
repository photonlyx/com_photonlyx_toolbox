package com.photonlyx.toolbox.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

public class ButtonImage extends JPanel implements MouseListener
{
	private TriggerList tl;
	private BufferedImage im,imNormal,imDisable;	
	private boolean disable=false;
	private String tip="";
	private TipWindow tw=null;

	public ButtonImage(TriggerList tl,String imageFileInResources)
	{
		this.tl=tl;

		this.setBackground(Global.background);
		this.setPreferredSize(new Dimension(20,20));

		//im=new CImage(com.photonlyx.toolbox.util.Util.getImage(imageFileInResources,true));
		Image im0=com.photonlyx.toolbox.util.Util.getImage(imageFileInResources,true);
		CImageProcessing cim=new CImageProcessing(new CImage(im0));
		imNormal=cim.getBufferedImage();

		//disable look:
		CImageProcessing cimpDisable=cim.getAcopy();
		cimpDisable._desaturate();
		imDisable=cimpDisable.getBufferedImage();

		this.setDisable(false);

		//take the same size of the image:
		this.setPreferredSize(new Dimension(im.getWidth(),im.getHeight()));

		this.addMouseListener(this);

	}


	public void  paintComponent(Graphics g)
	{
		Graphics2D g2=(Graphics2D)g;
		//setPreferredSize(new Dimension(cim.w(),cim.h()));
		g2.drawImage(im, 0, 0, this);
	}


	public void setTip(String tip)
	{
		this.tip=tip;
	}



	@Override
	public void mouseClicked(MouseEvent arg0) 
	{
		if (arg0.getButton()!=MouseEvent.BUTTON1) return;
		if (tw!=null) tw.setVisible(false);		
		if (!disable) tl.trig();
	}

	@Override
	public void mouseEntered(MouseEvent e) 
	{
		Point p=this.getLocationOnScreen();
		p.translate(20, -im.getHeight());
	//	if (tw==null) tw=new TipWindow(this,tip);else tw.showMe();
	}

	@Override
	public void mouseExited(MouseEvent arg0) 
	{
		if (tw!=null) tw.setVisible(false);
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	public void setDisable(boolean b)
	{
		disable=b;
		if (disable)	im=imDisable; else im=imNormal; 
		repaint();
	}
	
	
	public void setEnable(boolean b)
	{
		disable=!b;
		if (disable)	im=imDisable; else im=imNormal; 
		repaint();
	}
	

	
	
	
	
	
	
	
	
	
}
