package com.photonlyx.toolbox.gui;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.photonlyx.toolbox.gui.menu.CJMenuFile;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.CFileManager;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Util;


/**template of application*/
public class WindowApp 
{	
	//private CFileManager cFileManagerDefault=new XMLFileStorage();//default file manager to store the data (XML)
	private CFileManager _cFileManager;//any file manager to store the data 
	//private CFileFilter cFileFilterDefault=new CFileFilter("xml","XML data");
	private CFileFilter _cFileFilter;//any file filter
	private CJMenuFile cJMenuFile;
	private FileIconsPanel fileIconsPanel;
	protected JTabbedPane appTabPane;
	private CJFrame jframe;
	protected TriggerList fileMenuTriggerList=new TriggerList();
	private JPanel topPanel=new JPanel();
	private JPanel panelSide;
	private JPanel toolBar;
	private int nbPanes=0;
	private String panel0Label;
	public String path="./";
	public String appHome="./";
	public String appName;

	
	public WindowApp()
	{
		init("app",false,false,false);
	}
	

	public WindowApp(String appName)
	{
		init(appName,false,false,false);
	}
	
	

	public WindowApp(String appName,boolean putPanelSide,
			boolean useStatusBar,boolean putToolsBar)
	{
		init(appName,putPanelSide,useStatusBar,putToolsBar);
	}




	private void init(String appName,boolean putPanelSide,
			boolean useStatusBar,boolean putToolsBar)
	{
		this.appName=appName;
		Global.appHome=Util.getOrCreateApplicationUserFolder(appName);
		//create frame and add the chart and and legend in a border layout:
		jframe=new CJFrame(this,useStatusBar);
		jframe.setSize(900, 600);
		jframe.setTitle(appName);

		topPanel.setLayout(new BorderLayout());
		topPanel.setBackground(Global.background);
		jframe.add(topPanel);

		jframe.updateWorkingDir(appName);

		if (putPanelSide) 
		{
			//panel for button and legend
			panelSide=new JPanel();
			panelSide.setPreferredSize(new Dimension(150, 200));
			panelSide.setLayout(new FlowLayout());
			panelSide.setBackground(Global.background);
			//add the side panel at right
			jframe.getContentPane().add(panelSide,BorderLayout.WEST);
		}

		if (putToolsBar) //tool bar to put actions icons
		{
			toolBar=new JPanel();
			toolBar.setPreferredSize(new Dimension(150, 40));
			toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
			toolBar.setBackground(Global.background);
			//add the toolbar panel at north
			jframe.getContentPane().add(toolBar,BorderLayout.NORTH);
		}


		jframe.setVisible(true);

	}

	public void putDefaultMenuFile()
	{
		putMenuFile(new XMLFileStorage(this),new CFileFilter("xml","XML chart data"));
	}




	public void putMenuFile(CFileManager cFileManager,CFileFilter cFileFilter)
	{
		this._cFileManager=cFileManager;
		this._cFileFilter=cFileFilter;

		cJMenuFile=new CJMenuFile(cFileManager,cFileFilter,fileMenuTriggerList,jframe);
		cJMenuFile.getJMenu().setText("File");

		JMenuBar bar=new JMenuBar();
		bar.setBackground(Global.background);
		jframe.setJMenuBar(bar);	
		bar.add(cJMenuFile.getJMenu());

		if (toolBar!=null)
		{
			fileIconsPanel =new FileIconsPanel(cFileManager,cFileFilter,fileMenuTriggerList,jframe);
			toolBar.add(fileIconsPanel.getComponent(),BorderLayout.NORTH);
		}

	}



	/*
private void setMenuFile(CFileFilter cFileFilter)
{
CJMenuFile cJMenuFile=new CJMenuFile(xMLFileStorage,cFileFilter,fileMenuTriggerList,jframe);
cJMenuFile.getJMenu().setText("File");

JMenuBar bar=new JMenuBar();
bar.setBackground(Global.background);
jframe.setJMenuBar(bar);	
bar.add(cJMenuFile.getJMenu());
}

	 */


	/**
	 * get the cFileManager which is a XMLFileStorage by default
	 * @return
	 */
	public XMLFileStorage getxMLFileStorage() 
	{
		return (XMLFileStorage) _cFileManager;
	}

	public CFileManager getCFileManager()
	{
		return _cFileManager;
	}

	//public void setCFileManager(CFileManager cfm)
	//{
	//this._cFileManager=cfm;
	//}
	//
	//public void setCFileFilter(CFileFilter cff)
	//{
	//this._cFileFilter=cff;
	//}

	public CFileFilter setCFileFilter()
	{
		return _cFileFilter;
	}

	public CFileFilter get_cFileFilter() {
		return _cFileFilter;
	}


	public TriggerList getFileMenuTriggerList()
	{
		return fileMenuTriggerList;
	}

	public CJFrame getCJFrame() {
		return jframe;
	}


	public JPanel getPanelSide()
	{
		return panelSide;
	}

	public JPanel getToolBar()
	{
		return toolBar;
	}



	public FileIconsPanel getFileIconsPanel() {
		return fileIconsPanel;
	}

	/**
	 * add a panel to the tabbed pane
	 * @param panel
	 * @param label
	 */
	public void add(JPanel panel,String label) 
	{
		nbPanes++;
		if (nbPanes==2)
		{
			Component panel0=topPanel.getComponent(0);
			topPanel.remove(panel0);
			appTabPane=new JTabbedPane();
			appTabPane.setBackground(Global.background);
			topPanel.add(appTabPane,BorderLayout.CENTER);
			appTabPane.add(panel0,panel0Label);
			appTabPane.add(panel,label);
			//panel0.setBackground(Global.background);
			//panel.setBackground(Global.background);
		}
		else if (nbPanes>2)
		{
			appTabPane.add(panel,label);
		}
		else 
		{
			//panel.setBackground(Global.background);
			topPanel.add(panel,BorderLayout.CENTER);
			panel0Label=label;
		}
		this.getCJFrame().validate();
		this.getCJFrame().repaint();
	}


	public void removePanels()
	{
		if (nbPanes==0) return;
		else if (nbPanes==1) 		
		{
			Component panel0=topPanel.getComponent(0);
			topPanel.remove(panel0);
		}
		else if (nbPanes==2) 		
		{
			appTabPane.removeAll();		
			topPanel.removeAll();
			appTabPane=null;
		}
		nbPanes=0;
	}

	public void addIconToolBar(JPanel toolbar)
	{
		topPanel.add(toolbar,BorderLayout.NORTH);
	}

	public void setMessage(String s)
	{
		jframe.setMessage(s);
	}



	public void update()
	{
		jframe.validate();

	}

	public void hidePanelSide()
	{
		jframe.getContentPane().remove(panelSide);
		jframe.validate();
	}

	public void showPanelSide()
	{
		jframe.getContentPane().add(panelSide,BorderLayout.WEST);
		jframe.validate();
	}

	public void selectPane(Component c)
	{
		appTabPane.setSelectedComponent(c);
	}

	public void addSideParam(Object o,String[] paramNames)
	{
		ParamsBox pb=new ParamsBox(o,null,null,paramNames,null,null,ParamsBox.VERTICAL,200,28);
		this.getPanelSide().add(pb.getComponent());
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
