package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import com.photonlyx.toolbox.mysql.MysqlRow;
import com.photonlyx.toolbox.param.ParDouble;
import com.photonlyx.toolbox.param.ParInt;
import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;




/**
permits to show and edit a string param (name,value) and trig objects when value changed

*/
public class ParStringTextArea   implements DocumentListener,KeyListener,MouseWheelListener
{
private String label="";
private Color valbackground=Color.lightGray;
private boolean editable=true;
	
private TriggerList triggerList;//list of object to update when this parameter is changed
private ParString p;
private JTextArea val;
private JLabel name;



private JPanel jp=new JPanel();

/**
 * 
 * @param tl can be null
 * @param p
 */
public ParStringTextArea(Object paramOwner,String paramName,String paramLabel,
		TriggerList tl)
{
triggerList=tl;	
p=new ParString(paramOwner,paramName);
if (paramLabel==null) this.setLabel(paramName);else this.setLabel(paramLabel);
init(600,400);
}

public ParStringTextArea(Object paramOwner,String paramName,String paramLabel,
		TriggerList tl,int w,int h)
{
triggerList=tl;	
p=new ParString(paramOwner,paramName);
if (paramLabel==null) this.setLabel(paramName);else this.setLabel(paramLabel);
init(w,h);
}

public void init(int w,int h)
{
jp.removeAll();
jp.setBackground(Global.background);
jp.setPreferredSize(new Dimension(w,h));

name=new JLabel();
name.setFont(Global.font);
name.setBackground(Global.background);
jp.add(name);

val=new JTextArea();
jp.add(val);
val.setBackground(Global.background);
val.setFont(Global.font);
//panel.add(jta);
JPanel jptxt=new JPanel();
jptxt.setLayout(new BorderLayout());
JScrollPane jscrollpane = new JScrollPane(val);
jptxt.setPreferredSize(new Dimension(w-30,h-30));
jptxt.add(jscrollpane);
jp.add(jptxt);




val.getDocument().addDocumentListener(this);
val.addKeyListener(this);
val.addMouseWheelListener(this);
val.setOpaque(true);
val.setEditable(editable);
val.setFont(Global.font);
val.setBackground(Global.background);

val.doLayout();
update();
}





public void setLabel(String label) {
	this.label = label;
}




public boolean isEditable() {
	return editable;
}


public void setEditable(boolean editable) {
	this.editable = editable;
	//init();
}





public void update()
{
name.setText(label);
val.setText(p.stringVal());
}




//***********interface DocumentListener ***************

//Gives notification that an attribute or set of attributes changed.
public  void changedUpdate(DocumentEvent e)
{
//System.out.println(getClass()+" val box changed ");
actualiseParamFromBox();
//System.out.println(getClass()+" has changed:"+p.hasChanged());
}

//Gives notification that there was an insert into the document.
public   void insertUpdate(DocumentEvent e)
{
//System.out.println(getClass()+" inserted ");
actualiseParamFromBox();
}
public   void removeUpdate(DocumentEvent e)
{
//System.out.println(getClass()+" remove ");
actualiseParamFromBox();
}
//***********end interface DocumentListener ***************


//**********interface keyListener**********************
public void keyPressed(KeyEvent e)
{
switch (e.getKeyCode())
	{
	case KeyEvent.VK_ENTER :
// 	System.out.println(getClass()+" VK_ENTER ");
	//if (triggerList!=null) triggerList.trig();
	//launchTheNetwork();
	jp.repaint();
	break;
	}

	
}

public void keyTyped(KeyEvent e){}
public void keyReleased(KeyEvent e){}

//**********end interface keyListener**********************



public void mouseWheelMoved(MouseWheelEvent e)
{


}

private void actualiseParamFromBox()
{
if (p==null) return;
//System.out.println(getClass()+" actualiseParamFromBox : "+val.getText());
if (p instanceof ParString) 
	{
	String s1=val.getText();
	p.setVal(s1);
//	//if the object that have this parameter is a MysqlLine, update the database:
//	if (p.getParamOwner() instanceof MysqlLine) 
//		{
//		MysqlLine l=(MysqlLine)p.getParamOwner();
//		l.getTable().updateSQL(l,p.getParamName());
//		}

	}
else 	
	{
	//System.out.println(getClass()+" actualiseParamFromBox : not ParString ");
	if (!p.setVal(val.getText())) val.setBackground(Color.red);
	else val.setBackground(valbackground);
	}
//if (triggerList!=null) if (triggerEnabled)triggerList.trig();
}



public Component getComponent(){return jp;}

public String getParamName()
{
return p.getParamName();
}

public void setEnabled(boolean b)
{
val.setEnabled(b);
name.setEnabled(b);
}

public Param getParam(){return p;}

}