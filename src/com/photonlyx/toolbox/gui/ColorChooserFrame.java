package com.photonlyx.toolbox.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class ColorChooserFrame 
{
private ColorApplier ca;
private ColorChooser cc=new ColorChooser();
private JFrame jf;
private JPanel standardColors=new JPanel();
//private Vector

public  ColorChooserFrame()
{
jf=new JFrame();
jf.setLocation(new Point(50,200));
jf.setSize(new Dimension(200,200));

//continuous color chooser:
jf.getContentPane().setLayout(new BorderLayout());
jf.add(cc.getComponent(),BorderLayout.CENTER);

//standard colors
standardColors.setSize(new Dimension(200,100));
standardColors.setLayout(new GridLayout(1,0));
ColorList cl=new ColorList();
for (int i=0;i<cl.nbColors();i++)
	{
	StandardColorPanel jp=new StandardColorPanel(cl.javaColor(i));
	standardColors.add(jp);
	}
jf.add(standardColors,BorderLayout.NORTH);


jf.setVisible(false);
}

public void setColorApplier(ColorApplier ca)
{
this.ca=ca;
cc.setColorApplier(ca);
}



public void setVisible(boolean b)
{
jf.setVisible(b);	
}

public boolean isVisible() {
	return jf.isVisible();
}



class StandardColorPanel extends JPanel implements MouseListener
{
	
public StandardColorPanel(Color background)
{
this.setSize(new Dimension(200,100));
this.setBackground(background);	
this.addMouseListener(this);
}

public void mouseClicked(MouseEvent arg0)
	{
	ca.applyColor(ColorUtil.color2String(this.getBackground()));
	}

public void mouseEntered(MouseEvent arg0) 
	{
		
		
	}

public void mouseExited(MouseEvent arg0) {
		
		
	}

public void mousePressed(MouseEvent arg0) {
	
		
	}

public void mouseReleased(MouseEvent arg0) {
		
		
	}
	
}







}