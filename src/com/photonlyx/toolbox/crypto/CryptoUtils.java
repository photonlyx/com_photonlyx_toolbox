package com.photonlyx.toolbox.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
 
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
 


/**
 * A utility class that encrypts or decrypts a file.
 * @author www.codejava.net
 *
 */
public class CryptoUtils {
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";
 
    public static void encrypt(String key, File inputFile, File outputFile)
            throws CryptoException {
        doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
    }
 
    public static void decrypt(String key, File inputFile, File outputFile)
            throws CryptoException {
        doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);
    }
    
    
    public static byte[]  encrypt(String key,  byte[] inputBytes)
            throws CryptoException {
        return doCrypto(Cipher.ENCRYPT_MODE, key, inputBytes);
    }
 
    public static byte[] decrypt(String key, byte[] inputBytes)
            throws CryptoException {
    	return doCrypto(Cipher.DECRYPT_MODE, key,inputBytes);
    }
    
   private static byte[] doCrypto(int cipherMode, String key, byte[] inputBytes) 
    		throws CryptoException {
    try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);
             
            byte[] outputBytes = cipher.doFinal(inputBytes);
             
         return   outputBytes;
         
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException  ex) {
            throw (new CryptoException("Error encrypting/decrypting file", ex));
        }
    }

 
    private static void doCrypto(int cipherMode, String key, File inputFile,
            File outputFile) throws CryptoException {
    try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);
             
            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);
             
            byte[] outputBytes = cipher.doFinal(inputBytes);
             
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);
             
            inputStream.close();
            outputStream.close();
             
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException ex) {
            throw (new CryptoException("Error encrypting/decrypting file", ex));
        }
    }

    
    
public static String decryptFromFile(String fullfile,String key)
    {
    File encryptedFile = new File(fullfile);
    String s=null;
    try {
        FileInputStream inputStream = new FileInputStream(encryptedFile);
        byte[] inputBytes = new byte[(int) encryptedFile.length()];
        inputStream.read(inputBytes);
        byte[] outputBytes =  CryptoUtils.decrypt(key,inputBytes);
        s=new String(outputBytes);
        inputStream.close();
        
    } catch (Exception ex) {
        System.out.println(ex.getMessage());
        ex.printStackTrace();
        Messager.messErr("Not decrypted !");
        System.out.println("Not decrypted !");
        return null;
    }
    System.out.println(fullfile+" decrypted");
    return s;
    }   
    

    
public static void encrypt2file( String key, String s_toencrypt,String fullfile) 
{
try {
         
	
     byte[] outputBytes =  CryptoUtils.encrypt(key,s_toencrypt.getBytes());
        FileOutputStream outputStream = new FileOutputStream(new File(fullfile));
        outputStream.write(outputBytes);
        outputStream.close();
         
    } catch (Exception ex) {
        System.out.println(ex.getMessage());
        ex.printStackTrace();
        Messager.messErr("Not encrypted !");
      
    }
}

     
    
    

public static class CryptoException extends Exception {

public CryptoException() {
}

public CryptoException(String message, Throwable throwable) {
    super(message, throwable);
}
}







public static class CryptoUtilsTest {
public static void main(String[] args) {
    String key = "Mary has one cat";
    File inputFile = new File("document.txt");
    File encryptedFile = new File("document.encrypted");
    File decryptedFile = new File("document.decrypted");
     
    try {
        CryptoUtils.encrypt(key, inputFile, encryptedFile);
        CryptoUtils.decrypt(key, encryptedFile, decryptedFile);
    } catch (CryptoException ex) {
        System.out.println(ex.getMessage());
        ex.printStackTrace();
    }
}
}






}
