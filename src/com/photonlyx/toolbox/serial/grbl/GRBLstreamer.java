package com.photonlyx.toolbox.serial.grbl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Date;
import java.util.LinkedList;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.photonlyx.toolbox.serial.LineReceivedListener;
import com.photonlyx.toolbox.thread.CTask2;
import com.photonlyx.toolbox.thread.TaskEndListener;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

public  class GRBLstreamer  implements CTask2
{
	private int bauds = 115200;
	public int[] baudsOptions = {9600,115200};
	private SerialPort port=null;
	private DeviceListener spl=new DeviceListener();
	private String identity;
	private Vector<LineReceivedListener> lineReceivedListeners= new Vector<LineReceivedListener>();
	private boolean stopped=false;
	private File file;
 	static int GRBL_BUFFER_SIZE = 125;
 	private BlockingQueue<Integer> completedCommands = new LinkedBlockingQueue<Integer>();
 	private LinkedList<Integer>  activeCommandSizes = new LinkedList<Integer>();
 	private int currentBufferSize;
	private TaskEndListener taskEndListener;
	
	
	public GRBLstreamer(File f)
	{
		file=f;
	}
	
	public void run()
	{		
		if (!isConnected()) 
			{
			System.out.println("Port not connected");
			connectOnePort();
			}
		try {Thread.sleep(500);} catch (Exception e) {};
		send(file);
		port.closePort();
		if (taskEndListener!=null) taskEndListener.whenTaskFinished();
	}

	/**
	 * connect the present port
	 */
	public void connectOnePort()
	{
		SerialPort[] comPorts = SerialPort.getCommPorts();
		for (SerialPort p:comPorts)
		{
			System.out.println("See port "+p.getSystemPortName());
		}
		if (comPorts.length>0)		
		{
			port=comPorts[0];
			port.setBaudRate(bauds);
			port.openPort();
			port.addDataListener(spl);
		}
		if (port!=null) System.out.println("Connected to port "+port.getSystemPortName());
		else System.out.println("Can't see any serial port");
	}


	public void connectPort(String identity)
	{
		this.identity=identity;
		SerialPort[] comPorts = SerialPort.getCommPorts();
		for (SerialPort p:comPorts)
		{
			p.setBaudRate(bauds);
			//https://github.com/Fazecast/jSerialComm/wiki/Modes-of-Operation 
			//p.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 100, 0);
			p.openPort();
			p.addDataListener(spl);
			port=p;
			try {Thread.sleep(2000);} catch (Exception e) {};

			//check if it is the expected instrument:
			System.out.println("Ask identity of  "+p.getSystemPortName());
			send("w\n");
			try {Thread.sleep(100);} catch (Exception e) {};
			//System.out.println(p.getSystemPortName()+" received : \""+spl.getLastReceivedLine()+"\"");
			if (spl.getLastReceivedLine().contains(identity)) 
			{
				//this is the correct port, leave
				break;
			}
			else 
			{
				//this is not the correct port, close and try other ports
				p.closePort();
				port=null;
			}			
		}
		if (port!=null) System.out.println("Device "+identity+" is connected to port "+port.getSystemPortName());
		else System.out.println("Can't see any serial port with identity "+identity);
	}

	public void addLineReceivedListener(LineReceivedListener lrl)
	{
		lineReceivedListeners.add(lrl);
	}

//	public void writeData(String s) 
//	{
//		send(s);
//	}

	public void send(String s) 
	{
		if (port==null) return;
		//System.out.println("Send: " + s);
		try 
		{
			byte[] bytes=s.getBytes();
			port.writeBytes(bytes,bytes.length);
		} catch (Exception e) 
		{
			System.out.println("could not write to port");
		}
	}


//
//	public void writeDataAndWaitForAknowledgment(String message) 
//	{
//		sendAndWaitForAknowledgment(message) ;
//	}


//
//	public void sendAndWaitForAknowledgment(String message) 
//	{
//		this.getDeviceListener().eraseLastReceivedLine();
//		try 
//		{
//			//System.out.println("SendAndWaitDone ->"+message);
//			send(message);		
//			//try{ Thread.sleep(100);} catch(Exception e){;}
//			for (;;)
//			{
//				String l=this.getDeviceListener().getLastReceivedLine();
//				//System.out.println("SendAndWaitDone ->lastReceivedLine="+l);
//				if (l.startsWith("done")) break;
//				try{ Thread.sleep(100);} catch(Exception e){;}
//			}
//
//		} 
//		catch (Exception e) 
//		{
//			System.out.println(identity+": could not write to port");
//		}
//	}
//


	public boolean isConnected()
	{
		if (port==null) return false;
		else return port.isOpen();
	}

	public boolean isAvailable()
	{
		return isConnected();
	}

	public DeviceListener getDeviceListener()
	{
		return spl;
	}



	class	DeviceListener implements SerialPortDataListener
	{
		private String lastReceivedLine="";

		private StringBuffer line=new StringBuffer();

		@Override
		public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; }

		public void eraseLastReceivedLine() 
		{
			lastReceivedLine="";
		}

		@Override
		public void serialEvent(SerialPortEvent event)
		{
			if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)return;
			byte[] newData = new byte[port.bytesAvailable()];
			int numRead = port.readBytes(newData, newData.length);
			//System.out.println("Read " + numRead + " bytes.");
			//String s=new String(newData);
			//System.out.println(new String(newData));

			for (byte b: newData) 
			{
				if (b == '\n') 
				{
					lastReceivedLine= line.toString();
					//System.out.println(getClass()+" Received &&&: " + line);
					line.setLength(0);
					for (LineReceivedListener lrl:lineReceivedListeners) lrl.actionWhenReceivingALine(lastReceivedLine);
					if (lastReceivedLine.toString().startsWith("ok") || lastReceivedLine.toString().startsWith("error")) 
						{
						completedCommands.add(1);
						//System.out.println("Command received completedCommands size="+completedCommands.size());
						}
				} 
				else 
				{
					line.append((char)b);
					//System.out.println("inputBuffer="+inputBuffer);
				}
			}      

		}
		public String getLastReceivedLine()
		{
			return lastReceivedLine.toString();
		}
	}


	public void closePort() 
	{
		port.closePort();
	}
	
	

	public void send(File file)
	{
		//count the nb of lines
		int c=0;
		try 
		{
		BufferedReader br2 = new BufferedReader(new FileReader(file));
		while (br2.readLine() != null) c++;
		br2.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		int nbLines=c;
		//System.out.println("nbLines="+nbLines);
		Date date0=new Date();
		c=0;

		BufferedReader br = null;
		try 
		{
			String sCurrentLine;
			br = new BufferedReader(new FileReader(file));
			while ((sCurrentLine = br.readLine()) != null) 
			{
				if (c%50==0)//show status
				{
					Date date1=new Date();
					double dts=(date1.getTime()-date0.getTime())/1000.0;
					double pc=((double)c/(double)nbLines*100);
					double totalTime_s=dts/(pc/100.0);
					int timeLeft_s=(int)(totalTime_s-dts);
					int hh=(timeLeft_s)/3600;
					int mm=(timeLeft_s-hh*3600)/60;
					int ss=(timeLeft_s-hh*3600-mm*60);
					String tt=hh+"h"+mm+"m"+ss+"s";
					Global.setMessage("Send line "+(c)+"/"+nbLines+" ("+(int)pc+"%)  "+
							sCurrentLine+" time to end:"+(int)timeLeft_s+" s "+tt);
				}
				try {Thread.sleep(100);} catch (Exception e) {};
				sendLine(sCurrentLine);
				c++;
				if (stopped) break;
			}
			if (br != null)br.close();
		} 
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
	}

	
	
	private void sendLine(String s) 
	{
        // Wait until there is room, if necessary.
        while (GRBL_BUFFER_SIZE < (currentBufferSize + s.length() + 1)) {
            try {
                //System.out.println("waiting for room.... active command count: " + this.activeCommandSizes.size());

                // Wait for a command to complete
                completedCommands.take();
                currentBufferSize -= this.activeCommandSizes.removeFirst();
				//System.out.println("Command can be sent completedCommands size="+completedCommands.size());
                } catch (InterruptedException ex) {
                ex.printStackTrace();
                return;
            }
        }	
		
		try {
			//System.out.printf(".");
			byte[] b=(s.trim()+"\n").getBytes();
			this.port.writeBytes(b,b.length);
			//System.out.println("Sending command: " + new String(b));
			//System.out.println("Command can be sent completedCommands size="+completedCommands.size());
			int commandSize = s.length() + 1;
            activeCommandSizes.add(commandSize);
			currentBufferSize  += commandSize;
			//System.out.println("CurrentBufferSize= " + currentBufferSize);
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
			return;
		}
	}
	
	

	public int getBauds() {
		return bauds;
	}

	public void setBauds(int bauds) {
		this.bauds = bauds;
	}
	@Override
	public void setStopped(boolean b) {
		stopped=b;
		
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public boolean isStopped() 
	{
		return stopped;
	}

	@Override
	public void setTaskEndListener(TaskEndListener taskEndListener) 
	{
		this.taskEndListener=taskEndListener;			
	}


	public static void main(String[] args)
	{
		Global.path="./";
		String file="test.gcode";
		
//		GRBLstreamer d=new GRBLstreamer(new File(Global.path+file));
//		d.setBauds(115200);
//		d.connectOnePort();
//		System.out.println("Streaming <" + file + "> bauds: " + d.getBauds() + ">");
//		d.run();
		
		GRBLstreamer d=new GRBLstreamer(new File(Global.path+file));
		d.setBauds(115200);
		Thread thread=new Thread(d);
		//System.out.println("Button stopped="+stopped);
		thread.start();
		
		//try {Thread.sleep(3000);} catch (Exception e) {};

//		d.send("G91\n");
//		try {Thread.sleep(100);} catch (Exception e) {};
//		d.send("G1X1F200\n");
//		try {Thread.sleep(100);} catch (Exception e) {};
//		d.send("G1X-1F200\n");
//		try {Thread.sleep(100);} catch (Exception e) {};
		
		
//		d.closePort();



	}
	@Override
	public void setTriggerListDuring(TriggerList tl) {
		// TODO Auto-generated method stub
		
	}		


}
