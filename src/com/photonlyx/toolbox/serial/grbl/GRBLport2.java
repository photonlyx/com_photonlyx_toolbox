package com.photonlyx.toolbox.serial.grbl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.photonlyx.toolbox.serial.Device;
import com.photonlyx.toolbox.serial.LineReceivedListener;
import com.photonlyx.toolbox.util.Global;

public class GRBLport2  implements LineReceivedListener
{
	private Device d=new Device();
	private double[] pos=new double[3];//position
	private static DecimalFormat df3=new DecimalFormat("0.000",new DecimalFormatSymbols(Locale.ENGLISH));


	public boolean connectPort()
	{
		d.connectOnePort();
		d.addLineReceivedListener(this);
		return (d.isConnected()) ;
	}


	public boolean isConnected()
	{
		return d.isConnected();
	}



	public void setIncrementalMode()
	{
		if (!this.isConnected())
		{
			System.out.println("Serial port not connected");
			return;
		}
		send("G91\n");
	}

	public void setAbsoluteMode()
	{
		if (!this.isConnected())
		{
			System.out.println("Serial port not connected");
			return;
		}
		if (isConnected()) send("G90\n");
	}



	public double[] getPos() 
	{
		return pos;
	}



	public void closePort()
	{
		d.closePort();
	}	


	public void send(String s)
	{
		if (!this.isConnected())
		{
			System.out.println("Serial port not connected");
			return;
		}
		d.send(s);
	}
	public void sendAndWaitForAknowledgment(String s)
	{
		if (!this.isConnected())
		{
			System.out.println("Serial port not connected");
			return;
		}
		d.sendAndWaitForAknowledgment(s, "ok");
	}


	public  void actionWhenReceivingALine(String s)
	{
		//		System.out.println("actionWhenReceivingALine :" + s);
		if (s.charAt(0)=='<') 
		{
			GRBLport2.decodeGrblStatus(s,pos);
			//System.out.println("x="+pos[0]+" y="+pos[1]+" z="+pos[2]);
			//Global.setMessage("x="+pos[0]+" y="+pos[1]+" z="+pos[2]);
		}
	}

	/**
	 * update pos with x and y found in string
	 * @param s
	 * @param pos
	 */
	public static  void decodeGrblStatus(String s,double[] pos)
	{
		int index1=s.indexOf(':');
		int index2=s.indexOf(',',index1);
		pos[0]=Double.parseDouble(s.substring(index1+1,index2));
		int index3=s.indexOf(',',index2+1);
		int index4=s.indexOf('|',index3+1);
		//		System.out.println("index1="+index1);
		//		System.out.println("index2="+index2);
		//		System.out.println("index3="+index3);
		//		System.out.println("index4="+index4);
		//		System.out.println(s.substring(index3+1,index4));
		pos[1]=Double.parseDouble(s.substring(index2+1,index3));
		pos[2]=Double.parseDouble(s.substring(index3+1,index4));
		//System.out.println("******"+s+"   "+"("+pos[0]+","+pos[1]+","+pos[2]+")");
	}


	/**
	 * update position using ? command
	 */
	public void updatePos()
	{
		if (!this.isConnected())
		{
			System.out.println("Serial port not connected");
			return;
		}
		this.send("?\n");
		try{ Thread.sleep(5);} catch(Exception e){;}
	}

	/*
	 * incremental
	 */
	public void moveXYZ(double dx,double dy,double dz,double speed)
	{
		if (!this.isConnected())
		{
			System.out.println("Serial port not connected");
			return;
		}
		updatePos();
		//System.out.println("pos: X"+df3.format(pos[0])+" Y"+df3.format(pos[1])+" Z"+df3.format(pos[2]));
		double newx=pos[0]+dx;
		double newy=pos[1]+dy;
		double newz=pos[2]+dz;
		String order="G1 "+"X"+df3.format(dx)+" Y"+df3.format(dy)+" Z"+df3.format(dz)+" F"+(int)speed+"\n";
		this.send(order+"\n");
		System.out.println("GRBLport moveXYZ goal: ("+newx+","+newy+","+newz+")");
		waitUntilPos(newx,newy,newz);

	}
	
	/**
	 * absolute coordinates
	 * @param dx
	 * @param dy
	 * @param dz
	 * @param speed
	 */
	public boolean moveXYZAbsolute(double x,double y,double z,double speed)
	{
		if (!this.isConnected())
		{
			System.out.println("Serial port not connected");
			return false;
		}
		this.setAbsoluteMode();
		String order="G1 "+"X"+df3.format(x)+" Y"+df3.format(y)+" Z"+df3.format(z)+" F"+(int)speed+"\n";
		this.send(order+"\n");
		//System.out.println("GRBLport moveXYZ goal: ("+x+","+y+","+z+")");
		boolean ok=waitUntilPos(x,y,z);
		return ok;
	}

	public  boolean waitUntilPos(double newx,double newy,double newz)
	{
		boolean b=false;
		int i=0;
		double diff=0;
		//System.out.println("GRBLport waitUntilPos   pos: X"+df3.format(newx)+" Y"+df3.format(newy)+" Z"+df3.format(newz));
		for(i=0;i<500;i++)
		{
			try{ Thread.sleep(10);} catch(Exception e){;}
			updatePos();
			//System.out.println("GRBLport   pos: X"+df3.format(pos[0])+" Y"+df3.format(pos[1])+" Z"+df3.format(pos[2]));
			diff=Math.abs(newx-pos[0])+Math.abs(newy-pos[1])+Math.abs(newz-pos[2]);
			//System.out.println(i+"       GRBLport waitUntilPos   diff="+diff);
			if (diff<0.01) 
			{
				//if (diff>=0.0001) 
				//System.out.println(i+"       GRBLport waitUntilPos   diff="+diff);
				//System.out.println("GRBLport waitUntilPos  Position reached at step "+(i+1)+"!");
				b=true;
				break;
			}
		}
		if (!b)	System.out.println(i+" GRBLport waitUntilPos  Position NOT reached! diff="+diff);
		return b;

	}
//	public  void waitUntilStability()
//	{
//		int i=0;
//		double diff=0;
//		double[] pos1=new double[3];
//		try{ Thread.sleep(300);} catch(Exception e){;}
//		for(i=0;i<50;i++)
//		{
//			updatePos();
//			pos1[0]=pos[0];
//			pos1[1]=pos[1];
//			pos1[2]=pos[2];
//			System.out.println("GRBLport waitUntilStability   pos: X"+df3.format(pos[0])+" Y"+df3.format(pos[1])+" Z"+df3.format(pos[2]));
//			try{ Thread.sleep(1000);} catch(Exception e){;}
//			updatePos();
//			System.out.println("GRBLport waitUntilStability   pos: X"+df3.format(pos[0])+" Y"+df3.format(pos[1])+" Z"+df3.format(pos[2]));
//			diff=Math.abs(pos1[0]-pos[0])+Math.abs(pos1[1]-pos[1])+Math.abs(pos1[2]-pos[2]);
//			System.out.println(i+"       GRBLport waitUntilStability   diff="+diff);
//			if (diff>0.00001)
//			{
//				System.out.println(i+" GRBLport waitUntilStability  Position NOT reached! diff="+diff);
//				continue;
//			}
//			else
//			{
//				System.out.println(i+" GRBLport waitUntilStability  Position reached!");
//				break;
//			}
//		}
//
//	}
	
	
	
	public void home()
	{
		//System.out.println("Homing");
		if (!this.isConnected())
		{
			System.out.println("Serial port not connected");
			return;
		}
		send("$H\n");
		try{ Thread.sleep(3000);} catch(Exception e){;}
		//waitUntilPos(0, 0, 2);//wait for end of homing
		waitUntilPos(0, 0, 0);//wait for end of homing
	}



	public static void main(String[] args)
	{
		GRBLport2 grblPort=new GRBLport2();
		boolean ok=grblPort.connectPort();
		try{Thread.sleep(2000);}catch(Exception e){}
		
		if (ok)
		{
			grblPort.setAbsoluteMode();
			
			//home:
//			grblPort.send("$H\n");
//			System.out.println("Send: "+"$H\n");
//			try{Thread.sleep(3000);}catch(Exception e){}
			
			grblPort.home();

			//		grblPort.send("G1 X-10 F500\n");
			//		try{Thread.sleep(2000);}catch(Exception e){}
			//		grblPort.send("G1 X10 F500\n");
			//		try{Thread.sleep(2000);}catch(Exception e){}
			grblPort.updatePos();
			double[] pos=grblPort.getPos();
			System.out.println("GRBLport   pos: X"+df3.format(pos[0])+" Y"+df3.format(pos[1])+" Z"+df3.format(pos[2]));

			double step=0.5;
			int n=20;
			int speed=2000;
			for (int i=1;i<=n;i++)
			{
				double z=2+i*step;
				
//				String s="G1Z"+df3.format(z)+"F"+speed+"\n";
//				grblPort.send(s);
//				System.out.println("Send: "+s);
//				try{Thread.sleep(3000);}catch(Exception e){}
				
				boolean ok1=grblPort.moveXYZAbsolute(0,0,z,speed);
				if (!ok1)
				{
					System.out.println("Error moving the scanner");
					break;
				}
				
			}
			//home:
//			grblPort.send("$H\n");
//			System.out.println("Send: "+"$H\n");
//			try{Thread.sleep(3000);}catch(Exception e){}
			grblPort.home();


			grblPort.closePort();
		}

	}

}
