package com.photonlyx.toolbox.serial.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.serial.Device;
import com.photonlyx.toolbox.serial.LineReceivedListener;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class SerialMonitor2 extends WindowApp implements LineReceivedListener
{
	private JTextArea taReceiveAll=new JTextArea();
	private JTextArea taReceive=new JTextArea();
	private JTextArea taSend=new JTextArea();
	private Device d=new Device();

	public SerialMonitor2()
	{
		this.getCJFrame().setTitle("com.photonlyx.SerialMonitor");
		JPanel jp=new JPanel();
		jp.setLayout(new BorderLayout());
		JScrollPane jscrollpane = new JScrollPane(taReceiveAll);
		jp.add(jscrollpane,BorderLayout.CENTER);

		jp.add(taReceive,BorderLayout.SOUTH);

		JPanel jpsend=new JPanel();
		jpsend.setLayout(new BorderLayout());
		jpsend.add(taSend,BorderLayout.CENTER);
		jp.add(jpsend,BorderLayout.NORTH);

		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"sendCommand"));
			CButton cButton=new CButton(tl_button);
			cButton.setText("Send");
			cButton.setPreferredSize(new Dimension(60,30));
			jpsend.add(cButton,BorderLayout.EAST);
		}

		this.add(jp,"All");

		taReceive.setFont(new Font("Arial",Font.BOLD,20));
		taReceive.setEditable(false);
		taReceiveAll.setEditable(false);

		d.addLineReceivedListener(this);
		d.connectOnePort();
	}


	public  void actionWhenReceivingALine(String line)
	{
		//System.out.println("actionWhenReceivingALine line=" + line);
		taReceiveAll.append(line+"\n");
		taReceive.setText(line);
	}

	public void sendCommand()
	{
		d.send(taSend.getText()+'\r');
	}	
	
	
	public static void main(String[] args)
	{
		SerialMonitor2 m=new SerialMonitor2();
	}



}
