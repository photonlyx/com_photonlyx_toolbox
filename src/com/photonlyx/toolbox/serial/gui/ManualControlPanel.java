package com.photonlyx.toolbox.serial.gui;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParStringBox;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.TaskButton2;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.serial.grbl.GRBLport2;
import com.photonlyx.toolbox.serial.grbl.GRBLstreamer;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;

public class ManualControlPanel extends JPanel implements ActionListener
{
	private int wPanel=700,hPanel=500;
	private JPanel jpButtons =new JPanel();
	private int wButton=50;//wPanel/10;
	private int hButton=50;//hPanel/5;
	public int speed=500;
	private Vector<JButton> buttons=new Vector();
	private CButton bopen,bclose, bstatus,bhold,bhome;
	private JTextArea jtagcode=new JTextArea();//box for gcode
	private static String gcodeFileName="manualPanel.gcode";
	protected static GRBLport2 grblPort;
	private String pos;
	private ParamsBox boxPos;

	public ManualControlPanel()
	{
		//this.bauds = bauds;
		grblPort=new GRBLport2();

		this.setBackground(Global.background);
		this.setLayout(new BorderLayout());

		JPanel jp1 =new JPanel();
		jp1.setPreferredSize(new Dimension(160, 500));
		jp1.setBackground(Global.background);
		//jp1.setBackground(Color.red);
		jp1.setLayout(new FlowLayout());
		//this.add(jp1);

		//button to open port 
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(grblPort,"connectPort"));
			tl_button.add(new Trigger(this,"updateButtons"));
			bopen=new CButton(tl_button);
			bopen.setPreferredSize(new Dimension(80,20));
			bopen.setText("Connect");
			jp1.add(bopen);
		}

		//button to close port 
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(grblPort,"closePort"));
			tl_button.add(new Trigger(this,"updateButtons"));
			bclose=new CButton(tl_button);
			bclose.setPreferredSize(new Dimension(80,20));
			bclose.setText("Close");
			jp1.add(bclose);
		}

		//		//box to adjust the bauds rate
		//		{
		//			String[] names2={"bauds"};
		//			ParamsBox box2=new ParamsBox(grblPort,null,names2);
		//			jp1.add(box2.getComponent());
		//		}
		//box to adjust the speed
		{
			String[] names2={"speed"};
			ParamsBox box2=new ParamsBox(this,null,names2);
			jp1.add(box2.getComponent());
		}
		//box to see position
		{
			String[] names={"pos"};
			String[] labels={""};
			boxPos=new ParamsBox(this,null,null,names,null,labels);
			ParStringBox pb=(ParStringBox)boxPos.getParamBoxOfParam("pos");
			pb.setFont(Global.font.deriveFont((float)20));
			pb.setFieldLength(10);
			jp1.add(boxPos.getComponent());
		}

		//button to ask position (grbl status)
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"status"));
			bstatus=new CButton(tl_button);
			bstatus.setPreferredSize(new Dimension(80,20));
			bstatus.setText("Ask pos");
			jp1.add(bstatus);
		}

		//button to hold
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"hold_resume"));
			bhold=new CButton(tl_button);
			bhold.setPreferredSize(new Dimension(80,20));
			bhold.setText("Hold");
			jp1.add(bhold);
		}
		//button to home
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"home"));
			bhome=new CButton(tl_button);
			bhome.setPreferredSize(new Dimension(80,20));
			bhome.setText("Home");
			jp1.add(bhome);
		}


		//panel for the buttons
		jpButtons.setPreferredSize(new Dimension(wPanel, hPanel));
		jpButtons.setBackground(Global.background);
		jpButtons.setLayout(null);
		this.add(jpButtons);


		placeButton('X',100);
		placeButton('X',10);
		placeButton('X',1);
		placeButton('X',0.1);
		placeButton('X',-100);
		placeButton('X',-10);
		placeButton('X',-1);
		placeButton('X',-0.1);

		placeButton('Y',100);
		placeButton('Y',10);
		placeButton('Y',1);
		placeButton('Y',0.1);
		placeButton('Y',-100);
		placeButton('Y',-10);
		placeButton('Y',-1);
		placeButton('Y',-0.1);

		placeButton('Z',100);
		placeButton('Z',10);
		placeButton('Z',1);
		placeButton('Z',0.1);
		placeButton('Z',-100);
		placeButton('Z',-10);
		placeButton('Z',-1);
		placeButton('Z',-0.1);

		Insets insets = jp1.getInsets();
		Dimension size = jp1.getPreferredSize();
		jp1.setBounds(0,0,size.width, size.height);
		jpButtons.add(jp1);

		//gcode panel
		JPanel jpgcode=new JPanel();
		jpgcode.setLayout(new BorderLayout());
		jpgcode.setPreferredSize(new Dimension(200,400));
		jtagcode.setBackground(Global.background);
		jtagcode.setFont(new Font("Courrier",Font.PLAIN,10));

		JScrollPane jsp=new JScrollPane(jtagcode);
		jpgcode.add(jsp,BorderLayout.CENTER);
		this.add(jpgcode,BorderLayout.WEST);

		//panel for button
		JPanel jpgcodeTop=new JPanel();
		jpgcodeTop.setLayout(new FlowLayout());
		jpgcodeTop.setBackground(Global.background);
		jpgcodeTop.setPreferredSize(new Dimension(80,60));
		jpgcode.add(jpgcodeTop,BorderLayout.NORTH);

		//button to execute the gcode of the text area: 
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"saveGcodeFromTextArea"));
			//com.photonlyx.toolbox.serial.grbl.SimpleJavaGrblStreamer grblStreamer=new com.photonlyx.toolbox.serial.grbl.SimpleJavaGrblStreamer(Global.path,gcodeFileName);
			GRBLstreamer grblStreamer=new GRBLstreamer(new File(Global.path+gcodeFileName));
			TaskButton2 button=new TaskButton2(grblStreamer, tl_button);
			button.setText("Send");
			button.getJButton().setPreferredSize(new Dimension(70,20));
			jpgcodeTop.add(button.getJButton());
		}


		////button to open file
		//{
		//TriggerList tl=new TriggerList();
		//tl.add(new Trigger(this,"openFile"));
		//com.photonlyx.serial.grbl.SimpleJavaGrblStreamer grblStreamer=new com.photonlyx.serial.grbl.SimpleJavaGrblStreamer(Global.path,gcodeFileName);
		//TaskButton button=new TaskButton(grblStreamer, tl);
		//button.setText("Open file...");
		//button.getJButton().setPreferredSize(new Dimension(100,20));
		//jpgcodeTop.add(button.getJButton());
		//}
		//button to open file 
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"openFile"));
			CButton b=new CButton(tl);
			b.setPreferredSize(new Dimension(100,20));
			b.setText("Open file...");
			jpgcodeTop.add(b);
		}

		boolean bo=grblPort.isConnected();
		enableButtons(bo);
		if (bo) grblPort.setIncrementalMode();

		//		Thread t=new Thread(new AskPos());
		//		t.start();

	}



	//place the buttons
	private void placeButton(char axis,double val)
	{
		JButton b=new JButton();
		b.setPreferredSize(new Dimension(wButton,hButton));
		b.setFont(Global.font);
		b.setBorderPainted(true);
		b.addActionListener(this);
		b.setMargin(new Insets(0,0,0,0));
		//b.setLocation(100, 100);
		Insets insets = jpButtons.getInsets();
		Dimension size = b.getPreferredSize();

		if (axis=='X') 
		{
			int xpos=(int)(Math.log10(Math.abs(val))*wButton*Math.signum(val));
			xpos+=(int)(wButton*2*Math.signum(val));
			b.setBounds(-size.width+ wPanel/2-size.width/2+xpos + insets.left,-size.height/2+ hPanel/2 + insets.top,size.width, size.height);
			b.setText("X"+val);
		}
		if (axis=='Y') 
		{
			int ypos=(int)(Math.log10(Math.abs(val))*hButton*Math.signum(val));
			ypos+=(int)(hButton*2*Math.signum(val));
			b.setBounds(-size.width+ wPanel/2-size.width/2 + insets.left,-size.height/2+  hPanel/2-ypos + insets.top,size.width, size.height);
			b.setText("Y"+val);
		}
		if (axis=='Z') 
		{
			int ypos=(int)(Math.log10(Math.abs(val))*hButton*Math.signum(val));
			ypos+=(int)(hButton*2*Math.signum(val));
			b.setBounds(-size.width+ wPanel-size.width + insets.left,-size.height/2+  hPanel/2-ypos + insets.top,size.width, size.height);
			b.setText("Z"+val);
		}
		jpButtons.add(b);
		buttons.add(b);
	}


	public void enableButtons(boolean bo)
	{		
		for (JButton b:buttons) b.setEnabled(bo);
		bclose.setEnabled(bo);
		bopen.setEnabled(!bo);
		//bstatus.setEnabled(bo);
		bhold.setEnabled(bo);
	}

	public void updateButtons()
	{	
		enableButtons(grblPort.isConnected());
	}




	@Override
	public void actionPerformed(ActionEvent e)
	{
		//System.out.println(e.getSource().getClass());
		if (e.getSource() instanceof JButton)
		{
			JButton b=(JButton)e.getSource();
			//System.out.println(b.getText());
			String s=b.getText();

			//int val=new Integer(s.substring(1));
			String gcode="G91 G1 "+s+" F"+speed;
			System.out.println(gcode);
			try
			{
				//if (serialPort!=null) serialPort.writeString(gcode+"\n");
				grblPort.send(gcode+"\n");
				//grblPort.send("?\n");
			}
			catch(Exception ex)
			{
				ex.printStackTrace();	
			}	
		}
	}


	public  void saveGcodeFromTextArea()
	{
		String filename=Global.path+gcodeFileName;
		TextFiles.saveString(filename,jtagcode.getText() );
	}


	public void openFile()
	{
		JFileChooser df=new JFileChooser(Global.path);
		CFileFilter ff=new CFileFilter("gcode","gcode file");
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showOpenDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			Global.path=df.getSelectedFile().getParent()+File.separator;
			String fn=df.getSelectedFile().getName();
			String s=TextFiles.readFile(Global.path+fn).toString();
			jtagcode.setText(s);
		}	
	}

	public void status()
	{
		grblPort.updatePos();
		double[] pos1=grblPort.getPos();
		if (pos1!=null) pos="( "+pos1[0]+" , "+pos1[1]+" , "+pos1[2]+" )";
		boxPos.update();
	}

	public void hold_resume()
	{
		if (bhold.getText().compareTo("Hold")==0) 
		{
			//hold machine movement:
			grblPort.send("!");
			bhold.setText("Resume");
		}
		else
		{
			//resume machine movement:
			grblPort.send("~");
			bhold.setText("Hold");			
		}
	}
	public void home()
	{
		//homing
		grblPort.send("$H\n");
	}

	public String getPos() {
		return pos;
	}



	public void setPos(String pos) {
		this.pos = pos;
	}



	public static void main(String[] args)
	{
		//Global.path=
		WindowApp wa=new WindowApp("GRBL control",false,true,false);
		wa.getCJFrame().setTitle("GRBL control");
		wa.add(new ManualControlPanel(),"");
	}




	class AskPos implements Runnable
	{

		@Override
		public void run() 
		{
			for (;;)
			{
				if (grblPort.isConnected()) status();
				try{ Thread.sleep(500);} catch(Exception e){;}
			}	


		}

	}


}
