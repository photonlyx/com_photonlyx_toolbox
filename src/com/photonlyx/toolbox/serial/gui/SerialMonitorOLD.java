package com.photonlyx.toolbox.serial.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class SerialMonitorOLD extends WindowApp
{
	private JTextArea taReceiveAll=new JTextArea();
	private JTextArea taReceive=new JTextArea();
	private JTextArea taSend=new JTextArea();
	private SerialPort p;

	public SerialMonitorOLD()
	{
		this.getCJFrame().setTitle("com.photonlyx.SerialMonitor");
		JPanel jp=new JPanel();
		jp.setLayout(new BorderLayout());
		JScrollPane jscrollpane = new JScrollPane(taReceiveAll);
		jp.add(jscrollpane,BorderLayout.CENTER);

		jp.add(taReceive,BorderLayout.SOUTH);

		JPanel jpsend=new JPanel();
		jpsend.setLayout(new BorderLayout());
		jpsend.add(taSend,BorderLayout.CENTER);
		jp.add(jpsend,BorderLayout.NORTH);

		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"sendCommand"));
			CButton cButton=new CButton(tl_button);
			cButton.setText("Send");
			cButton.setPreferredSize(new Dimension(60,30));
			jpsend.add(cButton,BorderLayout.EAST);
		}

		this.add(jp,"All");

		taReceive.setFont(new Font("Arial",Font.BOLD,20));
		taReceive.setEditable(false);
		taReceiveAll.setEditable(false);

		initialize();
	}

	public void initialize() 
	{
		SerialPort[] comPorts = SerialPort.getCommPorts();
		for (SerialPort p:comPorts)
		{
			System.out.println(p.getSystemPortName());
		}
		if (comPorts.length>0)
		{
			p=comPorts[0];
			p.setBaudRate(115200);
			//https://github.com/Fazecast/jSerialComm/wiki/Modes-of-Operation 
			//p.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
			p.openPort();
			p.addDataListener(new SerialPortDataListener() {
				private String grbl_status;
				private StringBuilder inputBuffer=new StringBuilder();
				private BlockingQueue<Integer> completedCommands;
				private Vector<String> lines=new Vector<String>();
				@Override
				public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; }
				@Override
				public void serialEvent(SerialPortEvent event)
				{
					if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
						return;
					byte[] newData = new byte[p.bytesAvailable()];
					int numRead = p.readBytes(newData, newData.length);
					//System.out.println("Read " + numRead + " bytes.");
					String s=new String(newData);
					//System.out.println(s);
					taReceiveAll.setText(taReceiveAll.getText()+s/*+"\n"*/);
					taReceive.setText(s);
					
					for (byte b: newData) 
					{
						//System.out.print( (char)b);	
						if (b == '\n') 
							{
							//System.out.println("end of line");//detect end of line
							//if (s1.startsWith("ok") ||s1.startsWith("done") ||  s1.startsWith("error")) completedCommands.add(1);
							String s1= inputBuffer.toString();
							//System.out.println(s1);
							if (s1.charAt(0)=='<') 
							{
								grbl_status=s1;
								System.out.println("grlb status:"+grbl_status);
							}
							inputBuffer.setLength(0);
							}
						else inputBuffer.append((char)b);
//						if (b == '\n') //detect end of line
//						{
//							String s1= inputBuffer.toString();
//							System.out.println(getClass()+" Received &&&: " + s);
//							//if (s1.startsWith("ok") ||s1.startsWith("done") ||  s1.startsWith("error")) completedCommands.add(1);
//							if (s1.charAt(0)=='<') 
//							{
//								grbl_status=s1;
//							}
//							inputBuffer.setLength(0);
//						} 
//						else 
//						{
//							inputBuffer.append((char)b);
//							System.out.println("inputBuffer="+inputBuffer);
//						}
					}   				
					
					
				}
			});
		}

	}

	public void writeString(String s) 
	{
		if (p==null) return;
		System.out.println("Send: " + s);
		try 
		{
			byte[] bytes=s.getBytes();
			p.writeBytes(bytes,bytes.length);
		} catch (Exception e) 
		{
			System.out.println("could not write to port");
		}
	}


	public void sendCommand()
	{
		writeString(taSend.getText()+'\r');
	}	
	
	public static void main(String[] args)
	{
		SerialMonitorOLD m=new SerialMonitorOLD();
	}



}
