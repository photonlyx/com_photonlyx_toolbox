package com.photonlyx.toolbox.serial.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ColorList;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.serial.Device;
import com.photonlyx.toolbox.serial.LineReceivedListener;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.util.Global;

public class SerialPlotterApp extends WindowApp implements LineReceivedListener
{
	private Device d;
	private ChartPanel cp=new ChartPanel();//chart
	private JLabel jl=new JLabel();//arduino msg
	private JTextArea taSend=new JTextArea();//msg sent to arduino
	private JTextArea taReceiveAll=new JTextArea();
	private Date date0=new Date();
	private int previousn=-1;
	private Plot[] plots;

	public 	SerialPlotterApp()
	{
		super("com.photonlyx.toolbox.serial.gui.SerialPlotterApp");
		d=new Device();
		d.addLineReceivedListener(this);
		d.connectOnePort();

		cp.getChart().setXlabel("Time");
		cp.getChart().setYlabel("");
		cp.getChart().setXunit("s");
		cp.getChart().setFormatTickx("0");
		cp.getChart().setFormatTicky("0.000");
		cp.getSplitPane().setDividerLocation(800);
		cp.hidePanelSide();
		
		JPanel jp=new JPanel();
		jp.setLayout(new BorderLayout());
		jp.add(cp,BorderLayout.CENTER);
		
		jp.add(jl,BorderLayout.SOUTH);
		
		
		//gui to send msg:
		JPanel jpsend=new JPanel();
		jpsend.setLayout(new BorderLayout());
		jpsend.add(taSend,BorderLayout.CENTER);
		taSend.setBackground(Color.LIGHT_GRAY);
		jp.add(jpsend,BorderLayout.NORTH);
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"sendCommand"));
			CButton cButton=new CButton(tl_button);
			cButton.setText("Send");
			cButton.setPreferredSize(new Dimension(60,30));
			jpsend.add(cButton,BorderLayout.EAST);
		}
		
		//controls:
		JPanel jpc=new JPanel();
		jpc.setPreferredSize(new Dimension(150,0));
		jpc.setLayout(new FlowLayout());
		jpc.setBackground(Global.background);
		jp.add(jpc,BorderLayout.WEST);
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"clean"));
			CButton cButton=new CButton(tl_button);
			cButton.setText("Clean");
			cButton.setPreferredSize(new Dimension(150,30));
			jpc.add(cButton,BorderLayout.EAST);
		}
		
		taReceiveAll.setEditable(false);
		
		JPanel jp1=new JPanel();
		jp1.setLayout(new BorderLayout());
		JScrollPane jscrollpane = new JScrollPane(taReceiveAll);
		JPanel jp2=new JPanel();
		jp2.setLayout(new BorderLayout());
		jp2.add(jscrollpane,BorderLayout.CENTER);
		
		
		this.add(jp, "Chart");
		this.add(jp2,"Terminal");
		
		
		

	}
	public void sendCommand()
	{
		d.send(taSend.getText()+'\r');
	}	
	
	public void clean()
	{
		for (Plot p:cp.getChart().getConcatenatedPlots())
		{
			Signal1D1DXY sig2=(Signal1D1DXY)p.getSignal();
			sig2.init(0);
			//System.out.println(sig2);
		}
		this.taReceiveAll.setText("");
	}

	public Device getDevice()
	{
		return d;
	}

	public void actionWhenReceivingALine(String line) 
	{
		//System.out.println(line);
		jl.setText(line);
		taReceiveAll.append(line+"\n");
		Definition def=new Definition(line);
		int n=def.dim();
		if (n!=previousn)
		{
			cp.getChart().removeAllPlots();
			plots=new Plot[n];
			for (int i=0;i<n;i++) plots[i]=new Plot();
			ColorList cl=new ColorList();
			for (Plot p:plots)
			{
				Signal1D1DXY sig=new Signal1D1DXY();
				p.setSignal(sig);
				p.setColor(cl.getNewColour());
				p.setDotSize(0);
				p.setLineThickness(3);
				cp.getChart().add(p);
			}
			previousn=n;
			cp.getChart().update();
		}
		int c=0;
		for (String s:def)
		{
			try
			{
				double v0= Double.parseDouble(s);
				double time=(new Date().getTime()-date0.getTime())/1000.0;
				Plot p=plots[c];
				Signal1D1DXY sig=(Signal1D1DXY)p.getSignal();
				sig.addPoint(time,v0);
			}
			catch(Exception e)
			{
				//System.out.println(s+" is not a number");
				//e.printStackTrace();
			}
			c++;
		}
		cp.getChart().update(false);
		jl.repaint();

	}

	public static void main(String[] args) //throws Exception 
	{
		SerialPlotterApp app=new SerialPlotterApp();
	}

}
