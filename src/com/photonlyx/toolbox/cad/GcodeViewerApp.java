package com.photonlyx.toolbox.cad;

import java.io.File;

import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.CFileManager;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;

public class GcodeViewerApp extends WindowApp implements CFileManager
{
	private TriggerList updateGUI=new TriggerList();
	private PanelGcodeViewer p3d=new PanelGcodeViewer();
	private double xmin=0,xmax=300,ymin=0,ymax=200;
	private String gcodefilename;

	public GcodeViewerApp()
	{
		super("com_cionin_gcodeviewer",false,true,true);

		this.putMenuFile(this, new CFileFilter("gcode","gcode file"));
		this.getFileMenuTriggerList().add(updateGUI);

		this.add(p3d.getJPanel(), "view");

		updateGUI.add(new Trigger(p3d,"update"));

	}



	public PanelGcodeViewer getPanel() {
		return p3d;
	}





	public boolean save()
	{
		TextFiles.saveString(Global.path+gcodefilename, p3d.getGcode());
		return true;
	}


	public boolean save(String path, String filename)
	{
		TextFiles.saveString(Global.path+filename, p3d.getGcode());
		return true;
	}


	public boolean load()
	{
		open(Global.path,gcodefilename);
		return true;
	}


	public boolean open(String path, String filename)
	{
		gcodefilename=filename;
		p3d.setGcode(TextFiles.readFile(path+File.separator+filename, false).toString());
		return true;
	}


	public void newFile(String newFileName)
	{
		p3d.setGcode("");
	}


	public boolean isSaved()
	{
		return true;
	}


	public void setSaved()
	{

	}


	public boolean isNewFile()
	{
		return false;
	}


	/**get the file name (without path)*/
	public String getFileName()
	{
		return gcodefilename;
	}
	
	public String getPath()
	{
		return this.getPath();
	}


	public static void main(String[] args)
	{

		GcodeViewerApp gcv=new GcodeViewerApp();
		//	gcv.getCFileManager().open("/home/laurent/temp", "cableRap.gcode");
		//	gcv.getPanel().update();

	}



}
