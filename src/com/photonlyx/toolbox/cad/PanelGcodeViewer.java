package com.photonlyx.toolbox.cad;

import java.awt.Color;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.threeD.gui.Text3DColor;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.util.Messager;

public class PanelGcodeViewer extends Graph3DPanel 
{
	private TriggerList updateGUI=new TriggerList();
	private String gcode;
	private double xmax=100,ymax=100;

	public PanelGcodeViewer()
	{
		updateGUI.add(new Trigger(this,"update"));
		this.setAxesLength(150);
	}

	/**
	 * decode Gcode to draw the path in 3D
	 */
	public void update()
	{
		this.removeAll3DObjects();

		//working area
		double step=10;//mm
		int nbSegx=(int)(xmax/step);
		int nbSegy=(int)(ymax/step);
		for (int i=0;i<=nbSegx;i++) this.add(new Segment3DColor(new Vecteur(i*step,0,0),new Vecteur(i*step,nbSegy*step,0),Color.LIGHT_GRAY));
		for (int i=0;i<=nbSegy;i++) this.add(new Segment3DColor(new Vecteur(0,i*step,0),new Vecteur(nbSegx*step,i*step,0),Color.LIGHT_GRAY));

		this.add(new Text3DColor("("+xmax+",0,0)",new Vecteur(xmax,0,0)));
		this.add(new Text3DColor("(0,"+ymax+",0)",new Vecteur(0,ymax,0)));

		if (gcode.contains("G91"))
		{
			Messager.messErr("Relative movements not (yet) available!");
		}
		if (!gcode.contains("G90"))
		{
			Messager.mess("G90 not found. Assume absolute movements");
		}

		//decode gcode:
		String[] lines=StringSource.readLines(gcode);
		Vecteur p1=new Vecteur(),p2;
		String xs,ys,zs;
		double x=0,y=0,z=0;
		int i2;
		Color color=Color.BLUE;
		for (String line:lines)
		{
			//System.out.println(line);
			int i_g=line.indexOf("G1",0);
			if (i_g!=-1)
			{
				int i_x=line.indexOf("X");
				if (i_x!=-1) 
				{
					i2=nextNotNumber(line,i_x+1);
					xs=line.substring(i_x+1, i2);
					//System.out.println("xs="+xs);
					x=Double.parseDouble(xs);
					//System.out.println("x="+x);
				}
				int i_y=line.indexOf("Y");
				if (i_y!=-1) 
				{
					i2=nextNotNumber(line,i_y+1);
					ys=line.substring(i_y+1, i2);
					//System.out.println("ys="+ys);
					y=Double.parseDouble(ys);
					//System.out.println("y="+y);
				}
				int i_z=line.indexOf("Z");
				if (i_z!=-1) 
				{
					i2=nextNotNumber(line,i_z+1);
					zs=line.substring(i_z+1, i2);
					//System.out.println("zs="+zs);
					z=Double.parseDouble(zs);
					//System.out.println("z="+z);
				}
				p2=new Vecteur(x,y,z);
				//System.out.println("p2="+p2);
				Point3DColor p=new Point3DColor(p1,Color.BLACK,2);
				this.add(p);
				Segment3DColor seg=new Segment3DColor(p1,p2,color);
				this.add(seg);
				p1=p2.copy();
			}
			if (line.contains("M5")) color=Color.blue;
			if (line.contains("M3S")) 
			{
				int i=line.indexOf("M3S");
				String powers=line.substring(i+3,i+3+4);
				//System.out.println("powers="+powers);
				int power=Integer.parseInt(powers);
				color=new Color(255,0,0,power*255/1000);
			}

		}
		//p3d.initialiseProjectorAuto();
		//this.update();

	}


	private int nextNotNumber(String s, int index)
	{
		int i=index;
		for (;;)
		{
			//System.out.println(s.charAt(i));
			Character c=s.charAt(i);
			if  ((!Character.isDigit(c))&&(c!='-')) return i;
			i++;
			if (i==s.length()) return (i-1);
		}

	}


	public String getGcode() {
		return gcode;
	}


	public void setGcode(String gcode) {
		this.gcode = gcode;
	}


}
