package com.photonlyx.toolbox.thread;

import com.photonlyx.toolbox.triggering.TriggerList;

public interface CTask2 extends Runnable
{
public void setStopped(boolean b);
public boolean isStopped() ;
public void setTaskEndListener(TaskEndListener taskEndListener);
public void setTriggerListDuring(TriggerList tl);//to trig during the process (if needed)
}


