//LB


package com.photonlyx.toolbox.io;

import java.io.File;
import java.util.Vector;
import javax.swing.filechooser.FileFilter;

/**
 files filtering (extensions, can accept or reject directories)

 */
public class CFileNameFilter  extends FileFilter implements FileFilterWithExtension
{
	private Vector<String> extensions= new Vector<String>();
	private String description;
	private String body;


	/**
	 * 
	 * @param extension a list of extension (lower case only)
	 * @param description description of the accepted files
	 * @param body body of the file names (ie image for image0000.png )
	 * 
	 */
	public CFileNameFilter(String[] extensions, String description,String body) 
	{
		super();
		for (String s:extensions) this.extensions.add(s);
		this.description = description;
		this.body = body;
	}



	// Accept files having the correct extensions
	public boolean accept(File f)
	{
		//		System.out.println(getClass()+" "+f.getPath());
		if (f.isDirectory()) return false;
		String sextension = Utils.getExtension(f);
		if (extensions.size()>0 )
			for (String ext : extensions) 
				if (ext.equals(sextension)) 
				{
					if (f.getName().startsWith(body)) return true;
				}
		return false;
	}


	/**
The description of this filter
@return The description of this filter
	 */
	public String getDescription()
	{
		return description;
	}

	/**
The extension of this filter (the first of the list)
@return The extension of this filter
	 */
	public String getExtension()
	{
		return extensions.elementAt(0);
	}


	public String getFileExtension() 
	{
		return getExtension();
	}


}
