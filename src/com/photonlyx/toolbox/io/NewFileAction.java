

package com.photonlyx.toolbox.io;


import java.io.File;
import java.text.DecimalFormat;

import javax.swing.JFrame;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.util.Global;



/**
ask the JoloFileManager son to create a new file
*/
public class NewFileAction 
{
//links
private CFileManager jfm;
private CFileFilter jff;
private CJFrame fatherWindow;

public NewFileAction(CFileManager jfm,CFileFilter jff,CJFrame fatherWindow)
{
this.jfm=jfm;
this.jff=jff;
this.fatherWindow=fatherWindow;
}



public void work()
{
String s=NewFileAction.getNewFileName(fatherWindow.getApp().getPath(), jff.getExtension());
jfm.newFile(s);
if (fatherWindow!=null) fatherWindow.setTitle(s);
}




public static  String getNewFileName(String path,String extension)
{
//add a number to the default file name such that it is a new file
String newfilename,newglobalfilename;
DecimalFormat f=new DecimalFormat("000");
int count=1;
do
	{
	newfilename="new"+f.format(count)+"."+extension;
	newglobalfilename=path+newfilename;
// 	System.out.println("NewFileAction"+" "+newglobalfilename+" exists ? ->"+new File(newglobalfilename).exists());
	count++;
	}
while (new File(newglobalfilename).exists());
// System.out.println("NewFileAction"+" "+"choosen:"+newglobalfilename);
return newfilename;
}




}