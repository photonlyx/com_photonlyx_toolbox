// Author Laurent Brunel

package com.photonlyx.toolbox.io;

import java.util.*;
import java.io.File;

public class Utils {

/*    public final static String jpeg = "jpeg";
    public final static String jpg = "jpg";
    public final static String gif = "gif";
    public final static String tiff = "tiff";
    public final static String tif = "tif";*/
 
    /*
     * Get the extension of a file.
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
    
    
public static String[] getDirectories(String path)
{
File f=new File(path);
String[] listprov=f.list();
if (listprov==null) return null;

Vector v=new Vector();
for (int i=0;i<listprov.length;i++)
	{
	File ff=new File(path+listprov[i]);
	if ((ff.isDirectory()) &&!(listprov[i].startsWith(".")) ) v.addElement(listprov[i]);
	}

//nouvelle liste avec les directoires
String[] listdir=new String[v.size()];
v.copyInto(listdir);
return listdir;
}
    
    

}
