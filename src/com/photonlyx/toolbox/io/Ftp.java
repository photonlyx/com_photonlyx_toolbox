package com.photonlyx.toolbox.io;

import com.cqs.ftp.FTP;
import com.photonlyx.toolbox.gui.InputDialog;
import com.photonlyx.toolbox.util.Messager;

public class Ftp
{
//private String filename;//
//private String site;//
//private String user;//
//private String directory;//
private static String pwd=null;

public Ftp(String site, String user,String directory,String distantFilename,String localFullFilename)
{
FTP ftp = new FTP(site, 21);
ftp.setLogFile("ftp_log_file.txt");

ftp.connect();
System.out.println(ftp.lastReply());
if (ftp.lastCode() != FTP.CODE_CONNECT_OK)
	{
	System.out.println("Connection failed.");
	Messager.mess("Connection failed."+ftp.lastReply());
	return;
	}

if (pwd==null)
	{
	InputDialog inputDialog=new InputDialog();
	inputDialog.setFieldLabel("ftp password ?");
	inputDialog.setInitialFieldValue("");
	inputDialog.setIsHiddenPassword(true);
	inputDialog.show();
	pwd=inputDialog.getStringAnswer();
	if (pwd==null) return;
	}


ftp.login(user, pwd);
System.out.println(ftp.lastReply());
if (ftp.lastCode() != FTP.CODE_LOGGEDIN)
	{
	System.out.println("incorrect login.");
	ftp.disconnect();
	Messager.mess("incorrect login."+ftp.lastReply());
	pwd=null;
	return;
	}

ftp.setTransfer(FTP.TRANSFER_PASV);
ftp.setMode(FTP.MODE_AUTO);

ftp.cd(directory);
System.out.println(ftp.lastReply());
if (ftp.lastCode() != FTP.CODE_CD_OK)
	{
	System.out.println("error while changing to directory: "+directory);
    ftp.disconnect();
	Messager.mess(ftp.lastReply());
    return;
	}


System.out.println("upload "+localFullFilename);
ftp.upload(localFullFilename,distantFilename);
System.out.println(ftp.lastReply());
if (ftp.lastCode() != FTP.CODE_TRANSFER_OK)
	{
	System.out.println("error while uploading.");
    ftp.disconnect();
	Messager.mess("error while uploading. "+ftp.lastReply());
    return;
	}

ftp.disconnect();
System.out.println(ftp.lastReply());
if (ftp.lastCode() != FTP.CODE_DISCONNECT_OK)
	{
	System.out.println("disconnection failed.");
	Messager.mess("disconnection failed."+ftp.lastReply());
	return;
	}

ftp.closeLogFile();

System.out.println("OK.");
Messager.mess("Upload to "+site+"/"+user+" of "+directory+"/"+distantFilename+" OK !");


}

//
//public void work()
//{
//if (pwd.val.length()==0)
//	{
//	Messager.messErr("Empty password (see config)");
//	return;
//	}
//if (fileMaker!=null) fileMaker.saveFile();
////String fullFilename=Global.chemin+filename.val;
//String fullFilename;
//String file;
//if (filename instanceof ParString) 
//	{
//	fullFilename=((ParString)filename).val; 
//	file=fullFilename;
//	}
//else 
//	{
//	fullFilename=Global.chemin+filename.val;
//	file=filename.val;
//	}
////if (pwd==null)
////	{
////	InputDialog inputDialog=new InputDialog();
////	inputDialog.setFieldLabel("PSWD: "+site.val+"/"+user.val);
////	inputDialog.setParent((JFrame)getNode().lookForFatherOfClass("javax.swing.JFrame"));
////	inputDialog.show();
////	pwd.val=inputDialog.getStringAnswer();
////	}
////if (pwd==null) return;
//
//FTP ftp = new FTP(site.val, 21);
//ftp.setLogFile("ftp_log_file.txt");
//
//ftp.connect();
//af(ftp.lastReply());
//if (ftp.lastCode() != FTP.CODE_CONNECT_OK)
//	{
//	af("Connection failed.");
//	Messager.mess(ftp.lastReply());
//	return;
//	}
//
//ftp.login(user.val, pwd.val);
//af(ftp.lastReply());
//if (ftp.lastCode() != FTP.CODE_LOGGEDIN)
//	{
//	af("incorrect login.");
//	ftp.disconnect();
//	Messager.mess(ftp.lastReply());
//	return;
//	}
//
//ftp.setTransfer(FTP.TRANSFER_PASV);
//ftp.setMode(FTP.MODE_AUTO);
//
//ftp.cd(directory.val);
//af(ftp.lastReply());
//if (ftp.lastCode() != FTP.CODE_CD_OK)
//	{
//    af("error while changing to directory: "+directory.val);
//    ftp.disconnect();
//	Messager.mess(ftp.lastReply());
//    return;
//	}
//
//af("upload "+fullFilename);
//ftp.upload(fullFilename,file);
//af(ftp.lastReply());
//if (ftp.lastCode() != FTP.CODE_TRANSFER_OK)
//	{
//    af("error while uploading.");
//    ftp.disconnect();
//	Messager.mess(ftp.lastReply());
//    return;
//	}
//
//ftp.disconnect();
//af(ftp.lastReply());
//if (ftp.lastCode() != FTP.CODE_DISCONNECT_OK)
//	{
//	af("disconnection failed.");
//	Messager.mess(ftp.lastReply());
//	return;
//	}
//
//ftp.closeLogFile();
//
//af("OK.");
//Messager.mess("Upload to "+site.val+"/"+user.val+" de "+directory.val+"/"+file+" OK !");
//
//}


}
