//LB


package com.photonlyx.toolbox.io;

import java.io.File;
import java.util.Vector;

import javax.swing.filechooser.FileFilter;

import com.photonlyx.toolbox.txt.Definition;


/**
 files filtering (extensions, can accept or reject directories
 */
public class CFileFilter  extends FileFilter implements FileFilterWithExtension
{
	//params
	private Vector<String> extensions= new Vector<String>();
	private String description;
	private boolean acceptDirectories=true;


	/**
	 * 
	 * @param extension a list of extension (lower case only)
	 * @param description
	 */
	public CFileFilter(String[] extensions, String description) 
	{
		super();
		for (String s:extensions) this.extensions.add(s);
		this.description = description;
	}

	/**
	 * @Deprecated
	 * @param extensions
	 * @param description
	 */
	public CFileFilter(String extensions, String description) 
	{
		super();
		Definition def=new Definition(extensions);
		for (int i=0;i<def.dim();i++) this.extensions.add(def.word(i));
		this.description = description;
	}

	/**
	 * 
	 * @param extension a list of extension (lower case only)
	 * @param description
	 */
	public CFileFilter(String[] extensions, String description,boolean acceptDirectories) 
	{
		super();
		for (String s:extensions) this.extensions.add(s);
		this.description = description;
		this.acceptDirectories = acceptDirectories;
	}

	/**
	 * @Deprecated
	 * @param extensions
	 * @param description
	 * @param acceptDirectories
	 */
	public CFileFilter(String extensions, String description,boolean acceptDirectories) 
	{
		super();
		Definition def=new Definition(extensions);
		for (int i=0;i<def.dim();i++) this.extensions.add(def.word(i));
		this.description = description;
		this.acceptDirectories = acceptDirectories;
	}

	// Accept files having the correct extensions
	public boolean accept(File f)
	{
		if (f.isDirectory())
		{
			return acceptDirectories;
		}

		String sextension = Utils.getExtension(f);
		if (extensions.size()>0 )
		{
			for (String ext : extensions)
			{
				if (ext.equals(sextension)) return true;
			}	
		}
		else
		{
			return false;
		}

		return false;
	}

	/**
The description of this filter
@return The description of this filter
	 */
	public String getDescription()
	{
		return description;
	}

	/**
The extension of this filter (the first of the list)
@return The extension of this filter
	 */
	public String getExtension()
	{
		return extensions.elementAt(0);
	}


	public String getFileExtension() 
	{
		return getExtension();
	}


}
