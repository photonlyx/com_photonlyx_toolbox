

package com.photonlyx.toolbox.io;


import java.io.File;
import java.text.DecimalFormat;
import javax.swing.*;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;



/**
ask the JoloFileManager son to create a new file
*/
public class SaveAsFileAction 
{
//links
private CFileManager jfm;
private CFileFilter jff;
private CJFrame fatherWindow;


public SaveAsFileAction(CFileManager jfm, CFileFilter jff,CJFrame fatherWindow)
{
this.jfm = jfm;
this.jff = jff;
this.fatherWindow=fatherWindow;
}


public void work()
{
String ch=fatherWindow.getApp().getPath();
if (ch==null) ch=".";
JFileChooser df=new JFileChooser(ch);
df.addChoosableFileFilter(jff);
df.setFileFilter(jff);
df.setSelectedFile(new File(jfm.getPath()+File.separator+jfm.getFileName()));
int returnVal = df.showSaveDialog(null);
if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	String path=df.getSelectedFile().getParent()+File.separator;
	fatherWindow.getApp().setPath(path);
	//System.out.println("Global.chemin:"+Global.chemin);
	String fileName=df.getSelectedFile().getName();
	String extension=jff.getExtension();
	if (!fileName.endsWith(extension)) fileName+="."+extension;
	if(df.getSelectedFile().exists())
		{
		if (JOptionPane.showConfirmDialog(null,Messager.getString("overwrite_file")+" "+fileName+"?"
			,Messager.getString("Confirm"), JOptionPane.YES_NO_OPTION)== JOptionPane.NO_OPTION)
			{
			return;
			}
		else
			{
			jfm.save(path,fileName);
			if (fatherWindow!=null) fatherWindow.setTitle(/*path+*/fileName);
			}
      	}
	else 
		{
		jfm.save(path,fileName);
		if (fatherWindow!=null) fatherWindow.setTitle(/*path+*/fileName);		
		}
	}
else 
	{
	return;
	}
}






}