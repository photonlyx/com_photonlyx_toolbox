// Author Laurent Brunel


package com.photonlyx.toolbox.io;

import java.io.File;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.Vector;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.txt.TextFiles;




/**
File storage (text file) of objects
 */
public class XMLFileStorage  implements CFileManager
{
	//internal
	private boolean isNewFile=true;
	private String filename="";
	private Vector<XMLstorable> thingsToStore=new Vector<XMLstorable>();
	private WindowApp app;

	public XMLFileStorage()
	{

	}

	public XMLFileStorage(WindowApp app)
	{
		this.app=app;
	}

	public void addThingToStore(XMLstorable thing)
	{
		thingsToStore.add(thing);
	}

	/**
save the file
	 */
	public boolean save()
	{
		if (filename==null) return false;
		return save(app.path,filename);
	}

	/**
save the file with a new filename
@param path the path of the file directory
@param fileName the file name
	 */
	public boolean save(String path,String fileName)
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		for (XMLstorable thing:thingsToStore) el.addChild(thing.toXML());
		StringBuffer sb=new StringBuffer();
		sb.append("<?xml version=\"1.0\"?> \n");
		XMLFileStorage.toStringWithIndentation(el,sb,"");
		//sb.append(el.toString());
		TextFiles.saveString(path+File.separator+fileName,sb.toString());
		filename=fileName;
		System.out.println(getClass()+" "+"saved to "+path+File.separator+fileName);
		setSaved();
		isNewFile=false;
		return true;
	}

	/**
	 * save a XML storable object ina XML file
	 * @param o
	 * @param path
	 * @param fileName
	 * @return
	 */
	public static boolean save(XMLstorable o,String path,String fileName)
	{
		XMLElement el =o.toXML();
		StringBuffer sb=new StringBuffer();
		sb.append("<?xml version=\"1.0\"?> \n");
		XMLFileStorage.toStringWithIndentation(el,sb,"");
		//sb.append(el.toString());
		TextFiles.saveString(path+File.separator+fileName,sb.toString());
		System.out.println(o.getClass()+" "+"saved to "+path+File.separator+fileName);
		return true;
	}

	/**
load the file
	 */
	public boolean load()
	{
		return open(app.path,filename);
	}

	public boolean open(String path,String fileName)
	{
		return open(path,fileName,true);
	}


	/**
open a new file
@param path the path of the file directory
@param fileName the file name
	 */
	public boolean open(String path,String fileName,boolean verbose)
	{
		if (verbose) System.out.println(getClass()+" "+"open: "+path+File.separator+fileName);
		String s=TextFiles.readFile(path+File.separator+fileName,false,verbose).toString();
		if (s==null) return false;

		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(s);
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) {ex.printStackTrace();}

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		int i=0;
		while (enumSons.hasMoreElements()) 
		{
			XMLElement sonXML = enumSons.nextElement();
			XMLstorable thing=thingsToStore.elementAt(i++);
			thing.updateFromXML(sonXML);
		}
		setSaved();
		isNewFile=false;
		filename=fileName;
		return true;
	}

	public static XMLElement  readXMLfile(String fullPath)
	{
		StringBuffer sb=TextFiles.readFile(fullPath,false);
		if (sb==null) return null;
		String s=sb.toString();
		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(s);
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) 
		{
			ex.printStackTrace();
			xml=null;
		}
		return xml;
	}

	/**
create a new file
	 */
	public void newFile(String newFileName)
	{
		//constructFromString(null);
		setSaved();
		filename=newFileName;
		for (XMLstorable thing:thingsToStore) thing.updateFromXML(null);
		isNewFile=true;
	}



	public boolean isSaved()
	{
		//TODO
		return true;
	}


	public void setSaved()
	{
		//TODO

	}

	/**
tells if the file is new or has been already saved
	 */
	public boolean isNewFile()
	{
		return isNewFile;
	}

	/**get the file name (without path)*/
	public String getFileName()
	{
		return filename;
	}

	public String getPath()
	{
		return app.getPath();
	}
	//
	///**
	// * get the son project 
	// * @return
	// */
	//private FObject project()
	//{
	//FLink fl=(FLink) getSonOfClass("flexo.FLink",true);
	////FProject project=(FProject)lookForFatherOfClassAndName("flexo.FProject", "project", false);
	////return fl.getLinkedFObjectFrom(project);
	//return fl.getLinkedFObject(true);
	////return (FObject)getSonOfClassAndName("flexo.FProject","projectOfEditor",false);
	//}
	//
	//private FObject getGroupToSave()
	//{
	//FObject[] fls=this.getLinkedFObjectsOfClass("flexo.io.XMLStringStorable",false);
	////af("nb links: "+fls.length);
	//FObject group=new FObject();
	//for (int i=0;i<fls.length;i++) group.addSon(fls[i]);
	//return group;
	//}


	public static void toStringWithIndentation(XMLElement xml,StringBuffer sb,String indent)
	{
		//put the name of the xml element:
		sb.append(indent+"<"+xml.getName());

		//get the the sons
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		//get the attributes:
		Enumeration<String> enum_attributes_names =xml.enumerateAttributeNames();
		String indent_="        ";

		//put the attributes:
		while (enum_attributes_names.hasMoreElements())
		{
			String atttribute_name=enum_attributes_names.nextElement();
			Object object_attribute=xml.getAttribute(atttribute_name);
			sb.append(" "+atttribute_name+"=\""+object_attribute+"\"");  
		}
		sb.append(">");
		//put the content:
		//Object content=xml.getContent();
		//String s=content.toString();
		//System.out.println("content:");
		//System.out.println(s);
		sb.append(xml.getContent().toString());

		if (enumSons.hasMoreElements()) sb.append("\n");

		while (enumSons.hasMoreElements())
		{
			XMLElement son=enumSons.nextElement();
			toStringWithIndentation(son,sb,indent+indent_);
		}
		sb.append(indent+"</"+xml.getName()+">\n");
	}




	public static String toStringWithIndentation(XMLElement xml)
	{
		StringBuffer sb=new StringBuffer();
		sb.append("<?xml version=\"1.0\"?> \n");
		toStringWithIndentation(xml,sb,"");
		return sb.toString();
	}


	public static XMLElement convertStringToXML(String s)
	{
		//convert to XML:
		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(s);
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) {ex.printStackTrace();}
		return xml;
	}





}

