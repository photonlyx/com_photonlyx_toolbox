package com.photonlyx.toolbox.chart.process;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.photonlyx.toolbox.chart.Chart;
import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.math.Complex;
import com.photonlyx.toolbox.math.signal.FFT;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class Signal2D1DprocessWindowSmooth extends CJFrame
{
/**
 * 
 */
private Chart chart;
public int radius=5;
private Signal2D1D result=new Signal2D1D();
private ChartPanel result_cp=new ChartPanel(result);
private Complex[][] co;

public Signal2D1DprocessWindowSmooth(Chart chart)
{
this.chart=chart;

CJFrame cjf=this;
cjf.setTitle("Smooth square kernel");
cjf.getContentPane().setLayout(new BorderLayout());
cjf.pack();
cjf.setExitAppOnExit(false);
cjf.setAutoCenter(true);
cjf.setVisible(true);
cjf.setPreferredSize(new Dimension(500,500));

JTabbedPane jtp=new JTabbedPane();
cjf.getContentPane().add(jtp,BorderLayout.CENTER);

JPanel toolBar=new JPanel();
toolBar.setPreferredSize(new Dimension(0,100));
cjf.getContentPane().add(toolBar,BorderLayout.NORTH);


TriggerList tl=new TriggerList();
tl.add(new Trigger(this,"update"));
String[] names1={"radius"};
ParamsBox paramsBox1=new ParamsBox(this,tl,names1);
toolBar.add(paramsBox1.getComponent());


//Signal2D1D fft=new Signal2D1D();

result_cp.hidePanelSide();
//fft_cp.showPalette();
//fft_cp.getChart().setSignal2D1D(fft);
jtp.add(result_cp,"fft");


result_cp.getChart().setGridLinex(false);
result_cp.getChart().setGridLiney(false);


this.update();
this.pack();

}



public void update()
{
Signal2D1D signal=chart.getSignal2D1D();
int dimx=signal.dimx();
int dimy=signal.dimy();

result.copy(signal.smoothSquareKernel(radius));

result_cp.update();
}


public void windowClosing(WindowEvent event)
{

chart.update();
}




}
