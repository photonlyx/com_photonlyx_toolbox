package com.photonlyx.toolbox.chart.process;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.photonlyx.toolbox.chart.Chart;
import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.math.Complex;
import com.photonlyx.toolbox.math.signal.FFT;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class Signal2D1DprocessWindowFourierFilter extends CJFrame
{
/**
 * 
 */
private Chart chart;
public double fmin=0,fmax=1000000;
private Signal2D1D fft=new Signal2D1D(),filtered=new Signal2D1D();
private ChartPanel fft_cp=new ChartPanel(fft);
private ChartPanel filtered_cp=new ChartPanel(filtered);
private Complex[][] co;

public Signal2D1DprocessWindowFourierFilter(Chart chart)
{
this.chart=chart;

CJFrame cjf=this;
cjf.setTitle("Fourier filter");
cjf.getContentPane().setLayout(new BorderLayout());
cjf.pack();
cjf.setExitAppOnExit(false);
cjf.setAutoCenter(true);
cjf.setVisible(true);
cjf.setPreferredSize(new Dimension(500,500));

JTabbedPane jtp=new JTabbedPane();
cjf.getContentPane().add(jtp,BorderLayout.CENTER);

JPanel toolBar=new JPanel();
toolBar.setPreferredSize(new Dimension(0,100));
cjf.getContentPane().add(toolBar,BorderLayout.NORTH);


TriggerList tl=new TriggerList();
tl.add(new Trigger(this,"update"));
String[] names1={"fmin","fmax"};
ParamsBox paramsBox1=new ParamsBox(this,tl,names1);
toolBar.add(paramsBox1.getComponent());


//Signal2D1D fft=new Signal2D1D();

fft_cp.hidePanelSide();
//fft_cp.showPalette();
//fft_cp.getChart().setSignal2D1D(fft);
jtp.add(fft_cp,"fft");

filtered_cp.hidePanelSide();
//filtered_cp.showPalette();
//filtered_cp.getChart().setSignal2D1D(filtered);
jtp.add(filtered_cp,"filtered");

fft_cp.getChart().setGridLinex(false);
fft_cp.getChart().setGridLiney(false);
filtered_cp.getChart().setGridLinex(false);
filtered_cp.getChart().setGridLiney(false);


this.update();
this.pack();

}



public void update()
{
Signal2D1D signal=chart.getSignal2D1D();
int dimx=signal.dimx();
int dimy=signal.dimy();

//set dimensions of image to power of 2 and fill with zeros:
int newdimx=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimx)/Math.log(2))));
int newdimy=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimy)/Math.log(2))));
co=new Complex[newdimx][newdimy];
for (int i=0;i<newdimx;i++)//
	  for (int j=0;j<newdimy;j++)//
		  {
		  if ((i<dimx)&&(j<dimy)) co[i][j]=new Complex(signal.getValue(i, j),0);
		  else co[i][j]=new Complex(0,0);
		  }


//calc the complex fft:
FFT.FFT2D(co, 1);


translate();
placeCoNorm(fft);
filter();
placeCoNorm(fft);
//untranslate();

//calc the inverse complex fft:
FFT.FFT2D(co, -1);	

placeCoNorm(filtered);
fft_cp.update();
filtered_cp.update();
}

private void translate()
{
//translate:
int newdimx=co.length;
int newdimy=co[0].length;
Complex[][] co2=new Complex[newdimx][newdimy];
int ii,jj;
for (int i=0;i<newdimx;i++)//
	  for (int j=0;j<newdimy;j++)//
		  {
		  ii=i-newdimx/2;
		  jj=j-newdimy/2;
		  if (ii<0) ii+=newdimx;
		  if (jj<0) jj+=newdimy;
		  co2[i][j]= co[ii][jj];		  
		  }

for (int i=0;i<newdimx;i++)//
	  for (int j=0;j<newdimy;j++)//
		  {
		  co[i][j]= co2[i][j];
		  }

}



private void untranslate()
{
//translate:
int newdimx=co.length;
int newdimy=co[0].length;
Complex[][] co2=new Complex[newdimx][newdimy];
int ii,jj;
for (int i=0;i<newdimx;i++)//
	  for (int j=0;j<newdimy;j++)//
		  {
		  ii=i+newdimx/2;
		  jj=j+newdimy/2;
		  if (ii>=newdimx) ii-=newdimx;
		  if (jj>=newdimy) jj-=newdimy;
		  co2[i][j]= co[ii][jj];		  
		  }

for (int i=0;i<newdimx;i++)//
	  for (int j=0;j<newdimy;j++)//
		  {
		  co[i][j]= co2[i][j];
		  }
}



private void filter()
{
int newdimx=co.length;
int newdimy=co[0].length;
int ii,jj;//frequencies
double r;
Complex c0=new Complex(0,0);
for (int i=0;i<newdimx;i++)//
	  for (int j=0;j<newdimy;j++)//
		  {		  
		  ii=i-newdimx/2;
		  jj=j-newdimy/2;
		  r=Math.sqrt(ii*ii+jj*jj);
		  
//		  r=Math.sqrt(i*i+j*j);
		  if (r<fmin) 
			  {
			  co[i][j]._mul(0);
//			  co[-i+newdimx-1][j]=c0;
//			  co[i][-j+newdimy-1]=c0;
//			  co[-i+newdimx-1][-j+newdimy-1]=c0;
			  }
		  if (r>fmax) 
			  {
			  co[i][j]._mul(0);
//			  co[-i+newdimx-1][j]=c0;
//			  co[i][-j+newdimy-1]=c0;
//			  co[-i+newdimx-1][-j+newdimy-1]=c0;
			  }
		  }
}





private void invertFFT()
{
//calc the inverse complex fft:
FFT.FFT2D(co, -1);	
}


private void placeCoNorm(Signal2D1D sig)
{
Signal2D1D signal=chart.getSignal2D1D();
double deltax=(signal.xmax()-signal.xmin())/(signal.dimx()-1);
double deltay=(signal.ymax()-signal.ymin())/(signal.dimy()-1);
int newdimx=(int)Math.pow(2,(int)(Math.ceil(Math.log(signal.dimx())/Math.log(2))));
int newdimy=(int)Math.pow(2,(int)(Math.ceil(Math.log(signal.dimy())/Math.log(2))));
double newxmin=-deltax*newdimx/2;
double newxmax=deltax*newdimx/2;
double newymin=-deltay*newdimy/2;
double newymax=deltay*newdimy/2;
Signal2D1D sig1=new Signal2D1D(newxmin,newxmax,newymin,newymax,newdimx,newdimy);
int ii,jj;
for (int i=0;i<newdimx;i++)//
	  for (int j=0;j<newdimy;j++)//
		  {
//		  ii=i-newdimx/2;
//		  jj=j-newdimy/2;
//		  if (ii<0) ii+=newdimx;
//		  if (jj<0) jj+=newdimy;
//		  sig1.setValue(i, j, co[ii][jj].norm());
		  sig1.setValue(i, j, co[i][j].norm());
		  }
sig.copy(sig1);


}




private void calcComplexFFT()
{
Signal2D1D signal=chart.getSignal2D1D();
int dimx=signal.dimx();
int dimy=signal.dimy();

//set dimensions of image to power of 2 and fill with zeros:
int newdimx=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimx)/Math.log(2))));
int newdimy=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimy)/Math.log(2))));
co=new Complex[newdimx][newdimy];
for (int i=0;i<newdimx;i++)//
	  for (int j=0;j<newdimy;j++)//
		  {
		  if ((i<dimx)&&(j<dimy)) co[i][j]=new Complex(signal.getValue(i, j),0);
		  else co[i][j]=new Complex(0,0);
		  }
//calc the complex fft:
FFT.FFT2D(co, 1);
}


public void windowClosing(WindowEvent event)
{

chart.update();
}




}
