package com.photonlyx.toolbox.chart;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.util.ClipBoard;



public class PlotSet implements Iterable<Plot>,XMLstorable
{
private boolean rainbow=false;
private boolean reverseRainbow=false;
	
private Vector<Plot> plotList=new Vector<Plot>();
private boolean hasChanged=false;



public boolean isRainbow() {
	return rainbow;
}

/**
 * 
 * @param rainbow if true put rainbow colors
 */
public void setRainbow(boolean rainbow) {
	this.rainbow = rainbow;
	applyRainbowColors();
}


public boolean isReverseRainbow() {
	return reverseRainbow;
}


public void setReverseRainbow(boolean reverseRainbow) {
	this.reverseRainbow = reverseRainbow;
	applyRainbowColors();
}


/**add a JoloPlotXY*/
public void add(Plot plot)
{
plotList.add(plot);
}


public void paint(Graphics g,Transformation2DPix t)
{
for(Plot plot:plotList)
		{
		plot.paint(g,t);
		}
}


/**
 * get the occupancy rectangle. Update the point Pmin and Pmax if bigger
 */
public void updateBoundaries(Vecteur2D Pmin,Vecteur2D Pmax)
{
for (Plot plot : plotList)	plot.updateBoundaries(Pmin, Pmax);
}


public Plot getSelectedPlot() 
{
for (Plot plot : plotList) if (plot.isSelected()) return plot;
return null;
}

/**
get an array containing the selected plots
*/
public Plot[] getSelectedPlots()
{
int nbSelected=0;
Vector<Plot> v=new Vector<Plot>();
for (int k=0;k<nbPlots();k++)
	{
	if (getPlot(k).isSelected())
		{
		v.add(getPlot(k));
		nbSelected++;
		}
	}
// System.out.println(getClass()+" "+nbSelected+" selected plots");
if (nbSelected<1) return null;
Plot[] array=new Plot[nbSelected];
v.copyInto(array);
return array;
}

/**
get an array containing the visible plots
*/
public Plot[] getVisiblePlots()
{
int nbVisible=0;
Vector<Plot> v=new Vector<Plot>();
for (int k=0;k<nbPlots();k++)
	{
	if (getPlot(k).isVisible())
		{
		v.add(getPlot(k));
		nbVisible++;
		}
	}
// System.out.println(getClass()+" "+nbSelected+" selected plots");
if (nbVisible<1) return null;
Plot[] array=new Plot[nbVisible];
v.copyInto(array);
return array;
}


public int nbPlots() {

	return plotList.size();
}


public Plot getPlot(int i) 
{
if (i>=nbPlots()) return null;
return plotList.elementAt(i);
}

/**
 * get the plot of a date (epoqu in ms)
 * @param date_ms
 * @return
 */
public Plot getPlotOfDate(long date_ms) 
{
Plot pp=null;
for (Plot p:plotList) 
	{
	long date=p.getDate();
	if (date==date_ms) 
		{
		pp=p;
		break;
		}
	}
return pp;
}
/**
 * get the Plot having this string in the label
 * @param s
 * @return
 */
public Plot getPlotHavingInLabel(String s) 
{
Plot pp=null;
for (Plot p:plotList) 
	{
	if (p.getLabel().contains(s)) return p;
	}
return pp;
}

/**
 * get the Plot having this string in the label
 * @param s
 * @return
 */
//public Vector<Plot> getPlotsHavingInLabel(String s) 
//{
//Vector<Plot> v=new Vector<Plot>();
//for (Plot p:plotList) 
//	{
//	if (p.getLabel().contains(s)) v.add(p);
//	}
//return v;
//}

/**
 * get the Plot having this string in the label
 * @param s
 * @return
 */
public PlotSet getPlotsHavingInLabel(String s) 
{
PlotSet ps=new PlotSet();
for (Plot p:plotList) 
	{
	if (p.getLabel().contains(s)) ps.add(p);
	}
return ps;
}

public  Vector<Plot >getPlots() { return plotList;}



public Iterator<Plot> iterator() {
	return plotList.iterator();
}


public void add(PlotSet plotSet) 
{
for (Plot plot : plotSet) add(plot);	
}


public void addNotGrayed(PlotSet plotSet) 
{
for (Plot plot : plotSet) 
	{
	//System.out.println(plot.getLabel()+" grayed="+plot.isGrayed());
	if (!plot.isGrayed()) add(plot);	
	}
}


/**
 * add a new JoloPlotXY connected the a Signal1D1DXY
 * @param signal
 * @return
 */
public Plot add(Signal1D1D signal)
{
//System.out.println(getClass()+" add plot with signal:"+signal.getNode().getName());
Plot plot=new Plot(signal);
this.add(plot);
return plot;
}

public void add(Signal1D1DXY signal, String string)
{
Plot plot=new Plot(signal);
plot.setLabel(string);
this.add(plot);
}

public void removePlot(Plot plot) 
{
if (plot.isErasable()) plotList.remove(plot);
}


public boolean has(Plot p)
{
return plotList.contains(p);
}

public boolean hasPlotOfDate(long date_ms)
{
for (Plot plot : plotList) 
	{
	if (plot.getDate()==date_ms) return true;
	}
return false;
}

public void  removePlotOfDate(long date_ms)
{
Plot p=null;
for (Plot plot : plotList) 
	{
	if (plot.getDate()==date_ms) p=plot;;
	}
if (p!=null) plotList.remove(p);
}

public void select(int selectedPlotIndex) 
{
if ((selectedPlotIndex>=0)&(selectedPlotIndex<plotList.size()))
	plotList.elementAt(selectedPlotIndex).setSelected(true);
	
}

public void unselectAll() {
	for (Plot plot : plotList) plot.setSelected(false);
	
}
public void selectAll() {
	for (Plot plot : plotList) plot.setSelected(true);
	
}

/** raises the plot in th Z-Order*/
public void raisePlot(Plot plot)
{
// Can't raise the topmost (last) plot
int index = plotList.indexOf( plot );
if (index == plotList.size() - 1) return;

plotList.insertElementAt(plotList.elementAt(index+1), index );
plotList.removeElementAt(index+2);
setChanged(true);
}

/** lowers the plot in th Z-Order*/
public void lowerPlot(Plot plot)
{
// Can't lower the first element 
int index = plotList.indexOf( plot );
if ( index == 0) return;

plotList.removeElementAt(index);
plotList.insertElementAt(plot, index -1 );
setChanged(true);
}



/**
set the "changed or not" status
@param b if true the object has changed, if false the object has not changed
*/
public void setChanged(boolean b)
{
//TODO?applyFilter();
hasChanged=b;
}


//public String toXML() 
//{
//NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
//nf.setMaximumFractionDigits(10);
//nf.setMinimumFractionDigits(10);
//nf.setGroupingUsed(false);
//StringBuffer sb=new StringBuffer();
//sb.append("<"+getClass()+">");
//for (Plot plot : plotList) sb.append(plot.toXML());
//sb.append("</"+getClass()+"> ");
//return sb.toString();
//}


public XMLElement toXML() 
{
XMLElement el = new XMLElement();
el.setName(getClass().toString().replace("class ", ""));
for (Plot plot : plotList) el.addChild(plot.toXML());
return el;
}


public void updateFromXML(XMLElement xml)
{
plotList.removeAllElements();
if (xml==null) return;
Enumeration<XMLElement> enumSons = xml.enumerateChildren();
while (enumSons.hasMoreElements())
	{
	Plot plot=new Plot();
	plot.updateFromXML(enumSons.nextElement());
	//System.out.println("plotset updateFromXML, plot: "+plot.getLabel());
	add(plot);
	}
}


/**
paste a text of data placed in columns in (nbcolumns-1) curves (the first column is the x data).

 * @param namesInFirstLine if true the first line contains the name of data series
 */
public void pasteStringInPlots(boolean namesInFirstLine)
	{
	ClipBoard clipBoard = new ClipBoard();
	String spasted=clipBoard.getText();	
	putStringInPlots(spasted,namesInFirstLine);
	}



public void putStringInPlots(String s,boolean namesInFirstLine)
{
Definition defLegend=null;
if (namesInFirstLine) 
	{
	String fl=StringSource.firstLine(s);
	defLegend=new Definition(fl);
	}	
s=s.replaceAll(",", ".");
double[][] data=StringSource.fillMatrix(s,namesInFirstLine);
// 			System.out.println(getClass()+" pasted data:\n"+spasted);
if (data==null) return;
for (int i=1;i<data[0].length;i++)
	{
	Signal1D1DXY pasted=new Signal1D1DXY();
	pasted.init(data.length);
	for (int k=0;k<pasted.getDim();k++) pasted.setValue(k,data[k][0],data[k][i]);
	Plot plotpasted=new Plot(pasted);
	if (namesInFirstLine)  plotpasted.setLabel(defLegend.word(i));
	add(plotpasted);
	}
}



public void removeAllPlots() {
	plotList.removeAllElements();
	
}

public void applyRainbowColors()
{
if (reverseRainbow)
	{
	float startHue = 0.69f;
	for (int i = 0 ; i < nbPlots() ;i++)
		{
		float hueStep = 0.55f * i /(nbPlots() - 1.0f);
		if (hueStep > 0.42) hueStep+=0.15;
		float hue = startHue - hueStep;
		Color c=Color.getHSBColor(hue,1.0f,1.0f);
		getPlot(i).setColor("rgb"+" "+c.getRed()+" "+c.getGreen()+" "+c.getBlue());
		}
	}
//System.out.println(getClass()+" nbPlots="+this.nbPlots());
//System.out.println(getClass()+" rainbow="+rainbow);
if (rainbow)
	{
	//System.out.println(getClass()+" repaint with rainbow");
	for (int i=0;i<nbPlots();i++)
		{
		float hue=-0.09f+(float)i/(float)(nbPlots())*0.78f;
		if (hue>1) hue-=1;
		Color c=Color.getHSBColor(hue,1.0f,1.0f);
		getPlot(i).setColor("rgb"+" "+c.getRed()+" "+c.getGreen()+" "+c.getBlue());
		}
	}
}




}
