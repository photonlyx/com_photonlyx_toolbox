package com.photonlyx.toolbox.chart;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal2D1D;


/**
 * @Deprecated
 * @author laurent
 *
 */
public class ChartWindowOld
{
private Chart chart=new Chart();
private Legend l=new Legend(chart);
private CJFrame cjframe	=new CJFrame();
private PlotSet ps=new PlotSet();

private void init()
{
//create chart and legend:
l.setChart(chart);
l.updateGraphics();
chart.setTitle(" ");
chart.setFixedLimitY(false);
//c.setYmin(0);
//c.setYmax(6);
chart.setFixedLimitX(false);
chart.setXmin(0);
chart.setXmax(23);//make a PlotSet

chart.add(ps);

//create frame and add the chart and and legend in a border layout:
cjframe.setTitle("Chart");
cjframe.setSize(600, 400);
cjframe.setExitAppOnExit(false);
JTabbedPane topTabPane=new JTabbedPane();
cjframe.add(topTabPane);

JPanel pc=new JPanel();
pc.setLayout(new BorderLayout());
topTabPane.add(pc,"");
//pc.add(chart,BorderLayout.CENTER);

//split pane for graph and legend:
JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, chart ,l.getComponent());
sp.setDividerLocation(500);
sp.setOneTouchExpandable(true); 
sp.setContinuousLayout(true); 
sp.setResizeWeight(1);

pc.add(sp);

//pc.add(l.getMainPanel(),BorderLayout.EAST);
chart.update();
l.update();

cjframe.setVisible(true);
}

public ChartWindowOld()
{
init();
}

public ChartWindowOld(Signal1D1D s)
{
init();
Plot plot=new Plot(s);
plot.setLabel("signal");
ps.add(plot);
chart.repaint();
}

public ChartWindowOld(PlotSet ps2)
{
init();
chart.add(ps2);
cjframe.setVisible(true);
chart.repaint();
l.update();
}

public ChartWindowOld(Signal2D1D imageFromProfile)
{
init();
chart.setSignal2D1D(imageFromProfile);
chart.repaint();
}

public void add(Signal1D1D s)
{
Plot plot=new Plot(s);
plot.setLabel("signal");
ps.add(plot);
chart.repaint();
}

public void add(Signal1D1D s,String label)
{
Plot plot=new Plot(s);
plot.setLabel(label);
ps.add(plot);
chart.repaint();
}
public void add(Signal1D1D s,String label,String color,int lineThickness,int dotSize)
{
Plot plot=new Plot(s);
plot.setLabel(label);
plot.setColor(color);
plot.setLineThickness(lineThickness);
plot.setDotSize(dotSize);
ps.add(plot);
chart.repaint();
l.update();
}
public void update()
{
chart.update();
}

public CJFrame getCjframe()
{
	return cjframe;
}

public Chart getChart()
{
	return chart;
}



public PlotSet getPlotSet()
{
	return ps;
}






}
