package com.photonlyx.toolbox.chart;

import java.util.Vector;

public class ChartEvent
{
private Vector<Plot> deletedPlots=new Vector<Plot>();
private Vector<Plot> modifiedPlots=new Vector<Plot>();
	

public void addDeletedPlot(Plot p)
{
deletedPlots.add(p);
}

public void addModifiedPlot(Plot p)
{
modifiedPlots.add(p);
}

public Vector<Plot> getDeletedPlots()
{
return deletedPlots;
}

public Vector<Plot> getModifiedPlots()
{
return modifiedPlots;
}

}
