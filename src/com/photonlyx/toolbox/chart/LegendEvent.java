package com.photonlyx.toolbox.chart;

import java.util.Vector;

public class LegendEvent
{
private Vector<Plot> legendModifiedPlot=new Vector<Plot>();
	
public void addLegendModifiedPlot(Plot p)
{
legendModifiedPlot.add(p);
}

public Vector<Plot> getLegendModifiedPlots()
{
return legendModifiedPlot;
}

}
