//Author Laurent Brunel
	

package com.photonlyx.toolbox.chart;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Vector;

import com.photonlyx.toolbox.gui.ColorList;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.txt.Definition;


/**
 * 
 * @author laurent
 * parameters available:
 * thickness centerValue integral integralProfile centerSlope mean 
 * rms yValue xValue powerLaw picXposition meanSlope spotSize
 *
 */

public class AnalysePlotSet 
{
//links
private PlotSet analysedPlotSet;//the faisceau source
private PlotSet resultPlotSet=new PlotSet();//the faisceau sink where to put the results
//private Signal1D1DXY signal;//the result
//params
private String faisceauSourceLabel;
private String xParameter="formula";
private String yParameter="formula";
private String zParameter="formula";


//possible words:
//  thickness centerValue integral integralProfile centerSlope mean 
//  rms yValue xValue powerLaw picXposition meanSlope
private String xUserParameter="index";//user defined (in legend) parameter for x
private String yUserParameter="mean";//user defined (in legend) parameter for y
private String zUserParameter="none";//user defined (in legend) parameter for z

private String xunit="";
private String xlabel;
private String ylabel;
//private Param xAxisInSeconds;
private double xValue;//x value taken for a y value serie
private double yValue;//y value taken for a y value serie
//private double xValue4x;//x value taken for a x value serie
//private double yValue4x;//y value taken for a x value serie
//private double xValue4z;//x value taken for a z value serie
//private double yValue4z;//y value taken for a z value serie
private String filter="";//text that must be in the legend, if nothing takes all curves
private String separate="";//user parameter used as criterium for separating in as many curves as values for this parameter
private String format="0.000";//format for separator value in legend
private boolean average=false;//user parameter used as flag for averaging: all results of the curves having this flag will be averaged and merged in one result
private boolean logy=false; //status of the graph y axis 
private double xMinRange=0,xMaxRange=0,yMinRange=0,yMaxRange=0;//limits of the zone defined by the user

private int pdotSize=10,plineThickness=0;
private String pdotColor="red",plineColor="red",psurfaceColor="red";
//private String pstyle;//the string that define the style
private Signal2D1D image=new Signal2D1D();//if output is an image
private int imageDimx=10,imageDimy=10;
private long datems0;
private Vector<PlotProcessor> processors=new Vector();
private String[] processNames ={"index","time","yValue" ,"xValue","mean", "centerValue" ,"integral", "integralProfile" ,"thickness",
		"centerSlope"  ,"rms" , "powerLaw", "picXposition" ,
		"meanSlope", "spotSize"};


private ColorList colorList=new ColorList(" blue    red  brown black green lightGray darkGray pink pastelBlue pastelRed");
private boolean sortResult=false;


public void addPlotProcessor(PlotProcessor pr)
{
processors.addElement(pr);
}

public Vector<String> getAllProcessNames()
{
Vector<String> v=new Vector<String>();
for (String s:processNames) v.add(s);
for (PlotProcessor p:processors) v.add(p.getName());
return v;
}

public void analyse()
{
PlotSet faisceauSource=analysedPlotSet;
if (faisceauSource==null) return;

int  dotSize=pdotSize;//"10";
int lineThickness=plineThickness;//"0";
String dotColor=pdotColor;//"red";
String lineColor=plineColor;//"red";
String surfaceColor=psurfaceColor;//"red";

//keep the style of the previous curve if it exists
if (resultPlotSet.nbPlots()>0)
	{
	//style=faisceauSink.getPlot(0).getStyleString();
	dotSize=resultPlotSet.getPlot(0).getDotSize();
	lineThickness=resultPlotSet.getPlot(0).getLineThickness();
	dotColor=resultPlotSet.getPlot(0).getDotColor();
	lineColor=dotColor;
	surfaceColor=dotColor;
	}

//remove all curves on the analyse graph
resultPlotSet.removeAllPlots();

if (faisceauSource.nbPlots()==0) return;

boolean useZ=(zParameter!=null);
if (useZ) useZ=useZ&&(zUserParameter.compareTo("none")!=0);

//case of processing to an image:
if (useZ)
	{
	processPlots2Image( faisceauSource);
	//af(matrix.toString());
	}
//af("separate=*"+separate.val+"*");
else if (separate.compareTo("")==0)//we want just one curve
	{
	//af ("No separator ");
	Signal1D1DXY signal=processPlots2Curve( faisceauSource,"",0);
	//af(signal);
//	String label;
//	if (analysePlotLabel==null)  label="Analyse"; else label=analysePlotLabel;
	String comment="";
	
	//average all values having the same X
	if (average)
		{
		//af("average");
		signal=signal.averageForSameX(logy);
		}
	if (sortResult) signal._sort();
	Plot plot=new Plot(signal,yUserParameter,comment,dotSize,lineThickness,dotColor,lineColor,surfaceColor);
	resultPlotSet.add(plot);
	
	}
else
{
	//check the possible values of the parameter of name separare.val:
	Vector<Double> v=new Vector<Double>();
	for (int i=0;i<faisceauSource.nbPlots();i++)
		{
		if (!filter(filter,faisceauSource.getPlot(i).getLabel())) continue;
		double d=formula(separate,faisceauSource.getPlot(i),xValue,yValue);
		if (!has(v,d)) 
				{
				//af("separate: "+separate.val+"="+d);
				v.add(new Double(d));
				}
		}
	
	double[] a=new double[v.size()];
	for (int i=0;i<v.size();i++) a[i]=v.elementAt(i);
	int[] indicesSorted=com.photonlyx.toolbox.math.Sorter.sort(a);
	
	DecimalFormat nf = new DecimalFormat(format,new DecimalFormatSymbols(Locale.ENGLISH));
	
	//make an analysis for each value of the parameter "separate.val"
	for (int i=0;i<v.size();i++) 
		{
		double value=v.elementAt(indicesSorted[i]).doubleValue();
		//af ("analyse for separator "+separate.val+" value= "+value);
		Signal1D1DXY signal=processPlots2Curve( faisceauSource,separate,value);
		if (average) signal=signal.averageForSameX(logy);
		String label=separate+"="+nf.format(value);
		String comment="";
		String color=colorList.colour(i);
		if (sortResult) signal._sort();
		Plot plot=new Plot(signal,label, comment,dotSize,lineThickness,color,color,color);
		resultPlotSet.add(plot);
		}
}






}

private boolean has(Vector<Double> v,double d)
{
for (int i=0;i<v.size();i++) 
	{
	if (v.elementAt(i).doubleValue()==d) return true;
	}
return false;
}



private Signal1D1DXY  processPlots2Curve(PlotSet faisceau,String separatorName,double separatorValue)
{
Signal1D1DXY signal=new Signal1D1DXY();
signal.init(0);
datems0=0;
if (faisceau.nbPlots()>=1) /*datems0=datems(faisceau.getPlot(0));*/
datems0=faisceau.getPlot(0).getDate();
System.out.println("datems0="+datems0);
for (int i=0;i<faisceau.nbPlots();i++)
	{
	//if (!faisceau.plot(i).isVisible()) continue;
	String label=faisceau.getPlot(i).getLabel();
	
	//check if there is the good separator
	if (separatorName.compareTo("")!=0) 
		{
		double d=formula(separatorName,faisceau.getPlot(i),xValue,yValue);
		if (d!=separatorValue) continue;
		}
	
	//check the filter
	boolean ok=filter(filter,label);
	if (!ok) continue;

//		if (ok)	 faisceau.getPlot(i).setVisible(true);
//		else
//			{
//			faisceau.getPlot(i).setVisible(false);
//			continue;
//			}
	
//	af("process "+i);
	if ( faisceau.getPlot(i)==null) continue;
	if ( faisceau.getPlot(i).getSignal()==null) continue;
	Signal1D1DXY plotSignal=faisceau.getPlot(i).getSignal().transfertTo1D1DXY();
	if ( !((xMinRange==xMaxRange)||(yMinRange==yMaxRange)) )
		{
		//System.out.println(xMinRange+" "+xMaxRange+" "+yMinRange+" "+yMaxRange);
		plotSignal._truncx(xMinRange,xMaxRange);
		plotSignal._truncy(yMinRange,yMaxRange);
		}

//*******************calc the x value:
double x=i;

if ((xParameter.compareTo("index")==0)
		||((xParameter.compareTo("formula")==0)&(xUserParameter.compareTo("index")==0))
		)
		{
		x=i;
		xlabel="Index";
		xunit="";
		}
else  if (xParameter.compareTo("formula")==0)
		{
		//x=formula(xUserParameter,faisceau.getPlot(i),xValue4x,yValue4x);
		x=formula(xUserParameter,faisceau.getPlot(i),xValue,yValue);
		xlabel=xUserParameter;
		xunit="";
		}
else 
		{
		xlabel=xParameter;
		xunit="";
		//x=getCalculationOnCurve( faisceau.getPlot(i),xParameter,xValue4x,yValue4x);
		x=getCalculationOnCurve( faisceau.getPlot(i),xParameter,xValue,yValue);
		}
	
	//af("xval="+x);

//******************calc the y value:
		ylabel=yParameter;
	if (yParameter.compareTo("formula")==0) 
		{
		double y=formula(yUserParameter,faisceau.getPlot(i),xValue,yValue);
		ylabel=yUserParameter;
		signal.addPoint(x,y);
		}
	else	
	if (yParameter.compareTo("yValue")==0) 
		{
		signal.addPoint(x,plotSignal.valueInterpol(xValue));
		ylabel="Yvalue at X="+xValue;
		}
	else if (yParameter.compareTo("xValue")==0) 
		{
		//Signal1D1DXY profile2=plotSignal.transfertTo1D1DXY();
		double r=plotSignal.solve(yValue);
		//af("solve Xvalue for y="+yValue+" result="+r);
		signal.addPoint(x,r);
		ylabel="Xvalue at Y="+yValue;
		}

	else
		{
		double r=getCalculationOnCurve( faisceau.getPlot(i),yParameter,xValue,yValue);
		signal.addPoint(x,r);
		}
	}
/*if (fs instanceof JoloGraphXY2) 
		{
		//af("repaint : "+ fs.getClass());
		((JoloGraphXY2)fs).work();
		Legend l=((JoloGraphXY2)fs).getLegend();
		if (l!=null) l.work();
		}*/
//af(signal);
return signal;
}






private void processPlots2Image(PlotSet faisceau)
{
//vectors because some plots will be filtered:
Vector<Double> vx=new Vector<Double>();
Vector<Double> vy=new Vector<Double>();
Vector<Double> vz=new Vector<Double>();

datems0=0;
if (faisceau.nbPlots()>=1) /*datems0=datems(faisceau.getPlot(0));*/
datems0=faisceau.getPlot(0).getDate();

for (int i=0;i<faisceau.nbPlots();i++)
	{
	double x,y,z;
	//if (!faisceau.plot(i).isVisible()) continue;
	String label=faisceau.getPlot(i).getLabel();
	
	//check if there is the good separator
//	if (separatorName.compareTo("")!=0) 
//		{
//		double d=formula(separatorName,faisceau.plot(i),xValue.val,yValue.val);
//		if (d!=separatorValue) continue;
//		}
	
	//check the filter
	boolean ok=filter(filter,label);

		if (ok)	 faisceau.getPlot(i).setVisible(true);
		else
			{
			faisceau.getPlot(i).setVisible(false);
			continue;
			}
	
//	af("process "+i);
	Signal1D1DXY plotSignal=faisceau.getPlot(i).getSignal().transfertTo1D1DXY();
	if ( !((xMinRange==xMaxRange)||(yMinRange==yMaxRange)) )
		{
		plotSignal._truncx(xMinRange,xMaxRange);
		plotSignal._truncy(yMinRange,yMaxRange);
		}

//*******************calc the x value:

if (xParameter.compareTo("index")==0)
		{
		x=i;
		xlabel="Index";
		xunit="";
		}
else  if (xParameter.compareTo("formula")==0)
		{
//		x[i]=formula(xUserParameter,faisceau.getPlot(i),xValue4x,yValue4x);
		x=formula(xUserParameter,faisceau.getPlot(i),xValue,yValue);
		xlabel=xUserParameter;
		xunit="";
//		System.out.println(i+" "+xUserParameter+" x="+x[i]);
		}
else 
		{
		xlabel=xParameter;
		xunit="";
//		x[i]=getCalculationOnCurve( faisceau.getPlot(i),xParameter,xValue4x,yValue4x);
		x=getCalculationOnCurve( faisceau.getPlot(i),xParameter,xValue,yValue);
		}
	
	//af("xval="+x);

//******************calc the y value:


		ylabel=yParameter;
	if (yParameter.compareTo("formula")==0) 
		{
		y=formula(yUserParameter,faisceau.getPlot(i),xValue,yValue);
		ylabel=yUserParameter;
//		System.out.println(i+" "+yUserParameter+" y="+y[i]);
		//af("y="+y[i]);
		}
		
	else if (yParameter.compareTo("yValue")==0) 
		{
		y=plotSignal.valueInterpol(xValue);
		ylabel="Yvalue at X="+xValue;
		}
	else if (yParameter.compareTo("xValue")==0) 
		{
		y=plotSignal.solve(yValue);
		//af("solve Xvalue for y="+yValue+" result="+r);
		ylabel="Xvalue at Y="+yValue;
		}
	else
		{
		y=getCalculationOnCurve( faisceau.getPlot(i),yParameter,xValue,yValue);
		}
	
	//******************calc the z value:

		//ylabel.val=yParameter.val;
	if (zParameter.compareTo("formula")==0) 
		{
		//z[i]=formula(yUserParameter,faisceau.getPlot(i),xValue4z,yValue4z);
		z=formula(zUserParameter,faisceau.getPlot(i),xValue,yValue);
		//ylabel.val=yUserParameter.val;
//			System.out.println(i+" "+zUserParameter+" z="+z[i]);
		}
		
	else if (zParameter.compareTo("yValue")==0) 
		{
//			z[i]=plotSignal.valueInterpol(xValue4z);
		z=plotSignal.valueInterpol(xValue);
		//ylabel.val="Yvalue at X="+xValue.val;
		}
	else if (zParameter.compareTo("xValue")==0) 
		{
//			z[i]=plotSignal.solve(yValue4z);
		z=plotSignal.solve(yValue);
		//af("solve Xvalue for y="+yValue.val+" result="+z[i]);
		//ylabel.val="Xvalue at Y="+yValue.val;
		}
	else
		{
//			z[i]=getCalculationOnCurve( faisceau.getPlot(i),zParameter,xValue4z,yValue4z);
		z=getCalculationOnCurve( faisceau.getPlot(i),zParameter,xValue,yValue);
		}
		
		vx.add(new Double(x));
		vy.add(new Double(y));
		vz.add(new Double(z));
	}

//collect the x,y,z vamue in arrays:
int n=vx.size();
double[] x=new double[n];
double[] y=new double[n];
double[] z=new double[n];
for (int i=0;i<n;i++) x[i]=vx.elementAt(i).doubleValue();
for (int i=0;i<n;i++) y[i]=vy.elementAt(i).doubleValue();
for (int i=0;i<n;i++) z[i]=vz.elementAt(i).doubleValue();

//max of the x and y:
double xmax=-1e30;
double ymax=-1e30;
for (int i=0;i<n;i++)
	{
	if (x[i]>xmax) xmax=x[i];
	if (y[i]>ymax) ymax=y[i];
	//af("y="+y[i]+" ymax="+ymax);
	}
//max of the x and y:
double xmin=1e30;
double ymin=1e30;
for (int i=0;i<n;i++)
	{
	if (x[i]<xmin) xmin=x[i];
	if (y[i]<ymin) ymin=y[i];
	}

//af("xmin="+xmin);
//af("xmax="+xmax);
//af("ymin="+ymin);
//af("ymax="+ymax);

//set the boundaries of the image
image.init(xmin, xmax, ymin, ymax, imageDimx, imageDimy);

//fill the image:
//double sx=matrix.samplingx();
//double sy=matrix.samplingy();
for (int i=0;i<n;i++)
	{
	//af(x[i]+" "+y[i]+" "+z[i]);
	int cellx=(int)(((x[i]-xmin)/(xmax-xmin))*(image.dimx()-1));
	int celly=(int)(((y[i]-ymin)/(ymax-ymin))*(image.dimy()-1));
	//System.out.println("cellx="+cellx+" celly="+celly+" z="+z[i]);
	image.setValue(cellx, celly, z[i]);
	}

/*if (fs instanceof JoloGraphXY2) 
		{
		//af("repaint : "+ fs.getClass());
		((JoloGraphXY2)fs).work();
		Legend l=((JoloGraphXY2)fs).getLegend();
		if (l!=null) l.work();
		}*/
}



//
//private long datems(Plot plot)
//{
//long datems;
//Definition def=new Definition(plot.getComment());
////af(def);
//try
//	{
//	datems=new Long(def.word(0)).longValue();
//	}
//catch(Exception e)
//	{
//	return 0;
//	}
//return datems;
//}
	

/**
 * calculate the parameter following the formula
 * @param formula
 * @param plot
 * @param xvalue value on x axis that might be used
 * @param yvalue value on y axis that might be used
 * @return
 */
private double formula(String formula,Plot plot,double xvalue,double yvalue)
{
String phrase=plot.getLabel()+" "+plot.getComment();
Definition defPhrase=new Definition(phrase);
//if (formula.indexOf("+")!=-1) formula=Matcher.quoteReplacement(formula).replaceAll(Matcher.quoteReplacement("+"),Matcher.quoteReplacement( " + "));
if (formula.indexOf("-")!=-1) formula=formula.replaceAll("-", " - ");
//if (formula.indexOf("*")!=-1) formula=formula.replaceAll("*", " * ");
if (formula.indexOf("/")!=-1) formula=formula.replaceAll("/", " / ");
Definition defFormula=new Definition(formula);
//System.out.println("formula:  "+defFormula);
double result=0;
String operator=null;
for (int i=0;i<defFormula.dim();i++)
	{
	String w=defFormula.word(i);
//	if ((operator==null)&(!isOperator(w)))
//		{
//		Messager.messErr("Wrong formula");
//		return 0;
//		}
	double value=0;
	
	if (isOperator(w)) operator=w;
	else 
		{
		if (isANumericalValue(w))
			{
			value=new Double(w.replace(',','.')).doubleValue();
			}
		else if (defPhrase.hasWord(w)) 
			{
			value=defPhrase.searchValue(w, 0, false);
			//af("param "+w+" value="+value);
			}
		else
			{
			value= getCalculationOnCurve(plot,w,xvalue,yvalue);
			//af("calc "+w+" on curve. Result="+value);
			}
		 
		if (operator==null) result=value;
			else
				{
				//af ("Operation: "+result+operator+value);
				if (operator.compareTo("+")==0) result+=value;
				if (operator.compareTo("-")==0) result-=value;
				if (operator.compareTo("*")==0) result*=value;
				if (operator.compareTo("/")==0) result/=value;
				operator=null;
				}
		}
		
	}
return result;
}

private boolean isOperator(String word)
{
return (word.compareTo("+")==0)||(word.compareTo("-")==0)||(word.compareTo("*")==0)||(word.compareTo("/")==0);
}


/**
get the thickness of the profile
*/
public double getThickness(Signal1D1D p)
{
//calculate the thickness in x
double rms=0;
double s=0;
for (int j=0;j<p.getDim();j++) 
	{
	double dx=p.x(j);
	rms+=dx*dx*p.y(j);
	s+=p.y(j);
	}
rms/=s;
rms=Math.sqrt(rms);

return rms;
}

/**
get the integral of the profile
*/
public double getIntegral(Signal1D1D p)
{
double s=0;
for (int j=0;j<p.getDim();j++) 
	{
	s+=p.y(j);
	}
return s;
}

///**integrate volume (having revolution symmetry) having profile p
// * 
// * @param p profile
// * @return
// */
//public double getIntegralProfile(Signal1D1D p)
//{
//double E=0;
//for (int i=0;i<p.getDim()-1;i++)
//	{
//	double dv=Math.PI*(p.y(i+1)+p.y(i))/2*(Math.pow(p.x(i+1),2)-Math.pow(p.x(i),2));
//	E+=dv;
//	}
//return E;
//}


/**
get the integral of the profile
*/
public double getCenterSlope(Signal1D1D p)
{
return (p.y(1)-p.y(0))/(p.x(1)-p.x(0));
}

/**
 * calc a parameter on a curve
 * @param plot
 * @param word
 * @param xvalue value on x axis that might be used
 * @param yvalue value on y axis that might be used
 * @return
 */
private double  getCalculationOnCurve(Plot plot,String word,double xvalue,double yvalue)
{
double d=0;
//Signal1D1D plotSignal=plot.getSignal();

Signal1D1DXY plotSignal=plot.getSignal().transfertTo1D1DXY();
//if ( !((xMinRange==xMaxRange)||(yMinRange==yMaxRange)) )
//	{
//	plotSignal._truncx(xMinRange,xMaxRange);
//	plotSignal._truncy(yMinRange,yMaxRange);
//	}
if ( (xMinRange!=xMaxRange) )
	{
	plotSignal._truncx(xMinRange,xMaxRange);
	}
if ( (yMinRange!=yMaxRange) )
	{
	plotSignal._truncy(yMinRange,yMaxRange);
	}


//System.out.println("getCalculationOnCurve word: "+word);

if (word.compareTo("time")==0)
	{
	double dateseconds=(plot.getDate()-datems0)/1000.;
	d=dateseconds;
//	xAxisInSeconds.val=1;
	xlabel="Time";
	xunit="s";
	}
/*else if (word.compareTo("index")==0)
	{
	Faisceau f=((Faisceau)plot.getNode().getFatherOfClass("fa.reuse.jolo.plot.Faisceau"));
	d=f.indexOf(plot);
//	xAxisInSeconds.val=0;
	xlabel="Index";
	xunit="";
	}*/


else if (word.compareTo("thickness")==0) d=getThickness(plotSignal);
else if (word.compareTo("centerValue")==0) d=plotSignal.y(0);
else if (word.compareTo("integral")==0) d=getIntegral(plotSignal);
else if (word.compareTo("integralProfile")==0) d=plotSignal.getSignal1D1D().getIntegralProfile();
else if (word.compareTo("centerSlope")==0) d=getCenterSlope(plotSignal);
else if (word.compareTo("mean")==0) d=plotSignal.mean();
else if (word.compareTo("rms")==0) d=plotSignal.rms();
else if (word.compareTo("yValue")==0) 
		{
		d=plotSignal.valueInterpol(xvalue);
		ylabel="Yvalue at X="+xvalue;
		}
else if (word.compareTo("xValue")==0) 
		{
		Signal1D1DXY profile2=plotSignal.transfertTo1D1DXY();
		double r=profile2.solve(yvalue);
		//af("solve Xvalue for y="+yValue.val+" result="+r);
		d=r;
		}
else if (word.compareTo("powerLaw")==0) 
		{
		Signal1D1DXY profile2=plotSignal.transfertTo1D1DXY();
		profile2._log10TheX();
		profile2._log10TheY();
		d=profile2.meanSlope();
		}
else if (yParameter.compareTo("picXposition")==0) 
			{
			Signal1D1DXY profile2=plotSignal.transfertTo1D1DXY();
			double r1=profile2.solve(yvalue,profile2.xmin(),true);
			double r2=profile2.solve(yvalue,profile2.xmax(),false);
			d=(r1+r2)/2;
			}
else if (word.compareTo("meanSlope")==0) 
		{
		Signal1D1DXY profile2=plotSignal.transfertTo1D1DXY();
		d=profile2.meanSlope();
		//af("meanSlope="+d);
		}
else if (word.compareTo("spotSize")==0) 
		{
		Signal1D1D c=plotSignal.getIntegralProfileCumulator();
		double ymax=c.value(c.getDim()-1);//c is always increasing
		double y=c.transfertTo1D1DXY().solve(ymax*0.85);
		d=y;
	//	Signal1D1DXY profile2=plotSignal.transfertTo1D1DXY();
	//	d=profile2.solve(profile2.ymax()*0.0002);
		}
else//look in the plugged processors: 
	{
	for (PlotProcessor pr:processors)
		{
		if (word.compareTo(pr.getName())==0)
			{	
			if (!(xMinRange==xMaxRange)) d=pr.process(plot,xMinRange,xMaxRange);
			else d=pr.process(plot);
			}
		}
	}
return d;
}

private boolean isANumericalValue(String s)
{
	double val;
	try
		{
		val=new Double(s.replace(',','.')).doubleValue();
		return true;
		}
	catch (NumberFormatException e)
		{
		return false;
		}
}

private boolean filter(String filteringFormula,String label)
{
boolean ok=true;

	if (filteringFormula.compareTo("")!=0)
		{
		Definition def=new Definition(label);
		Definition defFilter=new Definition(filteringFormula);
		//af(def);
		if (defFilter.dim()==1)
			{
			if (filteringFormula.contains("!"))
				{
				String s=filteringFormula.replace("!", "");
				if (def.hasWord(s)) ok=false;else ok= true;
				}
			else if (!def.hasWord(filteringFormula)) ok=false;
			}
		else 
			{
			if (defFilter.hasWord("AND"))
				{
				defFilter.removeWord("AND");
				ok=true;
				for (int k=0;k<defFilter.dim();k++) ok=ok&def.hasWord(defFilter.word(k));
				}
			else if (defFilter.hasWord("OR"))
				{
				defFilter.removeWord("OR");
				ok=false;
				for (int k=0;k<defFilter.dim();k++) ok=ok|def.hasWord(defFilter.word(k));
				}
			else//no AND nor OR means AND
				{
				ok=true;
				for (int k=0;k<defFilter.dim();k++) ok=ok&def.hasWord(defFilter.word(k));
				}
			
			}
//		if (!faisceau.plot(i).label().contains(filter.val)) continue;
		}
	else ok=true;

return ok;
}

public PlotSet getFaisceauSink()
{
	return resultPlotSet;
}

public void setFaisceauSink(PlotSet faisceauSink)
{
	this.resultPlotSet = faisceauSink;
}
/*
public String getxParameter()
{
	return xParameter;
}

public void setxParameter(String xParameter)
{
	this.xParameter = xParameter;
}

public String getyParameter()
{
	return yParameter;
}

public void setyParameter(String yParameter)
{
	this.yParameter = yParameter;
}

public String getzParameter()
{
	return zParameter;
}

public void setzParameter(String zParameter)
{
	this.zParameter = zParameter;
}
*/


public String getxUserParameter()
{
	return xUserParameter;
}

public void setxUserParameter(String xUserParameter)
{
	this.xUserParameter = xUserParameter;
}

public String getyUserParameter()
{
	return yUserParameter;
}

public void setyUserParameter(String yUserParameter)
{
	this.yUserParameter = yUserParameter;
}

public String getzUserParameter()
{
	return zUserParameter;
}

public void setzUserParameter(String zUserParameter)
{
	this.zUserParameter = zUserParameter;
}

public double getxValue()
{
	return xValue;
}

public void setxValue(double xValue)
{
	this.xValue = xValue;
}

public double getyValue()
{
	return yValue;
}

public void setyValue(double yValue)
{
	this.yValue = yValue;
}

/*
public double getxValue4x()
{
	return xValue4x;
}

public void setxValue4x(double xValue4x)
{
	this.xValue4x = xValue4x;
}

public double getyValue4x()
{
	return yValue4x;
}

public void setyValue4x(double yValue4x)
{
	this.yValue4x = yValue4x;
}

public double getxValue4z()
{
	return xValue4z;
}

public void setxValue4z(double xValue4z)
{
	this.xValue4z = xValue4z;
}

public double getyValue4z()
{
	return yValue4z;
}

public void setyValue4z(double yValue4z)
{
	this.yValue4z = yValue4z;
}
*/
public String getFilter()
{
	return filter;
}

public void setFilter(String filter)
{
	this.filter = filter;
}

public String getSeparate()
{
	return separate;
}

public void setSeparate(String separate)
{
	this.separate = separate;
}

public boolean isAverage()
{
	return average;
}

public void setAverage(boolean average)
{
	this.average = average;
}

public PlotSet getAnalysedPlotSet()
{
	return analysedPlotSet;
}

public void setAnalysedPlotSet(PlotSet analysedPlotSet)
{
	this.analysedPlotSet = analysedPlotSet;
}

public PlotSet getResultPlotSet()
{
	return resultPlotSet;
}

public void setResultPlotSet(PlotSet resultPlotSet)
{
	this.resultPlotSet = resultPlotSet;
}

public Signal2D1D getImage()
{
	return image;
}

public int getImageDimx()
{
	return imageDimx;
}

public void setImageDimx(int imageDimx)
{
	this.imageDimx = imageDimx;
}

public int getImageDimy()
{
	return imageDimy;
}

public void setImageDimy(int imageDimy)
{
	this.imageDimy = imageDimy;
}

public double getxMinRange()
{
	return xMinRange;
}

public void setxMinRange(double xMinRange)
{
	this.xMinRange = xMinRange;
}

public double getxMaxRange()
{
	return xMaxRange;
}

public void setxMaxRange(double xMaxRange)
{
	this.xMaxRange = xMaxRange;
}

public double getyMinRange()
{
	return yMinRange;
}

public void setyMinRange(double yMinRange)
{
	this.yMinRange = yMinRange;
}

public double getyMaxRange()
{
	return yMaxRange;
}

public void setyMaxRange(double yMaxRange)
{
	this.yMaxRange = yMaxRange;
}

public boolean getSortResult() {
	return sortResult;
}

public void setSortResult(boolean sortResult) {
	this.sortResult = sortResult;
}

}