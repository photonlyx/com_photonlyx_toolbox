package com.photonlyx.toolbox.chart;

public interface PlotProcessor 
{
/**
 * process the plot
 * @param p
 * @return
 */
public double process(Plot p);
/**
 * process the plot after trunking between xmin and xmax
 * @param p
 * @param xmin
 * @param xmax
 * @return
 */
public double process(Plot p,double xmin,double xmax);
/** get the name of this processing*/
public String getName();

}
