package com.photonlyx.toolbox.chart;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.util.Global;

import nanoxml.XMLElement;

public class Note implements MouseListener,MouseMotionListener,MouseWheelListener, KeyListener,XMLstorable
{
private String text;
private double[] pos=new double[2];//position in real coordinates
private Color color=Color.blue;
private Color selectedColor=Color.red;
private boolean selected,clicked;
private Chart c;
private LabelEditNote labelEditNote;
private Point pos1,pos2;


public Note(Chart c)
{
this.c=c;	
}


public Note(Chart c,double[] pos,String text)
{
this.c=c;	
this.pos[0]=pos[0];
this.pos[1]=pos[1];
this.text=text;
}

public void draw(Graphics g, Chart c) 
{
int[] p1Screen=c.getTransformation().toPixels(pos);
if (selected) g.setColor(selectedColor); else g.setColor(color);
g.drawString(text,p1Screen[0], p1Screen[1]);
}


/** move by creen coordinates*/
private void translate(int dx,int dy)
{
int[] p1Screen=c.getTransformation().toPixels(pos);
p1Screen[0]+=dx;
p1Screen[1]+=dy;
pos=c.getTransformation().imageToReal(p1Screen);
}

public String toString()
{
	return text+" (x,y)="+pos[0]+" "+pos[1];
}

public String getText() {
	return text;
}

public void setText(String text) {
	this.text = text;
}

public double[] getPos() {
	return pos;
}


private double distance2(Point p1,int[] p2)
{
return Math.pow(p1.x-p2[0],2)+Math.pow(p1.y-p2[1],2);	
}

@Override
public void mouseClicked(MouseEvent e) 
{
Point p=e.getPoint();
int[] p1=c.getTransformation().toPixels(pos);
double d2=distance2(p,p1);
if (d2<1000) 
	{
	System.out.println(this);
	if (e.getClickCount()==1)	
		{
		selected=true; 
		}
	if (e.getClickCount()==2)	
		{
		System.out.println("double click at "+p.x+" "+p.y);
		//edit task label:
		labelEditNote= new LabelEditNote(this);
		labelEditNote.setBackground(Global.background);
		c.add(labelEditNote);
		//labelEditJTextArea.setLocation(e.getPoint().x,e.getPoint().y);
		labelEditNote.setLocation(p);
		c.validate();
		//System.out.println("edit");
		}
	}
else 
	{
	selected=false;
	if (labelEditNote!=null)
		{
		c.remove(labelEditNote);
		labelEditNote=null;
		}
	}

}

@Override
public void mouseEntered(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseExited(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void mousePressed(MouseEvent e) 
{
Point p=e.getPoint();
int[] p1=c.getTransformation().toPixels(pos);
double d2=distance2(p,p1);
if (d2<1000) clicked=true; else clicked=false;
pos1=p;	
}

@Override
public void mouseReleased(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}


@Override
public void mouseDragged(MouseEvent e) 
{
//System.out.println(this.getClass()+" dragged");
if (clicked)
	{
	int dx=e.getX()-pos1.x;
	int dy=e.getY()-pos1.y;
	
	//int[] p1Screen=c.getTransformation().toPixels(pos);
	//p1Screen[0]+=dx;
	//p1Screen[1]+=dy;
	pos=c.getTransformation().imageToReal(e.getPoint());

	}
}


@Override
public void mouseMoved(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}



@Override
public void mouseWheelMoved(MouseWheelEvent arg0) {
	// TODO Auto-generated method stub
	
}



@Override
public void keyPressed(KeyEvent e) 
{
switch(e.getKeyCode())
	{
	case KeyEvent.VK_DELETE:
	case KeyEvent.VK_BACK_SPACE:
		{
		if (selected) 
			{
			System.out.println(this.getClass()+" remove "+this.text);
			c.getNotes().remove(this);
			}
		}
		break;
	}	
}


@Override
public void keyReleased(KeyEvent e) {
	// TODO Auto-generated method stub
	
}


@Override
public void keyTyped(KeyEvent e) {
	// TODO Auto-generated method stub
	
}






public XMLElement toXML() 
{
XMLElement el = new XMLElement();
el.setName(getClass().toString().replace("class ", ""));
el.setAttribute("text",text );
el.setAttribute("x",pos[0] );
el.setAttribute("y",pos[1] );
return el;
}


public void updateFromXML(XMLElement xml)
{
if (xml==null) 
	{
	text="";
	pos=new double[2];
	return;
	}
text=xml.getStringAttribute("text", "");
pos[0]=xml.getDoubleAttribute("x", 0);
pos[1]=xml.getDoubleAttribute("y", 0);

}













public class LabelEditNote extends JTextArea implements DocumentListener
{
private Note t;

public LabelEditNote(Note t)
{
this.t=t;
this.getDocument().addDocumentListener(this);
this.setSize(new Dimension(500,20));
this.setText(t.getText());
}



@Override
public void insertUpdate(DocumentEvent e)
{
String s=this.getText();
//remove chars that can corrupt the xml storage:
s=s.replace('"','-');
s=s.replace('>','-');
s=s.replace('<','-');
s=s.replace('&','-');
t.setText(s);
//System.out.println(t.getLabel());

}


@Override
public void removeUpdate(DocumentEvent e)
{
t.setText(this.getText());

}


@Override
public void changedUpdate(DocumentEvent e)
{
t.setText(this.getText());

}



}
























































}
