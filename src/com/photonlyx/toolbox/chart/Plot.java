package com.photonlyx.toolbox.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Stroke;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.gui.ColorUtil;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.txt.Definition;


public class Plot 
{
	public static int NO_SYMBOL=0;
	public static int ROUND=1;
	public static int SQUARE=2;
	public static int CROSS=3;
	public static int TRIANGLE=4;
	public static int NB_SYMBOLS=5;
	public static int[] SYMBOL_LIST= {NO_SYMBOL,ROUND,SQUARE,CROSS,TRIANGLE};

	public static int NO_BARS=0;
	//	public static int VERT_BARS=1;
	//	public static int COLUMNS=2;
	//	public static int HALF_VERT_BARS=3;
	//	public static int TOP_BARS=4;
	public static int FULL_BARS=1;
	public static int EMPTY_BARS=2;

	private String lineColor="red";
	private String surfaceColor="red";
	private String dotColor="red";
	private String barColor="red";
	private boolean empty;//empty symbols
	private int dotSize=5;
	private boolean shining=false;
	private int symbol=ROUND;
	private boolean showLine=true;
	private int lineThickness=1;
	private int barsStyle=NO_BARS;
	private int barsThickness=10;
	private  String label="";
	private  String comment="";
	private long dateEpochMs=-1;

	private boolean selected=false;
	private boolean visible=true;//for the legend filter: if true the plot will not be seen neither in the chart nor in the legend
	//	private int visibility =1;
	//	public static int INVISIBLE=0;
	//	public static int FULLY_VISIBLE=1;
	//	public static int VISIBLE_IN_LEGEND_ONLY=2;
	private boolean grayed=false;//for manual visibility: if true put in gray in legend and not visible in chart
	private boolean erasable=true;

	private Signal1D1D signal;
	private Signal1D1D error;//error bar


	public Plot()
	{
		this.setDateEpochMs(new Date().getTime());
	}
	public Plot(Signal1D1D signal)
	{
		this.setDateEpochMs(new Date().getTime());
		this.signal	=signal;
	}

	/**
	 * @Deprecated
	 * @param signal
	 * @param label
	 * @param color
	 */
	public Plot(Signal1D1D signal,String label,String color)
	{
		this.setDateEpochMs(new Date().getTime());
		this.signal	=signal;
		this.setLabel(label);
		this.setColor(color);
	}

	/**
	 * 
	 * @param signal
	 * @param label
	 * @param c
	 */
	public Plot(Signal1D1D signal,String label,Color c)
	{
		this.setDateEpochMs(new Date().getTime());
		this.signal	=signal;
		this.setLabel(label);
		this.setColor(ColorUtil.color2String(c));
	}
	public Plot(Signal1D1D signal,String label,Color c,Date date)
	{
		this.setDateEpochMs(new Date().getTime());
		this.signal	=signal;
		this.setLabel(label);
		this.setColor(ColorUtil.color2String(c));
		this.setDateEpochMs(date.getTime());
	}
	public Plot(Signal1D1D signal,String label,Color c,int style)
	{
		this.setDateEpochMs(new Date().getTime());
		this.signal	=signal;
		this.setLabel(label);
		this.setColor(ColorUtil.color2String(c));
		this.setDotStyle(style);
	}

	public Plot(Signal1D1D signal,Signal1D1D error, String label,Color c)
	{
		this.setDateEpochMs(new Date().getTime());
		this.signal	=signal;
		this.setLabel(label);
		this.setColor(ColorUtil.color2String(c));
		this.setError(error);
	}

	/**
	 * @Deprecated
	 * @param signal
	 * @param label
	 * @param color
	 * @param dotSize
	 * @param lineThickness
	 */
	public Plot (Signal1D1D signal,String label,String color,int dotSize,int lineThickness)
	{
		this.setDateEpochMs(new Date().getTime());
		this.setSignal(signal);
		this.setLabel(label);
		this.setDotSize(dotSize);
		this.setLineThickness(lineThickness);
		this.setColor(color);
	}

	/**
	 * 
	 * @param signal
	 * @param label
	 * @param c
	 * @param dotSize
	 * @param lineThickness
	 */
	public Plot (Signal1D1D signal,String label,Color c,int dotSize,int lineThickness)
	{
		this.setDateEpochMs(new Date().getTime());
		this.setSignal(signal);
		this.setLabel(label);
		this.setDotSize(dotSize);
		this.setLineThickness(lineThickness);
		this.setColor(ColorUtil.color2String(c));
	}

	public Plot (Signal1D1D signal,String label,String comment,int dotSize,
			int lineThickness,String dotColor,String lineColor,String surfaceColor)
	{
		this.setDateEpochMs(new Date().getTime());
		this.setSignal(signal);
		this.setLabel(label);
		this.setComment(comment);
		this.setDotSize(dotSize);
		this.setLineThickness(lineThickness);
		this.setDotColor(dotColor);
		this.setLineColor(lineColor);
		this.setSurfaceColor(surfaceColor);
	}


	public boolean paint(Graphics g,Transformation2DPix t)
	{
		//if (!visible) 	System.out.println(getClass()+this.getLabel()+" visible="+this.isVisible());

		if (signal==null) return false;
		if (signal.getDim()==0) return false;
		if (this.isGrayed()) return false;
		if (!this.isVisible()) return false;
		//	if (visibility==Plot.INVISIBLE) return false;
		//	if (visibility==Plot.VISIBLE_IN_LEGEND_ONLY) return false;


		double[] real=new double[2];
		double[] real1=new double[2];
		int[] p1im;
		int[] p2im;
		int[] p3im;


		//	real[0] = t.xmin();
		//	real[1] = t.ymin();
		//int[] topLeft = t.realToImage(real);

		//	real[0] = t.xmax();
		//	real[1] = t.ymax();
		//int[] bottomRight = t.realToImage(real);


		//	if (barsStyle==VERT_BARS)
		//		{
		//		g.setColor(ColorUtil.getColor(lineColor));
		//		int yfloor=t.getDimension().height-t.bordery();
		//		for (int i=0;i<signal.getDim()-1;i++)
		//			{
		//			real[0]=signal.x(i);
		//			real[1]=signal.value(i);
		//			p1im=t.realToImage(real);
		//			real[0]=signal.x(i+1);
		//			p2im=t.realToImage(real);
		//			g.drawLine(p1im[0],p1im[1],p1im[0],yfloor);
		//			g.drawLine(p2im[0],p2im[1],p2im[0],yfloor);
		//			}
		//		}
		//
		//	if (barsStyle==COLUMNS)
		//		{
		//		g.setColor(ColorUtil.getColor(lineColor));
		//		int yfloor=t.getDimension().height-t.bordery();
		//		for (int i=0;i<signal.getDim()-1;i++)
		//			{
		//			real[0]=signal.x(i);
		//			real[1]=signal.value(i);
		//			p1im=t.realToImage(real);
		//			for (int k=-(int)lineThickness/2;k<=(int)lineThickness/2;k++)
		//				g.drawLine(p1im[0]+k,p1im[1],p1im[0]+k,yfloor);
		//			}
		//		}
		//
		//	if (barsStyle==HALF_VERT_BARS)
		//		{
		//		g.setColor(ColorUtil.getColor(lineColor));
		//		real[0]=signal.x(0);
		//		real[1]=t.yminreal();
		//		p1im=t.realToImage(real);
		//		for (int i=0;i<signal.getDim()-1;i++)
		//			{
		//			real[0]=signal.x(i);
		//			real[1]=signal.value(i);
		//			p2im=t.realToImage(real);
		//			real[0]=signal.x(i+1);
		//			p3im=t.realToImage(real);
		//			g.drawLine(p1im[0],p1im[1],p2im[0],p2im[1]);
		//			g.drawLine(p2im[0],p2im[1],p3im[0],p3im[1]);
		//			p1im=p3im;
		//			}
		//		real[1]=t.yminreal();
		//		p2im=t.realToImage(real);
		//		g.drawLine(p1im[0],p1im[1],p2im[0],p2im[1]);
		//		}
		//
		//
		//	if (barsStyle==TOP_BARS)
		//		{
		//		g.setColor(ColorUtil.getColor(lineColor));
		//		for (int i=0;i<signal.getDim()-1;i++)
		//			{
		//			real[0]=signal.x(i);
		//			real[1]=signal.value(i);
		//			p1im=t.realToImage(real);
		//			real[0]=signal.x(i+1);
		//			p2im=t.realToImage(real);
		//			g.drawLine(p1im[0],p1im[1],p2im[0],p2im[1]);
		//			}
		//		}

		if ((barsStyle==FULL_BARS)||(barsStyle==EMPTY_BARS))
		{
			int[] xPoints=new int[4];
			int[] yPoints=new int[4];

			//calculate the floor of the surface
			//get a point of the x axis
			real[0]=0;
			real[1]=0;
			real1[0]=0;
			real1[1]=0;
			p1im=t.realToImage(real);
			int lowborder=t.getDimension().height-t.bordery();//the lower border y position
			int upborder=t.bordery();//the upper border y position
			int yfloor;//the choosen floor
			yfloor=lowborder;
			if (t.ymin()>=0) yfloor=lowborder;
			if ((t.ymin()<0)&&(t.ymax()>0)) yfloor=p1im[1];
			if (t.ymax()<=0) yfloor=upborder;
			g.setColor(ColorUtil.getColor(barColor));
			if (signal.isPointsSignal()) 
				for (int i=0;i<signal.getDim();i++)
				{
					//left side of the polygon
					real[0]=signal.x(i);
					real[1]=signal.value(i);
					p1im=t.realToImage(real);

					if (i!=signal.getDim()-1)
					{
						real1[0]=signal.x(i+1);
						real1[1]=signal.value(i);
						p2im=t.realToImage(real1);
						barsThickness=(int)((p2im[0]-p1im[0])*0.8);
						//System.out.println("ecart="+(p2im[0]-p1im[0])+"   barsThickness="+barsThickness);
					}

					xPoints[0]=p1im[0]-barsThickness/2;
					yPoints[0]=yfloor;
					xPoints[1]=p1im[0]-barsThickness/2;
					yPoints[1]=p1im[1];
					//right side of the polygon
					//			real[0]=signal.x(i+1);
					//			real[1]=signal.value(i);
					//			p1im=t.realToImage(real);
					xPoints[2]=p1im[0]+barsThickness/2;
					yPoints[2]=p1im[1];
					xPoints[3]=p1im[0]+barsThickness/2;
					yPoints[3]=yfloor;
					//draw the polygon
					if (barsStyle==FULL_BARS) g.fillPolygon(xPoints,yPoints,4);
					if (barsStyle==EMPTY_BARS) g.drawPolygon(xPoints,yPoints,4);			
				}
			else
				for (int i=0;i<signal.getDim()-1;i++)
				{
					//left side of the polygon
					real[0]=signal.x(i);
					real[1]=signal.value(i);
					p1im=t.realToImage(real);
					xPoints[0]=p1im[0];
					yPoints[0]=yfloor;
					xPoints[1]=p1im[0];
					yPoints[1]=p1im[1];
					//right side of the polygon
					real1[0]=signal.x(i+1);
					real1[1]=signal.value(i);
					p2im=t.realToImage(real1);
					xPoints[2]=p2im[0];
					yPoints[2]=p2im[1];
					xPoints[3]=p2im[0];
					yPoints[3]=yfloor;
					//draw the polygon
					if (barsStyle==FULL_BARS) g.fillPolygon(xPoints,yPoints,4);
					if (barsStyle==EMPTY_BARS) g.drawPolygon(xPoints,yPoints,4);			
				}
			//draw the last cell:
			{
				//left side of the polygon
				real[0]=signal.x(signal.getDim()-1);
				real[1]=signal.value(signal.getDim()-1);
				p1im=t.realToImage(real);
				xPoints[0]=p1im[0];
				yPoints[0]=yfloor;
				xPoints[1]=p1im[0];
				yPoints[1]=p1im[1];
				//right side of the polygon
				real[0]=signal.xmax();
				real[1]=signal.value(signal.getDim()-1);
				p1im=t.realToImage(real);
				xPoints[2]=p1im[0];
				yPoints[2]=p1im[1];
				xPoints[3]=p1im[0];
				yPoints[3]=yfloor;
				//draw the polygon
				if (barsStyle==FULL_BARS) g.fillPolygon(xPoints,yPoints,4);
				if (barsStyle==EMPTY_BARS) g.drawPolygon(xPoints,yPoints,4);			
			}

		}

		Graphics2D g2 = (Graphics2D) g;
		Stroke oldStroke = g2.getStroke();

		int th=lineThickness;
		if ((selected)&&(lineThickness!=0)) th*=3;

		BasicStroke drawingStroke = new BasicStroke(th);
		g2.setStroke(drawingStroke);

		if (th!=0)
		{
			g2.setColor(ColorUtil.getColor(lineColor));
			real[0]=signal.xSample(0);
			real[1]=signal.value(0);
			p1im=t.realToImage(real);

			boolean p1Displayed = t.isInTheArea(real[0],real[1]);
			double[] lastP1 = new double[2];
			lastP1[0] = real[0];
			lastP1[1] = real[1];

			for (int i=1;i<signal.getDim();i++)
			{
				real[0]=signal.xSample(i);
				real[1]=signal.value(i);

				if (t.isInTheArea(real[0],real[1]) == false)
				{
					lastP1[0] = real[0];
					lastP1[1] = real[1];

					if (p1Displayed == false)
					{
						continue;
					}

					p1Displayed = false;
				}
				else
				{
					if (p1Displayed == false)
					{
						p1im=t.realToImage(lastP1);
					}

					p1Displayed = true;

				}

				p2im=t.realToImage(real);

				if (p2im[0] == p1im[0] && p2im[1] == p1im[1]) 
				{
					continue;
				}

				g2.drawLine(p1im[0],p1im[1],p2im[0],p2im[1]);

				//				{
				//					//arrow:
				//					double c1=0.9;
				//					double c2=0.05;
				//					int[] v=new int[2];
				//					v[0]=p2im[0]-p1im[0];
				//					v[1]=p2im[1]-p1im[1];
				//					int[] v1=new int[2];
				//					v1[0]=-v[1];
				//					v1[1]=v[0];
				//					int[] p1=new int[2];
				//					p1[0]=(int)(p1im[0]+v[0]*c1+v1[0]*c2);
				//					p1[1]=(int)(p1im[1]+v[1]*c1+v1[1]*c2);
				//					int[] p2=new int[2];
				//					p2[0]=(int)(p1im[0]+v[0]*c1-v1[0]*c2);
				//					p2[1]=(int)(p1im[1]+v[1]*c1-v1[1]*c2);
				//					g2.drawLine(p2im[0],p2im[1],p1[0],p1[1]);
				//					g2.drawLine(p2im[0],p2im[1],p2[0],p2[1]);
				//				}


				p1im=p2im;
			}

		}

		//draw the dot symbol
		if (symbol!=NO_SYMBOL) 
		{
			g.setColor(ColorUtil.getColor(dotColor));

			real[0]=(signal.x(0));
			real[1]=signal.value(0);
			p1im=t.realToImage(real);

			p2im = new int[2];
			p2im[0] = p1im[0] + 1;
			p2im[1] = p1im[1] + 1;

			for (int i=0;i<signal.getDim();i++)
			{
				//if (signal.isPointsSignal()) real[0]=(signal.x(i));
				//else 
				real[0]=(signal.xSample(i));
				real[1]=signal.value(i);

				if (t.isInTheArea(real[0],real[1]) == false)
				{
					continue;
				}

				p1im=t.realToImage(real);

				if (p2im[0] == p1im[0] && p2im[1] == p1im[1]) 
				{
					continue;
				}
//				//pour diagramme inversion lyxS microspheres
//				if (i==signal.getDim()-1)
//				{
//					int ssymbol=symbol;;
//					int sdotSize=dotSize;
//					boolean sempty=empty;
//					symbol=SQUARE;
//					dotSize=5;
//					empty=false;
//					drawSymbol(g,p1im[0],p1im[1],0);
//					symbol=ssymbol;
//					dotSize=sdotSize;
//					empty=sempty;
//					
//				}
//				else
				{
					if (error!=null)
					{
						real[0]=signal.x(i);
						real[1]=signal.y(i)-error.y(i);
						int[] pimMin=t.realToImage(real);
						int errorPix=p1im[1]-pimMin[1];
						drawSymbol(g,p1im[0],p1im[1],errorPix);
					}
					else drawSymbol(g,p1im[0],p1im[1],0);
				}
				p2im=p1im;
			}

		}

		g2.setStroke(oldStroke);

		return true;

	}

	public void drawSymbol(Graphics g,int x,int y,int errorPix)
	{
		if (dotSize==0) return;
		int dotSizeBis=dotSize;
		if (selected) dotSizeBis+=1;
		if (symbol==ROUND)
		{
			if (empty) g.drawOval(x-dotSizeBis/2,y-dotSizeBis/2,dotSizeBis,dotSizeBis);
			else g.fillOval(x-dotSizeBis/2,y-dotSizeBis/2,dotSizeBis,dotSizeBis);
			if (shining)
			{
				g.setColor(Color.white);
				g.fillOval(x-0*dotSizeBis/4,y-0*dotSizeBis/4,dotSizeBis/3,dotSizeBis/3);
				g.setColor(ColorUtil.getColor(dotColor));
			}
		}
		else if (symbol==SQUARE)
		{
			if (empty) g.drawRect(x-dotSizeBis/2,y-dotSizeBis/2,dotSizeBis,dotSizeBis);
			else g.fillRect(x-dotSizeBis/2,y-dotSizeBis/2,dotSizeBis,dotSizeBis);	
		}
		else if (symbol==CROSS)
		{
			g.drawLine(x-dotSizeBis/2 - 1, y-dotSizeBis/2 -1 , x + dotSizeBis/2 + 1 , y + dotSizeBis / 2 + 1);
			g.drawLine(x-dotSizeBis/2 -1, y+dotSizeBis/2 +1, x + dotSizeBis/2 + 1, y - dotSizeBis / 2 - 1);
		}
		else if (symbol==TRIANGLE)
		{
			Polygon point = new Polygon();
			point.addPoint(x - dotSizeBis/2, y +dotSizeBis /2);
			point.addPoint(x, y - dotSizeBis / 2);
			point.addPoint(x + dotSizeBis / 2, y + dotSizeBis/2);
			if (empty) g.drawPolygon(point);
			else g.fillPolygon(point);
		}

		if (errorPix!=0) //draw error bar
		{
			g.drawLine(x,y-errorPix,x,y+errorPix);
			g.drawLine(x-5,y-errorPix,x+5,y-errorPix);
			g.drawLine(x-5,y+errorPix,x+5,y+errorPix);

		}

	}
	public void updateBoundaries(Vecteur2D Pmin,Vecteur2D Pmax)
	{
		updateBoundariesX( Pmin, Pmax);
		updateBoundariesY( Pmin, Pmax);
	};

	/**
	 * get the occupancy rectangle. Update the point Pmin and Pmax if bigger
	 */
	public void updateBoundariesX(Vecteur2D Pmin,Vecteur2D Pmax)
	{
		if( signal==null) return;
		if (signal.getDim()==0) return;
		double r=signal.xmin();
		if (r<Pmin.x()) Pmin.setX(r);
		r=signal.xmax();
		if (r>Pmax.x()) Pmax.setX(r);
	}

	/**
	 * get the occupancy rectangle. Update the point Pmin and Pmax if bigger
	 */
	public void updateBoundariesY(Vecteur2D Pmin,Vecteur2D Pmax)
	{
		if( signal==null) return;
		if (signal.getDim()==0) return;
		double r=signal.ymin();
		if (error!=null) r-=error.ymax();
		if (r<Pmin.y()) Pmin.setY(r);
		r=signal.ymax();
		if (error!=null) r+=error.ymax();
		if (r>Pmax.y()) Pmax.setY(r);
	}

	/**
	 * get the occupancy rectangle in x. Update the point Pmin and Pmax if bigger
	 */
	public void updateBoundariesXStrictPositive(Vecteur2D Pmin,Vecteur2D Pmax)
	{
		if( signal==null) return;
		if (signal.getDim()==0) return;
		double r=signal.xminStrictPositive();
		if (r<Pmin.x()) Pmin.setX(r);
		r=signal.xmax();
		if (r>Pmax.x()) Pmax.setX(r);
	}


	/**
	 * get the occupancy rectangle in y. Update the point Pmin and Pmax if bigger
	 */
	public void updateBoundariesYStrictPositive(Vecteur2D Pmin,Vecteur2D Pmax)
	{
		if( signal==null) return;
		double r=signal.yminStrictPositive();
		if (signal.getDim()==0) return;
		if (r<Pmin.y()) Pmin.setY(r);
		r=signal.ymax();
		if (r>Pmax.y()) Pmax.setY(r);
	}


	public boolean isSelected()
	{
		return selected;
	}

	public void setSelected(boolean b)
	{
		selected=b;
	}

	public Signal1D1D getSignal() 
	{
		return signal;
	}

	/**
	 * error bar
	 * @return
	 */
	public Signal1D1D getError() 
	{
		return error;
	}



	public int getLineThickness() {
		return lineThickness;
	}


	public void setLineThickness(int th) {
		lineThickness=th;

	}


	public void showLine(boolean b) {
		showLine=b;

	}


	//
	//public int getVisibility()
	//{
	//	return visibility;
	//}
	//public void setVisibility(int visibility)
	//{
	//	this.visibility = visibility;
	//}


	/**
	 * set the color of dots line and surface
	 * @param newcolour
	 */
	public void setColor(String newcolour) 
	{
		lineColor=newcolour;
		surfaceColor=newcolour;
		dotColor=newcolour;	
	}

	public void setColor(Color c)
	{
		String newcolour=ColorUtil.color2String(c);
		lineColor=newcolour;
		surfaceColor=newcolour;
		dotColor=newcolour;	
	}


	public boolean isVisible()
	{
		return visible;
	}


	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}


	public boolean isGrayed()
	{
		return grayed;
	}
	public void setGrayed(boolean grayed)
	{
		this.grayed = grayed;
	}
	public void setNextDotStyle() {
		symbol++;
		if (symbol>=NB_SYMBOLS) symbol=0;

	}

	public void setDotStyle(int style) 
	{
		symbol=style;
	}

	public int getDotSize() {

		return dotSize;
	}


	public void setDotSize(int size) {
		dotSize=size;

	}


	public boolean getEmpty() {

		return empty;
	}


	public void setEmpty(boolean b) {
		empty=b;

	}




	/**
	 * return a String with the plots data in columns , 
	 * first row with labels ready to paste in excel or openoffice
	 * @param plots
	 * @return
	 */
	public static String getPlotsDataForExcel(Vector<Plot> plots)
	{
		StringBuffer sb=new StringBuffer();
		//find the nb of rows	
		int nbr=0;
		for (Plot plot:plots) 
		{
			//Plot plot=(Plot)plots[i];
			com.photonlyx.toolbox.math.signal.Signal1D1D signal=plot.getSignal();
			if (signal.getDim()>nbr) nbr=signal.getDim();
		}

		//print the comment
		int i=0;
		for (Plot plot:plots)
		{
			//Plot plot=(Plot)plots[i];
			String s=plot.getComment();
			if (s==null) s="-"+i;
			if (s.length()==0) s="-";
			i++;
			sb.append("Comment:"+"\t"+s+"\t");
		}
		sb.append("\n");

		//print the label
		i=0;
		for (Plot plot:plots)
		{
			//Plot plot=(Plot)plots[i];
			String s=plot.getLabel();
			if (s==null) s="plot"+i;
			if (s.length()==0) s="plot"+i;
			sb.append("x"+"\t"+s+"\t");
			if (plot.getError()!=null)  sb.append("sigma"+"\t");

			i++;
		}
		sb.append("\n");

		for (int k=0;k<nbr;k++)
		{
			for (Plot plot:plots)
			{
				Signal1D1D signal=plot.getSignal();
				Signal1D1D error=plot.getError();
				if (k<signal.getDim())	sb.append(signal.x(k)+"\t"+signal.y(k)+"\t");
				if (error!=null)  sb.append(error.y(i)+"\t");

			}
			sb.append("\n");
		}

		return sb.toString();
	}


	//public String toXML() 
	//{
	//NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
	//nf.setMaximumFractionDigits(10);
	//nf.setMinimumFractionDigits(10);
	//nf.setGroupingUsed(false);
	//StringBuffer sb=new StringBuffer();
	//sb.append("<"+getClass()+" \n");
	//
	//sb.append("lineColor=\""+lineColor+"\"\n");
	//sb.append("surfaceColor=\""+surfaceColor+"\"\n");
	//sb.append("dotColor=\""+dotColor+"\"\n");
	//sb.append("empty=\""+empty+"\"\n");
	//sb.append("dotSize=\""+dotSize+"\"\n");
	//sb.append("shining=\""+shining+"\"\n");
	//sb.append("symbol=\""+symbol+"\"\n");
	//sb.append("showLine=\""+showLine+"\"\n");
	//sb.append("lineThickness=\""+lineThickness+"\"\n");
	//sb.append("barsStyle=\""+barsStyle+"\"\n");
	//sb.append("label=\""+label+"\"\n");
	//sb.append("comment=\""+comment+"\"\n");
	//sb.append(signal.toXML()+"\n");
	//sb.append("</"+getClass()+"> ");
	//return sb.toString();
	//}

	public XMLElement toXML() 
	{
		XMLElement el = new XMLElement();	
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("lineColor", lineColor);
		el.setAttribute("surfaceColor", surfaceColor);
		el.setAttribute("dotColor", dotColor);
		el.setAttribute("barColor", barColor);
		el.setAttribute("empty", empty);
		el.setIntAttribute("dotSize", dotSize);
		el.setAttribute("shining", shining);
		el.setIntAttribute("symbol", symbol);
		el.setAttribute("showLine", showLine);
		el.setIntAttribute("lineThickness", lineThickness);
		el.setIntAttribute("barsStyle", barsStyle);
		el.setAttribute("label", label);
		el.setAttribute("comment", comment);
		el.addChild(signal.toXML());
		if (error!=null) el.addChild(error.toXML());
		el.setDoubleAttribute("dateEpochMs", dateEpochMs);
		return el;
	}


	public void updateFromXMLString(String xmlstring) 
	{
		XMLElement xml = new XMLElement();
		xml.parseString(xmlstring);
		this.updateFromXML(xml);
	}


	public void updateFromXML(XMLElement xml) 
	{
		lineColor=xml.getStringAttribute("lineColor");	
		surfaceColor=xml.getStringAttribute("surfaceColor");	
		dotColor=xml.getStringAttribute("dotColor");	
		barColor=xml.getStringAttribute("barColor","red");	
		empty=xml.getBooleanAttribute("empty","true","false",false);	
		dotSize=xml.getIntAttribute("dotSize");	
		shining=xml.getBooleanAttribute("shining","true","false",false);	
		symbol=xml.getIntAttribute("symbol");	
		showLine=xml.getBooleanAttribute("showLine","true","false",false);	
		lineThickness=xml.getIntAttribute("lineThickness");	
		barsStyle=xml.getIntAttribute("barsStyle");	
		label=xml.getStringAttribute("label");	
		comment=xml.getStringAttribute("comment");	
		dateEpochMs=(long)xml.getDoubleAttribute("dateEpochMs",-1);	
		//System.out.println("label"+label);
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		Signal1D1DXY signal=new Signal1D1DXY();
		signal.updateFromXML(enumSons.nextElement());
		if (enumSons.hasMoreElements())
		{
			//get the error bars
			error=new Signal1D1DXY();
			error.updateFromXML(enumSons.nextElement());
		}
		setSignal(signal);
	}




	public String toStringJustData() {

		return signal.toStringJustData();
	}

	public String getLineColor() {
		return lineColor;
	}


	public void setLineColor(String lineColor) {
		this.lineColor = lineColor;
	}


	public String getSurfaceColor() {
		return surfaceColor;
	}


	public void setSurfaceColor(String surfaceColor) {
		this.surfaceColor = surfaceColor;
	}


	public boolean isShining() {
		return shining;
	}


	public void setShining(boolean shining) {
		this.shining = shining;
	}


	public int getSymbol() {
		return symbol;
	}


	public void setSymbol(int symbol) {
		this.symbol = symbol;
	}


	public boolean isShowLine() {
		return showLine;
	}


	public void setShowLine(boolean showLine) {
		this.showLine = showLine;
	}


	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getComment() {
		return comment;
	}


	public long getDateEpochMs()
	{
		return dateEpochMs;
	}
	public void setDateEpochMs(long dateEpochMs)
	{
		this.dateEpochMs = dateEpochMs;
	}
	public String getDotColor() {
		return dotColor;
	}


	public void setDotColor(String dotColor) {
		this.dotColor = dotColor;
	}


	public String getBarColor()
	{
		return barColor;
	}
	public void setBarColor(String barColor)
	{
		this.barColor = barColor;
	}
	public int getBarsStyle() {
		return barsStyle;
	}

	/**
	 * set the next bar style
	 */
	public void changeBarsStyle()
	{
		int ii=getBarsStyle()+1;
		if (ii>2) ii=0;
		setBarsStyle(ii);
	}


	/**
	 * Plot.FULL_BARS or Plot.EMPTY_BARS
	 * @param barsStyle
	 */
	public void setBarsStyle(int barsStyle) {
		this.barsStyle = barsStyle;
	}


	public int getBarsThickness()
	{
		return barsThickness;
	}
	public void setBarsThickness(int barsThickness)
	{
		this.barsThickness = barsThickness;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}


	public void setSignal(Signal1D1D signal) {
		this.signal = signal;
	}
	public void setError(Signal1D1D e) {
		this.error = e;
	}
	public boolean isErasable() {
		return erasable;
	}
	public void setErasable(boolean erasable) {
		this.erasable = erasable;
	}


	/**
	 * copy all the data of the plot p 
	 * @return
	 */
	public void copy(Plot p)
	{
		this.setSignal(p.getSignal().transfertTo1D1DXY());//make a full copy
		this.setLabel(p.getLabel());
		this.setComment(p.getComment());
		this.setDotSize(p.getDotSize());
		this.setLineThickness(p.getLineThickness());
		this.setDotColor(p.getDotColor());
		this.setLineColor(p.getLineColor());
		this.setSurfaceColor(p.getSurfaceColor());
		this.setDateEpochMs(p.getDateEpochMs());
	}

	/**
	 * get a copy
	 * @return
	 */
	public Plot copy()
	{
		Plot p=new Plot();
		p.copy(this);
		return p;
	}

	public Plot getAcopy()
	{
		return copy();
	}

	/**
	 * @Deprecated
	 * @return
	 */
	public long getDate()
	{
		long datems=0;
		double dateInComment=new Definition(getComment()).searchValue("date", -1, false);
		if (dateInComment!=-1)	datems=(long)dateInComment;	else datems=(long)getDateEpochMs();
		return datems;
		//return (long)new Definition(this.getComment()).searchValue("date", 0, false);
	}

}




