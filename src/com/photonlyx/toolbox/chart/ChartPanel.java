package com.photonlyx.toolbox.chart;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.Border;

import com.photonlyx.toolbox.chartApp.ChartApp;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.util.Global;

public class ChartPanel extends JPanel
{
	private WindowApp app;
	private Chart chart=new Chart();
	private Legend legend=new Legend(chart);
	private PlotSet ps=new PlotSet();
	private JSplitPane splitPane ;
	private JPanel panelSide;

	public ChartPanel()
	{
		init();
	}

	public ChartPanel(Signal1D1D s,String label)
	{
		init();
		Plot plot=new Plot(s);
		plot.setLabel(label);
		ps.add(plot);
		chart.repaint();
	}


	public ChartPanel(Signal1D1D s)
	{
		init();
		Plot plot=new Plot(s);
		plot.setLabel("signal");
		ps.add(plot);
		chart.repaint();
	}

	public ChartPanel(Plot p)
	{
		init();
		ps.add(p);
		chart.repaint();
	}

	public ChartPanel(PlotSet ps2)
	{
		init();
		chart.add(ps2);
		chart.repaint();
	}

	public ChartPanel(Signal2D1D s)
	{
		init();
		chart.setSignal2D1D(s);
		showPalette();
	}


	private void init()
	{
		//create chart and legend:
		legend.setChart(chart);
		legend.updateGraphics();
		chart.setChartPanel(this);
		chart.setTitle(" ");
		chart.setFixedLimitY(false);
		//c.setYmin(0);
		//c.setYmax(6);
		chart.setFixedLimitX(false);
		chart.setXmin(0);
		chart.setXmax(23);//make a PlotSet

		chart.add(ps);


		//split pane for graph and legend:
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, chart ,legend.getComponent());
		splitPane.setDividerLocation(500);
		splitPane.setDividerSize(8);
		splitPane.setOneTouchExpandable(true); 
		splitPane.setContinuousLayout(true); 
		splitPane.setResizeWeight(1);
		Border empty = BorderFactory.createEmptyBorder();
		splitPane.setBorder(empty);

		this.setLayout(new BorderLayout());

		//panel at side
		panelSide=new JPanel();
		panelSide.setPreferredSize(new Dimension(150, 200));
		panelSide.setLayout(new FlowLayout());
		panelSide.setBackground(Global.background);
		//add the side panel at right
		this.add(panelSide,BorderLayout.WEST);

		this.add(splitPane);

		//pc.add(l.getMainPanel(),BorderLayout.EAST);
		chart.update();
		legend.update();


	}

	
	public void add(PlotSet plotset)
	{
		chart.add(plotset);
		chart.repaint();
		legend.update();
	}

	public void add(Signal1D1D s)
	{
		Plot plot=new Plot(s);
		plot.setLabel("signal");
		ps.add(plot);
		chart.repaint();
		legend.update();
	}

	public Plot add(Signal1D1D s,String label)
	{
		Plot plot=new Plot(s);
		plot.setLabel(label);
		ps.add(plot);
		chart.repaint();
		legend.update();
		return plot;
	}

	public void add(Plot plot)
	{
		ps.add(plot);
		chart.repaint();
		legend.update();

	}

	public Plot add(Signal1D1D s,String label,String color,int lineThickness,int dotSize)
	{
		Plot plot=new Plot(s);
		plot.setLabel(label);
		plot.setColor(color);
		plot.setLineThickness(lineThickness);
		plot.setDotSize(dotSize);
		ps.add(plot);
		chart.repaint();
		legend.update();
		return plot;
	}

	public void setSignal2D1D(Signal2D1D s)
	{
		//sp.remove(l.getComponent());
		chart.setSignal2D1D(s);
		//sp.add(chart.getPalette().getVisualisation());
		chart.repaint();
	}


	public void update()
	{
		//System.out.println("repaint");
		chart.repaint();
		legend.update();
		panelSide.validate();
		panelSide.repaint();
	}


	public Chart getChart()
	{
		return chart;
	}



	public PlotSet getPlotSet()
	{
		return ps;
	}



	public JSplitPane getSplitPane()
	{
		return splitPane;
	}

	public JPanel getPanelSide()
	{
		return panelSide;
	}

	public void addParams(ParamsBox box2)
	{
		panelSide.add(box2.getComponent());
	}

	public void hideLegend()
	{
		splitPane.remove(legend.getComponent());
		splitPane.remove(chart.getPalette().getVisualisation());
	}

	public void hidePanelSide()
	{
		this.remove(panelSide);
		this.validate();
	}

	public void showPalette()
	{
		splitPane.remove(legend.getComponent());
		splitPane.add(chart.getPalette().getVisualisation(),1);
		splitPane.setDividerLocation(0.9);
		chart.repaint();
	}
	public void showLegend()
	{
		splitPane.remove(chart.getPalette().getVisualisation());
		splitPane.add(legend.getComponent());
		chart.repaint();
	}

	public void hidePalette()
	{
		splitPane.remove(chart.getPalette().getVisualisation());
	}

	public void setPaletteVisible(boolean b)
	{
		if (b) this.showPalette();
		else this.hidePalette();
	}


	public boolean isPaletteVivible()
	{
		Component[] cs=splitPane.getComponents();
		for (Component c:cs) if (c==chart.getPalette().getVisualisation()) return true;
		return false;
	}

	public void putPanelSideAtNorth()
	{
		this.remove(panelSide);
		this.add(panelSide,BorderLayout.NORTH);
	}

	public void saveToPNG(String path,String filename,boolean verbose)
	{
		BufferedImage bi = new BufferedImage(this.getSize().width, this.getSize().height, BufferedImage.TYPE_INT_ARGB); 
		Graphics g = bi.createGraphics();
		this.paint(g);  //this == JComponent
		g.dispose();
		try{ImageIO.write(bi,"png",new File(path+File.separator+filename));}catch (Exception e1) {}
		if (verbose) System.out.println("Saved image capture in "+path+File.separator+filename);
	}
	


	public WindowApp getApp() {
		return app;
	}

	public void setApp(WindowApp app) {
		this.app = app;
	}

	public static void main(String[] args) 
	{
		ChartApp chartApp=new ChartApp();
		ChartPanel cp=chartApp.getChartPanel();
		Chart c=cp.getChart();
		//cp.getSplitPane().setDividerLocation(0.6);
		chartApp.getCJFrame().setSize(new Dimension(1000,700));
		
		Signal1D1DXY s=new Signal1D1DXY();
		s.addPoint(0.1, 0.1);
		s.addPoint(1.1, 1.1);
		s.addPoint(2.1, 2.1);
		s.addPoint(4.1, 5.1);
		s.addPoint(7.1, 1.1);
		s.addPoint(8.5, 3.1);
		Plot p=new Plot(s);
		p.setLabel("toto1");
		//p.setBarsStyle(Plot.EMPTY_BARS);
		p.setLineThickness(3);
		c.add(p);
		
		c.getLegend().setSeeFilter(false);
		cp.update();
	
	}
		

}