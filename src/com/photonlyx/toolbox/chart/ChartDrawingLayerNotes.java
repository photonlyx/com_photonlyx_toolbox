package com.photonlyx.toolbox.chart;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.Enumeration;
import java.util.Vector;


import nanoxml.XMLElement;

public class ChartDrawingLayerNotes implements ChartDrawingLayer
{	
private Vector<Note> notes=new Vector<Note>();
private Chart c;

public ChartDrawingLayerNotes(Chart c)
{
this.c=c;	
//add(new Note(c,new double[2],"This is a note"));
}


@Override
public void mouseClicked(MouseEvent arg0) 
{
	//System.out.println(this.getClass());	
	for (Note n:notes) n.mouseClicked( arg0) ;
}

@Override
public void mouseEntered(MouseEvent arg0) {
	for (Note n:notes) n.mouseEntered( arg0) ;
	
}

@Override
public void mouseExited(MouseEvent arg0) {
	for (Note n:notes) n.mouseExited( arg0) ;
	
}

@Override
public void mousePressed(MouseEvent arg0) {
	for (Note n:notes) n.mousePressed( arg0) ;
	
}

@Override
public void mouseReleased(MouseEvent arg0) {
	for (Note n:notes) n.mouseReleased( arg0) ;
	
}

@Override
public void mouseDragged(MouseEvent arg0) 
{
//System.out.println(this.getClass()+" dragged");
for (Note n:notes) n.mouseDragged( arg0) ;	
}

@Override
public void mouseMoved(MouseEvent arg0) {
	for (Note n:notes) n.mouseMoved( arg0) ;
	
}

@Override
public void mouseWheelMoved(MouseWheelEvent arg0) {
	for (Note n:notes) n.mouseWheelMoved( arg0) ;
	
}


@Override
public void draw(Graphics g, Chart c) 
	{
	for (Note n:notes) n.draw(g, c);
	
	}


public void add(Note note) {
	notes.add(note);
	
}
	
public void remove(Note note) {
	notes.remove(note);
	
}


@Override
public void keyPressed(KeyEvent arg0) {
	for (Note n:notes) n.keyPressed( arg0) ;
	
}


@Override
public void keyReleased(KeyEvent arg0) {
	for (Note n:notes) n.keyReleased( arg0) ;
	
}


@Override
public void keyTyped(KeyEvent arg0) {
	for (Note n:notes) n.keyTyped( arg0) ;
	
}
		
public XMLElement toXML() 
{
XMLElement el = new XMLElement();
el.setName(getClass().toString().replace("class ", ""));
//add notes:
for (Note n:notes) el.addChild(n.toXML());
return el;
}


public void updateFromXML(XMLElement xml)
{
notes.removeAllElements();
if (xml==null) 
	{
	return;
	}
Enumeration<XMLElement> enumSons = xml.enumerateChildren();
while (enumSons.hasMoreElements())
	{
	XMLElement el=enumSons.nextElement();
	Note n=new Note(c);
	n.updateFromXML(el);
	this.add(n);
	}
}	

}
