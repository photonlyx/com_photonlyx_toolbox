package com.photonlyx.toolbox.chart;

import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;

import javax.swing.JPanel;



public interface ChartDrawingLayer extends MouseListener,MouseMotionListener,MouseWheelListener, KeyListener
{

public void draw(Graphics g,Chart c);


}
