package com.photonlyx.toolbox.chart;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

import com.photonlyx.toolbox.math.analyse.FitLin;
import com.photonlyx.toolbox.math.analyse.FitPolynomeN;
import com.photonlyx.toolbox.math.analyse.LinearInterpolation;
import com.photonlyx.toolbox.math.function.usual.Line;
import com.photonlyx.toolbox.math.function.usual.PolynomeN;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.math.spline.ControlPoints1D1DFitLeastSquare1D;
import com.photonlyx.toolbox.math.spline.SplineCubic;



public class PlotProcessing 
{




public static Plot meanOfPlots(Vector<Plot> plots)
{
Vector<LinearInterpolation> interps=new Vector<LinearInterpolation>();
for (Plot plot:plots)
	{
	interps.add( new LinearInterpolation(plot.getSignal()));
	}
Signal1D1D masterSignal=plots.elementAt(0).getSignal();//the sampling is taken from this signal
Signal1D1DXY mean=new Signal1D1DXY(masterSignal.getDim());
for (int i=0;i<mean.getDim();i++)
	{
	double r=0;
	double x=masterSignal.x(i);
	for (LinearInterpolation f:interps)
		{
		r+=f.f(x);
		}
	r/=interps.size();
	mean.setValue(i,x,r);
	}	
Plot plot2=new Plot(mean);
plot2.setColor(plots.elementAt(0).getDotColor());
plot2.setDotSize(plots.elementAt(0).getDotSize());
plot2.setLineThickness(plots.elementAt(0).getLineThickness());
plot2.setLabel("mean");
return plot2;
}

public static Plot subtractTwoPLots(Plot plot1,Plot plot2)
{
Signal1D1D s1,s2;//will perform s1-s2
s1=plot1.getSignal();
s2=plot2.getSignal();
Signal1D1DXY diff=new Signal1D1DXY(s1.getDim());
LinearInterpolation interp2=new LinearInterpolation(s2);
for (int i=0;i<s1.getDim();i++)
	{
	double r=s1.y(i)-interp2.f(s1.x(i));
	diff.setValue(i,s1.x(i),r);
	}

Plot plotDiff=new Plot(diff);
plotDiff.setColor(plot1.getDotColor());
plotDiff.setLabel(plot1.getLabel()+" - "+plot2.getLabel());
return plotDiff;	
}


/**
merge the selected plots in one new plot, sort is done
@return the new JoloPlotXY added
*/
public static  Plot mergeSelectedPlots(Vector<Plot> plots)
{
Signal1D1DXY signal=new Signal1D1DXY();
String newlabel="";
for (Plot p:plots)
	{
	if (p.isSelected())
		{
		signal.merge(p.getSignal().transfertTo1D1DXY());
		newlabel+="_"+p.getLabel();
		}
	}
//sort with respect to the x values
signal._sort();
//create and add a new plot
Plot plot=new Plot(signal);
//change the label of the new plot
plot.setLabel(newlabel);
return plot;
}


/**
 * return a new plot with a line fitted to the data of plot 	
 * @param plot
 * @param logx
 * @param logy
 * @return
 */
public static Plot fitLine(Plot plot,boolean logx,boolean logy)
{
Signal1D1DXY signal=plot.getSignal().transfertTo1D1DXY();
if (logx) signal._log10TheX();
if (logy) signal._log10TheY();
FitLin fitlin=new FitLin(signal);
fitlin.work();
Line line=fitlin.getLine();
NumberFormat nf=new DecimalFormat("0.000E0");
String comment;
if (logx&!logy)
	{
	comment="y = "+nf.format(line.a())+" log10x + "+nf.format(line.b());
	}
else if (logy&!logx)
	{
	comment="log10y = "+nf.format(line.a())+" x + "+nf.format(line.b());
	}
else if (logy&logx)
	{
	comment="log10y = "+nf.format(line.a())+" log10x + "+nf.format(line.b());
	}
else 
	{
	comment="y="+nf.format(line.a())+"x+"+nf.format(line.b());
	}
//String label="fit of "+plot.getLabel();
SignalDiscret1D1D signalFit1=new SignalDiscret1D1D(signal.xmin(),signal.xmax(),20,line);
Signal1D1DXY signalFit2=signalFit1.transfertTo1D1DXY();
if (logx) signalFit2._p10TheX();
if (logy) signalFit2._p10TheY();
//String color=plot.getDotColor();
Plot plotFit=new Plot(signalFit2);
plotFit.setColor(plot.getDotColor());
plotFit.setLabel(plot.getLabel()+" fit "+comment);
plotFit.setDotSize(0);
//plotFit.setComment(comment);
return plotFit;
}


/**
 * return a new plot with a parabol fitted to the data of plot 	
 * @param plot
 * @param logx
 * @param logy
 * @return
 */
public static Plot fitParabol(Plot plot,boolean logx,boolean logy)
{
Signal1D1DXY signal=plot.getSignal().transfertTo1D1DXY();
if (logx) signal._log10TheX();
if (logy) signal._log10TheY();

PolynomeN pol=FitPolynomeN.fitPolynomeN(signal,2);


String comment;
if (logx&!logy)
	{
	comment=pol.toString("logx","0.000E0");
	}
else if (logy&!logx)
	{
	comment="log10y = "+pol.toString("x","0.000E0");
	}
else if (logy&logx)
	{
	comment="log10y = "+pol.toString("logx","0.000E0");
	}
else 
	{
	comment="y="+pol.toString("x","0.000E0");
	}
String label="fit of "+plot.getLabel();
SignalDiscret1D1D signalFit1=new SignalDiscret1D1D(signal.xmin(),signal.xmax(),20,pol);
Signal1D1DXY signalFit2=signalFit1.transfertTo1D1DXY();
if (logx) signalFit2._p10TheX();
if (logy) signalFit2._p10TheY();
String color=plot.getDotColor();
Plot plotFit=new Plot(signalFit2);
plotFit.setColor(plot.getDotColor());
plotFit.setLabel(plot.getLabel()+" fit "+comment);
plotFit.setDotSize(0);
//plotFit.setComment(comment);
return plotFit;
}


/**
 * return a new plot with a polynome fitted to the data of plot 	
 * @param plot
 * @param logx
 * @param logy
 * @param order order of the polynome
* @return
 */
public static Plot fitPolynome(Plot plot,boolean logx,boolean logy,int order)
{
Signal1D1DXY signal=plot.getSignal().transfertTo1D1DXY();
if (logx) signal._log10TheX();
if (logy) signal._log10TheY();

PolynomeN pol=FitPolynomeN.fitPolynomeN(signal,order);


String comment;
if (logx&!logy)
	{
	comment=pol.toString("logx","0.000000000E0");
	}
else if (logy&!logx)
	{
	comment="log10y = "+pol.toString("x","0.000000000E0");
	}
else if (logy&logx)
	{
	comment="log10y = "+pol.toString("logx","0.000000000E0");
	}
else 
	{
	comment="y="+pol.toString("x","0.000000000E0");
	}
String label="fit of "+plot.getLabel();
SignalDiscret1D1D signalFit1=new SignalDiscret1D1D(signal.xmin(),signal.xmax(),100,pol);
Signal1D1DXY signalFit2=signalFit1.transfertTo1D1DXY();
if (logx) signalFit2._p10TheX();
if (logy) signalFit2._p10TheY();
Plot plotFit=new Plot(signalFit2);
plotFit.setColor(plot.getDotColor());
plotFit.setLabel(plot.getLabel()+" fit "+comment);
plotFit.setDotSize(0);
//plotFit.setComment(comment);
return plotFit;
}


public static Plot interpolateWithCubicSpline(Plot plot,boolean logx,boolean logy)
{
Signal1D1DXY signal=plot.getSignal().transfertTo1D1DXY();
if (logx) signal._log10TheX();
if (logy) signal._log10TheY();

SplineCubic sp=new SplineCubic(signal);
sp.update();
SignalDiscret1D1D signalFit1=new SignalDiscret1D1D(signal.xmin(),signal.xmax(),100,sp);
Signal1D1DXY signalFit2=signalFit1.transfertTo1D1DXY();
if (logx) signalFit2._p10TheX();
if (logy) signalFit2._p10TheY();
Plot plotFit=new Plot(signalFit2);
plotFit.setColor(plot.getDotColor());
plotFit.setLabel(plot.getLabel()+" interp cubic spline");
plotFit.setDotSize(0);
return plotFit;
}


public static Plot fitCubicSpline(Plot plot,boolean logx,boolean logy,int nbCtrl)
{
Signal1D1DXY signal=plot.getSignal().transfertTo1D1DXY();
if (logx) signal._log10TheX();
if (logy) signal._log10TheY();

SplineCubic sp=new SplineCubic(nbCtrl);
ControlPoints1D1DFitLeastSquare1D fit= new ControlPoints1D1DFitLeastSquare1D(signal,sp,true,5,true);
fit.update();
SignalDiscret1D1D signalFit1=new SignalDiscret1D1D(signal.xmin(),signal.xmax(),signal.getDim()*5,sp);
Signal1D1DXY signalFit2=signalFit1.transfertTo1D1DXY();
if (logx) signalFit2._p10TheX();
if (logy) signalFit2._p10TheY();
String color=plot.getDotColor();
Plot plotFit=new Plot(signalFit2);
plotFit.setColor(plot.getDotColor());
plotFit.setLabel(plot.getLabel()+" cubic spline");
plotFit.setDotSize(0);
return plotFit;
}


}
