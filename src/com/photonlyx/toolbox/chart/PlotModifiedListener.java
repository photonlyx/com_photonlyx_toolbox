package com.photonlyx.toolbox.chart;

public interface PlotModifiedListener
{
	
public void plotModified(ChartEvent e);

}
