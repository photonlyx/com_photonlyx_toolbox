package com.photonlyx.toolbox.chart;

import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal2D1D;

public class ChartWindow  
{
	private CJFrame cjframe	=new CJFrame();
	private JTabbedPane topTabPane;

	public ChartWindow()
	{
		init();
		topTabPane.add(new ChartPanel());
		update();
	}


	public ChartWindow(ChartPanel cp,String panelTitle)
	{
		init();
		topTabPane.add(cp,panelTitle);
		update();
	}

	public ChartWindow(Signal1D1D s)
	{
		init();
		topTabPane.add(new ChartPanel(s));
		update();
	}
	public ChartWindow(Signal1D1D s,String label)
	{
		init();
		topTabPane.add(new ChartPanel(s,label));
		update();
	}

	public ChartWindow(Plot p)
	{
		init();
		topTabPane.add(new ChartPanel(p));
		update();
	}

	public ChartWindow(PlotSet ps)
	{
		init();
		topTabPane.add(new ChartPanel(ps));
		update();
	}

	public ChartWindow(Signal2D1D s)
	{
		init();
		topTabPane.add(new ChartPanel(s));
		update();
	}

	public ChartWindow(Signal2D1D s,String title)
	{
		init();
		topTabPane.add(new ChartPanel(s));
		cjframe.setTitle("");
		topTabPane.setTitleAt(0, title);
		update();
	}

	private ChartPanel getChartPanel(int index)
	{
		return ((ChartPanel)topTabPane.getComponent(index));
	}

	public ChartPanel getLastChartPanel()
	{
		return ((ChartPanel)topTabPane.getComponent(topTabPane.getComponentCount()-1));
	}

	public void removeAllPanels()
	{
		topTabPane.removeAll();
	}

	public void add(PlotSet ps)
	{
		if (topTabPane.getComponentCount()==0) topTabPane.add(new ChartPanel());
		getChartPanel(0).getChart().add(ps);
		update();
	}

	public void add(Plot p)
	{
		if (topTabPane.getComponentCount()==0) topTabPane.add(new ChartPanel());
		getChartPanel(0).getChart().add(p);
		update();
	}

	private void init()
	{
		//create frame and add the chart and and legend in a border layout:
		cjframe.setTitle("Chart");
		cjframe.setSize(600, 400);
		cjframe.setLocation(new Point(30,30));
		cjframe.setExitAppOnExit(false);
		topTabPane=new JTabbedPane();
		cjframe.add(topTabPane);
	}

	/**
	 * get panel 0
	 * @return
	 */
	public ChartPanel getChartPanel(){return getChartPanel(0);}

	/**
	 * add a Signal2D1D, if there is already one add a new pane
	 * @param s
	 * @param title
	 * @return the involved ChartPanel
	 */
	public ChartPanel  add(Signal2D1D s,String title)
	{
		ChartPanel cp;
		//if (topTabPane.getComponentCount()==0) topTabPane.add(new ChartPanel());
		//if (getChartPanel(0).getChart().getSignal2D1D()==null) 
		//	{
		//	cp=getChartPanel(0);
		//	cp.getChart().setSignal2D1D(s);
		//	topTabPane.setTitleAt(0, title);
		//	cp.showPalette();
		//	}
		//else
		//	{
		//	topTabPane.add(cp=new ChartPanel(s),title);	
		//	}
		topTabPane.add(cp=new ChartPanel(s),title);	
		return cp;
	}


	public void add(Signal1D1D s)
	{
		if (topTabPane.getComponentCount()==0) topTabPane.add(new ChartPanel());
		Plot plot=new Plot(s);
		plot.setLabel("signal");
		getChartPanel(0).getChart().add(plot);
		update();
	}

	public void add(Signal1D1D s,String label)
	{
		if (topTabPane.getComponentCount()==0) topTabPane.add(new ChartPanel());
		Plot plot=new Plot(s);
		plot.setLabel(label);
		getChartPanel(0).getChart().add(plot);
		update();
	}

	public void add(Signal1D1D s,String label,String color,int lineThickness,int dotSize)
	{
		if (topTabPane.getComponentCount()==0) topTabPane.add(new ChartPanel());
		Plot plot=new Plot(s);
		plot.setLabel(label);
		plot.setColor(color);
		plot.setLineThickness(lineThickness);
		plot.setDotSize(dotSize);
		getChartPanel(0).getChart().add(plot);
		getChartPanel(0).getChart().repaint();
	}
	

	public void addPanel(JPanel jp,String title)
	{
		topTabPane.add(jp,title);
	}


	public void update()
	{
		//if (topTabPane.getComponentCount()>0) 
		for (int i=0;i<topTabPane.getComponentCount();i++)
		{
			if (topTabPane.getComponent(i) instanceof ChartPanel)
				((ChartPanel)topTabPane.getComponent(i)).update();
			//getChartPanel(0).update();
		}
	}

	public Chart getChart()
	{
		return getChartPanel(0).getChart();
	}

	/**
	 * get the ChartPanel of title title
	 * @param title
	 * @return
	 */
	public ChartPanel getChartPanel(String title)
	{
		for (int i=0;i<topTabPane.getComponentCount();i++) 
			if (topTabPane.getTitleAt(i).compareTo(title)==0) 
				return ((ChartPanel)topTabPane.getComponentAt(i));
		return null;
	}




	/**
	 * get the ChartPanel of title title
	 * @param title
	 * @return
	 */
	public JPanel getJPanel(String title)
	{
		for (int i=0;i<topTabPane.getComponentCount();i++) 
			if (topTabPane.getTitleAt(i).compareTo(title)==0) 
				return ((JPanel)topTabPane.getComponentAt(i));
		return null;
	}




	public CJFrame getCJFrame(){return cjframe;}
}
