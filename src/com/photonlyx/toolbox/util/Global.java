
// Author Laurent Brunel



package com.photonlyx.toolbox.util;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CMessageHolder;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.txt.TextFiles;

import nanoxml.XMLElement;



public class Global
{
	private  static Vector<CMessageHolder> cmessageHolderList=new Vector<CMessageHolder>();	

	//name of the file where is stored the last working directory:
	public static String workingDirectoryFileName="workingDir.txt";

	//name of the file where are stored the persistent parameters:
	public static String persistentParamsFileName="persistents.xml";

	//directory of the application where user params are stored:
	public static String appHome;

	//current path of the session, the working directory
	public static String path;


	//directory from where the application is executed
	//public static String execDir;

	public static Font font=new Font("DejaVu Sans",Font.PLAIN,10);
	//public static Font font=new Font("Arial",Font.PLAIN,10);
	//public static Font font=new Font("Berlin Sans FB",Font.PLAIN,10);

	public static Color background=Color.white;
	public static Color foreground=Color.black;
	public static Color chartGridLinesColor=new Color(200,200,200);
	public static boolean log=false;//make a log file
	public static boolean messagesInConsole=false;//if true print the messages in console
	public static String logFileName="log.txt";
	public static String photonlyxToolBoxDir="photonlyx.toolbox";
	public static Vector<HasPersistentParam> objectsWithPersistentParams=new Vector<HasPersistentParam>();

	public static void addMessageHolder(CMessageHolder cmh)
	{
		cmessageHolderList.add(cmh);
	}

	public static void removeMessageHolder(CMessageHolder cmh)
	{
		cmessageHolderList.remove(cmh);
	}

	public  static void setMessage(String s)
	{
		for (CMessageHolder cmessageHolder:cmessageHolderList) cmessageHolder.setMessage(s);	
		log(s);
	}


	public  static void log(String s)
	{
		if (messagesInConsole) System.out.println(s);
		if (log)
		{
			try
			{
				File file =new File(logFileName);
				if(!file.exists()){
					file.createNewFile();
				}
				FileWriter fileWritter = new FileWriter(file.getName(),true);
				BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
				bufferWritter.write(s+"\n");
				bufferWritter.close();
			}
			catch (Exception e) 
			{
				Messager.messErr(Messager.getString("Probleme_ecriture_fichier")+" "+logFileName);;
			}
		}
	}

	public static void resetDefaultMessage()
	{
		setMessage("PhotonLyX")	;

	}

	public static XMLElement getPersistentParams(String appName,String className)
	{
		XMLElement xml=null;
		//look for the folder where are stored the user params:
		Global.appHome=Util.getOrCreateApplicationUserFolder(appName);
		//read the file for persistence:
		StringBuffer sb=TextFiles.readFile(Global.appHome+File.separator+Global.persistentParamsFileName, false,false);
		String s="";
		if (sb!=null) 
		{
			s=sb.toString();
			xml = new XMLElement();
			StringReader reader = new StringReader(s);
			try {	xml.parseFromReader(reader);}
			catch (Exception ex) {ex.printStackTrace();}
		}
		if (xml==null) return null;
		//look for the son of name className
		XMLElement xml1=null;
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement son=enumSons.nextElement();
			{
				if (son.getName().compareTo(className)==0) 
				{
					xml1=son;
				}
			}
		}
		System.out.println("Loaded persistent params:");
		System.out.println(xml1);
		return xml1;
	}

	//save the working directory in its file (in XML):
	public static void saveWorkingDirectory(WindowApp app)
	{
		XMLElement el = new XMLElement();	
		el.setName("userParams");
		if (app!=null) 
		{
			if (app.path!=null) el.setAttribute("path", app.path);
			//System.out.println("app.appHome: "+app.appHome);
			//System.out.println("file of persistents: "+app.appHome+File.separator+Global.workingDirectoryFileName);
			if (app.appHome!=null) TextFiles.saveString(app.appHome+File.separator+Global.workingDirectoryFileName,el.toString());
		}
	}

	public static void savePersistentParams(WindowApp app)
	{
		//save the persistent parameters
		XMLElement el = new XMLElement();	
		el.setName("persistentParams");
		for (HasPersistentParam object:Global.getObjectsWithPersistentParams())
		{
			XMLElement son=object.savePersistentParams();
			System.out.println("Save persistent param:"+son);
			el.addChild(son);
		}
		System.out.println("Save persistent params:");
		//System.out.println(el);	
		if (app!=null)
			if (app.appHome!=null) 
				TextFiles.saveString(app.appHome+File.separator+Global.persistentParamsFileName,el.toString());
	}


	public static Vector<HasPersistentParam> getObjectsWithPersistentParams() 
	{
		return objectsWithPersistentParams;
	}


}

