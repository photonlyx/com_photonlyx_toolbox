package com.photonlyx.toolbox.util;

import nanoxml.XMLElement;

public interface HasPersistentParam 
{
	public void loadPersistentParams(String appName);
	public XMLElement savePersistentParams();
}
