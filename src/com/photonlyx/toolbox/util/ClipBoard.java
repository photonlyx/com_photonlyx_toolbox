package com.photonlyx.toolbox.util;


import java.awt.*;
import java.awt.datatransfer.*;
import java.io.*;

/**
*/
public class ClipBoard 
{
	protected ImageSelection imgSelection;

	public static class ImageSelection implements Transferable 
	{
		// the Image object which will be housed by the ImageSelection
		private Image image;

		public ImageSelection(Image image) 
		{
			this.image = image;
		}

		// Returns the supported flavors of our implementation
		public DataFlavor[] getTransferDataFlavors() 
		{
			return new DataFlavor[] {DataFlavor.imageFlavor};
		}

		// Returns true if flavor is supported
		public boolean isDataFlavorSupported(DataFlavor flavor) 
		{
			return DataFlavor.imageFlavor.equals(flavor);
		}

		// Returns Image object housed by Transferable object
		public Object getTransferData(DataFlavor flavor)
		throws UnsupportedFlavorException,IOException 
		{
			if (!DataFlavor.imageFlavor.equals(flavor)) 
				{
				throw new UnsupportedFlavorException(flavor);
				}
			
			// else return the payload
			return image;
		}
	}

	/** 
	 * Put an image in the system clipboard 
	 * 
	 * @param img Image that will be put in the clipboard
	 */
	public void putImage(Image img)
	{
		ImageSelection imageSelection = new ImageSelection(img);

		Toolkit
			.getDefaultToolkit()
			.getSystemClipboard()
			.setContents(imageSelection, null);
	}
	
	/** 
	 * Put some text in the system clipboard 
	 * 
	 * @param str String to store in the clipboard
	 */
	public static void putText(String str)
	{
		// get the system clipboard
		Clipboard systemClipboard =
			Toolkit
				.getDefaultToolkit()
				.getSystemClipboard();
		
		// set the textual content on the clipboard to our 
		// Transferable object
		
		Transferable transferableText = new StringSelection(str);
		
		systemClipboard.setContents( transferableText, null);
	}
	
	
	/** 
	 * Get some text in the system clipboard 
	 * 
	 * @param str String to store in the clipboard
	 */
	public static String getText()
	{
		// get the system clipboard
		Clipboard systemClipboard =
			Toolkit
				.getDefaultToolkit()
				.getSystemClipboard();
		
		// set the textual content on the clipboard to our 
		// Transferable object
		
		Transferable transferable =   systemClipboard.getContents(null);
		DataFlavor[]  flavors=transferable.getTransferDataFlavors() ;
// 		for (int i=0;i<flavors.length;i++) System.out.println(flavors[i]);
		
		String s=null;
		try 
			{
 			//System.out.println(transferable.getTransferData(DataFlavor.stringFlavor));
			s=transferable.getTransferData(DataFlavor.stringFlavor).toString(); 
			}
		catch (Exception e) {e.printStackTrace();}
		return s;
		
	}
	
	
	
}


