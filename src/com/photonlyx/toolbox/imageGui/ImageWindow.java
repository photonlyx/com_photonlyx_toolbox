package com.photonlyx.toolbox.imageGui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.math.image.CImage;



public class ImageWindow extends CJFrame
{
private JTabbedPane topTabPane;
private CImage cim=new CImage();
private PanelImage pi=new PanelImage(cim);
private JPanel mainPanel=new JPanel();
private JPanel panelSide=new JPanel();

public ImageWindow()
{
init();
}

public ImageWindow(CImage cim)
{
init();
topTabPane.add(pi);
pi.setCImage(cim);
pi.update();
}

public ImageWindow(CImage cim1,CImage cim2)
{
init();
add(cim1);
add(cim2);
}

public ImageWindow(CImage cim1,CImage cim2,CImage cim3)
{
init();
add(cim1);
add(cim2);
add(cim3);
}

public ImageWindow(BufferedImage bim)
{
init();
topTabPane.add(pi);
CImage cim=new CImage(bim);
pi.setCImage(cim);
pi.update();
}



private void init()
{
setTitle("Chart");
setSize(600, 400);
setExitAppOnExit(false);

mainPanel.setLayout(new BorderLayout());
add(mainPanel);

topTabPane=new JTabbedPane();
mainPanel.add(topTabPane,BorderLayout.CENTER);

panelSide.setPreferredSize(new Dimension(100,0));
mainPanel.add(panelSide,BorderLayout.WEST);
}



public void add(CImage cim)
{
PanelImage pi=new PanelImage(cim);
topTabPane.add(pi);
//pi.setCImage(cim);
pi.update();
}



public PanelImage getPanelImage(){return pi;}

public JPanel getPanelSide(){return panelSide;}

public void update()
{
for (int i=0;i<topTabPane.getTabCount();i++) topTabPane.getComponent(i).repaint();
}

public void removeAllPanels()
{
topTabPane.removeAll();

}

}
