package com.photonlyx.toolbox.imageGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.util.Global;

public class PanelImageWithPanelSide extends JPanel
{
private JSplitPane sp ;
private JPanel panelSide=new JPanel();
private PanelImage panelImage;
private CImage cim;

public PanelImageWithPanelSide(CImage cim) 
{
this.cim=cim;
this.setLayout(new BorderLayout());
panelImage=new PanelImage(cim);
//panel for button and legend
panelSide=new JPanel();
panelSide.setPreferredSize(new Dimension(150, 200));
panelSide.setLayout(new FlowLayout());
panelSide.setBackground(Global.background);
this.add(panelImage,BorderLayout.CENTER);
this.add(panelSide,BorderLayout.EAST);

}




public PanelImageWithPanelSide() 
{
this.setLayout(new BorderLayout());
cim=new CImage();
panelImage=new PanelImage(cim);
//panel for button and legend
panelSide=new JPanel();
panelSide.setPreferredSize(new Dimension(150, 200));
panelSide.setLayout(new FlowLayout());
panelSide.setBackground(Global.background);
this.add(panelImage,BorderLayout.CENTER);
this.add(panelSide,BorderLayout.EAST);
}



public void putPanelSideAtLeft()
{
this.remove(panelSide);
this.add(panelSide,BorderLayout.WEST);

}

public PanelImage getPanelImage(){return panelImage;}
public JPanel getPanelSide(){return panelSide;}
public void update(){panelImage.update();}


public static void main(String[] params)
{
	WindowApp app=new WindowApp();
	CImageProcessing cimp=new CImageProcessing();
	cimp.init(100,100);
	PanelImageWithPanelSide p=new PanelImageWithPanelSide(cimp);
	p.getPanelSide().setPreferredSize(new Dimension(100,0));
	
	p.getPanelSide().setBackground(Color.RED);
	
	app.add(p,"test");

	app.getCJFrame().validate();
	app.getCJFrame().repaint();

}






}
