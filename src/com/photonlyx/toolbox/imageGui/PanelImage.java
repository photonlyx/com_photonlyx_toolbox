

package com.photonlyx.toolbox.imageGui;

import javax.annotation.processing.Processor;
import javax.imageio.ImageIO;
import javax.swing.*;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.ColorUtil;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.imageGui.drawingLayer.DrawingLayerPainting;
import com.photonlyx.toolbox.imageGui.processor.PanelImageProcessor;
import com.photonlyx.toolbox.imageGui.processor.PanelImageProcessorListener;
import com.photonlyx.toolbox.math.histo.Histogram1D1D;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.image.DrawingLayer;
import com.photonlyx.toolbox.math.image.ImageFileFilter;
import com.photonlyx.toolbox.math.image.PNGFileFilter;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.ClipBoard.ImageSelection;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;



/**
Panel that show an image, with some processing tools
 */
public class PanelImage extends JPanel implements
MouseListener,MouseMotionListener,MouseWheelListener,
ActionListener, KeyListener,
PanelImageProcessorListener

{
	//	links
	private	 CImage cimage;
	private Vector<DrawingLayer> drawingLayers=new Vector<DrawingLayer>();
	//params
	private int x,y,w=320,h=240;
	private boolean adaptImageToPanel=true;
	private boolean keepXYratio=true;
	public double angle;//angle of rotation
	private boolean reticule=false;
	private String reticuleColor="white";// colour of the reticule
	//private int xs,ys,ws,hs;//subimage (or region of interest) coordinates
	private boolean seeSubImage;//if true print the sub image rectangle
	//private double zoom=1;
	//private int dx=0,dy=0;//
	private double wheelCoef=1.1;//how is multiply the zoom at each wheel click
	//internal
	private  JPopupMenu popMenu;
	private JMenuItem[] popUpMenuItems;
	private String mouseTag="";//what to print near the mouse
	private String mouseTag2="";//what to print near the mouse
	private String message="";//message at bottom
	private Point mouse=new Point(0,0);// mouse position
	private Point piZone11=new Point(0,0),piZone12=new Point(0,0);//ROI 1 in image coordinates
	private Point piZone21=new Point(0,0),piZone22=new Point(0,0);//ROI 2 in image coordinates
	private int mouseButton;//mouse button pressed
	private Point mouseLast;//last mouse position
	private boolean showMouseTag=false;
	private int xp0=0,yp0=0;//for panel to image coordinates conversion, top left image corner coords in panel
	private double cx=1,cy=1;//for panel to image coordinates conversion, zoom in x and y
	private boolean zoning=false;//tells if user is drawing the ROI 1 or 2
	public boolean zoning2=false;// tells if user is drawing ROI 2
	private Color zone1Color=Color.red;//color of ROI 1
	private Color zone2Color=Color.blue;//color of ROI 2
	private int mouseTagBrightness;
	public int offset=0,amp=100; //to modify the contrast 
	public int smooth=0; //to smooth the image
	public boolean turnRight=false;
	public boolean turnLeft=false;
	public boolean zoom=false;//show only the image inside the zone defined by the user
	public boolean mergeZones=false;//show an image composed by the 2 zones or ROI
	private boolean showMenu=false;
	private ChartWindow fourierWindow=null;
	private Vector<PanelImageProcessor> pimps=new Vector<PanelImageProcessor>();
	//private boolean painting=false;
	private DrawingLayerPainting layerPainting=null;

	public  PanelImage()
	{
		super();
		init();
	}

	public  PanelImage(CImage image)
	{
		super();
		this.cimage=image;
		init();
	}

	public  PanelImage(BufferedImage bufferedImage)
	{
		super();
		this.cimage=new CImage(bufferedImage);
		init();
	}


	private void init()
	{
		this.setPreferredSize(new Dimension(w,h));
		this.setLocation(x,y);
		this.setBackground(Global.background);

		//set popup menu
		String[] labels=new String[20];

		int n=0;
		labels[n]=Messager.getString("copy_image");n++;
		labels[n]=Messager.getString("paste_image");n++;
		labels[n]=Messager.getString("open_image_as_jpg_png_gif_bmp");n++;
		labels[n]=Messager.getString("save_image_as_jpg_png_gif_bmp");n++;
		labels[n]=Messager.getString("turn right");n++;
		labels[n]=Messager.getString("turn left");n++;
		labels[n]=Messager.getString("crop");n++;
		labels[n]=Messager.getString("meanX");n++;
		labels[n]=Messager.getString("meanY");n++;
		labels[n]=Messager.getString("setHue");n++;
		labels[n]=Messager.getString("setSaturation");n++;
		labels[n]=Messager.getString("setBrightness");n++;
		labels[n]=Messager.getString("reduceResolution");n++;
		int nbmenus=n;
		popMenu=new JPopupMenu(Messager.getString("Action"));

		this.add(popMenu);
		popUpMenuItems =new JMenuItem[nbmenus];
		for (int i=0;i<nbmenus;i++) 
		{
			popUpMenuItems[i]=new JMenuItem(labels[i]);
			popUpMenuItems[i].setFont(Global.font);
			popUpMenuItems[i].addActionListener(this);
			popMenu.add(popUpMenuItems[i]);
		}

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addKeyListener(this);
		this.addMouseWheelListener(this);



		repaint();

	}



	public void setCImage(CImage cim)
	{
		this.cimage=cim;
		update();
	}

	/**
	 * top left point of the zone
	 * @return
	 */
	public int getXs() {
		return Math.min(piZone11.x,piZone12.x);
	}


	/**
	 * top left point of the zone
	 * 
	 */
	public void setXs(int xs) {
		this.piZone11.x = xs;
	}


	/**
	 * top left point of the zone
	 * @return
	 */
	public int getYs() {
		return Math.min(piZone11.y,piZone12.y);
	}


	/**
	 * top left point of the zone

	 */
	public void setYs(int ys) {
		this.piZone11.y = ys;
	}

	/**
	 * width of the zone
	 * @return
	 */

	public int getWs() 
	{
		int ws=Math.abs(piZone12.x-piZone11.x);
		if (ws==0) return cimage.w();
		else return ws;
	}


	/**
	 * width of the zone
	 * @return
	 */
	public void setWs(int ws) {
		piZone12.x = piZone11.x+ws;
	}


	/**
	 * height of the zone
	 * @return
	 */
	public int getHs() 
	{
		int hs=Math.abs(piZone12.y-piZone11.y);
		if (hs==0) return cimage.h();
		else return hs;
	}


	/**
	 * height of the zone
	 * @return
	 */
	public void setHs(int hs) {
		piZone12.y = piZone11.y+hs;
	}

	/**
	 * stick al least 2 sides to the panel border (2 if setKeepXYratio(false) )
	 * @param adaptImageToPanel
	 */
	public void setAdaptImageToPanel(boolean adaptImageToPanel) {
		this.adaptImageToPanel = adaptImageToPanel;
	}


	public void setKeepXYratio(boolean keepXYratio) {
		this.keepXYratio = keepXYratio;
	}


	public void setAngle(double angle) {
		this.angle = angle;
	}


	public void setReticule(boolean reticule) {
		this.reticule = reticule;
	}


	public void setReticuleColor(String reticuleColor) {
		this.reticuleColor = reticuleColor;
	}


	public void setSeeSubImage(boolean seeSubImage) {
		this.seeSubImage = seeSubImage;
	}


	public void setShowMouseTag(boolean showMouseTag) {
		this.showMouseTag = showMouseTag;
	}


	public void paintComponent(Graphics g)
	{
		if (cimage==null) return;
		Image im=cimage.getImage();
		if (im==null) return;

		//erase:
		g.setColor(this.getBackground());
		g.fillRect(0,0,getWidth(),getHeight());

		if (showMenu)
		{
			int x=5;
			int y=10;
			int dy=15;
			g.setColor(Color.DARK_GRAY);
			g.drawString("m: hide/show menu", x, y+=dy);
			g.drawString("s: show mouse tag", x, y+=dy);
			g.drawString("R: show reticule", x, y+=dy);
			g.drawString("o: post processing", x, y+=dy);
			g.drawString("z: zoom", x, y+=dy);
			g.drawString("c: crop on zone", x, y+=dy);
			g.drawString("+: brighter by 10", x, y+=dy);
			g.drawString("-: darker by 10", x, y+=dy);
			g.drawString("*: brighter by 10%", x, y+=dy);
			g.drawString("/: darker by 10%", x, y+=dy);
			g.drawString("f: see 2D Fourier transform", x, y+=dy);
			g.drawString("P: bigger pencil", x, y+=dy);
			g.drawString("p: smaller pencil", x, y+=dy);
			g.drawString("h: histogram of the zone", x, y+=dy);
			return;
		}

		CImageProcessing imp2=postProcessing(cimage);
		drawImage(g,imp2);

		for (DrawingLayer drawingLayer:drawingLayers) 
		{
			drawingLayer.draw(g,this,cimage);
		}

		//mouse tag
		if (showMouseTag)
		{
			g.setFont(Global.font.deriveFont((float)10.0));
			//g.setColor(Color.white);
			g.setColor(new Color(mouseTagBrightness,mouseTagBrightness,mouseTagBrightness));
			g.drawString(mouseTag, mouse.x, mouse.y);
			g.drawString(mouseTag2, mouse.x, mouse.y+10);
		}

		g.setColor(Color.lightGray);
		g.drawString(message, 10,this.getHeight()-10);
	}

	public void drawImage(Graphics g,CImageProcessing imp2)
	{
		int xImageOnPanel=0,yImageOnPanel=0;	
		int imageWidthOnPanel=getWidth(),imageHeightOnPanel=getHeight();	
		//if (angle!=0)
		{
			Graphics2D g2=(Graphics2D)g;
			if (angle!=0) g2.rotate(angle/180.0*Math.PI,getWidth()/2.,getHeight()/2.);
			super.paintComponent(g2);
			if (adaptImageToPanel) 
			{
				if (!keepXYratio) 
				{
					xImageOnPanel=0;
					yImageOnPanel=0;
					imageWidthOnPanel=getWidth();
					imageHeightOnPanel=getHeight();
					xp0=0;
					cx=getWidth()/(double)imp2.w();
					yp0=0;
					cy=getHeight()/(double)imp2.h();
					g2.drawImage(imp2.getImage(),xImageOnPanel,yImageOnPanel,imageWidthOnPanel,imageHeightOnPanel,this);
				}
				else 
				{
					int margin=00;
					//			System.out.println(getClass()+" image w x h = "+image.w()+"x"+image.h());
					//			System.out.println(getClass()+" panel w x h = "+getWidth()+"x"+getHeight());
					double XYratioImage=imp2.w()/(double)imp2.h();
					double XYratioPanel=(getWidth())/(double)(getHeight());
					//			System.out.println(getClass()+" XYratioImage="+XYratioImage);
					//			System.out.println(getClass()+" XYratioPanel="+XYratioPanel);
					if (XYratioPanel<XYratioImage) //left and right sides of image stick to panel
					{
						imageWidthOnPanel=getWidth()-2*margin;
						imageHeightOnPanel=(int)(imageWidthOnPanel/XYratioImage);
						xImageOnPanel=margin;
						yImageOnPanel=(getHeight()-imageHeightOnPanel)/2;;
						//				System.out.println(getClass()+" yy="+yy);
						g2.drawImage(imp2.getImage(),xImageOnPanel,yImageOnPanel,imageWidthOnPanel,imageHeightOnPanel,this);
						xp0=0;
						cx=getWidth()/(double)imp2.w();
						yp0=yImageOnPanel;
						cy=imageHeightOnPanel/(double)imp2.h();
					}
					else  //top and bottom sides of image stick to panel
					{
						imageHeightOnPanel=getHeight()-2*margin;
						imageWidthOnPanel=(int)(imageHeightOnPanel*XYratioImage);
						xImageOnPanel=(getWidth()-imageWidthOnPanel)/2;
						yImageOnPanel=(getHeight()-imageHeightOnPanel)/2;;
						g2.drawImage(imp2.getImage(),xImageOnPanel,yImageOnPanel,imageWidthOnPanel,imageHeightOnPanel,this);
						xp0=xImageOnPanel;
						cx=imageWidthOnPanel/(double)imp2.w();
						yp0=0;
						cy=getHeight()/(double)imp2.h();
					}
				}
			}
			else 
			{
				g2.drawImage(imp2.getImage(),xp0,yp0,(int)(imp2.w()*cx),(int)(imp2.h()*cy),this);
			}
		}


		if (reticule)
		{
			g.setColor(ColorUtil.getColor(reticuleColor));
			g.drawLine(0,getHeight()/2,getWidth(),getHeight()/2);
			g.drawLine(getWidth()/2,0,getWidth()/2,getHeight());
		}


		//if (zoning)
		//if (seeSubImage)
		{
			if ((!zoom)&&(!mergeZones))
			{
				//draw ROI or zone 1:
				g.setColor(zone1Color);
				Point p1=this.convertImageToPanelCoordinates(piZone11);
				Point p2=this.convertImageToPanelCoordinates(piZone12);
				int x1=Math.min(p1.x, p2.x);
				int x2=Math.max(p1.x, p2.x);
				int y1=Math.min(p1.y, p2.y);
				int y2=Math.max(p1.y, p2.y);
				//g.drawRect(p1.x,p1.y,p2.x-p1.x,p2.y-p1.y);
				g.drawRect(x1,y1,x2-x1,y2-y1);
				//draw ROI or zone 21:
				g.setColor(zone2Color);
				Point p21=this.convertImageToPanelCoordinates(piZone21);
				Point p22=this.convertImageToPanelCoordinates(piZone22);
				x1=Math.min(p21.x, p22.x);
				x2=Math.max(p21.x, p22.x);
				y1=Math.min(p21.y, p22.y);
				y2=Math.max(p21.y, p22.y);
				//g.drawRect(p21.x,p21.y,p22.x-p21.x,p22.y-p21.y);
				g.drawRect(x1,y1,x2-x1,y2-y1);
			}
			else if (mergeZones)
			{
			}
			else
			{
				g.setColor(zone1Color);
				g.drawRect(xImageOnPanel,yImageOnPanel,imageWidthOnPanel-1,imageHeightOnPanel-1);
			}
		}



	}

	/**
	 * get the x in panel coordinates
	 * @param xi x in image coordinate
	 * @return
	 */
	public int xp(double xi)
	{
		return (int)(xp0+cx*xi);
	}

	/**
	 * get the y in panel coordinates
	 * @param yi y in image coordinate
	 * @return
	 */
	public int yp(double yi)
	{
		return  (int)(yp0+cy*yi);
	}


	/**
	 * get the x in image coordinates
	 * @param xp x in panel coordinate
	 * @return
	 */
	public int xi(double xp)
	{
		return (int)((xp-xp0)/cx);
	}

	/**
	 * get the y in image coordinates
	 * @param yp y in panel coordinate
	 * @return
	 */
	public int yi(double yp)
	{
		return (int)((yp-yp0)/cy);
	}

	/**
	 * convert a displacement in panel coords to one in image coords
	 * 
	 * @param dxs
	 * @return
	 */
	public double dxp2dxi(int dxp)
	{
		return dxp/cx;
	}

	/**
	 * convert a displacement in panel coords to one in image coords
	 * 
	 * @param dxs
	 * @return
	 */
	public double dyp2dyi(int dyp)
	{
		return dyp/cy;
	}



	private Point convertPanelToImageCoordinates(Point p)
	{
		Point p2=new Point();
		p2.x=xi(p.x);
		p2.y=yi(p.y);
		return p2;
	}

	private Point convertImageToPanelCoordinates(Point p)
	{
		Point p2=new Point();
		p2.x=xp(p.x);
		p2.y=yp(p.y);
		return p2;
	}


	private void adaptImageToPanel()
	{
		double XYratioImage=cimage.w()/(double)cimage.h();
		double XYratioPanel=getWidth()/(double)getHeight();
		if (XYratioPanel<XYratioImage) //left and right sides of image stick to panel
		{
			int imageHeightOnPanel=(int)(getWidth()/XYratioImage);
			xp0=0;
			cx=getWidth()/(double)cimage.w();
			yp0=(getHeight()-imageHeightOnPanel)/2;
			cy=imageHeightOnPanel/(double)cimage.h();
		}
		else  //top and bottom sides of image stick to panel
		{
			int imageWidthOnPanel=(int)(getHeight()*XYratioImage);
			xp0=(getWidth()-imageWidthOnPanel)/2;
			cx=imageWidthOnPanel/(double)cimage.w();
			yp0=0;
			cy=getHeight()/(double)cimage.h();
		}
	}




public boolean zoneIsAllImage() {
	return (	(piZone11.x==0)&&	(piZone11.y==0)&&	(piZone12.x==0)&&	(piZone12.y==0)	);
}

	public void mouseClicked(MouseEvent e)
	{
		//System.out.println(getClass()+ " mouse clicked "+e.getPoint().x+" "+e.getPoint().y);
		if ((e.getButton()==MouseEvent.BUTTON3) ) 
			popMenu.show(this,e.getPoint().x,e.getPoint().y);
		//popMenu.show(this,e.getPoint().x,e.getPoint().y);

		if ((!zoom)&&(e.getButton()==MouseEvent.BUTTON1)&&(e.getClickCount()==1) )
		{
			piZone11.x=0;
			piZone11.y=0;
			piZone12.x=0;
			piZone12.y=0;
		}

		if ((e.getButton()==MouseEvent.BUTTON1)&&(e.getClickCount()==2) )
		{
			String filename=new Date().toString()+".png";
			cimage.saveImage(filename);
			message="Image saved to "+filename;
			//System.out.println(getClass()+" image saved to "+filename);
		}

		if ((e.getButton()==MouseEvent.BUTTON1)&&(e.getClickCount()==2) )
		{
			message=" ";
		}
		//System.out.println("Zone: ("+pZone1.x+" "+pZone1.y+")"+" ("+pZone2.x+" "+pZone2.y+")"); 

		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.mouseClicked(e);
	}


	public void mouseEntered(MouseEvent e) 
	{
		this.requestFocus();	
		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.mouseEntered(e);
	}


	public void mouseExited(MouseEvent e) 
	{
		mouseTag="";
		repaint();
		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.mouseExited(e);	
	}


	public void mousePressed(MouseEvent e) 
	{
		mouse=e.getPoint();
		mouseButton=e.getButton();
		mouseLast=mouse;
		if ((!zoom)&&(e.getButton()==MouseEvent.BUTTON1))
		{
			zoning=true;
			if (zoning2) piZone21=convertPanelToImageCoordinates(mouse);
			else piZone11=convertPanelToImageCoordinates(mouse);
		}
		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.mousePressed(e);
	}


	public void mouseReleased(MouseEvent e) 
	{
		//System.out.println(getClass()+ " mouse released "+e.getPoint().x+" "+e.getPoint().y);
		if ((!zoom)&&(zoning)&&(e.getButton()==MouseEvent.BUTTON1))
		{
			zoning=false;
			//System.out.println("Zone: ("+pZone1.x+" "+pZone1.y+")"+" ("+pZone2.x+" "+pZone2.y+")"); 
		}
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.mouseReleased(e);
	}



	public void mouseDragged(MouseEvent e) 
	{
		//System.out.println("mouseDragged "+e.getButton());

		if ((!zoom)&&(zoning)&&(mouseButton==MouseEvent.BUTTON1) )
		{
			Point p2p=e.getPoint();
			Point p2i2=new Point();
			Point p2i3=new Point();
			p2i2.x=xi(p2p.x);
			p2i2.y=yi(p2p.y);
			p2i3.x=p2i2.x;
			p2i3.y=p2i2.y;
			//check if zone outside of image:
			int wmax=cimage.w(),hmax=cimage.h();
			if ((turnRight)||(turnLeft)) 
			{
				wmax=cimage.h();
				hmax=cimage.w();
			}

			if (p2i2.x<0) p2i3.x=(0);
			if (p2i2.x>=wmax) p2i3.x=wmax-1;
			if (p2i2.y<0) p2i3.y=(0);
			if (p2i2.y>=hmax) p2i3.y=hmax-1;


			//	if (zoning2) pZone22=convertPanelToImageCoordinates(e.getPoint());
			//	else pZone12=convertPanelToImageCoordinates(e.getPoint());
			if (zoning2)  
			{
				piZone22=p2i3;	
				//check other corner:
				if (piZone21.x<0) piZone21.x=0;
				if (piZone21.x>=wmax) piZone21.x=wmax-1;
				if (piZone21.y<0) piZone21.y=(0);
				if (piZone21.y>=hmax) piZone21.y=hmax-1;	
			}
			else 
			{
				piZone12=p2i3;	
				//check other corner:
				if (piZone11.x<0) piZone11.x=0;
				if (piZone11.x>=wmax) piZone11.x=wmax-1;
				if (piZone11.y<0) piZone11.y=(0);
				if (piZone11.y>=hmax) piZone11.y=hmax-1;
			}

			repaint();
			//System.out.println("dragging"+pZone2.x+" "+pZone2.y);
		}
		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.mouseDragged(e);
	}


	public void mouseMoved(MouseEvent e) 
	{
		if (cimage==null) return;
		if ((cimage.w()*cimage.h())==0) return;
		mouse=e.getPoint();
		int x=xi(mouse.x);
		int y=yi(mouse.y);

		//System.out.println("x="+x+" y="+y);
		int[] im=cimage.getRawPixelsFromImage();
		int w=cimage.w();
		int h=cimage.h();
		int index=x+w*y;
		if ((index>=0)&(index<im.length)) 
		{
			Color c=new Color(im[index]);
			int r=c.getRed();
			int g=c.getGreen();
			int b=c.getBlue();
			mouseTag=x+" "+y+" rgb:"+r+" "+g+" "+b+" pix:"+(int)(y*w+x);
			float[] hsb=Color.RGBtoHSB(r, g, b, null);
			int hh=(int)(hsb[0]*255);
			int ss=(int)(hsb[1]*255);
			int bb=(int)(hsb[2]*255);
			mouseTagBrightness=255-(int)(bb*0.7);
			mouseTag2="    hsb:   "+hh+" "+ss+" "+bb;//+" "+c.getRGB();
		}
		else mouseTag=x+" "+y;
		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.mouseMoved(e);
		repaint();
	}





	public void mouseWheelMoved(MouseWheelEvent e) 
	{
		int nbClicks=e.getWheelRotation();
		//coords of the panel central point in the image:
		int xic1=xi(this.getWidth()/2);
		int yic1=yi(this.getHeight()/2);
		System.out.println("xic1="+xic1+" yic1="+yic1);
		//do the zoom:
		if (nbClicks>0) {cx*=wheelCoef*nbClicks;cy=cx;}
		else {cx/=wheelCoef*(-nbClicks);cy=cx;}
		//coords of the panel central point in the image after zoom:
		int xic2=xi(this.getWidth()/2);
		int yic2=yi(this.getHeight()/2);
		//do the offset
		xp0=(int)(this.getWidth()/2-cx*xic1);
		yp0=(int)(this.getHeight()/2-cy*yic1);
		repaint();
	}




	public void actionPerformed(ActionEvent e) 
	{
		Object source = e.getSource();
		Component parent=null;
		parent = this;
		int n=0;


		if (source==popUpMenuItems[n])//copy image to clipboard
		{
			//	CImageProcessing simp=new CImageProcessing(cimage);
			//	int xc=this.getXs();
			//	int yc=this.getYs();
			//	int wc=this.getWs();
			//	int hc=this.getHs();
			//	
			//	simp._mul(amp/100.0);
			//	simp._add(offset);
			//	
			//	ImageSelection imgSel;
			//	if ((wc<=5)&(hc<=5)) imgSel = new ImageSelection(simp.getImage());
			//	else imgSel = new ImageSelection(simp.crop(xc, yc, wc, hc).getBufferedImage());
			//	
			//	
			ImageSelection imgSel = new ImageSelection(this.postProcessing(cimage).getBufferedImage());

			//ImageSelection imgSel = new ImageSelection(image.getImage());	
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);
		}
		n++;

		if (source==popUpMenuItems[n])//paste image from clipboard
		{
			Transferable t =Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);	
			try
			{
				if (t != null && t.isDataFlavorSupported(DataFlavor.imageFlavor)) 
				{
					Image img = (Image) t.getTransferData(DataFlavor.imageFlavor);
					cimage=new CImage(img);
				}
			}
			catch(Exception ee){ee.printStackTrace();}


		}

		n++;
		if (source==popUpMenuItems[n])//load image from disk
		{
			//browse the file
			JFileChooser df=new JFileChooser(Global.path);
			df.addChoosableFileFilter(new ImageFileFilter());
			int returnVal = df.showOpenDialog(parent);
			if(returnVal == JFileChooser.APPROVE_OPTION)
			{
				Global.path=df.getSelectedFile().getParent()+File.separator;
				String fileName=df.getSelectedFile().getName();
				String path=df.getSelectedFile().getParent()+File.separator;
				if (cimage==null) return;
				cimage.setImage(CImage.loadImage(path,fileName, false,false));
				repaint();
			}
		}
		n++;

		if (source==popUpMenuItems[n])//save image (post-processed) to disk
		{
			JFileChooser df=new JFileChooser(Global.path);
			df.addChoosableFileFilter(new PNGFileFilter());
			int  returnVal = df.showSaveDialog(parent);
			if(returnVal == JFileChooser.APPROVE_OPTION)
			{
				String fileName=df.getSelectedFile().getName();
				String path=df.getSelectedFile().getParent()+File.separator;
				if (cimage==null) return;
				this.postProcessing(cimage).saveImage(path, fileName);
			}
		}
		n++;

		if (source==popUpMenuItems[n])//turn right
		{
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._turnRight();
			//ImageWindow iw=new ImageWindow(cimp);
			cimage.copy(cimp);
			update();
		}
		n++;

		if (source==popUpMenuItems[n])//turn left
		{
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._turnLeft();
			cimage.copy(cimp);
			update();
		}
		n++;


		if (source==popUpMenuItems[n])//crop
		{
			//get the zone	
			//	Point p1=this.convertImageToPanelCoordinates(piZone11);
			//	Point p2=this.convertImageToPanelCoordinates(piZone12);
			int x1=Math.min(piZone11.x, piZone12.x);
			int x2=Math.max(piZone11.x, piZone12.x);
			int y1=Math.min(piZone11.y, piZone12.y);
			int y2=Math.max(piZone11.y, piZone12.y);
			//this.postProcessing(cimage).crop(x1,y1,x2-x1,y2-y1);
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._crop(x1,y1,x2-x1,y2-y1);
			//ImageWindow iw=new ImageWindow(cimp);
			//int w=cimp.w();
			//int h=cimp.h();
			//System.out.println("cropped: "+w+"x"+h);
			cimage.copy(cimp);
			//reset the zone 1:
			piZone11=new Point();
			piZone12=new Point();
			update();
		}
		n++;

		if (source==popUpMenuItems[n])//meanX
		{
			//get the zone	
			int x1=Math.min(piZone11.x, piZone12.x);
			int x2=Math.max(piZone11.x, piZone12.x);
			int y1=Math.min(piZone11.y, piZone12.y);
			int y2=Math.max(piZone11.y, piZone12.y);
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._crop(x1,y1,x2-x1,y2-y1);
			SignalDiscret1D1D proj=cimp._meanXRed();
			ChartWindow cw=new ChartWindow(proj);
			update();
		}
		n++;
		if (source==popUpMenuItems[n])//meanY
		{
			//get the zone	
			int x1=Math.min(piZone11.x, piZone12.x);
			int x2=Math.max(piZone11.x, piZone12.x);
			int y1=Math.min(piZone11.y, piZone12.y);
			int y2=Math.max(piZone11.y, piZone12.y);
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._crop(x1,y1,x2-x1,y2-y1);
			SignalDiscret1D1D proj=cimp._meanYRed();
			ChartWindow cw=new ChartWindow(proj);
			update();
		}
		n++;



		if (source==popUpMenuItems[n])//setHue replace color by hue
		{
			//get the zone	
			int x1=Math.min(piZone11.x, piZone12.x);
			int x2=Math.max(piZone11.x, piZone12.x);
			int y1=Math.min(piZone11.y, piZone12.y);
			int y2=Math.max(piZone11.y, piZone12.y);
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._crop(x1,y1,x2-x1,y2-y1);
			cimp._setHue();
			cimage.copy(cimp);
			update();
		}
		n++;

		if (source==popUpMenuItems[n])// setSaturation replace color by saturation
		{
			//get the zone	
			int x1=Math.min(piZone11.x, piZone12.x);
			int x2=Math.max(piZone11.x, piZone12.x);
			int y1=Math.min(piZone11.y, piZone12.y);
			int y2=Math.max(piZone11.y, piZone12.y);
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._crop(x1,y1,x2-x1,y2-y1);
			cimp._setSaturation();
			cimage.copy(cimp);
			update();
		}
		n++;

		if (source==popUpMenuItems[n])//setBrightness replace color by brightness
		{
			//get the zone	
			int x1=Math.min(piZone11.x, piZone12.x);
			int x2=Math.max(piZone11.x, piZone12.x);
			int y1=Math.min(piZone11.y, piZone12.y);
			int y2=Math.max(piZone11.y, piZone12.y);
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._crop(x1,y1,x2-x1,y2-y1);
			cimp._setBrightness();
			cimage.copy(cimp);
			update();
		}
		n++;

		if (source==popUpMenuItems[n])//reduceResolution
		{
			CImageProcessing cimp=new CImageProcessing(cimage);
			int w=cimp.getWidth();
			CImage im2=new CImage(cimp.resample(w/2));
			cimage.copy(im2);
			update();
		}
		n++;


	}		



	public void keyPressed(KeyEvent e)
	{
		//System.out.println(e.getKeyChar());
		char keychar=e.getKeyChar();	
		switch (keychar)
		{
		case 's'  :
			showMouseTag=!showMouseTag;
			repaint();
			break;
		case 'R'  :
			reticule=!reticule	;
			repaint();
			break;
		case 'r'  :
			adaptImageToPanel();
			repaint();
			break;
		case 'o'  ://more options
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"update"));
			String[] names1={"offset","amp","angle","smooth","turnRight","turnLeft","zoom","zoning2","mergeZones"};
			ParamsBox paramsBox1=new ParamsBox(this,tl,names1);
			CJFrame cjf=new CJFrame();
			cjf.setTitle("Post processing");
			cjf.getContentPane().setLayout(new FlowLayout());
			cjf.getContentPane().add(paramsBox1.getComponent());
			cjf.pack();
			cjf.setExitAppOnExit(false);
			cjf.setAutoCenter(true);
			cjf.setLocation(cjf.getLocation().x-cjf.getWidth(), cjf.getLocation().y);
			cjf.setVisible(true);
			break;
		case 'z'  :
			zoom=!zoom;
			update();
			break;
		case 'c'  :
		{
			int x1=Math.min(piZone11.x, piZone12.x);
			int x2=Math.max(piZone11.x, piZone12.x);
			int y1=Math.min(piZone11.y, piZone12.y);
			int y2=Math.max(piZone11.y, piZone12.y);
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._crop(x1,y1,x2-x1,y2-y1);
			cimage.copy(cimp);
			//reset the zone 1:
			piZone11=new Point();
			piZone12=new Point();
			update();
		}
		break;
		case '+'  :
		{
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._add(10);
			cimage.copy(cimp);
			update();
		}
		break;
		case '-'  :
		{
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._add(-10);
			cimage.copy(cimp);
			update();
		}
		break;
		case '*'  :
		{
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._mul(1.1);
			cimage.copy(cimp);
			update();
		}
		break;
		case '/'  :
		{
			CImageProcessing cimp=new CImageProcessing(cimage);
			cimp._mul(1/1.1);
			cimage.copy(cimp);
			update();
		}
		break;
		case 'f': //fourier transform
		{
			Signal2D1D im=new Signal2D1D();
			im.fillWithImage(postProcessing(cimage).getImage(),"R", 1);
			im.copy(im.fft2d());
			if (fourierWindow==null) fourierWindow=new ChartWindow( im);
			else 
			{
				fourierWindow.getCJFrame().setVisible(true);
				fourierWindow.getChart().setSignal2D1D(im);
			}
			fourierWindow.getChart().setLogz(true);
			fourierWindow.getChart().setGridLinex(false);
			fourierWindow.getChart().setGridLiney(false);
			fourierWindow.getChart().update();
		}
		break;
		case 'p':
		{
			if (!this.hasDrawingLayer(layerPainting)) 
			{

			}
			else 
			{
				int size=layerPainting.getBrushSize();
				layerPainting.setBrushSize(size-1);
				if (size==0) this.removeDrawingLayer(layerPainting);
			}
		}
		break;
		case 'P':
		{
			if (!this.hasDrawingLayer(layerPainting)) 
			{
				layerPainting=new DrawingLayerPainting(this);
				this.setLayerPainting(layerPainting);
			}
			else 
			{
				layerPainting.setBrushSize(layerPainting.getBrushSize()+1);
			}
		}
		break;
		case 'h'://histogram (of brightness) of the zone
		{
			Signal2D1D im=new Signal2D1D();
			CImageProcessing cimp2= new CImageProcessing(cimage);
			CImageProcessing cimp3=cimp2.crop(this.getXs(), this.getYs(), this.getWs(), this.getHs());
			im.fillWithImage(cimp3.getImage(),"Br", 1);
			Histogram1D1D histo=new Histogram1D1D(255,0,255,im);
			ChartWindow cw=new ChartWindow(histo);
			cw.getChartPanel().hidePanelSide();
			cw.getChartPanel().hideLegend();
			break;
		}
		case 'm':
		{
			showMenu=!showMenu;	
		}
		break;
		}
		repaint();
		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.keyPressed(e);
	}


	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.keyReleased(e);

	}


	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		//pass the event to the drawing layers
		for (DrawingLayer drawingLayer:drawingLayers) drawingLayer.keyTyped(e);

	}


	public void addDrawingLayer(DrawingLayer dl)
	{
		drawingLayers.add(dl);
	}

	public void removeDrawingLayer(DrawingLayer dl)
	{
		drawingLayers.remove(dl);
	}

	public boolean hasDrawingLayer(DrawingLayer dl)
	{
		return drawingLayers.contains(dl);
	}

	public CImage getCImage() {
		return cimage;
	}

	public void update()
	{
		repaint();
		for (PanelImageProcessor p:pimps) p.process();
	}

	public void setImage(CImage cimage)
	{
		this.cimage=cimage;
	}

	/**
	 * save the Frame as png
	 * @param path
	 * @param filename
	 */
	public void savePanel(String path,String filename)
	{
		JPanel p=this;
		JFrame win = (JFrame)SwingUtilities.getWindowAncestor(p);
		//Dimension size = win.getSize();
		//BufferedImage image = (BufferedImage)win.createImage(size.width, size.height);
		//Graphics g = image.getGraphics();
		//win.paint(g);

		BufferedImage image = (BufferedImage)win.createImage(w,h);
		Graphics g = this.getGraphics();
		this.paint(g);
		g.dispose();
		try      {
			ImageIO.write(image, "PNG", new File(path+filename));
		}
		catch (IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * test program
	 * @param args
	 */
	public static void main(String[] args)
	{
		CJFrame cjf=new CJFrame();
		CImageProcessing cimRaw=new CImageProcessing();//panel image
		//CImage cimRaw=new CImage();//panel image
		cimRaw.loadFile("","com/photonlyx/chartApp/Penelope-Cruz.jpg", true);
		PanelImage pi=new PanelImage(cimRaw);
		cjf.add(pi);
		pi.update();
		cjf.validate();
	}


	public CImageProcessing getPostProcessedImage()
	{
		return postProcessing(cimage);
	}

	public CImageProcessing postProcessing(CImage cimage)
	{

		//if ((offset==0)&&(amp==100)&&(!turnRight)&&(!turnLeft)&&(!zoom)) return;
		CImageProcessing imp2=new CImageProcessing(cimage);
		if (!((offset==0)&&(amp==100))) 
		{
			imp2._mul(amp/100.0);
			imp2._add(offset);
		}
		if (smooth>0)
		{
			imp2._smoothSquareKernel(smooth);
		}

		if (turnRight)
		{
			imp2._turnRight();
		}
		else if (turnLeft)
		{
			imp2._turnLeft();
		}

		if (zoom)//zoom on the zone selection
		{
			//	BufferedImage bim=imp2.crop(this.getXs(), this.getYs(), this.getWs(), this.getHs());
			//	imp2.copy(bim);
			imp2._crop(this.getXs(), this.getYs(), this.getWs(), this.getHs());
		}
		if (!zoom) if (this.mergeZones)
		{
			int w1=piZone12.x-piZone11.x;
			int h1=piZone12.y-piZone11.y;
			int w2=piZone22.x-piZone21.x;
			int h2=piZone22.y-piZone21.y;
			CImageProcessing zone1=imp2.crop(this.getXs(), this.getYs(), w1, h1);
			CImageProcessing zone2=imp2.crop(piZone21.x, piZone21.y, w2, h2);
			int w=Math.max(w1, w2);
			int h=h1+h2;
			CImageProcessing merge=new CImageProcessing(w,h);
			merge._paste(zone1,0,0);
			merge._paste(zone2,0,h1);
			imp2=merge;
		}

		return imp2;
	}

	@Override
	public void addPanelImageProcessor(PanelImageProcessor pimp)
	{
		pimp.setPanelImage(this);
		pimps.add(pimp);
	}

	/**
	 * 
	 */
	public CImageProcessing getImageInZone()
	{
		int x1=Math.min(piZone11.x, piZone12.x);
		int x2=Math.max(piZone11.x, piZone12.x);
		int y1=Math.min(piZone11.y, piZone12.y);
		int y2=Math.max(piZone11.y, piZone12.y);
		CImageProcessing cimp=new CImageProcessing(cimage);
		cimp._crop(x1,y1,x2-x1,y2-y1);
		return cimp;
	}

	public DrawingLayerPainting getLayerPainting() 
	{
		return layerPainting;
	}

	public void setLayerPainting(DrawingLayerPainting layerPainting) 
	{
		this.addDrawingLayer(layerPainting);		
		this.layerPainting = layerPainting;
	}



}
