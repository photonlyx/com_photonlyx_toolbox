package com.photonlyx.toolbox.imageGui.patterns;

import java.awt.Color;

import com.photonlyx.toolbox.imageGui.ImageWindow;
import com.photonlyx.toolbox.math.image.CImage;

public class SinWave extends CImage
{
private double lambda=5;//wavelength in pixels
private double centerx,centery;//center of the wave
private double amplitude=10;
private double mean=150;
private double attenuation=50;//multiplied by exp(-r/attenuation)


public SinWave(int w ,int h)
{
super(w,h);
centerx=w/2;
centery=w/2;
update();
}


public void update()
{
int[] pix=this.getRawPixelsFromImage();
int w=w();
int h=h();
for (int i=0;i<w;i++)
	for (int j=0;j<h;j++)
		{
		double r=Math.sqrt(Math.pow(i-centerx,2)+Math.pow(j-centery,2));
		int c=(int)(Math.sin(r/lambda)*amplitude*Math.exp(-r/attenuation)+mean);
		pix[i+j*w]=new Color(c,c,c).getRGB() ;
		}
this.setPixelsDirect(pix, w, h);
}

public static void main(String[] args)
{
SinWave im=new SinWave(500,500);
ImageWindow iw=new ImageWindow(im);
}


public double getLambda()
{
return lambda;
}


public void setLambda(double lambda)
{
this.lambda = lambda;
}


public double getCenterx()
{
return centerx;
}


public void setCenterx(double centerx)
{
this.centerx = centerx;
}


public double getCentery()
{
return centery;
}


public void setCentery(double centery)
{
this.centery = centery;
}


public double getAmplitude()
{
return amplitude;
}


public void setAmplitude(double amplitude)
{
this.amplitude = amplitude;
}


public double getMean()
{
return mean;
}


public void setMean(double mean)
{
this.mean = mean;
}


public double getAttenuation()
{
return attenuation;
}


public void setAttenuation(double attenuation)
{
this.attenuation = attenuation;
}





}
