package com.photonlyx.toolbox.imageGui.drawingLayer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

import javax.swing.JPanel;

import com.photonlyx.toolbox.imageGui.PanelImage;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.image.DrawingLayer;
import com.photonlyx.toolbox.math.image.ImageSource;

public class MeanLevelDetection implements DrawingLayer
{
public static  DecimalFormat df0=new DecimalFormat("0");
public static  DecimalFormat df1=new DecimalFormat("0.0");
public static  DecimalFormat df2=new DecimalFormat("0.00");
public static  DecimalFormat df3=new DecimalFormat("0.000");
public static  DecimalFormat df4=new DecimalFormat("0.0000");

private double mean;



public double getMean()
{
return mean;
}

@Override
	public void mouseClicked(MouseEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
	// TODO Auto-generated method stub
	
	}

	@Override
	public void draw(Graphics g, JPanel jp, ImageSource is)
	{
	PanelImage pi=(PanelImage)jp;
	int xc=pi.getXs();
	int yc=pi.getYs();
	int wc=pi.getWs();
	int hc=pi.getHs();
	mean=calcMeanLevel(xc,yc,wc,hc,pi.getCImage());
	int xp1,yp1,xp2,yp2;
	g.setColor(new Color(255,0,0,128));
	xp1=pi.xp(xc);
	yp1=pi.yp(yc);
	//print the mean level:
	g.setFont(new Font("Arial",Font.BOLD,26));
	g.drawString("mean:"+df2.format(mean),xp1,yp1-5);
	}


public static double calcMeanLevel(int xc,int yc,int wc,int hc,CImage cim)
{
CImageProcessing cimp=new CImageProcessing(cim);
cimp._crop(xc, yc, wc, hc);
//System.out.println("wc="+wc+" hc="+hc);//debug
//calc the mean level:
double mean=cimp.calcMeanBrightness();
return mean;
}	
		
}






