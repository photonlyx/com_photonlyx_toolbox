package com.photonlyx.toolbox.imageGui.drawingLayer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

import javax.swing.JPanel;

import com.photonlyx.toolbox.imageGui.PanelImage;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.image.DrawingLayer;
import com.photonlyx.toolbox.math.image.ImageSource;

public class DrawingLayerPainting implements DrawingLayer
{
	public String mess1="";
	public String mess2="";
	//private CImageProcessing image=new CImageProcessing();//image painted that is superposed 
	//private CImageProcessing image2=new CImageProcessing();//image painted that is superposed 
	private PanelImage pi;//PanelIMage that is superposed
	private int brushSize;
	private Point pBrush=new Point(); //brush position
	private Point pBrush1=new Point();//first corner of brush
	private Point pBrush2=new Point();//second corner of brush
	private Color color=new Color(255,0,0,128);

	public DrawingLayerPainting(PanelImage pi)
	{
		super();
		this.pi=pi;
		int w=pi.getCImage().w();
		int h=pi.getCImage().h();
		//image=new  CImageProcessing(w,h);
		//image.fill(new Color(0,0,0,0));
		//image.loadResource("com/photonlyx/chartApp/Penelope-Cruz.jpg");
		//image2.loadResource("com/photonlyx/chartApp/Penelope-Cruz.png");

	}

	@Override
	public void mouseClicked(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0)
	{
		//System.out.println("Start painting");
	}

	@Override
	public void mouseReleased(MouseEvent arg0)
	{
		//System.out.println("Stop painting");

	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		//System.out.println("Painting");
		Point pOnScreen=e.getPoint();
		Point pOnImage=new Point();
		pOnImage.x=pi.xi(pOnScreen.x);
		pOnImage.y=pi.yi(pOnScreen.y);
		CImage cim=pi.getCImage();
		int w=cim.w();
		int h=cim.h();
		if (cim instanceof CImageProcessing) 
		{
			CImageProcessing cimp=(CImageProcessing)cim;
			cimp.setRGB(pOnImage.x, pOnImage.y, color.getRGB());
			for (int i=-brushSize;i<brushSize;i++)
				for (int j=-brushSize;j<brushSize;j++)
				{
					if ((pOnImage.x+i>0)&&(pOnImage.x+i<w-1)&&(pOnImage.y+j>0)&&(pOnImage.y+j<h-1))
							cimp.setRGB(pOnImage.x+i, pOnImage.y+j, color.getRGB());
				}
		}
		pi.repaint();
	}	

	@Override
	public void mouseMoved(MouseEvent e)
	{
		pBrush=e.getPoint();
		if (brushSize>0)
		{
			setBrush();
		}
	}	
	
	public void setBrush()
	{
			Point pOnImage1=new Point();
			pOnImage1.x=pi.xi(pBrush.x)-brushSize;
			pOnImage1.y=pi.yi(pBrush.y)-brushSize;
			pBrush1.x=pi.xp(pOnImage1.x);
			pBrush1.y=pi.yp(pOnImage1.y);
			Point pOnImage2=new Point();
			pOnImage2.x=pi.xi(pBrush.x)+brushSize;
			pOnImage2.y=pi.yi(pBrush.y)+brushSize;
			pBrush2.x=pi.xp(pOnImage2.x);
			pBrush2.y=pi.yp(pOnImage2.y);	
	
	}

	@Override
	public void keyPressed(KeyEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(Graphics g, JPanel jp, ImageSource is)
	{
		//System.out.println(pBrush1+"   "+pBrush2);
		g.setColor(color);
		g.drawRect(pBrush1.x,pBrush1.y,pBrush2.x-pBrush1.x,pBrush2.y-pBrush1.y);
	}

	public int getBrushSize() {
		return brushSize;
	}

	public void setBrushSize(int i) 
	{
		brushSize=i;
		setBrush();
		pi.repaint();

	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		//System.out.println(getClass()+ " set color "+color.getRGB());
		this.color = color;
	}



}


