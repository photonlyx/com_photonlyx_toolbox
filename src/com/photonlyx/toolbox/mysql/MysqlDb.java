package com.photonlyx.toolbox.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;

import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Util;

public class MysqlDb 
{
	private String url;
	private String url_local;
	private Connection c=null;
	private Vector<MysqlTable> tables=new Vector<MysqlTable>();
	private  boolean enable=true;
	private Vector<MysqlRow> waitingList=new Vector<MysqlRow>();

	public MysqlDb()
	{

	}

	public MysqlDb(String url,String url_local, String user, String pass,boolean useLocalURL) 
	{

		this.url=url;
		this.url_local=url_local;
		//this.user=user;
		//this.pass=pass;

		if (!useLocalURL)
		{
			// Open a mysql database connection with the server
			try
			{
				//System.out.println("Connect to url :"+url);
				DriverManager.setLoginTimeout(30);
				c = DriverManager.getConnection(url,user,pass);
				System.out.println("Connected to database "+url);
				Global.setMessage("Connected to database "+url);		
			}
			catch(Exception e)
			{
				System.err.println("Cant connect to database"+url);
				System.out.println("Connect to alternative url :"+url);
				try
				{
					c = DriverManager.getConnection(url_local,user,pass);
					System.out.println("Connected to database "+url_local);
					Global.setMessage("Connected to database "+url_local);		
				}
				catch(Exception e1)
				{
					e1.printStackTrace();
					System.err.println("Cant connect to database"+url_local);
					Global.setMessage("Cant connect to database "+url_local);
				}

				e.printStackTrace();
				System.err.println("Cant connect to database"+url);
				Global.setMessage("Cant connect to database "+url);
				return;
			}
		}
		else
		{
			// Open a mysql database connection with the server
			try
			{
				//System.out.println("Connect to url :"+url_local);
				DriverManager.setLoginTimeout(30);
				c = DriverManager.getConnection(url,user,pass);
				System.out.println("Connected to database "+url_local);
				Global.setMessage("Connected to database "+url_local);		
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.err.println("Cant connect to database"+url_local);
				Global.setMessage("Cant connect to database "+url_local);
				return;
			}
		}

	}

	public MysqlDb(String url,String user, String pass) 
	{
		this.url=url;
		//this.user=user;
		//this.pass=pass;

		// Open a mysql database connection with the server
		try
		{
			System.out.println("Connect to url :"+url);
			DriverManager.setLoginTimeout(30);
			c = DriverManager.getConnection(url,user,pass);
			System.out.println("Connected to database "+url);
			Global.setMessage("Connected to database "+url);		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.err.println("Cant connect to database"+url);
			Global.setMessage("Cant connect to database "+url);
			return;
		}
	}

	public Vector<MysqlTable> getTables() {
		return tables;
	}


	public String getUrl() {
		return url;
	}



	public Connection getConnection() 
	{
		return c;
	}



	public boolean isEnable() {
		return enable;
	}



	public void setEnableSQL(boolean enable) {
		this.enable = enable;
	}



	public void add(MysqlTable t)
	{
		tables.add(t);
	}

	public MysqlTable getTableOfName(String name)
	{
		MysqlTable t0=null;
		for (MysqlTable t:tables)
		{
			if (t.getTableName().compareTo(name)==0) t0=t;
		}
		return t0;
	}

	public void addToWaitingList(MysqlRow l) 
	{
		if (waitingList.contains(l)) waitingList.add(l);
	}




	public  Vector<String> getColumnNames(String tableName) 
	{
		Vector<String> names=new Vector<String>();
		Statement stmt=null;
		ResultSet rs;
		try 
		{
			stmt=this.getConnection().createStatement();
			String command="SELECT *FROM "+tableName;
			System.out.println(command);
			rs = stmt.executeQuery(command);
			ResultSetMetaData md = (ResultSetMetaData) rs.getMetaData();
			int counter = md.getColumnCount();
			//String colName[] = new String[counter];
			//System.out.println("The column names are as follows:");
			for (int i=1;i<=counter;i++) 
			{
				//colName[loop-1] = md.getColumnLabel(loop);
				//System.out.println(colName[loop-1]);
				names.add(md.getColumnLabel(i));
			}
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return names;
	}



	/**
	 * get the value of a given column of a row of a given dbNumber
	 * @param tableName
	 * @param dbNumber
	 * @param columnName
	 * @return
	 */
	public String getField(String tableName, int dbNumber,String columnName)
	{
		String field=null;
		if (this.getConnection()==null) 
		{
			System.err.println("Not connected to database "+url);
			return "";
		}
		try
		{
			Statement stmt =  this.getConnection().createStatement();
			String query = "SELECT "; //"SELECT id, first, last, age FROM Registration";
			query+=" * ";
			query+=" FROM "+tableName;	
			query+=" WHERE  dbNumber='"+dbNumber+"';";	
			//System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next())
			{
				Object o=rs.getObject(columnName);
				field=""+o;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		return field;
	}

	/**
	 * get the rows having a given value for a given column
	 * @param tableName
	 * @param columnName
	 * @return
	 */
	public Vector<Integer> getRows(String tableName,String columnName,String value)
	{
		//Vector<Object> list=new Vector<Object>();
		Vector<Integer> list=new Vector<Integer>();
		if (this.getConnection()==null) 
		{
			System.err.println("Not connected to database "+url);
			return list;
		}
		try
		{
			Statement stmt =  this.getConnection().createStatement();
			String query = "SELECT "; //"SELECT id, first, last, age FROM Registration";
			query+=" * ";
			query+=" FROM "+tableName;	
			query+=" WHERE "+ columnName+"='"+value+"';";	
			//System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next())
			{
				Object o=rs.getObject("dbNumber");
				list.add((int)o);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		return list;
	}

	/**
	 * get the number of row
	 * @param tableName
	 * @return
	 */
	public int getNbRows(String tableName)
	{
		int nb=-1;
		Vector<Object> list=new Vector<Object>();
		if (this.getConnection()==null) 
		{
			System.err.println("Not connected to database "+url);
			return -1;
		}
		try
		{
			Statement stmt =  this.getConnection().createStatement();
			String query = "SELECT COUNT(*) FROM "+tableName; 
			System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			//nb=Integer.parseInt(""+rs.getObject(1));
			while(rs.next())
			{
				String s=rs.getObject(1)+"";
				//System.out.println(s);
				nb=Integer.parseInt(s);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return nb;
	}


	/**
	 * get the db numbers of the rows having a given val in a given column
	 * @param tableName
	 * @param columnName
	 * @param val
	 * @return
	 */
	public Vector<String> getDbNumbers(String tableName,String columnName,String val) 
	{
		Vector<String> indices=new Vector<String>();
		if (this.getConnection()==null) 
		{
			System.err.println("Not connected to database "+url);
			return indices;
		}
		try
		{
			Statement stmt =  this.getConnection().createStatement();
			String query = "SELECT "; //"SELECT id, first, last, age FROM Registration";
			query+=" * ";
			query+=" FROM "+tableName;	
			query+=" WHERE  "+columnName+"='"+val+"';";	
			//System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next())
			{
				Object o=rs.getObject("dbNumber");
				//System.out.println(rs.getClass());
				indices.add(""+o);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return indices;
	}

	/**
	 * get the db numbers of the rows having a given val in a given column
	 * @param tableName
	 * @param columnName
	 * @param val
	 * @return
	 */
	public String[][] getAllData(String tableName) 
	{
		Vector<String> colNames=this.getColumnNames(tableName);	
		int nbRows=this.getNbRows(tableName);
		String[][] matrice=new String[nbRows][colNames.size()];

		if (this.getConnection()==null) 
		{
			System.err.println("Not connected to database "+url);
			return null;
		}
		try
		{
			Statement stmt =  this.getConnection().createStatement();
			String query = "SELECT "; 
			query+=" * ";
			query+=" FROM "+tableName;	
			query+="';";	
			System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next())
			{
				for (String colName:colNames)
				{
					Object o=rs.getObject(colName);
					System.out.print(o+""+"   ");
					//indices.add(""+o);
				}
				System.out.println();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return matrice;
	}


	public Vector<Integer> getdBNumbers(String tableName) 
	{
		Vector<Integer> indices=new Vector<Integer>();

		if (this.getConnection()==null) 
		{
			System.err.println("Not connected to database "+url);
			return null;
		}
		try
		{
			Statement stmt =  this.getConnection().createStatement();
			String query = "SELECT "; 
			query+=" dbNumber ";
			query+=" FROM "+tableName;	
			query+=";";	
			System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next())
			{
				int id=rs.getInt(1);
				System.out.print(id+""+"   ");
				indices.add(id);
			}
			System.out.println();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return indices;
	}



	public void updateField(String tableName, int dbNumber,String fieldName,String fieldValue) 
	{
		if (this.getConnection()==null) 
		{
			System.err.println("Not connected to database "+url);
			return ;
		}
		//remove "'" character:
		fieldValue=fieldValue.replaceAll("'","_");
		String sql = "UPDATE "+tableName;
		sql+=" SET ";
		sql+=" "+fieldName+ "=  '"+fieldValue+"' ";
		sql+="WHERE dBnumber= '"+dbNumber+"' ";
		try
		{
			Statement stmt = this.getConnection().createStatement();	
			//String sql=getUpdateSQLcommand(l,fieldName);
			System.out.println(sql);
			stmt.executeUpdate(sql);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}




	public Vector<String> getTableNames() 
	{
		//		Vector<String> names=new Vector<String>();
		//		try 
		//		{		
		//            String[] types = {"TABLE"};
		//			ResultSet rs =c.getMetaData().getTables(null, null, "%", types	);
		//			while (rs.next()) 
		//			{
		//				String s=rs.getString(3);
		//				System.out.println(s);
		//				names.add(s);
		//			}
		//		}
		//		catch(Exception e) 
		//		{
		//			e.printStackTrace();
		//		}
		//		return names;


		Vector<String> indices=new Vector<String>();
		if (this.getConnection()==null) 
		{
			System.err.println("Not connected to database "+url);
			return indices;
		}
		try
		{
			Statement stmt =  this.getConnection().createStatement();
			{
				//stmt.execute("USE fablab;");
			}
			//String query = "SELECT * FROM INFORMATION_SCHEMA.TABLES ";
			String query = "SHOW TABLES;";
			System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next())
			{
				String tableName=rs.getString(1);
				//System.out.println(tableName);
				indices.add(tableName);
				//String name=""+rs.getObject("table_name");
				//String type=""+rs.getObject("table_type");
				//if (type.compareTo("BASE TABLE")==0)
				{
					//System.out.println(name);
				}
				//indices.add(""+name);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return indices;

	}


	public static void main(String args[])
	{

		//test

		String url= "jdbc:mysql://cloud.fablabsantander.org:3306/fablab";
		String url1= "jdbc:mysql://192.168.1.147:3306/fablab";
		String user="laurent";
		String pass="Mimosa34!";
		MysqlDb db=new MysqlDb(url,url1,user,pass,true);


		//	String fieldName="postalAddress";
		//	String f=db.getField("Customers",1,fieldName);
		//	System.out.println("Customer first name of ID 1:"+f);

		//db.updateField("Customers",1,fieldName,"Calle Rucandial 16");

		//Vector<String> fields=db.getFields("Customers",1);
		//for (String s:fields) System.out.println(s);

		//	Vector<String> cols=db.getColumnNames("Sales");
		//	for (String s:cols) System.out.println(s);


		//	System.out.println(db.getField("Sales",10,"date"));


//		Vector<String> tableNames=db.getTableNames();
//		for (String s:tableNames) System.out.println(s);

//		Vector<Integer> ids=db.getdBNumbers("Customers"); 
//		for (int id:ids) System.out.println(id);

		int nb=db.getNbRows("Customers");
		System.out.println(nb);
		
		
	}
}
