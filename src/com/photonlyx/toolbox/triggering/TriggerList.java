package com.photonlyx.toolbox.triggering;

import java.util.Vector;
import java.lang.reflect.Method;


/**
 * has a list of objects that can be triggered
 * @author laurent
 *
 */
public class TriggerList 
{
private Vector<Trigger> list=new Vector<Trigger>();
private Vector<TriggerList> list2=new Vector<TriggerList>();


public void add(Trigger t)
{
if (!list.contains(t)) list.add(t);
}
public void add(TriggerList tl)
{
list2.add(tl);
}

public void trig()
{
//System.out.println(getClass()+" trig ");
for (Trigger t:list)	t.trig();
for (TriggerList tl:list2)	tl.trig();
	
	
}



}
