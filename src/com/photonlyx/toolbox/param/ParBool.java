package com.photonlyx.toolbox.param;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import nanoxml.XMLElement;


public class ParBool extends Param
{
//public boolean val;
	
public ParBool(Object paramOwner, String paramName) 
{
super(paramOwner, paramName);
}




public String stringVal()
{
return ""+val();
}

public boolean setVal(String string) 
{
setVal(string.compareTo("true")==0);
return true;
}
		
//public boolean val()
//{
//Method[] methods=getParamOwner().getClass().getMethods();
//Boolean dd=null;
//for (Method m:methods) 
//	{
//	String name=m.getName();
//	//System.out.println(name);
//	if (  (name.compareTo("is"+this.getParamName())==0) )
//		{
//		try {dd=(Boolean)m.invoke(getParamOwner(), (Object[])null);}
//		catch(Exception e){e.printStackTrace();}
//		break;
//		}
//	}	
//boolean val;
//if (dd!=null) val=dd.booleanValue();else val=false;
//return val;
//}

public boolean val()
{
Field field=null;
try
	{
	field=getParamOwner().getClass().getField(getParamName());
	if (field!=null) 
		{
		return field.getBoolean(getParamOwner());
		}
	}
catch (Exception e){};

Method m=getMethodIs();
if (m==null) return false;
Boolean dd=null;
try {dd=(Boolean)m.invoke(getParamOwner(), (Object[])null);}
catch(Exception e){e.printStackTrace();}
boolean val;
if (dd!=null) val=dd.booleanValue();else val=false;
return val;
}

public void setVal(boolean d) 
{
Field field=null;
try
	{
	field=getParamOwner().getClass().getField(getParamName());
	if (field!=null) 
		{
		field.set(getParamOwner(), d);
		return;
		}
	}
catch (Exception e){};
//System.out.println(paramOwner.getClass()+" **************************field: "+field);

Method m=getMethodSet();
if (m==null) return;
Boolean dd=new Boolean(d);
try {m.invoke(getParamOwner(), dd);}
catch(Exception e){e.printStackTrace();}
}


//public void setVal(boolean b) 
//{
////System.out.println(getClass()+" setVal "+d);
//Method[] methods=getParamOwner().getClass().getMethods();
//Boolean bb=new Boolean(b);
//for (Method m:methods) 
//	{
//	String name=m.getName();
//	//System.out.println("Method:"+name);
//	if (  (name.compareTo("set"+this.getParamName())==0) )
//		{
//		try {m.invoke(getParamOwner(), bb);}
//		catch(Exception e){e.printStackTrace();}
//		break;
//		}
//	}	
//
//}


//
//
//public void constructRecursive(XMLElement xml)
//{
//super.constructRecursive(xml);
//val=(xml.getStringAttribute("VAL").compareTo("true")==0);
//}


public void updateAttributes(XMLElement xml)
{
super.updateAttributes(xml);
String s=xml.getStringAttribute("VAL");
if (s==null) setVal(false);
else setVal(s.compareTo("true")==0);
}

}
