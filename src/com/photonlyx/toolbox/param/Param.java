package com.photonlyx.toolbox.param;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;

import nanoxml.XMLElement;


public abstract class Param 
{
private boolean persistent;
private Object paramOwner;//the object that owns the parameter
private String paramName;//the name of the parameter (methods getName and setName)



public Param(Object paramOwner, String paramName) 
{
super();
this.paramOwner = paramOwner;
this.paramName = paramName;
Util.getType(paramOwner,paramName);
}

//public Param(Object paramOwner,String paramNam)
//{
//	
//}

public void updateAttributes(XMLElement xml)
{
String att=xml.getStringAttribute("persistent");
if (att==null) persistent=false; else persistent=(att.compareTo("true")==0);
}

public abstract String stringVal();

public boolean isPersistent(){return persistent;}


/**recursive method */
public  String toStringRecursive(String decalage)
{
StringBuffer sb=new StringBuffer();
sb.append(decalage+" val="+stringVal()+"\n");
return sb.toString();
}


public abstract boolean setVal(String string) ;



public String toXMLIn() 
{
return " val=\""+stringVal()+"\"";
}



protected void setChanged(boolean b) {
	// TODO Auto-generated method stub
	
}



public Object getParamOwner() {
	
	return paramOwner;
}

public String getParamName() {
	
	return paramName;
}


public Method getMethodGet()
{
String firstLetter=paramName.substring(0, 1).toUpperCase();
String s2=firstLetter+paramName.substring(1, paramName.length());
Method[] methods=getParamOwner().getClass().getMethods();
for (Method m:methods) 
	{
	String name=m.getName();
	//System.out.println(name);
	if (  (name.compareTo("get"+paramName)==0)||(name.compareTo("get"+s2)==0) ) return m;
	}
System.out.println(getClass()+" getMethodGet() Warning: could not find "+"get"+paramName+" or "+"get"+s2+" in "+getParamOwner().getClass());
return null;
}

public Method getMethodIs()
{
String firstLetter=paramName.substring(0, 1).toUpperCase();
String s2=firstLetter+paramName.substring(1, paramName.length());
Method[] methods=getParamOwner().getClass().getMethods();
for (Method m:methods) 
	{
	String name=m.getName();
	//System.out.println(name);
	if (  (name.compareTo("is"+paramName)==0)||(name.compareTo("is"+s2)==0) ) return m;
	}
System.out.println(getClass()+" getMethodGet() Warning: could not find "+"is"+paramName+" or "+"is"+s2+" in "+getParamOwner().getClass());
return null;
}

public Method getMethodSet()
{
String firstLetter=paramName.substring(0, 1).toUpperCase();
String s2=firstLetter+paramName.substring(1, paramName.length());
Method[] methods=getParamOwner().getClass().getMethods();
for (Method m:methods) 
	{
	String name=m.getName();
	//System.out.println(name+ "is "+"set"+paramName+"   OR  "+"set"+s2+"  ?");
	if (  (name.compareTo("set"+paramName)==0)||(name.compareTo("set"+s2)==0) ) return m;
	}
System.out.println(getClass()+" getMethodSet() Warning: could not find "+"set"+paramName+" or "+"set"+s2+" in "+getParamOwner().getClass());
return null;
}








/**
 * change the value of a parameter in an object,using java.lang.reflect
 * @param o
 * @param paramName name of the parameter
 * @param d
 * @return true if successfull
 */
public  static boolean setParam(Object o,String paramName,double d)
{
Method m=getMethodSet(o,paramName);
if (m==null) return false;
Double dd=new Double(d);
try {m.invoke(o, dd);}
catch(Exception e){e.printStackTrace();return false;}
return true;
}




public static double getParam(Object o,String paramName)
{
Field field=null;
try
	{
	field=o.getClass().getField(paramName);
	if (field!=null) 
		{
		return field.getDouble(o);
		}
	}
catch (Exception e){};

Method m=getMethodGet(o,paramName);
if (m==null) return 0;
Double dd=null;
try {dd=(Double)m.invoke(o, (Object[])null);}
catch(Exception e){e.printStackTrace();}
double val;
if (dd!=null) val=dd.doubleValue();else val=0;
return val;
}



public static Method getMethodSet(Object o,String paramName)
{
String firstLetter=paramName.substring(0, 1).toUpperCase();
String s2=firstLetter+paramName.substring(1, paramName.length());
Method[] methods=o.getClass().getMethods();
for (Method m:methods) 
	{
	String name=m.getName();
	//System.out.println(name+ "is "+"set"+paramName+"   OR  "+"set"+s2+"  ?");
	if (  (name.compareTo("set"+paramName)==0)||(name.compareTo("set"+s2)==0) ) return m;
	}
System.out.println("com.photonlyx.toolbox.param.Param"+" getMethodSet() Warning: could not find "+"set"+paramName+" or "+"set"+s2+" in "+o.getClass());
return null;
}


public static Method getMethodGet(Object o,String paramName)
{
String firstLetter=paramName.substring(0, 1).toUpperCase();
String s2=firstLetter+paramName.substring(1, paramName.length());
Method[] methods=o.getClass().getMethods();
for (Method m:methods) 
	{
	String name=m.getName();
	//System.out.println(name);
	if (  (name.compareTo("get"+paramName)==0)||(name.compareTo("get"+s2)==0) ) return m;
	}
System.out.println("com.photonlyx.toolbox.param.Param"+" getMethodGet() Warning: could not find "+"get"+paramName+" or "+"get"+s2+" in "+o.getClass());
return null;
}

















}
