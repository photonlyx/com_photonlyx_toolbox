package com.photonlyx.toolbox.param;

import com.photonlyx.toolbox.txt.Definition;



/**
parameter of type time. Example of definition for 3 days 2 hours 3minutes 4seconds 10ms : 
param { Time name timelaps default 3d2h3m4s010   }. The time is internally stored as a double in seconds.

*/

public  class ParTime extends ParDouble
{


private static String dayLabel="d";
private static String hourLabel="h";
private static String minuteLabel="min";
private static String secondLabel="s";
private static String millisecondLabel="ms";

public ParTime(Object paramOwner, String paramName)
	{
	super(paramOwner, paramName);
	}


/**
format the time such that the 2 main fields (or one if only one) are taken. For instance 2d3h or 2"350  or 3h or 3h2m.
*/
public  String stringVal()
{
return formatSeconds(val());
}

/**set the value from a string
@return false if the string cannot be converted in the Param value (then the value is nor changed)
*/
public boolean setVal(String string)
{
double previous=val();
int id=string.indexOf(dayLabel);
int ih=string.indexOf(hourLabel);
int im=string.indexOf(minuteLabel);
int is=string.indexOf(secondLabel);
int ims=string.indexOf(millisecondLabel);
int d=0,h=0,m=0,s=0,ms=0;
// System.out.println(getClass()+" setVal: received:"+string);
try
	{
	int i=0;
	if (id!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,id));
		d=new Integer(string.substring(i,id)).intValue();
		i=id+dayLabel.length();
		}
	if (ih!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,ih));
		h=new Integer(string.substring(i,ih)).intValue();
		i=ih+hourLabel.length();
		}
	if (im!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,im));
		m=new Integer(string.substring(i,im)).intValue();
		i=im+minuteLabel.length();
		}
	if (is!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,is));
		s=new Integer(string.substring(i,is)).intValue();
		i=is+secondLabel.length();
		}
	if (ims!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,ims));
		ms=new Integer(string.substring(i,ims)).intValue();
// 		i=ims+1;
		}
// 	if (i<string.length())  ms=new Integer(string.substring(i)).intValue();
// System.out.println(getClass()+" setVal: casted->"+d+"d"+h+"h"+m+"m"+s+"s"+ms+"ms");
	
	}
catch (NumberFormatException e)
	{
	return false;
	}
this.setVal(ms/1000.+s+m*60+h*3600+d*24*3600);
if (val()!=previous) setChanged(true);
return true;
}



/**
format the time such that the 2 main fields (or one if only one) are taken. For instance 2d3h or 2"350  or 3h or 3h2m.
 * @param seconds number of seconds to format
 * @param hasDays show days or only hours (2d3h become 51h13min)
 * @param has2charsValues if true, the number of minutes and seconds have 2 digits
 * @return the formated string
*/
public static String formatSeconds(double seconds, boolean hasDays, boolean has2charsValues)
{
	return formatSeconds( seconds,  hasDays,  has2charsValues,1);
}

/**
format the time such that the 2 main fields (or one if only one) are taken. For instance 2d3h or 2"350  or 3h or 3h2m.
 * @param seconds number of seconds to format
 * @param hasDays show days or only hours (2d3h become 51h13min)
 * @param has2charsValues if true, the number of minutes and seconds have 2 digits
 * @param msPrecision When the ms are dissplayed after a number of seconds, this prameter indicates the
 * 			precision: if 1 then all ms are shown (ex:3s245), if 10, only 2 digits are shown
 * 			(ex:3s24), if 100, only 1 digit is shown (ex:3s2)
 * @return the formated string
*/
public static String formatSeconds(double seconds, boolean hasDays, boolean has2charsValues, int msPrecision)
{
if (Math.abs(seconds)<0.001) return "0";
boolean negative=(seconds<0);
seconds=Math.abs(seconds);
int ms=(int)Math.round((seconds-(int)seconds)*1000.);
int s=(int)seconds;
int min=s/60;
s=s-min*60;
int h=min/60;
min=min-h*60;
int d = 0;
if (hasDays == true)
	{
	d=h/24;
	h=h-d*24;
	}
String string="";
if (d!=0)
	{
	string+=d+dayLabel;
	if (h!=0) string+=h+hourLabel;
	}
else if ((h!=0))
	{
	string+=h+hourLabel;
	if (min!=0) 
		{
		if (has2charsValues && min < 10 )
			{
			string+="0";
			}
		string+=min+minuteLabel;
		}
	}
else if ((min!=0))
	{
	string+=min+minuteLabel;
	if (s!=0) 
		{
		if (has2charsValues && s < 10)
			{
			string+="0";
			}
		string+=s+secondLabel;
		}
	}
else if ((s!=0))
	{
	string+=s+secondLabel;
	int msNbDigits = 1;
	if (msPrecision == 1){msNbDigits = 3;}
	else if (msPrecision == 10){msNbDigits = 2;}
	else if (msPrecision == 100){msNbDigits = 1;}
	
	if ((int) ms/msPrecision!=0) string+= String.format("%0" + msNbDigits + "d", (int) ms/msPrecision);
	}
else if ((ms!=0))
	{
	string+=ms+millisecondLabel;
	}
if (negative) string="-"+string;
return string;
}

/**
format the time such that the 2 main fields (or one if only one) are taken. For instance 2d3h or 2"350  or 3h or 3h2m.
*/
public static String formatSeconds(double seconds)
{
	return formatSeconds(seconds, true, false);
}


/**
parse a string to extract a time in seconds
*/
public static double parse(String string)
{
int id=string.indexOf(dayLabel);
int ih=string.indexOf(hourLabel);
int im=string.indexOf(minuteLabel);
int is=string.indexOf(secondLabel);
int ims=string.indexOf(millisecondLabel);
int d=0,h=0,m=0,s=0,ms=0;
// System.out.println(getClass()+" setVal: received:"+string);
try
	{
	int i=0;
	if (id!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,id));
		d=new Integer(string.substring(i,id)).intValue();
		i=id+dayLabel.length();
		}
	if (ih!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,ih));
		h=new Integer(string.substring(i,ih)).intValue();
		i=ih+hourLabel.length();
		}
	if (im!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,im));
		m=new Integer(string.substring(i,im)).intValue();
		i=im+minuteLabel.length();
		}
	if (is!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,is));
		s=new Integer(string.substring(i,is)).intValue();
		i=is+secondLabel.length();
		}
	if (ims!=-1) 
		{
// 		System.out.println(getClass()+" setVal:"+string.substring(i,ims));
		ms=new Integer(string.substring(i,ims)).intValue();
// 		i=ims+1;
		}
// 	if (i<string.length())  ms=new Integer(string.substring(i)).intValue();
// System.out.println(getClass()+" setVal: casted->"+d+"d"+h+"h"+m+"m"+s+"s"+ms+"ms");
	
	}
catch (NumberFormatException e)
	{
	System.out.println("fa.reuse.jolo.ParTime static parse:"+"error parsing "+string);
	}
return ms/1000.+s+m*60+h*3600+d*24*3600;
}


}
