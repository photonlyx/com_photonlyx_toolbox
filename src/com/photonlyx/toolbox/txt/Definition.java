package com.photonlyx.toolbox.txt;

import java.util.*;

import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;

import java.text.*;


//*******************************************************************************************
//*************************** CLASSE DEFINITION *********************************************
//*******************************************************************************************

/** class which stores the definition of a word, this word beeing the first item of the definition*/
public class Definition implements Iterable<String>
{
private Vector<String> list;
int scanindex=0; //for the scanning

private NumberFormat nf=NumberFormat.getInstance(Locale.ENGLISH);

/** create a new Definition with the model Vector v*/
	

public Iterator<String> iterator() {
	return list.iterator();
}

/** create a new Definition from a String : the first word is the definition name*/
public Definition(String string)
{
list=Util.readWordsInString(string);
}

/** create a new Definition from a String : the first word is the definition name*/
public Definition(String string,Vector<Character> spacers)
{
list=Util.readWordsInString(string,spacers);
}

public Definition(String string,char spacer)
{
	Vector<Character> spacers=new Vector<Character> ();
	spacers.add(spacer);
	list=Util.readWordsInString(string,spacers);
}


/** create a new Definition from a String : the first word is the definition name*/
public Definition(String string, boolean eliminateComments)
{
if (eliminateComments) list=Util.readWordsInString(Util.eliminateCommentsInString(string).toString());
else list=Util.readWordsInString(string);
}


/** create a new empty Definition */
public Definition()
{
}

/** reset the Definition from a String : the first word is the definition name*/
public void resetDefinition(String string)
{
list=Util.readWordsInString(string);
}



public void addWord(String word)
{
list.addElement(word);	

}


	
/** get the name of the definition DEPRECATED*/
public String nom_definition()
{
if (list.size()>=1) return this.list.elementAt(0);else return "empty_def";
}

/** get the name of the definition*/
public String name()
{
if (list.size()>=1) return this.list.elementAt(0); else return "empty_definition!";
}

public String print()
{
StringBuffer c = new StringBuffer();
for (String s:list) c.append(s+" ");
return c.toString();
}
	
	/** put the defininition in a String*/
public String printOLD()
		{
		StringBuffer c = new StringBuffer();
		for (String s:list)
			{
			boolean neadsDelimiter = false;

			if (s.contains(" ") || s.contains("\t") || s.contains("\n") || s.contains("\r"))
				{
				neadsDelimiter = true;
				}

			if (neadsDelimiter)
				{
				c.append("\""+s+"\"\t");
				}
			else
				{
				c.append(s+"\t");
				}
			}
		return c.toString();
		}

	/** put the definition in a String*/
public String toString()
	{
	return print();
	}

/**
 *  @DEPRECATED 
 * see method word
 * */
public String mot(int i)
{
if ( (i<0)||(i>=dim()) ) return null; else return list.elementAt(i);
}
		
/** get the word number i of the definition*/
/** remark: the number 0 is the name of the definition*/ 
public String word(int i)
{
if ( (i<0)||(i>=dim()) ) return null; else return list.elementAt(i);
}



/** get the number of words including the definition name*/
public int dim()
	{
	if (list!=null) return list.size(); else return 0;
	}

/**look for a word in the Definition, return -1 if not existing*/
public int search(String word)
{
for (int i=0;i<dim();i++) if (mot(i).compareTo(word)==0) return i;
return (-1);
}

public boolean hasWord(String word)
{
return (search(word)!=-1);
}

/**look for a word in the Definition starting at index, return -1 if not existing*/
public int search(String word,int startindex)
{
for (int i=startindex;i<dim();i++) if (mot(i).compareTo(word)==0) return i;
return (-1);
}

/**search a value from a name*/
/**the value must follow the name in the definition*/
public double searchValue(String valueName)
{
return searchValue(valueName,0);
}



/**
search a value from a name</p>
the value must follow the name in the definition</p>
default is the default value in case the valueName is not in the definition</p>
*/
public double searchValue(String valueName,double defaut,boolean alert)
{
double r=0;
int i=search(valueName);
if (i==-1) 
	{
	if (alert) Messager.messErr(getClass()+Messager.getString("Ne_peux_trouver")+" "+valueName+" "
			+Messager.getString("dans_definition")+" "+nom_definition()
			 +"\n"+Messager.getString("vpardefaut")+ defaut);
	return defaut;
	}
	
try
	{
	r=(nf.parse(word(i+1),new ParsePosition(0))).doubleValue();
	}
catch (Exception e)
	{
	if (alert) Messager.messErr(getClass()+" "+Messager.getString("Erreur_convertion_nombre")+" "+valueName+" "+word(i+1)+" "
		+Messager.getString("dans_definition")+"\n"+this+"\n"+e);
	e.printStackTrace();
	return defaut;
	}
return r;
}

/**
search a value from a name</p>
the value must follow the name in the definition</p>
default is the default value in case the valueName is not in the definition</p>
*/
public double searchValue(String valueName,double defaut)
{
return searchValue(valueName,defaut,true);
}

/**
search a numerical value at the internal index</p>
return default if index out of boundaries or the word is not a numerical value</p>
*/
public double getIndexValue(double defaut)
{
return this.getValue(this.getIndex(), defaut);
}

/**
search a numerical value at the internal index</p>
return default if index out of boundaries or the word is not a numerical value</p>
*/
public double getIndexValueAndIncrement(double defaut)
{
double r= this.getValue(this.getIndex(), defaut);
scanindex++;
return r;
}

/**
search a numerical value at an index</p>
return default if index out of boundaries or the word is not a numerical value</p>
*/
public double getValue(int index,double defaut)
{
double r=0;
if ((index<0)||(index>=dim())) 	
	{
	Messager.messErr(getClass()+" index out of boundaries");
	return defaut;	
	}
try
	{
	r=(nf.parse(word(index),new ParsePosition(0))).doubleValue();
	}
catch (Exception e)
	{
	Messager.messErr(Messager.getString("Erreur_convertion_nombre")+" "+word(index)+" "
		+Messager.getString("dans_definition")+" "+this+" "+e);
	}
return r;
}


/**
search a word from a name</p>
the word must follow the name in the definition</p>
*/
public String searchWord(String wordName)
{
double r=0;
int i=search(wordName);
if (i==-1) 
	{
	Messager.messErr(Messager.getString("Ne_peux_trouver")+" "+wordName+" "
			+Messager.getString("dans_definition")+" "+nom_definition()
			 +"\n");
	return null;
	}
return word(i+1);
}

/**
search a word from a name</p>
the word must follow the name in the definition</p>
*/
public String searchWord(String wordName,boolean check)
{
double r=0;
int i=search(wordName);
if (i==-1)
	{
	if (check) Messager.messErr(Messager.getString("Ne_peux_trouver")+" "+wordName+" "
			+Messager.getString("dans_definition")+" "+nom_definition()
			 +"\n");
	return null;
	}
return word(i+1);
}

/**
search a word from a name</p>
the word must follow the name in the definition</p>
return the default if not found
*/
public String searchWord(String wordName,String defaut)
{
int i=search(wordName);
if (i==-1) return defaut;
else return word(i+1);
}


/**
search for a sub-definition. The form is:  "name { word1 word2 ....}"
*/
public Definition searchSubDefinition(String name,boolean check)
{
//System.out.println(getClass()+" je commence searchSubDefinition");
Definition subdef=new Definition();
subdef.addWord(name);
//System.out.println(getClass()+" je cree la suddef "+subdef);
int index=search(name);
if (index==-1) 
	{
	if (check) Messager.messErr(Messager.getString("Ne_peux_trouver")+" "+name+" "
			+Messager.getString("dans_definition")+" "+nom_definition()
			 +"\n");
	return null;
	}
index+=2;
//System.out.println(getClass()+" je vois "+word(index));
while (word(index).compareTo("}")!=0)
	{
	String mot=word(index);
	if (mot==null) return null;	
	index++;
	subdef.addWord(mot);
	}
return subdef;

}


 /**
search for all  sub-definitions after keyword. The form is:  "keyword { word0 word1 ....}"
*/
public Definition[] searchSubDefinitions(String keyword)
{
Vector v=new Vector();
int index=0;
index=search(keyword,index);

while (index!=-1)
	{
	//go to the next keyword
	index+=2;  // pass thekeyword and the  {
	Definition subdef=new Definition();
	while (word(index).compareTo("}")!=0)
		{
		String mot=word(index);
		if (mot==null) return null;	
		index++;
		subdef.addWord(mot);
		}
	v.addElement(subdef);
	index=search(keyword,index);
	}
if (v.size()==0) return null;
Definition[] ldef= new Definition[v.size()];
v.copyInto(ldef);
return ldef;
}



/**
search for a names list in the definition. The form is:  "name { word1 word2 ....}"
*/
public String[] searchNamesList(String name,boolean check)
{
Vector liste=new Vector();
//System.out.println(getClass()+" je commence searchNamesList "+name);
int index=search(name);
if (index==-1)
	{
	if (check) Messager.messErr(Messager.getString("Ne_peux_trouver")+" "+name+" "
			+Messager.getString("dans_definition")+" "+nom_definition()
			 +"\n");
	return null;
	}
index+=2;
//System.out.println(getClass()+" je vois "+word(index));
while (word(index).compareTo("}")!=0)
	{
	String mot=word(index);
	if (mot==null) return null;	
	index++;
	liste.addElement(mot);
	}
//System.out.println(getClass()+" termine avec "+word(index-1));
//System.out.println(getClass()+" nb de fils "+liste.size());
String[] listeString=new String[liste.size()];
for (int i=0;i<liste.size();i++) listeString[i]=(String) liste.elementAt(i);
//listeString=(String[]) liste.toArray();
//System.out.println("mis dans tableau");
return listeString;

}

//methods to help the scanning of the definition

/**reset the scan index to 0*/
public void reset() {scanindex=0;}

/**
 * return the value of the internal index
 */
public int getIndex()
{
return scanindex;
}

/**give the indexed word*/
public String getIndexedWord()
{
return word(scanindex);
}

/**give the indexed word and increment the index*/
public String getIndexedWordAndIncrement()
{
//System.out.println("word current: "+word(scanindex)+" next: "+word(scanindex+1));
return word(scanindex++);
}
/**
 * set the internal index to the word s
 * @param s
 * @return
 */
public boolean setIndexTo(String s)
{
scanindex=search(s);
if (scanindex==-1) return false;
else return true;
}

/**set the scan index to the next word, return false is end reached*/
public boolean nextWord()
{
if (scanindex==(dim()-1)) return false;
scanindex++;
return true;
}


public Vector<String> getList()
{
	return list;
}

public void append(Definition def)
{
	list.addAll(def.getList());
}

/**get a copy of this def*/
public Definition getAcopy()
{
Definition def2=new Definition();
 for (int i=0;i<dim();i++) def2.addWord(word(i));
 return def2;
}



public void removeWord(String string) 
{
	int index=search(string);
	list.remove(index);
}

public boolean indexAtEnd() {
	return (scanindex>=(dim()-1)) ;
}



public boolean indexOverFlow() {
return (scanindex>(dim()-1)) ;
}


public void _sortInAlphabeticOrder()
{
list.sort(ALPHA_ORDER);
}


public static final Comparator<String> ALPHA_ORDER = new Comparator<String>() 
{
public int compare(String e1, String e2) 
	{
	return e1.compareToIgnoreCase(e2);
    }
};



}// end of class Definition
