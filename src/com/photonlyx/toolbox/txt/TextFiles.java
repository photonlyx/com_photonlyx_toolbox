package com.photonlyx.toolbox.txt;

import java.io.*;
import java.util.*;

import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;

import nanoxml.XMLElement;

import java.net.*;


public  class TextFiles
{

	public static void saveString(String filename,String text)
	{
		saveString(filename,text,true);
	}

	public static void saveString(String filename,String text,boolean verbose)
	{
		saveString(filename,text,true,verbose);
	}

	/** 
	 * To save a String  in a text file
	 * */
	public static void saveString(String filename,String text,boolean alarm,boolean verbose)
	{
		try
		{
			FileOutputStream fo = new FileOutputStream(filename);
			PrintWriter pw = new PrintWriter(fo);
			pw.print(text);
			pw.flush();
			pw.close();
			fo.close();
			if (verbose) System.out.println("TextFiles "+filename+" saved");
		}
		catch (Exception e) 
		{
			if (alarm) Messager.messErr("TextFiles "+Messager.getString("Probleme_ecriture_fichier")+" "+filename);
			if (verbose)  System.out.println("TextFiles "+Messager.getString("Probleme_ecriture_fichier")+" "+filename);
			e.printStackTrace();
		}
	}

	/** To save a String  in a text file*/
	public static void saveStringUTF8(String filename,String text)
	{
		try
		{
			FileOutputStream fo = new FileOutputStream(filename);
			OutputStreamWriter osw=new OutputStreamWriter(fo,"UTF8");
			osw.write(text,0,text.length());
			osw.close();
			fo.close();
			System.out.println("TextFiles "+filename+" saved as UTF8");
		}
		catch (Exception e) 
		{
			Messager.messErr("TextFiles "+Messager.getString("Probleme_ecriture_fichier")+" "+filename);;
		}
	}




	/**read a file and put in a String with line numbers*/
	public String TexteAvecNumLignes(String fileName)
	{
		FileReader r;
		LineNumberReader lnr;
		String s=new String();
		String ligne=new String();
		try 
		{
			r=new FileReader(fileName);
			lnr=new LineNumberReader(r);
			while ((ligne=lnr.readLine())!=null)
			{
				s+=lnr.getLineNumber()+"\t"+ligne+"\n";
				//		System.out.println(ligne);
			}
		}
		catch (EOFException e) 
		{
			return s;
		}
		catch (Exception e) 
		{
			Messager.messErr(Messager.getString("Probleme_ouverture_fichier")+" "+fileName+" "+e);
			return null;
		}
		return s;
	}

	/**read a text file with 2 columns containing x and y real data
fill the x and y arrays*/
	public static void readxyInTextFile(String filename,double [] x,double y[])
	{
		FileReader r;
		StreamTokenizer st;

		try 	
		{
			r= new FileReader(filename);
		}
		catch (Exception e) 
		{
			Messager.messErr(Messager.getString("Probleme_ouverture_fichier")+" "+filename+"\n"+e);
			return;
		}

		st= new StreamTokenizer(r);

		st.wordChars(0x26,0x26); 
		st.wordChars(0x5F,0x5F); 
		st.wordChars(0x7B,0x7B); 
		st.wordChars(0x7D,0x7D); 
		st.slashSlashComments(true);
		st.slashStarComments(true);
		try
		{
			for (int i=0;i<x.length;i++)
			{
				if (st.ttype==st.TT_EOF) 
				{
					Messager.messErr("Le fichier "+filename+" est trop court");
					x=null;
					return;
				}
				while (st.ttype!=st.TT_NUMBER) st.nextToken();
				x[i]=st.nval; 
				st.nextToken();
				while (st.ttype!=st.TT_NUMBER) st.nextToken();
				y[i]=st.nval; 
				st.nextToken();
			}
		}
		catch (Exception e) 
		{
			Messager.messErr(Messager.getString("Probleme_lecture_definition") +"\n"+e);
		}

	}



	public static String readInput(String filename) {

		StringBuffer buffer = new StringBuffer();
		try {
			FileInputStream fis = new FileInputStream(filename);
			InputStreamReader isr = new InputStreamReader(fis,"UTF8");
			Reader in = new BufferedReader(isr);
			int ch;
			while ((ch = in.read()) > -1) 
			{
				//System.out.println(ch+" "+new String(ch));
				buffer.append((char)ch);
			}
			in.close();
			return buffer.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}



	/**read sentences separated by semicolon ; in a file*/
	public static String[] readStrings(String filename)
	{
		Vector list=new Vector();
		StringBuffer buffer = new StringBuffer();

		try
		{
			FileInputStream fis = new FileInputStream(filename);
			InputStreamReader isr = new InputStreamReader(fis,"UTF8");
			Reader in = new BufferedReader(isr);
			int ch;
			ch = in.read();
			while (ch > -1)
			{
				while ( (ch!=';')&&(ch > -1) )
				{
					//System.out.println(ch+" "+(char)ch);
					buffer.append((char)ch);
					ch = in.read();
				}
				//System.out.println(buffer.toString());
				list.addElement(buffer.toString());
				buffer= new StringBuffer();
				ch = in.read();
			}
			in.close();
			String[] strings=new String[list.size()-1];
			for (int i=0;i<strings.length;i++) strings[i]=(String)list.elementAt(i);
			return strings;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}




	/**
remove c++ comments 
@Deprecated
	 */
	public static StringBuffer eliminateComments(String filename)
	{
		StringBuffer buffer = new StringBuffer();

		try
		{
			FileInputStream fis = new FileInputStream(filename);
			InputStreamReader isr = new InputStreamReader(fis,"UTF8");
			Reader in = new BufferedReader(isr);
			int ch1=0,ch2=0;

			ch1=ch2;ch2= in.read();
			while (ch2 > -1)
			{
				ch1=ch2;
				ch2= in.read();
				//System.out.print((char)ch2);
				if ((ch1=='/')&&(ch2=='*'))
				{
					//System.out.println("on voit un /*");
					while (!( (ch1=='*')&&(ch2=='/') ))
					{
						ch1=ch2;ch2= in.read();//System.out.print((char)ch2);
					}
					ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
					ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
				}
				if ((ch1=='/')&&(ch2=='/'))
				{
					//System.out.println("on voit un //");
					while ( !(ch2=='\n') )
					{
						ch1=ch2;ch2= in.read();//System.out.print((char)ch2);
					}
					ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
					//ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
				}
				buffer.append((char)ch1);
			}
			in.close();
			fis.close();
			//buffer.deleteCharAt(0);
			System.out.println("C++ Comments removed");
			return buffer;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

	}





	/**
read a file. put a popup if no file found
	 */
	public static StringBuffer readFile(String filename)
	{
		return readFile(filename,true);
	}



	/**
	 * read a text file in the resources
	 * @param alarm put a popup if no file found
	 */
	public static StringBuffer readFileInResources(String filename,boolean alarm)
	{
		ClassLoader cl=new Util().getClass().getClassLoader();
		URL url=cl.getResource(filename);
		if (url==null)
		{
			if (alarm) Messager.messErr(Messager.getString("Cant_find_txt_file_in_resources:")+" "+filename);
			return null;
		}
		return TextFiles.readFile(url);

	}

	public static StringBuffer readFileInResources2(String filename,boolean alarm)
	{
		InputStream is=TextFiles.class.getResourceAsStream(filename);
		if (is==null)
		{
			if (alarm) Messager.messErr(Messager.getString("Cant_find_txt_file_in_resources:")+" "+filename);
			return null;
		}
		InputStreamReader isr=new InputStreamReader(is);
		StringBuffer buffer=new StringBuffer();
		try
		{
			int ch1=0;
			while (ch1 > -1)
			{
				ch1= isr.read();
				buffer.append((char)ch1);
			}
			isr.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return buffer;
	}


	/**
	 * read a file
	 * @param filename
	 * @param alarm if true launch a popup blocking message if no file found
	 * @return
	 */
	public static StringBuffer readFile(String filename,boolean alarm)
	{
		return readFile(filename,alarm,true);
	}



	/**
	 * read a file
	 * @param filename
	 * @param alarm if true launch a popup blocking message if no file found
	 * @param verbose if true confirm if file is opened
	 * @return
	 */
	public static StringBuffer readFile(String filename,boolean alarm,boolean verbose)
	{
		FileInputStream fis;
		StringBuffer buffer = new StringBuffer();
		try
		{
			fis = new FileInputStream(filename);
		}
		catch (IOException e)
		{
			if (alarm) Messager.messErr(Messager.getString("Probleme_ouverture_fichier")+" "+filename);
			if (verbose) System.out.println("TextFiles"+" "+"Probleme_ouverture_fichier"+" "+filename);
			//e.printStackTrace();
			return null;
		}
		try
		{
			InputStreamReader isr = new InputStreamReader(fis/*,"UTF8"*/);
			Reader in = new BufferedReader(isr);
			int ch1=0;

			//			while (ch1 > -1)
			//			{
			//				ch1= in.read();
			//				buffer.append((char)ch1);
			//			}
			for (;;)
			{
				ch1= in.read();
				if (ch1==-1) break;
				else buffer.append((char)ch1);
			}
			in.close();
			//buffer.deleteCharAt(0);
			if (verbose) System.out.println( "TextFiles"+" "+filename+" read");
			return buffer;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * read a txt file at the url
	 */
	public static StringBuffer readFileAtURL(String urlString)
	{

		try
		{
			URL url=new URL(urlString);	
			return readFile(url);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}


	/**
	 * read a file
	 */
	public static StringBuffer readFile(URL url)
	{
		InputStream is;
		StringBuffer buffer = new StringBuffer();

		try
		{
			is = url.openStream();
		}
		catch (IOException e)
		{
			Messager.messErr(Messager.getString("Problem_reading_url")+" "+url);
			//e.printStackTrace();
			return null;
		}

		try
		{
			InputStreamReader isr = new InputStreamReader(is/*,"UTF8"*/);
			Reader in = new BufferedReader(isr);
			int ch1=0;

			while (ch1 > -1)
			{
				ch1= in.read();
				buffer.append((char)ch1);
			}
			in.close();
			//buffer.deleteCharAt(0);
			//System.out.println(url+" read");
			return buffer;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

	}



	/**read the fist line of a file*/
	public static String readFirstLine(String filename)
	{
		try
		{
			FileInputStream fis = new FileInputStream(filename);
			InputStreamReader isr = new InputStreamReader(fis,"UTF8");
			LineNumberReader lnr=new LineNumberReader(isr);
			String s= lnr.readLine();
			return s;
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * read a csv file ( matrix of strings with coma as separator)
	 * @param fullFileName
	 * @param alarm
	 * @return
	 */
	public static String[][] readCSV(String fullFileName,boolean alarm,String separator,boolean verbose)
	{
		StringBuffer sb=TextFiles.readFile(fullFileName, alarm,verbose);
		if (sb==null) return null;
		String[] lines=StringSource.readLines(sb.toString());
		String[][] m=new String[lines.length][];
		for (int i=0;i<lines.length;i++)
		{
			//if (lines[i].contains("JCC")) System.out.println(lines[i]);
			m[i]=lines[i].split(separator);
		}
		return m;
	}
	
	/**
	 * read a csv file ( matrix of strings with coma as separator)
	 * @param fullFileName
	 * @param alarm
	 * @return
	 */
	public static Vector<Vector<String>> readCSV1(String fullFileName,boolean alarm,String separator,boolean verbose)
	{
		Vector<Vector<String>> v=new Vector<Vector<String>>();
		StringBuffer sb=TextFiles.readFile(fullFileName, alarm,verbose);
		if (sb==null) return null;
		String[] lines=StringSource.readLines(sb.toString());
		for (int i=0;i<lines.length;i++)
		{
			//if (lines[i].contains("JCC")) System.out.println(lines[i]);
			String[] line=lines[i].split(separator);
			Vector<String> line1=new Vector<String>();
			v.add(line1);
			for (int j=0;j<line.length;j++) line1.add(line[j]);
		}
		return v;
	}

	/**
	 * read a csv file ( matrix of strings with coma as separator)
	 * @param fullFileName
	 * @param alarm
	 * @param separator , or tab 
	 * @return
	 */
	public static String[][] readCSV(String fullFileName,boolean alarm)
	{
		return readCSV(fullFileName,alarm,",",true);
	}
	public static String[][] readCSV(String fullFileName,boolean alarm,String separator)
	{
		return readCSV(fullFileName,alarm,separator,true);
	}


	public static String[] readLines(String fullFileName,boolean alarm,boolean verbose)
	{
		String s=TextFiles.readFile(fullFileName, alarm, verbose).toString();
		return StringSource.readLines(s);
	}

	public static XMLElement readXMLfile(String fullFileName,boolean alarm,boolean verbose)
	{
		StringBuffer sb=TextFiles.readFile(fullFileName,alarm,verbose);
		if (sb==null) return null;
		String s=sb.toString();
		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(s);
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) 
		{
			ex.printStackTrace();
			return null;
		}
		return xml;
	}


	public static String tail2( File file, int lines) {
		java.io.RandomAccessFile fileHandler = null;
		try {
			fileHandler = 
					new java.io.RandomAccessFile( file, "r" );
			long fileLength = fileHandler.length() - 1;
			StringBuilder sb = new StringBuilder();
			int line = 0;

			for(long filePointer = fileLength; filePointer != -1; filePointer--){
				fileHandler.seek( filePointer );
				int readByte = fileHandler.readByte();

				if( readByte == 0xA ) {
					if (filePointer < fileLength) {
						line = line + 1;
					}
				} else if( readByte == 0xD ) {
					if (filePointer < fileLength-1) {
						line = line + 1;
					}
				}
				if (line >= lines) {
					break;
				}
				sb.append( ( char ) readByte );
			}

			String lastLine = sb.reverse().toString();
			return lastLine;
		} catch( java.io.FileNotFoundException e ) {
			e.printStackTrace();
			return null;
		} catch( java.io.IOException e ) {
			e.printStackTrace();
			return null;
		}
		finally {
			if (fileHandler != null )
				try {
					fileHandler.close();
				} catch (IOException e) {
				}
		}
	}

/**
 * return the nb of lines of the file
 * @param url
 * @return
 */
	public static long nbLines(File file)
	{
		long nbl=0;
		try {
			Scanner myReader = new Scanner(file);
			while (myReader.hasNextLine()) 
			{
				String data = myReader.nextLine();
				//System.out.println(data);
				nbl++;
			}
			myReader.close();
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		return nbl;
	}


}
