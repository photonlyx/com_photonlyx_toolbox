// Author Laurent Brunel

package com.photonlyx.toolbox.txt;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.*;

import com.photonlyx.toolbox.util.Messager;

/**
an object that can deliver a String. Able to fill 2 arrays for x,y from the string. remove c c++ comments.
*/
public abstract class StringSource
{

public abstract String getString();

/**
replace the arrays x and y by filled arrays with the first 2 columns in the string s of this StringSource
*/
public double[][] fillWithTwoFirstColumns()
{
return fillWithTwoFirstColumns(getString());
}

/**
replace the arrays x and y by filled arrays with  2 columns having good names in the string s of this StringSource
*/
public double[][] fillWithTwoColumns(String namex,String namey)
{
return fillWithTwoColumns(getString(),namex,namey);
}
/**
return an array with 2 columns of the string s named namex and namey
*/
public static double[][] fillWithTwoColumns(String s,String namex,String namey)
{
int nbc=nbColumns(s);
//System.out.println("nbColumns="+nbc);
if (nbc<2)
	{
	Messager.messErr("Pas_assez_de_colonnes");
	return null;
	}
int nbl=nbLines(s,false)-1;
//System.out.println("nbLines="+(nbl+1));
if (nbl<=0) return null;
int xindex=indexOf(s,namex);
int yindex=indexOf(s,namey);
if (xindex>=nbc)
	{
	Messager.messErr(" pas colonne de nom "+namex);
	return null;
	}
//System.out.println("index of "+ namey+"="+yindex);
if (yindex>=nbc)
	{
	Messager.messErr(" pas colonne de nom "+namey);
	return null;
	}

s=s.substring(s.indexOf("\n"));//remove the first line

double[][] data=fill(s);
double[][] data2Col=new double[2][nbl];
//System.out.println("index of "+ namex+"="+xindex);
//System.out.println("xindex of "+namex+" ="+xindex);
//System.out.println("yindex of "+namey+" ="+yindex);
for (int l=0;l<nbl;l++)
	{
	//System.out.println("l="+l);
	data2Col[0][l]=data[l][xindex];
	data2Col[1][l]=data[l][yindex];
	}
return data2Col;
}


/**
return an array with the data of the string s  
array[row][col]
@param string the string to read
@return the array filled with the value, the first index is row, the second is column
*/
public static double[][] fillMatrix(String string)
{
return fillMatrix( string,false);
}

/**
return an array with the data of the string s  
array[row][col]
@param string the string to read
@param lengendInFirstLine if true consider the first line as the names of the columns
@return the array filled with the value, the first index is row, the second is column
*/
public static double[][] fillMatrix(String string,boolean lengendInFirstLine)
{
int nbc=nbColumns(string);
//System.out.println("nbColumns="+nbc);
if (nbc<2)
	{
	Messager.messErr("Pas_assez_de_colonnes");
	return null;
	}
if (lengendInFirstLine) 
	{
	string=string.substring(string.indexOf("\n")+1, string.length());
	}
int nbl=nbLines(string,false);
//System.out.println("nbLines="+(nbl));
if (nbl<=0) return null;
double[][] data=new double[nbl][nbc];
String s="";
StringReader r;
//r= new StringReader(eliminateComments(string));
r= new StringReader(string);
LineNumberReader lnr=new LineNumberReader(r);
double[] rowdata;
// System.out.println("fill: nblines: "+nbl);
try
	{
	for (int i=0;i<nbl;i++)
		{
		s= lnr.readLine();
		//System.out.println(i+"   "+s);
		rowdata=readNumericalValuesInString(s);
		if (rowdata.length==nbc)for (int j=0;j<nbc;j++)
			{
			data[i][j]=rowdata[j];
			//System.out.println(i+" "+j+"   "+rowdata[j]);
			}
		}
	}
catch(Exception e)
	{
	Messager.messErr("la ligne qui suit :"+s+"\n"+e);
	}
r.close();
//System.out.println("fill:  done!");
return data;
}




/**
replace the arrays x and y by filled arrays with the first 2 columns in the string s
*/
public static double[][] fillWithTwoFirstColumns(String s)
{
StringReader r;
StreamTokenizer st;
r= new StringReader(s);
st= new StreamTokenizer(r);
st.wordChars(0x26,0x26);
st.wordChars(0x5F,0x5F);
st.wordChars(0x7B,0x7B);
st.wordChars(0x7D,0x7D);
st.slashSlashComments(true);
st.slashStarComments(true);
int dim=NbNumericalValuesInString(s)/2;
//System.out.println(" fillWithString: dim:"+dim);
double[][] data2Col=new double[2][dim];
try
	{
	for (int i=0;i<dim;i++)
		{
		if (st.ttype==StreamTokenizer.TT_EOF)
			{
			Messager.messErr("La chaine  est trop courte");
			return null;
			}
		while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
		data2Col[0][i]=st.nval;
		//System.out.println(i+" x="+data2Col[0][i]);
		st.nextToken();
		while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
		data2Col[1][i]=st.nval;
		//System.out.println(i+" y="+data2Col[1][i]);
		st.nextToken();
		}
	}
catch (Exception e)
	{
	Messager.messErr(Messager.getString("Probleme_lecture_chaine") +"\n"+e);
	return null;
	}
r.close();
return data2Col;
}


/**
return the number of numerical values in a string (c c++ comments removed)
*/
public static int NbNumericalValuesInString(String s)
{
return NbWordsInString(s,true);
}

/**
return the number of words in a string (c c++ comments removed)
*/
public static int NbWordsInString(String s)
{
return NbWordsInString(s,false);
}

/**
return the number of words in a string (c c++ comments removed)
*/
private static int NbWordsInString(String s,boolean numerical)
{
if (s==null) return 0;
StringReader r;
StreamTokenizer st;
r= new StringReader(s);
st= new StreamTokenizer(r);
st.wordChars(0x26,0x26);
st.wordChars(0x5F,0x5F);
st.wordChars(0x7B,0x7B);
st.wordChars(0x7D,0x7D);
st.slashSlashComments(true);
st.slashStarComments(true);
int compteur=0;
//System.out.println("NbWordsInString de : "+s+"fin");
//System.out.println("numerical : "+numerical);
try
{
while (st.ttype!=StreamTokenizer.TT_EOF)
	{
	st.nextToken();
	if (numerical)
		{
		if (st.ttype==StreamTokenizer.TT_NUMBER)
			{
			compteur++;
			//if (st.ttype==st.TT_NUMBER) System.out.println("numerical "+compteur+" "+st.nval);
			//if (st.ttype==st.TT_WORD) System.out.println("numerical "+compteur+" "+st.sval);
			}
		}
	if (!numerical)
		{
		compteur++;
		//if (st.ttype==st.TT_NUMBER) System.out.println("NOT numerical "+compteur+" "+st.nval);
		//if (st.ttype==st.TT_WORD) System.out.println("NOT numerical "+compteur+" "+st.sval);
		}
	}
}
catch(Exception e) {;}
r.close();
if (!numerical) compteur--;
return compteur;
}

/**
fill the numerical values of the string in the array (c c++ comments removed)
*/
public static double[]  readNumericalValuesInString(String s)
{
Definition def=new Definition(s);
double[] data=new double[def.dim()];
for (int i=0;i<def.dim();i++)  
	{
	try
	{ data[i]=new Double(def.word(i)).doubleValue();}
	catch(Exception e){data[i]=0;}
	}
return data;
}
/**
fill the numerical values of the string in the array (c c++ comments removed)
*/
public static void fillWithNumericalValuesInStringOLD(String s,double[] linedata)
{
StringReader r;
StreamTokenizer st;
r= new StringReader(s);
st= new StreamTokenizer(r);
st.wordChars(0x26,0x26);
st.wordChars(0x5F,0x5F);
st.wordChars(0x7B,0x7B);
st.wordChars(0x7D,0x7D);
st.slashSlashComments(true);
st.slashStarComments(true);
int colindex=0;
//System.out.println("fill with "+s);
try
{
for (int i=0;i<linedata.length;i++)
	if (st.ttype!=StreamTokenizer.TT_EOF)
		{
		st.nextToken();
		if (st.ttype==StreamTokenizer.TT_NUMBER)
			{
			linedata[colindex++]=st.nval;
			//System.out.println("index="+(colindex-1)+" "+st.nval);
			}
		}
}
catch(Exception e) {;}
r.close();
}

/**
 * return the first line of the string
 * @param string
 * @return
 */
public static String firstLine(String string)
{
String s="";
StringReader r;
r= new StringReader(eliminateComments(string));
LineNumberReader lnr=new LineNumberReader(r);
try
	{
	s= lnr.readLine();
	}
catch(Exception e) {Messager.messErr("Ne_peux_lire la ligne qui suit :"+s);}
r.close();
//System.out.println("string:\n"+s+"\n");
//System.out.println("first line:\n"+s);
return s;
}

/**give the index of the column with that name*/
public static int indexOf(String string,String name)
{
String s=firstLine(string);
StringReader r;
StreamTokenizer st;
r= new StringReader(s);
st= new StreamTokenizer(r);
st.wordChars(0x26,0x26);
st.wordChars(0x5F,0x5F);
st.wordChars(0x7B,0x7B);
st.wordChars(0x7D,0x7D);
st.slashSlashComments(true);
st.slashStarComments(true);
int colindex=-1;
try
{
while (st.ttype!=StreamTokenizer.TT_EOF)
	{
	st.nextToken();
	System.out.println(st.sval);
	colindex++;
	if (name.compareTo(st.sval)==0) break ;
	}
}
catch(Exception e) {;}
r.close();
return colindex;
}


/**
 * 
 * @param s
 * @param eliminateComments
 * @return
 */
public static int nbLines(String s,boolean eliminateComments)
{
int compteur=0;
String ss="";
StringReader r;
if (eliminateComments) r= new StringReader(eliminateComments(s));
else  r= new StringReader(s);
LineNumberReader lnr=new LineNumberReader(r);
//System.out.println("Count the lines:  ");
try
	{
	while (ss!=null)
		{
		ss= lnr.readLine();
		compteur++;
		//System.out.println("line "+compteur+":   "+ss);
		}
	}
catch(Exception e)
	{
	Messager.messErr(" ne_peux_lire la ligne qui suit :"+ss);
	}
r.close();
return compteur-1;
}

public static int nbColumns(String string)
{
String s=firstLine(string);
//System.out.println("first line: "+s);
Definition def=new Definition(s);
return def.dim();
//return NbWordsInString(f);
}

/**
 * fill a matrix of data from a string containing a matrix of numbers
 * @param string
 * @param data
 */
public static double[][]  fill(String string)
{
String s="";
StringReader r;
r= new StringReader(eliminateComments(string));
LineNumberReader lnr=new LineNumberReader(r);

//read
int nbc=StringSource.nbColumns(string);
int nbl=StringSource.nbLines(string,false);
System.out.println("fa.reuse.txt.StringSource"+" fill a double matrix with a text matrix: nblines: "+nbl+" nbcol: "+nbc);
double[][] data=new double[nbl][nbc];

//double[] rowdata=new double[data.length];
double[] rowdata=new double[nbc];
try
	{
	//read the first line with the names
	//s= lnr.readLine();
	//System.out.println("Premiere ligne: "+s);
	//for (int i=0;i<data[0].length;i++)
	for (int i=0;i<nbl;i++)
		{
		s= lnr.readLine();
		//System.out.println(i+"   "+s);
		//int l=NbNumericalValuesInString(s);
		//fillWithNumericalValuesInString(s,rowdata);
		rowdata=readNumericalValuesInString(s);
		for (int j=0;j<nbc;j++)
			{
			data[i][j]=rowdata[j];
			//System.out.println(j+"   "+rowdata[j]);
			}
		}
	}
catch(Exception e)
	{
	Messager.messErr("la ligne qui suit :"+s+"\n"+e);
	}
r.close();
System.out.println("fill:  done!");
return data;
}


/**
put the lines in an array of strings
eliminates the c c++ comments
@param string : the string to read
@return the array with the lines
*/
public static String[] readLines(String string)
{
return readLines(string,true);
}
/**
put the lines in an array of strings
eliminates the c c++ comments
* @param string : the string to read
* @param eliminateComments: if true eliminate c++ like comments
* @return the array with the lines
*/
/**
 * 
 * @param string
 * @return
 */
public static String[] readLines(String string,boolean eliminateComments)
{
String s="";
StringReader r;
if (eliminateComments) r= new StringReader(eliminateComments(string));
else r= new StringReader(string);
LineNumberReader lnr=new LineNumberReader(r);
Vector<String> v=new Vector<String>();
try
	{
	int c=1;
	for (;;)
		{
		s= lnr.readLine();
		//System.out.println("line "+(c++)+"   "+s);
		if (s==null) break;
		v.addElement(s);
		}
	}
catch(Exception e)
	{
	Messager.messErr("readLines"+" "+"erreur"+" la ligne qui suit :"+s+"\n"+e);
	}
r.close();
String[] sts= new String[v.size()];
v.copyInto(sts); 
return sts;
}


/**
 * separe the string in an array of strings using the separator 
 * @param separator
 * @return
 */
public static Vector<String> readBlocks(String s,char separator)
{
//StringBuffer buffer = new StringBuffer();
Vector<String> v=new Vector<String>();
try
{
StringReader in = new StringReader(s);
int ch1=0;
ch1= in.read();	
while (ch1 > -1)
	{
	StringBuffer phrase=new StringBuffer();
	while (( ch1!=separator )&&(ch1>-1))
		{
		//System.out.println((char)ch1);
		phrase.append((char)ch1);
		ch1= in.read();	
		}
	//System.out.println(phrase.toString());
	//System.out.println("*"+(char)ch1+"*"+ch1);
	if (ch1!=-1) v.add(phrase.toString());
	ch1= in.read();	
	//System.out.println("*"+(char)ch1+"*"+ch1);
	}
in.close();
}
catch (IOException e)
	{
	e.printStackTrace();
	return null;
	}
return v;
}



/**
remove c++ comments
*/
public static String eliminateComments(String s)
{
StringBuffer buffer = new StringBuffer();

try
{
StringReader in = new StringReader(s);
int ch1=0,ch2=0;
/*System.out.println();
System.out.println("debuts"+s+"fins");
System.out.println();
System.out.print("debutsbis");*/

ch1=ch2;ch2= in.read();
while (ch2 > -1)
	{
	ch1=ch2;
	ch2= in.read();
	//System.out.print((char)ch2);
	if ((ch1=='/')&&(ch2=='*'))
		{
		//System.out.println("on voit un /*");
		while (!( (ch1=='*')&&(ch2=='/') ))
			{
			ch1=ch2;ch2= in.read();//System.out.print((char)ch2);
			}
		ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
		ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
		}
	else if ((ch1=='/')&&(ch2=='/'))
		{
		//System.out.println("on voit un //");
		while ( !(ch2=='\n') )
			{
			ch1=ch2;ch2= in.read();//System.out.print((char)ch2);
			}
		ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
		}
	else buffer.append((char)ch1);
	//System.out.print("append:"+(char)ch1);
	}
//System.out.println("finsbis");
in.close();
//buffer.deleteCharAt(0);
//System.out.println("C++ Comments removed");
return buffer.toString();
}
catch (IOException e)
	{
	e.printStackTrace();
	return null;
	}

}


}
