package com.photonlyx.toolbox.chartApp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import com.photonlyx.toolbox.chart.AnalysePlotSet;
import com.photonlyx.toolbox.chart.Chart;
import com.photonlyx.toolbox.chart.Legend;
import com.photonlyx.toolbox.chart.PlotProcessor;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParStringComboBox;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;


/**
@Deprecated
*/
public class AnalysePanel extends JPanel
{
//private PlotSet profiles;
private Chart profileChart;
private Chart chart;
//private double rmin=0;//mm
//private double rmax=1;//mm
//private Signal1D1DXY signal1=new Signal1D1DXY();
//private Signal1D1DXY signal2=new Signal1D1DXY();
private AnalysePlotSet analysePlotSet=new AnalysePlotSet();
private ParamsBox boxXvalues,boxYvalues;
private ParamsBox boxImageDimvalues;
private JPanel panelSide=new JPanel();
private Legend legend;
private ParamsBox boxXuserDefinedParameter,boxYuserDefinedParameter,boxZuserDefinedParameter;
private ParamsBox boxRange;
private ParStringComboBox pscb_x,pscb_y,pscb_z;


public	AnalysePanel(Chart profileChart)
{
this.profileChart=profileChart;
//profile panel
this.setLayout(new BorderLayout());
this.setFont(Global.font);
this.setBackground(Global.background);

//create the chart for the profile
chart=new Chart();
chart.setTitle("");
chart.setXlabel("");
chart.setXunit("");
chart.setYlabel("");
chart.setYunit("");
chart.setyScientific(true);
chart.setLogx(false);
chart.setLogy(false);
chart.setAdjustBoundariesToTicks(true);	
//add the chart at the center:
this.add(chart,BorderLayout.CENTER);	

chart.removeAllPlots();


//panel for button and legend
panelSide.setPreferredSize(new Dimension(150, 200));
panelSide.setLayout(new FlowLayout());
panelSide.setBackground(Global.background);
//add the side panel at right
this.add(panelSide,BorderLayout.EAST);


//button
TriggerList tl_button=new TriggerList();
tl_button.add(new Trigger(this,"analyse"));
tl_button.add(new Trigger(chart,"update"));
tl_button.add(new Trigger(legend,"update"));
CButton cButtonBMPs=new CButton(tl_button);
cButtonBMPs.setText("Analyse");
panelSide.add(cButtonBMPs);

TriggerList tl_xy=new TriggerList();
tl_xy.add(new Trigger(this,"setMenus"));
tl_xy.add(new Trigger(this,"analyse"));
tl_xy.add(new Trigger(chart,"update"));
tl_xy.add(new Trigger(legend,"update"));

ParString par_x=new ParString(analysePlotSet,"xUserParameter");
 pscb_x=new ParStringComboBox(tl_xy,par_x);
pscb_x.setLabel("X");
pscb_x.setSize(180,30);
//pscb_x.addItem("x");
//pscb_x.addItem("yValue");
//pscb_x.addItem("xValue");
//pscb_x.addItem("index");
//pscb_x.addItem("time");
//pscb_x.addItem("lambda");
//pscb_x.addItem("user defined");
//for (String s:analysePlotSet.getAllProcessNames()) pscb_x.addItem(s);
panelSide.add(pscb_x.getComponent());

ParString par_y=new ParString(analysePlotSet,"yUserParameter");
 pscb_y=new ParStringComboBox(tl_xy,par_y);
pscb_y.setLabel("Y");
pscb_y.setSize(180,30);
//pscb_y.addItem("yValue");
//pscb_y.addItem("xValue");
//pscb_y.addItem("x");
//pscb_y.addItem("y");
//pscb_y.addItem("integralProfile");
//pscb_y.addItem("spotSize");
//pscb_y.addItem("lstar");
//pscb_y.addItem("user defined");
//for (String s:analysePlotSet.getAllProcessNames()) pscb_y.addItem(s);
panelSide.add(pscb_y.getComponent());

ParString par_z=new ParString(analysePlotSet,"zUserParameter");
 pscb_z=new ParStringComboBox(tl_xy,par_z);
pscb_z.setLabel("Z");
pscb_z.setSize(180,30);
//pscb_z.addItem("none");
//pscb_z.addItem("xValue");
//pscb_z.addItem("yValue");
//pscb_z.addItem("integralProfile");
//pscb_z.addItem("user defined");
//for (String s:analysePlotSet.getAllProcessNames()) pscb_z.addItem(s);
panelSide.add(pscb_z.getComponent());

//ParString par_z=new ParString(aps,"zUserParameter");
//ParStringComboBox pscb_z=new ParStringComboBox(tl_button,par_z);
//pscb_z.setLabel("Color");
//pscb_z.setSize(180,30);
//pscb_z.addItem("yValue");
//pscb_z.addItem("xValue");
//pscb_z.addItem("mean");
//panelSide.add(pscb_y.getComponent());
//

//box with the processing parameters:
//TriggerList trig=new TriggerList();
//trig.add(new Trigger(aps,"analyse"));
String[] namesx={"xValue"};
boxXvalues=new ParamsBox(analysePlotSet,tl_button,namesx);
String[] namesy={"yValue"};
boxYvalues=new ParamsBox(analysePlotSet,tl_button,namesy);

String[] namesdimImage={"ImageDimx","ImageDimy"};
boxImageDimvalues=new ParamsBox(analysePlotSet,tl_button,namesdimImage);

String[] names2={"Filter","Separate","Average","SortResult"};
ParamsBox box2=new ParamsBox(analysePlotSet,tl_button,names2);
panelSide.add(box2.getComponent());

String[] names3x={"xUserParameter"};
boxXuserDefinedParameter=new ParamsBox(analysePlotSet,tl_button,names3x);
String[] names3y={"yUserParameter"};
boxYuserDefinedParameter=new ParamsBox(analysePlotSet,tl_button,names3y);
String[] names3z={"zUserParameter"};
boxZuserDefinedParameter=new ParamsBox(analysePlotSet,tl_button,names3z);


String[] names4={"xMinRange","xMaxRange","yMinRange","yMaxRange",};
boxRange=new ParamsBox(analysePlotSet,tl_button,names4);

//legend
legend=new Legend(chart);
legend.getComponent().setPreferredSize(new Dimension(150, 200));
//legend.setChart(chart);
panelSide.add(legend.getComponent());
legend.update();

PlotSet result=analysePlotSet.getResultPlotSet();
chart.add(result);


setMenus();

}

/**
 * "index","time","yValue" ,"xValue","mean", "centerValue" ,"integral", "integralProfile" ,"thickness",
"centerSlope"  ,"rms" , "powerLaw", "picXposition" ,
"meanSlope", "spotSize"
 * @param s
 */
public void addXCalculation(String s)
{
pscb_x.addItem(s);	
setMenus();
}
/**
 * "index","time","yValue" ,"xValue","mean", "centerValue" ,"integral", "integralProfile" ,"thickness",
"centerSlope"  ,"rms" , "powerLaw", "picXposition" ,
"meanSlope", "spotSize"
 * @param s
 */
public void addYCalculation(String s)
{
pscb_y.addItem(s);	
setMenus();
}
/**
 * "index","time","yValue" ,"xValue","mean", "centerValue" ,"integral", "integralProfile" ,"thickness",
"centerSlope"  ,"rms" , "powerLaw", "picXposition" ,
"meanSlope", "spotSize"
 * @param s
 */
public void addZCalculation(String s)
{
pscb_z.addItem(s);	
setMenus();
}

public void addPlotProcessor(PlotProcessor pp)
{
analysePlotSet.addPlotProcessor(pp);
}

public void setMenus()
{
//System.out.println("setMenus");
//remove boxes
if (panelSide==null) return;
if (boxXvalues!=null)panelSide.remove(boxXvalues.getComponent());
if (boxYvalues!=null) panelSide.remove(boxYvalues.getComponent());
if (boxImageDimvalues!=null)panelSide.remove(boxImageDimvalues.getComponent());
if (boxXuserDefinedParameter!=null)panelSide.remove(boxXuserDefinedParameter.getComponent());
if (boxYuserDefinedParameter!=null)panelSide.remove(boxYuserDefinedParameter.getComponent());
if (boxZuserDefinedParameter!=null)panelSide.remove(boxZuserDefinedParameter.getComponent());
if (boxRange!=null)panelSide.remove(boxRange.getComponent());
if (legend!=null)panelSide.remove(legend.getComponent());

//put boxes:
if (boxYvalues!=null)
	if ((analysePlotSet.getxUserParameter().compareTo("xValue")==0) 
	||(analysePlotSet.getyUserParameter().compareTo("xValue")==0) 
	||(analysePlotSet.getzUserParameter().compareTo("xValue")==0) 
	)
	 panelSide.add(boxYvalues.getComponent(),4);

if (boxXvalues!=null)
if ((analysePlotSet.getxUserParameter().compareTo("yValue")==0) 
		||(analysePlotSet.getyUserParameter().compareTo("yValue")==0) 
		||(analysePlotSet.getzUserParameter().compareTo("yValue")==0) 
		)
panelSide.add(boxXvalues.getComponent(),4);

//user defined param boxes:
if ((analysePlotSet.getxUserParameter().compareTo("user defined")==0) )
	panelSide.add(boxXuserDefinedParameter.getComponent());
if ((analysePlotSet.getyUserParameter().compareTo("user defined")==0) )
	panelSide.add(boxYuserDefinedParameter.getComponent());
if ((analysePlotSet.getzUserParameter().compareTo("user defined")==0) )
	panelSide.add(boxZuserDefinedParameter.getComponent());

//range box:
//panelSide.add(boxRange.getComponent());

if (legend!=null)
	if (analysePlotSet.getzUserParameter().compareTo("none")!=0) 
		panelSide.add(boxImageDimvalues.getComponent(),4);
	else 
		{
		panelSide.add(legend.getComponent());
		}



panelSide.validate();
panelSide.repaint();
}



public void analyse()
{
analysePlotSet.setxMinRange(profileChart.getZoneXmin());
analysePlotSet.setxMaxRange(profileChart.getZoneXmax());
analysePlotSet.setyMinRange(profileChart.getZoneYmin());
analysePlotSet.setyMaxRange(profileChart.getZoneYmax());
//boxRange.update();

//System.out.println("ZoneXmin="+profileChart.getZoneXmin());
//System.out.println("ZoneXmax="+profileChart.getZoneXmax());
//if (profileChart.getZoneXmin()!=profileChart.getZoneXmax())
//	{
//	adjustModelLstarFlux.setRmin(profileChart.getZoneXmin());
//	adjustModelLstarFlux.setRmax(profileChart.getZoneXmax());
//	}
//else 
//	{
//	adjustModelLstarFlux.setRmin(profileChart.getXmin());
//	}

analysePlotSet.setAnalysedPlotSet(profileChart.getConcatenatedPlots());
analysePlotSet.analyse();
//chart.removeAllPlots();
chart.setSignal2D1D(null);

if (analysePlotSet.getzUserParameter().compareTo("none")!=0)//print an image
	{
	chart.setSignal2D1D(analysePlotSet.getImage());
	//chart.setKeepXYratio(true);
	}
else 
	{
	PlotSet ps=analysePlotSet.getResultPlotSet();
	chart.add(ps);
	}

chart.setXlabel(analysePlotSet.getxUserParameter());
chart.setYlabel(analysePlotSet.getyUserParameter());

if (analysePlotSet.getyUserParameter().compareTo("lstar")==0)
	{
	chart.setSeeXaxis(true);
	chart.setYunit("µm");
	chart.setFormatTicky("0");
	chart.setyScientific(false);
	}

if (analysePlotSet.getyUserParameter().compareTo("spotSize")==0) 
	{
	chart.setSeeXaxis(true);
	chart.setYunit("mm");
	chart.setFormatTicky("0.00");
	chart.setyScientific(false);
	}

chart.setFormatTickx("0.000");
if (analysePlotSet.getxUserParameter().compareTo("lambda")==0) 
	{
	chart.setFormatTickx("0");
	chart.setXunit("µm");
	}

if (legend!=null) legend.update();

}


/**
 * x yValue xValue index time lambda "user defined" integralProfile spotSize lstar
 * @param s
 */
public void setXparameter(String s)
{
analysePlotSet.setxUserParameter(s);	
setMenus();
}
/**
 * x yValue xValue index time lambda "user defined" integralProfile spotSize lstar
 * @param s
 */
public void setYparameter(String s)
{
analysePlotSet.setyUserParameter(s);	
setMenus();
}
/**
 * x yValue xValue index time lambda "user defined" integralProfile spotSize lstar
 * @param s
 */
public void setZparameter(String s)
{
analysePlotSet.setzUserParameter(s);	
setMenus();
}





//
//public double getRmin()
//{
//	return rmin;
//}
//
//
//
//public void setRmin(double rmin)
//{
//	this.rmin = rmin;
//}
//
//
//
//public double getRmax()
//{
//	return rmax;
//}
//
//
//
//public void setRmax(double rmax)
//{
//	this.rmax = rmax;
//}






}
