package com.photonlyx.toolbox.threeD.rendering;

import com.photonlyx.toolbox.math.geometry.Vecteur;

public class PointLightSource extends LightSource
{
private Vecteur o=new Vecteur();//source location


/** get the illumination vector*/
public Vecteur getIl(Vecteur p)
{
return p.sub(o);
}

/**
 * set source position
 * @return
 */
public Vecteur getO()
{
return o;
}

/**
 * get source position
 * @param o
 */
public void setO(Vecteur o)
{
this.o = o;
}

public double getX(){return o.x();}
public void setX(double d){this.o.setX(d);}
public double getY(){return o.y();}
public void setY(double d){this.o.setY(d);}
public double getZ(){return o.z();}
public void setZ(double d){this.o.setZ(d);}




}
