package com.photonlyx.toolbox.threeD.rendering;

import com.photonlyx.toolbox.math.geometry.Vecteur;




public abstract class LightSource
{
private double sourcePower=1;//total power emitted

public abstract Vecteur getIl(Vecteur p);


public double getSourcePower()
{
return sourcePower;
}



public void setSourcePower(double sourcePower)
{
this.sourcePower = sourcePower;
}




}
