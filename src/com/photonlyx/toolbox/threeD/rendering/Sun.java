package com.photonlyx.toolbox.threeD.rendering;

import com.photonlyx.toolbox.math.geometry.Rotator;
import com.photonlyx.toolbox.math.geometry.Vecteur;

public class Sun extends LightSource
{
private double angle=35;//angle of illumination vertically degrees
private double angleHor=0;//angle of illumination horizontally degrees
private Vecteur il=new Vecteur(0,0,-1); //illumination vector

/**
 * create sun with light going vertically downwards
 * 
 */
public Sun()
{
this.angle=0;
this.angleHor=0;
updateIlluminationVector();
}

/**
 * configure by turning the Y axis. (0,0) gives the Y axis.
 * @param angle angle of illumination vertically degrees (90==vertical downwards: zenital sun)
 * @param angleHor angle of illumination horizontally degrees( 0==direction of Y axis)
 */
public Sun(double angle,double angleHor)
{
this.angle=angle;
this.angleHor=angleHor;
updateIlluminationVector();
}


private void updateIlluminationVector()
{
double angleRad=angle*Math.PI/180.0;
double angleHorRad=angleHor*Math.PI/180.0;
Rotator r=new Rotator(angleRad,0,angleHorRad);
il=r.turn(new Vecteur(0,1,0));
}

/** get the illumination vector at point p*/
public Vecteur getIl(Vecteur p)
{
return il;
}

/** get the illumination vector */
public Vecteur getIl()
{
return il;
}


/**
 * 
 * @return angle in degrees
 */
public double getAngle()
{
return angle;
}

/**
 * 
 * @param angle in degrees
 */
public void setAngle(double angle)
{
this.angle = angle;
updateIlluminationVector();
}


/**
 * 
 * @return angle in degrees
 */
public double getAngleHor()
{
return angleHor;
}


/**
 * 
 * @param angleHor angle in degrees
 */
public void setAngleHor(double angleHor)
{
this.angleHor = angleHor;
updateIlluminationVector();
}






}
