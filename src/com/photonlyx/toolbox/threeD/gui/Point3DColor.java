package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.rendering.Sun;

public class Point3DColor   extends Vecteur implements Object3DColor
{
	private Color color;
	private int sizeOnScreen=1;
	
	private double[] cooScreen=new double[2];

	public Point3DColor()
	{
		super();
	}

	public Point3DColor(Vecteur _p)
	{
		this.affect(_p);
		color=Color.black;
	}
	public Point3DColor(Vecteur _p,int colorRGB)
	{
		this.affect(_p);
		color=new Color(colorRGB);
	}
	public Point3DColor(Vecteur _p,Color color)
	{
		this.affect(_p);
		this.color=color;
	}

	public Point3DColor(Vecteur _p,Color color,int sizeOnScreen)
	{
		this.affect(_p);
		this.color=color;
		this.sizeOnScreen=sizeOnScreen;
	}


	public void setColor(Color c)
	{
		color=c;

	}

	public Color getColor()
	{return color;}


	public void draw(Graphics g,Projector proj)
	{
		cooScreen=proj.calcCoorEcran(this);
		//System.out.println(this.getClass()+" "+ cooScreen[0]+" "+cooScreen[1]);
		g.setColor(color);
		g.fillRect((int)cooScreen[0]-sizeOnScreen/2, (int)cooScreen[1]-sizeOnScreen/2, sizeOnScreen, sizeOnScreen);
	}
	public void draw(Graphics g, Projector proj, Sun sun)
	{
		this.draw(g,proj);
	}

	public double[] getCooScreen(Projector proj)
	{
		return proj.calcCoorEcran(this);
	}

	/**
	 * get a copy
	 * @return
	 */
	public Point3DColor copy()
	{
		return new Point3DColor(this.copy(),color);
	}



	/**if one of the is out of the cube, extends the cube*/

	public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax)
	{
		for (int k=0;k<3;k++) 
		{
			double r=this.coord(k);
			if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
			if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}	
	}


	public double getDistanceToScreen(Projector proj)
	{
		return 	proj.zCoord(this);
	}

	public Vecteur getP()
	{
		return this;
	}

	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		double d=Math.pow(cooScreen[0]-p.x,2)+Math.pow(cooScreen[1]-p.y,2);
		return (d<5);
	}

	public int getSizeOnScreen() {
		return sizeOnScreen;
	}

	public void setSizeOnScreen(int sizeOnScreen) {
		this.sizeOnScreen = sizeOnScreen;
	}




}
