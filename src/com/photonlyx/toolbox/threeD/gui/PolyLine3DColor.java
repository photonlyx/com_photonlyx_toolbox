package com.photonlyx.toolbox.threeD.gui;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import com.photonlyx.toolbox.math.geometry.PolyLine3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.rendering.Sun;

public class PolyLine3DColor implements Object3DColor
{
	private Color color;
	private boolean seePoints=true;
	private PolyLine3D pol;

	public PolyLine3DColor()
	{
		this.pol=new PolyLine3D();
		color=Color.BLACK;
	}


	public PolyLine3DColor(PolyLine3D pol,Color c)
	{
		super();
		this.pol=pol;
		this.color=c;
	}



	public void setColor(Color c)
	{
		color=c;
	}



	public PolyLine3D getPolyLine3D() {
		return pol;
	}


	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
	{
		for (Vecteur p:pol)
			for (int k=0;k<3;k++) 
			{
				double r=p.coord(k);
				if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
				if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
			}
	}




	public void draw(Graphics g, Projector proj)
	{
		g.setColor(color);
		pol.draw(g, proj);
		
//		int n=pol.getList().size();
//		double[][] cooScreen=new double[2][];
//		for (int i=0;i<n-1;i++)
//		{
//			Vecteur p1=pol.getList().elementAt(i);
//			Vecteur p2=pol.getList().elementAt(i+1);
//			//System.out.println(p1+"    "+p2);
//			cooScreen[0]=proj.calcCoorEcran(p1);
//			cooScreen[1]=proj.calcCoorEcran(p2);
//			//System.out.println(cooScreen[0][0]+" "+cooScreen[0][1]+"     "+cooScreen[1][0]+" "+cooScreen[1][1]);
//			g.setColor(color);
//			g.drawLine((int)cooScreen[0][0], (int)cooScreen[0][1], (int)cooScreen[1][0], (int)cooScreen[1][1]);
//			if (seePoints) g.fillRect((int)cooScreen[0][0]-1, (int)cooScreen[0][1]-1,3,3);
//		}
//		//last point
//		if (pol.nbPoints()>=1)
//		{
//			Vecteur p1=pol.getList().elementAt(n-1);
//			cooScreen[0]=proj.calcCoorEcran(p1);
//			if (seePoints) g.fillRect((int)cooScreen[0][0]-1, (int)cooScreen[0][1]-1,3,3);
//		}
	}

	public void draw(Graphics g, Projector proj, Sun sun)
	{
		this.draw(g,proj);
	}



	public double getDistanceToScreen(Projector proj)
	{
		return 	proj.zCoord(pol.first());
	}



	@Override
	public boolean isOn(Point p, Projector proj)
	{
		// TODO Auto-generated method stub
		return false;
	}





	public boolean isSeePoints()
	{
		return seePoints;
	}



	public void setSeePoints(boolean seePoints)
	{
		this.seePoints = seePoints;
	}








}
