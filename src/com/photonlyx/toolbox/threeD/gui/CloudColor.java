package com.photonlyx.toolbox.threeD.gui;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.cloud.Cloud;
import com.photonlyx.toolbox.math.geometry.cloud.CloudPoint;
import com.photonlyx.toolbox.threeD.rendering.Sun;

public class CloudColor implements Object3DColor
{
	private Color color;
	private int sizeOnScreen=5;;
	private Cloud cloud;

	public CloudColor()
	{
		this.cloud=new Cloud();
		color=Color.BLACK;
	}


	public CloudColor(Cloud pol,Color c)
	{
		super();
		this.cloud=pol;
		this.color=c;
	}



	public void setColor(Color c)
	{
		color=c;
	}




	public Cloud getCloud() {
		return cloud;
	}


	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
	{
		cloud.checkOccupyingCube(cornerMin, cornerMax);
	}




	public void draw(Graphics g, Projector proj)
	{
		g.setColor(color);

		for (CloudPoint cp:cloud)
		{
			double[] cooScreen=new double[2];
			cooScreen=proj.calcCoorEcran(cp.getP());
			g.fillRect((int)(cooScreen[0]-sizeOnScreen/2),(int)( cooScreen[1]-sizeOnScreen/2), sizeOnScreen, sizeOnScreen);
		}
		cloud.draw(g, proj);
	}


	public void draw(Graphics g, Projector proj, Sun sun)
	{
		this.draw(g,proj);
	}


	public double getDistanceToScreen(Projector proj)
	{
		return 	0;
	}



	@Override
	public boolean isOn(Point p, Projector proj)
	{
		// TODO Auto-generated method stub
		return false;
	}










}
