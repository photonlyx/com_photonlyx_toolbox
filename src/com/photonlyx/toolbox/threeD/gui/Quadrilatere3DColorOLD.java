package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3D;
import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.rendering.Sun;



public class Quadrilatere3DColorOLD  implements Object3DColor , Facet
{
	private Vecteur[] p=new Vecteur[4];//positions of vertices in global coordinates
	private Vecteur[] pLocal=new Vecteur[4];
	private Color color;
	private Vecteur centre;
	private boolean filled=false; // if false paint only the frame
	private double[][] cooScreen=new double[4][2];
	public  double x,y,z,ax,ay,az;//position of the object
	//private boolean positionModified=true;


	public Quadrilatere3DColorOLD()
	{
		p=new Vecteur[4];
		for (int i=0;i<4;i++) p[i]=new Vecteur();	
	}

	public Quadrilatere3DColorOLD(Quadrilatere3D q,Color c)
	{
		pLocal[0]=q.p1();	
		pLocal[1]=q.p2();	
		pLocal[2]=q.p3();	
		pLocal[3]=q.p4();		
		this.color=c;
		update();
		calcCentre();
	}

	public Quadrilatere3DColorOLD(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4)
	{
		pLocal[0]=_p1;	
		pLocal[1]=_p2;	
		pLocal[2]=_p3;	
		pLocal[3]=_p4;	
		color=Color.black;
		update();
		calcCentre();
	}

	public Quadrilatere3DColorOLD(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4,int colorRGB)
	{
		pLocal[0]=_p1;	
		pLocal[1]=_p2;	
		pLocal[2]=_p3;	
		pLocal[3]=_p4;	
		update();
		calcCentre();
		color=new Color(colorRGB);
	}

	public Quadrilatere3DColorOLD(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4,Color _color)
	{
		pLocal[0]=_p1;	
		pLocal[1]=_p2;	
		pLocal[2]=_p3;	
		pLocal[3]=_p4;	
		update();
		calcCentre();
		color=_color;
	}
	public Quadrilatere3DColorOLD(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4,Color _color,boolean _filled)
	{
		pLocal[0]=_p1;	
		pLocal[1]=_p2;	
		pLocal[2]=_p3;	
		pLocal[3]=_p4;	
		update();
		calcCentre();
		color=_color;
		filled=_filled;
	}

	public void update()
	{
		calcGlobal();
	}

	private void calcGlobal()
	{
		Vecteur o=new Vecteur(x,y,z);
		for (int j=0;j<4;j++) p[j]=pLocal[j].addn(o);
		//positionModified=false;
	}

	public Vecteur p1(){return p[0];}
	public Vecteur p2(){return p[1];}
	public Vecteur p3(){return p[2];}
	public Vecteur p4(){return p[3];}


	private void calcCentre()
	{
		if (centre==null) 
		{
			centre=p[0].addn(p[1]).addn(p[2]).addn(p[3]).scmul(0.25);
		}
	}

	/**if one of the is out of the cube, extends the cube*/
	public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax)
	{
		for (int j=0;j<4;j++)
		{
			for (int k=0;k<3;k++) 
			{
				double r=p[j].coord(k);
				if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
				if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
			}	
		}
	}

	public Vecteur getNormal()
	{
		Triangle3DColor t=new Triangle3DColor(p[0],p[1],p[2]);
		return t.getNormal();
	}

	public void draw(Graphics g, Projector proj)
	{
		draw( g, proj, null);
	}

	/**
	 * draw the quad 
	 * @param g
	 * @param proj
	 * @param sun light source for rendering
	 */
	public void draw(Graphics g,Projector proj,Sun sun)
	{
		Polygon pol=new Polygon();
		//System.out.println("quad filled="+filled+" sun="+sun);
		for (int j=0;j<4;j++) 
		{
			cooScreen[j]=proj.calcCoorEcranDouble(p[j]);
			pol.addPoint((int)cooScreen[j][0],(int)cooScreen[j][1]);
		}

		if (sun!=null)
		{
			Triangle3DColor t=new Triangle3DColor(p[0],p[1],p[2]);
			//	Vecteur v=sun.getIl();
			//	Vecteur n=t.getNormal();
			//cos of illumination vector:
			float cos_n_il=(float) t.getNormal().mul(sun.getIl().scmul(-1));
			if (cos_n_il<0) cos_n_il=0;
			//System.out.println(p[0]+"   "+n+" cos:"+cos_n_il);
			Color c=new Color(cos_n_il,cos_n_il,cos_n_il);
			g.setColor(c);
			g.fillPolygon(pol);
		}
		else
		{
			if (filled) 
			{
				g.setColor(color);
				g.fillPolygon(pol);
			}
			else
				for (int j=0;j<4;j++) 
				{
					int k=j+1;
					if (k==4) k=0;
					{
						g.setColor(color);
						//g.setColor(Color.black);
						g.drawLine((int)cooScreen[j][0], (int)cooScreen[j][1], (int)cooScreen[k][0], (int)cooScreen[k][1]);
					}
					//g.drawPolygon(pol);
				}
		}
	}



	public double getDistanceToScreen(Projector proj)
	{
		return 	proj.zCoord(centre);
	}

	public Color getColor()
	{
		return color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public Vecteur getCentre()
	{
		return centre;
	}

	public void setCentre(Vecteur centre)
	{
		this.centre = centre;
	}

	public boolean isFilled()
	{
		return filled;
	}

	public void setFilled(boolean filled)
	{
		this.filled = filled;
	}

	public void translate(Vecteur v)
	{
		x+=v.x();
		y+=v.y();
		z+=v.z();
		this.calcGlobal();
	}

	@Override
	public boolean isOn(Point p, Projector proj)
	{
		double[] point=new double[2];
		for (int j=0;j<3;j++) 
		{
			point[0]=p.x;
			point[1]=p.y;
			//distance from point to segment:
			double dist=Segment2D.segdist2D(cooScreen[j], cooScreen[j+1], point);
			if (dist<5) return true;
		}
		return false;
	}

	public Vector<Triangle3D> transformIn2Triangles()
	{
		Vector<Triangle3D> v=new Vector<Triangle3D>();
		Vecteur p1=this.p1();
		Vecteur p2=this.p2();
		Vecteur p3=this.p3();
		Vecteur p4=this.p4();
		Triangle3D t1=new Triangle3D(p1,p2,p4);
		Triangle3D t2=new Triangle3D(p2,p3,p4);
		v.add(t2);
		v.add(t1);
		return v;
	}


}
