package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Iterator;
import java.util.Vector;

import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.math.function.usual.Line;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Point3D;
import com.photonlyx.toolbox.math.geometry.PolyLine3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.Signal3D1D;
import com.photonlyx.toolbox.threeD.rendering.Sun;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;


public class Object3DColorSet implements Iterable<Object3DColor>,Object3DColor
{
	private Vecteur cornermin=new Vecteur();//occupancy cube
	private Vecteur cornermax=new Vecteur();//occupancy cube
	private Vector<Object3DColor> list=new Vector<Object3DColor>();

	public Object3DColorSet() {}

	public Object3DColorSet(Vector<Object3DColor> list)
	{
		for (Object3DColor o:list) add(o);
	}
	public Object3DColorSet(Object3DSet set,Color c)
	{
		add(set,c);
	}
	
	
	public  Vector<Object3DColor> getObject3DList()
	{
		return list;
	}

	public void add(Object3DColor o)
	{
		list.add(o);
	}

	public void remove(Object3DColor o)
	{
		list.remove(o);
	}
	
	public void add(TriangleMesh tris)
	{
		add(tris,Color.WHITE);
	}
	public void add(TriangleMesh tris,Color c)
	{
		//System.out.println(this.getClass()+"add TriangleMesh");
		for (Triangle3D t:tris.getTriangles()) 
			{
			Triangle3DColor tc=new Triangle3DColor(t,c,true);
			list.add(tc);
			}
	}
	
	public void add(Object3DColorSet set)
	{
//		System.out.println("*  add "+set.getClass());
		for (Object3DColor o:set) 
			{
//			System.out.println("       add "+o.getClass());
			//if (o instanceof TriangleMeshSource) this.add((TriangleMeshSource)o);
			list.add(o);
			}
	}

	public void add(Object3DSet set,Color c)
	{
		for (Object3D o:set) 
		{
			Object3DColor oc;
			if (o instanceof Segment3D) 
			{
				oc=new Segment3DColor((Segment3D)o,c);
				list.add(oc);
			}
			if (o instanceof Point3D) 
			{
				oc=new Point3DColor((Point3D)o,c);
				list.add(oc);
			}
			if (o instanceof PolyLine3D) 
			{
				oc=new PolyLine3DColor((PolyLine3D)o,c);
				list.add(oc);
			}
			if (o instanceof Quadrilatere3D) 
			{
				oc=new Quadrilatere3DColor((Quadrilatere3D)o,c);
				list.add(oc);
			}
			if (o instanceof Triangle3D) 
			{
				oc=new Triangle3DColor((Triangle3D)o,c,true);
				list.add(oc);
			}
	}
	}

	public Iterator<Object3DColor> iterator() 
	{
		return getObject3DList().iterator();
	}


	public void setColor(Color c)
	{
		for (Object3DColor o:this) o.setColor(c); 	
	}

	/**
	 * get the min cube containing the objects
	 * @return array of 2 Vecteur (xmin,ymin,zmin) and (xmax,ymax,zmax)
	 */
	public Vecteur[] getOccupyingCube()
	{
		cornermin=new Vecteur(1000000000,1000000000,1000000000);
		cornermax=new Vecteur(-1000000000,-1000000000,-1000000000);
		for (Object3D o:this) o.checkOccupyingCube(cornermin,cornermax);
		Vecteur[] cube=new Vecteur[2];
		cube[0]=cornermin;
		cube[1]=cornermax;
		return cube;
	}

	public void clean()
	{
		list.removeAllElements();
	}	

	public TriangleMesh transformToTriangles()
	{
		//transform quads in triangle for STL
		TriangleMesh set=new TriangleMesh();
		for (Object3D o:this) 
		{
			if (o instanceof Quadrilatere3DColor)
			{
				Quadrilatere3DColor q=(Quadrilatere3DColor)o;
				Vecteur p1=q.p1();
				Vecteur p2=q.p2();
				Vecteur p3=q.p3();
				Vecteur p4=q.p4();
				Triangle3DColor t1=new Triangle3DColor(p1,p2,p4,q.getColor());
				Triangle3DColor t2=new Triangle3DColor(p2,p3,p4,q.getColor());
				set.add(t2);
				set.add(t1);
			}
			if (o instanceof Triangle3DColor)
			{
				set.add((Triangle3DColor)o);
			}
		}
		return set;
	}

	/**
	 * add the image of altitudes (z) as quadrilaters
	 * @param im
	 */
	public void addImageOfAltitudesAsQuadrilaters(Signal2D1D im)
	{
		double sx=((im.xmax()-im.xmin())/im.dimx());
		double sy=((im.ymax()-im.ymin())/im.dimy());
		for (int i=0;i<im.dimx()-1;i++)
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=im.xmin()+i*sx;
				double y=im.ymin()+j*sy;
				double z1=im.z(i, j);
				double z2=im.z(i+1, j);
				double z3=im.z(i+1, j+1);
				double z4=im.z(i, j+1);
				Vecteur p1=new Vecteur(x,y,z1);
				Vecteur p2=new Vecteur(x+sx,y,z2);
				Vecteur p3=new Vecteur(x+sx,y+sy,z3);
				Vecteur p4=new Vecteur(x,y+sy,z4);
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				this.add(q);
			}
	}

	public int getNbObjects(){return list.size();}


	/**
	 * add objects from the OBJ format
	 * https://en.wikipedia.org/wiki/Wavefront_.obj_file
	 * @param s
	 */
	public void addObjObjects(String s)
	{
		this.addObjObjects(s, Color.black);
	}


	/**
	 * add objects from the OBJ format
	 * https://en.wikipedia.org/wiki/Wavefront_.obj_file
	 * @param s
	 */
	public void addObjObjects(String s,Color c)
	{
		// see spec in http://www.martinreddy.net/gfx/3d/OBJ.spec
		String[] li=StringSource.readLines(s,false);
		Vector<Vecteur> vertices=new Vector<Vecteur> ();
		//Vector<Vecteur> points=new Vector<Vecteur> ();
		//Vector<Vecteur> faces=new Vector<Vecteur> ();
		//Vector<Vecteur> lines=new Vector<Vecteur> ();
		//System.out.println(li.length+" lines");
		for (String line:li)
		{
			//System.out.println(l);
			if (line.length()<=1) continue;
			if (line.charAt(0)=='#') continue;
			Definition def=new Definition(line);
			//System.out.println(def);
			if (line.startsWith("vt")) continue;
			if (line.startsWith("vn")) continue;
			if (line.charAt(0)=='v')
			{
				Vecteur v=new Vecteur(def.getValue(1,0),def.getValue(2,0),def.getValue(3,0));
				Point3DColor t=new Point3DColor(v,c,1);
				this.add(t);
				vertices.add(v);
			}
			if (line.charAt(0)=='f')
			{
				if (def.dim()==4)//triangle
				{
					int i1=new Integer(removeNormal(def.word(1)));
					int i2=new Integer(removeNormal(def.word(2)));
					int i3=new Integer(removeNormal(def.word(3)));
					//			int i1=new Integer(def.word(1).replace("//",""));
					//			int i2=new Integer(def.word(2).replace("//",""));
					//			int i3=new Integer(def.word(3).replace("//",""));
					Triangle3DColor t=new Triangle3DColor(vertices.elementAt(i1-1),
							vertices.elementAt(i2-1),vertices.elementAt(i3-1),c);
					this.add(t);
				}
				if (def.dim()==5)
				{
					int i1=new Integer(removeNormal(def.word(1)));
					int i2=new Integer(removeNormal(def.word(2)));
					int i3=new Integer(removeNormal(def.word(3)));
					int i4=new Integer(removeNormal(def.word(4)));
					Quadrilatere3DColor t=new Quadrilatere3DColor(vertices.elementAt(i1-1),
							vertices.elementAt(i2-1),vertices.elementAt(i3-1),
							vertices.elementAt(i4-1),c);
					this.add(t);
				}
			}
			if (line.charAt(0)=='l')//if (def.dim()==3)
			{
				//		int i1=new Integer(def.word(1));
				//		int i2=new Integer(def.word(2));
				//		Segment3DColor t=new Segment3DColor(vertices.elementAt(i1-1),vertices.elementAt(i2-1),c);
				//		this.add(t);
				//System.out.println("Line: "+line);
				PolyLine3D pol=new PolyLine3D();
				for (int i=1;i<def.dim();i++)
				{
					int k=new Integer(def.word(i));
					pol.addPoint(vertices.elementAt(k-1));
					//System.out.println(i+" "+vertices.elementAt(k-1));
				}
				PolyLine3DColor polc=new PolyLine3DColor(pol,c);
				polc.setSeePoints(false);
				this.add(polc);
			}
			if (line.charAt(0)=='p')if (def.dim()==2)
			{
				int i1=new Integer(def.word(1));
				Point3DColor t=new Point3DColor(vertices.elementAt(i1-1),c,1);
				this.add(t);
			}
		}

	}

	private static String removeNormal(String s)
	{
		int i=s.indexOf("/");
		if (i!=-1)s=s.substring(0,i);
		return s;
	}

	public Object3DColor elementAt(int i)
	{
		return list.elementAt(i);
	}


	public void removeAll()
	{
		list.removeAllElements();
	}

	public void removeAllElements()
	{
		list.removeAllElements();
	}

	public void addElement(Object3DColor o)
	{
		list.addElement(o);

	}




	/**
	 * build a 3D objects set with points from a signal 3D -> 1D
	 * @param s3D
	 * @param dotSize
	 * @param lut look up function ( [min,max]-> [0,255] )to convert value in transparency
	 * @param alpha not used
	 * @param rgb color of the dots
	 */
	public static Object3DColorSet build3Dset(Signal3D1D s3D,int dotSize,Function1D1D lut,int alpha,int rgb)
	{
		Object3DColorSet object3DColorSet=new Object3DColorSet();
		double sx=((s3D.xmax()-s3D.xmin())/(s3D.dimx()-1));
		double sy=((s3D.ymax()-s3D.ymin())/(s3D.dimy()-1));
		double sz=((s3D.zmax()-s3D.zmin())/(s3D.dimz()-1));
		double xmin=s3D.xmin();
		double ymin=s3D.ymin();
		double zmin=s3D.zmin();
		double xmax=s3D.xmax();
		double ymax=s3D.ymax();
		double zmax=s3D.zmax();

		object3DColorSet.add(new VoxelColor(null,
				new Vecteur(xmin,ymin,zmin),
				new Vecteur(xmax,ymax,zmax),
				Color.BLACK)	);

		//System.out.println(getClass()+" vmax="+vmax);
		Color c;
		Color c1=new Color(rgb);
		int r1=c1.getRed();
		int g1=c1.getGreen();
		int b1=c1.getBlue();
		int v1;
		for (int i=0;i<s3D.dimx();i++)
		{
			for (int j=0;j<s3D.dimy();j++)
			{
				for (int k=0;k<s3D.dimz();k++)
				{
					Vecteur p=new Vecteur(xmin+sx*i,ymin+sy*j,zmin+sz*k);
					double v=s3D.val(i,j,k);
					if (v==0) continue;
					v1=(int)lut.f(v);
					if (v1<0.00) v1=0;
					if (v1>=255) v1=255;
					//c=new Color(v1,v1,v1,alpha);
					//System.out.println(i+" "+j+" "+k+" v="+v+" v1="+v1);
					c=new Color(r1,g1,b1,v1);
					Point3DColor pc=new Point3DColor(p,c,dotSize);
					object3DColorSet.add(pc);
				}
			}
		}
		return object3DColorSet;
	}


	public  static Object3DColorSet build3Dset(Signal3D1D s3D,int dotSize,Color col)
	{
		return build3Dset(s3D,dotSize,new Line(1,0),0,col.getRGB());
	}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{	
		for (Object3DColor o:this)
		{
			o.checkOccupyingCube(cornerMin, cornerMax);
		}
	}

	@Override
	public void draw(Graphics g, Projector proj) 
	{
		for (Object3DColor o:this)
		{
		//if (o instanceof Triangle3DColor) System.out.println(getClass()+" draw Triangle3DColor ");
			o.draw(g, proj);
		}	
	}
	

	public void draw(Graphics g, Projector proj,Sun sun) 
	{
		for (Object3DColor o:this)
		{
		//if (o instanceof Triangle3DColor) System.out.println(getClass()+" draw Triangle3DColor ");
			o.draw(g, proj,sun);
		}	
	}

	@Override
	public double getDistanceToScreen(Projector proj) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isOn(Point p, Projector proj) {
		// TODO Auto-generated method stub
		return false;
	}

}
