package com.photonlyx.toolbox.threeD.gui;

import java.awt.Dimension;

import javax.swing.JTabbedPane;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.SphericalCap;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class Graph3DWindow  
{
private CJFrame cjframe	=new CJFrame();
private Graph3DPanel graph3DPanel=new Graph3DPanel();

public Graph3DWindow()
{
	//button to see surface 0
	{
		TriggerList tl_button=new TriggerList();
		tl_button.add(new Trigger(this,"clean"));
		//tl_button.add(tl_update);
		CButton cButton=new CButton(tl_button);
		cButton.setPreferredSize(new Dimension(150, 20));
		cButton.setText("Clean");
		graph3DPanel.getPanelSide().add(cButton);
	}
	
	
	
	init();
}

public Graph3DWindow(Signal2D1D altitudes)
{
init();
graph3DPanel.add(altitudes);
graph3DPanel.initialiseProjectorAuto();
graph3DPanel.repaint();
}

public Graph3DWindow(Object3DColorSet ob)
{
init();
graph3DPanel.addColorSet(ob);
graph3DPanel.initialiseProjectorAuto();
graph3DPanel.repaint();
}

public Graph3DWindow(Object3D ob)
{
init();
graph3DPanel.add(ob);
graph3DPanel.initialiseProjectorAuto();
graph3DPanel.repaint();
}

private void init()
{
//create frame and add the chart and and legend in a border layout:
cjframe.setTitle("Graph 3D");
cjframe.setSize(600, 400);
cjframe.setExitAppOnExit(false);
JTabbedPane topTabPane=new JTabbedPane();
cjframe.add(topTabPane);
topTabPane.add(graph3DPanel.getComponent());
}


public void clean()
{
	graph3DPanel.removeAll3DObjects();
	graph3DPanel.update();
}

public Graph3DPanel getGraph3DPanel(){return graph3DPanel;}



public void updateView()
{
graph3DPanel.repaint();
}


public CJFrame getCJFrame(){return cjframe;}


public static void main(String args[])
{
	Graph3DWindow g3dw=new Graph3DWindow();
	Object3DColorSet setc=new Object3DColorSet();
	setc.add(new SphericalCap().getTriangles());		
	g3dw.getGraph3DPanel().addColorSet(setc);

}


}
