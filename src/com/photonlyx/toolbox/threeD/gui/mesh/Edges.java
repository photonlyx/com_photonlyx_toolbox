package com.photonlyx.toolbox.threeD.gui.mesh;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;

public class Edges  extends Vector<Segment3D> implements Object3D
{
	Vector<Segment2D>	screenCoord=new Vector<Segment2D>();

	public void allocateScreenCoords()
	{
		screenCoord.removeAllElements();
		for (Segment3D s:this) screenCoord.add(new Segment2D());
	}

	public boolean isOn(Point p, Projector proj) 
	{		
		for (Segment2D s:screenCoord) if (s.getDistance(p.x,p.y)<3) return true;
		return false;
	}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (Segment3D s:this) 
			for (int j=0;j<2;j++)
				for (int k=0;k<3;k++)
				{
					double r=s.point(j).coord(k);
					if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
					if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
				}
	}

	private void calcScreenCoord(Projector proj)
	{
		int c=0;
		for (Segment3D s:this) 
			{
			Segment2D segScreen=screenCoord.get(c++);
			proj.calcCoorScreen(segScreen.p1(), s.p1());
			proj.calcCoorScreen(segScreen.p2(), s.p2());
			}
		
	}


	@Override
	public void draw(Graphics g, Projector proj) 
	{
		if (screenCoord.size()==0) this.allocateScreenCoords();
		calcScreenCoord(proj);
		for (Segment2D s:screenCoord) 
			g.drawLine((int)s.p1().x(), (int)s.p1().y(), (int)s.p2().x(), (int)s.p2().y());
	}


	@Override
	public double getDistanceToScreen(Projector proj) {
		// TODO Auto-generated method stub
		return 0;
	}
}
