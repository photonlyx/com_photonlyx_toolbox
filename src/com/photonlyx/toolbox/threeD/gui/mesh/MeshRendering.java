package com.photonlyx.toolbox.threeD.gui.mesh;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Object3DFrame;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.rendering.Sun;

public class MeshRendering implements Object3DColor
{
	private Color c;
	private Mesh mesh;
	//private Vector<double[]> screenCoord=new Vector<double[]> ();
	private int verticesSize=3;

	public MeshRendering(Mesh mesh,Color c)
	{
		this.mesh=mesh;
		this.c=c;
		//allocateScreenCoord();
	}
	
	public MeshSource getMeshSource()
	{
	return mesh;	
	}
	
//	private void allocateScreenCoord()
//	{
//		screenCoord.removeAllElements();
//		for (Vecteur v:mesh.getVerticesGlobal()) 
//		{
//			double[] sc=new double[2];
//			screenCoord.add(sc);
//		}
//		for (Segment3D v:mesh.getEdgesGlobal()) 
//		{
//			screenCoord.add(new double[2]);
//			screenCoord.add(new double[2]);
//		}
//		for (PolyLine3D pol:mesh.getFacesGlobal()) 
//			for (Vecteur v:pol)
//				screenCoord.add(new double[2]);
//	}

//	private void calcScreenCoord(Projector proj)
//	{
//		int c=0;
//		for (Vecteur v:mesh.getVerticesGlobal()) 
//		{
//			proj.calcCoorScreen(screenCoord.get(c++),v);
//		}
//		for (Segment3D seg:mesh.getEdgesGlobal()) 
//		{			
//			proj.calcCoorScreen(screenCoord.get(c++),seg.p1());
//			proj.calcCoorScreen(screenCoord.get(c++),seg.p2());
//		}
//		for (PolyLine3D pol:mesh.getFacesGlobal()) 
//			for (Vecteur v:pol)
//				proj.calcCoorScreen(screenCoord.get(c++),v);
//	}

	
	public void setColor(Color c1) 
	{
		c=c1;
	}

	public Color getColor() {
		return c;
	}

	

	public int getVerticesSize() {
		return verticesSize;
	}

	public void setVerticesSize(int verticesSize) {
		this.verticesSize = verticesSize;
	}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		if (mesh instanceof Object3D) ((Object3D)mesh).checkOccupyingCube(cornerMin, cornerMax);
	}

	@Override
	public void draw(Graphics g, Projector proj) 
	{
		g.setColor(c);
		mesh.draw(g, proj);
		
		
		
//		this.calcScreenCoord(proj);
//		
//		int c=0;
//		for (Vecteur v:mesh.getVerticesGlobal()) 
//		{
//			double[] sc=screenCoord.get(c++);
//			g.fillRect((int)sc[0]-verticesSize/2, (int)sc[1]-verticesSize/2, verticesSize, verticesSize);
//		}
//		for (Segment3D seg:mesh.getEdgesGlobal()) 
//		{
//			double[] sc1=screenCoord.get(c++);
//			double[] sc2=screenCoord.get(c++);
//			g.drawLine((int)sc1[0], (int)sc1[1], (int)sc2[0], (int)sc2[1]);
//		}
//		for (PolyLine3D pol:mesh.getFacesGlobal()) 
//		{
//			int n=pol.nbPoints();
//			//store the first point:
//			double[] sc01,sc1,sc2;
//			sc1=screenCoord.get(c++);
//			sc01=sc1;
//			//draw the segments
//			for (int i=0;i<n-1;i++)
//			{
//				sc2=screenCoord.get(c++);
//				g.drawLine((int)sc1[0], (int)sc1[1], (int)sc2[0], (int)sc2[1]);
//				sc1=sc2;
//			}
//			//draw last segment:
//			sc1=screenCoord.get(c);
//			g.drawLine((int)sc1[0], (int)sc1[1], (int)sc01[0], (int)sc01[1]);
//		}
		
		
	}
	public void draw(Graphics g, Projector proj, Sun sun)
	{
		this.draw(g,proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		if (mesh instanceof Object3D)
			{
			Object3D oo=(Object3D)mesh;
			return oo.getDistanceToScreen(proj);
			}
		else return 0;
	}

	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		//return o.getEdges()
		if (mesh instanceof Object3DFrame) 
			{
			return  ((Object3DFrame)mesh).isOn(p, proj);
			}
		else return false;
	}

	

}
