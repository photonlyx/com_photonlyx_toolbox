package com.photonlyx.toolbox.threeD.gui.mesh;

import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.Frame;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Object3DFrame;
import com.photonlyx.toolbox.math.geometry.PolyLine3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMeshSource;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.cloud.Cloud;
import com.photonlyx.toolbox.math.geometry.cloud.CloudPoint;

public class Mesh   implements Object3DFrame,MeshSource,Object3D
{
	private Frame frame=new Frame();//local frame of the cylinder

	private Vertices verticesLocal=new  Vertices ();
	private Edges edgesLocal=new  Edges();
	private Faces facesLocal=new  Faces ();
	
	private Vertices verticesGlobal=new  Vertices ();
	private Edges edgesGlobal=new  Edges();
	private Faces facesGlobal=new  Faces ();

	/**
	 * create using a box
	 * @param b
	 */
	public Mesh (Box b)
	{
//		for (Vecteur p:b.getVertices()) verticesLocal.add(p.copy());
		for (Segment3D s:b.getEdges()) edgesLocal.add(s.copy());
//		for (PolyLine3D  f:b.getFaces()) facesLocal.add(f.copy());
		
		allocateGlobal();
		updateGlobal();	
	}
	
	/**
	 * create using a source of triangles
	 * @param b
	 */
	public Mesh (TriangleMeshSource b)
	{
		for (Triangle3D t:b.getTriangles())
		{
			edgesLocal.add(new Segment3D(t.p1(),t.p2()));
			edgesLocal.add(new Segment3D(t.p2(),t.p3()));
			edgesLocal.add(new Segment3D(t.p3(),t.p1()));
		}
		allocateGlobal();
		updateGlobal();	
	}
	
	
	
	public Mesh (Cloud c)
	{
		for (CloudPoint cp:c) verticesLocal.add(cp.getP());
		allocateGlobal();
		updateGlobal();	
	}

	
	
	private void allocateGlobal()
	{
		for (Vecteur v:verticesLocal) 	verticesGlobal.add(new Vecteur());	
		for (Segment3D s:edgesLocal) 	edgesGlobal.add(new Segment3D());	
		for (PolyLine3D p:facesLocal) 	facesGlobal.add(new PolyLine3D());	
	}
	
	
	
	@Override
	public Mesh getMesh() {
		return this;
	}

	
	
	public boolean isOn(Point p, Projector proj) 
	{		
		if (verticesGlobal.isOn(p, proj))  return true;
		if (edgesGlobal.isOn(p, proj))  return true;
		if (facesGlobal.isOn(p, proj))  return true;
		return false;
	}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		verticesGlobal.checkOccupyingCube(cornerMin, cornerMax);
		edgesGlobal.checkOccupyingCube(cornerMin, cornerMax);
		facesGlobal.checkOccupyingCube(cornerMin, cornerMax);
	}



	@Override
	public void draw(Graphics g, Projector proj) 
	{
		verticesGlobal.draw(g, proj);
		edgesGlobal.draw(g, proj);
		facesGlobal.draw(g, proj);
	}


	@Override
	public double getDistanceToScreen(Projector proj) {
		// TODO Auto-generated method stub
		return 0;
	}




	@Override
	public Frame getFrame() 
	{
		return frame;
	}




	@Override
	public void updateGlobal() 
	{
		int c=0;
		for (Vecteur p:verticesLocal) frame.global(verticesGlobal.get(c++), p);
		c=0;
		for (Segment3D s:edgesLocal) 
			{
			Segment3D seg=edgesGlobal.get(c++);
			frame.global(seg.p1(),s.p1() );
			frame.global(seg.p2(),s.p2() );
			}
		c=0;
		for (PolyLine3D  f:facesLocal) 
			{
			for (int k=0;k<f.nbPoints();k++) frame.global(facesGlobal.get(c++).getPoint(k),f.getPoint(k));
			}
	}




	public Vertices getVerticesGlobal() {
		return verticesGlobal;
	}




	public Edges getEdgesGlobal() {
		return edgesGlobal;
	}




	public Faces getFacesGlobal() {
		return facesGlobal;
	}
	
}
