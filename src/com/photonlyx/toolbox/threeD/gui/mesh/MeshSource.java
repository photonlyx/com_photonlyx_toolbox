package com.photonlyx.toolbox.threeD.gui.mesh;

public interface MeshSource 
{
	
public Mesh getMesh();
	
}
