package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3D;
import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.rendering.Sun;



public class Quadrilatere3DColor extends Quadrilatere3D implements Object3DColor
{
	private Color color;
	private boolean filled=false; // if false paint only the frame

	private double[][] cooScreen=new double[4][2];

	public Quadrilatere3DColor()
	{
		super();
	}

	public Quadrilatere3DColor(Quadrilatere3D q,Color c)
	{
		super();
		this.color=c;
		update();
		calcCentre();
	}

	public Quadrilatere3DColor(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4)
	{
		super(_p1,_p2,_p3,_p4);
		color=Color.black;
		update();
		calcCentre();
	}

	public Quadrilatere3DColor(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4,int colorRGB)
	{
		super(_p1,_p2,_p3,_p4);
		update();
		calcCentre();
		color=new Color(colorRGB);
	}

	public Quadrilatere3DColor(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4,Color _color)
	{
		super(_p1,_p2,_p3,_p4);
		update();
		calcCentre();
		color=_color;
	}
	public Quadrilatere3DColor(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4,Color _color,boolean _filled)
	{
		super(_p1,_p2,_p3,_p4);
		update();
		calcCentre();
		color=_color;
		filled=_filled;
	}

	public void update()
	{
		calcGlobal();
	}



	public void draw(Graphics g, Projector proj)
	{
		draw( g, proj, null);
	}

	/**
	 * draw the quad 
	 * @param g
	 * @param proj
	 * @param sun light source for rendering
	 */
	public void draw(Graphics g,Projector proj,Sun sun)
	{
		Polygon pol=new Polygon();
		//System.out.println("quad filled="+filled+" sun="+sun);
		for (int j=0;j<4;j++) 
		{
			cooScreen[j]=proj.calcCoorEcranDouble(p[j]);
			pol.addPoint((int)cooScreen[j][0],(int)cooScreen[j][1]);
		}

		if (sun!=null)
		{
			Triangle3DColor t=new Triangle3DColor(p[0],p[1],p[2]);
			//	Vecteur v=sun.getIl();
			//	Vecteur n=t.getNormal();
			//cos of illumination vector:
			float cos_n_il=(float) t.getNormal().mul(sun.getIl().scmul(-1));
			if (cos_n_il<0) cos_n_il=0;
			//System.out.println(p[0]+"   "+n+" cos:"+cos_n_il);
			Color c=new Color(cos_n_il,cos_n_il,cos_n_il);
			g.setColor(c);
			g.fillPolygon(pol);
		}
		else
		{
			if (filled) 
			{
				g.setColor(color);
				g.fillPolygon(pol);
			}
			else
				for (int j=0;j<4;j++) 
				{
					int k=j+1;
					if (k==4) k=0;
					{
						g.setColor(color);
						//g.setColor(Color.black);
						g.drawLine((int)cooScreen[j][0], (int)cooScreen[j][1], (int)cooScreen[k][0], (int)cooScreen[k][1]);
					}
					//g.drawPolygon(pol);
				}
		}
	}



	public Color getColor()
	{
		return color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	

	public boolean isFilled()
	{
		return filled;
	}

	public void setFilled(boolean filled)
	{
		this.filled = filled;
	}

	public void translate(Vecteur v)
	{
		x+=v.x();
		y+=v.y();
		z+=v.z();
		this.calcGlobal();
	}

	@Override
	public boolean isOn(Point p, Projector proj)
	{
		double[] point=new double[2];
		for (int j=0;j<3;j++) 
		{
			point[0]=p.x;
			point[1]=p.y;
			//distance from point to segment:
			double dist=Segment2D.segdist2D(cooScreen[j], cooScreen[j+1], point);
			if (dist<5) return true;
		}
		return false;
	}




}
