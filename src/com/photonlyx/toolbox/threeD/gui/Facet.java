package com.photonlyx.toolbox.threeD.gui;

import java.awt.Graphics;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.threeD.rendering.Sun;

public interface Facet extends Object3DColor
{
public void draw(Graphics g,Projector proj,Sun sun);
public void setFilled(boolean b);

}
