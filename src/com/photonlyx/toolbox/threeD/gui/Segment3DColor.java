package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.rendering.Sun;

public class Segment3DColor  extends Segment3D implements Object3DColor
{
private Color color;


public Segment3DColor()
{
super();
}

public Segment3DColor(Segment3D seg,Color color)
{
super(seg.p1(),seg.p2());
this.color=color;
}

public Segment3DColor(Vecteur _p1,Vecteur _p2)
{
super(_p1,_p2);
color=Color.black;
}
public Segment3DColor(Vecteur _p1,Vecteur _p2,int colorRGB)
{
super(_p1,_p2);
color=new Color(colorRGB);
}
public Segment3DColor(Vecteur _p1,Vecteur _p2,Color color)
{
super(_p1,_p2);
this.color=color;
}



public void setColor(Color c)
{
color=c;

}

@Override
public void draw(Graphics g,Projector proj)
{
double[][] cooScreen=new double[2][];
cooScreen[0]=proj.calcCoorEcran(p1());
cooScreen[1]=proj.calcCoorEcran(p2());
g.setColor(color);
g.drawLine((int)cooScreen[0][0], (int)cooScreen[0][1], (int)cooScreen[1][0], (int)cooScreen[1][1]);
}
public void draw(Graphics g, Projector proj, Sun sun)
{
	this.draw(g,proj);
}

/**
 * get a copy
 * @return
 */
public Segment3DColor copy()
{
return new Segment3DColor(p1().copy(),p2().copy(),color);
}



}
