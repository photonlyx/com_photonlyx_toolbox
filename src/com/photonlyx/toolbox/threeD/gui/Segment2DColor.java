package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;

import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;

public class Segment2DColor  extends Segment2D
{
private Color color;


public Segment2DColor()
{
super();	
}

public Segment2DColor(Vecteur2D _p1,Vecteur2D _p2)
{
super(_p1,_p2);
color=Color.black;
}
public Segment2DColor(Vecteur2D _p1,Vecteur2D _p2,int colorRGB)
{
super(_p1,_p2);
color=new Color(colorRGB);
}
public Segment2DColor(Vecteur2D _p1,Vecteur2D _p2,Color color)
{
super(_p1,_p2);
this.color=color;
}



public void setColor(Color c)
{
color=c;

}


}
