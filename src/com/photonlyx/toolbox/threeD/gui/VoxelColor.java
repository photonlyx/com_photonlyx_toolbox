package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.Voxel;
import com.photonlyx.toolbox.threeD.rendering.Sun;


/**
 * tree of rectangular bricks
 * @author laurent
 */
public class VoxelColor extends Voxel implements Object3DColor
{
private Color c;
private boolean filled=false;
	
public VoxelColor(Voxel father, Vecteur pmin, Vecteur pmax,Color c)
{
super(father,pmin,pmax);
this.c=c;
}


/**
 * draw the voxels sons if any
 */
public void draw(Graphics g, Projector proj)
{
//draw the vertices:
g.setColor(c);
double[] cooScreen1,cooScreen2;
for (Segment3D seg:getVertices()) 
	{
	cooScreen1=proj.calcCoorEcran(seg.p1());
	cooScreen2=proj.calcCoorEcran(seg.p2());
	g.drawLine((int)cooScreen1[0],(int)cooScreen1[1],(int)cooScreen2[0],(int)cooScreen2[1]);
	}
}

public void draw(Graphics g, Projector proj, Sun sun)
{
	this.draw(g,proj);
}

@Override
public void setColor(Color c) {
	// TODO Auto-generated method stub
	
}




}
