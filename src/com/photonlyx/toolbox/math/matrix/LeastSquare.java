package com.photonlyx.toolbox.math.matrix;

public class LeastSquare 
{

	/**
	 * solve a least square optimisation
	 * https://en.m.wikipedia.org/wiki/Ordinary_least_squares
	 * n observations
	 * p parameters
	 * @param X  design matrix, dim: n*p
	 * @param y  response variables, dim: n*1
	 * @return optimal parameters beta, dim : p*1
	 */
	public static FullMatrix solve(FullMatrix X,FullMatrix y,boolean verbose)	
	{
		if (verbose)
		{
			System.out.println("A");
			System.out.println(X);

			System.out.println("y");
			System.out.println(y);
		}
		//calculate the normal matrix
		FullMatrix At=X.transpose();
		FullMatrix N=At.multiply(X);

		if (verbose)
		{
			System.out.println("N");
			System.out.println(N);
		}

		FullMatrix AtF=At.multiply(y);

		if (verbose)
		{
			System.out.println("AtF");
			System.out.println(AtF);
		}

		//now solve the equation:  AtA dX = AtF
		int[] indx=new int[X.nbl()];
		FullMatrix.decompose_lu(N,indx);//N is destroyed!
		FullMatrix.systeme_lu(N,indx,AtF);//now the solution is in AtF!
		return AtF;
	}



}
