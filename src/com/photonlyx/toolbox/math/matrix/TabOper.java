package com.photonlyx.toolbox.math.matrix;

/**
//*******************************************************************************************
//*************************** CLASSE TABLEAU D'OPERATIONS ***********************************
//*******************************************************************************************
*/
import java.awt.Frame;

import com.photonlyx.toolbox.util.Messager;

public class TabOper
{
static Frame PARENT;
short dim;
Oper op[];
short compteur;

public TabOper(short i)
{
dim=i;
op =new Oper[dim];
compteur=0;
}

public int memoryUsed()
{
int mem=0;
for (short k=0;k<dim;k++) mem+=op[k].nbBytes();
return mem;
}



public void reset()
{
compteur=0;
}

public boolean met(Oper o)
{
if (compteur==dim) 
	{
	Messager.messErr("Tableau d'operations plein");
	return false;
	}
op[compteur++]=o;
return true;
}

public String print()
{
String s=new String();
for (short k=1;k<=dim;k++) 
	{
	s=s+"pivot"+k+"\n";
	if (serie(k)!=null) s=s+serie(k).print();
	}
return s;
}

public Oper serie(short i)
{
return op[i-1]; 
}


/**elimination by just reapply the operations stored in the TabOper*/
public void eliminationAvecTaboper(FullMatrix b)
{
//elimination d'un vecteur plein b a partir des operations stockees dans tabOp
short indice;
for (short k=0;k<dim;k++)
	{
//	if (op[k]!=null)
		for (short i=1;i<=op[k].nEt();i++)
			{
			indice=op[k].ind(i);
			b.affc1(indice-1,b.elc1(indice-1)-op[k].coef(i)*b.elc1(k) );
			}
	}
}

public void clean() 
{
op =new Oper[dim];
compteur=0;
}






} // fin classe TabOper


