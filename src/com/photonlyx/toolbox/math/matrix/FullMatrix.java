package com.photonlyx.toolbox.math.matrix;

import java.awt.*;
import java.io.*;
import java.text.*;
import java.util.*;

import com.photonlyx.toolbox.util.Messager;

/*************************************************************************
*                                                                        *
*                           Class MatricePleine                          *
*                                                                        *
*************************************************************************/

/**
full matrix
the indices are 0 based
*/
public class FullMatrix implements Matrix
{
static Frame PARENT;
int lignes;
int colonnes;
double[][] tab;
/** for the LU decomposition*/
private static double TINY=1.0e-20;

public FullMatrix(int l,int c)
{
lignes=l;
colonnes=c;
tab=new double[lignes][colonnes];
}

public FullMatrix(double[][] array)
{
lignes=array.length;
colonnes=array[0].length;
tab=array;
}

/**
create a new MatricePleine from a file. The file file must start
with 2 integers: the number of  lines and the number of rows
  */
public FullMatrix(String nomFichier)
{
FileReader r;
StreamTokenizer st;
int N=0,M=0;
try 
	{
	r = new FileReader(nomFichier);
	st=new StreamTokenizer(r);
	st.nextToken();		
	N=(int)st.nval;
	st.nextToken();		
	M=(int)st.nval;
	System.out.println();
	lignes=N;
	colonnes=M;

tab=new double[lignes][colonnes];

	for (short i=0;i<N;i++)
		{ 
		for (short j=0;j<M;j++) 
			{
			st.nextToken();
			AFFECT(this,i,j,st.nval);	
			}
		}
	r.close();
	}
catch (Exception e) 
	{
	System.out.println("Creation_from_file_MatricePleine"
	+":"
	+N+"x"+M
	+"Erreur"
	+" : "
	+e);
	}
	
}



/** return the value at the row l and column c, the matrix starts at (0,0) */
static double ELEMENT(FullMatrix m,int l,int c)
{
return m.tab[l][c];
}

/** return the value at the row l and column c, the matrix starts at (0,0)*/
public double el(int l,int c)
{
return ELEMENT(this,l,c);
}


/** return the value at the row l and column c, the matrix starts at (0,0)*/
public double get(int row,int column)
{
return tab[row][column];
}

/** return the value at the row l and column c, BEWARE!!!, here it is assumed that the matrix starts at (1,1) */
public double val(int l,int c)
{
return ELEMENT(this,l-1,c-1);
}


/**affect a value r at the place (l,c), the matrix starts at (0,0) */
static void AFFECT(FullMatrix m,int l,int c,double r)
{
m.tab[l][c]=r;
}

static double ELEMENTSERIE(FullMatrix m,int i)
{
return ELEMENT(m,(i)/m.lignes,(i)-(i)/m.lignes);
}

static void AFFECTSERIE(FullMatrix m,int i,double r)
{
m.tab[i/m.lignes][i-i/m.lignes]=r;
}

public boolean affc1(int l,double v)
{
AFFECT(this,l,0,v);
return true;
}

public double elc1(int l)
{
return ELEMENT(this,l,0);
}


/**affect a value r at the place (l,c), the matrix starts at (0,0) */
public void affect(int l,int c,double v)
{
AFFECT(this,l,c,v);
}


/** attention : suppose HERE that the matrice indexing is ONE based !!!!*/
public void metEl(int l,int c,double v)
{
AFFECT(this,l-1,c-1,v);
}

/** row and column starts at 0*/
public void put(int row,int column,double value)
{
tab[row][column]=value;
}


/**return the number of columns*/
public int nbc()
{
return colonnes;
}

public int getNbColumns()
{
return nbc();
}


/**return the number of rows*/

public int nbl()
{
return lignes;
}

public int getNbRows()
{
return nbl();
}

/** return the memory used by the matrix in Bytes*/
public int memoryUsed()
{
return nbl()*nbc()*8;//a double uses 8 Bytes
}



public void anulate()
{
for (int i=0;i<this.lignes;i++)
	for (int j=0;j<this.colonnes;j++)  AFFECT(this,i,j,0.);

}



public static FullMatrix copie_matrice(FullMatrix a_copier)
{
int i,j;
FullMatrix m_tempo=new FullMatrix(a_copier.lignes,a_copier.colonnes);

for (i=0;i<a_copier.lignes;i++)
	for(j=0;j<a_copier.colonnes;j++)
		AFFECT(m_tempo,i,j,ELEMENT(a_copier,i,j));
return m_tempo;
}

public static void _copie_matrice(FullMatrix a_copier,FullMatrix dest)
{
        int i,j;
        
        for (i=0;i<a_copier.lignes;i++)
                for(j=0;j<a_copier.colonnes;j++)
                        AFFECT(dest,i,j,ELEMENT(a_copier,i,j));
}



public static FullMatrix addition( FullMatrix add_gauche, FullMatrix add_droite)
{
        FullMatrix m_tempo=null;

        if ((add_gauche.lignes != add_droite.lignes)
                || (add_droite.colonnes != add_gauche.colonnes))
        {
                return(null);
        }
        else
        {
        m_tempo=new FullMatrix(add_gauche.lignes,add_gauche.colonnes);
                if (m_tempo!=null)
                        _addition(add_gauche,add_droite,m_tempo);
                return(m_tempo);
        }
}

public static void _addition(FullMatrix gauche, FullMatrix droite, FullMatrix dest)
{
        int i,j;

        if ((dest!=null) && (gauche!=null) && (droite!=null))
                for (i=0;i<dest.lignes;i++)
                        for (j=0;j<dest.colonnes;j++)
                                AFFECT(dest,i,j,
                                            ELEMENT(gauche,i,j) +
                                            ELEMENT(droite,i,j));
}


public static FullMatrix soustraction(FullMatrix gauche,FullMatrix droite)
{
FullMatrix m_tempo=null;
if ((gauche.lignes != droite.lignes)|| (droite.colonnes != gauche.colonnes))
	{
	return(null);
	}
else
	{
	m_tempo=new FullMatrix(gauche.lignes,gauche.colonnes);
	if (m_tempo!=null) _soustraction(gauche,droite,m_tempo);
	return(m_tempo);
	}
}


public static void _soustraction(FullMatrix gauche, FullMatrix droite, FullMatrix dest)
{
	int i,j;

	if ((dest!=null) && (gauche!=null) && (droite!=null))
		for (i=0;i<dest.lignes;i++)
			for (j=0;j<dest.colonnes;j++)
				AFFECT(dest,i,j,ELEMENT(gauche,i,j) - ELEMENT(droite,i,j));
}



/**
return a new matrix, compute this-mat 
  */
public FullMatrix soustraction(FullMatrix mat)
{
return soustraction(this,mat);

}

public FullMatrix multiplication(FullMatrix mul_gauche,FullMatrix mul_droite)
{
FullMatrix m_tempo=null;
if ((mul_gauche == null)||(mul_droite == null)) 
	{
	Messager.messErr(Messager.getString("null_matrix"));
	return(null);
	}
if ((mul_gauche.colonnes != mul_droite.lignes))
	{
	Messager.messErr(Messager.getString("incompatible_matrices_multiplication"));
	return(null);
	}
else
	{
	m_tempo=new FullMatrix(mul_gauche.lignes,mul_droite.colonnes);
	if (m_tempo!=null) _multiplication(mul_gauche,mul_droite,m_tempo);
	return(m_tempo);
	}
}

/**
 * multiply this matrix by mat
 * @param mat
 * @return
 */
public FullMatrix multiply(FullMatrix mat)
{
return multiplication(this,mat);
}


public static void _multiplication(FullMatrix gauche, FullMatrix droite, FullMatrix dest)
{
int i,j,k;

for (i=0;i<dest.lignes;i++)
	for (j=0;j<dest.colonnes;j++)
		{
		AFFECT(dest,i,j,0.);
		for(k=0;k<gauche.colonnes;k++)
			{
			AFFECT(dest,i,j, ELEMENT(dest,i,j)+ ELEMENT(gauche,i,k) * ELEMENT(droite,k,j) );
			}
		}
}


/**
the right matrix is only a column and represents a diagonal matrix
  */
public FullMatrix multiplication_par_diag(FullMatrix mul_droite)
{
return multiplication_par_diag(this,mul_droite);
}

/**
the right matrix is only a column and represents a diagonal matrix
  */
public FullMatrix multiplication_par_diag(FullMatrix mul_gauche,FullMatrix mul_droite)
{
FullMatrix m_tempo=null;
if ((mul_gauche.colonnes != mul_droite.lignes)
                || (mul_gauche == null)
                || (mul_droite == null))
        {
                return(null);
        }
        else
        {
        m_tempo=new FullMatrix(mul_gauche.lignes,mul_gauche.colonnes);
                if (m_tempo!=null)
                        _multiplication_par_diag(mul_gauche,mul_droite,m_tempo);
                return(m_tempo);
        }
}

/**
the right matrix is only a column and represents a diagonal matrix
  */
public void _multiplication_par_diag(FullMatrix gauche, FullMatrix droite, FullMatrix dest)
{
        int i,j;

                for (i=0;i<dest.lignes;i++)
                        for (j=0;j<dest.colonnes;j++)
                              AFFECT(dest,i,j, ELEMENT(gauche,i,j) * ELEMENT(droite,j,0));
}



public static void _multiplie_scalaire(FullMatrix a,double scalaire, FullMatrix dest)
{
        int i,j;

        for (i=0;i< a.lignes;i++)
                for(j=0;j<a.colonnes;j++)
                        AFFECT(dest,i,j,
                                         ELEMENT(a,i,j) * scalaire);
}



public  void _multiplie_scalaire(double scalar)
{
        int i,j;

        for (i=0;i< this.lignes;i++)
                for(j=0;j<this.colonnes;j++) tab[i][j]=tab[i][j]*scalar;
 }
/**
 * *return a new matrix multiplied by a scalar
 * @param entree
 * @param scalaire
 * @return
 */
public static FullMatrix multiplie_scalaire( FullMatrix entree,double scalaire)
{
        FullMatrix m_tempo=null;
        if (entree==null)
        {
                return(null);
        }
        else
        {
        m_tempo=new FullMatrix(entree.lignes,entree.colonnes);
                if (m_tempo!=null)
                        _multiplie_scalaire(entree,scalaire,m_tempo);
                return(m_tempo);
        }
}

/**
return a new transposed matrix
  */
public FullMatrix transpose( FullMatrix a_transposer)
{
int i,j;
FullMatrix m_tempo=null;
if (a_transposer==null)
	{
	return(null);
	}
else
	{
	m_tempo=new FullMatrix(a_transposer.colonnes,a_transposer.lignes);
	if (m_tempo != null)
		{
		for (i=0;i< m_tempo.lignes;i++)
			{
			for (j=0;j < m_tempo.colonnes;j++)
				{
				AFFECT(m_tempo,i,j,ELEMENT(a_transposer,j,i));
				}
			}
		}
	return(m_tempo);
	}
}

/**return a new transposed matrix*/
public FullMatrix transpose()
{
return transpose(this);
}


/**
return the norm of the matrix
  */
public double norme( )
{ 
int i,j;
double r=0;
	for (i=0;i< lignes;i++)      
		for (j=0;j < colonnes;j++)   r+=(ELEMENT(this,i,j))*(ELEMENT(this,i,j));
	r=Math.sqrt(r);   
	return(r);
}

/**
return the maximum of the absolute values of the matrix
  */
public double maxAbs()
{
double r=0;
for (int l=0;l<lignes;l++)
	for (int c=0;c<colonnes;c++)
		{
		double abs=Math.abs(ELEMENT(this,l,c));
		if (abs>r) r=abs;
		}
return r;
}



/**
return a new (full) matrix with this matrix (full) multipied by the sparse matrix B
  */
FullMatrix multiplie(Creuse B)
{
El elB;
FullMatrix A=this;
FullMatrix AB=new FullMatrix(A.lignes,B.nbc);
double p;

if (A.colonnes!=B.nbl) 
	{
	System.out.println("Pas_possible_de_multipier_ces_matrices");
	return null;
	}

for (int l=0;l<AB.lignes;l++) //balaye les lignes de AB
	for (int c=0;c<AB.colonnes;c++) //balaye les colonnes de AB
		{
		elB=B.chElColonne((short)1,(short)c); //se place en debut de colonne de B
		p=0.0;
		for(;;)
			{
			p=p+elB.v*ELEMENT(A,l,(int)elB.l);
			elB=elB.sc;
			if (elB==null) break;
			}
		AFFECT(AB,l,c,p);
		}
return AB;
}








/**

calculate the  determinant
attention : not tested yet

  */
double determinant( FullMatrix matrice)
{
        if ((matrice==null) || (matrice.lignes!=matrice.colonnes))
        {
                return(0.);
        }
        else
        {
                int ordre=matrice.lignes;

                if (ordre==2)
                {
                return(ELEMENT(matrice,0,0) * ELEMENT(matrice,1,1) - ELEMENT(matrice,0,1) * ELEMENT(matrice,1,0));
                }
                else
                {
                         FullMatrix mineure=null;
                        double somme=0.0;
                        int i;


                        for(i=1;i<ordre+1;i++)
                        {
                                mineure=minore(matrice,i);
                                somme+=Math.pow((-1),i+1)* ELEMENTSERIE(matrice,(i-1)) * determinant(mineure);
                              //  somme+=pow((-1),i+1)* *(ptr+(i-1)) * determinant(mineure);
                        }
                        return(somme);

                        
                }
        }
}



/**

not tested !!!!!
  */
 FullMatrix minore( FullMatrix mat,int place)
{
FullMatrix m_tempo=new FullMatrix((mat.lignes)-1,(mat.lignes)-1);
        if (m_tempo != null)
        {
 //               TYPELEM *ptr1=mat.ptr_debut+mat.lignes;
   //             TYPELEM *ptr2=m_tempo.ptr_debut;
                int ptr1=mat.lignes;
                int ptr2=0;
                int i;

                for (i=0;i<(mat.lignes-1)*mat.lignes;i++,ptr1++)
                {
                        if ((i+1)%(mat.lignes)!=(place%(mat.lignes)))
                        {
                           //     *ptr2++=*ptr1;
                                ptr2++;
                                AFFECTSERIE(m_tempo,ptr2,ELEMENTSERIE(mat,ptr1));
                        }
                }
        }
        return(m_tempo);

}                                  

/**
give a string containing the text view of the matrixs
  */
 public String toString()
{
NumberFormat nf3 = NumberFormat.getInstance(Locale.ENGLISH);
nf3.setMaximumFractionDigits(3);
nf3.setMinimumFractionDigits(3);
nf3.setGroupingUsed(false);

StringBuffer s=new StringBuffer();
s.append(nbl()+"rows "+nbc()+"columns\n");
for (int i=0;i<this.lignes;i++)
	{
	for (int j=0;j<this.colonnes;j++)
		{
		s.append(nf3.format(ELEMENT(this,i,j))+" ");
		}
	s.append("\n");
	}
return s.toString();
}


/**
save the matrix in a text file
  */
public boolean sauve(String fileName)
{
System.out.println("Saving full matrix in "+fileName);
try
	{
	PrintWriter st=new PrintWriter(new FileOutputStream(fileName));
	st.print(toString());
	st.close();
	return true;
	}
catch (Exception e)
	{
	System.out.println("Error_writing_full_matrix_in"+" "+fileName+"\n"+e);
	return false;
	}
}



/********************************************************************
 *    Fonctions utilisant la decomposition LU
 ********************************************************************/

/**
To be done before solving a linear system of equation. 
@param mat the left part of the system
@param indx array of size the matrix mat nb of lines: will be used by systeme_lu
*/
public static void decompose_lu(FullMatrix mat, int[] indx)
{
        int i, imax=0, j, k;
        double big, dum, sum, temp;
        FullMatrix vv;

        vv=new FullMatrix(mat.lignes, 1);
        double d=1.0;
        for(i=0;i<mat.lignes;i++)
        {
                big=0.0;
                for(j=0;j<mat.lignes;j++)
                        if((temp=Math.abs(ELEMENT(mat,i,j)))>big)
                                big=temp;
                if (big==0.0)
                        return;
                AFFECT(vv,i,0,1.0/big);
        }

        for (j=0;j<mat.lignes;j++)
        {
                for(i=0;i<j;i++)
                {
                        sum=ELEMENT(mat,i,j);
                        for(k=0;k<i;k++)
                                sum-=ELEMENT(mat,i,k) * ELEMENT(mat,k,j);
                        AFFECT(mat,i,j,sum);
                }
                big=0.0;
                for(i=j;i<mat.lignes;i++)
                {
                        sum=ELEMENT(mat,i,j);
                        for(k=0;k<j;k++)
                                sum-=ELEMENT(mat,i,k) * ELEMENT(mat,k,j);
                        AFFECT(mat,i,j,sum);
                        if((dum=ELEMENT(vv,i,0)*Math.abs(sum))>=big)
                        {
                                big=dum;
                                imax=i;
                        }
                }
                if (j!=imax)
                {
                        for(k=0;k<mat.lignes;k++)
                        {
                                dum=ELEMENT(mat,imax,k);
                                AFFECT(mat,imax,k,ELEMENT(mat,j,k));
                                AFFECT(mat,j,k,dum);
                        }
                        d=-(d);
                        AFFECT(vv,imax,0,ELEMENT(vv,j,0));
                }
                //*(indx+j)=imax;
                indx[j]=imax;
                if (ELEMENT(mat,j,j)==0.0)
                {
                        AFFECT(mat,j,j,TINY);
                        System.out.println("La_matrice_est_certainement_singuliere");
                }
                if (j!=mat.lignes)
                {
                        dum=1.0/ ELEMENT(mat,j,j);
                        for(i=j+1;i<mat.lignes;i++)
                                AFFECT(mat,i,j,ELEMENT(mat,i,j) *dum);
                }
        }
}


/**
Solve the linear system of equation LU. To apply after decompose_lu. 
@param LU the left part of the system
@param indx array of size the matrix nb of lines:  indexes given by decompose_lu
@param Y the right part of the system
*/
public static void systeme_lu(FullMatrix LU, int[] indx, FullMatrix Y)
{
        int i,ii=-1,ip,j;
        double sum;

        for(i=0;i<LU.lignes;i++)
        {
                //ip=*(indx+i);
                ip=indx[i];
                sum=ELEMENT(Y,ip,0);
                AFFECT(Y,ip,0,ELEMENT(Y,i,0));
                if (ii>=0)
                        for(j=ii;j<i;j++)
                                sum-=ELEMENT(LU,i,j) * ELEMENT(Y,j,0);
                else
                        if (sum!=0) ii=i;
                AFFECT(Y,i,0,sum);
        }
        for(i=LU.lignes-1;i>=0;i--)
        {       
                sum=ELEMENT(Y,i,0);
                for(j=i+1;j<LU.lignes;j++)
                        sum-=ELEMENT(LU,i,j) * ELEMENT(Y,j,0);
                AFFECT(Y,i,0,sum/ ELEMENT(LU,i,i));
        }
}

public static void _inverse_lu(FullMatrix a, FullMatrix dest)
{
// double d=0;
int i,j;
int[] indx;
FullMatrix LU, col;

LU=copie_matrice(a);
col=new FullMatrix(LU.lignes,1);
// indx=(int *)ALLOUE(LU.lignes*sizeof(int));
indx=new int[LU.lignes];
decompose_lu(LU,indx);
for(j=0;j<LU.colonnes;j++)
	{
	for(i=0;i<LU.lignes;i++) AFFECT(col,i,0,0.0);
	AFFECT(col,j,0,1.0);
	systeme_lu(LU,indx,col);
	for(i=0;i<LU.lignes;i++)
	AFFECT(dest,i,j,ELEMENT(col,i,0));
	}
}


public static FullMatrix inverse_lu(FullMatrix a_inverser)
{
        FullMatrix m_tempo=null;
        
        if (a_inverser.lignes!=a_inverser.colonnes) return null;
        m_tempo=new FullMatrix(a_inverser.lignes,a_inverser.lignes);
        _inverse_lu(a_inverser,m_tempo);
        return m_tempo;
}


public static FullMatrix inverse_lu2(FullMatrix a_inverser)
{
int N=a_inverser.lignes;
double[][] a=new double[N+1][N+1];
for (int i=1;i<=N;i++)//ligne
	for (int j=1;j<=N;j++)//colonne
		a[i][j]=a_inverser.el(i-1,j-1);     //convention a[ligne][colonne]
	
double[][] inverse=LU.inverse(a);

FullMatrix m_tempo=new FullMatrix(N,N);
for (int i=1;i<=N;i++)//ligne
	for (int j=1;j<=N;j++)//colonne
		m_tempo.affect(i-1,j-1,inverse[i][j]);//convention inverse[ligne][colonne]

return m_tempo;
}

public static FullMatrix inverse_lu3(FullMatrix a_inverser)
{
int n=a_inverser.lignes;
double[][] inverse=LU2.inverse(a_inverser.tab);
return new FullMatrix(inverse);
}




/**maybe the sign is wrong*/
public double determinant_lu(FullMatrix mat)
{
        double produit=1.0,d=1 ;
        FullMatrix LU=null;
        int j;
        int[] indx;

        LU=copie_matrice(mat);
        indx=new int [LU.lignes];
        //indx=(int *)ALLOUE(LU.lignes*sizeof(int));
        decompose_lu(LU,indx);
        for(j=0;j<LU.lignes;j++)
                produit*= ELEMENT(LU,j,j);
        return (d*produit);
}

/********************************************************************
Return a column matrix containing the diagonal
 elements of the inverse matrix
This routine solves n linear systems, this gives the columns
of the inverse. Just the diagonal elements are stored
 *******************************************************************/
public FullMatrix calcElDiagInverse()
{		
FullMatrix eldinv=new FullMatrix(nbl(),1);
System.out.println("Calcule "+nbl()+" elements_diagonaux_algo_LU_sur_matrice_pleine");
// double d=0;
int[] indx;
FullMatrix LU, col;

System.out.println("Copie_matrice");
//LU=copie_matrice(this);
LU=this;
col=new FullMatrix(LU.lignes,1);
indx=new int[LU.lignes];
System.out.println("Decompose_LU");
decompose_lu(LU,indx);
for(int j=0;j<LU.colonnes;j++)
	{
	//System.out.println("Resoud_LU_elt_diag"+j+"/"+nbl());

	// prepare the right member of the linear system
	col.anulate();
	AFFECT(col,j,0,1.0);
	
	//solve the system
	systeme_lu(LU,indx,col);

	AFFECT(eldinv,j,0,ELEMENT(col,j,0));
	}
return eldinv;
}

public void removeColumn(int colToRemove) {
	double[][] newtab=new double[lignes][colonnes-1];
	for (int i = 0; i < lignes; i++)
		{
		for (int j = 0; j < colToRemove; j++) 
			{
			newtab[i][j] = tab[i][j];
			}
		for (int j = colToRemove+1; j < colonnes; j++) 
			{
			newtab[i][j-1] = tab[i][j];
			}
		} 
	colonnes--;	
	tab=newtab;
}































}