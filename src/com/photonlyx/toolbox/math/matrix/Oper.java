package com.photonlyx.toolbox.math.matrix;


/**

//*******************************************************************************************
//*************************** CLASSE OPERATION **********************************************
//*******************************************************************************************
for the storage of an operation made during elimination process
 */
import java.awt.Frame;

import com.photonlyx.toolbox.util.Messager;

 class Oper
{
static Frame PARENT;
short nEt;
short ind[];
double coef[];
short compteur; //indice la ou il mettre un nouvel elt


int nbBytes()//nb of Bytes used by one Oper
{
/*
double: 8 bytes
short: 2 bytes
reference to an El : 4 Bytes ????
*/
return (2+2+nEt*(2+8));
}; 

public Oper(short n)
{
nEt=n;
//System.out.println("Creation oper ordre"+nEt);
ind=new short[nEt];
coef=new double[nEt];
compteur=0;
}

public Oper(Oper o)
{
nEt=(short)(o.compteur);
//System.out.println("Creation oper ordre"+nEt);
ind=new short[nEt];
coef=new double[nEt];
compteur=0;
for (short i=0;i<nEt;i++) 
	{
	ind[i]=o.ind[i];
	coef[i]=o.coef[i];
	}
}

public short nEt()
{
return nEt;
}

public boolean met(short i,double c)
{
if (compteur==nEt) 
	{
	Messager.messErr("Serie (Oper) d'operations plein");
	return false;
	}
ind[compteur]=i;
coef[compteur]=c;
compteur++;
return true;
}

public void reset()
{
compteur=0;
}

public double coef(short i)
{
if ((i>nEt)||(i<1)) 
	{
	Messager.messErr("Serie d'operations : indice hors serie :"+i);
	return 0;
	}
return coef[i-1];
}

public short ind(short i)
{
if ((i>nEt)||(i<1)) 
	{
	Messager.messErr("Serie d'operations : indice hors serie"+i);
	return 0;
	}
return ind[i-1];
}

public String print()
{
String s=new String();
for (short i=0;i<nEt;i++) s=s+"\t"+(i+1)+"\t"+ind[i]+"\t"+coef[i]+"\n";
return s;
}


}  // fin classe Oper



