package com.photonlyx.toolbox.math.matrix;


import java.io.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

import com.photonlyx.toolbox.util.Messager;

import java.lang.Math;



/**
//*******************************************************************************************
//*************************** CLASSE MATRICE CREUSE *****************************************
//*******************************************************************************************
*/

public class Creuse implements Matrix
{
static Frame PARENT;
public int nbl;
public int nbc;
int nbel;
int nbNouvnuls;
El el11;
El ligne1[];
El colonne1[];
El elCourant,elSuivant;

public Creuse(int nnbl,int nnbc)
{
nbl=nnbl;
nbc=nnbc;
System.out.println("Creation matrice creuse"+nbl+"x"+nbc);
initialise();
}




/******************************************************/
//Matrix interface implementation
/******************************************************/
public int getNbRows()
{
return (int)nbl();
}

public int getNbColumns()
{
return (int)nbc();
}

public void put(int row,int column,double value)
{
row++;
column++;
metEl((int)row,(int)column,value);
}

public void increment(int row,int column,float value)
{
row++;
column++;
El el=chEl((int)row,(int)column);
if (el!=null) el.v+=(double)value;
else metEl((int)row,(int)column,(double)value);
}

public double get(int row,int column)
{
row++;
column++;
return val((int)row,(int)column);
}


public void plotWithGrayLevels(Graphics g,int width,int height,int xoffset,int yoffset,boolean log)
{
El el;
Creuse mat;

mat=this;
double max=mat.getMax();

int pixelW=(width-2*xoffset)/mat.nbc;
int pixelH=(height-2*yoffset)/mat.nbl;
//System.out.println("pixelW"+pixelW+" pixelH"+pixelH);

Color colorSave=g.getColor();
g.setColor(new Color(0,0,0));
g.fillRect(xoffset,yoffset,pixelW*mat.nbc,pixelH*mat.nbl);

for (int l=1;l<=mat.nbl;l++) //balaye les lignes
	{
	el=mat.chElLigne(l,(int)1); //se place en debut de ligne de mat
	plot(g,xoffset,yoffset,el,pixelH,pixelW,max,log);
	for (;;)
		{
		el=el.sl;
		if (el==null) break;
		plot(g,xoffset,yoffset,el,pixelH,pixelW,max,log);
		}
	}
g.setColor(colorSave);
}

public void save(String filename)
{
sauve(filename);
}

public double getMax()
{
El el;
Creuse mat=this;
double r=-1e300;
for (int l=1;l<=mat.nbl;l++) //balaye les lignes
	{
	el=mat.chElLigne(l,(int)1); //se place en debut de ligne de mat
	if (el.v>r) r=el.v;
	for (;;)
		{
		el=el.sl;
		if (el==null) break;
		if (el.v>r) r=el.v;
		}
	}
return r;
}



public double getStrictPositiveMin()
{
El el;
Creuse mat=this;
double r=1e300;
for (int l=1;l<=mat.nbl;l++) //balaye les lignes
	{
	el=mat.chElLigne(l,(int)1); //se place en debut de ligne de mat
	if ((el.v<r)&&(el.v>0))  r=el.v;
	for (;;)
		{
		el=el.sl;
		if (el==null) break;
		if ((el.v<r)&&(el.v>0))   r=el.v;
		}
	}
return r;
}



public double getMin()
{
El el;
Creuse mat=this;
double r=1e300;
for (int l=1;l<=mat.nbl;l++) //balaye les lignes
	{
	el=mat.chElLigne(l,(int)1); //se place en debut de ligne de mat
	if (el.v<r)  r=el.v;
	for (;;)
		{
		el=el.sl;
		if (el==null) break;
		if (el.v<r)   r=el.v;
		}
	}
return r;
}


/**************************************************************/
/*  end interface Matrix implementation*/
/***************************************************************/

public void plot(Graphics g,int xoffset,int yoffset,El el,int pixelH,int pixelW,double max,boolean log)
{
float f;
if (max==0) f=0;
 else 
	{
	if (log) f=(float)(Math.log(el.v+1)/Math.log(max+1));
	else f=(float)(el.v/max);
	}
if ((f<0)||(f>1)) return;
g.setColor(new Color(f,f,f));
int x=yoffset+((int)el.l-1)*pixelH;
int y=xoffset+((int)el.c-1)*pixelW;
g.fillRect(y,x,pixelW,pixelH);
}


public void normalise()
{
El el;
Creuse mat=this;
double max=mat.getMax();
for (int l=1;l<=mat.nbl;l++) //balaye les lignes
	{
	el=mat.chElLigne(l,(int)1); //se place en debut de ligne de mat
	el.v/=max;
	for (;;)
		{
		el=el.sl;
		if (el==null) break;
		el.v/=max;
		}
	}
}


public void initialise()
{
//System.out.println("Initialisation matrice creuse"+nbl+"x"+nbc);
nbel=nbl+nbc-1;
el11=new El((int)1,(int)1,0);
ligne1=new El[nbc+1];
colonne1=new El[nbl+1];

colonne1[2]=new El((int)2,(int)1,0);
el11.sc=colonne1[2];
for (int i=3;i<=nbl;i++) 
	{
	colonne1[i]=new El(i,(int)1,0);
	colonne1[i-1].sc=colonne1[i];
	}

ligne1[2]=new El((int)1,(int)2,0);
el11.sl=ligne1[2];
for (int j=3;j<=nbc;j++) 
	{
	ligne1[j]=new El((int)1,j,0);
	ligne1[j-1].sl=ligne1[j];
	}
}

/*******************************
* return a copy of the matrix  *
*******************************/

public Creuse copie()
{
Creuse cop;
El el;
cop=new Creuse(nbl,nbc);
System.out.println("copie matrice..");
for (int l=1;l<=nbl;l++)
	{
	el=chElLigne(l,(int)1); //se place en debut de ligne
	for (;;)
		{
		cop.metEl(el.l,el.c,el.v);
		el=el.sl;
		if (el==null) break; //sort si fin de ligne
		}
	}
return cop;
}




public Creuse(String nomFichier)
{
FileReader r;
StreamTokenizer st;
int N,M;
try 
	{
	r = new FileReader(nomFichier);
	st=new StreamTokenizer(r);
	st.nextToken();		
	N=(int)st.nval;
	st.nextToken();		
	M=(int)st.nval;
	System.out.println("Creation depuis fichier matrice creuse ordre:"+N+"x"+M);
	nbl=N;
	nbc=M;
	initialise();
	for (int i=1;i<=N;i++)
		{ 
		for (int j=1;j<=M;j++) 
			{
			st.nextToken();
			metEl(i,j,st.nval);	
			}
		}
	r.close();
	}
catch (Exception e) 
	{
	System.out.println("Erreur : "+e);
	}
	
}


public boolean sauve(String nomfichier)
{
System.out.println("Enregistrement matrice creuse dans "+nomfichier);
try
	{
	PrintWriter st=new PrintWriter(new FileOutputStream(nomfichier));
	st.print(print(false));
	st.close();
	return true;
	}
catch (Exception e)
	{
	System.out.println("Erreur ecriture matrice creuse: "+e);
	return false;
	}
}

public boolean sauvediag(String nomfichier)
{
System.out.println("Enregistrement diagonale matrice creuse dans "+nomfichier);
try
	{
	PrintWriter st=new PrintWriter(new FileOutputStream(nomfichier));
	st.print(printdiag());
	st.close();
	return true;
	}
catch (Exception e)
	{
	System.out.println("Erreur ecriture matrice creuse: "+e);
	return false;
	}
}

public int nbl()
{
return nbl;
}
public int nbc()
{
return nbc;
}

/**return the elt l,c, scan the line l, start the search at the begining of the line l*/
public El chElLigne(int l,int c)
{
//System.out.println("chElLigne "+l+","+c);
if ((l==1)&&(c==1)) return el11;
if (l==1) return ligne1[c];
if (c==1) return colonne1[l];
elCourant=colonne1[l];
for (;;)
	{
	elSuivant=elCourant.sl;
	if (elSuivant==null) return null;
	if (elSuivant.c>c) return null;
	if (elSuivant.c==c) return elSuivant;
	elCourant=elSuivant;
	}
}

/**return the elt l,c, scan the line l, start the search at depart*/
public El chElLigne(int l,int c,El depart)
{
//System.out.println("chElLigne "+l+","+c);
if ((l==1)&&(c==1)) return el11;
if (l==1) return ligne1[c];
if (c==1) return colonne1[l];
if (depart.c==c) return depart;
elCourant=depart;
for (;;)
	{
//	System.out.println("depart elCourant :"+depart.l+" "+depart.c+"     "+elCourant.l+" "+elCourant.c);
	elSuivant=elCourant.sl;
	if (elSuivant==null) return null;
	if (elSuivant.c>c) return null;
	if (elSuivant.c==c) return elSuivant;
	elCourant=elSuivant;
	}
}


/** return the elt l,c, scan the column c, 
start the search at the begining of the column c */
public El chElColonne(int l,int c)
{
//System.out.println("chElColonne "+l+","+c);
if ((l==1)&&(c==1)) return el11;
if (l==1) return ligne1[c];
if (c==1) return colonne1[l];
elCourant=ligne1[c];
for (;;)
	{
	elSuivant=elCourant.sc;
	if (elSuivant==null) return null;
	if (elSuivant.l>l) return null;
	if (elSuivant.l==l) return elSuivant;
	elCourant=elSuivant;
	}
}

/** return the elt l,c, scan the column c, 
start the search at the elt depart */
public El chElColonne(int l,int c,El depart)
{
//System.out.println("chElColonne "+l+","+c);
if ((l==1)&&(c==1)) return el11;
if (l==1) return ligne1[c];
if (c==1) return colonne1[l];
elCourant=depart;
for (;;)
	{
	elSuivant=elCourant.sc;
	if (elSuivant==null) return null;
	if (elSuivant.l>l) return null;
	if (elSuivant.l==l) return elSuivant;
	elCourant=elSuivant;
	}
}


/**return the elt (l,c), no optimisation*/
public El chEl(int l,int c)
{
if (l>=c) return chElLigne(l,c);
else return chElColonne(l,c);
}


public El metEl(int l,int c,double v)
{
El el;
El eldc,eldl;

//System.out.println("metEl "+l+","+c+" "+v);

if ((l<1)||(c<1)||(l>nbl)||(c>nbc)) 
	{
	System.out.println("Indice hors intervalle :"+l+","+c);
	return null;
	}

el=chElLigne(l,c);
if (el!=null) 
	{
	el.v=v;
	return el;
	}
if (v==0) return el;
el=new El(l,c,v);
elCourant.sl=el;
el.sl=elSuivant;
nbel++;

//met les liens colonne :
chElColonne(l,c);
elCourant.sc=el;
el.sc=elSuivant;

return el;
}

/**version optimisee: faire la recherche a partir de depart.
 Suppose que cet element n'existe pas (si il existe il est remplace)
 ,suppose que v n'est pas nul !
departL is an El on the same line before the new El
departC is an El on the same column before the new El */
public El metEl(int l,int c,double v,El departL,El departC)
{
El el;
El eldc,eldl;

//System.out.println("metEl "+l+","+c+" "+v);

el=chElLigne(l,c,departL);
if (el!=null) 
	{
	el.v=v;
	return el;
	}
el=new El(l,c,v);
elCourant.sl=el;
el.sl=elSuivant;
nbel++;

//met les liens colonne :
chElColonne(l,c,departC);
elCourant.sc=el;
el.sc=elSuivant;

return el;
}

public void libereEl(int l,int c) //utiliser si l'elt en (l,c) existe deja !
{
El el;
el=chElLigne(l,c);
if ((l==1)||(c==1))
	{
	el.v=0.0;
	return;
	}
elCourant.sl=el.sl;
el=chElColonne(l,c);
elCourant.sc=el.sc;
nbel--;
}





public double val(int l,int c)
{
El el=chEl(l,c);
if (el==null) return 0;else return el.v;
}

public String print()
{
return print(false);
}

public String printbete(boolean complete)
{
El el;
String s=new String();
//System.out.println("Impression");
s+=printProprietes();
for (int i=1;i<=nbl;i++)
	{
	for (int j=1;j<=nbc;j++)
		{
//		System.out.print(i+","+j+"\t");
		if (!complete) s=s+val(i,j)+"\t";
		else
			{
			el=chEl(i,j);
			if (el==null) s=s+"("+i+","+j+")"+" v=0\t\t\t";
			else s=s+el.print()+"\t";
			}
		}
	s=s+"\n";
//	System.out.print("\n");
	}
return s;
}


public String print(boolean complete)
{
NumberFormat nf=new DecimalFormat("+0.000E0;-0.000E0");
El el;
StringBuffer s=new StringBuffer();
s.append(printProprietes());
for (int l=1;l<=nbl;l++) //balaye les lignes
	{
	el=chElLigne(l,(int)1); //se place en debut de ligne de A
	s.append(nf.format(el.v)+"\t");
	for (;;)
		{
		int c1=el.c;
		el=el.sl;
		if (el==null) 
			{
			for (int i=1;i<=(nbc-c1);i++) s.append(nf.format(0.0)+"\t");	
			break;
			}
		for (int i=1;i<=(el.c-c1-1);i++) s.append(nf.format(0.0)+"\t");
		s.append(nf.format(el.v)+"\t");
		}
	s.append("\n");
	}
return s.toString();
}


public String printdiag()
{
El el;
int n;
StringBuffer s=new StringBuffer();
s.append(printProprietes());
n=nbl;
if (nbc<nbl) n=nbc;
for (int l=1;l<=n;l++) //balaye les lignes
	{
	s.append(val(l,l)+"\n");
	}
return s.toString();
}

public int memoryUsed()
{
return nbel*El.nbBytes;
}


public String printProprietes()
{
El el;
String s=new String();
//System.out.println("Impression");
s=s+"Matrix "+nbl+"x"+nbc+" sparse "+nbel+" non null elements ("
	+(int)((double)nbel/(double)nbl/(double)nbc*100.0)+"%)\n";
s=s+"Memory size :"+nbel*20+" octets   ";
s=s+"instead of "+nbl*nbc*8+" octets\n";
return s;
}

public Creuse transpose()
{
El el;
Creuse Abis=new Creuse(nbc,nbl);

System.out.println(Messager.getString("transpose_matrice"));
for (short l=1;l<=nbl;l++) //balaye les lignes
        {
        el=chElLigne(l,(short)1); //se place en debut de ligne
        for (;;)
                {
                Abis.metEl(el.c,el.l,el.v);
                el=el.sl;
                if (el==null) break; //sort si fin de ligne
                }
        }
return Abis;
}

public FullMatrix multiplie(FullMatrix B)
{
El elA;
Creuse A=this;
FullMatrix AB=new FullMatrix(A.nbl,B.nbc());
double p;

if (A.nbc!=B.nbl())
        {
        Messager.messErr(Messager.getString("Pas_possible_de_multipier_ces_matrices"));
        return null;
        }

for (short l=1;l<=AB.nbl();l++) //balaye les lignes de AB
        for (short c=1;c<=AB.nbc();c++) //balaye les colonnes de AB
                {
                elA=A.chElLigne(l,(short)1); //se place en debut de ligne de A
                p=0.0;
                for(;;)
                        {
                        p=p+elA.v*B.el(elA.c-1,c-1);
                        elA=elA.sl;
                        if (elA==null) break;
                        }
                AB.affect(l-1,c-1,p);
                }
return AB;
}




public Creuse multiplie(Creuse B)
{
El elA,elB;
Creuse A=this;
Creuse AB=new Creuse(A.nbl,B.nbc);
double p;

if (A.nbc!=B.nbl)
	{
	Messager.messErr(Messager.getString("Pas_possible_de_multipier_ces_matrices"));
	return null;
	}

for (short l=1;l<=AB.nbl;l++) //balaye les lignes
	for (short c=1;c<=AB.nbc;c++) //balaye les colonnes
		{
		elA=A.chElLigne(l,(short)1); //se place en debut de ligne de A
		elB=B.chElColonne((short)1,c); //se place en debut de colonne de B
		p=0.0;
		for(;;)
			{
//			if ((l==10)&&(c==6))
//		System.out.println(l+" "+c+"->"+" A"+elA.l+" "+elA.c+"  B"+elB.l+" "+elB.c);
			if (elA.c==elB.l)
				{
				p=p+elA.v*elB.v;
				elA=elA.sl;
				elB=elB.sc;
				}
			if ((elA==null)||(elB==null))break;
			if (elA.c<elB.l)
				for(;;)
					{
					elA=elA.sl;
					if (elA==null) break;
					if (elA.c>=elB.l) break;
//			if ((l==10)&&(c==6))
//		System.out.println(l+" "+c+"->"+" A"+elA.l+" "+elA.c+"  B"+elB.l+" "+elB.c);
					}
			else if (elA.c>elB.l)
				for(;;)
					{
					elB=elB.sc;
					if (elB==null) break;
					if (elB.l>=elA.c) break;
//			if ((l==10)&&(c==6))
//		System.out.println(l+" "+c+"->"+" A"+elA.l+" "+elA.c+"  B"+elB.l+" "+elB.c);
					}
			if ((elA==null)||(elB==null))break;
			}
		AB.metEl(l,c,p);
		}
return AB;
}
/********************************************************************
* return a copy of the matrix of type MatricePleine (full storage)  *
********************************************************************/

public FullMatrix copiePleine()
{
FullMatrix cop=new FullMatrix(nbl,nbc);
El el;
System.out.println(Messager.getString("Copy_a_sparse_matrix_into_a_full_matrix"));
for (short l=1;l<=nbl;l++)
	{
	el=chElLigne(l,(short)1); //se place en debut de ligne
	for (;;)
		{
		cop.affect(el.l-1,el.c-1,el.v);//the MatricePleine cop is zero based !!!!!!!!!
		el=el.sl;
		if (el==null) break; //sort si fin de ligne
		}
	}
return cop;
}


public boolean elimination(Creuse B)
{
short N=(short)nbl;
El elkk,elik,elkj,elij,elBkj,elBij;
double coef;

for (short k=1;k<=N;k++)
	{
	elkk=chEl(k,k);
	if (elkk==null)
		{
		Messager.messErr(Messager.getString("Pivot_nul_dans_elimination"));
		return false;
		}
//	System.out.println("Pivot elkk :"+elkk.print());
	elik=elkk;//prepare el elik
	for (;;)
		{
		elik=elik.sc; //balaye la colonne en dessus du pivot
		if (elik==null) break; //sort si fin de colonne
//		System.out.print("elik :"+elik.print()+"  ");
		coef=elik.v/elkk.v;
		libereEl(elik.l,k); //annulle et libere la case (i,k)
		elkj=elkk; //prepare elkj
		for (;;)//balayage sur la ligne apres le pivot
			{
			elkj=elkj.sl;  //avance l'elt kj
			if (elkj==null) break; //sort si hors ligne de l'elt kj
//			System.out.println("elkj :"+elkj.print());
			elij=chEl(elik.l,elkj.c);
			if (elij==null) metEl(elik.l,elkj.c,-coef*elkj.v); // cas ou pas de ij
			else elij.v=elij.v-coef*elkj.v; // cas il existe deja le ij
			}
		elBkj=B.chEl(k,(short)1);//passe au premier elt de B sur la ligne i
		for (;;) //balayage sur la matrice B
			{
			elBij=B.chEl(elik.l,elBkj.c);
			if (elBij==null) B.metEl(elik.l,elBkj.c,-coef*elBkj.v);// cas ou pas de ij
			else elBij.v=elBij.v-coef*elBkj.v; // cas ou il existe deja le ij
			elBkj=elBkj.sl;
			if (elBkj==null) break; //sort si l'elt kj hors ligne
			}
		}
	}
return true;
}


public boolean retroSub(Creuse B,Creuse X)
{
short N=(short)nbl;
El Akk,Akj,Bkp;
double Bkpv;

for (short k=N;k>=1;k--)
	{
	Akk=this.chElLigne(k,k); //cherche le pivot
//	System.out.println("A("+k+","+k+")");
	for (short p=1;p<=N;p++)  //balaye sur toutes les colonnes de l'inverse
		{
		Bkp=B.chEl(k,p); //cherche le Bkp
//		System.out.println("\tB("+k+","+p+")");
		if (Bkp!=null) Bkpv=Bkp.v;  //prend la valeur en Bkp
		else Bkpv=0.0;
		Akj=Akk;      // se positionne sur le pivot de A
		for (;;)
			{
			Akj=Akj.sl;
			if (Akj==null) break;
//			System.out.print("\t\tA("+k+","+Akj.c+")");
//			System.out.println(" X("+Akj.c+","+p+")");
			Bkpv=Bkpv-Akj.v*X.val(Akj.c,p);
			}
		X.metEl(k,p,Bkpv/Akk.v); // met le resultat en Xkp
		}
	}
return true;
}


public boolean echangeLignes(short l1,short l2)
{
if ((l1<1)||(l1>nbl)||(l2<1)||(l2>nbl))
	{
	Messager.messErr("Ne peux echanger lignes "+l1+" et "+l2);
	return false;
	}
return true;
}

public boolean elimination(FullMatrix b)
{
short N=(short)nbl;
El elkk,elik,elkj,elij,depart;
double coef;
short i,j;

for (short k=1;k<=N;k++)
	{
	elkk=chEl(k,k);
	if (elkk==null)
		{
		Messager.messErr(Messager.getString("Pivot_nul_dans_elimination"));
		return false;
		}
//	System.out.println("Pivot elkk :"+elkk.print());
	elik=elkk;//prepare el elik
	for (;;)
		{
		elik=elik.sc; //balaye la colonne en dessus du pivot
		if (elik==null) break; //sort si fin de colonne
//		System.out.print("elik :"+elik.print()+"  ");
		i=(short)elik.l;//i contient le numero de ligne
		coef=elik.v/elkk.v;
		libereEl(elik.l,k); //annulle et libere la case (i,k)
		elkj=elkk; //prepare elkj
		depart=elik;//prepare pour le balayage de la ligne i apres le pivot
		for (;;)//balayage sur la ligne k apres le pivot
			{
			elkj=elkj.sl;  //avance l'elt kj
			if (elkj==null) break; //sort si hors ligne de l'elt kj
//			System.out.println("elkj :"+elkj.print());
			j=(short)elkj.c;
			elij=chElLigne(i,j,depart);//cherche sur la ligne l'element ij en partant de depart
			if (elij==null) // cas ou pas de ij
				{
				metEl(i,j,-coef*elkj.v);
				}
			else  // cas il existe deja le ij
				{
				elij.v=elij.v-coef*elkj.v;
				depart=elij;//avance le depart de recherche
				}
			elij=chEl(i,j);
			if (elij==null) metEl(i,j,-coef*elkj.v); // cas ou pas de ij
			else elij.v=elij.v-coef*elkj.v; // cas il existe deja le ij
			}
		b.affc1(i-1,b.elc1(i-1)-coef*b.elc1(k-1)); //traite le vecteur b
		}
	}
return true;
}


public TabOper eliminationStocke(FullMatrix b)
// elimination avec matrice b colonne MatricePleine et conservation des operations
{
short N=(short)nbl;
El elkk,elik,elkj,elij,departL;
double coef;
short i,j;
Oper op=new Oper((short)(N-1));
TabOper tabOp=new TabOper(N);


System.out.println(Messager.getString("Elimination_avec_stockage_des_operations"));
// Fattente fat=new Fattente(Messager.getString("Elimination"));

//FenetreMatrice fmat=new FenetreMatrice(PARENT,this);// affiche la matrice******
//fmat.repaint();// affiche la matrice******
//Fmessage fmes;//stope pour afficher********
//fmes=new Fmessage(PARENT,"Regarde la matrice");//stope pour afficher********

for (short k=1;k<=N;k++)
	{
// 	fat.update(Messager.getString("Pivot")+" "+k+"/"+N);
//	System.out.println("Pivot "+k+"/"+N);
	elkk=chEl(k,k);
	if (elkk==null)
		{
		Messager.messErr(Messager.getString("Pivot_nul_dans_elimination"));
		return null;
		}
//	System.out.println("Pivot elkk :"+elkk.print());
	elik=elkk;//prepare el elik
	for (;;)
		{
		elik=elik.sc; //balaye la colonne en dessus du pivot
		if (elik==null) break; //sort si fin de colonne
//		System.out.print("elik :"+elik.print()+"  ");
		i=(short)elik.l;
		coef=elik.v/elkk.v;
		libereEl(elik.l,k); //annulle et libere la case (i,k)
		elkj=elkk; //prepare elkj
		departL=elik;//prepare pour le balayage de la ligne i apres le pivot
		for (;;)//balayage sur la ligne k apres le pivot
			{
			elkj=elkj.sl;  //avance l'elt kj
			if (elkj==null) break; //sort si hors ligne de l'elt kj
//			System.out.println("elkj :"+elkj.print());
			j=(short)elkj.c;
			elij=chElLigne(i,j,departL);//cherche sur la ligne l'element ij en partant de depart
			if (elij==null) // cas ou pas de ij
				{
//				if (elkj.v!=0.) departL=metEl(i,j,-coef*elkj.v,departL,elkj);
				metEl(i,j,-coef*elkj.v);
				}
			else  // cas il existe deja le ij
				{
				elij.v=elij.v-coef*elkj.v;
				departL=elij;//avance le depart de recherche
				}
			}
		b.affc1(i-1,b.elc1(i-1)-coef*b.elc1(k-1)); //traite le vecteur b
		op.met(i,coef);//stocke l'operation
		}
	tabOp.met(new Oper(op));
	op.reset();

//	fmat.repaint();// affiche la matrice*********
//	fmes=new Fmessage(PARENT,"Regarde la matrice");//stope pour afficher********

	}
// fat.dispose();
return tabOp;
}



/**retrosub avec matrice pleine

*/
public boolean retroSub(FullMatrix b,FullMatrix x,short arret)
{
short N=(short)nbl;
El Akk,Akj;
short j;

for (short k=N;k>=arret;k--)
	{
	Akk=this.chEl(k,k); //cherche le pivot
	if (Akk==null)
		{
		Messager.messErr(Messager.getString("Pivot_nul_dans_elimination"));
		return false;
		}
//	System.out.println("A("+k+","+k+")");
	Akj=Akk;      // se positionne sur le pivot de A
	for (;;)
		{
		Akj=Akj.sl; //balaye a partie de A
		if (Akj==null) break;
		j=(short)Akj.c;
//		System.out.print("\t\tA("+k+","+j+")");
		b.affc1(k-1,b.elc1(k-1)-x.elc1(j-1)*Akj.v);
		}
	x.affc1(k-1,b.elc1(k-1)/Akk.v);
	}
return true;
}

public boolean resoud(FullMatrix b)
{
if (elimination(b)) return retroSub(b,b,(short)1); else return false;
}

public FullMatrix calcElDiagInverse(TabOper t)
{		
FullMatrix bb,eldinv;

bb=new FullMatrix(nbl,1);
eldinv=new FullMatrix(nbl,1);

bb.affc1(0,1.0);
t.clean();
t=eliminationStocke(bb);
retroSub(bb,bb,(short)1);
eldinv.affc1(0,bb.elc1(0));
//System.out.println("elt diag 1/"+nbl);
//fat.update(Simulgeo.messages.getString("elt_diag")+" 1/"+nbl);
for (short i=2;i<=nbl;i++) 
	{
	bb.anulate();
	bb.affc1(i-1,1.0);
//	t.eliminationAvecTaboper(bb,i);
	t.eliminationAvecTaboper(bb);
	retroSub(bb,bb,i);
//	System.out.println("elt diag"+i+"/"+nbl);
	//fat.update(Simulgeo.messages.getString("elt_diag")+" "+i+"/"+nbl);
	eldinv.affc1(i-1,bb.elc1(i-1));
	}
//fat.dispose();
return eldinv;
}







}// fin classe matrice creuse







