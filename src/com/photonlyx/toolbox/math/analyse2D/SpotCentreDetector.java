
// Author Laurent Brunel
package com.photonlyx.toolbox.math.analyse2D;

import com.photonlyx.toolbox.math.signal.Signal2D1D;


/**
locate the centre of a spot
*/
public class SpotCentreDetector 
{
//links
private Signal2D1D imSource;//the spot image
//in
private double threshold=0.9;
private boolean invertContrast=false;//if true invert contract: black<->white
private boolean barycentreWithAllValues=false;//if true invert contract: black<->white
private int x0=0;// x of the ROI corner
private int y0=0;// y of the ROI corner
private int w0 ;//width of the ROI
private int h0 ;//height of the ROI
 
//out
private double spotCentrex,spotCentrey;




public Signal2D1D getImSource() {
	return imSource;
}


public void setImSource(Signal2D1D imSource)
{
w0=imSource.dimx();
h0=imSource.dimy();
this.imSource = imSource;
}


public double getThreshold() {
	return threshold;
}


public void setThreshold(double threshold) {
	this.threshold = threshold;
}


public boolean isInvertContrast() {
	return invertContrast;
}


public void setInvertContrast(boolean invertContrast) {
	this.invertContrast = invertContrast;
}


public boolean isBarycentreWithAllValues() {
	return barycentreWithAllValues;
}


public void setBarycentreWithAllValues(boolean barycentreWithAllValues) {
	this.barycentreWithAllValues = barycentreWithAllValues;
}


public double getSpotCentrex() {
	return spotCentrex;
}


public double getSpotCentrey() {
	return spotCentrey;
}



public int getX0() {
	return x0;
}


public void setX0(int x0) {
	this.x0 = x0;
}


public int getY0() {
	return y0;
}


public void setY0(int y0) {
	this.y0 = y0;
}


public int getW0() {
	return w0;
}


public void setW0(int w0) {
	this.w0 = w0;
}


public int getH0() {
	return h0;
}


public void setH0(int h0) {
	this.h0 = h0;
}


public void calculate()
{
//create a copy of the image
Signal2D1D imc=imSource.copy();
if (imc==null) return;
//boolean _barycentreWithAllValues=false;
//if (barycentreWithAllValues) _barycentreWithAllValues=true;
double[] c;//=new double[2]	;

if (invertContrast) 
	{
	imc._invertContrast();
//	imc._multiply(-1);
//	imc._add(1);
	}

if (barycentreWithAllValues)
	{
	//imc._add(-imc.minValue());
	c = imc.barycentreThresholdRelative(threshold,x0,y0,w0,h0);
	}
else
	{
//	//the threshold is applied to get a binary image
//	imc._thresholdRelative(threshold,x0,y0,w0,h0);
//	//the barycentre of the binary image  is taken
//	c = imc.barycentre(x0,y0,w0,h0);
	c = imc.barycentreThresholdRelative(threshold,x0,y0,w0,h0);
	}
//ouput for the user
spotCentrex=c[0];
spotCentrey=c[1];
//System.out.println("xcentre="+this.getSpotCentrex()+" ycentre="+this.getSpotCentrey());

//debug
//int size;
//size=imc.binaryObjectSize(threshold,x0,y0,w0,h0);
////size=imc.binaryObjectSize(threshold,0,0,imc.dimx(),imc.dimy());
//System.out.println("Size of object="+size);

}



}
