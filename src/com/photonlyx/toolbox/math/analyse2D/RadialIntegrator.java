
// Author Laurent Brunel
package com.photonlyx.toolbox.math.analyse2D;

import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.Signal2D1DSource;


/**
A radial profile around a center of a 2D1D signal.Only the biggest
circle inserted in the signal is counted</p>
*/
public class RadialIntegrator 
{
//links
private Signal2D1D im;//the spot image
private Signal1D1DXY profile;
//params in 
private double spotCentrex,spotCentrey;
private double rmin=0;//min radius
private double rmax;//max radius
private int dimProfile=100;//the number of points
private double scale=1;//scale to apply 
private boolean normalise=false;//if true normalise the result
private double normaliseCoef=1;//set the max value to this one
private double alphaMin=-180;//min polar angle to consider in degrees
private double alphaMax=+180;//min polar angle to consider in degrees
private boolean profileInLogx=false;
private boolean setOutPixelsToZero=false;

public RadialIntegrator()
{
}

public RadialIntegrator(Signal2D1D im, Signal1D1DXY profile)
{
super();
this.im = im;
this.profile = profile;
}






public void calculate()
{
if (im==null) return;
//compute the radius of the biggest inserted circle
//double rx=Math.min(spotCentrex-im.xmin(),im.xmax()-spotCentrex);
//double ry=Math.min(spotCentrey-im.ymin(),im.ymax()-spotCentrey);
//double _rmax=Math.min(rx,ry);
//if (rmax>_rmax) rmax=_rmax;
double sampling=(rmax-rmin)/(double)dimProfile;

double logrmin=Math.log10(rmin);
double logrmax=Math.log10(rmax);
double samplingLog=(logrmax-logrmin)/(double)dimProfile;
//af("spotCentrex.val"+spotCentrex.val);
//af("spotCentrey.val"+spotCentrey.val);
//af("im.xmax()"+im.xmax());
//af("im.ymax()="+im.ymax());
//af("rx="+rx);
//af("ry="+ry);
//af("rmax="+rmax.val);

//create the empty profile
if (profile.getDim()!=(int)dimProfile) 	profile.init((int)dimProfile);
else profile._annulate();
//start the profile calculation
//double profileSampling=(rmax-rmin)/(dimProfile-1);
double sampx=im.samplingx();
double sampy=im.samplingy();
double xx,yy,d;
int cell;
double[] counter=new double[(int)dimProfile];
double _alphaMinRad=-Math.PI,_alphaMaxRad=Math.PI;
_alphaMinRad=alphaMin*Math.PI/180.0;
_alphaMaxRad=alphaMax*Math.PI/180.0;
double alpha=0;//polar angle of the pixel considered
for (int i=0;i<im.dimx();i++)//scan all the pixels
	for (int j=0;j<im.dimy();j++)
	{
	xx=im.xmin()+i*sampx-spotCentrex;//co-ordinate of the current pixel wrt image centre
	yy=im.ymin()+j*sampy-spotCentrey;
	d=Math.sqrt(xx*xx+yy*yy);//distance of current pixel from the centre
	alpha=Math.atan2(yy, xx);//polar angle
	if ((d<rmax)&&(d>rmin)&&(alpha>_alphaMinRad)&&(alpha<_alphaMaxRad))//check if pixel is the good ring sector
		{
		//calculate the profile cell corresponding to the current pixel
		if (!profileInLogx) cell=(int)Math.floor((d-rmin)/sampling);
		else cell=(int)Math.floor((Math.log10(d)-logrmin)/samplingLog);
		//increment the profile cell value with pixel value
		//System.out.println(i+" "+j+" "+cell);
		if (cell<profile.getDim()) 
			{
			profile.setValue(cell,profile.value(cell)+im.getValue(i,j));
			counter[cell]++;//count the pixel added to this cell
			}
		}
	else if (setOutPixelsToZero) im.setValue(i,j,0);//the pixels out of the ring sector are put to 0
         //else System.out.println();
	}

//now the mean value are calculated thanks to counters values
for (int i=0;i<dimProfile;i++)
	{
	double yval;
	if (counter[i]!=0) yval=profile.value(i)/counter[i];else yval=0;
	if (!profileInLogx) profile.setValue(i,(rmin+sampling*i)*scale,yval);
	else profile.setValue(i,Math.pow(10,logrmin+samplingLog*i),yval);
	}

if (normalise)
	{
	//af("normalise");
	profile._multiply(normaliseCoef*1/profile.ymax());
	}
//af(profil);
//System.out.println(this);
}





public double getRmin() 
{
return rmin;
}



public void setRmin(double rmin) 
{
this.rmin = rmin;
}



public int getDimProfile() 
{
return dimProfile;
}



public void setDimProfile(int dimProfile) 
{
this.dimProfile = dimProfile;
}



public Signal1D1DXY getProfile() {
	return profile;
}



public void setProfile(Signal1D1DXY profile) {
	this.profile = profile;
}



public Signal2D1D getIm() {
	return im;
}



public double getScale() {
	return scale;
}



public void setScale(double scale) {
	this.scale = scale;
}



public boolean isNormalise() {
	return normalise;
}



public void setNormalise(boolean normalise) {
	this.normalise = normalise;
}



public double getNormaliseCoef() {
	return normaliseCoef;
}



public void setNormaliseCoef(double normaliseCoef) {
	this.normaliseCoef = normaliseCoef;
}



public double getAlphaMin() {
	return alphaMin;
}



public void setAlphaMin(double alphaMin) {
	this.alphaMin = alphaMin;
}



public double getAlphaMax() {
	return alphaMax;
}



public void setAlphaMax(double alphaMax) {
	this.alphaMax = alphaMax;
}



public double getRmax() {
	return rmax;
}



public void setRmax(double rmax) {
	this.rmax = rmax;
}



public Signal1D1DXY getProfil() {
	return profile;
}

public void setIm(Signal2D1D im) {
	this.im = im;
}



public double getSpotCentrex() {
	return spotCentrex;
}



public void setSpotCentrex(double spotCentrex) {
	this.spotCentrex = spotCentrex;
}



public double getSpotCentrey() {
	return spotCentrey;
}



public void setSpotCentrey(double spotCentrey) {
	this.spotCentrey = spotCentrey;
}



public boolean isProfileInLogx()
{
	return profileInLogx;
}

public void setProfileInLogx(boolean profileInLogx)
{
	this.profileInLogx = profileInLogx;
}

public boolean isSetOutPixelsToZero() {
	return setOutPixelsToZero;
}

public void setSetOutPixelsToZero(boolean setOutPixelsToZero) {
	this.setOutPixelsToZero = setOutPixelsToZero;
}




}




