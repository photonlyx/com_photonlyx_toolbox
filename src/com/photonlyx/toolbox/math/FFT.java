
// Author Laurent Brunel
package com.photonlyx.toolbox.math;



//{************ TRANSFORMEE DE FOURIER RAPIDE *******************}

public class FFT
{

/**replace the wave x by its discrete Fourier transform. the value of index 0 is not taken  into account. the  number of value must be a power of 2*/
public static void  fft(Complex[] x,boolean directe)
{
double pi=Math.acos(-1);
Complex    w,save,z;
double zm,arg;
int  m,num,l,ldft,ndft,np,nq;
int  i,j,k;
int n=x.length-1;
Complex[] y=new Complex[x.length];
w= new Complex();
save= new Complex();
z= new Complex();

m=(int) (Math.log(n)/Math.log(2));

  y[1]=x[1];
  for (i=2;i<=n;i++)  
    {
    num=i-1;
    j=0;
    l=n;
    for (k=1;k<=m;k++) 
      {
      l=l / 2;
      if (num>=l) 
      			 {
                       j=j+(int)(Math.exp((k-1)*Math.log(2)));
                       num=num-l;
                       }
    }
    y[i]=x[j+1];
  }

  ldft=1;
  ndft=n;
  for (k=1 ;k<= m ;k++) 
    {
    ldft=2*ldft;
    ndft=(ndft / 2);
    for (i=1 ;i<= ndft;i++) 
      {
      for (j=1;j<= (ldft / 2);j++) 
        {
        if (directe==true)  arg=-(2.*pi)*(j-1)/ldft
;
                         else arg=(2.*pi)*(j-1)/ldft;
        
        w.setre(Math.cos(arg));
        w.setim(Math.sin(arg));

        np=j+ldft*(i-1);
        nq=np+(ldft / 2);

        z=Complex.mul(w,y[nq]);  //   {z=w*y[nq]}
        save=Complex.add(y[np],z); //{ save=y[np]+w*y[nq]; }

        y[nq]=Complex.sub(y[np],z);    //{  y[nq]=y[np]-w*y[nq];}
        y[np]=save;
        }
      }
    }
  if(directe==true)    for( i=1 ;i<= n;i++) x[i]=y[i];
              else   for( i=1 ;i<= n;i++)   x[i]=y[i].mul(1/n);
                       
  }
  
  
  
  
  
  
  }
