package com.photonlyx.toolbox.math.histo;


import com.photonlyx.toolbox.math.signal.Signal1D1DLogx;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1DAbstract;



/**
histogram that receive 1D values
the sampling is logarithmic
share the x axis in #dim cells from xmin to xmax
the cell size in log space is: (log10(xmax)-log10(xmin))/(dim)
the y(i) value contains all occurrence from x(i) to x(i+1)
The first cell is from x(0) to x(1)
The last cell is from x(dim-1) to xmax
*/
public  class Histogram1D1DLogx extends  Signal1D1DLogx implements Histogram1D1DInterface
{
//params
private int nbHits=0;
//private double lxmax;//log10 of xmax
//private double lxmin;//log10 of xmin
//private double sampling;//sampling: size of a cell after log10 transform

public Histogram1D1DLogx()
{
this.init(100,0,1);
//consider the y values as cells values
//this.setxArePoints(false);
//lxmax=Math.log(xmax());
//lxmin=Math.log(xmin());
//sampling=(lxmax-lxmin)/(getDim());
nbHits=0;
this.setPointsSignal(false);//an histo is made of cells
}

public Histogram1D1DLogx(int dim,double xmin,double xmax)
{
this.init(dim,xmin,xmax);
//consider the y values as cells values
//this.setxArePoints(false);
//lxmax=Math.log(xmax());
//lxmin=Math.log(xmin());
//sampling=(lxmax-lxmin)/(getDim());
nbHits=0;
this.setPointsSignal(false);//an histo is made of cells
}


public void init(int dim,double xmin,double xmax)
{
super.init( dim, xmin, xmax);
nbHits=0;
}


/**
return the number of occurences acquired
*/
public int getNbOccurences()
{
return nbHits;
}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurence(double occ,double dp)
{
if ((occ<xmin())||(occ>=xmax())) return;
//calculate the cell where to put the occurrence
double lx=Math.log(occ);
int cell=(int) Math.floor((lx-lxmin)/sampling);
double x1=Math.exp(lxmin+sampling*cell);
double x2=Math.exp(lxmin+sampling*(cell+1));
//System.out.println(occ+" "+dp+" cell:"+cell);
setValue(cell,value(cell)+dp/(x2-x1));
nbHits++;
}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurenceOLD(double occ,double dp)
{
int cell=this.getSampleIndexFloor(occ);
if ((cell<0)||(cell>=getDim())) return;
setValue(cell,value(cell)+dp);
nbHits++;
}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurence(double[] occ,double dp) 
{
addOccurence(occ[0],dp);
}

public void _annulate()
{
this.init(this.getDim(),this.xmin(),this.xmax());
nbHits=0;
}

@Override
public SignalDiscret1D1DAbstract getSignalDiscret1D1DAbstract()
{
return this;
}

}





