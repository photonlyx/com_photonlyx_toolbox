package com.photonlyx.toolbox.math.histo;


/**
interface for histograms able to store arrays of double
*/
public interface Histogram
{

/**
return the number of occurences acquired
*/
public int getNbOccurences();

/**
Add an occurence to the histogram. 
Of course, the eventual correction (like log) of the object axis is taken into account
*/
public void addOccurence(double[] occ,double dp);



}





