package com.photonlyx.toolbox.math.histo;

import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1DAbstract;



/**
histogram that receive 1D values.
share x axis in dim cells from xmin to xmax
the y(i) value contains all occurence from x(i) to x(i+1)
The first cell is from x(0) to x(1)
The last cell is from x(dim-1) to xmax
*/
public  class Histogram1D1D extends SignalDiscret1D1D implements Histogram1D1DInterface
{
private int nbOccurences;//number of occurrences
private double sampling;

public Histogram1D1D()
{
super();
}

public Histogram1D1D(int dim,double xmin,double xmax)
{
init(dim,xmin,xmax);
sampling=(xmax-xmin)/dim;//cell size, not the same sampling as SignalDiscret1D1D !
nbOccurences=0;
this.setPointsSignal(false);//an histo is made of cells
}

public Histogram1D1D(int dim,double xmin,double xmax,Signal1D1D signal)
{
init(dim,xmin,xmax);
sampling=(xmax-xmin)/dim;//cell size, not the same sampling as SignalDiscret1D1D !
nbOccurences=0;
this.setPointsSignal(false);//an histo is made of cells
this.processSignal1D1D(signal);
}

public Histogram1D1D(int dim,double xmin,double xmax,Signal2D1D im)
{
init(dim,xmin,xmax);
sampling=(xmax-xmin)/dim;//cell size, not the same sampling as SignalDiscret1D1D !
nbOccurences=0;
this.setPointsSignal(false);//an histo is made of cells
this.processSignal2D1D(im);
}

public void init(int dim,double xmin,double xmax)
{
super.init(dim, xmin, xmax);
sampling=(xmax-xmin)/dim;//cell size, not the same sampling as SignalDiscret1D1D !
nbOccurences=0;
this.setPointsSignal(false);//an histo is made of cells
}

public void processSignal2D1D(Signal2D1D image)
{
	for (int i=0;i<image.dimx();i++)
		for (int j=0;j<image.dimy();j++)
			{
			double v=image.getValue(i,j);
			addOccurence(v,1);
			}
	//System.out.println("load "+(image.dimx()*image.dimy())+" pixels");
}


public void processSignal1D1D(Signal1D1D signal)
{
for (int i=0;i<signal.getDim();i++) addOccurence(signal.value(i),1);
}


/**
return the number of occurences acquired
*/
public int getNbOccurences()
{
return nbOccurences;
}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurence(double occ,double dp)
{
if ((occ<xmin())||(occ>=xmax())) return;
//calculate the cell where to put the occurence
int cell=(int)Math.floor((occ-xmin())/sampling);
setValue(cell,value(cell)+dp);
//if (cell>=(getDim()-1)) System.out.println("add occurence "+occ+" cell="+cell+" x1="+x(cell)+" x2="+x(cell+1));
nbOccurences++;
}

/**
add an occurrence at cell occ
*/
public void addOccurence(double occ)
{
addOccurence(occ,1.);
}


/**add the probability dp at the occurrence cell occ*/
public void addOccurence(double[] occ,double dp) 
{
addOccurence(occ[0],dp);
}

/**
annulate the histo*/
public void _annulate()
{
init(this.getDim(),this.xmin(),this.xmax());
nbOccurences=0;
}

@Override
public SignalDiscret1D1DAbstract getSignalDiscret1D1DAbstract()
{
return this;
}


}





