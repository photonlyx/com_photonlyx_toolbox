package com.photonlyx.toolbox.math.histo;

import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal3D1D;



/**
histogram that receive 2D values
*/
public  class Histogram3D1D extends Signal3D1D implements Histogram
{
//params
private int nbHits;
private double samplingx,samplingy,samplingz;


public  Histogram3D1D()
{
init( 0, 1, 0, 1,0,1, 10, 10,10);
}

public  Histogram3D1D(double xmin,double xmax,double ymin,double ymax,double zmin,double zmax,int dimx,int dimy,int dimz)
{
init( xmin, xmax, ymin, ymax,zmin,zmax, dimx, dimy,dimz);
}

public void init(double xmin,double xmax,double ymin,double ymax,double zmin,double zmax,int dimx,int dimy,int dimz)
{
super.init(xmin, xmax, ymin, ymax,zmin,zmax, dimx, dimy,dimz);
//here the data correspond to a cell, not a point:
samplingx=((xmax()-xmin())/(dimx()-1));
samplingy=((ymax()-ymin())/(dimy()-1));
samplingz=((zmax()-zmin())/(dimz()-1));
nbHits=0;
}

/**
return the number of occurences acquired
*/
public int getNbOccurences()
{
return nbHits;
}

public int getNbHits() {
	return nbHits;
}

public void setNbHits(int nbHits) {
	this.nbHits = nbHits;
}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurence(double[] occ,double dp)
{
//calculate the cell where to put the occurence
int cellx=(int)((occ[0]-xmin())/samplingx);
int celly=(int)((occ[1]-ymin())/samplingy);
int cellz=(int)((occ[2]-zmin())/samplingz);

//System.out.println("add occurence "+occ[0]+" "+occ[1]+" "+occ[2]+" cell:"+cellx+" "+celly+" "+cellz);

if ((cellx<0)||(cellx>=dimx())) return;
if ((celly<0)||(celly>=dimy())) return;
if ((cellz<0)||(cellz>=dimz())) return;

//System.out.println("OK");

setVal(cellx,celly,cellz,val(cellx,celly,cellz)+dp);
nbHits++;

}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurence(double x,double y,double z,double dp)
{
//calculate the cell where to put the occurence
int cellx=(int)((x-xmin())/samplingx);
if ((cellx<0)||(cellx>=dimx())) return;

int celly=(int)((y-ymin())/samplingy);
if ((celly<0)||(celly>=dimy())) return;

int cellz=(int)((z-zmin())/samplingz);
if ((cellz<0)||(celly>=dimy())) return;
setVal(cellx,celly,cellz,val(cellx,celly,cellz)+dp);
nbHits++;
}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurence(Vecteur p,double dp)
{
//calculate the cell where to put the occurence
int cellx=(int)((p.x()-xmin())/samplingx);
if ((cellx<0)||(cellx>=dimx())) return;

int celly=(int)((p.y()-ymin())/samplingy);
if ((celly<0)||(celly>=dimy())) return;

int cellz=(int)((p.z()-zmin())/samplingz);
if ((cellz<0)||(celly>=dimy())) return;

setVal(cellx,celly,cellz,val(cellx,celly,cellz)+dp);
nbHits++;

//System.out.println("add occurence "+occ);
}



public void _annulate()
{
init( xmin(), xmax(), ymin(), ymax(), zmin(),zmax(),dimx(), dimy(),dimz());
}

public double getSamplingx()
{
return samplingx;
}

public double getSamplingy()
{
return samplingy;
}

public double getSamplingz()
{
return samplingz;
}




}





