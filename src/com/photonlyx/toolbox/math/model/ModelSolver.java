package com.photonlyx.toolbox.math.model;

import com.photonlyx.toolbox.math.matrix.FullMatrix;


/**
 * adjust the input parameters of a model to get the output parameters
 * as close as possible to a target set of output parameters
 * iteratively Linearize the system and make the corrections
 * @author Laurent
 *
 */
public class ModelSolver
{
private ModelInterface model;


public ModelSolver(ModelInterface mi)
{
	model=mi;
}
	

/**
 * solver:  find the input parameters that gets the output parameters closest to the targets output
 * @param target the output parameters set to approach
 * @return norm of fermeture
 */
public double solve(double[] target)
{
int nbIter=5;
FullMatrix F=null;
FullMatrix A=null;
int n=model.getOut().length;
int m=model.getIn().length;




//******************************************************************************
//do the least square adjustment	
//*******************************************************************************

/*
 parameters: pi, i from 0 to m-1,  m=6 :  x,y,z,ax,ay,az
 measures:   Mi, i from 0 to n-1,  n=6 :  x0 y0 x1 y1 x2 y2
 measures of model:   mi, i from 0 to n-1,  n=6 :  x0 y0 x1 y1 x2 y2
 */
for (int k=0;k<nbIter;k++)
	{
	//fill the A matrix (matrix of the derivatives)
	
	
	/*
	If we note :
	the A matrix is a n*m matrix
	(                                                      )
	(                                                      )
	(                                                      )
	(                                                      )
	(          dmi                                        )
	(   ...   _____    ...                                 )
	(          dpj                                        )
	(                                                      )
	(                                                      )
	(                                                      )
	(                                                      )
	(                                                      )
	(                                                      )
	
	
	*/
	
	A=new FullMatrix(n,m);
	for (int outIndex=0;outIndex<n;outIndex++)//list the measures
		{
		for (int inIndex=0;inIndex<m;inIndex++)//list the camera parameters
			{
			double pd=partialDerivative(inIndex,outIndex);
			A.put(outIndex,inIndex,pd);
			}
		}
		
	
	//System.out.println("A");System.out.println(A.toString());
//	System.out.println();
	
	

	//fill the F matrix (fermeture). F is the vector (matrix n x 1) of the Xi-Pi
	/*
	
	
	   (                 )
	   (                 )
	   (                 )
	   (                 )	
	   (    ...          )
	F= (   (Xi)x-(Pi)x   )
	   (    ...          )
	   (                 )
	   (                 )
	   (                 )
	   (                 )
	   
	*/
	
	double[] outTheo=model.getOut();
	F=new FullMatrix(n,1);
	for (int outIndex=0;outIndex<n;outIndex++)//list output parameters
		{
		//System.out.println(outIndex+" theo:"+outTheo[outIndex]+" real:"+target[outIndex]);
		F.put(outIndex,0,outTheo[outIndex]-target[outIndex]);		
		}
	
	//System.out.println("F");	System.out.println(F.toString());	System.out.println();
	
	//calculate the normal matrix
	FullMatrix At=A.transpose();
	
/*	System.out.println("At");
	System.out.println(At.toString());
	System.out.println();*/
	
	FullMatrix N=At.multiply(A);
	
	//System.out.println("N");System.out.println(N.toString());System.out.println();
	
	/*System.out.println("N-1");
	System.out.println(MatricePleine.inverse_lu(N).toString());
	System.out.println();*/
	
	FullMatrix AtF=At.multiply(F);
	
//	System.out.println("AtF");
//	System.out.println(AtF.toString());
//	System.out.println();
	
	//now solve the equation:  AtA dX = AtF
	int[] indx=new int[2*n];
	FullMatrix.decompose_lu(N,indx);//N is destroyed!
	FullMatrix.systeme_lu(N,indx,AtF);//now the solution is in AtF!
	
//	System.out.println("solve");
//	System.out.println(AtF.toString());
//	System.out.println();

	//apply the correction:
		
	double[] in=model.getIn();	
	for (int inIndex=0;inIndex<m;inIndex++)in[inIndex]-=AtF.el(inIndex,0);
		
	model.setIn(in);
	model.update();
	//System.out.println("iter "+k+" norm of F:"+F.norme());
	
		
	}
System.out.print(" norm of F:"+F.norme());	
return F.norme();
}


	
	/**
	 * dx/da1 dx/da2 dx/da3
	 * dy/da1 dy/da2 dy/da3
	 * dz/da1 dz/da2 dz/da3
	 * 
	 * @return
	 */
	public FullMatrix moveMatrix()
	{
	FullMatrix a=new FullMatrix(3,3);	
	for (int j=0;j<3;j++) 
		for (int i=0;i<3;i++)
			{
			a.affect(i, j, partialDerivative(j,i));
			}
	return a;
	}


	
	
	
	private double getParam(int index)
	{
	return model.getIn()[index];
	}
	
	
	private double getOut(int index)
	{
	return model.getOut()[index];
	}

	/**
	gives the partial derivative of the co-ordinate y of the point of abcisse x of the curve  with respect to the Y coord of the control point of index ctrlIndex
	@param inIndex index of the input parameter
	@param outIndex index of the output parameter
	@return the value of the derivative
	*/
	private double partialDerivative(int inIndex,int outIndex)
	{
	double dY,absdiff;
	double der1;
	double der2=0;
	double y0;//initial param
	double y1,y2;// param

	absdiff=1;
	dY=0.001;

	//get the initial parameter value
	double Y0=this.getParam(inIndex);

	//get the initial theoretical measurement:
	model.update();


	y0=this.getOut(outIndex);
	//System.out.println("d0="+d0);


	while ((absdiff>1e-5))
	{
	model.setParam(inIndex,Y0+dY);
	model.update();
	y1=this.getOut(outIndex);
	der1=(y1-y0)/dY;

	dY=dY/2;

	model.setParam(inIndex,Y0+dY);
	model.update();
	y2=this.getOut(outIndex);
	der2=(y2-y0)/dY;
	absdiff=Math.abs(der2-der1);
	} 

	//replace 
	model.setParam(inIndex,Y0);
	model.update();
	return der2;
	}

	
	
	
}
