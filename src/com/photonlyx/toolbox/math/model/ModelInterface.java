package com.photonlyx.toolbox.math.model;



/**
 * interface for a model system with input and output parameters
 * @author Laurent
 *
 */
public interface ModelInterface
{

	
	
	
/**
 * get the input parameters
 * @return
 */
public double[] getIn();

/**
 * set the input parameters
 * @param i
 */
public void setIn(double[] i);


public void setParam(int index,double a);



/**
 * get the output parameters
 * @return
 */
public double[] getOut();

public double getOut(int i);


///**
// * set the output parameters
// * @param o
// */
//public void setOut(double [] o);

/**
 * recalc the model (use after changing one input parameter
 */
public void update();
	
}
