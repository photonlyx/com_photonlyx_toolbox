// Author Laurent Brunel

package com.photonlyx.toolbox.math.analyse;

import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DSource;

/**
fit the line y=ax
*/
public class FitSlope 
{
//links
private Signal1D1DSource sdaq;
//params
private double a;
private double xmin,xmax;

//internal use
private double sxy = 0;
private double sx2 = 0;


/**construct with the daq to fit*/
public FitSlope(Signal1D1D daq)
{
sdaq=daq;
}


/**
fit a line to the signal
*/
public void work()
{
Signal1D1D daq=sdaq.getSignal1D1D();
double[] x=daq.getXarray();
double[] y=daq.getYarray();
int n=daq.getDim();
sxy = 0;
sx2 = 0;
int i;
for(i=0;i<n;i++)
	{
    sxy += x[i]*y[i];
    sx2 += x[i]*x[i];
	}
a = sxy/sx2;

xmin=daq.xmin();
xmax=daq.xmax();
}

public double slope()
{
return a;
}


}
