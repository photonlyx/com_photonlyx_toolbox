// Author Laurent Brunel

package com.photonlyx.toolbox.math.analyse;

import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DSource;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;


/**
function that gives the linear  interpolation of the Signal1D1D from the Signal1D1DSource son
*/
public class LinearInterpolation extends Function1D1D
{
//links
private Signal1D1DSource sdaq;


public LinearInterpolation()
{
	
}

/**
create a function that linearly interpol the signal
@param signal: the 1D1D signal (assumed to be sorted) to interpol
*/
public LinearInterpolation(Signal1D1DSource signal)
{
sdaq=signal;
}




/**
gives a linear  interpolation at any x value 
assume that the signal is x sorted
@param x the abscissa value
@return the linear interpolation
*/
public double f(double x)
{
Signal1D1D signal=sdaq.getSignal1D1D();
if (signal==null) return 0;
double val=0;
// System.out.println(" x="+x+" xmin="+signal.x(0)+" xmax="+signal.x(signal.getDim()-1));
if (x<=signal.x(0)) val=signal.y(0);
else if (x>=signal.x(signal.getDim()-1)) val=signal.y(signal.getDim()-1);
else for (int i=0;i<signal.getDim()-1;i++)
	{
	if ((signal.x(i)<=x)&&(x<=signal.x(i+1))) 
		{
// 		index=i;
		double x1=signal.x(i);
		double x2=signal.x(i+1);
		double y1=signal.value(i);
		double y2=signal.value(i+1);
		val=y1+(y2-y1)*(x-x1)/(x2-x1);
// 		System.out.println("x1="+x1+" x2="+x2+" y1="+y1+" y2="+y2+" x="+x+" y="+val);
		break;
		}
	}
return val;
}

public Signal1D1D getSignal() 
{
	return sdaq.getSignal1D1D();
}





}
