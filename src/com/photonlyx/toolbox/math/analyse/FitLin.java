
// Author Laurent Brunel

package com.photonlyx.toolbox.math.analyse;

import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.math.function.Function1D1DSource;
import com.photonlyx.toolbox.math.function.usual.Line;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DSource;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;


/**
For an experiment fit a linear function to acquisition data . </p>
 1 son of class Signal1D1D </p>
Laurent Brunel 19/11/2002, 3/7/2003 </p>
*/
public class FitLin  implements Function1D1DSource
{
//links
private Signal1D1DSource sdaq;
//out
private double a,b,r,slope,delta,offset,xmin,xmax;

//internal use
private Line bestLine;
private double sxy = 0;
private double sx = 0;
private double sy = 0;
private double sx2 = 0;
private double sy2 = 0;
private static String defaultdef;



/**construct with the daq to fit*/
public FitLin(Signal1D1D daq)
{
//links
sdaq=daq;
//params

bestLine=new Line();

}



/**
fit a line to the signal
*/
public void work()
{
Signal1D1D daq=sdaq.getSignal1D1D();
double[] x=daq.getXarray();
double[] y=daq.getYarray();
int n=daq.getDim();
sxy = 0;
sx = 0;
sy = 0;
sx2 = 0;
sy2 = 0;
int i;
for(i=0;i<n;i++)
{
    sxy += x[i]*y[i];
    sx  += x[i];
    sy  += y[i];
    sx2 += x[i]*x[i];
    sy2 += y[i]*y[i];
}
a = (sxy - sx*sy/n) / (sx2 - sx*sx/n);
b = (sy - a * sx)/n;
r = ((sxy -sx*sy/n)*(sxy -sx*sy/n)/(sx2 - sx*sx/n))/(sy2 - sy*sy/n);

bestLine.setParams(a,b);

slope=bestLine.slope();
offset=bestLine.offset();
delta=delta();
xmin=daq.xmin();
xmax=daq.xmax();
}

/**
fit a line through a serie of points.
@param x array of the x values
@param y array of the y values
@param abr array of 3 doubles with coef a (slope) b (ordinate at x=0) and the regression coef r
*/
public static void fitLine(double[] x,double[] y,double[] abr)
{
int n=x.length;
double sxy = 0;
double sx = 0;
double sy = 0;
double sx2 = 0;
double sy2 = 0;
int i;
for(i=0;i<n;i++)
	{
    	sxy += x[i]*y[i];
    	sx  += x[i];
    	sy  += y[i];
    	sx2 += x[i]*x[i];
    	sy2 += y[i]*y[i];
	}
abr[0] = (sxy - sx*sy/n) / (sx2 - sx*sx/n);
abr[1] = (sy - abr[0] * sx)/n;
abr[2] = ((sxy -sx*sy/n)*(sxy -sx*sy/n)/(sx2 - sx*sx/n))/(sy2 - sy*sy/n);
}


/**
fit a line through a serie of points.
 * @param signal Signal to fit
@param abr array of 3 doubles with coef a (slope) b (ordinate at x=0) and the regression coef r
*/
public static void fitLine(Signal1D1D signal, double[] abr)
{
fitLine( signal, 0, signal.getDim()-1,abr);
}


public static Line getFittedLine(Signal1D1D signal)
{
FitLin fl=new FitLin(signal);
fl.work();
return fl.getLine();
}

/**
fit a line through a serie of points.
 * @param signal Signal to fit
 * @param indexStart index of the first point in the signal to be fitted
 * @param indexEnd index of the last point in the signal to be fitted
@param abr array of 3 doubles with coef a (slope) b (ordinate at x=0) and the regression coef r
*/
public static void fitLine(Signal1D1D signal, int indexStart, int indexEnd,double[] abr)
{
double[] x = signal.getXarray();
double[] y = signal.getYarray();
int n=indexEnd-indexStart + 1;
double sxy = 0;
double sx = 0;
double sy = 0;
double sx2 = 0;
double sy2 = 0;
int i;
for(i=indexStart;i<=indexEnd;i++)
	{
    	sxy += x[i]*y[i];
    	sx  += x[i];
    	sy  += y[i];
    	sx2 += x[i]*x[i];
    	sy2 += y[i]*y[i];
	}
abr[0] = (sxy - sx*sy/n) / (sx2 - sx*sx/n);
abr[1] = (sy - abr[0] * sx)/n;
abr[2] = ((sxy -sx*sy/n)*(sxy -sx*sy/n)/(sx2 - sx*sx/n))/(sy2 - sy*sy/n);
}

/** return the fitted fontion.usual.Line*/
public Function1D1D getFunction1D1D()
{
return bestLine;
}

/** return the fitted fontion.usual.Line*/
public Line getLine()
{
return bestLine;
}

/** return the y difference between the start and end of the best line */
public double delta()
{
Signal1D1D daq=sdaq.getSignal1D1D();
double a=bestLine.slope();
double b=bestLine.offset();
return (a*daq.xmax()+b)-(a*daq.xmin()+b);
}

/**
give the rms of the fit
@return the rms
*/
public double dispersionFit()
{
Signal1D1D daq=sdaq.getSignal1D1D();
//calculate the dispersion of the fitting
Signal1D1DXY difference=new Signal1D1DXY(daq.getDim());
for (int i=0;i<difference.getDim();i++)
	difference.setValue(i,daq.x(i),bestLine.f(daq.x(i))-daq.value(i));
return difference.rms();
}

/**
give the rms of the fit from the formula rms=sqrt(sy�*(1-r�));
may be wrong.....
@return the rms
*/
public double rms()
{
return Math.sqrt(sy2*(1-r*r));
}

/**
give the regression (the fit quality)
@return the regression
*/
public double r()
{
return r;
}


}
