package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;

public class ConeMesh  extends TriangleMeshFrame implements Object3DFrame,TriangleMeshSource
{
	private double d=1;//diameter of the cone
	private double h=1;//diameter of the cone
	private int dim=20;//nb of sampling points
	private Frame frame=new Frame();//local frame of the cylinder
	private Vector<Segment3D> localCoordinates=new Vector<Segment3D>();
	private Vector<Segment3D> globalCoordinates=new Vector<Segment3D>();
	private TriangleMesh tris=new TriangleMesh();

	public ConeMesh()
	{
		init();
	}

	/**
	 * 	
	 * @param o first face centre
	 * @param n axis
	 * @param d
	 * @param h
	 * @param dim
	 */
		public ConeMesh(double d,double h,int dim)
		{
			this.d=d;
			this.h=h;
			this.dim=dim;
			init();
		}

/**
 * 	
 * @param o first face centre
 * @param n axis
 * @param d
 * @param h
 * @param dim
 */
	public ConeMesh(Vecteur o,double d,double h,int dim)
	{
		this.frame.setCentre(o);
		this.d=d;
		this.h=h;
		this.dim=dim;
		init();
	}

	private void init()
	{
		double dAlpha=Math.PI*2/dim;
		localCoordinates.removeAllElements();
		Vecteur o=frame.getCentre();
		//side segments:
		for (int i=0;i<dim;i++) 
		{
			Vecteur p1=new Vecteur(d/2*Math.cos(i*dAlpha),d/2*Math.sin(i*dAlpha),0)._add(o);
			Vecteur p2=new Vecteur(0,0,h)._add(o);
			localCoordinates.add(new Segment3D(p1,p2));
		}
		
		//allocate global coords of vertical segment(different memory)
		for (int i=0;i<dim;i++) 
		{
			Segment3D seg=localCoordinates.get(i);
			globalCoordinates.add(new Segment3D(seg.p1().copy(),seg.p2().copy()));
		}

		//allocate triangles (global same memory as global segments
		tris.removeAllElements();
		for (int i=0;i<dim-1;i++) 
		{
			Segment3D seg1=globalCoordinates.get(i);
			Segment3D seg2=globalCoordinates.get(i+1);
			Triangle3D t1=new Triangle3D(seg1.p1(),seg1.p2(),seg2.p1());
			tris.add(t1);	
			Triangle3D t2=new Triangle3D(seg1.p2(),seg2.p1(),seg2.p2());
			tris.add(t2);	
		}
		{
			Segment3D seg1=globalCoordinates.get(dim-1);
			Segment3D seg2=globalCoordinates.get(0);
			Triangle3D t1=new Triangle3D(seg1.p1(),seg1.p2(),seg2.p1());
			tris.add(t1);	
			Triangle3D t2=new Triangle3D(seg1.p2(),seg2.p1(),seg2.p2());
			tris.add(t2);	
		}
	}
	
	@Override
	public void updateGlobal()
	{
		for (int i=0;i<dim;i++) 
		{
			Segment3D seg=localCoordinates.get(i);
			Segment3D segg=globalCoordinates.get(i);
			frame.global(segg.p1(), seg.p1());
			frame.global(segg.p2(), seg.p2());
		}
	}
	

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (Triangle3D tri:tris) tri.checkOccupyingCube(cornerMin, cornerMax);
	}
	
	@Override
	public void draw(Graphics g, Projector proj) 
	{
		for (Triangle3D tri:tris) tri.draw(g, proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Triangle3D tri:tris) 
			if (tri.isOn(p, proj)) return true;
		return false;
	}

	@Override
	public Frame getFrame()
	{
		return frame;
	}

	@Override
	public TriangleMesh getTriangles() 
	{
		return tris;
	}

	public static void main(String[] args)
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);

		Object3DColorSet set=new Object3DColorSet();

		double angle=30;//total angle deg
		
		Vecteur dir=new Vecteur(0,0.3,-1);
		Vecteur pos=new Vecteur(1,0,2);
		
		//the cone:
		double hcone=1;
		double dcone=2*hcone*Math.tan(angle/2*Math.PI/180);
		ConeMesh cone=new ConeMesh(dcone,hcone,20);//represents the probe
		cone.getFrame().buildSuchThatZaxisIs(dir.scmul(-1));
		cone.getFrame()._translate(pos);
		cone.getFrame()._translate(cone.getFrame().z().scmul(-hcone));
		cone.updateGlobal();	
		set.add(new Object3DColorInstance(cone.getFrame(),Color.BLUE));
		set.add(new Object3DColorInstance(cone,Color.RED));
		
		//the cylinder:
		double diameter=0.5;
		double hcyl=diameter/2;
		CylinderMesh cyl=new CylinderMesh(diameter,hcyl,10);//represents the probe
		set.add(new Object3DColorInstance(cyl,Color.RED));
		cyl.getFrame().buildSuchThatZaxisIs(dir.scmul(1));
		cyl.getFrame()._translate(pos);
		cyl.getFrame()._translate(cyl.getFrame().z().scmul(-hcyl));
		cyl.updateGlobal();
		
		cyl.getFrame().setAxisLength(1);
		//set.add(new Object3DColorInstance(cyl.getFrame(),Color.BLUE));
		
		graph3DPanel.add(set);
		
		graph3DPanel.updateOccupyingCube();
		graph3DPanel.initialiseProjector();
		graph3DPanel.update();

		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());

	}


}
