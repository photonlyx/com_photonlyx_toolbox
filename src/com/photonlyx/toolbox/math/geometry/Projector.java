package com.photonlyx.toolbox.math.geometry;

public interface Projector 
{

//public void initialise(Vecteur cornermin,Vecteur cornermax,int _screenW,int _screenH);
	
/** project a 3D point P on the plane of the Projecteur*/
public double[] projete(Vecteur P);	

/**calculates the screen co-ordinates*/
public double[] calcCoorEcranDouble(Vecteur P);

//**calculates the screen co-ordinates*/
public double[] calcCoorEcran(Vecteur P);

public void calcCoorScreen(double[] cooScreen,Vecteur P);

public void calcCoorScreen(Vecteur2D cooScreen,Vecteur P);


/** calc the z coordinate of the point P in the frame of the Projecteur*/
public double zCoord(Vecteur P);

/**turn the projector. angles in rad*/
public void turn(double ax,double ay,double az);

/**translate the viewed object in the screen*/
public void translater(double dx,double dy,double dz);

//public void zoom();
//
//public void unzoom() ;

}
