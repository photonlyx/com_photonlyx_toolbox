package com.photonlyx.toolbox.math.geometry;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import nanoxml.XMLElement;


public class PolyLine3D implements Iterable<Vecteur>,Object3D
{
	private Vector<Vecteur> list=new Vector<Vecteur>();
	private int dotSize=3;
	private boolean seeSegments=true;
	private boolean closed=false; // if true the polyline is closed : the last point is connected to the first

	public PolyLine3D()
	{
	}

	public int nbPoints()
	{
		return list.size();
	}

	public void addPoint(Vecteur _v)
	{
		list.add(_v);
	}

	public void removeAllPoints()
	{
		list.removeAllElements();
	}

	public Vector<Vecteur> getList()
	{
		return list;
	}
	public Vecteur get(int i)
	{
		return list.elementAt(i);
	}

	public Iterator<Vecteur> iterator() 
	{
		return list.iterator();
	}

	public Vecteur first()
	{
		if (list.size()>0) return list.firstElement();else return null;
	}

	public Vecteur last()
	{
		if (list.size()>0) return list.lastElement();else return null;
	}

	public Vecteur getPoint(int index)
	{
		return list.elementAt(index);
	}



	public int getDotSize() {
		return dotSize;
	}

	public void setDotSize(int dotSize) {
		this.dotSize = dotSize;
	}

	public boolean isSeeSegments() {
		return seeSegments;
	}

	public void setSeeSegments(boolean seeSegments) {
		this.seeSegments = seeSegments;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public XMLElement toXML() 
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		for (Vecteur p : list) el.addChild(p.toXML());
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		list.removeAllElements();
		if (xml==null) return;
		//System.out.println(xml);
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			Vecteur p=new Vecteur();
			p.updateFromXML(enumSons.nextElement());
			addPoint(p);
		}
	}

	/**
	 * sort the x values
	 */
	public void sortX()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.x());
		Collections.sort(vv);
	}


	/**
	 * sort they values
	 */
	public void sortY()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.y());
		Collections.sort(vv);
	}


	/**
	 * sort the x values
	 */
	public void sortZ()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.z());
		Collections.sort(vv);
	}


	/**
	 * sort the x+y values
	 */
	public void sortXplusY()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.x()+v.y());
		Collections.sort(vv);
	}

	/**
	 * sort the x+y values
	 */
	public void sortYplusZ()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.y()+v.z());
		Collections.sort(vv);
	}


	/**
	 * sort the x+z values
	 */
	public void sortXplusZ()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.x()+v.z());
		Collections.sort(vv);
	}



	public double ymin()
	{
		double d=1e30;//Double.MAX_VALUE;
		for (Vecteur v:list) if (v.y()<d) d=v.y();
		return d;	
	}

	public double ymax()
	{
		double d=-1e30;//Double.MIN_VALUE;
		for (Vecteur v:list) if (v.y()>d) d=v.y();
		return d;	
	}

	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		for (Vecteur p:this) sb.append(p.toString()+"\n");
		return sb.toString();
	}


	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
	{
		for (Vecteur p:this)
			for (int k=0;k<3;k++) 
			{
				double r=p.coord(k);
				if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
				if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
			}
	}


	public void draw(Graphics g, Projector proj)
	{
		int n=this.getList().size();
		double[][] cooScreen=new double[2][];
		for (int i=0;i<n-1;i++)
		{
			Vecteur p1=this.getList().elementAt(i);
			Vecteur p2=this.getList().elementAt(i+1);
			cooScreen[0]=proj.calcCoorEcran(p1);
			cooScreen[1]=proj.calcCoorEcran(p2);
			if(seeSegments) g.drawLine((int)cooScreen[0][0], (int)cooScreen[0][1], (int)cooScreen[1][0], (int)cooScreen[1][1]);
			if (dotSize>0) g.fillRect((int)cooScreen[0][0]-dotSize/2, (int)cooScreen[0][1]-1,dotSize,dotSize);
		}
		//last point
		if (dotSize>0) if(this.getList().size()>0)
		{
			Vecteur p1=this.getList().elementAt(n-1);
			cooScreen[0]=proj.calcCoorEcran(p1);
			g.fillRect((int)cooScreen[0][0]-dotSize/2, (int)cooScreen[0][1]-1,dotSize,dotSize);
		}
		if (closed)
		{
			Vecteur p1=this.getList().elementAt(n-1);
			Vecteur p2=this.getList().elementAt(0);
			cooScreen[0]=proj.calcCoorEcran(p1);
			cooScreen[1]=proj.calcCoorEcran(p2);
			if(seeSegments) g.drawLine((int)cooScreen[0][0], (int)cooScreen[0][1], (int)cooScreen[1][0], (int)cooScreen[1][1]);
			if (dotSize>0) g.fillRect((int)cooScreen[0][0]-dotSize/2, (int)cooScreen[0][1]-1,dotSize,dotSize);

		}
	}


	public double getDistanceToScreen(Projector proj)
	{
		return 	proj.zCoord(this.first());
	}

	@Override
	public boolean isOn(Point p, Projector proj)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public PolyLine3D copy() 
	{
		PolyLine3D p=new PolyLine3D();
		for (Vecteur v:this) p.addPoint(v.copy());
		return p;
	}

	public void add(PolyLine3D po) 
	{
		for (Vecteur p:po) this.addPoint(p);
	}



	/**
	 * transform unattached segments in polylines:
	 * 
	 * @param segs
	 * @param tolerance
	 * @return
	 */
	public static Vector<PolyLine3D> transformConnectedSegmentsInPolyLines(Vector<Segment3D> segs,double tolerance)
	{
		//will transform the segments in polylines:
		Vector<PolyLine3D> polylines=new Vector<PolyLine3D>();
		//int indexPolyLine=0;
		//add a first polyline:
		PolyLine3D current_polyline;
		current_polyline=new PolyLine3D();
		polylines.add(current_polyline);

		Vector<Segment3D> segments=(Vector<Segment3D>) segs.clone();
		//for (Segment3D seg_:segs) System.out.println(seg_);

		//get a first point of the first segment:
		Segment3D seg=segments.elementAt(0);
		Vecteur point=seg.p1();
		//remove the current segment from the list of segments:
		segments.remove(seg);
		for (;;)
		{
			//System.out.println("transformConnectedSegmentsInPolyLines:"+seg);
			//add the current point to the current polyline:
			current_polyline.addPoint(point);
			//if there are no more segments in the list, exit the loop
			if (segments.size()==0) 
			{
				//no more segments to add to the polyline
				//close the polyline:
				PolyLine3D pol1=polylines.elementAt(polylines.size()-1);
				//		System.out.println("first point "+pol1.first());
				//		System.out.println("last point "+pol1.last());
				//pol1.addPoint(pol1.getPoint(0));
				pol1.setClosed(true);
				break;
			}
			//find the other connected segment:
			int[] indexConnectionPoint=new int[1];	
			//System.out.println("Find connected segment to point  "+point+" (tolerance="+tolerance+")");
			Vector<Segment3D> segments_connected=new Vector<Segment3D>();
			Vector<Integer> indices=new Vector<Integer>();
			findConnectedSegment(segments_connected,indices,segments,point,tolerance,indexConnectionPoint);
			if (segments_connected.size()>1)
			{
				System.err.println("PolyLine3D transformConnectedSegmentsInPolyLines: A point is connect to more than 1 segments");
				return polylines;
			}
			//System.out.println("found "+seg2);
			if (segments_connected.size()==0) //can't find a connected segment
			{
				//break;
				if (segments.size()>0)//there are still unconnected points, start a new polyline:
				{
					//create a new polyline:
					current_polyline=new PolyLine3D();
					polylines.add(current_polyline);
					//indexPolyLine++;
					//work with a new segment and point:
					seg=segments.elementAt(0);
					point=seg.p1();
					continue;
				}
				//		else //no more segments to connect
				//			{
				//			break;
				//			}
			}
			if (segments_connected.size()==1) //found ONE connected segment
			{
				
				//	System.out.println("transformConnectedSegmentsInPolyLines: connected segment: "+seg2);
				//	System.out.println("transformConnectedSegmentsInPolyLines: now work on point "+seg2.point(1-indexConnectionPoint[0]));
				//add the other point of the segment to the current polyline:
				//polylines.elementAt(indexPolyLine).addPoint(seg2.point(1-indexConnectionPoint[0]));
				//now work on the connected segment
				seg=segments_connected.get(0);
				//now work on the other point:
				point=seg.point(1-indices.get(0));
				//System.out.println("transformConnectedSegmentsInPolyLines: now work on point "+point);
				//remove the found segment from the list of segments:
				segments.remove(seg);
			}
		}	
		//System.out.println(polylines.size()+" polyLines:");
		return polylines;
	}

	
	
	
	

	/**
	 * find the segment having one of its points very close to the point
	 * @param segs
	 * @param point
	 * @return null if no connected segment found
	 */
	public static void findConnectedSegment(Vector<Segment3D> list,Vector<Integer> indices,
			Vector<Segment3D> segs,Vecteur point,double tolerance,int[] indexConnectionPoint)
	{
		//System.out.println("findConnectedSegment with the point: "+point.toString());
		for (Segment3D seg:segs) 
		{
			//System.out.println("findConnectedSegment check:"+seg.toString());
			if (Vecteur.distance(seg.p1(),point)<=tolerance) 
			{
				indices.add(0);
				//System.out.println("findConnectedSegment found, index:"+indexConnectionPoint[0]);
				list.add(seg);
			}
			if (Vecteur.distance(seg.p2(),point)<=tolerance) 
			{
				indices.add(1);
				//System.out.println("findConnectedSegment found, index:"+indexConnectionPoint[0]);
				list.add(seg);
			}
		}
		//System.out.println("findConnectedSegment not found");
	}

	public void _scale(Vecteur centre,double s) 
	{
		for (Vecteur v:list)
		{
			v.affect(v.sub(centre).scmul(s).addn(centre));
		}
	}

	public Vecteur barycentre()
	{
		Vecteur b=new Vecteur();
		for (Vecteur v:list) b._add(v);
		b.scmul(1/this.nbPoints());
		return b;
	}


		
	



}
