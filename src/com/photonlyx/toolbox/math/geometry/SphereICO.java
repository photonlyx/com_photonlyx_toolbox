package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.util.Enumeration;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import nanoxml.XMLElement;

public class SphereICO extends TriangleMeshFrame implements XMLstorable
{
	private double r=1;//sphere radius 
	private int recursionLevel=1;

	public SphereICO()
	{
	}

	/**
	 * 
	 * @param r radius
	 * @param recursionLevel precision of the mesh minimum 1 (coarse)  
	 */
	public SphereICO(double r, int recursionLevel)
	{
		this.r=r;
		this.recursionLevel=recursionLevel;
		init();
		this.updateGlobal();
	}


	@Override
	public void updateGlobal()
	{
		//calc the global coordinates:
		for (int i=0;i<getGlobal().nbTriangles();i++)
		{
			frame.global(getGlobal().getTriangle(i),getLocal().getTriangle(i));
		}
	}

	


	private Vecteur correctRadius(Vecteur p)
	{
		double length = p.norme();
		return new Vecteur(p.x()/length*r, p.y()/length*r, p.z()/length*r);
	}



	//  http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
	//calc the local frame triangles
	public void init()
	{
		getLocal().removeAllElements();
		// create 12 vertices of a icosahedron
		double t = (1.0 + Math.sqrt(5.0)) / 2.0;//nombre d'or

		Vector<Vecteur> v=new Vector<Vecteur>();
		v.add(correctRadius(new Vecteur(-1,  t,  0)));
		v.add(correctRadius(new Vecteur( 1,  t,  0)));
		v.add(correctRadius(new Vecteur(-1, -t,  0)));
		v.add(correctRadius(new Vecteur( 1, -t,  0)));

		v.add(correctRadius(new Vecteur( 0, -1,  t)));
		v.add(correctRadius(new Vecteur( 0,  1,  t)));
		v.add(correctRadius(new Vecteur( 0, -1, -t)));
		v.add(correctRadius(new Vecteur( 0,  1, -t)));

		v.add(correctRadius(new Vecteur( t,  0, -1)));
		v.add(correctRadius(new Vecteur( t,  0,  1)));
		v.add(correctRadius(new Vecteur(-t,  0, -1)));
		v.add(correctRadius(new Vecteur(-t,  0,  1)));


		// create 20 triangles of the icosahedron
		Vector<Triangle3D> tris=  new Vector<Triangle3D>();

		// 5 faces around point 0
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(11), v.elementAt(5)));
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(5), v.elementAt(1)));
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(1), v.elementAt(7)));
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(7), v.elementAt(10)));
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(10), v.elementAt(11)));

		// 5 adjacent faces 
		tris.add(new Triangle3D(v.elementAt(1), v.elementAt(5), v.elementAt(9)));
		tris.add(new Triangle3D(v.elementAt(5), v.elementAt(11), v.elementAt(4)));
		tris.add(new Triangle3D(v.elementAt(11), v.elementAt(10), v.elementAt(2)));
		tris.add(new Triangle3D(v.elementAt(10), v.elementAt(7), v.elementAt(6)));
		tris.add(new Triangle3D(v.elementAt(7), v.elementAt(1), v.elementAt(8)));

		// 5 faces around point 3
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(9), v.elementAt(4)));
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(4), v.elementAt(2)));
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(2), v.elementAt(6)));
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(6), v.elementAt(8)));
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(8), v.elementAt(9)));

		// 5 adjacent faces 
		tris.add(new Triangle3D(v.elementAt(4), v.elementAt(9), v.elementAt(5)));
		tris.add(new Triangle3D(v.elementAt(2), v.elementAt(4), v.elementAt(11)));
		tris.add(new Triangle3D(v.elementAt(6), v.elementAt(2), v.elementAt(10)));
		tris.add(new Triangle3D(v.elementAt(8), v.elementAt(6), v.elementAt(7)));
		tris.add(new Triangle3D(v.elementAt(9), v.elementAt(8), v.elementAt(1)));


		// refine triangles
		for (int i = 0; i < recursionLevel; i++)
		{
			//var faces2 = new List<TriangleIndices>();
			Vector<Triangle3D> tris2 = new Vector<Triangle3D>();
			for (Triangle3D tri:tris)
			{
				// replace triangle by 4 triangles
				Vecteur a = Vecteur.getMiddlePoint(tri.p1(), tri.p2());
				Vecteur b = Vecteur.getMiddlePoint(tri.p2(), tri.p3());
				Vecteur c = Vecteur.getMiddlePoint(tri.p3(), tri.p1());
				Vecteur a2=correctRadius(a);
				Vecteur b2=correctRadius(b);
				Vecteur c2=correctRadius(c);
				tris2.add(new Triangle3D(tri.p1().copy(), a2, c2));
				tris2.add(new Triangle3D(tri.p2().copy(), b2, a2));
				tris2.add(new Triangle3D(tri.p3().copy(), c2, b2));
				tris2.add(new Triangle3D(a2, b2, c2));
			}
			//remove the bigger triangles:
			tris.removeAllElements();
			// done, now add triangles to mesh
			for (Triangle3D tri:tris2)
			{
				tris.add(tri);
			}
		}
		
		
		for (Triangle3D tri:tris) this.getLocal().add(tri);
		//set global mesh:
		getGlobal().removeAllElements();
		for (Triangle3D tri:getLocal()) getGlobal().add(tri.copy());
	}



	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public int getRecursionLevel() {
		return recursionLevel;
	}

	public void setRecursionLevel(int recursionLevel) {
		this.recursionLevel = recursionLevel;
	}

	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().getSimpleName().toString().replace("class ", ""));
		el.setAttribute("recursionLevel",this.recursionLevel);
		el.setAttribute("r",r);
		el.addChild(this.getFrame().toXML());
		return el;
	}

	@Override
	public void updateFromXML(XMLElement xml)
	{
		recursionLevel=xml.getIntAttribute("recursionLevel",2);	
		r=xml.getDoubleAttribute("r",1);	
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		this.getFrame().updateFromXML(enumSons.nextElement());
		init();
	}



	public static void main(String[] args) 
	{
		SphereICO s=new SphereICO(1,3);
	
		if (args.length==1)
		{
			if (args[0].compareTo("-h")==0)
			{
				System.out.println("Usage: java -cp com_photonlyx_toolbox.jar com.photonlyx.toolbox.math.geometry.SphereICO "
						+ "-i params.xml"+ " -o sphere1.stl");
				System.out.println("Example of xml file:");
				System.out.println(XMLFileStorage.toStringWithIndentation(s.toXML()));
				System.exit(0);
			}
		}

		if (args.length>=1)
		{
			String inputFileName=null;//XML file with input parameters
			String outpuFileName=null;//STL file with output geometry
			for (int i=0;i<args.length;i++)
			{
				if (args[i].compareTo("-i")==0)
				{

					inputFileName=args[i+1];
				}
				if (args[i].compareTo("-o")==0)
				{
					//load parameters from XML file
					outpuFileName=args[i+1];
				}
			}
			if ((inputFileName!=null)&&(outpuFileName!=null))
			{
				//load parameters from XML file
				XMLElement xml=XMLFileStorage.readXMLfile(inputFileName);
				s.updateFromXML(xml);
				System.out.println(s.toXML());
				s.init();
				s.saveToSTLfile("./",outpuFileName);
			}

		}
		if (args.length==0)
		{
			System.out.println("Example of xml file:");
			System.out.println(XMLFileStorage.toStringWithIndentation(s.toXML()));

			//p.getFrame()._translate(new Vecteur(1,0,0));
			//p.updateGlobal();

			//		//add to a set:
			//		Object3DColorSet set=new Object3DColorSet();
			//		set.add(s.getTriangles());
			//		graph3DPanel.addColorSet(set);

			s.recursionLevel=1;
			s.r=1;
			s.init();
			//s.getFrame()._translate(new Vecteur(1,0,0));
			s.getFrame()._rotate(new Vecteur(Math.PI/4,0,0));
			s.updateGlobal();

			//			Method[] methods=p.getClass().getMethods();
			//			for (Method m:methods) 
			//			{
			//				String name=m.getName();
			//				if (name.startsWith("get")) System.out.println(name+"   "+m.getGenericReturnType());
			//			}

			//add directly to the graph 3D:
			Graph3DPanel graph3DPanel=new Graph3DPanel();
			//graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
			//graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);
			graph3DPanel.setRender(false);
			graph3DPanel.add(s);


			//create frame and add the panel 3D
			CJFrame cjf=new CJFrame();
			cjf.getContentPane().setLayout(new BorderLayout());
			cjf.setSize(600, 400);
			cjf.setVisible(true);
			cjf.add(graph3DPanel.getComponent());
		}
		
		
		
		
		
		
		
		
		
		
		
	}




}





