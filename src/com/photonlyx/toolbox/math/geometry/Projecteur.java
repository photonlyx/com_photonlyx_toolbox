//*******************************************************************************************
//*************************** CLASSE PROJECTEUR ********************************************* //*******************************************************************************************
// Author Laurent Brunel.CERN 1996
package com.photonlyx.toolbox.math.geometry;




/**3D to screen projector*/
public class Projecteur   implements Projector
{
	private Vecteur projX,projY,projZ,projcentre;
	private int ecranW,ecranH;
	// double encombrement,coefzoom;
	private double coefzoom;
	private double zoomStep=1.4;
	private double angleStep=0.05;


	public void initialise(Vecteur cornermin,Vecteur cornermax,int _screenW,int _screenH)
	{
		coefzoom=Math.max(_screenW,_screenH);
		projX=new Vecteur(1,0,0);
		projY=new Vecteur(0,1,0);
		projZ=new Vecteur(0,0,1);
		projcentre=new Vecteur(0,0,0);
		ecranW=_screenW;
		ecranH=_screenH;
	}

	/**update the screen size*/
	public void updateScreenSize(int ex,int ey)	
	{
		// s'adapte a une nouvelle taille de fenetre
		ecranW=ex;
		ecranH=ey;
	}

	/** project a 3D point P on the plane of the Projecteur*/
	public double[] projete(Vecteur P)
	{
		Vecteur OP;
		double[] proj=new double[2];
		OP=P.sub(projcentre);
		proj[0]=OP.mul(projX);
		proj[1]=OP.mul(projY);
		return proj;
	}

	/** calc the z coordinate of the point P in the frame of the Projecteur*/
	public double zCoord(Vecteur P)
	{
		Vecteur OP;
		double[] proj=new double[2];
		OP=P.sub(projcentre);
		double z=OP.mul(projZ);
		return z;
	}

	/**
return the vector parallel to the plane X,Y and that would give coorEcran if it is projected
	 */
	public Vecteur deProjeteRelative(double ecranDeltax,double ecranDeltay)
	{
		double x,y;
		x=(ecranDeltax)/(ecranW/coefzoom);
		y=(-ecranDeltay)/(ecranH/coefzoom);
		return projX.scmul(x)._add(projY.scmul(y));
	}

	/**
return the POINT of the plane X,Y and that would give coorEcran if it is projected
	 */
	public Vecteur deProjeteAbsolute(double ecranCoordx,double ecranCoordy)
	{
		double x,y;
		x=(ecranCoordx-ecranW/2)/(ecranW/coefzoom);
		y=(-ecranCoordy+ecranH/2)/(ecranH/coefzoom);
		return (projX.scmul(x)._add(projY.scmul(y)))._add(projcentre);
	}

	/**calculates the screen co-ordinates*/
	public void calcCoorScreen(double[] coorEcran,Vecteur P) 
	{
		double[] proj;
		proj=projete(P);
		coorEcran[0]= (proj[0]*ecranW/coefzoom+ecranW/2);
		coorEcran[1]= (proj[1]*ecranH/coefzoom+ecranH/2);
		coorEcran[1]=ecranH-coorEcran[1];
	}

	public void calcCoorScreen(Vecteur2D cooScreen,Vecteur P)
	{
		double[] proj;
		proj=projete(P);
		cooScreen.setX(proj[0]*ecranW/coefzoom+ecranW/2);
		cooScreen.setY(ecranH-(proj[1]*ecranH/coefzoom+ecranH/2));
	}



	/**calculates the screen co-ordinates*/
	public double[] calcCoorEcran(Vecteur P) 
	{
		double[] proj;
		double[] coorEcran=new double[2]; 
		proj=projete(P);
		coorEcran[0]= (proj[0]*ecranW/coefzoom+ecranW/2);
		coorEcran[1]=(proj[1]*ecranH/coefzoom+ecranH/2);
		coorEcran[1]=ecranH-coorEcran[1];
		return coorEcran;
	}

	/**calculates the screen co-ordinates*/
	public double[] calcCoorEcranDouble(Vecteur P) 
	{
		double[] proj;
		double[] coorEcran=new double[2]; 
		proj=projete(P);
		coorEcran[0]=proj[0]*ecranW/coefzoom+ecranW/2;
		coorEcran[1]=proj[1]*ecranH/coefzoom+ecranH/2;
		coorEcran[1]=ecranH-coorEcran[1];
		return coorEcran;
	}

	/**turn the projector. angles in rad*/
	public void turn(double ax,double ay,double az)
	{
		Vecteur angles=new Vecteur(ax,ay,az);
		Rotator r=new Rotator(angles);
		projX.affect(r.turn(projX));
		projY.affect(r.turn(projY));
		projZ.affect(r.turn(projZ));
		//	System.out.println("angles :"+angles.print(""));
		//	System.out.println("projX :"+projX.print(""));
		//	System.out.println("projY :"+projY.print(""));
		//	System.out.println("projZ :"+projZ.print(""));
	}

	/**set the angular step for rotations*/
	public void setAngleStep(double r){angleStep=r;}
	/**turn x axis by the angleStep*/
	public void turnX(){turn(angleStep,0,0);}
	/**turn x axis by - angleStep*/
	public void unturnX(){turn(-angleStep,0,0);}
	/**turn y axis by the angleStep*/
	public void turnY(){turn(0,angleStep,0);}
	/**turn y axis by -angleStep*/
	public void unturnY(){turn(0,-angleStep,0);}
	/**turn y axis by the angleStep*/
	public void turnZ(){turn(0,0,angleStep);}
	/**turn y axis by - angleStep*/
	public void unturnZ(){turn(0,0,-angleStep);}



	public void translater(double ecranDeltax,double ecranDeltay,double dz)//TODO revoir dz
	{
		//System.out.println(ecranDeltax+" "+ecranDeltay);
		Vecteur delta=deProjeteRelative( -ecranDeltax, -ecranDeltay);
		projcentre._add(delta);
	}

	/**get the centre of the prjector*/
	public Vecteur getCentre()
	{
		return projcentre;
	}

	public void setZoomStep(double r)
	{
		zoomStep=r;
	}


	/**
	 * recenter the drawing to the clicked point
	 * */
	public void recentrer(int xpointe,int ypointe)
	{
		Vecteur decX,decY;
		double scalX,scalY;
		decX=new Vecteur();
		decX.affect(projX);
		scalX=coefzoom*ecranW*(-(double)ecranW/2+(double)xpointe);
		//System.out.println("scalX "+scalX);
		decX=decX.scmul(scalX);

		decY=new Vecteur();
		decY.affect(projY);
		scalY=coefzoom*ecranH*((double)ecranH/2-(double)ypointe);
		//System.out.println("scalY "+scalY);
		decY=decY.scmul(scalY);

		projcentre._add(decX);
		projcentre._add(decY);
		//System.out.println("decX "+decX.print());
		//System.out.println("decY "+decY.print());
	}

}

