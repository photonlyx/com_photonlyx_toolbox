package com.photonlyx.toolbox.math.geometry;

import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;




/**
A sphere </p>
*/

public class Sphere extends  Shape implements XMLstorable
{
//params
//private double x,y,z;//centre of the sphere
private double r=1;//radius of the sphere

private Vecteur O;//center of the sphere




public Sphere()
{
O=new Vecteur();
}


/**
 * 
 * @param x x of centre
 * @param y y of centre
 * @param z z of centre
 * @param r radius
 */
public Sphere(double x, double y, double z, double r) {
	super();
//	this.x = x;
//	this.y = y;
//	this.z = z;
	this.r = r;
	O=new Vecteur(x,y,z);
}



public double getX() {
	return O.x();
}




public void setX(double x) {
	O.setX(x);
}




public double getY() {
	return O.y();
}




public void setY(double y) {
O.setY(y);
}




public double getZ() {
	return O.z();
}




public void setZ(double z) {
O.setZ(z);
}




public double getR() {
	return r;
}




public void setR(double r) {
	this.r = r;
}




public double distance(Vecteur pos,Vecteur dir)
{
Vecteur I1=new Vecteur();
Vecteur I2=new Vecteur();
Vecteur phI1,phI2;
int nbsol=intersectionSphereLine(O,r,pos,dir,I1,I2);
if (nbsol==0) return 1e10;

else
	{
	//vector from photon to intersection
	phI1=I1.sub(pos);
	double sc1=dir.mul(phI1);
	if (sc1<=1e-5) sc1= 1e10;

	phI2=I2.sub(pos);
	double sc2=dir.mul(phI2);
	if (sc2<=1e-5) sc2= 1e10;
	return(Math.min(sc1,sc2));
	}
}




public Vecteur  normal(Vecteur I)
{
//I is a point belonging to the sphere
Vecteur OI=I.sub(O);
return OI.scmul(1/r);
}

/**return the centre point of the plane*/
public Vecteur centre()
{
return O;
}



/**
compute the eventual intersection points between a line and a sphere
O is a point of the cylinder axis
r is the radius of the cylinder
P is a point of the line
N is a vector colinear to the line of norm 1
I1 and I2 are the intersection points (same if only one, null if none)
*/
public static int  intersectionSphereLine(Vecteur O,double r,Vecteur P,Vecteur N,Vecteur I1,Vecteur I2)
{
//projection of the sphere centre on the photon ray
Vecteur PO=O.sub(P);
Vecteur Po=N.scmul(PO.mul(N));
double d1c=PO.normeSquare()-Po.normeSquare();
double d2c=r*r-d1c;
if (d2c<0) 
	{
	return 0;
	}
if (d2c!=0)
	{
	double d2=Math.sqrt(d2c);
	Vecteur PI1=Po.sub(N.scmul(d2));
	Vecteur PI2=Po.addn(N.scmul(d2));
	I1.affect(P.addn(PI1));
	I2.affect(P.addn(PI2));
	return 2;
	}
else       //here I=o
	{
	I1.affect(P.addn(Po));
	I2.affect(I2);
	return 1;
	}
}



@Override
public Vecteur intersection(Vecteur pos, Vecteur dir)
{
Vecteur I1=new Vecteur();
Vecteur I2=new Vecteur();
Vecteur phI1,phI2;
int nbsol=intersectionSphereLine(O,r,pos,dir,I1,I2);
Vecteur i;
if (nbsol==0) return null;
else
	{
	//vector from photon to intersection
	phI1=I1.sub(pos);
	double sc1=dir.mul(phI1);
	if (sc1<=1e-5) sc1= 1e10;

	phI2=I2.sub(pos);
	double sc2=dir.mul(phI2);
	if (sc2<=1e-5) sc2= 1e10;
	
	if (sc1<sc2) return I1;else return I2;
	}

}

@Override
public XMLElement toXML()
{
XMLElement el = new XMLElement();
el.setName(getClass().getSimpleName().toString().replace("class ", ""));
el.setDoubleAttribute("x",O.x());
el.setDoubleAttribute("y",O.y());
el.setDoubleAttribute("z",O.z());
el.setDoubleAttribute("r",r);
el.setAttribute("name",getName());
return el;
}

@Override
public void updateFromXML(XMLElement xml)
{
setX(xml.getDoubleAttribute("x",0));	
setY(xml.getDoubleAttribute("y",0));	
setZ(xml.getDoubleAttribute("z",0));	
setR(xml.getDoubleAttribute("r",100));	
setName(xml.getStringAttribute("name",""));	
}




}
