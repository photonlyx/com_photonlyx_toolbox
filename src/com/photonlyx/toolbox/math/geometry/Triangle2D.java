package com.photonlyx.toolbox.math.geometry;

import java.util.ArrayList;
import java.util.List;



public class Triangle2D 
{
	private Point2D p1;
	private Point2D p2;
	private Point2D p3;

	public Triangle2D(Point2D p1, Point2D p2, Point2D p3) {
		this.setP1(p1);
		this.setP2(p2);
		this.setP3(p3);
	}

	// Returns a list of edges for this triangle
	public  List<Edge> getEdges() {
		List<Edge> edges = new ArrayList<>();
		edges.add(new Edge(getP1(), getP2()));
		edges.add(new Edge(getP2(), getP3()));
		edges.add(new Edge(getP3(), getP1()));
		return edges;
	}

	// Checks if a point is inside the circumcircle of this triangle
	public  boolean isPointInCircumcircle(Point2D point) {
		double ax = getP1().x - point.x;
		double ay = getP1().y - point.y;
		double bx = getP2().x - point.x;
		double by = getP2().y - point.y;
		double cx = getP3().x - point.x;
		double cy = getP3().y - point.y;

		double det = (ax * ax + ay * ay) * (bx * cy - by * cx)
				- (bx * bx + by * by) * (ax * cy - ay * cx)
				+ (cx * cx + cy * cy) * (ax * by - ay * bx);

		return det > 0;
	}

	// Checks if this triangle shares any vertex with another triangle
	public  boolean sharesVertexWith(Triangle2D triangle) {
		return getP1().equals(triangle.getP1()) || getP1().equals(triangle.getP2()) || getP1().equals(triangle.getP3()) ||
				getP2().equals(triangle.getP1()) || getP2().equals(triangle.getP2()) || getP2().equals(triangle.getP3()) ||
				getP3().equals(triangle.getP1()) || getP3().equals(triangle.getP2()) || getP3().equals(triangle.getP3());
	}

	// Checks if this triangle has a specific edge
	public  boolean hasEdge(Edge edge) {
		return getEdges().contains(edge);
	}






	public Point2D getP1() {
		return p1;
	}

	public void setP1(Point2D p1) {
		this.p1 = p1;
	}






	public Point2D getP2() {
		return p2;
	}

	public void setP2(Point2D p2) {
		this.p2 = p2;
	}






	public Point2D getP3() {
		return p3;
	}

	public void setP3(Point2D p3) {
		this.p3 = p3;
	}






	// Class representing an edge between two points
	public class Edge {
		private Point2D p1;
		private Point2D p2;

		Edge(Point2D p1, Point2D p2) {
			this.setP1(p1);
			this.setP2(p2);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null || getClass() != obj.getClass()) return false;
			Edge edge = (Edge) obj;
			return (getP1().equals(edge.getP1()) && getP2().equals(edge.getP2())) || (getP1().equals(edge.getP2()) && getP2().equals(edge.getP1()));
		}

		public Point2D getP1() {
			return p1;
		}

		public void setP1(Point2D p1) {
			this.p1 = p1;
		}

		public Point2D getP2() {
			return p2;
		}

		public void setP2(Point2D p2) {
			this.p2 = p2;
		}
	}







}
