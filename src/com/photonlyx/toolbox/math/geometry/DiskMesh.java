package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;

public class DiskMesh  extends TriangleMeshFrame
{
	private double d=1;//diameter of the cylinder
	private int dim=20;//nb of sampling points

	public DiskMesh()
	{
		init();
		updateGlobal();
	}

	/**
	 * 	
	 * @param o first face centre
	 * @param n axis
	 * @param d
	 * @param dim
	 */
	public DiskMesh(double d,int dim)
	{
		this.d=d;
		this.dim=dim;
		init();
		updateGlobal();
	}

	
	private void init()
	{
		double dAlpha=Math.PI*2/dim;
		Vector<Vecteur> vertices=new Vector<Vecteur>();
		for (int i=0;i<dim;i++) 
		{
			Vecteur p1=new Vecteur(d/2*Math.cos(i*dAlpha),d/2*Math.sin(i*dAlpha),0);
			vertices.add(p1);
		}
		Vecteur o=frame.getCentre();
		for (int i=0;i<dim;i++) 
		{
			int i2=(i+1)%dim;
			Triangle3D t1=new Triangle3D(vertices.get(i).copy(),vertices.get(i2).copy(),o).copy();
			local.add(t1);	
		}
		
		//create global triangles:
		for (Triangle3D tri:local) global.add(tri.copy());

	}





	public static void main(String[] args)
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();

		//the disk:
		double diameter=10;
		DiskMesh disk1=new DiskMesh(diameter,5);
		System.out.println(disk1.saveToSTL());
		disk1.getFrame()._translate(new Vecteur(1,1,1));	
		disk1.getFrame()._rotate(new Vecteur(0,0.2,0));	
		disk1.updateGlobal();
		System.out.println(disk1.saveToSTL());
		
		
		//CylinderMesh cyl=new CylinderMesh();
		//Box box=new Box();
	
		//graph3DPanel.add(box);
		graph3DPanel.add(disk1);
		
		graph3DPanel.updateOccupyingCube();
		graph3DPanel.initialiseProjector();
		graph3DPanel.update();
		graph3DPanel.initialiseProjector();

		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
		
		
		graph3DPanel.initialiseProjector();


	}

}
