package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.mesh.Edges;
import com.photonlyx.toolbox.threeD.gui.mesh.Faces;
import com.photonlyx.toolbox.threeD.gui.mesh.MeshSource;
import com.photonlyx.toolbox.threeD.gui.mesh.Vertices;


public class Box  extends TriangleMeshFrame implements Object3DFrame,TriangleMeshSource
{
	private double dx,dy,dz;//size in x y and z
	private Vecteur[][][]  pLocal=new Vecteur[2][2][2];
	private double[][][][] cooScreen=new double[2][2][2][2];//projection on screen coordinates
	private Vecteur[][][]  pGlobal=new Vecteur[2][2][2];
	private Segment3D[] globalEdges=new Segment3D[12];
	private Frame frame=new Frame();//local frame of the cylinder
	private TriangleMesh globalTriangleMesh=new TriangleMesh();

	//for MeshSource:
	private Vertices vertices=new  Vertices ();
	private Edges edges=new  Edges();
	private Faces faces=new  Faces ();

	public Box()
	{
		init();
	}

	public Box(Vecteur o,double dx,double dy,double dz)
	{
		this.init(o, dx, dy, dz);
	}
	
	public Box(Vecteur o,Vecteur diag)
	{
		this.init(o, diag.x(), diag.y(), diag.z());
	}


	private void init()
	{
		init(new Vecteur(),1,1,1);
	}

	private void init(Vecteur o,double dx,double dy,double dz)
	{
		for (int i=0;i<2;i++ )
			for (int j=0;j<2;j++ )
				for (int k=0;k<2;k++ ) 	
				{
					pLocal[i][j][k]=new Vecteur();
					pGlobal[i][j][k]=new Vecteur();
				}
		frame.setCentre(o);
		this.dx=dx;
		this.dy=dy;
		this.dz=dz;
		allocateVertices();
		updateGlobal();
		allocateEdges(); 
		allocateTriangles(); 
		allocateMesh();
	}
	
	/**
	 * set the origin wrt (0,0,0) corner
	 * @param v
	 */
	public void setOrigin(Vecteur origin)
	{
		allocateVertices(origin);
		updateGlobal();
		allocateEdges(); 
		allocateTriangles(); 
		allocateMesh();
	}

	private void allocateVertices(Vecteur origin)
	{
		for (int i=0;i<2;i++ )
			for (int j=0;j<2;j++ )
				for (int k=0;k<2;k++ ) 	
				{
					pLocal[i][j][k].affect(new Vecteur(i*dx,j*dy,k*dz).sub(origin));
					//pGlobal[i][j][k]=new Vecteur(i*dx,j*dy,k*dz);
				}
	}
	
	private void allocateVertices()
	{
		for (int i=0;i<2;i++ )
			for (int j=0;j<2;j++ )
				for (int k=0;k<2;k++ ) 	
				{
					pLocal[i][j][k].affect(new Vecteur(i*dx,j*dy,k*dz));
					//pGlobal[i][j][k]=new Vecteur(i*dx,j*dy,k*dz);
				}
	}

	private void calcScreenCoordinates(Projector proj)
	{
		for (int i=0;i<2;i++ )
			for (int j=0;j<2;j++ )
				for (int k=0;k<2;k++ ) 	
				{
					proj.calcCoorScreen(cooScreen[i][j][k],pGlobal[i][j][k]);
				}
	}


	private void allocateMesh()
	{
		vertices.removeAllElements();
		edges.removeAllElements();
		faces.removeAllElements();
		for (Segment3D s:globalEdges) edges.add(s); 
	}

	@Override
	public void updateGlobal()
	{
		for (int i=0;i<2;i++ )
			for (int j=0;j<2;j++ )
				for (int k=0;k<2;k++ ) 	
					frame.global(pGlobal[i][j][k], pLocal[i][j][k]);
	}

	private void allocateEdges()
	{
		int c=0;
		for (int x=0;x<2;x++)
			for (int y=0;y<2;y++)
				for (int z=0;z<2;z++)
				{
					if ((1-x)>x) globalEdges[c++]=(new Segment3D(pGlobal[x][y][z],pGlobal[1-x][y][z]));
					if ((1-y)>y)  globalEdges[c++]=(new Segment3D(pGlobal[x][y][z],pGlobal[x][1-y][z]));
					if ((1-z)>z)  globalEdges[c++]=(new Segment3D(pGlobal[x][y][z],pGlobal[x][y][1-z]));			
				}
	}

	public void allocateTriangles()
	{
		//transform quads in triangle for STL
		globalTriangleMesh.removeAllElements();
		//TriangleMesh set=this;
		Triangle3D t;
		t=new Triangle3D(pGlobal[0][0][0],pGlobal[0][1][0],pGlobal[0][0][1]);t._invertNormal();globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[0][1][1],pGlobal[0][1][0],pGlobal[0][0][1]);globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[1][0][0],pGlobal[1][1][0],pGlobal[1][0][1]);globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[1][1][1],pGlobal[1][1][0],pGlobal[1][0][1]);t._invertNormal();globalTriangleMesh.add(t);

		t=new Triangle3D(pGlobal[0][0][0],pGlobal[1][0][0],pGlobal[0][0][1]);globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[1][0][1],pGlobal[1][0][0],pGlobal[0][0][1]);t._invertNormal();globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[0][1][0],pGlobal[1][1][0],pGlobal[0][1][1]);t._invertNormal();globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[1][1][1],pGlobal[1][1][0],pGlobal[0][1][1]);globalTriangleMesh.add(t);

		t=new Triangle3D(pGlobal[0][0][0],pGlobal[1][0][0],pGlobal[0][1][0]);t._invertNormal();globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[1][1][0],pGlobal[1][0][0],pGlobal[0][1][0]);globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[0][0][1],pGlobal[1][0][1],pGlobal[0][1][1]);globalTriangleMesh.add(t);
		t=new Triangle3D(pGlobal[1][1][1],pGlobal[1][0][1],pGlobal[0][1][1]);t._invertNormal();globalTriangleMesh.add(t);
	}






	/**if one of the is out of the cube, extends the cube*/
	public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax)
	{
		for (int i=0;i<2;i++ )
			for (int j=0;j<2;j++ )
				for (int k=0;k<2;k++ ) 	
					for (int l=0;l<3;l++ ) 	
					{
						double r=pLocal[i][j][k].coord(l);
						if (r < cornerMin.coord(l)) cornerMin.coordAffect(l,r);
						if (r > cornerMax.coord(l)) cornerMax.coordAffect(l,r);
					}	
	}



	public void draw(Graphics g,Projector proj)
	{
		this.calcScreenCoordinates(proj);
		//draw
		for (int x=0;x<2;x++)
			for (int y=0;y<2;y++)
				for (int z=0;z<2;z++)
				{
					//System.out.println((int)cooScreen[x][y][z][0]+"   "+(int)cooScreen[x][y][z][1]);	
					if ((1-x)>x) g.drawLine((int)cooScreen[x][y][z][0],(int)cooScreen[x][y][z][1],(int)cooScreen[1-x][y][z][0],(int)cooScreen[1-x][y][z][1]);
					if ((1-y)>y) g.drawLine((int)cooScreen[x][y][z][0],(int)cooScreen[x][y][z][1],(int)cooScreen[x][1-y][z][0],(int)cooScreen[x][1-y][z][1]);
					if ((1-z)>z) g.drawLine((int)cooScreen[x][y][z][0],(int)cooScreen[x][y][z][1],(int)cooScreen[x][y][1-z][0],(int)cooScreen[x][y][1-z][1]);
				}
	}



	public double getDistanceToScreen(Projector proj)
	{
		return 	0;
	}



	public Vecteur point(int i,int j,int k) {
		return pLocal[i][j][k];
	}


	public String toString()
	{
		return "";
	}

	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		Segment2D seg2;
		for (int x=0;x<2;x++)
			for (int y=0;y<2;y++)
				for (int z=0;z<2;z++)
				{
					seg2=new Segment2D(cooScreen[x][y][z],cooScreen[1-x][y][z]);
					if (seg2.getDistance(p.x,p.y)<3) return true;
					seg2=new Segment2D(cooScreen[x][y][z],cooScreen[x][1-y][z]);
					if (seg2.getDistance(p.x,p.y)<3) return true;
					seg2=new Segment2D(cooScreen[x][y][z],cooScreen[x][y][1-z]);
					if (seg2.getDistance(p.x,p.y)<3) return true;
				}
		return false;
	}


	@Override
	public TriangleMesh getTriangles()
	{
		return globalTriangleMesh;
	}

	@Override
	public Frame getFrame()
	{
		return frame;
	}

	



	public Edges getEdges() {
		return edges;
	}

	public String saveToOBJ() 
	{
		StringBuffer sb=new StringBuffer();
			for (int x=0;x<2;x++)
				for (int y=0;y<2;y++)
					for (int z=0;z<2;z++)
					{
						Vecteur p=pGlobal[x][y][z];
						sb.append("v "+p.x()+" "+p.y()+" "+p.z()+"\n");
					}
			sb.append("f 1 2 4 3 \n");	
			sb.append("f 7 8 6 5 \n");	
			sb.append("f 5 6 2 1 \n");	
			sb.append("f 3 4 8 7 \n");	
			sb.append("f 1 3 7 5 \n");	
			sb.append("f 6 8 4 2 \n");	
		return sb.toString();	
	}

	

	public static void main(String[] args) 
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);
//		Object3DColorSet set=new Object3DColorSet();
//		graph3DPanel.addColorSet(set);


		Box box=new Box();
		//box.getFrame()._translate(new Vecteur(1,0,0));
		//box.updateGlobal();
		box.setOrigin(new Vecteur(0.5,0.5,0.5));
		graph3DPanel.add(box);

		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
	}



}
