package com.photonlyx.toolbox.math.geometry;


import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;




/**
Simulate a plane interface of equation z=z0 </p>
*/

public class PlaneZ  extends Plane implements XMLstorable
{


public PlaneZ()
{
pPlane.affect(0,0,0);
nPlane.affect(0,0,1);
}

public PlaneZ(double z)
{
pPlane.coordAffect(2, z);
}

public PlaneZ(Vecteur pPlane,Vecteur nPlane)
{
super(pPlane,nPlane);
}



public double getZ() 
{
return pPlane.coord(2);
}

public void setZ(double z) 
{
pPlane.coordAffect(2, z);
}

/**give the distance to the plane  from the point pos in direction of dir*/
public double distance(Vecteur pos,Vecteur dir)
{
//rayon parallele au plan
//if ( ((pPlane.z()-pos.z())*dir.z())<=1e-9) return 1e10;
//intersection point between the ray and the surface
Vecteur i=Vecteur.intersectionPlaneLine(pPlane,nPlane,pos,dir);
//System.out.println("procedure distance a plan photon direction: "+dir;
//System.out.println("procedure distance a plan photon norme: "+dir.norme());

//vector from photon to intersection
Vecteur pi=i.sub(pos);
//System.out.println("pi="+pi);

double sc=dir.mul(pi);
//double iz=i.z();
//double iz2=pos.addn(dir.scmul(sc)).z();
//if (Math.abs(iz-iz2)>0.001)
//	{
//	System.out.println("****************************");
//	System.out.println("iz="+i.z());
//	System.out.println("sc="+sc);
//	//System.out.println("pPlane="+pPlane+"   nPlane="+nPlane);
//	System.out.println("  angle%Z "+180.0/Math.PI*Math.acos(dir.mul(Vecteur.baseZ))+"°");
//	System.out.println("iz2="+iz2);
//	System.out.println("pPlane="+pPlane);
//	System.out.println("nPlane="+nPlane);
//	System.out.println("pos="+pos);
//	System.out.println("dir="+dir+"(norm="+dir.norme()+")");
//	System.out.println("****************************");
//	}

//if (sc<=1e-5) return 1e10;
return sc;
}

/**return the centre point of the plane*/
public Vecteur centre()
{
return pPlane;
}

/**return the normal vector at the point i (that must belong to the plane!)*/
public Vecteur normal(Vecteur i)
{
return nPlane;
}



/**return a new Vecteur which is the intersection of this plane with a line.*/
public Vecteur intersectionDroite(Vecteur Pdroite,Vecteur ndroite)
{
double tdroite;
Vecteur PdPp,u;
double r;

PdPp=pPlane.sub(Pdroite);
r=ndroite.mul(nPlane);
if (r!=0) tdroite=PdPp.mul(nPlane)/r;  else tdroite=1e50;
u=ndroite.scmul(tdroite);
return u.addn(Pdroite);
}

@Override
public Vecteur intersection(Vecteur pos, Vecteur dir)
{
Vecteur i=Vecteur.intersectionPlaneLine(pPlane,nPlane,pos,dir);
return i;
}


public String toString()
{
String s="PlaneZ\n";
s+="  pPlane="+pPlane+"\n";
s+="  nPlane="+nPlane;
return s;
}

@Override
public XMLElement toXML()
{
XMLElement el = new XMLElement();
el.setName(getClass().getSimpleName().toString().replace("class ", ""));
el.setDoubleAttribute("z",getZ());
el.setAttribute("name",getName());
return el;
}

@Override
public void updateFromXML(XMLElement xml)
{
setZ(xml.getDoubleAttribute("z",0));	
String name=xml.getStringAttribute("name","");
setName(name);	

}



}
