package com.photonlyx.toolbox.math.geometry;

import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;


/**
a cylinder axis along Y if ax=0 and az=0</p>
*/

public class Cylinder extends Shape implements XMLstorable
{
private double x,y,z;
private double ax,az;//angles in degree
private double r;//radius of the cylinder
//internal
private Vecteur O;//a point of the axis
private Vecteur C;//vector parallel to the axis, norm 1




public Cylinder()
{
}

private void init()
{
O=new Vecteur(x,y,z);
Rotator rot=new Rotator(ax*Math.PI/180,0,az*Math.PI/180);
C=rot.turn(new Vecteur(0,1,0));

}


public double getX() {
	return x;
}


public void setX(double x) 
{
	this.x = x;
	init();
	}


public double getY() {
	return y;
}


public void setY(double y) {
	this.y = y;
	init();
}


public double getZ() {
	return z;
}


public void setZ(double z) {
	this.z = z;
	init();
}


public double getAx() {
	return ax;
}


public void setAx(double ax) {
	this.ax = ax;
	init();
}


public double getAz() {
	return az;
}


public void setAz(double az) {
	this.az = az;
	init();
}


public double getR() {
	return r;
}


public void setR(double r) {
	this.r = r;
}


/**give the distance to the cylinder from the point pos in direction of dir, return 1e10 if no intersection*/
public double distance(Vecteur pos,Vecteur dir)
{
Vecteur I1=new Vecteur();
Vecteur I2=new Vecteur();
Vecteur phI1,phI2;
int nbsol=intersectionCylinderLine(O,C,r,pos,dir,I1,I2);
double d;
//Messager.logln(getClass()+" "+getNode().getName()+" intersections: "+I1+" "+I2);
if (nbsol==0) d=1e10;
else
	{
	//vector from photon to intersection
	phI1=I1.sub(pos);
	double sc1=dir.mul(phI1);
	if (sc1<=1e-5) sc1= 1e10;
	phI2=I2.sub(pos);
	double sc2=dir.mul(phI2);
	if (sc2<=1e-5) sc2= 1e10;
	d=Math.min(sc1,sc2);
	}
//Messager.logln(getClass()+" "+getNode().getName()+" distance "+d);
return d;
}

public Vecteur  normal(Vecteur I)
{
//I is a point belonging to the cylinder
Vecteur OI=I.sub(O);
//P is the projection of I on the cylinder axis
Vecteur OP=C.scmul(C.mul(OI));
Vecteur PI=OI.sub(OP);
PI._normalise();
return PI;
}

/**return the centre point of the plane*/
public Vecteur centre()
{
return O;
}






/**
compute the eventual intersection points between a line and a cylinder
O is a point of the cylinder axis
C is a vector of norm 1 colinear to the cylinder axis
r is the radius of the cylinder
P is a point of the line
N is a vector colinear to the line of norm 1
I1 and I2 are the intersection points (same if only one, null if none)
return the number of solution (0 if outside ,1 if tangent, 2)
*/
public static int  intersectionCylinderLine(Vecteur O,Vecteur C,double r,Vecteur P,Vecteur N,Vecteur I1,Vecteur I2)
{

//projection on a vectorial plane perpendicular to the cylinder axis
double NmC=N.mul(C);
Vecteur n=N.sub(C.scmul(NmC));
n._normalise();
//n is the normalised projection of N
//Messager.logln("n "+n.print());//*********
Vecteur PO=O.sub(P);
//Messager.logln("PO "+PO.print());//*******
Vecteur Po=PO.sub(C.scmul(PO.mul(C)));
//Messager.logln("Po "+Po.print());//********
//o is the projection of O in the plane having P and perpendicular to the cylinder axis
Vecteur Ph=n.scmul(PO.mul(n));
//Messager.logln("Ph "+Ph.print());//*********
//the triangle Poh is rectangle at h
//square of the distance from the cylinder axis to the line
double d2=Po.normeSquare()-Ph.normeSquare();
//Messager.logln("r "+r);//********
//Messager.logln("d2 "+d2);//********
double hi2=r*r-d2;
//Messager.logln("hi2 "+hi2);//********
if (hi2<0) 
	{
	return 0;
	}
double hi=Math.sqrt(hi2);
//Messager.logln("hi "+hi);//**********

if (hi2!=0)
	{
	Vecteur vhi=n.scmul(hi);
//Messager.logln("vhi "+vhi.print());//************
	Vecteur Pi1= Ph.sub(vhi);
	Vecteur Pi2=Ph.addn(vhi);
//Messager.logln("Pi1 "+Pi1.print());//********
//Messager.logln("Pi2 "+Pi2.print());//********
	double tan=Math.sqrt(1/(1/(NmC*NmC)-1));
	if (NmC<0) tan=-tan;
//Messager.logln("NmC: "+NmC);//*******
//Messager.logln("tan: "+tan);//********
	Vecteur PI1=Pi1.addn( C.scmul (tan*Pi1.mul(n) ) );
	Vecteur PI2=Pi2.addn( C.scmul (tan*Pi2.mul(n) ) );
	I1.affect(P.addn(PI1));
	I2.affect(P.addn(PI2));
	return 2;
	}
else
	{
	double tan=Math.sqrt(1/(1/(NmC*NmC)-1));
	if (NmC<0) tan=-tan;
	Vecteur PI=Ph.addn(C.scmul(Ph.mul(n)*tan));
//Messager.logln("PI "+PI.print());//**********
	I1.affect(P.addn(PI));
	return 1;
	}


}

public String getDefaultDef()
{
String s="";
s+="param { name  x default 0 } \n";
s+="param { name  y default 0 }\n";
s+="param { name  z default 0 } \n";
s+="param { name  ax default 0 } \n";
s+="param { name  ay default 0 }\n";
s+="param { name  radius default 1 }\n";
return s;
}


@Override
public Vecteur intersection(Vecteur pos, Vecteur dir)
{
Vecteur I1=new Vecteur();
Vecteur I2=new Vecteur();
Vecteur phI1,phI2;
int nbsol=intersectionCylinderLine(O,C,r,pos,dir,I1,I2);
double d;
Vecteur i;
//Messager.logln(getClass()+" "+getNode().getName()+" intersections: "+I1+" "+I2);
if (nbsol==0) return null;
else
	{
	//vector from photon to intersection
	phI1=I1.sub(pos);
	double sc1=dir.mul(phI1);
	if (sc1<=1e-5) sc1= 1e10;
	phI2=I2.sub(pos);
	double sc2=dir.mul(phI2);
	if (sc2<=1e-5) sc2= 1e10;
	//d=Math.min(sc1,sc2);
	if (sc1<sc2) i=I1;else i=I2;
	}
return i;
}


@Override
public XMLElement toXML()
{
XMLElement el = new XMLElement();
el.setName(getClass().getSimpleName().toString().replace("class ", ""));
el.setDoubleAttribute("x",O.x());
el.setDoubleAttribute("y",O.y());
el.setDoubleAttribute("z",O.z());
el.setDoubleAttribute("ax",O.x());
el.setDoubleAttribute("az",O.y());
el.setDoubleAttribute("r",r);
el.setAttribute("name",getName());
return el;
}

@Override
public void updateFromXML(XMLElement xml)
{
setX(xml.getDoubleAttribute("x",0));	
setY(xml.getDoubleAttribute("y",0));	
setZ(xml.getDoubleAttribute("z",0));	
setX(xml.getDoubleAttribute("ax",0));	
setY(xml.getDoubleAttribute("az",0));	
setR(xml.getDoubleAttribute("r",100));	
setName(xml.getStringAttribute("name",""));	
}


}
