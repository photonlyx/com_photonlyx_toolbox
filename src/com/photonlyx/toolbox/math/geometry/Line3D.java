package com.photonlyx.toolbox.math.geometry;

public class Line3D 
{
private Vecteur o;//origin of the line
private Vecteur v;//direction of the line, normalized


/**
 * v must be normalized
 * @param o
 * @param v
 */
public Line3D(Vecteur o,Vecteur v)
{
this.o=o;
this.v=v;	
}

public Line3D(Segment3D seg)
{
o=seg.p1();
v=seg.p2().sub(seg.p1());	
}

/**get the origin*/
public Vecteur o(){return o;};

/**get the direction*/
public Vecteur v(){return v;};


}
