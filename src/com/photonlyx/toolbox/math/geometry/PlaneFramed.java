package com.photonlyx.toolbox.math.geometry;

public class PlaneFramed extends Plane
{
private Vecteur X,Y;

/**
 * 
 * @param x point of the plane, x
 * @param y point of the plane, y
 * @param z point of the plane, z
 * @param ax frame FIRST turned by ax radian around x vector
 * @param ay frame SECOND turned by ay radian around y vector
 * @param az frame THIRD turned by az radian around z vector
 */
public  PlaneFramed(double x,double y,double z,double ax,double ay,double az)
{
super(x,y,z,ax,ay,az);
Rotator rot=new Rotator(ax,ay,az);
X=rot.turn(new Vecteur(1,0,0));
Y=rot.turn(new Vecteur(0,1,0));
}



public PlaneFramed(Vecteur p, Vecteur ux, Vecteur uy, Vecteur uz) 
{
super(p,uz);
this.X=ux;
this.Y=uy;
}



/**return the local co-ordinates the intersection of this plane
 with a line.*/
public double[] intersectionLineLocal(Vecteur Pdroite,Vecteur ndroite)
{
double[] co=new double[2];
Vecteur i=intersectionLine(Pdroite,ndroite);
Vecteur Oi=i.sub(pPlane);
co[0]=X.mul(Oi);
co[1]=Y.mul(Oi);
return co;
}

/**return the local co-ordinates the intersection of this plane
 with a line. only intersect the line from Pdroite in the direction of ndroite*/
public double[] intersectionLineLocalForward(Vecteur Pdroite,Vecteur ndroite)
{
double[] co=new double[2];
Vecteur i=intersectionLine(Pdroite,ndroite);
if (i==null) return null;
if (i.sub(Pdroite).mul(ndroite)<0) return null;
Vecteur Oi=i.sub(pPlane);
co[0]=X.mul(Oi);
co[1]=Y.mul(Oi);
return co;
}


/**return the local co-ordinates of the normal projection of the point p*/
public double[] projectionLocalCoordinates(Vecteur p)
{
double[] co=new double[2];
Vecteur i=this.project(p);
Vecteur Oi=i.sub(pPlane);
co[0]=X.mul(Oi);
co[1]=Y.mul(Oi);
return co;
}

}
