package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import nanoxml.XMLElement;

public class SphereBumpy extends TriangleMeshFrame implements Object3DFrame,XMLstorable
{
	private int nLon=20,nLat=20;// nb of picks in longitude and latitude
	private int n=100;//sampling
	private double r0=3;//radius
	private double h=0.4;//pick height (radius is one)
	private double latMax=Math.PI/2*0.8;
	private Object3DColorSet set4obj=new Object3DColorSet();

	private Frame frame=new Frame();//local frame 

	public SphereBumpy()
	{
		init();
	}


	@Override
	public void updateGlobal()
	{
		TriangleMesh globalMesh=this.getGlobal();
		//calc the global coordinates:
		for (int i=0;i<globalMesh.nbTriangles();i++)
		{
			frame.global(globalMesh.getTriangle(i),getLocal().getTriangle(i));
		}
	}

	//public TriangleMesh getTriangles() {return tris;}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		getGlobal().checkOccupyingCube(cornerMin, cornerMax);

	}
	@Override
	public void draw(Graphics g, Projector proj) 
	{
		getGlobal().draw(g, proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		return getGlobal().getDistanceToScreen(proj);
	}
	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Triangle3D tri:getGlobal()) 
			if (tri.isOn(p, proj)) return true;
		return false;
	}


	@Override
	public Frame getFrame()
	{
		return frame;
	}



	public void init()
	{
		set4obj.removeAll();
		double lon,lat,r;
		double lons=Math.PI*2/n;//sampling in longitude, rad
		double lats=latMax/n;//sampling in latitude
		Rotator rot=new Rotator();

		Vecteur[][] mat=new Vecteur[n+1][n];

		//build the points in mat:
		for (int j=-n/2;j<=n/2;j++)//loop on latitude
		{
			lat=j*lats*2;
			for (int i=0;i<n;i++)//loop on longitude
			{
				lon=lons*i;
				r=r0+h*Math.sin(lon*nLon)*Math.cos(lat*nLat*Math.PI/2/latMax);
				rot.init(0,lat,lon);
				Vecteur p=new Vecteur(r,0,0);
				rot._turn(p);
				mat[j+n/2][i]=p;
			}
		}
		//build the quadrilateres:
		Vecteur p1,p2,p3,p4;
		for (int j=0;j<n+1-1;j++)//loop on latitude
			for (int i=0;i<n;i++)//loop on longitude
			{
				p1=mat[j][i];
				p4=mat[j+1][i];
				if (i==n-1)
				{
					p2=mat[j][0];
					p3=mat[j+1][0];
				}
				else
				{
					p2=mat[j][i+1];
					p3=mat[j+1][i+1];
				}
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p4,p3,p2);
				set4obj.add(q);
			}
		//generate top and bottom
		for (int k=0;k<=n;k+=n)
			for (int i=0;i<n;i++)//loop on longitude
			{
				p1=mat[k][i];
				if (i==n-1)
				{
					p2=mat[k][0];
				}
				else
				{
					p2=mat[k][i+1];
				}
				p3=new Vecteur(0,0,mat[k][0].z());
				Triangle3DColor t;
				if (k==n) t=new Triangle3DColor(p1,p3,p2,Color.black,true);
				else t=new Triangle3DColor(p1,p2,p3,Color.black,true);
				set4obj.add(t);
			}

		//lonPeriod=Math.PI*r0*2/nLon;
		//latPeriod=4*latMax*r0/nLat;
		//saveAsSTL(set,"shape.stl");
		//set local mesh:
		TriangleMesh localMesh=this.getLocal();
		localMesh.removeAllElements();		
		for (Object3DColor o:set4obj)
		{
			if (o instanceof Quadrilatere3DColor)
			{
				Quadrilatere3DColor q=(Quadrilatere3DColor)o;
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
			if (o instanceof Triangle3DColor)
			{
				Triangle3DColor t=(Triangle3DColor)o;
				localMesh.add(t);
			}

		}
		//set global mesh:
		getGlobal().removeAllElements();
		for (Triangle3D tri:localMesh) getGlobal().add(tri.copy());

		updateGlobal();

	}





	public int getnLon() {
		return nLon;
	}


	public void setnLon(int nLon) {
		this.nLon = nLon;
	}


	public int getnLat() {
		return nLat;
	}


	public void setnLat(int nLat) {
		this.nLat = nLat;
	}


	public int getN() {
		return n;
	}


	public void setN(int n) {
		this.n = n;
	}


	public double getR0() {
		return r0;
	}


	public void setR0(double r0) {
		this.r0 = r0;
	}


	public double getH() {
		return h;
	}


	public void setH(double h) {
		this.h = h;
	}



	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().getSimpleName().toString().replace("class ", ""));
		el.setAttribute("nLon",nLon);
		el.setAttribute("nLat",nLat);
		el.setAttribute("n",n);
		el.setAttribute("r0",r0);
		el.setAttribute("h",h);
		return el;
	}

	@Override
	public void updateFromXML(XMLElement xml)
	{
		nLon=xml.getIntAttribute("nLon",10);	
		nLat=xml.getIntAttribute("nLat",10);	
		n=xml.getIntAttribute("n",100);	
		r0=xml.getDoubleAttribute("r0",1);	
		h=xml.getDoubleAttribute("h",0.1);	
	}



	public static void main(String[] args) 
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		//graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		//graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);


		SphereBumpy s=new SphereBumpy();

		//		{
		//			//save object parameters in an XML file:
		//			XMLElement el=s.toXML();
		//			TextFiles.saveString("/home/laurent/temp/bumpy.xml",el.toString());
		//		}

		if (args.length==1)
		{
			if (args[0].compareTo("-h")==0)
			{
				System.out.println("Usage: java -cp com_photonlyx_toolbox.jar com.photonlyx.toolbox.math.geometry.SphereBumpy"
						+ "-i params.xml"+ " -o sphere1.stl");
				System.out.println("Example of xml file:");
				System.out.println(XMLFileStorage.toStringWithIndentation(s.toXML()));
				System.exit(0);
			}

		}
		if (args.length>=1)
		{
			String inputFileName=null;//XML file with input parameters
			String outpuFileName=null;//STL file with output geometry
			for (int i=0;i<args.length;i++)
			{
				if (args[i].compareTo("-i")==0)
				{

					inputFileName=args[i+1];
				}
				if (args[i].compareTo("-o")==0)
				{
					//load parameters from XML file
					outpuFileName=args[i+1];
				}
			}
			if ((inputFileName!=null)&&(outpuFileName!=null))
			{
				//load parameters from XML file
				XMLElement xml=XMLFileStorage.readXMLfile(inputFileName);
				s.updateFromXML(xml);
				System.out.println(s.toXML());
				s.init();
				s.getFrame()._translate(new Vecteur(0,0,-s.r0));
				s.getFrame()._rotate(new Vecteur(Math.PI/2,0,0));
				s.updateGlobal();
				s.saveToSTLfile("./",outpuFileName);
			}

		}		

		if (args.length==0)
		{
			s.getFrame()._translate(new Vecteur(1,0,0));
			s.updateGlobal();

			//		//add to a set:
			//		Object3DColorSet set=new Object3DColorSet();
			//		set.add(s.getTriangles());
			//		graph3DPanel.addColorSet(set);

			//add directly to the graph 3D:
			graph3DPanel.add(s.getTriangles());


			//create frame and add the panel 3D
			CJFrame cjf=new CJFrame();
			cjf.getContentPane().setLayout(new BorderLayout());
			cjf.setSize(600, 400);
			cjf.setVisible(true);
			cjf.add(graph3DPanel.getComponent());
		}



	}




}





