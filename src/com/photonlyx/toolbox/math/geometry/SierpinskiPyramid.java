package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;

public class SierpinskiPyramid implements Object3DFrame,TriangleMeshSource
{
	private double a=1;//side
	private double h=1;//height
	private int recursion=0;
	private TriangleMesh localMesh=new TriangleMesh();
	private TriangleMesh globalMesh=new TriangleMesh();
	private Frame frame=new Frame();//local frame 

	public SierpinskiPyramid()
	{
		init();
	}

	public SierpinskiPyramid(double a,double h, int recursionLevel)
	{
		this.a=a;
		this.h=h;
		this.recursion=recursionLevel;
		init();
	}


	@Override
	public void updateGlobal()
	{
		//calc the global coordinates:
		for (int i=0;i<globalMesh.nbTriangles();i++)
		{
			frame.global(globalMesh.getTriangle(i),localMesh.getTriangle(i));
		}
	}

	//public TriangleMesh getTriangles() {return tris;}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		globalMesh.checkOccupyingCube(cornerMin, cornerMax);

	}
	@Override
	public void draw(Graphics g, Projector proj) 
	{
		globalMesh.draw(g, proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		return globalMesh.getDistanceToScreen(proj);
	}
	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Triangle3D tri:globalMesh) 
			if (tri.isOn(p, proj)) return true;
		return false;
	}


	@Override
	public Frame getFrame()
	{
		return frame;
	}



	@Override
	/**
	 * 
	 */
	public TriangleMesh getTriangles() {
		return globalMesh;
	}


	public  static Vector<Pyramid> refine(Pyramid p)
	{
		double a1=p.getA();
		double h1=p.getH();
		Vecteur o=p.getFrame().getCentre();
		Vector<Pyramid> ps=new Vector<Pyramid>();
		{
			Pyramid p1=new Pyramid(a1/2,h1/2);
			p1.getFrame()._translate(new Vecteur(a1/4,a1/4,0).addn(o));
			p1.updateGlobal();
			ps.add(p1);
		}

		{
			Pyramid p1=new Pyramid(a1/2,h1/2);
			p1.getFrame()._translate(new Vecteur(-a1/4,a1/4,0).addn(o));
			p1.updateGlobal();
			ps.add(p1);
		}

		{
			Pyramid p1=new Pyramid(a1/2,h1/2);
			p1.getFrame()._translate(new Vecteur(a1/4,-a1/4,0).addn(o));
			p1.updateGlobal();
			ps.add(p1);
		}

		{
			Pyramid p1=new Pyramid(a1/2,h1/2);
			p1.getFrame()._translate(new Vecteur(-a1/4,-a1/4,0).addn(o));
			p1.updateGlobal();
			ps.add(p1);
		}

		{
			Pyramid p1=new Pyramid(a1/2,h1/2);
			p1.getFrame()._translate(new Vecteur(0,0,h1/2).addn(o));
			p1.updateGlobal();
			ps.add(p1);
		}
		return ps;
	}


	public  static Vector<Pyramid> refine(Vector<Pyramid> ps)
	{
		Vector<Pyramid> ps1=new Vector<Pyramid>();
		for (Pyramid p:ps) 
		{
			Vector<Pyramid> ps2=refine(p);
			for (Pyramid p1:ps2) ps1.add(p1);
		}
		return ps1;
	}

	//  http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
	//calc the local frame triangles
	public void init()
	{
		localMesh.removeAllElements();	
		Pyramid p=new Pyramid(a,h);
		Vector<Pyramid> ps=new Vector<Pyramid>();
		ps.add(p);
		TriangleMesh tris=new TriangleMesh();
		Vector<Pyramid> ps1=new Vector<Pyramid>();//total list of pyramids
		for (int i=0;i<recursion;i++)
		{
			Vector<Pyramid> ps2=refine(ps);
			ps1.removeAllElements();
			for (Pyramid p1:ps2) ps1.add(p1);
			ps=ps1;
		}
		for (Pyramid p1:ps1) tris.add(p1.getTriangles());
		for (Triangle3D tri:tris) localMesh.add(tri);
		for (Triangle3D tri:tris) globalMesh.add(tri.copy());
		updateGlobal();
	}






	public static void main(String[] args) 
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);

		for (int i=0;i<8;i++)
		{
			SierpinskiPyramid s=new SierpinskiPyramid(1,1,i);
			graph3DPanel.addObject3D(s);
			String path="/home/laurent/fabserver/cloud1/photonlyx/projets/2015_MC_rendering/starlyx/projets_de_test/sierpinsky";
			String file="sierpinsky"+"_"+i+".obj";
			s.getTriangles().saveToOBJ(path+"/"+file);
			System.out.println(s.getTriangles().nbTriangles()+" triangles");
			//s.getTriangles().saveToSTL(path+"sierpinsky.stl");
		}


		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
	}




}





