package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.File;
import java.util.Vector;

import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;
import com.photonlyx.toolbox.txt.TextFiles;

/**
 * mesh of triangles having a local frame
 * @author laurent
 *
 */
public class TriangleMeshFrame  implements Object3DFrame,TriangleMeshSource
{
	protected Frame frame=new Frame();
	protected TriangleMesh local=new TriangleMesh();
	protected TriangleMesh global=new TriangleMesh();


	public TriangleMeshFrame()
	{
	}

	public TriangleMeshFrame(TriangleMesh t)
	{
		local=t;
		createGlobalTriangles();
	}

	@Override
	public Frame getFrame() 
	{
		return frame;
	}

	public void add(Triangle3D t) 
	{
		local.add(t);
		Triangle3D tglobal=t.copy();
		global.add(tglobal);
		frame.global(tglobal,t);
	}

	public void add(Quadrilatere3D q) 
	{
		Triangle3D t1=new Triangle3D(q.p1(),q.p2(),q.p3());
		local.add(t1);
		Triangle3D t2=new Triangle3D(q.p1(),q.p3(),q.p4());
		local.add(t2);
		Triangle3D tglobal1=t1.copy();
		Triangle3D tglobal2=t2.copy();
		global.add(tglobal1);
		global.add(tglobal2);
		frame.global(tglobal1,t1);
		frame.global(tglobal2,t2);
	}


	private void createGlobalTriangles()
	{
		//create global triangles
		global.removeAllElements();
		for (Triangle3D t:local) 
		{
			Triangle3D t2=new Triangle3D(t.p1().copy(),t.p2().copy(),t.p3().copy());
			global.add(t2);
		}
	}

	@Override
	/**
	 * update coord of global triangles
	 */
	public void updateGlobal() 
	{
		for (int i=0;i<global.nbTriangles();i++)
		{
			frame.global(global.getTriangle(i),local.getTriangle(i));
		}
	}

	public  void add(TriangleMesh mesh)
	{
		local.add(mesh.copy());
		for (int i=0;i<mesh.nbTriangles();i++) global.add(new Triangle3D());
		updateGlobal();
	}



	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) {
		global.checkOccupyingCube(cornerMin, cornerMax);

	}

	@Override
	public void draw(Graphics g, Projector proj) {
		global.draw(g, proj);

	}

	@Override
	public double getDistanceToScreen(Projector proj) {
		return global.getDistanceToScreen(proj);
	}

	@Override
	public boolean isOn(Point p, Projector proj) {
		return global.isOn(p, proj);
	}

	public void readOBJ(String s)
	{
		local.readOBJ(s);
		frame.copy(new Frame());		
		createGlobalTriangles();
	}

	/**
	 * load mesh from OBJ ascii file
	 * @param path
	 * @param file
	 */
	public void readOBJ(String path, String file)
	{
		String s=TextFiles.readFile(path+File.separator+file).toString();
		local.readOBJ(s);
		frame.copy(new Frame());		
		createGlobalTriangles();
	}


	public String saveToOBJ()
	{
		return global.saveToOBJ();
	}

	public void saveToOBJfile(String path, String file)
	{
		TextFiles.saveString(path+File.separator+file,this.saveToOBJ(),false);
	}


	public void readSTL(String s)
	{
		local.readSTL(s);
		frame.copy(new Frame());
		createGlobalTriangles() ;		
	}
	public void readSTLasciiFile(String path,String filename)
	{ 
		String s=TextFiles.readFile(path+File.separator+filename).toString();
		this.readSTL(s);
		frame.copy(new Frame());
		createGlobalTriangles() ;		
	}
	public void readSTLinBinaryFile(String path,String filename)
	{ 
		local.updateFromBinarySTLfile(path+File.separator+filename);
		frame.copy(new Frame());
		createGlobalTriangles() ;		
	}

	public String saveToSTL()
	{
		return global.saveToSTL();
	}

	public void saveToSTLfile(String path, String file)
	{
		TextFiles.saveString(path+File.separator+file,this.saveToSTL(),false);
	}

	public void readSignal2D1D(Signal2D1D im)
	{
		local.importSignal2D1D(im);
		frame.copy(new Frame());
		createGlobalTriangles() ;		
	}


	@Override
	public TriangleMesh getTriangles() {
		return global;
	}


	public Vector<Segment3D> getIntersectedSegments(Plane plane)
	{
		TriangleMesh mesh=this.getTriangles();
		Vector<Segment3D> list=new	Vector<Segment3D>();
		for (int i=0;i<mesh.nbTriangles();i++)
		{
			Triangle3D t=mesh.getTriangle(i);
			Segment3D seg=t.getIntersectedSegment(plane);
			if (seg!=null) list.add(seg);
		}
		return list;
	}

	/**
	 * cut he geometry with plane. return the 2 half geometries, first at the plane normal side, 
	 * second the geometry at the other side
	 * @param p
	 * @return
	 */
	public Vector<TriangleMesh> cutWithPlane(Plane p)
	{
		//the 2 meshes with triangles up and down:
		Vector<TriangleMesh> meshes=new Vector<TriangleMesh>();
		TriangleMesh up=new TriangleMesh();//at the side of the normal
		TriangleMesh down=new TriangleMesh();//at the opposite side of the normal
		meshes.add(up);
		meshes.add(down);
		TriangleMesh mesh=this.getTriangles();
		for (int i=0;i<mesh.nbTriangles();i++)
		{
			Triangle3D t=mesh.getTriangle(i);
			//check which corner is at which side of the plane:
			Vector<Vecteur> points_up=new Vector<Vecteur>();
			Vector<Vecteur> points_down=new Vector<Vecteur>();
			for (int j=0;j<3;j++)
			{
				Vecteur c=t.p[j];
				if (c.sub(p.pPlane).mul(p.nPlane)>0) points_up.add(c);
				else points_down.add(c);
			}
			if (points_up.size()==3)
			{
				//all the corners of the triangle are up
				up.add(t);
			}
			else if (points_down.size()==3)
			{
				//all the corners of the triangle are down
				down.add(t);
			}
			else
			{
				//the triangle is intersected by the plane
				if (points_up.size()==1)
				{
					//just one corner is up
					//cut the triangles in ones up and down
					Vecteur cup=points_up.elementAt(0);
					//c1 and c1 are the corners down:
					Vecteur c1=points_down.elementAt(0);
					Vecteur c2=points_down.elementAt(1);
					Vecteur i1=new Segment3D(cup,c1).intersection(p);
					Vecteur i2=new Segment3D(cup,c2).intersection(p);
					//create the triangle up (cup,i1,i2):
					up.add(new Triangle3D(cup,i1,i2));
					//create the triangles down (i1,i2,c1) and (c1,i2,c2):
					down.add(new Triangle3D(i1,i2,c1));
					down.add(new Triangle3D(c1,i2,c2));
				}
				else
				{
					//2 corners are up
					//cut the triangles in ones up and down
					Vecteur cdown=points_down.elementAt(0);
					//c1 and c1 are the corners up:
					Vecteur c1=points_up.elementAt(0);
					Vecteur c2=points_up.elementAt(1);
					Vecteur i1=new Segment3D(cdown,c1).intersection(p);
					Vecteur i2=new Segment3D(cdown,c2).intersection(p);
					//create the triangle down (cup,i1,i2):
					down.add(new Triangle3D(cdown,i1,i2));
					//create the triangles up (i1,i2,c1) and (c1,i2,c2):
					up.add(new Triangle3D(i1,i2,c1));
					up.add(new Triangle3D(c1,i2,c2));
				}
			}
		}
		return meshes;
	}






	public TriangleMesh getLocal() {
		return local;
	}

	public TriangleMesh getGlobal() {
		return global;
	}

	public static void joconde()
	{
		Signal2D1D im=new Signal2D1D(100,100);
		//	Gaussian f=new Gaussian(0.1,0.3,0.5,0.5);
		//	im.fillWithFonction(f);

		String path="/home/laurent/fabserver/cloud1/photonlyx/projets/2022_05_17_Joconde/2022_11_22_couche_rayée";
		String filename="peinture_rayee.png";
		double w=8;
		double dz=0.05;
		im.loadFromImage(new CImage(path,filename,false));
		double scaleXY=w/im.dimx();
		im.setxmin(0);
		im.setxmax(im.dimx()*scaleXY);
		im.setymin(0);
		im.setymax(im.dimy()*scaleXY);
		im._multiply(dz/(im.zmax()-im.zmin()));
		im._flipAroundX();

		TriangleMeshFrame mesh=new TriangleMeshFrame();
		mesh.local.importSignal2D1D(im,0.05);
		mesh.frame.copy(new Frame());
		mesh.createGlobalTriangles() ;
		//rotate around Y:
		mesh.getFrame()._rotate(new Vecteur(0,Math.PI/2,0));
		mesh.getFrame()._translate(new Vecteur(0,-w/2,-w/2));
		//translate:
		mesh.getFrame()._translate(new Vecteur(-0.06,0,0));
		//rotate around y:
		//mesh.getFrame()._rotate(new Vecteur(0,Math.PI,0,0));
		mesh.updateGlobal();

		//Graph3DWindow g3d=new Graph3DWindow();
		//g3d.getGraph3DPanel().add(mesh);

		mesh.saveToOBJfile(path,filename.replace("png","obj"));
	}

	public static void testCut(String[] args)
	{
		Graph3DWindow w=new Graph3DWindow();
		w.getCJFrame().setExitAppOnExit(true);
		Plane plane=new Plane(new Vecteur(),new Vecteur(0,0,1));

		SphereICO o=new SphereICO(1,1);o.getFrame()._translate(new Vecteur(0,0,0.1));o.updateGlobal();
		//Box o=new Box(new Vecteur(-1,-1,-1),2,2,2);
		//Pyramid o=new Pyramid(1,1); o.getFrame()._translate(new Vecteur(0,0,-0.5));o.updateGlobal();

		//graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		w.getGraph3DPanel().getJPanel().setBackground(Color.LIGHT_GRAY);
		w.getGraph3DPanel().setRender(false);
		//w.getGraph3DPanel().add(o.getTriangles());

		Vector<TriangleMesh> meshes=o.cutWithPlane(plane);
		w.getGraph3DPanel().add(meshes.elementAt(0));
		w.getGraph3DPanel().add(meshes.elementAt(1));


	}

	public static void main(String[] args)
	{
		//joconde();
		testCut(args);



	}

	public void addLocalTriangles(TriangleMesh triangles) 
	{
		local.add(triangles);
		this.createGlobalTriangles();
	}

	public TriangleMeshFrame getAcopy() 
	{
		TriangleMeshFrame m=new TriangleMeshFrame();
		m.frame=this.frame.getAcopy();
		for (Triangle3D t: this.local) m.local.add(t.copy());
		for (Triangle3D t: this.global) m.global.add(t.copy());
		return m;
	}


}
