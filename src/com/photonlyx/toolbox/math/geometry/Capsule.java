package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;

import nanoxml.XMLElement;


/**
 * shape of a pill : cylinder closed by 2 half spheres at the ends
 */
public class Capsule extends TriangleMeshFrame implements XMLstorable
{
	private double h=2;//lnght of the cylinder
	private int n=20;//sampling of circle
	private double d=1;//diameter of the cylinder

	public Capsule()
	{
	}


	public void init()
	{

//		{
//			//the cylinder
//			double dAlpha=Math.PI*2/n;
//			Vector<Segment3D> localSegments=new Vector<Segment3D>();
//			Vecteur o=frame.getCentre();
//			//side segments:
//			for (int i=0;i<n;i++) 
//			{
//				Vecteur p1=new Vecteur(d/2*Math.cos(i*dAlpha),d/2*Math.sin(i*dAlpha),0)._add(o);
//				Vecteur p2=new Vecteur(d/2*Math.cos(i*dAlpha),d/2*Math.sin(i*dAlpha),h)._add(o);
//				localSegments.add(new Segment3D(p1,p2));
//			}
//			for (int i=0;i<n;i++) 
//			{
//				int i2=(i+1)%n;
//				Segment3D seg1=localSegments.get(i);
//				Segment3D seg2=localSegments.get(i2);
//				Triangle3D t1=new Triangle3D(seg1.p1().copy(),seg1.p2().copy(),seg2.p1().copy());
//				local.add(t1);	
//				Triangle3D t2=new Triangle3D(seg1.p2().copy(),seg2.p1().copy(),seg2.p2().copy());
//				local.add(t2);	
//			}
//		}

		{
			//down sphere
			SphereICO s=new SphereICO(d/2,2);
			s.getFrame()._rotate(new Vecteur(-Math.PI/4,0,0));
			s.updateGlobal();
			for (Triangle3D tri:s.getGlobal()) 
				{
				//if (tri.getCentre().z()<0) 
					getLocal().add(tri.copy());
				}
		}

//		{
//			//top sphere
//			SphereICO s=new SphereICO(d/2,2);
//			s.getFrame()._rotate(new Vecteur(Math.PI/4,0,0));
//			s.getFrame()._translate(new Vecteur(0,0,h));
//			s.updateGlobal();
//			for (Triangle3D tri:s.getGlobal()) 
//				{
//				if (tri.getCentre().z()>h) getLocal().add(tri.copy());
//				}
//		}
		
		
		//set global mesh:
		getGlobal().removeAllElements();
		for (Triangle3D tri:getLocal()) getGlobal().add(tri.copy());
		updateGlobal();
	}


	public int getN() {
		return n;
	}


	public void setN(int n) {
		this.n = n;
	}





	public double getH() {
		return h;
	}


	public void setH(double h) {
		this.h = h;
	}


	public double getD() {
		return d;
	}


	public void setD(double d) {
		this.d = d;
	}


	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().getSimpleName().toString().replace("class ", ""));
		el.setAttribute("d",d);
		el.setAttribute("h",h);
		el.setAttribute("n",getN());
		return el;
	}

	@Override
	public void updateFromXML(XMLElement xml)
	{
		d=xml.getDoubleAttribute("d",0.5);	
		setN(xml.getIntAttribute("n",16));	
		h=xml.getDoubleAttribute("h",1);	
	}


	public static void main(String[] args) 
	{
		//test prg:
		Capsule pill=new Capsule();
		if (args.length==0)
		{

			pill.setN(32);
			pill.setD(0.5);
			pill.setH(1);
			pill.init();
			//			//rotate by -90 degrees (-PI/2 rad) around y axis
			//			dr.getFrame()._rotate(new Vecteur(0,-Math.PI/2,0));
			//			dr.getFrame()._translate(new Vecteur(2,0,0));
			pill.updateGlobal();
			
			pill.saveToOBJfile("/home/laurent/temp","pill.obj");

			Graph3DWindow g3d=new Graph3DWindow(pill);

		}



	}



}





