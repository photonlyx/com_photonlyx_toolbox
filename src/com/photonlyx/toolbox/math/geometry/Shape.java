package com.photonlyx.toolbox.math.geometry;




/**
interface for geometrical shape (plane,cylinder,sphere,...)
*/

public abstract class Shape 
{
private String name="";

/** Return the distance of this surface to a point along  the line defined by this point and a direction*/
public abstract double distance(Vecteur pos,Vecteur dir);

/** Return the intersection with the line defined by a point and a direction */
public abstract Vecteur intersection(Vecteur pos,Vecteur dir);

/**return the centre point*/
public abstract Vecteur centre();

/**return the normal vector at the point i (that must belong to the plane!)*/
public abstract Vecteur normal(Vecteur i);

public String getName()
{
return name;
}

public void setName(String name)
{
this.name = name;
}


}
