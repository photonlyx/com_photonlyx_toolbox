package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;

public class CylinderMesh  extends TriangleMeshFrame implements Object3DFrame,TriangleMeshSource
{
	private double d=1;//diameter of the cylinder
	private double h=1;//diameter of the cylinder
	private int dim=20;//nb of sampling points


	public CylinderMesh()
	{
	}

	/**
	 * 	
	 * @param o first face centre
	 * @param n axis
	 * @param d
	 * @param h
	 * @param dim
	 */
	public CylinderMesh(double d,double h,int dim)
	{
		this.d=d;
		this.h=h;
		this.dim=dim;
		init();
		updateGlobal();
	}

	/**
	 * 	
	 * @param o first face centre
	 * @param n axis
	 * @param d
	 * @param h
	 * @param dim
	 */
	public CylinderMesh(Vecteur o,double d,double h,int dim)
	{
		this.frame.setCentre(o);
		this.d=d;
		this.h=h;
		this.dim=dim;
		init();
		updateGlobal();
	}

	private void init()
	{
		double dAlpha=Math.PI*2/dim;
		Vector<Segment3D> localSegments=new Vector<Segment3D>();
		Vecteur o=frame.getCentre();
		//side segments:
		for (int i=0;i<dim;i++) 
		{
			Vecteur p1=new Vecteur(d/2*Math.cos(i*dAlpha),d/2*Math.sin(i*dAlpha),0)._add(o);
			Vecteur p2=new Vecteur(d/2*Math.cos(i*dAlpha),d/2*Math.sin(i*dAlpha),h)._add(o);
			localSegments.add(new Segment3D(p1,p2));
		}

		//allocate triangles (global same memory as global segments
		local.removeAllElements();
		for (int i=0;i<dim;i++) 
		{
			int i2=(i+1)%dim;
			Segment3D seg1=localSegments.get(i);
			Segment3D seg2=localSegments.get(i2);
			Triangle3D t1=new Triangle3D(seg1.p1().copy(),seg1.p2().copy(),seg2.p1().copy());
			local.add(t1);	
			Triangle3D t2=new Triangle3D(seg1.p2().copy(),seg2.p1().copy(),seg2.p2().copy());
			local.add(t2);	
		}
		//flat faces :
		//centre of the top flat face:
		Vecteur o2=new Vecteur(0,0,h);
		for (int i=0;i<dim;i++) 
		{
			int i2=(i+1)%dim;
			Segment3D seg1=localSegments.get(i);
			Segment3D seg2=localSegments.get(i2);
			Triangle3D t1=new Triangle3D(seg1.p1().copy(),seg2.p1().copy(),o.copy());
			local.add(t1);	
			Triangle3D t2=new Triangle3D(seg1.p2().copy(),seg2.p2().copy(),o2.copy());
			local.add(t2);	
		}
		
		//create global triangles:
		for (Triangle3D tri:local) global.add(tri.copy());
		updateGlobal();

	}




	public static void main(String[] args)
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();

		
	
		//the cylinder:
		double diameter=10;
		double hcyl=1;
		CylinderMesh cyl=new CylinderMesh(diameter,hcyl,10);
		cyl.getFrame()._translate(new Vecteur(1,1,1));	
		cyl.getFrame()._rotate(new Vecteur(0,0.2,0));	
		cyl.updateGlobal();
	
		//graph3DPanel.add(box);
		graph3DPanel.add(cyl);
		
		graph3DPanel.updateOccupyingCube();
		graph3DPanel.initialiseProjector();
		graph3DPanel.update();
		graph3DPanel.initialiseProjector();

		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
		
		
		graph3DPanel.initialiseProjector();


	}

}
