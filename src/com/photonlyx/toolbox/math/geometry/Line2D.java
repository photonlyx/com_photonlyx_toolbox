package com.photonlyx.toolbox.math.geometry;

public class Line2D 
{
	private Vecteur2D o;//origin of the line
	private Vecteur2D v;//direction of the line, normalized


	/**
	 * v must be normalized
	 * @param o
	 * @param v direction
	 */
	public Line2D(Vecteur2D o,Vecteur2D v)
	{
		this.o=o;
		this.v=v;	
	}

	public Line2D(double[] o,double[] v)
	{
		this.o=new Vecteur2D(o);
		this.v=new Vecteur2D(v);	
	}

	public Line2D(Segment2D seg)
	{
		o=seg.p1();
		v=seg.p2().subn(seg.p1());	
	}

	public Vecteur2D getIntersectionPointWith(Line2D l2)
	{
		//System.out.println(getClass()+" line1  "+this+"     line2 "+l);
		if (this.parallelWith(l2))	
		{
			System.out.println(getClass()+" lines are parallel ");
			return null;
		}
		double xi,yi;
		//transform in equation y=ax+b
		if  ( (this.v().x()==0)&&(l2.v().x()==0))//line 1 and line 2 vertical
		{
			System.out.println(getClass()+" line 1 and line 2 vertical ");
			return null;
		}
		else if (this.v().x()==0)//line 1 vertical
		{
			System.out.println(getClass()+" line 1 vertical ");
			xi=this.o().x();
			double t=(this.o().x()-l2.o().x())/l2.v().x();
			yi=l2.o.y()+l2.v().y()*t;
		}
		else if (l2.v().x()==0)//line 2 vertical
		{
			//System.out.println(getClass()+" line 2 vertical ");
			xi=l2.o().x();
			double t=(l2.o().x()-this.o().x())/this.v().x();
			yi=this.o.y()+this.v().y()*t;
		}
		else
		{
			//System.out.println(getClass()+" none is vertical ");
			double a1=this.v().y()/this.v().x();
			double b1=this.o().y()-(a1*this.o().x());
			double a2=l2.v().y()/l2.v().x();
			double b2=l2.o().y()-(a2*l2.o().x());
			xi=(b2-b1)/(a1-a2);
			yi=(a1*b2-a2*b1)/(a1-a2);

		}
		Vecteur2D i=new Vecteur2D(xi,yi);

		//Vecteur2D o1=this.o();
		//Vecteur2D o2=l.o();
		//Vecteur2D v1=this.v();
		//Vecteur2D v2=l.v();
		//double t;
		//double o1o2x=o2.x()-o1.x();
		//double o1o2y=o2.y()-o1.y();
		//Vecteur2D i;
		//if (Math.abs(o1o2x)>Math.abs(o1o2y)) 
		//	{
		//	t=o1o2x/(v1.x()-v2.x());
		//	}
		//else 
		//	{
		//	t=o1o2y/(v1.y()-v2.y());
		//	}
		//i=o1.addn(v1.scmul(t));
		return i;
	}


	public boolean parallelWith(Line2D l)
	{
		//return (this.v().equals(l.v())) ;
		return ((this.v.x()==l.v.x())&&(this.v.y()==l.v.y()));
	}

	/**get the origin*/
	public Vecteur2D o(){return o;};

	/**get the direction*/
	public Vecteur2D v(){return v;};

	public String toString()
	{
		return o.toString()+" dir ----> "+v;
	}
}
