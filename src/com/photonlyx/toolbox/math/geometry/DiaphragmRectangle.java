package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import nanoxml.XMLElement;


/**
 * rectangle (or open box) with an hole. 
 */
public class DiaphragmRectangle extends TriangleMeshFrame implements XMLstorable
{
	private double dx=10,dy=10;//size in x and y direction
	private int n=100;//sampling of circle
	private double r=1;//radius of the hole
	private double h=1;//size of eventual box

	public DiaphragmRectangle()
	{
	}


	public void init()
	{

		if ((2*r)>=Math.min(dx, dy)) 
		{
			System.err.println(getClass()+" diametre of hole bigger than rectangle size");
			return;
		}

		//set local mesh:
		TriangleMesh localMesh=this.getLocal();
		localMesh.removeAllElements();	

		//make the corners:
		Vecteur[] corners=new Vecteur[4];
		corners[0]=new Vecteur(dx/2,dy/2,0);//corner of the first quarter:
		corners[1]=new Vecteur(-dx/2,dy/2,0);//corner of the 2nd quarter:
		corners[2]=new Vecteur(-dx/2,-dy/2,0);//corner of the 3rd quarter:
		corners[3]=new Vecteur(dx/2,-dy/2,0);//corner of the 4th quarter:


		if (r>0)
		{
			//make the points of the circle:
			setN((getN()/4)*4);
			double dAlpha=Math.PI/2/(getN()/4);
			//make the first quarter:
			//Vector<Vecteur> vertices=new Vector<Vecteur>();
			for (int j=0;j<4;j++) 
			{
				double alpha0=j*Math.PI/2;
				for (int i=0;i<getN()/4;i++) 
				{
					double alpha1=alpha0+i*dAlpha;
					double alpha2=alpha1+dAlpha;
					Vecteur p1=new Vecteur(r*Math.cos(alpha1),r*Math.sin(alpha1),0);
					Vecteur p2=new Vecteur(r*Math.cos(alpha2),r*Math.sin(alpha2),0);
					//vertices.add(p);
					Triangle3D t=new Triangle3D(p2,p1,corners[j]);
					localMesh.add(t);
				}
				double alpha3=alpha0+Math.PI/2;
				Vecteur p=new Vecteur(r*Math.cos(alpha3),r*Math.sin(alpha3),0);
				Triangle3D t=new Triangle3D(p,corners[j],corners[(j+1)%4]);
				localMesh.add(t);
			}
		}
		else
		{
			Quadrilatere3D q=new Quadrilatere3D(corners[0],corners[1],corners[2],corners[3]);
			localMesh.add(q.getTriangles());			
		}

		if (h>0)
		{
			//make the corners:
			Vecteur[] corners1=new Vecteur[4];
			corners1[0]=new Vecteur(dx/2,dy/2,h);//corner of the first quarter:
			corners1[1]=new Vecteur(-dx/2,dy/2,h);//corner of the 2nd quarter:
			corners1[2]=new Vecteur(-dx/2,-dy/2,h);//corner of the 3rd quarter:
			corners1[3]=new Vecteur(dx/2,-dy/2,h);//corner of the 4th quarter:
			for (int j=0;j<4;j++) 
			{
				Quadrilatere3D q=new Quadrilatere3D(corners[j],corners1[j],corners1[(j+1)%4],corners[(j+1)%4]);
				for (Triangle3D t:q.getTriangles()) getLocal().add(t);
			}
		}

		//set global mesh:
		getGlobal().removeAllElements();
		for (Triangle3D tri:localMesh) getGlobal().add(tri.copy());
		updateGlobal();
	}


	public int getN() {
		return n;
	}


	public void setN(int n) {
		this.n = n;
	}


	public double getDx() {
		return dx;
	}


	public void setDx(double dx) {
		this.dx = dx;
	}


	public double getDy() {
		return dy;
	}


	public void setDy(double dy) {
		this.dy = dy;
	}


	public double getR() {
		return r;
	}


	public void setR(double r) {
		this.r = r;
	}


	public double getH() {
		return h;
	}


	public void setH(double h) {
		this.h = h;
	}



	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().getSimpleName().toString().replace("class ", ""));
		el.setAttribute("dx",dx);
		el.setAttribute("dy",dy);
		el.setAttribute("n",getN());
		el.setAttribute("r",r);
		el.setAttribute("h",h);
		return el;
	}

	@Override
	public void updateFromXML(XMLElement xml)
	{
		dx=xml.getDoubleAttribute("dx",10);	
		dy=xml.getDoubleAttribute("dy",10);	
		setN(xml.getIntAttribute("n",16));	
		r=xml.getDoubleAttribute("r",1);	
		h=xml.getDoubleAttribute("h",1);	
	}


	public static void main(String[] args) 
	{
		//test prg:
		DiaphragmRectangle dr=new DiaphragmRectangle();
		if (args.length==0)
		{
			//			dr.setN(16);
			//			dr.dx=8;
			//			dr.dy=7;
			//			dr.r=1;
			//			dr.h=1;
			//			dr.init();
			//
			//			//p.getFrame()._translate(new Vecteur(1,1,1));	
			//			dr.getFrame()._rotate(new Vecteur(0,-Math.PI/2,0));
			//
			//			double translationX=10;		
			//			dr.getFrame()._translate(new Vecteur(translationX,0,0));
			//
			//			dr.updateGlobal();

			dr.setN(64);
			dr.setDx(3);
			dr.setDy(3);
			dr.setR(0.2);
			dr.setH(1);
			dr.init();
			//rotate by -90 degrees (-PI/2 rad) around y axis
			dr.getFrame()._rotate(new Vecteur(0,-Math.PI/2,0));
			dr.getFrame()._translate(new Vecteur(2,0,0));
			dr.updateGlobal();




			//add directly to the graph 3D:
			Graph3DPanel graph3DPanel=new Graph3DPanel();
			//graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
			graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);
			graph3DPanel.setRender(false);
			graph3DPanel.add(dr.getTriangles());

			//create frame and add the panel 3D
			CJFrame cjf=new CJFrame();
			cjf.getContentPane().setLayout(new BorderLayout());
			cjf.setSize(600, 400);
			cjf.setVisible(true);
			cjf.add(graph3DPanel.getComponent());
		}



	}



}





