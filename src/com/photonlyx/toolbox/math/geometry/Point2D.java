package com.photonlyx.toolbox.math.geometry;

// Class representing a 2D point
public class Point2D 
{
	double x, y;

	public Point2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Point2D point = (Point2D) obj;
		return Double.compare(point.x, x) == 0 && Double.compare(point.y, y) == 0;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	
	
	

}
