package com.photonlyx.toolbox.math.geometry;



import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Point2D;
import com.photonlyx.toolbox.math.geometry.Point3D;
import com.photonlyx.toolbox.math.geometry.Triangle2D;
import com.photonlyx.toolbox.math.geometry.Triangle2D.Edge;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.cloud.Cloud;
import com.photonlyx.toolbox.math.geometry.cloud.CloudPoint;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;

//// Class representing a 2D point
//class Point {
//    double x, y;
//
//    Point(double x, double y) {
//        this.x = x;
//        this.y = y;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) return true;
//        if (obj == null || getClass() != obj.getClass()) return false;
//        Point point = (Point) obj;
//        return Double.compare(point.x, x) == 0 && Double.compare(point.y, y) == 0;
//    }
//}
//
//// Class representing an edge between two points
//class Edge {
//    Point p1, p2;
//
//    Edge(Point p1, Point p2) {
//        this.p1 = p1;
//        this.p2 = p2;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) return true;
//        if (obj == null || getClass() != obj.getClass()) return false;
//        Edge edge = (Edge) obj;
//        return (p1.equals(edge.p1) && p2.equals(edge.p2)) || (p1.equals(edge.p2) && p2.equals(edge.p1));
//    }
//}
//
//// Class representing a triangle
//class Triangle {
//    Point p1, p2, p3;
//
//    Triangle(Point p1, Point p2, Point p3) {
//        this.p1 = p1;
//        this.p2 = p2;
//        this.p3 = p3;
//    }
//
//    // Returns a list of edges for this triangle
//    List<Edge> getEdges() {
//        List<Edge> edges = new ArrayList<>();
//        edges.add(new Edge(p1, p2));
//        edges.add(new Edge(p2, p3));
//        edges.add(new Edge(p3, p1));
//        return edges;
//    }
//
//    // Checks if a point is inside the circumcircle of this triangle
//    boolean isPointInCircumcircle(Point point) {
//        double ax = p1.x - point.x;
//        double ay = p1.y - point.y;
//        double bx = p2.x - point.x;
//        double by = p2.y - point.y;
//        double cx = p3.x - point.x;
//        double cy = p3.y - point.y;
//
//        double det = (ax * ax + ay * ay) * (bx * cy - by * cx)
//                   - (bx * bx + by * by) * (ax * cy - ay * cx)
//                   + (cx * cx + cy * cy) * (ax * by - ay * bx);
//
//        return det > 0;
//    }
//
//    // Checks if this triangle shares any vertex with another triangle
//    boolean sharesVertexWith(Triangle triangle) {
//        return p1.equals(triangle.p1) || p1.equals(triangle.p2) || p1.equals(triangle.p3) ||
//               p2.equals(triangle.p1) || p2.equals(triangle.p2) || p2.equals(triangle.p3) ||
//               p3.equals(triangle.p1) || p3.equals(triangle.p2) || p3.equals(triangle.p3);
//    }
//
//    // Checks if this triangle has a specific edge
//    boolean hasEdge(Edge edge) {
//        return getEdges().contains(edge);
//    }
//}

// Main class for Delaunay triangulation
public class DelaunayTriangulation {
	List<Point2D> points;
	List<Triangle2D> triangles;

	public DelaunayTriangulation(List<Point2D> points) {
		this.points = points;
		this.triangles = new ArrayList<>();
	}

	// Run the Bowyer-Watson algorithm
	public void triangulate() {
		// Create a super-triangle large enough to encompass all points
		double maxX = 0, maxY = 0;
		for (Point2D p : points) {
			maxX = Math.max(maxX, p.getX());
			maxY = Math.max(maxY, p.getY());
		}
		Point2D p1 = new Point2D(-10 * maxX, -10 * maxY);
		Point2D p2 = new Point2D(10 * maxX, -10 * maxY);
		Point2D p3 = new Point2D(0, 10 * maxY);

		Triangle2D superTriangle = new Triangle2D(p1, p2, p3);
		getTriangles().add(superTriangle);

		// Add each point to the triangulation
		for (Point2D p : points) {
			List<Triangle2D> badTriangles = new ArrayList<>();

			// Find all triangles that are no longer valid due to the insertion
			for (Triangle2D t : getTriangles()) {
				if (t.isPointInCircumcircle(p)) {
					badTriangles.add(t);
				}
			}

			// Find the boundary of the polygonal hole
			List<Edge> polygon = new ArrayList<>();
			for (Triangle2D t : badTriangles) {
				for (Edge e : t.getEdges()) {
					if (!isEdgeShared(e, badTriangles)) {
						polygon.add(e);
					}
				}
			}

			// Remove the bad triangles from the triangulation
			getTriangles().removeAll(badTriangles);

			// Retriangulate the polygonal hole
			for (Edge e : polygon) {
				getTriangles().add(new Triangle2D(e.getP1(), e.getP2(), p));
			}
		}

		// Remove triangles that share vertices with the super-triangle
		getTriangles().removeIf(triangle -> triangle.sharesVertexWith(superTriangle));
	}

	// Helper method to check if an edge is shared among a list of triangles
	private boolean isEdgeShared(Edge edge, List<Triangle2D> triangles) {
		int count = 0;
		for (Triangle2D t : triangles) {
			if (t.hasEdge(edge)) count++;
		}
		return count > 1;
	}

	// Main method to run an example
	public static void main(String[] args) 
	{
		//        List<Point> points = List.of(
		//            new Point(1, 1), new Point(3, 3), new Point(5, 2),
		//            new Point(4, 4), new Point(2, 5), new Point(6, 6)
		//        );


		Vector<Point2D> points=new Vector<Point2D>();
		double dx=5;
		double dy=5;
		int nx=20;

		double x1=-dx/2;
		double x2=dx/2;
		double y1=-dy/2;
		double y2=dy/2;
		int ny=nx;
		double sx=(x2-x1)/(nx-1);
		double sy=(y2-y1)/(ny-1);

		//prepare data for Delaunay triangulation in plane XY:
		for (int i=0;i<nx;i++) 
			for (int j=0;j<ny;j++) 
			{
				double x=x1+i*sx+sx*((Math.random()-0.5)*0.2);
				double y=y1+j*sy+sy*((Math.random()-0.5)*0.2);
				points.add(new Point2D(x,y));
			}



		DelaunayTriangulation dt = new DelaunayTriangulation(points);
		dt.triangulate();

		System.out.println("Triangles:");
		for (Triangle2D t : dt.getTriangles()) 
		{
			System.out.println("Triangle: (" + t.getP1().getX() + "," + t.getP1().getY() + ") -> (" +
					t.getP2().getX() + "," + t.getP2().getY() + ") -> (" + t.getP3().getX() + "," + t.getP3().getY() + ")");
		}


		Graph3DWindow w=new Graph3DWindow();
		w.getCJFrame().setExitAppOnExit(true);
		w.getGraph3DPanel().getJPanel().setBackground(Color.LIGHT_GRAY);
		w.getGraph3DPanel().setRender(false);


		for (Point2D p:points)
		{
			w.getGraph3DPanel().add(new Object3DColorInstance(new Point3D(new Vecteur(p.getX(),p.getY(),0),5),Color.RED));

		}

		for (Triangle2D t : dt.getTriangles()) 
		{
			Triangle3D t1=new Triangle3D(
					new Vecteur(t.getP1().getX(),t.getP1().getY(),0),
					new Vecteur(t.getP2().getX(),t.getP2().getY(),0),
					new Vecteur(t.getP3().getX(),t.getP3().getY(),0));
			w.getGraph3DPanel().add(new Object3DColorInstance(t1,Color.BLACK));
		}
		//w.getGraph3DPanel().add(this.getMesh_layer());





	}

	public List<Triangle2D> getTriangles() {
		return triangles;
	}






}
