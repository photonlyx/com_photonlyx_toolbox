package com.photonlyx.toolbox.math.geometry;

public interface TriangleMeshSource 
{
	public TriangleMesh getTriangles();
}
