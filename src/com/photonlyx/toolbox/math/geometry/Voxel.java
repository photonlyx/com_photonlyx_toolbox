package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

public class Voxel implements Object3D
{
private Voxel father;
private Vecteur pmin=new Vecteur(),pmax=new Vecteur();
private Voxel[] sons= new Voxel[2];
private Vector<Triangle3D> triangles=new Vector<Triangle3D>();
private static Color edgeColor=Color.black;
private static final Vecteur bx=new Vecteur(1,0,0);//base vector x
private static final Vecteur by=new Vecteur(0,1,0);//base vector y
private static final Vecteur bz=new Vecteur(0,0,1);//base vector z

public Voxel()
{
}

public Voxel(Voxel father, Vecteur pmin, Vecteur pmax)
{
super();
this.father = father;
this.pmin = pmin;
this.pmax = pmax;
}

public void setSons(Voxel v1, Voxel v2)
{
sons[0]=v1;
sons[1]=v2;

}


///**
// * Recursively register the point in this voxel: 
// * find or create the son voxel that contains the point. 
// * And recursively do the same for the voxels sons .
// * @param tri
// */
//public void registerTriangle(Triangle3D tri,double minVoxelSizeX)
//{
//int ix,iy,iz;
//double phalfx=(pmax.x()+pmin.x())/2;
//double phalfy=(pmax.y()+pmin.y())/2;
//double phalfz=(pmax.z()+pmin.z())/2;
//
//tri.setVoxel(this);
//triangles.add(tri);
//
//Vecteur pmin2=new Vecteur(),pmax2=new Vecteur();
//if ((tri.getCentre().x())>=phalfx) 
//	{
//	ix=1;
//	pmin2.setX(phalfx);
//	pmax2.setX(pmax.x());
//	}
//else
//	{
//	ix=0;
//	pmin2.setX(pmin.x());
//	pmax2.setX(phalfx);
//	}
//	
//if ((tri.getCentre().y())>=phalfy)
//	{
//	iy=1;
//	pmin2.setY(phalfy);
//	pmax2.setY(pmax.y());
//	}
//	else
//	{
//	iy=0;
//	pmin2.setY(pmin.y());
//	pmax2.setY(phalfy);
//	}
//if ((tri.getCentre().z())>=phalfz)
//	{
//	iz=1;
//	pmin2.setZ(phalfz);
//	pmax2.setZ(pmax.z());
//	}
//	else
//	{
//	iz=0;
//	pmin2.setZ(pmin.z());
//	pmax2.setZ(phalfz);
//	}
//
////System.out.println("**"+pmin.x()+"   "+pmax.x());
////System.out.println(pmin2.x()+"   "+pmax2.x());
//if ((pmax2.x()-pmin2.x())>minVoxelSizeX) 
//	{
//	if (sons[ix][iy][iz]==null) sons[ix][iy][iz]=new Voxel(this,pmin2,pmax2);
//	sons[ix][iy][iz].registerTriangle(tri,minVoxelSizeX);
//	}
//}


@Override
public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
{
cornerMin.affect(pmin);
cornerMax.affect(pmax);
}


@Override
/**
 * draw the voxels sons if any
 */
public void draw(Graphics g, Projector proj)
{
//draw the edges:
g.setColor(edgeColor);
double[] cooScreen1,cooScreen2;
for (Segment3D seg:getVertices()) 
	{
	cooScreen1=proj.calcCoorEcran(seg.p1());
	cooScreen2=proj.calcCoorEcran(seg.p2());
	g.drawLine((int)cooScreen1[0],(int)cooScreen1[1],(int)cooScreen2[0],(int)cooScreen2[1]);
	}
	
}



//
//public boolean hasSons()
//{
//for (int ix=0;ix<2;ix++)for (int iy=0;iy<2;iy++)for (int iz=0;iz<2;iz++) 
//	if (sons[ix][iy][iz]!=null) 
//		{
//		return true;
//		}
//return false;
//}
//
///**destroy tree to avoid memory leaks*/
//public void destroyTheTree()
//{
//for (int ix=0;ix<2;ix++)
//	for (int iy=0;iy<2;iy++)
//		for (int iz=0;iz<2;iz++) 
//			if (sons[ix][iy][iz]!=null)
//				{
//				sons[ix][iy][iz].removeLinksToMeasures();
//				sons[ix][iy][iz].setFather(null);
//				sons[ix][iy][iz]=null;
//				}
//this.removeLinksToMeasures();
//this.setFather(null);
//}


@Override
public double getDistanceToScreen(Projector proj)
{
// TODO Auto-generated method stub
return 0;
}

public Vecteur getPmin()
{
return pmin;
}

public void setPmin(Vecteur pmin)
{
this.pmin .affect( pmin);
}

public Vecteur getPmax()
{
return pmax;
}

public void setPmax(Vecteur pmax)
{
this.pmax.affect( pmax);
}

public Voxel getFather()
{
return father;
}

//public Voxel[][][] getSons()
//{
//return sons;
//}

public Voxel[] getSons()
{
return sons;
}

public void setFather(Voxel father)
{
this.father = father;
}

/** free the memory !*/
public void removeLinksToMeasures()
{
triangles.removeAllElements();
}

/**get the measures inside the voxel*/
public Vector<Triangle3D> getTriangles()
{
return triangles;
}


public Vector<Segment3D> getVertices()
{
//half size vector
Vecteur size=(pmax.sub(pmin));
double dx=size.x();
double dy=size.y();
double dz=size.z();
//create the voxel corners:
Vecteur[][][] corner=new Vecteur[2][2][2];
for (int x=0;x<2;x++)
	for (int y=0;y<2;y++)
		for (int z=0;z<2;z++)
			corner[x][y][z]=pmin.addn(dx*x,dy*y,dz*z);

Vector<Segment3D> vertices=new Vector<Segment3D>(); 
for (int x=0;x<2;x++)
	for (int y=0;y<2;y++)
		for (int z=0;z<2;z++)
			{
			if ((1-x)>x) vertices.add(new Segment3D(corner[x][y][z],corner[1-x][y][z]));
			if ((1-y)>y) vertices.add(new Segment3D(corner[x][y][z],corner[x][1-y][z]));
			if ((1-z)>z) vertices.add(new Segment3D(corner[x][y][z],corner[x][y][1-z]));
			}
return vertices;
}


/**
 * return the intersections with the ray (pos,dir) of this voxel
 * @param pos
 * @param dir
 * @return
 */
public Vector<Vecteur> intersection(Vecteur pos,Vecteur dir)
{
////faces parallel to y
////check is ray in (y,z) plane and out of the voxel:
//if ((dir.x()==0)&&((pos.x()<pmin.x())||(pos.x()>pmax.x()))) return false;
//
//Vecteur i1=Plane.intersectionPlaneLine(pmin, new Vecteur(1,0,0), pos, dir);
//if ((i1.y()<pmin.y())||(i1.y()>pmax.y())) return false	
//	

Vector<Vecteur> v=new Vector<Vecteur>();
	
//intersection with (y,z) planes
Vecteur ixMin=Plane.intersectionPlaneLine(pmin, bx, pos, dir);
if ((ixMin!=null)
	&&(ixMin.y()>=pmin.y())&&(ixMin.y()<=pmax.y())
	&&(ixMin.z()>=pmin.z())&&(ixMin.z()<=pmax.z())) v.add(ixMin);
Vecteur ixMax=Plane.intersectionPlaneLine(pmax, bx, pos, dir);
if ((ixMax!=null)
	&&(ixMax.y()>=pmin.y())&&(ixMax.y()<=pmax.y())
	&&(ixMax.z()>=pmin.z())&&(ixMax.z()<=pmax.z())) v.add(ixMax);

//intersection with (x,z) planes
Vecteur iyMin=Plane.intersectionPlaneLine(pmin, by, pos, dir);
if ((iyMin!=null)
	&&(iyMin.x()>=pmin.x())&&(iyMin.x()<=pmax.x())
	&&(iyMin.z()>=pmin.z())&&(iyMin.z()<=pmax.z())) v.add(iyMin);
Vecteur iyMax=Plane.intersectionPlaneLine(pmax, by, pos, dir);
if ((iyMax!=null)
	&&(iyMax.x()>=pmin.x())&&(iyMax.x()<=pmax.x())
	&&(iyMax.z()>=pmin.z())&&(iyMax.z()<=pmax.z())) v.add(iyMax);

//intersection with (x,y) planes
Vecteur izMin=Plane.intersectionPlaneLine(pmin, bz, pos, dir);
if ((izMin!=null)
	&&(izMin.y()>=pmin.y())&&(izMin.y()<=pmax.y())
	&&(izMin.x()>=pmin.x())&&(izMin.x()<=pmax.x())) v.add(izMin);
Vecteur izMax=Plane.intersectionPlaneLine(pmax, bz, pos, dir);
if ((izMax!=null)
	&&(izMax.y()>=pmin.y())&&(izMax.y()<=pmax.y())
	&&(izMax.x()>=pmin.x())&&(izMax.x()<=pmax.x())) v.add(izMax);


return v;

}

@Override
public boolean isOn(Point p, Projector proj)
{
// TODO Auto-generated method stub
return false;
}


}
