package com.photonlyx.toolbox.math.geometry;


import nanoxml.XMLElement;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.analyse2D.Interpolation2D;
import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.math.function.Function2D1D;
import com.photonlyx.toolbox.math.function.usual2D1D.SphereCap;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.util.Global;

public class QuasiPlanarShape extends  Shape  implements XMLstorable
{
double ax,ay,az;
private Function2D1D function2D1D;
private Vecteur centre=new Vecteur();
private Vecteur X;//x axis of the base plane
private Vecteur Y;//y axis of the base plane
private Vecteur Z;//z axis of the base plane
private Signal1D1DXY debug1=new Signal1D1DXY();
private Signal1D1DXY debug2=new Signal1D1DXY();
private Signal1D1DXY debug3=new Signal1D1DXY();
//private double zminShape;//min altitude of shape in local frame (X,Y,Z)
//private double zmaxShape;//max altitude of shape in local frame (X,Y,Z)
private Vecteur centreMinPlane;//used for intersection3
private Vecteur centreMaxPlane;//used for intersection3
private String filename;//file raw with the control points of the shape

public  QuasiPlanarShape()
{

}

/**
 * 
 * @param x point of the plane, x
 * @param y point of the plane, y
 * @param z point of the plane, z
 * @param ax frame FIRST turned by ax radian around x vector
 * @param ay frame SECOND turned by ay radian around y vector
 * @param az frame THIRD turned by az radian around z vector
 * @param ctrl map of the z of the shape in the local frame
 */
public  QuasiPlanarShape(double x,double y,double z,double ax,double ay,double az,Signal2D1D ctrl)
{
centre=new Vecteur(x,y,z);
Rotator rot=new Rotator(ax,ay,az);
X=rot.turn(new Vecteur(1,0,0));
Y=rot.turn(new Vecteur(0,1,0));
Z=rot.turn(new Vecteur(0,0,1));
function2D1D=new Interpolation2D(ctrl);
double zminShape=ctrl.zmin();//min altitude of shape in local frame (X,Y,Z)
double zmaxShape=ctrl.zmax();//max altitude of shape in local frame (X,Y,Z)
centreMinPlane=centre.addn(Z.scmul(zminShape));
centreMaxPlane=centre.addn(Z.scmul(zmaxShape));
}

/**
 * 
* @param x point of the plane, x
 * @param y point of the plane, y
 * @param z point of the plane, z
 * @param ax frame FIRST turned by ax radian around x vector
 * @param ay frame SECOND turned by ay radian around y vector
 * @param az frame THIRD turned by az radian around z vector
 * @param f function of the shape (2D->1D)
 * @param zminShape min altitude of shape in local frame (X,Y,Z)
 * @param zmaxShape max altitude of shape in local frame (X,Y,Z)
 */
public  QuasiPlanarShape(double x,double y,double z,double ax,double ay,double az,
		Function2D1D f,double zminShape,double zmaxShape)
{
centre=new Vecteur(x,y,z);
Rotator rot=new Rotator(ax,ay,az);
X=rot.turn(new Vecteur(1,0,0));
Y=rot.turn(new Vecteur(0,1,0));
Z=rot.turn(new Vecteur(0,0,1));
function2D1D=f;
centreMinPlane=centre.addn(Z.scmul(zminShape));
centreMaxPlane=centre.addn(Z.scmul(zmaxShape));
}



@Override
/**
 * get the distance of the surface from the point pos in the direction dir. 
 * Look only from the point towards the direction dir. If the intersection is backwards
 * return 1.3e30.  
 * NOTE: start at the abcisse 0.001 (and not 0) to find the intersection 
 * @param pos 
 * @param dir
 * @return
 */
public double distance(Vecteur pos, Vecteur dir)
{
//offset=0.001  starting abscissa
//sampling=0.005  typical size of the structure of the shape
Vecteur i=getIntersection3(pos,dir,0,0.002);
//if ((i.sub(pos).mul(dir))>0) return pos.sub(i).norme(); else return 1.3e30;
if (i!=null) return pos.sub(i).norme(); else return 1.3e30;
}

@Override
public Vecteur centre()
{
return centre;
}


/**
 * normal of the surface corresponding to the point (x,y) of the base plane
 * @param x
 * @param y
 * @return
 */
public Vecteur normal(double[] xy)
{
double z=getFunction().f(xy);
//point on the surface:
Vecteur i=new Vecteur(xy[0],xy[1],z);
//project i to the base plane:
Vecteur p1=Vecteur.intersectionPlaneLine(centre, Z, i, Z);
//corresponding point on the surface:
double x=p1.sub(centre).mul(X);
double y=p1.sub(centre).mul(Y);
double der[]=function2D1D.derivate(x,y);
//local normal vector:
Vecteur n=new Vecteur(-der[0],-der[1],1).getNormalised();
return n;
}



@Override
public Vecteur normal(Vecteur i)
{
//project i to the base plane:
Vecteur p1=Vecteur.intersectionPlaneLine(centre, Z, i, Z);
//corresponding point on the surface:
double x=p1.sub(centre).mul(X);
double y=p1.sub(centre).mul(Y);
double der[]=function2D1D.derivate(x,y);
//local normal vector:
Vecteur n=new Vecteur(-der[0],-der[1],1).getNormalised();
return n;
}




/**
 * intersection of a line with the surface. Using iterative method.
 * @param pos
 * @param dir
 * @return
 */
public Vecteur getIntersection(Vecteur pos, Vecteur dir)
{
	debug1.init(0);
	debug1.addPoint(pos.x(), pos.z());
Vecteur p1,p2,p3,p4=new Vecteur(0,0,0);
//intersection of the line with the base plane:
p1=Vecteur.intersectionPlaneLine(centre, Z, pos, dir);
//coordinates in plane local frame of this point on the plane:
double[] p2D=new double[2];
p2D[0]=p1.sub(centre).mul(X);
p2D[1]=p1.sub(centre).mul(Y);
	//System.out.println("On plane: "+p1);
	debug1.addPoint(p1.x(), p1.z());
//corresponding point on the surface:
p2=new Vecteur(p2D[0],p2D[1],function2D1D.f(p2D));
	//System.out.println("On surface: "+p2);
	debug1.addPoint(p2.x(), p2.z());
	//System.out.println("Start iteration");
int i=0;
for (;;)
	{
		//System.out.println("Iteration "+i);
	//projection on the line
	p3=projectPointOnLine(p2,pos,dir);
		//System.out.println("On line:"+p3);
		debug1.addPoint(p3.x(), p3.z());
	//coordinates in plane local frame of this point on the plane:
	p2D[0]=p3.sub(centre).mul(X);
	p2D[1]=p3.sub(centre).mul(Y);
	//System.out.println("On plane: "+X.scmul(p2D[0]).addn(Y.scmul(p2D[1])));
	//corresponding point on the surface:
	p4=X.scmul(p2D[0]).addn(Y.scmul(p2D[1])).addn(Z.scmul(function2D1D.f(p2D)));
		//System.out.println("On surface: "+p4);
		debug1.addPoint(p4.x(), p4.z());
	if (p4.sub(p3).norme()<1e-6) break;
	if (i==1000) break;
	p2=p4;
	i++;
	}
//if (i==1000) 
//	{
//	System.out.println("algo 1: "+i+" iterations"+" inter="+p4);
//	//distance from p4 to the line 
//	Vecteur posP4=pos.sub(p4);
//	double dist=Math.sqrt(posP4.normeSquare()-Math.pow(posP4.mul(dir),2));
//	System.out.println("algo 1: dist of solution to line="+dist);
//	}
return p4;
}

/**
 * intersection of a line with the surface. Using iterative method.
 * @param pos
 * @param dir
 * @return
 */
public Vecteur getIntersection2(Vecteur pos, Vecteur dir)
{
debug2.init(0);
debug2.addPoint(pos.x(), pos.z());
Vecteur p1,p2,p3,p4=new Vecteur(0,0,0);
//intersection of the line with the base plane:
p1=Vecteur.intersectionPlaneLine(centre, Z, pos, dir);
//coordinates in plane local frame of this point on the plane:
double[] p2D=new double[2];
p2D[0]=p1.sub(centre).mul(X);
p2D[1]=p1.sub(centre).mul(Y);
//System.out.println("On plane: "+p1);
debug2.addPoint(p1.x(), p1.z());
//corresponding point on the surface:
p2=new Vecteur(p2D[0],p2D[1],function2D1D.f(p2D));
//System.out.println("On surface: "+p2);
debug2.addPoint(p2.x(), p2.z());
//System.out.println("Start iteration");
int i=0;
for (;;)
//for (int i=0;i<10;i++)
	{
	//System.out.println("Iteration "+i);
	//intersection with new plan parallel to base plane going through p2:
	p3=Vecteur.intersectionPlaneLine(p2, Z, pos, dir);
	//System.out.println("Intersection with new plane:"+p3);
	debug2.addPoint(p3.x(), p3.z());
	//coordinates in plane local frame of this point on the plane:
	p2D[0]=p3.sub(centre).mul(X);
	p2D[1]=p3.sub(centre).mul(Y);
	//System.out.println("On plane: "+X.scmul(p2D[0]).addn(Y.scmul(p2D[1])));
	//corresponding point on the surface:
	p4=X.scmul(p2D[0]).addn(Y.scmul(p2D[1])).addn(Z.scmul(function2D1D.f(p2D)));
	//System.out.println("On surface: "+p4);
	debug2.addPoint(p4.x(), p4.z());
	if (p4.sub(p3).norme()<1e-6) break;
	if (i==1000) break;
	p2=p4;
	i++;
	}
System.out.println("algo 2: "+i+" iterations");
return p4;
}

public Function1D1D getIntersection3Function1D1D(Vecteur pos, Vecteur dir)
{
RayIntersection rayIntersection=new RayIntersection(pos,dir);
return rayIntersection;
}



/**
 * get intersection of ray (half line from pos in direction of dir) with the surface
 * @param pos starting point of the ray
 * @param dir direction of the ray
 * @param zminShape min altitude of shape in the local frame
 * @param zmaxShape max altitude of shape in the local frame
 * @param offset the abcisses range use to find the solution is [offset,abcisse to the boundary plane]
 * @param sampling size of the function solved
 * @return
 */
public Vecteur getIntersection3(Vecteur pos, Vecteur dir,double offset,double sampling)
{
Function1D1D f=new RayIntersection(pos,dir);
//limit the range of the abcisse along the ray t between the min and max planes :
Vecteur p1=Vecteur.intersectionPlaneLine(centreMinPlane,Z , pos, dir);
Vecteur p2=Vecteur.intersectionPlaneLine(centreMaxPlane,Z , pos, dir);
double tmin=p1.sub(pos).mul(dir);
double tmax=p2.sub(pos).mul(dir);
double d;
if (tmin>tmax) 
	{
	d=tmin;
	tmin=tmax;
	tmax=d;
	}
//System.out.println("tmin: " +tmin+" tmax: " +tmax);
if (tmax>0)
	{
	int n=(int)Math.max((tmax-offset)/sampling,10);
	//System.out.println("getIntersection3 n="+n);
	SignalDiscret1D1D s=new SignalDiscret1D1D(offset,tmax,n);
	s.fillWithFunction(f);
	boolean[] found=new boolean[1];
	double t=s.solve(0, offset, true,found);
	//System.out.println("found:"+found[0]);
	if( found[0]) 
		{
		//System.out.println("sol="+pos.addn(dir.scmul(t)));
		return pos.addn(dir.scmul(t));//return point for curve abcisse t
		}
	}
return null;
}
/**
 * 
 * @param p point to project
 * @param pos point of the line
 * @param dir direction of the line
 * @return
 */
private static Vecteur projectPointOnLine(Vecteur p,Vecteur pos, Vecteur dir)
{
//System.out.println("    dir:"+dir);
//System.out.println("    p.sub(pos):"+p.sub(pos));
//System.out.println("    p.sub(pos).mul(dir):"+p.sub(pos).mul(dir));
//System.out.println("    dir.scmul(p.sub(pos).mul(dir)):"+dir.scmul(p.sub(pos).mul(dir)));
return pos.addn(dir.scmul(p.sub(pos).mul(dir)));

//return Vecteur.intersectionPlaneLine(p, dir, pos, dir);
}

public Signal1D1DXY debug1(){return debug1;}
public Signal1D1DXY debug2(){return debug2;}

public static void main(String[] args)
{
double r=3;
Function2D1D f=new SphereCap(r,0,0,-r*(1-Math.cos(Math.PI/180.0*45)));
//Function2D1D f=new com.cionin.math.function.usual2D1D.Plane(new Vecteur(0,0,1),new Vecteur(0,0,1));
QuasiPlanarShape quasiPlanarShape=new QuasiPlanarShape(0,0,0,0,0,0,f,-2,0);

Vecteur pos=new Vecteur(0,0,0);
Vecteur dir=new Vecteur(0,0,1);
Rotator rot=new Rotator(0,-Math.PI/4,0);
rot._turn(dir);

Vecteur i=quasiPlanarShape.getIntersection2(pos,dir);


}

public Function2D1D getFunction()
{
return function2D1D;
}

@Override
public Vecteur intersection(Vecteur pos, Vecteur dir)
{
return this.getIntersection(pos, dir);
}

/**
 * z of a point with respect to the surface (0 if the point is on the surface)
 * @param p
 * @return
 */
public double altitude(Vecteur p)
{
//projection of the point on the base plane:
Vecteur p1=Vecteur.intersectionPlaneLine(centre, Z, p, Z);
//coordinates in plane local frame of this point on the plane:
double[] p2D=new double[2];
p2D[0]=p1.sub(centre).mul(X);
p2D[1]=p1.sub(centre).mul(Y);
//altitude of the surface at this point:
double zs=function2D1D.f(p2D);
//altitude of the point p
double zp=p.sub(centre).mul(Z);
return (zp-zs);
}
/**
 * tells on which side of the surface the point is
 * @param p
 * @return
 */
public boolean isOnZpositives(Vecteur p)
{

if (altitude(p)>0) return true;else return false;
}



public Vecteur getX()
{
return X;
}

public Vecteur getY()
{
return Y;
}

public Vecteur getZ()
{
return Z;
}



/**
 * solve ray shape intersection with a 1D problem.
 * 
 * @author laurent
 *
 */
public class RayIntersection extends Function1D1D
{
//ray:
private Vecteur pos,dir;
//proj of ray dir on the base plane:
//private Vecteur dir2;
//private Vecteur dir2Z;
//altitude of pos:
//double zpos;

public RayIntersection (Vecteur pos, Vecteur dir)
{
this.pos=pos;
this.dir=dir;
//dir2=X.scmul(dir.mul(X)).addn(Y.scmul(dir.mul(Y)));
//dir2Z=Z.scmul(dir.mul(Z));
//zpos=pos.sub(centre).mul(Z);

}


@Override
/**
 * x pos of the point along the  ray on the base plane
 * return the diff of altitude between ray and surface
 */
public double f(double x)
	{
	Vecteur p=pos.addn(dir.scmul(x));
	//coordinates in plane local frame of this point on the plane:
	double[] p2D=new double[2];
	p2D[0]=p.sub(centre).mul(X);
	p2D[1]=p.sub(centre).mul(Y);
	//altitude of the surface at this point:
	double zs=function2D1D.f(p2D);
	//altitude of the ray at this point
	double z=p.sub(centre).mul(Z);
	//return the diff
	return z-zs;
	}

}


@Override
public XMLElement toXML()
{
XMLElement el = new XMLElement();
el.setName(getClass().getSimpleName().toString().replace("class ", ""));
el.setDoubleAttribute("x",centre.x());
el.setDoubleAttribute("y",centre.y());
el.setDoubleAttribute("z",centre.z());
el.setDoubleAttribute("ax",ax);
el.setDoubleAttribute("ay",ay);
el.setDoubleAttribute("az",az);
el.setAttribute("filename",filename);
el.setAttribute("name",getName());
return el;
}

@Override
public void updateFromXML(XMLElement xml)
{
filename=xml.getStringAttribute("filename");
Signal2D1D ctrl=new Signal2D1D();//control points of the shape:
ctrl.readRawFloatImage(Global.path+filename);
double x=xml.getDoubleAttribute("x",0);
double y=xml.getDoubleAttribute("y",0);
double z=xml.getDoubleAttribute("z",0);
ax=xml.getDoubleAttribute("ax",0);
ay=xml.getDoubleAttribute("ay",0);
az=xml.getDoubleAttribute("az",0);
centre=new Vecteur(x,y,z);
Rotator rot=new Rotator(ax,ay,az);
X=rot.turn(new Vecteur(1,0,0));
Y=rot.turn(new Vecteur(0,1,0));
Z=rot.turn(new Vecteur(0,0,1));
function2D1D=new Interpolation2D(ctrl);
double zminShape=ctrl.zmin();//min altitude of shape in local frame (X,Y,Z)
double zmaxShape=ctrl.zmax();//max altitude of shape in local frame (X,Y,Z)
centreMinPlane=centre.addn(Z.scmul(zminShape));
centreMaxPlane=centre.addn(Z.scmul(zmaxShape));
setName(xml.getStringAttribute("name",""));	
}



}
