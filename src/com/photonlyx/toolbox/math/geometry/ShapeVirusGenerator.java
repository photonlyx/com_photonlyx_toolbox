package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import javax.swing.JFileChooser;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.threeD.io.STLFileFilter;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;


/**
 * generate a sphere with picks
 * @author laurent
 *
 */
public class ShapeVirusGenerator extends WindowApp
{
public int nLon=20,nLat=20;// nb of picks in longitude and latitude
public int n=200;//sampling
public double r0=3;//radius
public double h=0.4;//pick height (radius is one)
public double latMax=Math.PI/2*0.8;
public double lonPeriod,latPeriod;//pick to pick distance in longitude and latitude

private Graph3DPanel g3d=new Graph3DPanel();
private Object3DColorSet set4obj=new Object3DColorSet();
private Object3DColorSet set4dat=new Object3DColorSet();
private ParamsBox pbOut;

public ShapeVirusGenerator()
{
super("com_photonlyx_edelweiss",true,false,false);
this.add(g3d.getJPanel(), "");
g3d.addColorSet(set4obj);
g3d.addColorSet(set4dat);

TriggerList tl=new TriggerList();
{
String[] names={"nLon","nLat","n","r0","h","latMax"};
ParamsBox pb=new ParamsBox(this,tl,null,names,null,null,ParamsBox.VERTICAL,200,28);
this.getPanelSide().add(pb.getComponent());
}	

{
String[] names={"lonPeriod","latPeriod"};
pbOut=new ParamsBox(this,null,null,names,null,null,ParamsBox.VERTICAL,200,28);
pbOut.setEditable(false);
this.getPanelSide().add(pbOut.getComponent());
}	

tl.add(new Trigger(this,"update"));
tl.add(new Trigger(g3d,"update"));
tl.add(new Trigger(pbOut,"update"));

{
TriggerList tl1=new TriggerList();
tl1.add(new Trigger(this,"saveAsSTL"));
CButton cButton=new CButton(tl1);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setText("saveAsSTL");
this.getPanelSide().add(cButton);
}


//button to import DDSCAT dat file
{
TriggerList tl_button=new TriggerList();
tl_button.add(new Trigger(this,"importDATdialog"));
tl_button.add(new Trigger(g3d,"update"));
CButton cButton=new CButton(tl_button);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setText("Import dat file");
this.getPanelSide().add(cButton);
}


this.getCJFrame().validate();

tl.trig();
g3d.update();
}



public void update()
{
set4obj.removeAll();
double lon,lat,r;
double lons=Math.PI*2/n;//sampling in longitude, rad
double lats=latMax/n;//sampling in latitude
Rotator rot=new Rotator();

Vecteur[][] mat=new Vecteur[n+1][n];

//build the points in mat:
for (int j=-n/2;j<=n/2;j++)//loop on latitude
	{
	lat=j*lats*2;
	for (int i=0;i<n;i++)//loop on longitude
		{
		lon=lons*i;
		r=r0+h*Math.sin(lon*nLon)*Math.cos(lat*nLat*Math.PI/2/latMax);
		rot.init(0,lat,lon);
		Vecteur p=new Vecteur(r,0,0);
		rot._turn(p);
		mat[j+n/2][i]=p;
		}
	}
//build the quadrilateres:
Vecteur p1,p2,p3,p4;
for (int j=0;j<n+1-1;j++)//loop on latitude
	for (int i=0;i<n;i++)//loop on longitude
		{
		p1=mat[j][i];
		p4=mat[j+1][i];
		if (i==n-1)
			{
			p2=mat[j][0];
			p3=mat[j+1][0];
			}
		else
			{
			p2=mat[j][i+1];
			p3=mat[j+1][i+1];
			}
		Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p4,p3,p2);
		set4obj.add(q);
		}
//generate top and bottom
for (int k=0;k<=n;k+=n)
	for (int i=0;i<n;i++)//loop on longitude
		{
		p1=mat[k][i];
		if (i==n-1)
			{
			p2=mat[k][0];
			}
		else
			{
			p2=mat[k][i+1];
			}
		p3=new Vecteur(0,0,mat[k][0].z());
		Triangle3DColor t;
		if (k==n) t=new Triangle3DColor(p1,p3,p2,Color.black,true);
		else t=new Triangle3DColor(p1,p2,p3,Color.black,true);
		set4obj.add(t);
		}

lonPeriod=Math.PI*r0*2/nLon;
latPeriod=4*latMax*r0/nLat;
//saveAsSTL(set,"shape.stl");
}


public void saveAsSTL()
{
JFileChooser df=new JFileChooser(Global.path);
STLFileFilter ff=new STLFileFilter();
df.addChoosableFileFilter(ff);
df.setFileFilter(ff);
int returnVal = df.showSaveDialog(null);
if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	Global.path=df.getSelectedFile().getParent()+File.separator;
	String filename=df.getSelectedFile().getName();
	if (!filename.endsWith(STLFileFilter.getExtension())) filename+="."+STLFileFilter.getExtension();
	saveAsSTL(set4obj,Global.path+filename);
	}
}

/**
 * save quads cut in 2 triangle in a STL file
 * @param set
 */
public void saveAsSTL(Object3DColorSet set,String filename)
{
if (!filename.endsWith(STLFileFilter.getExtension())) filename+="."+STLFileFilter.getExtension();
try
	{
	FileOutputStream fo = new FileOutputStream(filename);
	PrintWriter pw = new PrintWriter(fo);
	pw.print("solid Triangle3DSet\n");
	for (Object3D o:set)
		{
		if (o instanceof Quadrilatere3DColor)
			{
			Quadrilatere3DColor q=(Quadrilatere3DColor)o;
			Vecteur p1=q.p1();
			Vecteur p2=q.p2();
			Vecteur p3=q.p3();
			Vecteur p4=q.p4();
			Triangle3DColor t1=new Triangle3DColor(p1,p2,p4,q.getColor());
			Triangle3DColor t2=new Triangle3DColor(p2,p3,p4,q.getColor());
			pw.print(t1.getSTLencodeing());
			pw.print(t2.getSTLencodeing());
			}
		if (o instanceof Triangle3DColor)
			{
			Triangle3DColor t1=(Triangle3DColor)o;
			pw.print(t1.getSTLencodeing());			
			}
		}
	pw.print("endsolid\n");
	pw.flush();
	pw.close();
	fo.close();
	Global.setMessage(filename+" saved");
	}
catch (Exception e) 
	{
	Messager.messErr("ShadowCasting "+Messager.getString("Probleme_ecriture_fichier")+" "+filename);;
	}
System.out.println(filename+" saved");
}




public void importDATdialog()
{
JFileChooser df=new JFileChooser(Global.path);
CFileFilter cff=new CFileFilter("dat","DDSCAT dat file");
df.addChoosableFileFilter(cff);
df.setFileFilter(cff);
int returnVal = df.showOpenDialog(null);
if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	String path=df.getSelectedFile().getParent()+File.separator;
	String filename=df.getSelectedFile().getName();
	importDAT(path,filename);
	}	
}

public void importDAT(String path,String file)
{
System.out.println(getClass()+" "+"open: "+path+file);
String s=TextFiles.readFile(path+file,false).toString();
if (s==null) return ;

set4dat.removeAll();
String[] lines=StringSource.readLines(s);
for (int i=7;i<lines.length;i++)
	{
	//System.out.println(lines[i]);
	if (lines.length<5) continue;
	Definition def=new Definition(lines[i]);
	Vecteur v=new Vecteur(Integer.parseInt(def.word(1)),Integer.parseInt(def.word(2)),Integer.parseInt(def.word(3)));
	set4dat.add(new Point3DColor(v,Color.red,2));
	}

}

public static void main(String[] args)
{
ShapeVirusGenerator shapeGenerator =new ShapeVirusGenerator();

}

}
