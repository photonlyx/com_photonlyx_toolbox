package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.util.Iterator;
import java.util.Vector;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;

/** 
 * Class to manipulate segments in 2D
 */
public class Polygon2D implements Iterable<Vecteur2D>
{
	private Vector<Vecteur2D> p=new Vector<Vecteur2D>();
	private double xmin,xmax,ymin,ymax; //bounding box
	private boolean boundingBoxCalculated=false;

	public Polygon2D()
	{
	}

	/**return the ith point of the segment*/
	public Vecteur2D p(int i){return p.elementAt(i);}

	public void setP(int i,double x,double y)
	{
		p(i).affect(x,y);
	}

	public void add(Vecteur2D v) {p.add(v);}



	public boolean isInside( double xx, double yy) 
	{
		int n = p.size();
		boolean inside = false;

		// Loop through all edges of the polygon
		for (int i = 0, j = n - 1; i < n; j = i++) 
		{
			Vecteur2D pi=this.p(i);
			double xi = pi.x(), yi = pi.y();
			Vecteur2D pj=this.p(j);
			double xj = pj.x(), yj = pj.y();

			// Check if point (xx, yy) is on an edge or within the polygon
			boolean intersect = ((yi > yy) != (yj > yy)) &&
					(xx < (xj - xi) * (yy - yi) / (yj - yi) + xi);
			if (intersect) {
				inside = !inside;
			}
		}

		return inside;
	}

	public boolean isOutsideBoundingBox(double x,double y)
	{
		if (!boundingBoxCalculated) calcBoundingBox();	
		if (x<xmin) return true;
		if (y<ymin) return true;
		if (x>xmax) return true;
		if (y>ymax) return true;
		return false;
	}
	public void calcBoundingBox()
	{
		xmin=1e30;
		xmax=-1e30;
		ymin=1e30;
		ymax=-1e30;
		for (Vecteur2D v:p) 	
		{
			if (v.x()<xmin) xmin=v.x();
			if (v.y()<ymin) ymin=v.y();
			if (v.x()>xmax) xmax=v.x();
			if (v.y()>ymax) ymax=v.y();
		}
		boundingBoxCalculated=true;
	}


	public Vector<Segment2D> getSegments()
	{
		Vector<Segment2D> segs=new Vector<Segment2D>();
		for (int i=0;i<p.size()-1;i++)
		{
			Segment2D s=new Segment2D(p.elementAt(i),p.elementAt(i+1));
			segs.add(s);
		}
		segs.add(new Segment2D(p.elementAt(p.size()-1),p.elementAt(0)));
		return segs;
	}

	@Override
	public Iterator<Vecteur2D> iterator() 
	{
		return p.iterator();
	}

	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		for (Vecteur2D v:p)
		{
			sb.append(v+"\n");
		}
		return sb.toString();
	}


	public static void testIsInside()
	{
		Polygon2D p=new Polygon2D();
		p.add(new Vecteur2D(0.038,0.962));
		p.add(new Vecteur2D(0.162,0.962));
		p.add(new Vecteur2D(0.582,0.842));
		p.add(new Vecteur2D(0.717,0.737));
		p.add(new Vecteur2D(0.909,0.5));
		p.add(new Vecteur2D(1.038,0.162));
		p.add(new Vecteur2D(1.072,0));
		p.add(new Vecteur2D(1.038,-0.162));
		p.add(new Vecteur2D(0.909,-0.5));
		p.add(new Vecteur2D(0.717,-0.737));
		p.add(new Vecteur2D(0.582,-0.842));
		p.add(new Vecteur2D(0.162,-0.962));
		p.add(new Vecteur2D(0.038,-0.962));
		p.add(new Vecteur2D(-0.382,-0.842));
		p.add(new Vecteur2D(-0.517,-0.737));
		p.add(new Vecteur2D(-0.709,-0.5));
		p.add(new Vecteur2D(-0.838,-0.162));
		p.add(new Vecteur2D(-0.872,0));
		p.add(new Vecteur2D(-0.838,0.162));
		p.add(new Vecteur2D(-0.709,0.5));
		p.add(new Vecteur2D(-0.517,0.737));
		p.add(new Vecteur2D(-0.382,0.842));

		Signal1D1DXY sigp=new Signal1D1DXY();
		for (Vecteur2D v:p) sigp.addPoint(v.x(),v.y());
		sigp.addPoint(p.p(0).x(),p.p(0).y());
		Plot pp=new Plot(sigp);
		pp.setLineThickness(2);
		pp.setDotSize(3);
		pp.setColor(Color.BLACK);
		ChartWindow cw=new ChartWindow(pp);
		cw.getCJFrame().setExitAppOnExit(true);

		Signal1D1DXY sigin=new Signal1D1DXY();
		Plot pin=new Plot(sigin);
		pin.setLineThickness(0);
		pin.setDotSize(3);
		pin.setColor(Color.RED);
		cw.getChartPanel().getChart().add(pin);
		Signal1D1DXY sigout=new Signal1D1DXY();
		Plot pout=new Plot(sigout);
		pout.setLineThickness(0);
		pout.setDotSize(3);
		pout.setColor(Color.BLUE);
		cw.getChartPanel().getChart().add(pout);
		p.calcBoundingBox();
		double dx=p.xmax-p.xmin;
		double dy=p.ymax-p.ymin;
		double x1=p.xmin-dx;
		double y1=p.ymin-dy;
		for (int i=0;i<10000;i++)
		{
			double x=x1+Math.random()*3*dx;
			double y=y1+Math.random()*3*dy;
			if (p.isInside(x,y)) sigin.addPoint(x, y);
			else sigout.addPoint(x, y);
		}
		cw.update();

		System.out.println("Probleme, is inside: "+p.isInside(-0.8,0.00000000));
	}


	public static void main(String[] args)
	{
		testIsInside();
	}



}
