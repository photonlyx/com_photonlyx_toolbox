package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import nanoxml.XMLElement;


/**
 * rectangle (or open box) with an hole in the X axis. 
 */
public class BoxWithHole extends TriangleMeshFrame implements XMLstorable
{
	private double dx=10,dy=10;//size in x and y direction
	private int n=100;//sampling of circle
	private double r1=1;//radius of the hole1
	private double r2=1;//radius of the hole1
	private double dz=1;//size of box in the z axis

	public BoxWithHole()
	{
	}


	public void init()
	{

		{
			//front plate with hole
			DiaphragmRectangle dr=new DiaphragmRectangle();
			dr.setN(64);
			dr.setDx(dx);
			dr.setDy(dy);
			dr.setR(r1);
			dr.setH(dz);
			dr.init();
			for (Triangle3D tri:dr.getGlobal()) getLocal().add(tri.copy());
		}

		{
			//front plate with hole
			DiaphragmRectangle dr=new DiaphragmRectangle();
			dr.setN(64);
			dr.setDx(dx);
			dr.setDy(dy);
			dr.setR(r2);
			dr.setH(0);
			dr.init();
			dr.getFrame()._rotate(new Vecteur(0,Math.PI,0));
			dr.updateGlobal();
			dr.getFrame()._translate(new Vecteur(0,0,dz));
			dr.updateGlobal();
			for (Triangle3D tri:dr.getGlobal()) getLocal().add(tri.copy());
		}
		
//		{
//			//make the corners:
//			Vecteur[] corners=new Vecteur[4];
//			corners[0]=new Vecteur(dx/2,dy/2,0);//corner of the first quarter:
//			corners[1]=new Vecteur(-dx/2,dy/2,0);//corner of the 2nd quarter:
//			corners[2]=new Vecteur(-dx/2,-dy/2,0);//corner of the 3rd quarter:
//			corners[3]=new Vecteur(dx/2,-dy/2,0);//corner of the 4th quarter:
//			//make the corners:
//			Vecteur[] corners1=new Vecteur[4];
//			corners1[0]=new Vecteur(dx/2,dy/2,dz);//corner of the first quarter:
//			corners1[1]=new Vecteur(-dx/2,dy/2,dz);//corner of the 2nd quarter:
//			corners1[2]=new Vecteur(-dx/2,-dy/2,dz);//corner of the 3rd quarter:
//			corners1[3]=new Vecteur(dx/2,-dy/2,dz);//corner of the 4th quarter:
//			for (int j=0;j<4;j++) 
//			{
//				Quadrilatere3D q=new Quadrilatere3D(corners[j],corners1[j],corners1[(j+1)%4],corners[(j+1)%4]);
//				for (Triangle3D t:q.getTriangles()) getLocal().add(t.copy());
//			}
//		}


		//set global mesh:
		getGlobal().removeAllElements();
		for (Triangle3D tri:getLocal()) getGlobal().add(tri.copy());
		updateGlobal();
	}


	public int getN() {
		return n;
	}


	public void setN(int n) {
		this.n = n;
	}


	public double getDx() {
		return dx;
	}


	public void setDx(double dx) {
		this.dx = dx;
	}


	public double getDy() {
		return dy;
	}


	public void setDy(double dy) {
		this.dy = dy;
	}


	public double getR1() {
		return r1;
	}




	public double getR2() {
		return r2;
	}


	public void setR2(double r2) {
		this.r2 = r2;
	}


	public void setR1(double r1) {
		this.r1 = r1;
	}


	public double getDz() {
		return dz;
	}


	public void setDz(double dz) {
		this.dz = dz;
	}



	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().getSimpleName().toString().replace("class ", ""));
		el.setAttribute("dx",dx);
		el.setAttribute("dy",dy);
		el.setAttribute("n",getN());
		el.setAttribute("r1",r1);
		el.setAttribute("r2",r2);
		el.setAttribute("h",dz);
		return el;
	}

	@Override
	public void updateFromXML(XMLElement xml)
	{
		dx=xml.getDoubleAttribute("dx",10);	
		dy=xml.getDoubleAttribute("dy",10);	
		setN(xml.getIntAttribute("n",16));	
		r1=xml.getDoubleAttribute("r2",1);	
		r2=xml.getDoubleAttribute("r2",1);	
		dz=xml.getDoubleAttribute("h",1);	
	}


	public static void main(String[] args) 
	{
		//test prg:
		BoxWithHole bx=new BoxWithHole();
		if (args.length==0)
		{

			bx.setN(64);
			bx.setDx(3);
			bx.setDy(3);
			bx.setDz(3);
			bx.setR1(1);
			bx.setR2(0);
			bx.init();
			//			//rotate by -90 degrees (-PI/2 rad) around y axis
			//			dr.getFrame()._rotate(new Vecteur(0,-Math.PI/2,0));
			//			dr.getFrame()._translate(new Vecteur(2,0,0));
			bx.updateGlobal();




			//add directly to the graph 3D:
			Graph3DPanel graph3DPanel=new Graph3DPanel();
			//graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
			graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);
			graph3DPanel.setRender(false);
			graph3DPanel.add(bx.getTriangles());

			//create frame and add the panel 3D
			CJFrame cjf=new CJFrame();
			cjf.getContentPane().setLayout(new BorderLayout());
			cjf.setSize(600, 400);
			cjf.setVisible(true);
			cjf.add(graph3DPanel.getComponent());
		}



	}



}





