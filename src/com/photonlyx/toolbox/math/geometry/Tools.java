package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3D;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;

public class Tools 
{
	private static NumberFormat df9 = NumberFormat.getInstance(Locale.ENGLISH);
	
	
	static
	{
		df9.setMaximumFractionDigits(6);
		df9.setMinimumFractionDigits(6);
		df9.setGroupingUsed(false);
	}





	private static void putVecteur(ByteBuffer buffer,Vecteur v)
	{
		buffer.putFloat((float)v.x());
		buffer.putFloat((float)v.y());
		buffer.putFloat((float)v.z());
	}

	public static void addTriangle(BufferedOutputStream bw,Triangle3D t1)
	{
		ByteBuffer buffer=ByteBuffer.allocate((12*4+2));
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		//normal
		putVecteur(buffer,t1.getNormal());
		//vertex 1
		putVecteur(buffer,t1.p1());
		//vertex 2
		putVecteur(buffer,t1.p2());
		//vertex 3
		putVecteur(buffer,t1.p3());

		//attribute byte count
		buffer.put((byte)0);
		buffer.put((byte)0);

		try
		{
			bw.write(buffer.array());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}



	/**
	 * test binary STL with one triangle using TriangleMesh
	 * @param path
	 * @param file
	 */
	public static void testSTLOneTriangleBinary1(String path,String file)
	{
		TriangleMesh m=new TriangleMesh();
		Vecteur p1=new Vecteur(0,0,0);
		Vecteur p2=new Vecteur(1,0,0);
		Vecteur p3=new Vecteur(0,1,0);
		m.add(new Triangle3D(p1,p2,p3));
		m.saveToBinarySTL(path+File.separator+file);
	}


	/**
	 * test binary STL with one triangle direct writing
	 * @param path
	 * @param file
	 */
	public static void testSTLOneTriangleBinary(String path,String file)
	{
		int nbTriangles=1;
		String stlfullfilename=path+File.separator+file;
		BufferedOutputStream bw;
		try
		{
			bw=new BufferedOutputStream(new FileOutputStream(stlfullfilename));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		ByteBuffer buffer=ByteBuffer.allocate(80+4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.put(new byte[80]);
		buffer.putInt(nbTriangles);
		try
		{
			bw.write(buffer.array());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		Vecteur p1=new Vecteur(0,0,0);
		Vecteur p2=new Vecteur(1,0,0);
		Vecteur p3=new Vecteur(0,1,0);
		addTriangle(bw,new Triangle3D(p1,p2,p3));

		try
		{
			bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * convert an image of altitudes in a triangles set
	 * @param im
	 * @param set
	 * @param boxThickness if not zero create a box with upper surface sculpted with the image
	 */
	public static void convertRawImage2STLbinaryWithoutMemory(Signal2D1D im,String path,String txtfile,
			double zBase,boolean createBorders,boolean createFloor)
	{
		String stlfullfilename=path+File.separator+txtfile.replaceAll(".txt",".stl");
		int nbTriangles=im.dimx()*im.dimy()*2;


		BufferedOutputStream bw;
		try
		{
			bw=new BufferedOutputStream(new FileOutputStream(stlfullfilename));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		ByteBuffer buffer=ByteBuffer.allocate(80+4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.put(new byte[80]);
		buffer.putInt(nbTriangles);
		try
		{
			bw.write(buffer.array());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		double sx=((im.xmax()-im.xmin())/im.dimx());
		double sy=((im.ymax()-im.ymin())/im.dimy());
		for (int i=0;i<im.dimx()-1;i++)
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=im.xmin()+i*sx;
				double y=im.ymin()+j*sy;
				double z1=im.z(i, j);
				double z2=im.z(i+1, j);
				double z3=im.z(i+1, j+1);
				double z4=im.z(i, j+1);
				Vecteur p1=new Vecteur(x,y,z1);
				Vecteur p2=new Vecteur(x+sx,y,z2);
				Vecteur p3=new Vecteur(x+sx,y+sy,z3);
				Vecteur p4=new Vecteur(x,y+sy,z4);
				Triangle3DColor t1=new Triangle3DColor(p1,p2,p4,Color.black,true);
				Triangle3DColor t2=new Triangle3DColor(p2,p3,p4,Color.black,true);
				addTriangle(bw,t1);
				addTriangle(bw,t2);
			}

		if (createBorders) 
		{
			Signal2D1D imShapePostProcessed=im;

			//add the edges of a box having the shape as its upper face:
			//face front
			for (int i=0;i<im.dimx()-1;i++)
			{
				double x=imShapePostProcessed.xmin()+i*sx;
				double y=imShapePostProcessed.ymin()+0*sy;
				double z1=imShapePostProcessed.z(i, 0);
				double z2=imShapePostProcessed.z(i+1, 0);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x+sx,y,zBase);
				Vecteur p3=new Vecteur(x+sx,y,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				Triangle3D t1=new Triangle3D(p1,p2,p3);
				Triangle3D t2=new Triangle3D(p1,p3,p4);
				//				System.out.println("front t1 n="+t1.calcNormal());
				//				System.out.println("front t2 n="+t2.calcNormal());
				addTriangle(bw,t1);
				addTriangle(bw,t2);
			}
			//face back:
			for (int i=0;i<im.dimx()-1;i++)
			{
				double x=imShapePostProcessed.xmin()+i*sx;
				double y=imShapePostProcessed.ymin()+(im.dimy()-1)*sy;
				double z1=imShapePostProcessed.z(i, im.dimy()-1);
				double z2=imShapePostProcessed.z(i+1, im.dimy()-1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x+sx,y,zBase);
				Vecteur p3=new Vecteur(x+sx,y,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				Triangle3D t1=new Triangle3D(p1,p3,p2);
				Triangle3D t2=new Triangle3D(p1,p4,p3);
				//				System.out.println("back t1 n="+t1.calcNormal());
				//				System.out.println("back t2 n="+t2.calcNormal());
				addTriangle(bw,t1);
				addTriangle(bw,t2);
			}
			//face left:
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=imShapePostProcessed.xmin()+0*sx;
				double y=imShapePostProcessed.ymin()+j*sy;
				double z1=imShapePostProcessed.z(0, j);
				double z2=imShapePostProcessed.z(0, j+1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x,y+sy,zBase);
				Vecteur p3=new Vecteur(x,y+sy,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				Triangle3D t1=new Triangle3D(p1,p3,p2);
				Triangle3D t2=new Triangle3D(p1,p4,p3);
				//				System.out.println("left t1 n="+t1.calcNormal());
				//				System.out.println("left t2 n="+t2.calcNormal());
				addTriangle(bw,t1);
				addTriangle(bw,t2);
			}
			//face right:
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=imShapePostProcessed.xmin()+(im.dimx()-1)*sx;
				double y=imShapePostProcessed.ymin()+j*sy;
				double z1=imShapePostProcessed.z(im.dimx()-1, j);
				double z2=imShapePostProcessed.z(im.dimx()-1, j+1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x,y+sy,zBase);
				Vecteur p3=new Vecteur(x,y+sy,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				Triangle3D t1=new Triangle3D(p1,p3,p2);
				Triangle3D t2=new Triangle3D(p1,p4,p3);
				//				System.out.println("right t1 n="+t1.calcNormal());
				//				System.out.println("right t2 n="+t2.calcNormal());
				addTriangle(bw,t1);
				addTriangle(bw,t2);
			}
			if (createFloor)
			{
				//create face bellow
				double x1=imShapePostProcessed.xmin()+(0)*sx;
				double y1=imShapePostProcessed.ymin()+(0)*sy;
				double x2=imShapePostProcessed.xmin()+(im.dimx()-1)*sx;
				double y2=imShapePostProcessed.ymin()+(0)*sy;
				double x3=imShapePostProcessed.xmin()+(im.dimx()-1)*sx;
				double y3=imShapePostProcessed.ymin()+(im.dimy()-1)*sy;
				double x4=imShapePostProcessed.xmin()+(0)*sx;
				double y4=imShapePostProcessed.ymin()+(im.dimy()-1)*sy;
				Vecteur p1=new Vecteur(x1,y1,zBase);
				Vecteur p2=new Vecteur(x2,y2,zBase);
				Vecteur p3=new Vecteur(x3,y3,zBase);
				Vecteur p4=new Vecteur(x4,y4,zBase);
				Triangle3D t1=new Triangle3D(p1,p3,p2);
				Triangle3D t2=new Triangle3D(p1,p4,p3);
				//				System.out.println("t1 n="+t1.calcNormal());
				//				System.out.println("t2 n="+t2.calcNormal());
				addTriangle(bw,t1);
				addTriangle(bw,t2);
			}
		}

		try
		{
			bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		System.out.println(stlfullfilename+" saved");

	}

	public static void addTriangleascii(PrintWriter pw,Triangle3D t)
	{
		t.calcNormal();
		Vecteur n=t.getNormal();
		pw.write("facet normal "+n.x()+" "+n.y()+" "+n.z()+"\n");
		pw.write("outer loop\n");
		pw.write("\tvertex "+df9.format(t.p1().x())+" "+df9.format(t.p1().y())+" "+df9.format(t.p1().z())+"\n");
		pw.write("\tvertex "+df9.format(t.p2().x())+" "+df9.format(t.p2().y())+" "+df9.format(t.p2().z())+"\n");
		pw.write("\tvertex "+df9.format(t.p3().x())+" "+df9.format(t.p3().y())+" "+df9.format(t.p3().z())+"\n");
		pw.write("endloop\n");
		pw.write("endfacet\n");
	}


	/**
	 * convert an image of altitudes in a triangles set
	 * if (zsurf-zBase)>0 create a box with upper surface sculpted with the image
	 * the surface is centered to (x,y)=(0,0)
	 * @param im
	 * @param path
	 * @param stlfilename
	 * @param zBase altitude of the base
	 * @param zsurf altitude of the surface
	 * @param createBorders
	 * @param createFloor
	 */
	public static void convertRawImage2STLasciiWithoutMemory(Signal2D1D im,String path,String stlfilename,
			double zBase,boolean createBorders,boolean createFloor)
	{
		String stlfullfilename=path+File.separator+stlfilename;

		//translate image to z=boxThickness and center it
		//		im._translate(-(im.xmax()-im.xmin())/2, -(im.ymax()-im.ymin())/2, zsurf);

		PrintWriter bw ;
		FileOutputStream fo;
		try
		{
			fo = new FileOutputStream(stlfullfilename);
			bw = new PrintWriter(fo);
		}
		catch (Exception e) 
		{
			Messager.messErr("TextFiles "+Messager.getString("Probleme_ecriture_fichier")+" "+stlfullfilename);
			System.out.println("TextFiles "+Messager.getString("Probleme_ecriture_fichier")+" "+stlfullfilename);
			e.printStackTrace();
			return;
		}

		bw.write("solid Triangle3DSet\n");

		double sx=((im.xmax()-im.xmin())/im.dimx());
		double sy=((im.ymax()-im.ymin())/im.dimy());
		for (int i=0;i<im.dimx()-1;i++)
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=im.xmin()+i*sx;
				double y=im.ymin()+j*sy;
				double z1=im.z(i, j);
				double z2=im.z(i+1, j);
				double z3=im.z(i+1, j+1);
				double z4=im.z(i, j+1);
				Vecteur p1=new Vecteur(x,y,z1);
				Vecteur p2=new Vecteur(x+sx,y,z2);
				Vecteur p3=new Vecteur(x+sx,y+sy,z3);
				Vecteur p4=new Vecteur(x,y+sy,z4);
				Triangle3DColor t1=new Triangle3DColor(p1,p2,p4,Color.black,true);
				Triangle3DColor t2=new Triangle3DColor(p2,p3,p4,Color.black,true);
				addTriangleascii(bw,t1);
				addTriangleascii(bw,t2);
			}

		if (createBorders) 
		{
			//add the edges of a box having the shape as its upper face:
			//face front
			for (int i=0;i<im.dimx()-1;i++)
			{
				double x=im.xmin()+i*sx;
				double y=im.ymin()+0*sy;
				double z1=im.z(i, 0);
				double z2=im.z(i+1, 0);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x+sx,y,zBase);
				Vecteur p3=new Vecteur(x+sx,y,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				Triangle3D t1=new Triangle3D(p1,p2,p3);
				Triangle3D t2=new Triangle3D(p1,p3,p4);
				//				System.out.println("front t1 n="+t1.calcNormal());
				//				System.out.println("front t2 n="+t2.calcNormal());
				addTriangleascii(bw,t1);
				addTriangleascii(bw,t2);
			}
			//face back:
			for (int i=0;i<im.dimx()-1;i++)
			{
				double x=im.xmin()+i*sx;
				double y=im.ymin()+(im.dimy()-1)*sy;
				double z1=im.z(i, im.dimy()-1);
				double z2=im.z(i+1, im.dimy()-1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x+sx,y,zBase);
				Vecteur p3=new Vecteur(x+sx,y,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				Triangle3D t1=new Triangle3D(p1,p3,p2);
				Triangle3D t2=new Triangle3D(p1,p4,p3);
				//				System.out.println("back t1 n="+t1.calcNormal());
				//				System.out.println("back t2 n="+t2.calcNormal());
				addTriangleascii(bw,t1);
				addTriangleascii(bw,t2);
			}
			//face left:
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=im.xmin()+0*sx;
				double y=im.ymin()+j*sy;
				double z1=im.z(0, j);
				double z2=im.z(0, j+1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x,y+sy,zBase);
				Vecteur p3=new Vecteur(x,y+sy,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				Triangle3D t1=new Triangle3D(p1,p3,p2);
				Triangle3D t2=new Triangle3D(p1,p4,p3);
				//				System.out.println("left t1 n="+t1.calcNormal());
				//				System.out.println("left t2 n="+t2.calcNormal());
				addTriangleascii(bw,t1);
				addTriangleascii(bw,t2);
			}
			//face right:
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=im.xmin()+(im.dimx()-1)*sx;
				double y=im.ymin()+j*sy;
				double z1=im.z(im.dimx()-1, j);
				double z2=im.z(im.dimx()-1, j+1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x,y+sy,zBase);
				Vecteur p3=new Vecteur(x,y+sy,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				Triangle3D t1=new Triangle3D(p1,p3,p2);
				Triangle3D t2=new Triangle3D(p1,p4,p3);
				//				System.out.println("right t1 n="+t1.calcNormal());
				//				System.out.println("right t2 n="+t2.calcNormal());
				addTriangleascii(bw,t1);
				addTriangleascii(bw,t2);
			}
		}
		if (createFloor)
		{
			//create face bellow
			double x1=im.xmin()+(0)*sx;
			double y1=im.ymin()+(0)*sy;
			double x2=im.xmin()+(im.dimx()-1)*sx;
			double y2=im.ymin()+(0)*sy;
			double x3=im.xmin()+(im.dimx()-1)*sx;
			double y3=im.ymin()+(im.dimy()-1)*sy;
			double x4=im.xmin()+(0)*sx;
			double y4=im.ymin()+(im.dimy()-1)*sy;
			Vecteur p1=new Vecteur(x1,y1,zBase);
			Vecteur p2=new Vecteur(x2,y2,zBase);
			Vecteur p3=new Vecteur(x3,y3,zBase);
			Vecteur p4=new Vecteur(x4,y4,zBase);
			Triangle3D t1=new Triangle3D(p1,p3,p2);
			Triangle3D t2=new Triangle3D(p1,p4,p3);
			//				System.out.println("t1 n="+t1.calcNormal());
			//				System.out.println("t2 n="+t2.calcNormal());
			addTriangleascii(bw,t1);
			addTriangleascii(bw,t2);
		}

		try
		{
			bw.write("endsolid\n");
			bw.close();
			bw.flush();
			bw.close();
			fo.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		System.out.println(stlfullfilename+" saved");

	}




	public static void testSTLbinary(String path)
	{
		//		Signal2D1D	im=new Signal2D1D(2,2);
		//		for (int i=0;i<im.dimx();i++)
		//			for (int j=0;j<im.dimy();j++)
		//				im.setValue(i, j, 0);
		//		//ChartWindow cw=new ChartWindow(im);

		Signal2D1D	im=new Signal2D1D();
		im.readRawFloatImage(path+File.separator+"covabeads_top.raw");
		im._crop(0, 0, 30, 30);
		ChartWindow cw=new ChartWindow(im);
		{
			String file="shapeNoMemoryBinary.stl";
			convertRawImage2STLbinaryWithoutMemory(im,path,file,0,true,false);
			Util.runCommand("f3d "+path+File.separator+file);
		}
		{
			//another calc to compare with:
			String file="shape.stl";
			TriangleMesh mesh=TriangleMesh.convert(im,0,false);
			mesh.saveToSTLascii(path+File.separator+file,true);
			Util.runCommand("f3d "+path+File.separator+file);
		}

	}

	public static void testSTLascii(String path)
	{
		//		Signal2D1D	im=new Signal2D1D(0,10,0,10,100,100);
		//		double sx=(im.xmax()-im.xmin())/(im.dimx()-1);
		//		double sy=(im.ymax()-im.ymin())/(im.dimy()-1);
		//		for (int i=0;i<im.dimx();i++)
		//			for (int j=0;j<im.dimy();j++)
		//			{
		//				double x=im.xmin()+i*sx;
		//				double y=im.ymin()+j*sy;
		//				double z=Math.sin(x)*Math.sin(y);
		//				im.setValue(i, j, z);
		//			}
		Signal2D1D	im=new Signal2D1D();
		im.readRawFloatImage(path+File.separator+"covabeads_top.raw");
		im._crop(0, 0, 50, 50);
		ChartWindow cw=new ChartWindow(im);

		{
			String file="shapeNoMemory.stl";
			convertRawImage2STLasciiWithoutMemory(im,path,file,0,true,false);
			Util.runCommand("f3d "+path+File.separator+file);
		}

		{
			//another calc to compare with:
			String file="shape.stl";
			TriangleMesh mesh=TriangleMesh.convert(im,0,false);
			mesh.saveToSTLascii(path+File.separator+file,true);
			Util.runCommand("f3d "+path+File.separator+file);
		}
	}


	public static void main(String args[])
	{
		String path;
		path="/home/laurent/temp/spheresBunch/";
		testSTLascii(path);
		//testSTLbinary(path);
	}







}
