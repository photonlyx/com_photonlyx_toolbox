package com.photonlyx.toolbox.math.geometry;

import java.util.Vector;

import com.photonlyx.toolbox.math.signal.Signal1D1DXY;

public class Circle2D 
{
private Vecteur2D centre;
private double r;//radius

public Circle2D(double xc,double yc, double r)
{
centre=new Vecteur2D(xc,yc);
this.r=r;	
}

public Circle2D(Vecteur2D centre, double r)
{
this.centre=centre;	
this.r=r;	
}

public static Circle2D getCircleFrom3Points(Vecteur2D p1,Vecteur2D p2,Vecteur2D p3)
{
Line2D l1=new Segment2D(p1,p2).getBisectorLine();
Line2D l2=new Segment2D(p2,p3).getBisectorLine();
Vecteur2D i=l1.getIntersectionPointWith(l2);
double radius=p1.subn(i).norme();
return new Circle2D(i,radius);
}

/**
 * get the circles transformed in a list of segements
 * @param n
 * @return
 */
public Vector<Segment2D> getSegments(int n)
{
	Vector<Segment2D> l=new Vector<Segment2D>(); 
	double xc=centre.x();
	double yc=centre.y();
	for (int i=0;i<n;i++)
	{
		double angle1=Math.PI*2*i/(double)n;
		double x1=xc+Math.cos(angle1)*r;
		double y1=yc+Math.sin(angle1)*r;
		double angle2=Math.PI*2*(i+1)/(double)n;
		double x2=xc+Math.cos(angle2)*r;
		double y2=yc+Math.sin(angle2)*r;
		Segment2D s=new Segment2D(new Vecteur2D(x1,y1),new Vecteur2D(x2,y2));
		l.add(s);
	}
	return l;
}

public Polygon2D getPolygon(int n)
{
	Polygon2D pol=new Polygon2D(); 
	double xc=centre.x();
	double yc=centre.y();
	for (int i=0;i<n;i++)
	{
		double angle1=Math.PI*2*i/(double)n;
		double x1=xc+Math.cos(angle1)*r;
		double y1=yc+Math.sin(angle1)*r;
		pol.add(new Vecteur2D(x1,y1));
	}
	return pol;
}

public  Signal1D1DXY createPolygon(int n)
{
	double x=this.getX();
	double y=this.getY();
	double d=this.getD();
	Signal1D1DXY sig=new Signal1D1DXY();
	for (int i=0;i<=n;i++)
	{
		double a=Math.PI*2/n*i;
		double x1=x+d/2*Math.cos(a);
		double y1=y+d/2*Math.sin(a);
		sig.addPoint(x1,y1);
	}
	return sig;
}

public double getD() {
	return r*2;
}

public double getR() {
	return r;
}

public double getX() {
	return centre.x();
}

public double getY() {
	return centre.y();
}


}
