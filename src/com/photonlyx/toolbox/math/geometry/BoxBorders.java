package com.photonlyx.toolbox.math.geometry;

import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;

public class BoxBorders  extends TriangleMeshFrame 
{

	/**
	 * create 4 quads defining the borders of a layer
	 * from x to dx in depth, size dy x sz in the y z plane.
	 * @param x x coord of the 
	 * @param dx
	 * @param dy
	 * @param dz
	 */
	public BoxBorders(double x,double dx,double dy,double dz)
	{
		TriangleMesh tris=super.getLocal();
		{
			Vecteur v1=new Vecteur(x,-dy/2,-dz/2);
			Vecteur v2=new Vecteur(x+dx,-dy/2,-dz/2);
			Vecteur v3=new Vecteur(x+dx,-dy/2,dz/2);
			Vecteur v4=new Vecteur(x,-dy/2,dz/2);
			Quadrilatere3D q=new Quadrilatere3D(v1,v2,v3,v4);
			tris.add(q.getTriangles());
		}
		{
			Vecteur v1=new Vecteur(x,dy/2,-dz/2);
			Vecteur v2=new Vecteur(x+dx,dy/2,-dz/2);
			Vecteur v3=new Vecteur(x+dx,dy/2,dz/2);
			Vecteur v4=new Vecteur(x,dy/2,dz/2);
			Quadrilatere3D q=new Quadrilatere3D(v1,v2,v3,v4);
			tris.add(q.getTriangles());
		}
		{
			Vecteur v1=new Vecteur(x   ,-dy/2,-dz/2);
			Vecteur v2=new Vecteur(x+dx,-dy/2,-dz/2);
			Vecteur v3=new Vecteur(x+dx,dy/2 ,-dz/2);
			Vecteur v4=new Vecteur(x   ,dy/2 ,-dz/2);
			Quadrilatere3D q=new Quadrilatere3D(v1,v2,v3,v4);
			tris.add(q.getTriangles());
		}
		{
			Vecteur v1=new Vecteur(x   ,-dy/2,dz/2);
			Vecteur v2=new Vecteur(x+dx,-dy/2,dz/2);
			Vecteur v3=new Vecteur(x+dx, dy/2,dz/2);
			Vecteur v4=new Vecteur(x   , dy/2,dz/2);
			Quadrilatere3D q=new Quadrilatere3D(v1,v2,v3,v4);
			tris.add(q.getTriangles());
		}
		for (Triangle3D tri:getLocal()) getGlobal().add(tri.copy());
		this.updateGlobal();
	}
	
	
	public static void main(String[] args) 
	{
	
	
	
		//the borders of the cosmetic product layer:
		BoxBorders borders=new BoxBorders(0,0.1,0.2,0.2);
		borders.getFrame()._rotate(new Vecteur(0,-Math.PI/2,0));
		borders.updateGlobal();
		
		Graph3DWindow w=new Graph3DWindow(borders);
	
	
	}

}
