package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;



public class Quadrilatere3D implements Object3D
{
	protected Vecteur[] p=new Vecteur[4];//positions of vertices in global coordinates
	private Vecteur[] pLocal=new Vecteur[4];
	private Vecteur centre;
	private double[][] cooScreen=new double[4][2];
	public  double x,y,z,ax,ay,az;//position of the object


	public Quadrilatere3D()
	{
		p=new Vecteur[4];
		for (int i=0;i<4;i++) pLocal[i]=new Vecteur();	
		for (int i=0;i<4;i++) p[i]=new Vecteur();	
	}

	public Quadrilatere3D(Vecteur _p1,Vecteur _p2,Vecteur _p3,Vecteur _p4)
	{
		pLocal[0]=_p1;	
		pLocal[1]=_p2;	
		pLocal[2]=_p3;	
		pLocal[3]=_p4;	
		update();
		calcCentre();
	}

	public TriangleMesh getTriangles()
	{
		TriangleMesh tris=new TriangleMesh();
		tris.removeAllElements();
		tris.add(new Triangle3D(p1(),p2(),p3()));
		tris.add(new Triangle3D(p1(),p3(),p4()));
		return tris;
	}


	public void update()
	{
		calcGlobal();
	}

	public void calcGlobal()
	{
		Vecteur o=new Vecteur(x,y,z);
		for (int j=0;j<4;j++) p[j]=pLocal[j].addn(o);
	}

	public Vecteur p1(){return p[0];}
	public Vecteur p2(){return p[1];}
	public Vecteur p3(){return p[2];}
	public Vecteur p4(){return p[3];}

	public Vecteur p(int i){return p[i];}

	public void calcCentre()
	{
		if (centre==null) 
		{
			centre=p[0].addn(p[1]).addn(p[2]).addn(p[3]).scmul(0.25);
		}
	}

	/**if one of the is out of the cube, extends the cube*/
	public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax)
	{
		for (int j=0;j<4;j++)
		{
			for (int k=0;k<3;k++) 
			{
				double r=p[j].coord(k);
				if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
				if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
			}	
		}
	}

	public Vecteur getNormal()
	{
		Triangle3D t=new Triangle3D(p[0],p[1],p[2]);
		return t.getNormal();
	}


	/**
	 * draw the quad 
	 * @param g
	 * @param proj
	 */
	public void draw(Graphics g,Projector proj)
	{
		Polygon pol=new Polygon();
		//System.out.println("quad filled="+filled+" sun="+sun);
		for (int j=0;j<4;j++) 
		{
			cooScreen[j]=proj.calcCoorEcranDouble(p[j]);
			pol.addPoint((int)cooScreen[j][0],(int)cooScreen[j][1]);
		}
		g.drawPolygon(pol);


	}



	public double getDistanceToScreen(Projector proj)
	{
		return 	proj.zCoord(centre);
	}

	

	public Vecteur getCentre()
	{
		return centre;
	}

	public void setCentre(Vecteur centre)
	{
		this.centre = centre;
	}

	


	@Override
	public boolean isOn(Point p, Projector proj)
	{
		double[] point=new double[2];
		for (int j=0;j<3;j++) 
		{
			point[0]=p.x;
			point[1]=p.y;
			//distance from point to segment:
			double dist=Segment2D.segdist2D(cooScreen[j], cooScreen[j+1], point);
			if (dist<5) return true;
		}
		return false;
	}

	public void _translate(Vecteur v)
	{
		x+=v.x();
		y+=v.y();
		z+=v.z();
		this.calcGlobal();
	}

	
	public Vector<Triangle3D> transformIn2Triangles()
	{
		Vector<Triangle3D> v=new Vector<Triangle3D>();
		Vecteur p1=this.p1();
		Vecteur p2=this.p2();
		Vecteur p3=this.p3();
		Vecteur p4=this.p4();
		Triangle3D t1=new Triangle3D(p1,p2,p4);
		Triangle3D t2=new Triangle3D(p2,p3,p4);
		v.add(t2);
		v.add(t1);
		return v;
	}



	public static void main(String[] args) 
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		graph3DPanel.getJPanel().setBackground(Color.DARK_GRAY);
		Object3DColorSet set=new Object3DColorSet();
		graph3DPanel.addColorSet(set);
		
		{
		Vecteur p1,p2,p3,p4;
		p1=new Vecteur(1,1,0);
		p2=new Vecteur(-1,1,0);
		p3=new Vecteur(-1,-1,0);
		p4=new Vecteur(1,-1,0);
		Quadrilatere3D q=new Quadrilatere3D(p1,p2,p3,p4);
		Quadrilatere3DColor qc=new Quadrilatere3DColor(q,Color.WHITE);
		//qc.setFilled(true);
		set.add(qc);
		}
		
		{
		Vecteur p1,p2,p3,p4;
		p1=new Vecteur(3,1,1);
		p2=new Vecteur(1,1,1);
		p3=new Vecteur(1,-1,0);
		p4=new Vecteur(3,-1,0);
		Quadrilatere3D q=new Quadrilatere3D(p1,p2,p3,p4);
		Quadrilatere3DColor qc=new Quadrilatere3DColor(q,Color.WHITE);
		//qc.setFilled(true);
		set.add(qc);
		}
	
		
		
		
		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
	}

}
