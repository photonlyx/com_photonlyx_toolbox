package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.math.geometry.cloud.Cloud;
import com.photonlyx.toolbox.math.geometry.cloud.CloudPoint;
import com.photonlyx.toolbox.math.geometry.cloud.DelaunayTriangulator;
import com.photonlyx.toolbox.math.geometry.cloud.IntArray;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;

public class SphericalCap implements Object3DFrame,TriangleMeshSource
{
	private double r=1;//sphere radius , infinite if r=0
	private double dCap=1;//diameter of the cap
	private int dim=20;//nb of sampling points
	private TriangleMesh localMesh=new TriangleMesh();
	private TriangleMesh globalMesh=new TriangleMesh();
	private boolean randomMesh=true;
	private Frame frame=new Frame();//local frame 

	public SphericalCap()
	{
		init();
	}

	public SphericalCap(double r,double dCap,int dim)
	{
		this.r=r;
		this.dCap=dCap;
		this.dim=dim;
		init();
	}

	public SphericalCap(Vecteur o,double r,double dCap,int dim)
	{
		this.frame.setCentre(o);
		this.r=r;
		this.dCap=dCap;
		this.dim=dim;
		init();
	}
	public SphericalCap(Vecteur o,double r,double dCap,int dim,boolean randomMesh)
	{
		this.frame.setCentre(o);
		this.r=r;
		this.dCap=dCap;
		this.dim=dim;
		this.randomMesh=randomMesh;
		init();
	}

	/**
	 * recalc mesh
	 */
	public void init()
	{
		localMesh.removeAllElements();
		globalMesh.removeAllElements();
		
		Cloud cloud=new Cloud();
		//generate points of the circle
		double dAlpha=Math.PI*2/dim;
		double z=getDepth();
		for (int i=0;i<dim;i++) 
		{
			Vecteur p=new Vecteur(dCap/2*Math.cos(i*dAlpha),dCap/2*Math.sin(i*dAlpha),z);
			cloud.add(new CloudPoint(p));
		}

		//generate random points inside the circle:
		if (randomMesh)
		{
			Vecteur tr=new Vecteur(-dCap/2,-dCap/2,0);
			double r2=dCap/2*dCap/2;
			for (int i=0;i<dim*dim;i++) 
			{
				Vecteur p=new Vecteur(Math.random(),Math.random(),0);
				//transfer from 0,1 square to -r +r square:
				p._scmul(2*dCap/2);
				p._add(tr );
				if (p.normeSquare()<r2)
				{
					double r1=p.norme();
					if (r!=0)
					{
						double ang=Math.asin(r1/r);
						z=-r*(1-Math.cos(ang));
					}
					else
					{
						z=0;
					}
					p.setZ(z);
					cloud.add(new CloudPoint(p));
				}
			}
		}
		else //points of mesh are in a regular grid
		{
			double dx=dCap/dim;
			double r2=dCap/2*dCap/2;
			for (int i=0;i<dim;i++) 
				for (int j=0;j<dim;j++)
				{
					Vecteur p=new Vecteur(-dCap/2+i*dx,-dCap/2+j*dx,0);
					if (p.normeSquare()<r2)
					{
						double r1=p.norme();
						if (r!=0)
						{
							double ang=Math.asin(r1/r);
							z=-r*(1-Math.cos(ang));
						}
						else z=0;
						p.setZ(z);
						cloud.add(new CloudPoint(p));
					}
				}

		}


		//prepare data for Delaunay triangulation in plane XY:
		int n=cloud.size();
		cloud.sortByXCoordinate();//required by delaunay triangulation algorithm
		float[] points=new float[n*2];
		for (int i=0;i<n;i++) 
		{
			points[2*i]=(float)cloud.elementAt(i).getP().x();
			points[2*i+1]=(float)cloud.elementAt(i).getP().y();
		}

		DelaunayTriangulator dt=new DelaunayTriangulator();
		IntArray triangles=dt.computeTriangles(points, 0, points.length, true);

		localMesh.removeAllElements();
		for (int i=0;i<triangles.size/3;i++) 
		{
			Vecteur p1=cloud.elementAt(triangles.get(3*i)/2).getP();
			Vecteur p2=cloud.elementAt(triangles.get(3*i+1)/2).getP();
			Vecteur p3=cloud.elementAt(triangles.get(3*i+2)/2).getP();
			Triangle3D t=new Triangle3D(p1,p2,p3);
			localMesh.add(t);
		}

		for (Triangle3D t:localMesh) globalMesh.add(new Triangle3D(t.p1().copy(),t.p2().copy(),t.p3().copy()));
		updateGlobal();


	}

	/**
	 * get altitude of the circular border in the local frame
	 * @return
	 */
	public double getDepth()
	{
		double z=0;
		if (r!=0) z=-r*(1-Math.cos(Math.asin(dCap/2/r)));
		return z;
	}

	@Override
	public void updateGlobal()
	{
		//calc the global coordinates:
		for (int i=0;i<globalMesh.nbTriangles();i++)
		{
			frame.global(globalMesh.getTriangle(i),localMesh.getTriangle(i));
		}
	}

	//public TriangleMesh getTriangles() {return tris;}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		globalMesh.checkOccupyingCube(cornerMin, cornerMax);

	}
	@Override
	public void draw(Graphics g, Projector proj) 
	{
		globalMesh.draw(g, proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		return globalMesh.getDistanceToScreen(proj);
	}
	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Triangle3D tri:globalMesh) 
			if (tri.isOn(p, proj)) return true;
		return false;
	}


	@Override
	public Frame getFrame()
	{
		return frame;
	}



	@Override
	/**
	 * 
	 */
	public TriangleMesh getTriangles() {
		return globalMesh;
	}

	


	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public double getdCap() {
		return dCap;
	}

	public void setdCap(double dCap) {
		this.dCap = dCap;
	}

	public int getDim() {
		return dim;
	}

	public void setDim(int dim) {
		this.dim = dim;
	}

	public boolean isRandomMesh() {
		return randomMesh;
	}

	public void setRandomMesh(boolean randomMesh) {
		this.randomMesh = randomMesh;
	}

	public static void testOrderedMesh(String[] args) 
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);
		graph3DPanel.setAxesLength(100);
		
		SphericalCap sc=new SphericalCap(new Vecteur(),100,25,50,false);
		sc.getFrame()._rotate(new Vecteur(0,-90*Math.PI/180.0,0));
		sc.getFrame()._translate(new Vecteur(100,15,0));
		sc.getFrame()._rotate(new Vecteur(0,0,-0.1));
		sc.updateGlobal();
		
		graph3DPanel.add(sc);

		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
	}
	
	public static void testRandomMesh(String[] args) 
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);
		graph3DPanel.setAxesLength(100);
		
		SphericalCap sc=new SphericalCap(new Vecteur(),10,25,50,true);
		
		graph3DPanel.add(sc);

		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
	}
	
	
public static void main(String[] args) 
{
	//testOrderedMesh(args);
	//testRandomMesh(args);
	SphericalCap s=new SphericalCap(new Vecteur(),1,2,20,false);
	Graph3DWindow g3d=new Graph3DWindow(s);
	

}



}
