//Author Laurent Brunel. CERN 1996

package com.photonlyx.toolbox.math.geometry;





//*******************************************************************************************
//*************************** CLASSE ROTATEUR *********************************************** //*******************************************************************************************

/**provides rotation formulas*/

public class Rotator
{
double mr[][];

/**Create the rotation matrix for these angles in rad
Warning:the rotation is done first with Z then Y then X !
Can be optimised by precalculating some repeated cos and sin
*/
public Rotator(Vecteur angles)
{
init(angles);
}

public Rotator()
{
init(0,0,0);
}

/**Create the rotation matrix for these angles in rad
Warning:the rotation is done first with Z then Y then X !
Can be optimised by precalculating some repeated cos and sin
*/
public Rotator(double ax,double ay,double az)
{
init(new Vecteur(ax,ay,az));
}
	
/**reset the rotation matrix for these angles in rad
Warning:the rotation is done first with Z then Y then X !
*/
public void init(double ax,double ay,double az)
{
init(new Vecteur(ax,ay,az));
}
	

/**reset the rotation matrix for these angles in rad
Warning:the rotation is done first with Z then Y then X !
*/
public void init(Vecteur angles)
	{
	/* computation of the rotation matrix elements
	om - 1st rotation around "x"
	ph - 2nd rotation around "y"
	ka - 3rd rotation around "Z" axis
warning:the rotation is done first with Z then Y then X !
	mr - rotation matrix                       */

	double om,ph,ka;
	mr=new double[3][3];

	om=angles.x();
	ph=angles.y();
	ka=angles.z();
	
	double cosom=Math.cos(om);
	double cosph=Math.cos(ph);
	double coska=Math.cos(ka);
	double sinom=Math.sin(om);
	double sinph=Math.sin(ph);
	double sinka=Math.sin(ka);
	
	mr[0][0]=cosph*coska;
	mr[0][1]=cosom*sinka+sinom*sinph*coska;
	mr[0][2]=sinom*sinka-cosom*sinph*coska;
	mr[1][0]=-cosph*sinka;
	mr[1][1]=cosom*coska-sinom*sinph*sinka;
	mr[1][2]=sinom*coska+cosom*sinph*sinka;
	mr[2][0]=sinph;
	mr[2][1]=-sinom*cosph;
	mr[2][2]=cosom*cosph;
	}

/**turn the Vecteur with the rotation matrix, return a new turned vector*/
public Vecteur turn(Vecteur ue)
	{
	double x,y,z;
	Vecteur us;
	x=mr[0][0]*ue.x()+mr[0][1]*ue.y()+mr[0][2]*ue.z();
	y=mr[1][0]*ue.x()+mr[1][1]*ue.y()+mr[1][2]*ue.z();
	z=mr[2][0]*ue.x()+mr[2][1]*ue.y()+mr[2][2]*ue.z();
	us = new Vecteur(x,y,z);
	return us;
	}

/**turn the Vecteur with the rotation matrix*/
public void _turn(Vecteur ue)
	{
	double x,y,z;
	x=mr[0][0]*ue.x()+mr[0][1]*ue.y()+mr[0][2]*ue.z();
	y=mr[1][0]*ue.x()+mr[1][1]*ue.y()+mr[1][2]*ue.z();
	z=mr[2][0]*ue.x()+mr[2][1]*ue.y()+mr[2][2]*ue.z();
	ue.affect(x,y,z);
	}

/** turn the frame*/
public void _turn(Frame f)
{
for (int i=0;i<3;i++) 
	{
//	Vecteur v=this.turn(f.axis(i));
//	f.axis(i).affect(v);
	this._turn(f.axis(i));
	}
	
}


public void _turn(Quadrilatere3D q1) 
{
	for (int j=0;j<4;j++) _turn(q1.p(j));	
}





} //fin classe rotateur
