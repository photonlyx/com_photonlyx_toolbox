package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import nanoxml.XMLElement;


/**
 * planar rectangle with sinusoidal mountains
 */
public class PlaneBumpy extends TriangleMeshFrame implements XMLstorable
{
	private int nx=10,ny=10;// nb of picks in  x and y direction
	private double dx=1,dy=1;//size in x and y direction
	private int n=100;//sampling
	private double h=0.1;//pick height (radius is one)
	private boolean box=false;//if true produce a closed surface (defining a volume) in shape of box
	private double dz=0.5;//size of eventual box in z direction
	private double r=10;//radius of global spherical surface (+ if convex - if concave)
	private Vecteur pos=new Vecteur();



	private Object3DColorSet set4obj=new Object3DColorSet();

	private Frame frame=new Frame();//local frame 

	public PlaneBumpy()
	{
		init();
	}


	@Override
	public void updateGlobal()
	{
		TriangleMesh globalMesh=this.getGlobal();
		//calc the global coordinates:
		for (int i=0;i<globalMesh.nbTriangles();i++)
		{
			frame.global(globalMesh.getTriangle(i),getLocal().getTriangle(i));
		}
	}

	//public TriangleMesh getTriangles() {return tris;}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		getGlobal().checkOccupyingCube(cornerMin, cornerMax);

	}
	@Override
	public void draw(Graphics g, Projector proj) 
	{
		getGlobal().draw(g, proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		return getGlobal().getDistanceToScreen(proj);
	}
	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Triangle3D tri:getGlobal()) 
			if (tri.isOn(p, proj)) return true;
		return false;
	}


	@Override
	public Frame getFrame()
	{
		return frame;
	}



	public void init()
	{
		set4obj.removeAll();
		double sx=dx/(n-1);//sampling in longitude
		double sy=dy/(n-1);//sampling in latitude
		Rotator rot=new Rotator();

		Vecteur[][] mat=new Vecteur[n+1][n];
		double x,y,z;
		double wx=Math.PI*2/(dx/nx);//omega x pulsation in x of sin wave
		double wy=Math.PI*2/(dy/ny);//omega y pulsation in y of sin wave
		double fleche=(dx/2*dx/2+dy/2*dy/2)/2/r;//sphere thickness at corners
		for (int i=0;i<n;i++)//loop on x
		{
			x=-dx/2+sx*i;
			for (int j=0;j<n;j++)//loop on y
			{
				y=-dy/2+sy*j;
				double sphere=(x*x+y*y)/2/r;
				z=h*Math.sin(wx*x)*Math.sin(wy*y)-sphere+fleche;
				mat[i][j]=new Vecteur(x,y,z);
			}
		}
		//build the quadrilateres:
		Vecteur p1,p2,p3,p4;
		for (int i=0;i<n-1;i++)//loop on x
			for (int j=0;j<n-1;j++)//loop on y
			{
				p1=mat[i][j];
				p4=mat[i+1][j];
				if (j==n-1)
				{
					p2=mat[i][0];
					p3=mat[i+1][0];
				}
				else
				{
					p2=mat[i][j+1];
					p3=mat[i+1][j+1];
				}
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p4,p3,p2);
				set4obj.add(q);
			}

		//set local mesh:
		TriangleMesh localMesh=this.getLocal();
		localMesh.removeAllElements();		
		for (Object3DColor o:set4obj)
		{
			if (o instanceof Quadrilatere3DColor)
			{
				Quadrilatere3DColor q=(Quadrilatere3DColor)o;
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
			if (o instanceof Triangle3DColor)
			{
				Triangle3DColor t=(Triangle3DColor)o;
				localMesh.add(t);
			}
		}
		//place sphere side
		for (int i=0;i<n-1;i++)//loop on x
		{
			{
				//behind:
				p1=new Vecteur(mat[i][n-1].x(),dy/2,0);
				p2=new Vecteur(mat[i+1][n-1].x(),dy/2,0);
				p3=mat[i+1][n-1];
				p4=mat[i][n-1];
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
			{
				//front:
				p1=new Vecteur(mat[i][0].x(),-dy/2,0);
				p2=new Vecteur(mat[i+1][0].x(),-dy/2,0);
				p3=mat[i+1][0];
				p4=mat[i][0];
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
		}
		for (int j=0;j<n-1;j++)//loop on y
		{
			{
				//left:
				p1=new Vecteur(-dx/2,mat[0][j].y(),0);
				p2=new Vecteur(-dx/2,mat[0][j+1].y(),0);
				p3=mat[0][j+1];
				p4=mat[0][j];
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
			{
				//right:
				p1=new Vecteur(dx/2,mat[n-1][j].y(),0);
				p2=new Vecteur(dx/2,mat[n-1][j+1].y(),0);
				p3=mat[n-1][j+1];
				p4=mat[n-1][j];
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
		}


		if (box)
		{
			{
				//place bottom
				p1=new Vecteur(-dx/2,-dy/2,-dz);
				p2=new Vecteur(+dx/2,-dy/2,-dz);
				p3=new Vecteur(+dx/2,+dy/2,-dz);
				p4=new Vecteur(-dx/2,+dy/2,-dz);
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
			{
				//place side
				p1=new Vecteur(-dx/2,-dy/2,-dz);
				p2=new Vecteur(+dx/2,-dy/2,-dz);
				p3=new Vecteur(+dx/2,-dy/2,0);
				p4=new Vecteur(-dx/2,-dy/2,0);
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
			{
				//place side
				p1=new Vecteur(-dx/2,dy/2,-dz);
				p2=new Vecteur(+dx/2,dy/2,-dz);
				p3=new Vecteur(+dx/2,dy/2,0);
				p4=new Vecteur(-dx/2,dy/2,0);
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
			{
				//place side
				p1=new Vecteur(-dx/2,-dy/2,-dz);
				p2=new Vecteur(-dx/2,+dy/2,-dz);
				p3=new Vecteur(-dx/2,+dy/2,0);
				p4=new Vecteur(-dx/2,-dy/2,0);
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}
			{
				//place side
				p1=new Vecteur(dx/2,-dy/2,-dz);
				p2=new Vecteur(dx/2,+dy/2,-dz);
				p3=new Vecteur(dx/2,+dy/2,0);
				p4=new Vecteur(dx/2,-dy/2,0);
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				Vector<Triangle3D> v=q.transformIn2Triangles();
				for (Triangle3D t:v) localMesh.add(t);
			}

		}

		//set global mesh:
		getGlobal().removeAllElements();
		for (Triangle3D tri:localMesh) getGlobal().add(tri.copy());
		
		//translate to position pos:
		this.getFrame()._translate(pos);

		updateGlobal();

	}






	public int getnx() {
		return ny;
	}


	public void setny(int i) {
		this.ny = i;
	}


	public int getN() {
		return n;
	}


	public void setN(int n) {
		this.n = n;
	}


	public double getH() {
		return h;
	}


	public void setH(double h) {
		this.h = h;
	}



	public int getNx() {
		return nx;
	}


	public void setNx(int nx) {
		this.nx = nx;
	}


	public int getNy() {
		return ny;
	}


	public void setNy(int ny) {
		this.ny = ny;
	}


	public double getDx() {
		return dx;
	}


	public void setDx(double dx) {
		this.dx = dx;
	}


	public double getDy() {
		return dy;
	}


	public void setDy(double dy) {
		this.dy = dy;
	}


	public boolean isBox() {
		return box;
	}


	public void setBox(boolean box) {
		this.box = box;
	}


	public double getDz() {
		return dz;
	}


	public void setDz(double dz) {
		this.dz = dz;
	}


	public double getR() {
		return r;
	}


	public void setR(double r) {
		this.r = r;
	}
	
	
	
	public double getX() {
		return pos.x();
	}


	public void setX(double x) {
		this.pos.setX(x);
	}


	public double getY() {
		return pos.y();
	}


	public void setY(double y) {
		this.pos.setY(y);
	}


	public double getZ() {
		return pos.z();
	}


	public void setZ(double z) {
		this.pos.setZ(z);
	}


	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().getSimpleName().toString().replace("class ", ""));
		el.setAttribute("x",pos.x());
		el.setAttribute("y",pos.y());
		el.setAttribute("z",pos.z());
		el.setAttribute("dx",dx);
		el.setAttribute("dy",dy);
		el.setAttribute("dz",dz);
		el.setAttribute("nx",nx);
		el.setAttribute("ny",ny);
		el.setAttribute("n",n);
		el.setAttribute("h",h);
		el.setAttribute("box",box);
		el.setAttribute("r",r);
		return el;
	}

	@Override
	public void updateFromXML(XMLElement xml)
	{
		pos.setX(xml.getDoubleAttribute("x",0));	
		pos.setY(xml.getDoubleAttribute("y",0));	
		pos.setZ(xml.getDoubleAttribute("z",0));	
		dx=xml.getDoubleAttribute("dx",1);	
		dy=xml.getDoubleAttribute("dy",1);	
		dz=xml.getDoubleAttribute("dz",1);	
		nx=xml.getIntAttribute("nx",10);	
		ny=xml.getIntAttribute("ny",10);	
		n=xml.getIntAttribute("n",100);	
		h=xml.getDoubleAttribute("h",0.1);	
		box=xml.getBooleanAttribute("box","true","false",false);	
		r=xml.getDoubleAttribute("r",0.1);	
	}



	public static void main(String[] args) 
	{
		//test prg:


		PlaneBumpy p=new PlaneBumpy();

		//		{
		//			//save object parameters in an XML file:
		//			XMLElement el=s.toXML();
		//			TextFiles.saveString("/home/laurent/temp/bumpy.xml",el.toString());
		//		}

		if (args.length==1)
		{
			if (args[0].compareTo("-h")==0)
			{
				System.out.println("Usage: java -cp com_photonlyx_toolbox.jar com.photonlyx.toolbox.math.geometry.PlaneBumpy "
						+ "-i params.xml"+ " -o sphere1.stl");
				System.out.println("Example of xml file:");
				System.out.println(XMLFileStorage.toStringWithIndentation(p.toXML()));
				System.exit(0);
			}
		}

		if (args.length>=1)
		{
			String inputFileName=null;//XML file with input parameters
			String outpuFileName=null;//STL file with output geometry
			for (int i=0;i<args.length;i++)
			{
				if (args[i].compareTo("-i")==0)
				{

					inputFileName=args[i+1];
				}
				if (args[i].compareTo("-o")==0)
				{
					//load parameters from XML file
					outpuFileName=args[i+1];
				}
			}
			if ((inputFileName!=null)&&(outpuFileName!=null))
			{
				//load parameters from XML file
				XMLElement xml=XMLFileStorage.readXMLfile(inputFileName);
				p.updateFromXML(xml);
				System.out.println(p.toXML());
				p.init();
				p.saveToSTLfile("./",outpuFileName);
			}

		}
		if (args.length==0)
		{
			//p.getFrame()._translate(new Vecteur(1,0,0));
			//p.updateGlobal();

			//		//add to a set:
			//		Object3DColorSet set=new Object3DColorSet();
			//		set.add(s.getTriangles());
			//		graph3DPanel.addColorSet(set);

			p.n=100;
			p.h=0.1;
			p.nx=5;
			p.ny=5;
			p.setDx(5);
			p.setDy(5);
			p.setDz(1);
			p.setR(10);
			p.setX(0);
			p.setY(0);
			p.setZ(1);
			p.setBox(true);
			p.init();
			
			p.saveToSTLfile("/home/laurent/temp","bumpy.stl");

			//			Method[] methods=p.getClass().getMethods();
			//			for (Method m:methods) 
			//			{
			//				String name=m.getName();
			//				if (name.startsWith("get")) System.out.println(name+"   "+m.getGenericReturnType());
			//			}

			//add directly to the graph 3D:
			Graph3DPanel graph3DPanel=new Graph3DPanel();
			//graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
			graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);
			graph3DPanel.setRender(false);
			graph3DPanel.add(p.getTriangles());


			//create frame and add the panel 3D
			CJFrame cjf=new CJFrame();
			cjf.getContentPane().setLayout(new BorderLayout());
			cjf.setSize(600, 400);
			cjf.setVisible(true);
			cjf.add(graph3DPanel.getComponent());
		}



	}




}





