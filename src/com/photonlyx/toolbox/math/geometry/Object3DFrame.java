package com.photonlyx.toolbox.math.geometry;

public interface Object3DFrame extends Object3D
{
	
	public Frame getFrame();

	/**
	 * update global coordinates (after changing frame)
	 */
	public void updateGlobal();

	
}
