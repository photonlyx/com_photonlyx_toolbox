package com.photonlyx.toolbox.math.geometry;

import java.util.Vector;

/** 
 * Class to manipulate segments in 2D
 * 
 *
 */
public class Segment2D
{
	private Vecteur2D p1,p2;



	public Segment2D()
	{
		p1=new Vecteur2D();	
		p2=new Vecteur2D();	
	}


	public Segment2D(Vecteur2D p1,Vecteur2D p2)
	{
		this.p1=p1;
		this.p2=p2;
	}

	public Segment2D(double[] _p1,double[] _p2)
	{
		p1=new Vecteur2D(_p1[0],_p1[1]);	
		p2=new Vecteur2D(_p2[0],_p2[1]);	
	}

	public Segment2D(int[] _p1,int[] _p2)
	{
		p1=new Vecteur2D(_p1[0],_p1[1]);	
		p2=new Vecteur2D(_p2[0],_p2[1]);		
	}

	/**return the first point of the segment*/
	public Vecteur2D p1(){return p1;}
	/**return the second point of the segment*/
	public Vecteur2D p2(){return p2;}
	//public double[] p1(){return p1;}
	//public double[] p2(){return p2;}

	public void setP1(double x,double y)
	{
		p1.affect(x,y);
	}
	public void setP2(double x,double y)
	{
		p2.affect(x,y);
	}


	public static double quadDist2D(double[] point)
	{
		return Math.sqrt(point[0]*point[0]+point[1]*point[1]);
	}

	public  double length()
	{
		double dx=p2.x()-p1.x();
		double dy=p2.y()-p1.y();
		return Math.sqrt(dx*dx+dy*dy);
	}

	public static double pointdist2D(double[] point1, double[] point2)
	{
		double[] delta = {point1[0]-point2[0], point1[1]-point2[1]};
		return quadDist2D(delta);
	}

	public boolean isOnSegment (Vecteur2D p)
	{
		//double linemag = this.length();
		//scalar product (P1 I).(P1 P2)
		Vecteur2D p1I=p.subn(p1);
		Vecteur2D p1p2=p2.subn(p1);
		double u=p1I.mul(p1p2)/p1p2.normeSquare();
		//double u =(intersection.x() - p1.x()) * (p2.x() - p1.x())+ (intersection.y() - p1.y()) * (p2.y() - p1.y());
		//u/=linemag*linemag;
		//System.out.println("intersectionIsOnSegment u="+u);
		return 	!(u < 0.0 || u > 1.0);
	}


	/**
	 * get the distance of a point to the segment
	 * @param point
	 * @return
	 */
	public double getDistance( double[] point)
	{
		double[] intersection=new double[2];
		return segdist2D(p1.getCoord(), p2.getCoord(),point,intersection);
	}

	/**
	 * get the distance of a point to the segment
	 * @param point
	 * @return
	 */
	public double getDistance( double x,double y)
	{
		double[] point=new double[2];
		point[0]=x;
		point[1]=y;
		return this.getDistance(point);
	}

	public double getDistance( Vecteur2D p)
	{
		return getDistance(p.x(),p.y());
	}


	/**
	 * get the distance of a point to a segment
	 * @param segcoord0
	 * @param segcoord1
	 * @param point
	 * @param intersection coord of the perpendicular projection of point to segment
	 * @return
	 */
	public static double segdist2D(double[] segcoord0, double[] segcoord1, double[] point,double[] intersection)
	{
		double linemag = pointdist2D(segcoord0, segcoord1);
		double u = 
				(point[0] - segcoord0[0]) * (segcoord1[0] - segcoord0[0])
				+ (point[1] - segcoord0[1]) * (segcoord1[1] - segcoord0[1]);

		u/=linemag*linemag;

		// if intersection is not on segment
		if (u < 0.0 || u > 1.0)
		{
			double pdist1 = pointdist2D(segcoord0, point);
			double pdist2 = pointdist2D(segcoord1, point);

			if (pdist1 < pdist2) 
			{
				intersection[0] = segcoord0[0];
				intersection[1] = segcoord0[1];
				return pdist1;
			}
			else
			{
				intersection[0] = segcoord1[0];
				intersection[1] = segcoord1[1];
				return pdist2;
			}
		}

		// intersection is on segment. find distance to segment
		intersection[0] = segcoord0[0] + u*(segcoord1[0] - segcoord0[0]);
		intersection[1] = segcoord0[1] + u*(segcoord1[1] - segcoord0[1]);

		return pointdist2D(intersection, point);
	}

	public static double segdist2D(double[] segcoord0, double[] segcoord1, double[] point)
	{
		double[] intersection = new double[2];

		return segdist2D(segcoord0,segcoord1,point,intersection);
	}



	/**
	 * return the line bisector of the segment made with p1 and p2
	 * @param p1
	 * @param p2
	 * @return
	 */
	public Line2D getBisectorLine()
	{
		Vecteur2D  p1p2=p2.subn(p1);
		Vecteur2D o=p1.addn(p1p2.scmul(0.5));//origin of the bisector
		Vecteur2D v=p1p2.getPerpVector(true);
		v.normalise();
		return new Line2D(o,v);
	}

	public String toString()
	{
		return p1+" "+p2;
	}

	/**
	 * check if the segment is fully in a rectangle
	 * @param xmin
	 * @param xmax
	 * @param ymin
	 * @param ymax
	 * @return
	 */
	public boolean isIn(double xmin, double xmax, double ymin, double ymax) 
	{
		return ((p1.isIn(xmin,xmax,ymin,ymax))&&(p2.isIn(xmin,xmax,ymin,ymax)));
	}



	public Vector<Vecteur2D> intersection(Polygon2D pol) 
	{
		Vector<Vecteur2D> l=new Vector<Vecteur2D>();
		Vector<Segment2D> segs=pol.getSegments();
		Vecteur2D i;
		for (Segment2D s:segs)
		{
			i=this.intersection(s);
			if (i!=null) l.add(i);
		}
		return l;
	}


	private Vecteur2D intersection(Segment2D s) 
	{
		Vecteur2D inter=null;
		Line2D l1=new Line2D(this);
		Line2D l2=new Line2D(s);
		Vecteur2D v=l1.getIntersectionPointWith(l2);
		if(v!=null)
			if (this.isOnSegment(v)) 
				inter=v;
		return inter;
	}





}
