package com.photonlyx.toolbox.math.geometry.cloud;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Frame;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Plane;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
/**
 * octree
 * @author laurent
 *
 */
public class Voxel implements Object3D
{
private Voxel father;
private Vecteur pmin=new Vecteur(),pmax=new Vecteur();
private Voxel[][][] sons= new Voxel[2][2][2];
private Cloud cloudPoints=new Cloud();
private static Color verticesColor=Color.blue;
public static  double minVoxelSizeX=1;
public static boolean seeCenter=false;
public static boolean seeVertices=true;
public static boolean seeNormals=false;
public static boolean seePlanes=false;

public Voxel()
{
}

public Voxel(Voxel father, Vecteur pmin, Vecteur pmax)
{
super();
this.father = father;
this.pmin = pmin;
this.pmax = pmax;
}

/**
 * Recursively register the point in this voxel: 
 * find or create the son voxel that contains the point. 
 * And recursively do the same for the voxels sons .
 * @param m
 */
public void registerPoint(CloudPoint m)
{
int ix,iy,iz;
double phalfx=(pmax.x()+pmin.x())/2;
double phalfy=(pmax.y()+pmin.y())/2;
double phalfz=(pmax.z()+pmin.z())/2;

//m.setVoxel(this);
cloudPoints.add(m);
m.setVoxel(this);

Vecteur pmin2=new Vecteur(),pmax2=new Vecteur();
if ((m.getP().x())>=phalfx) 
	{
	ix=1;
	pmin2.setX(phalfx);
	pmax2.setX(pmax.x());
	}
else
	{
	ix=0;
	pmin2.setX(pmin.x());
	pmax2.setX(phalfx);
	}
	
if ((m.getP().y())>=phalfy)
	{
	iy=1;
	pmin2.setY(phalfy);
	pmax2.setY(pmax.y());
	}
	else
	{
	iy=0;
	pmin2.setY(pmin.y());
	pmax2.setY(phalfy);
	}
if ((m.getP().z())>=phalfz)
	{
	iz=1;
	pmin2.setZ(phalfz);
	pmax2.setZ(pmax.z());
	}
	else
	{
	iz=0;
	pmin2.setZ(pmin.z());
	pmax2.setZ(phalfz);
	}

//System.out.println("**"+pmin.x()+"   "+pmax.x());
//System.out.println(pmin2.x()+"   "+pmax2.x());
if ((pmax2.x()-pmin2.x())>minVoxelSizeX) 
	{
	//if this voxel has no son around this point create it
	if (sons[ix][iy][iz]==null) 
		{
		Voxel v=new Voxel(this,pmin2,pmax2);
		sons[ix][iy][iz]=v;
		}
	//repeat that for this son voxel:
	sons[ix][iy][iz].registerPoint(m);
	}
}


@Override
public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
{
cornerMin.affect(pmin);
cornerMax.affect(pmax);
}


@Override
/**
 * draw the voxels sons if any
 */
public void draw(Graphics g, Projector proj)
{

boolean hasSons=false;
for (int ix=0;ix<2;ix++)for (int iy=0;iy<2;iy++)for (int iz=0;iz<2;iz++) 
	if (sons[ix][iy][iz]!=null) 
		{
		hasSons=true;
		sons[ix][iy][iz].draw(g, proj);
		}

if (hasSons) return;


//draw the vertices:
if (seeVertices) 
	{
	g.setColor(verticesColor);
	double[] cooScreen1,cooScreen2;
	for (Segment3D seg:getVertices()) 
		{
		cooScreen1=proj.calcCoorEcran(seg.p1());
		cooScreen2=proj.calcCoorEcran(seg.p2());
		g.drawLine((int)cooScreen1[0],(int)cooScreen1[1],(int)cooScreen2[0],(int)cooScreen2[1]);
		}
	}

//draw the center of gravity:
if (seeCenter) this.drawCenterOfGravity( g,  proj,3);


if (seePlanes) if (getCloudPoints().size()>5)
	{	
	Plane plane=getFittedPlane();
	if (plane!=null)
		{
		Frame frame=plane.getLocalFrame();
		double axisSize=pmax.sub(pmin).norme()/4;
		frame.draw(g, proj, Color.red, axisSize);
		}
	}

////enhance the points in the voxel:
//size=1;
//for (Measure m:this.getMeasures())
//	{
//	if (m.isSelected()) size=5;
//	g.setColor(Color.red);
//	cooScreen=proj.calcCoorEcran(m.getP());
//	g.fillRect(cooScreen[0]-size/2,cooScreen[1]-size/2, size, size);	
//	}

////draw the fitted plane boundaries
//Vector<Vecteur> bd=getFittedPlaneBoundaries();
////int[] cooScreen;
//if (bd!=null)
//	{
//	Polygon polb=new Polygon();
//	for (Vecteur p:bd) 
//		{
//		cooScreen=proj.calcCoorEcran(p);
//		polb.addPoint(cooScreen[0],cooScreen[1]);
//		}
//	g.setColor(Color.black);
//	g.drawPolygon(polb);
//	}




}


/**
 * get the center of the points in the voxel
 * @return
 */
public Vecteur calcCenterOfGravity()
{
int n=this.getCloudPoints().size();
Vecteur p0=new Vecteur();
for (CloudPoint m:this.getCloudPoints()) p0._add(m.getP());
p0._scmul(1.0/n);	
return p0;
}

public void drawCenterOfGravity(Graphics g, Projector proj,int size)
{
int n=this.getCloudPoints().size();
if (n<3) return;
Vecteur p0=new Vecteur();
for (CloudPoint m:this.getCloudPoints()) p0._add(m.getP());
p0._scmul(1.0/n);	

//debug:
//for (Measure m:v.getMeasures()) System.out.println(m.getP());
//System.out.println(v.getMeasures().size()+" pts");
//System.out.println("Center of gravity:"+p0);

g.setColor(verticesColor);
double[] cooScreen=proj.calcCoorEcran(p0);
g.fillRect((int)cooScreen[0]-size/2,(int)cooScreen[1]-size/2, size, size);	

if (seeNormals)
	{
	//draw the normals
	Vecteur p1=p0.addn(getFittedPlane().getNormal());
	g.setColor(Color.red);
	double[] cooScreen1=proj.calcCoorEcran(p1);
	g.drawLine((int)cooScreen[0],(int)cooScreen[1], (int)cooScreen1[0],(int)cooScreen1[1]);
	}
}




public boolean hasSons()
{
for (int ix=0;ix<2;ix++)for (int iy=0;iy<2;iy++)for (int iz=0;iz<2;iz++) 
	if (sons[ix][iy][iz]!=null) 
		{
		return true;
		}
return false;
}

/**destroy tree to avoid memory leaks*/
public void destroyTheTree()
{
for (int ix=0;ix<2;ix++)
	for (int iy=0;iy<2;iy++)
		for (int iz=0;iz<2;iz++) 
			if (sons[ix][iy][iz]!=null)
				{
				sons[ix][iy][iz].removeLinksToCloudPoints();
				sons[ix][iy][iz].setFather(null);
				sons[ix][iy][iz]=null;
				}
this.removeLinksToCloudPoints();
this.setFather(null);
}


@Override
public double getDistanceToScreen(Projector proj)
{
// TODO Auto-generated method stub
return 0;
}

public Vecteur getPmin()
{
return pmin;
}

public void setPmin(Vecteur pmin)
{
this.pmin .affect( pmin);
}

public Vecteur getPmax()
{
return pmax;
}

public void setPmax(Vecteur pmax)
{
this.pmax.affect( pmax);
}

public Voxel getFather()
{
return father;
}

public Voxel[][][] getSons()
{
return sons;
}

public void setFather(Voxel father)
{
this.father = father;
}

/** free the memory !*/
public void removeLinksToCloudPoints()
{
cloudPoints.removeAllElements();
}

/**get the cloud points inside the voxel*/
public Cloud getCloudPoints()
{
return cloudPoints;
}

/**
 * calc the plane fitted to the cloud of points inside the Voxel
 * @return null if not enough points in the voxel (<3)
 */
public Plane getFittedPlane()
{
Cloud cloud=new Cloud();
for (CloudPoint m:this.getCloudPoints()) cloud.add(new CloudPoint((m.getP())));
Plane plane=Plane.getClosestPlane(cloud);
return plane;
}


/**
 * return the vertices of the limits of the fitted plane in the voxel
 * @return
 */
public Vector<Vecteur> getFittedPlaneBoundaries()
{
//System.out.println("getFittedPlaneBoundaries:");
Plane plane=getFittedPlane();
if (plane==null) return null;
//now list the vertices that are boundaries of plane and voxel:
Vector<Vecteur> boundaries=new Vector<Vecteur>(); 
//System.out.println("vertices:");
//for (Segment3D vert:getVertices()) System.out.println("vert:"+vert);
for (Segment3D vert:getVertices()) 
	{
	Vecteur i=vert.intersection(plane);
	//System.out.println("boundary voxel:"+i);
	if (i!=null) boundaries.add(i);
	}
return boundaries;
}

public Vector<Segment3D> getVertices()
{
//half size vector
Vecteur size=(pmax.sub(pmin));
double dx=size.x();
double dy=size.y();
double dz=size.z();
//create the voxel corners:
Vecteur[][][] corner=new Vecteur[2][2][2];
for (int x=0;x<2;x++)
	for (int y=0;y<2;y++)
		for (int z=0;z<2;z++)
			corner[x][y][z]=pmin.addn(dx*x,dy*y,dz*z);

Vector<Segment3D> vertices=new Vector<Segment3D>(); 
for (int x=0;x<2;x++)
	for (int y=0;y<2;y++)
		for (int z=0;z<2;z++)
			{
			if ((1-x)>x) vertices.add(new Segment3D(corner[x][y][z],corner[1-x][y][z]));
			if ((1-y)>y) vertices.add(new Segment3D(corner[x][y][z],corner[x][1-y][z]));
			if ((1-z)>z) vertices.add(new Segment3D(corner[x][y][z],corner[x][y][1-z]));
			}
return vertices;
}

public Vector<Vecteur> getCorners()
{
Vector<Vecteur> corners=new Vector<Vecteur>(); 
//half size vector
Vecteur size=(pmax.sub(pmin));
double dx=size.x();
double dy=size.y();
double dz=size.z();
for (int x=0;x<2;x++)
	for (int y=0;y<2;y++)
		for (int z=0;z<2;z++)
			corners.add(pmin.addn(dx*x,dy*y,dz*z));
return corners;
}




/**
 * tells if the point is inside the voxel (faces included)
 * @param p
 * @return
 */
public boolean isInside(Vecteur p)
{
return (
		(p.x()>=pmin.x())&&(p.x()<=pmax.x())&&
		(p.y()>=pmin.y())&&(p.y()<=pmax.y())&&
		(p.z()>=pmin.z())&&(p.z()<=pmax.z())
		);
}

/**
 * get the distance of this voxel to a point
 * @param point
 * @return 0 if the point is inside the voxel
 */
public double getDistanceFromPoint(Vecteur point)
{
//half size vector
Vecteur size=(pmax.sub(pmin));
if (isInside(point)) return 0;
return Vecteur.distance(pmin.addn(pmax).scmul(0.5), point)-size.norme()/2;
}

/**
 * the distance of the voxel from a point. if the point is inside the voxel the distance is 0.
 * if the point is outside the distance is the one to the closest face.
 * @param point
 * @return
 */
public double getDistanceFromPointOLD(Vecteur point)
{
if (isInside(point)) return 0;
double r1=1e30,d1=1e30,d2=1e30,dFaces;
//distance to planes of faces:
Vecteur projmin=point.copy();
Vecteur projmax=point.copy();
//int coor1,coor2;
for (int coor=0;coor<3;coor++) 
	{
//	coor1=(coor+1)%3;
//	coor2=(coor+2)%3;
	//proj to plane min along axis coor:
	projmin.u[coor]=pmin.u[coor];
//	projmin.u[coor1]=pmin.u[coor1];
//	projmin.u[coor2]=pmin.u[coor2];
	if (this.isInside(projmin)) d1=Math.abs(point.u[coor]-pmin.u[coor]);
	
	//proj to plane max along axis coor:
	projmax.u[coor]=pmax.u[coor];
//	projmax.u[coor1]=pmax.u[coor1];
//	projmax.u[coor2]=pmax.u[coor2];
	if (this.isInside(projmax)) d2=Math.abs(point.u[coor]-pmax.u[coor]);
	dFaces=Math.min(d1,d2);
	if (dFaces<r1) r1=dFaces;
	}
//distance to vertices:
double r2=1e30,d; 
Vector<Vecteur> verts=getCorners();
for (Vecteur vert:verts)
	{
	d=Vecteur.distance(vert, point);
	if (d<r2) r2=d;
	}

return Math.min(r1,r2);
}

/**
 * 
 * @param list
 * @param v
 * @param point
 * @param dist
 * @return
 */
public  void getVoxelsCloseToAPoint_Recursive(Vector<Voxel> list,Vecteur point,double dist)
{
double voxeldist=this.getDistanceFromPoint(point);
//System.out.println("voxeldist"+voxeldist);
if (voxeldist>dist) return;//don't look at the sons
else 
	{
	if (!hasSons()) list.add(this);//it is a leaf
	else//look at the sons
		{
		for (int x=0;x<2;x++)
			for (int y=0;y<2;y++)
				for (int z=0;z<2;z++) if (sons[x][y][z]!=null) sons[x][y][z].getVoxelsCloseToAPoint_Recursive(list, point, dist);
		}
	}
}


public  void getVoxels_Recursive(Vector<Voxel> list)
{

	if (!hasSons()) list.add(this);//it is a leaf
	else//look at the sons
		{
		for (int x=0;x<2;x++)
			for (int y=0;y<2;y++)
				for (int z=0;z<2;z++) if (sons[x][y][z]!=null) sons[x][y][z].getVoxels_Recursive(list);
		}
}


public double getMeanNbPtsPerVoxel()
{
Vector<Integer> v=new Vector<Integer>();
this.getNbPtsPerVoxels_Recursive(v);
double r=0;
for (int nb:v) r+=nb;
r/=v.size();
return r;
}

public void getNbPtsPerVoxels_Recursive(Vector<Integer> v)
{
if (!hasSons()) 
	{
	v.add(this.getCloudPoints().size());//it is a leaf
	}
else//look at the sons
	{
	for (int x=0;x<2;x++)
		for (int y=0;y<2;y++)
			for (int z=0;z<2;z++) if (sons[x][y][z]!=null) sons[x][y][z].getNbPtsPerVoxels_Recursive(v);
	}

}

@Override
public boolean isOn(Point p, Projector proj)
{
// TODO Auto-generated method stub
return false;
}







}
