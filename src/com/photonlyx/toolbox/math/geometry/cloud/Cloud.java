package com.photonlyx.toolbox.math.geometry.cloud;

import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;
import java.util.stream.Stream;

import javax.swing.JFileChooser;

import nanoxml.XMLElement;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;

import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.Sorter;
import com.photonlyx.toolbox.math.geometry.DelaunayTriangulation;
import com.photonlyx.toolbox.math.geometry.Frame;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Plane;
import com.photonlyx.toolbox.math.geometry.Point2D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Triangle2D;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;

public class Cloud extends Vector<CloudPoint>  implements XMLstorable, Object3D
{
	private Voxel biggerVoxel;

	static final Comparator<CloudPoint> X_ORDER = new Comparator<CloudPoint>() 
	{
		public int compare(CloudPoint e1, CloudPoint e2) 
		{
			double diff=(e1.getP().x())-(e2.getP().x());
			int i=0;
			if (diff<0) i= -1;
			if (diff==0) i= 0;
			if (diff>0) i= 1;
			return i;
		}
	};

	public Cloud()
	{
		// TODO Auto-generated constructor stub
	}


	public Vecteur calcCenterOfGravity()
	{
		Vecteur p0=new Vecteur();
		for (CloudPoint cp:this) p0._add(cp.getP());
		int n=this.size();
		p0._scmul(1.0/n);
		return p0;
	}

	/**
	 * calc the RMS of distance to the center
	 */
	public double calcSigma(Vecteur calcCenterOfGravity)
	{
		return Math.sqrt(calcSigma2(calcCenterOfGravity));
	}

	/**
	 * calc the variance (sigma^2) of distance to the center
	 */
	public double calcSigma2(Vecteur calcCenterOfGravity)
	{
		double r=0;
		for (CloudPoint cp:this) 
		{
			Vecteur p=cp.getP();
			r+=calcCenterOfGravity.sub(p).normeSquare();
		}
		return r;
	}

	public void createVoxels()
	{
		biggerVoxel=new Voxel();
		//biggerVoxel.setPmin(new Vecteur(-100,-100,-100));
		//biggerVoxel.setPmax(new Vecteur(100,100,100));
		biggerVoxel.setPmin(this.calcPmin());
		biggerVoxel.setPmax(this.calcPmax());
		biggerVoxel.destroyTheTree();
		//create the voxels tree:
		for (CloudPoint m:this)
		{
			biggerVoxel.registerPoint(m);
		}
	}


	public Plane getClosestPlane()
	{
		return Plane.getClosestPlane(this);
	}

	public void sortByXCoordinate()
	{
		//int n=this.size();
		////sort by x coord (required by algorithm)
		//double[] x=new double[n];
		//for (int i=0;i<n;i++) x[i]=elementAt(i).getP().x();
		//int[] indicesSort=Sorter.sort(x);

		Collections.sort(this, X_ORDER);
		//for (int i=0;i<size();i++) System.out.println(elementAt(i).getP().x());
	}


	/**
	 * calculate the minimum corner of the occupancy box
	 * @return
	 */
	public Vecteur calcPmin()
	{
		double xmin=1e30,ymin=1e30,zmin=1e30;
		for (CloudPoint cp:this)
		{
			double x=cp.getP().x();
			double y=cp.getP().y();
			double z=cp.getP().z();
			if (x<xmin) xmin=x;
			if (y<ymin) ymin=y;
			if (z<zmin) zmin=z;
		}
		return new Vecteur(xmin,ymin,zmin);
	}

	/**
	 * calculate the maximum corner of the occupancy box
	 * @return
	 */
	public Vecteur calcPmax()
	{
		double xmax=-1e30,ymax=-1e30,zmax=-1e30;
		for (CloudPoint cp:this)
		{
			double x=cp.getP().x();
			double y=cp.getP().y();
			double z=cp.getP().z();
			if (x>xmax) xmax=x;
			if (y>ymax) ymax=y;
			if (z>zmax) zmax=z;
		}
		return new Vecteur(xmax,ymax,zmax);
	}



	/**
	 * get the Delaunay triangulation of this cloud, based on the closest plane
	 * @return
	 */
	public TriangleMesh getDelaunayTriangulation()
	{
		double ransacTolerance=this.calcPmax().sub(this.calcPmin()).norme()/20.0;
		Plane plane=fitPlaneByRANSAC(ransacTolerance);
		Frame frame=plane.getLocalFrame();
		Frame frame2=frame.invertFrameCoordinates();
		//calc the coordinates in the closest plane frame
		Cloud projCloud=new Cloud();
		for (CloudPoint cp:this)
		{
			Vecteur v2=frame.newCoordinates(cp.getP());
			projCloud.add(new CloudPoint(v2));
		}
		TriangleMesh tris=projCloud.getDelaunayTriangulationXY();
		TriangleMesh tris2=new TriangleMesh();
		for (Triangle3D tri:tris)
		{
			Triangle3D tri2=frame2.newCoordinates(tri);
			tris2.add(tri2);
			//frame2._newCoordinates(tri);
		}
		return tris2;
	}



	/**
	 * get the Delaunay triangulation of this cloud, based on the XY projection
	 * @return
	 */
	public TriangleMesh getDelaunayTriangulationXY()
	{
//		TriangleMesh tris=new TriangleMesh();
//		int n=this.size();
//		this.sortByXCoordinate();//required by delaunay triangulation algorithm
//		float[] points=new float[n*2];
//		for (int i=0;i<n;i++) 
//		{
//			points[2*i]=(float)this.elementAt(i).getP().x();
//			points[2*i+1]=(float)this.elementAt(i).getP().y();
//		}

//		DelaunayTriangulator dt=new DelaunayTriangulator();
//		IntArray triangles=dt.computeTriangles(points, 0, n*2, true);
//
//		for (int i=0;i<triangles.size/3;i++) 
//		{
//			Vecteur p1=this.elementAt(triangles.get(3*i)/2).getP();
//			Vecteur p2=this.elementAt(triangles.get(3*i+1)/2).getP();
//			Vecteur p3=this.elementAt(triangles.get(3*i+2)/2).getP();
//			Triangle3D t=new Triangle3D(p1,p2,p3);
//			if (t.area()!=0) tris.add(t.copy());
//		}
		

		int n=this.size();
		Vector<Point2D> points=new Vector<Point2D>();
		for (int i=0;i<n;i++) 
		{
			CloudPoint v=this.elementAt(i);
			double x=(float)v.getP().x();
			double y=(float)v.getP().y();
			Point2D p=new Point2D(x,y);
			points.add(p);
		}
		DelaunayTriangulation dt = new DelaunayTriangulation(points);
		dt.triangulate();
		TriangleMesh tris=new TriangleMesh();
		int i=0;
		for (Triangle2D t : dt.getTriangles()) 
		{
			CloudPoint v=this.elementAt(i);
			double z=v.getP().z();
			Triangle3D t1=new Triangle3D(
					new Vecteur(t.getP1().getX(),t.getP1().getY(),z),
					new Vecteur(t.getP2().getX(),t.getP2().getY(),z),
					new Vecteur(t.getP3().getX(),t.getP3().getY(),z));
			tris.add(t1);
			i++;
		}
		
		
		
		return tris;
	}


	/**
	 * fir a plane with RANSAC algo: random sample consensus
	 */
	public Plane fitPlaneByRANSAC(double tolerance)
	{
		int n=this.size();
		int[] indices=new int[n];
		Vecteur pPlane=new Vecteur();
		Vecteur nPlane=new Vecteur();
		Vecteur bestpPlane=new Vecteur();
		Vecteur bestnPlane=new Vecteur();
		int nbInliers1=0;
		int nbInliersMax=0;
		for (int i=0;i<n;i++)
		{
			nbInliers1=ransacCheck(indices,pPlane,nPlane,tolerance);
			if (nbInliers1==-1) continue; //the 3 points don't form a plane
			if (nbInliers1>nbInliersMax)
			{
				nbInliersMax=nbInliers1;
				bestpPlane.affect(pPlane);
				bestnPlane.affect(nPlane);
			}
			//System.out.println(i+" ransac nb inliers:"+nbInliers1+"/"+n);
			if ((double)nbInliers1/(double)n>0.9) break;
		}
		//System.out.println("ransac nb inliers:"+nbInliers1+"/"+n);
		return new Plane(bestpPlane,bestnPlane);
	}


	/**
	 * used by RANSAC. https://en.wikipedia.org/wiki/RANSAC
	 * @param indexes the indices of the inliers
	 * @return the numbers of inliers
	 */
	private int ransacCheck(int[] indices,Vecteur pPlane,Vecteur nPlane,double tolerance)
	{
		int n=this.size();
		//get 3 aleatory points
		int i1,i2,i3;//,i4;
		i1=(int)(Math.random()*(n-1));
		i2=i1;
		while (i2==i1) i2=(int)(Math.random()*(n-1));
		i3=i1;
		while ((i3==i2)||(i3==i1)) i3=(int)(Math.random()*(n-1));
		//i4=i1;
		//while ((i4==i3)||(i4==i2)||(i4==i1)) i4=(int)(Math.random()*(n-1));
		//System.out.println(i1+" "+i2+" "+i3+" "+i4);

		Triangle3D tri=new Triangle3D(this.elementAt(i1).getP(),this.elementAt(i2).getP(),this.elementAt(i3).getP());
		Plane plane1=tri.getPlane();
		if (plane1==null) return -1;//the 3 points don't form a plane

		////fit a plane to this subcloud:
		//Cloud c1=new Cloud();
		//c1.add(new CloudPoint(this.elementAt(i1).getP()));
		//c1.add(new CloudPoint(this.elementAt(i2).getP()));
		//c1.add(new CloudPoint(this.elementAt(i3).getP()));
		//c1.add(new CloudPoint(this.elementAt(i4).getP()));
		//Plane plane1=Plane.getClosestPlane(c1);

		//check the distance of all the points to this plane:
		int c=0;
		for (int i=0;i<this.size();i++)
		{
			CloudPoint pc=this.elementAt(i);
			double d=plane1.distance(pc.getP());
			if (d<=tolerance) 
			{
				indices[c]=i;
				c++;
			}
		}
		pPlane.affect(plane1.getpPlane());
		nPlane.affect(plane1.getNormal());
		//System.out.println("ransac nb inliers:"+c);
		return c;
	}





	public Voxel getBiggerVoxel()
	{
		return biggerVoxel;
	}



	/**
	 * get the voxels (leafs) that has at least a part closer to a point than a minimum distance dist
	 * @param dist
	 * @return
	 */
	public Vector<Voxel> getVoxelsCloseTo(Vecteur point,double dist)
	{
		Vector<Voxel> list=new Vector<Voxel>();
		biggerVoxel.getVoxelsCloseToAPoint_Recursive(list,point,dist);
		return list;
	}

	/**
	 * calc the occupancy box diagonal size ( norm(Pmax-Pmin) )
	 * @return
	 */
	public double calcDiagSize()
	{
		return this.calcPmax().sub(calcPmin()).norme();
	}

	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		//el.setAttribute("angle1CameraMin", angle1CameraMin);
		for (CloudPoint  m : this) el.addChild(m.toXML());
		return el;
	}

	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		removeAllElements();
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			CloudPoint m=new CloudPoint();
			m.updateFromXML(enumSons.nextElement());
			if (!m.isNaN())add(m);
			//System.out.println(m.getP());
		}
		Global.setMessage(this.size()+" points loaded");
	}

	public Vector<Voxel> getAllVoxels()
	{
		Vector<Voxel> list=new Vector<Voxel>();
		biggerVoxel.getVoxels_Recursive(list);
		return list;

	}

	/**
	 * calc the covariance matrix
	 * 
	 * i: raw
	 * j: col
	 * Ci value on dimension i ( C0 is x, C1 is y, C2 is Z)
	 * C'k = Ck- mean(Ck)=Ck- sum(Ck)/n the mean centered data
	 * 
	 * (    sigma(C'i C'j)/n  )
	 * 
	 * @param c centerOfGravity
	 * @return
	 */
	private Matrix calcCovarianceMatrix(Vecteur c)
	{
		Matrix m=new Matrix(3, 3);
		for (int i=0;i<3;i++)
			for (int j=0;j<=i;j++)
			{
				double r=0;
				for (CloudPoint cp:this) r+=(cp.getP().coord(i)-c.coord(i))*(cp.getP().coord(j)-c.coord(j));
				m.set(i, j, r);
			}
		//fill the other side:
		for (int i=0;i<3;i++)
			for (int j=i+1;j<3;j++) m.set(i, j, m.get(j, i));
		//m.print (new DecimalFormat("0.00000"),10);		
		return m;
	}

	/**
	 * principal component analysis
	 */
	public Frame pca()
	{
		//Vecteur c=this.calcCenterOfGravity();
		//Matrix cov=this.calcCovarianceMatrix(c);
		//EigenvalueDecomposition eigen=new EigenvalueDecomposition(cov);
		//Matrix eigenValuesMatrix=eigen.getD();
		//Matrix vectorsMatrix=eigen.getV();
		////eigenValuesMatrix.print(new DecimalFormat("0.00000"),10);		
		////vectorsMatrix.print(new DecimalFormat("0.00000"),10);	
		//double[] eigenValues=new double[3];
		//for (int i=0;i<3;i++) eigenValues[i]=eigenValuesMatrix.get(i,i);
		//int[] indices=Sorter.sort(eigenValues);
		////for (int i=0;i<3;i++) System.out.println(indices[i]);
		//Vecteur X=new Vecteur(vectorsMatrix.get(0, indices[2]),vectorsMatrix.get(1, indices[2]),vectorsMatrix.get(2, indices[2]));
		//Vecteur Y=new Vecteur(vectorsMatrix.get(0, indices[1]),vectorsMatrix.get(1, indices[1]),vectorsMatrix.get(2, indices[1]));
		//Vecteur Z=new Vecteur(vectorsMatrix.get(0, indices[0]),vectorsMatrix.get(1, indices[0]),vectorsMatrix.get(2, indices[0]));
		//Frame f=new Frame(c,X,Y,Z);
		//return f;

		double[] eigenValues=new double[3];
		Frame frame=new Frame();
		pca(eigenValues,frame);
		return frame;
	}

	/**
	 * Principal Component Analysis
	 * @param eigenValues sorted in decreasing order associated to X Y and Z
	 * @param frame the frame of the closest plane Z has the lowest eigen value, X the highest
	 */
	public void pca(double[] eigenValues,Frame frame)
	{
		Vecteur c=this.calcCenterOfGravity();
		Matrix cov=this.calcCovarianceMatrix(c);
		EigenvalueDecomposition eigen=new EigenvalueDecomposition(cov);
		Matrix eigenValuesMatrix=eigen.getD();
		Matrix vectorsMatrix=eigen.getV();
		//eigenValuesMatrix.print(new DecimalFormat("0.00000"),10);		
		//vectorsMatrix.print(new DecimalFormat("0.00000"),10);	
		double[] a=new double[3];
		for (int i=0;i<3;i++) a[i]=eigenValuesMatrix.get(i,i);
		int[] indices=Sorter.sort(a);
		//for (int i=0;i<3;i++) System.out.println(indices[i]);
		//Vecteur X=new Vecteur(vectorsMatrix.get(0, indices[2]),vectorsMatrix.get(1, indices[2]),vectorsMatrix.get(2, indices[2]));
		//Vecteur Y=new Vecteur(vectorsMatrix.get(0, indices[1]),vectorsMatrix.get(1, indices[1]),vectorsMatrix.get(2, indices[1]));
		//Vecteur Z=new Vecteur(vectorsMatrix.get(0, indices[0]),vectorsMatrix.get(1, indices[0]),vectorsMatrix.get(2, indices[0]));
		for (int i=0;i<3;i++) frame.setAxis(i,
				new Vecteur(vectorsMatrix.get(0, indices[2-i]),
						vectorsMatrix.get(1, indices[2-i]),
						vectorsMatrix.get(2, indices[2-i])));
		//frame.setAxis(0, X);
		//frame.setAxis(1, Y);
		//frame.setAxis(2, Z);
		frame.setCentre(c);
		//for (int i=0;i<3;i++) eigenValues[i]=eigenValuesMatrix.get(indices[2-i],indices[2-i]);
		for (int i=0;i<3;i++) eigenValues[i]=a[2-i];
	}


	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
	{
		for (CloudPoint cp:this) cp.checkOccupyingCube(cornerMin, cornerMax);
	}


	@Override
	public void draw(Graphics g, Projector proj)
	{
		for (CloudPoint cp:this) cp.draw(g, proj);
	}


	@Override
	public double getDistanceToScreen(Projector proj)
	{
		// TODO Auto-generated method stub
		return 0;
	}


	/**
	 * get the list of x y z coordinates:
	 * @return
	 */
	public StringBuffer getXYZlist()
	{
		StringBuffer sb=new StringBuffer();
		//for (CloudPoint m:this) sb.append(m.getP().toString()+"1\t"+"\n");
		Vecteur p;
		for (CloudPoint m:this) 
		{
			p=m.getP();
			sb.append(p.x()+"\t"+p.y()+"\t"+p.z()+"\t"+"\n");
		}
		return sb;
	}

	/**
	 * export cloud to a XYZ file format
	 */
	public void exportCloudXYZ()
	{
		JFileChooser df=new JFileChooser(Global.path);
		CFileFilter ff=new CFileFilter("xyz","points cloud ASCII");
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showSaveDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			Global.path=df.getSelectedFile().getParent()+File.separator;
			String filename=df.getSelectedFile().getName();
			if (!filename.endsWith(ff.getExtension())) filename+="."+ff.getExtension();
			try
			{
				FileOutputStream fo = new FileOutputStream(Global.path+filename);
				PrintWriter pw = new PrintWriter(fo);
				pw.print(this.getXYZlist().toString());
				pw.flush();
				pw.close();
				fo.close();
				Global.setMessage(filename+" saved");
			}
			catch (Exception e) 
			{
				Messager.messErr("ShadowCasting "+Messager.getString("Probleme_ecriture_fichier")+" "+filename);;
			}
			System.out.println(filename+" saved");
		}	

	}






	public void readXYZFormat(StringBuffer sb)
	{
		this.removeAllElements();
		String s=sb.toString();
		String[] lines=StringSource.readLines(s);
		for (String l:lines)
		{
			Definition def=new Definition(l);
			if (def.dim()==3)
			{
				double x=def.getValue(0, 0);
				double y=def.getValue(1, 0);
				double z=def.getValue(2, 0);
				CloudPoint m=new CloudPoint(new Vecteur(x,y,z));
				this.add(m);
			}
		}
	}


	/**
	 * read a cloud of points encoded in floats
	 * @param path
	 * @param file
	 * @param littleEndian  bigEndian for file produced by java, littleEndian for file produced by C
	 * @return
	 */
	public boolean readBinaryFile (String path,String file,boolean littleEndian)
	{
		byte[] b0=new byte[(1)*Float.BYTES];
		BufferedInputStream br;
		try
		{
			br=new BufferedInputStream(new FileInputStream(path+file));
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading cloud file ");
			e.printStackTrace();
			return false;
		}
		//read header:
		try
		{
			br.read(b0,0,b0.length);
			
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading   cloud file ");
			e.printStackTrace();
			try {br.close();} catch (Exception e1) {};
			return false;
		}
		ByteBuffer buf0=ByteBuffer.wrap(b0);
		if (littleEndian) buf0.order(ByteOrder.LITTLE_ENDIAN); 
		int nb_points=(int)buf0.getFloat();
		//System.out.println(nb_points +" points in points cloud file "+path+file);

		//read data:
		byte[] b=new byte[(nb_points*3)*Float.BYTES];
		//System.out.println("read "+(b.length)+" bytes in file:"+path+file+" ...");
		try
		{
			br.read(b,0,b.length);
			br.close();
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading  cloud file "+path+file);
			e.printStackTrace();
			return false;
		}

		ByteBuffer buf=ByteBuffer.wrap(b);
		if (littleEndian) buf.order(ByteOrder.LITTLE_ENDIAN); 
		this.removeAllElements();
		for (int i=0;i<nb_points;i++) 
			{
				Vecteur p=new Vecteur();
				p.xAffect(buf.getFloat());
				p.yAffect(buf.getFloat());
				p.zAffect(buf.getFloat());
				//System.out.println(p);
				CloudPoint cp=new CloudPoint(p);
				this.add(cp);
			}
		System.out.println("Points cloud file "+path+file+" loaded");
		return true;

	}


	public double calcTypicalpointDistance()
	{
		double cloudSize=this.calcDiagSize();
		double maxDistance=1;
		for (CloudPoint cp:this)
		{
			Cloud cn=cp.getNeighbors(this, maxDistance);

		}
		return 1;
	}

	/**
	 * tells if the cloud is like a plane
	 * @param tolerance (max thickness/mean size (x,y))
	 * @return
	 */
	public boolean isLikeAplane(double tolerance)
	{
		int n=this.size();

		//estimate the cloud shape by principal component analysis:
		double[] eigenValues=new double[3];
		Frame frame=new Frame();
		pca(eigenValues,frame);

		//System.out.println(frame);
		//System.out.print("Eigen size: ");
		//for (int i=0;i<3;i++)System.out.print(Math.sqrt(eigenValues[i])+" ");
		//System.out.println();

		double ratio=Math.sqrt(eigenValues[2])/((Math.sqrt(eigenValues[0])+Math.sqrt(eigenValues[1]))/2);
		if (ratio<tolerance) 
		{
			//System.out.println("The cloud is like a plane");
			return true;
		}
		else
		{
			return false;
		}
	}

	public double calcInterPoints()
	{
		//int n=this.size();
		//
		////estimate the cloud shape by principal component analysis:
		//double[] eigenValues=new double[3];
		//Frame frame=new Frame();
		//pca(eigenValues,frame);
		//
		//System.out.println(frame);
		//System.out.print("Eigen size: ");
		//for (int i=0;i<3;i++)System.out.print(Math.sqrt(eigenValues[i])+" ");
		//System.out.println();
		//
		//double ratio=Math.sqrt(eigenValues[2])/((Math.sqrt(eigenValues[0])+Math.sqrt(eigenValues[1]))/2);
		//double interPoints=1;
		//if (ratio<0.1) 
		//	{
		//	interPoints=Math.sqrt(eigenValues[0]*eigenValues[1])/(n);
		//	System.out.println("The cloud is like a plane");
		//	}
		//else
		//	{
		//	interPoints=Math.sqrt(eigenValues[0]*eigenValues[1]*eigenValues[2])/(n);
		//	}

		double s=this.calcSigma(this.calcCenterOfGravity());
		double save=Voxel.minVoxelSizeX;
		Voxel.minVoxelSizeX=s/10;
		for (;;)
		{
			this.createVoxels();
			double meanNbPtsPerVoxel=biggerVoxel.getMeanNbPtsPerVoxel();
			//System.out.println("minVoxelSizeX="+Voxel.minVoxelSizeX+"  meanNbPtsPerVoxel ="+meanNbPtsPerVoxel);
			if (meanNbPtsPerVoxel<3) break;
			Voxel.minVoxelSizeX/=1.5;
		}

		double interPoints=Voxel.minVoxelSizeX*1.5;
		System.out.println("inter points ="+interPoints);
		Voxel.minVoxelSizeX=save;
		//double meanNbNeihbors=0;
		//for (int i=0;i<n;i++)
		//	{
		//	CloudPoint cp=this.elementAt(i);
		//	Cloud cn=cp.getNeighbors(this, interPoints);
		//	meanNbNeihbors+=cn.size();
		//	}
		//meanNbNeihbors/=n;
		//System.out.println("meanNbNeihbors="+meanNbNeihbors);
		return interPoints;
	}



	public Vector<TriangleMesh> get3DTriangulation()
	{
		double interPoints= calcInterPoints();
		double startMaxDistance=interPoints*2;
		int n=this.size();

		Voxel.minVoxelSizeX=interPoints*3;
		this.createVoxels();

		//CloudPoint cp=this.elementAt(0);
		////Cloud cn=cp.getNeighbors(this, interPoints);
		//Cloud neighbors=new Cloud();
		//Vector<Double> distances=new Vector<Double>();
		////Vector<Integer> neighbors=new Vector<Integer>();
		//cp.getNeighbors(this,neighbors,distances,maxDistance);

		Vector<Cloud> cns=new Vector<Cloud>();
		for (int i=0;i<n;i++)
		{
			CloudPoint startcp=this.elementAt(i);
			if (!belongsTo(cns,startcp))//look for a point that is no in any part
			{
				//get a part from this point
				Cloud cn=getPlaneLikeSubCloud(startcp,startMaxDistance);
				//cloud of points to remove:
				Cloud toRemove=new Cloud();
				//list the  points that are already in the parts:
				for (CloudPoint cp:cn) if (belongsTo(cns,cp)) toRemove.add(cp);
				//System.out.println("remove "+toRemove.size()+"pts");
				//remove them:
				for (CloudPoint cp:toRemove) cn.remove(cp);
				//add this part to the others
				cns.add(cn);
			}
			if (i%(n/100)==0) Global.setMessage((int)((double)i/(double)n*100.0)+"%");
		}
		System.out.println("Triangulation with "+cns.size()+" parts");

		//for (Cloud c:cns) 
		//	{
		//	Vector<Triangle3D> tri=c.getDelaunayTriangulation();
		//	for (Triangle3D t:tri) tris.add(t);
		//	}

		//make Delaunay for each part and remove the big triangles:
		Vector<TriangleMesh> trisList=new Vector<TriangleMesh>(); 
		for (Cloud c:cns) if (c.size()>3)
		{
			Global.setMessage("Triangulation Delaunay of "+c.size()+" pts");
			//System.out.println("Triangulation Delaunay of "+c.size()+" pts");
			TriangleMesh tris=c.getDelaunayTriangulation();
			//prepare the list of triangles to remove:
			Vector<Triangle3D> toRemove=new Vector<Triangle3D>();
			//get the triangles too big
			for (Triangle3D t:tris) if (t.maxSideSize()>interPoints*5) toRemove.add(t);
			//remove them:
			for (Triangle3D t:toRemove) tris.remove(t);
			trisList.add(tris);
		}
		return trisList;

	}

	private boolean belongsTo(Vector<Cloud> cns,CloudPoint cp)
	{
		for (Cloud c:cns) if (c.contains(cp)) return true;
		return false;
	}

	private Cloud getPlaneLikeSubCloud(CloudPoint startcp,double startMaxDistance)
	{
		Cloud cn;
		for (;;)
		{
			cn=startcp.getNeighbors(this, startMaxDistance);
			//System.out.println("nb neighbors="+cn.size());
			if (!cn.isLikeAplane(0.15)) break;//the subcloud is no more planar
			if (cn.size()==this.size()) break;//the subcloud is the entire cloud
			startMaxDistance*=1.4;
		}
		startMaxDistance/=1.4;
		return cn;

		//return tris;
	}



	public void createSegments()
	{
		double interPoints= calcInterPoints();
		Voxel.minVoxelSizeX=interPoints*3;
		this.createVoxels();
		Cloud neighbors=new Cloud();
		Vector<Double> distances=new Vector<Double>();
		double[] distances1;
		int[] indices;
		for (CloudPoint cp:this)
		{
			cp.getNeighbors(this,neighbors, distances,interPoints*1);
			distances1=new double[distances.size()];
			for (int i=0;i<distances.size();i++) distances1[i]=distances.elementAt(i);
			indices=Sorter.sort(distances1);
			cp.removeAllSegments();
			for (int i=1;i<Math.min(5, neighbors.size());i++) 
			{
				CloudPoint otherSide=neighbors.elementAt(indices[i]);
				if (!otherSide.isConnectedTo(cp)) 
					cp.addSegment(otherSide);
			}
		}
	}


	@Override
	public Stream<CloudPoint> stream()
	{
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Stream<CloudPoint> parallelStream()
	{
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean isOn(Point p, Projector proj)
	{
		// TODO Auto-generated method stub
		return false;
	}


	



}
