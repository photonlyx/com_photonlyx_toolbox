package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;

public class Triangle3D implements Object3D
{
	protected Vecteur[] p;
	private Vecteur centre;
	private Vecteur n;//normal
	//private Voxel v;
	private double[][] cooScreen=new double[4][2];//screen coordinates
	public static boolean drawCenter=false;
	public static boolean drawNormal=false;

	public Triangle3D()
	{
		p=new Vecteur[3];
		for (int i=0;i<3;i++) p[i]=new Vecteur();	
	}

	public Triangle3D(Vecteur[] _ps)
	{
		p=new Vecteur[3];
		for (int i=0;i<3;i++) p[i]=_ps[i];	
	}

	public Triangle3D(Vecteur _p1,Vecteur _p2,Vecteur _p3)
	{
		p=new Vecteur[3];
		p[0]=_p1;	
		p[1]=_p2;	
		p[2]=_p3;	
		calcCentre();
		calcNormal();
	}


	public Vecteur p(int i){return p[i];}


	public Vecteur p1(){return p[0];}
	public Vecteur p2(){return p[1];}
	public Vecteur p3(){return p[2];}



	public Triangle3D invertNormal()
	{
		Vecteur pp=p[0];
		p[0]=p[1];
		p[1]=pp;
		return this;
	}


	public void _invertNormal()
	{
		Vecteur pp=p[0];
		p[0]=p[1];
		p[1]=pp;
	}

	//
	//public void setVoxel(Voxel v)
	//{
	//this.v=v;
	//}

	public void _translate(Vecteur v)
	{
		for (int i=0;i<3;i++) p[i]._translate(v);	
		centre._translate(v);	
	}






	/**if one of the is out of the cube, extends the cube*/
	public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax)
	{
		for (int j=0;j<3;j++)
		{
			for (int k=0;k<3;k++) 
			{
				double r=p[j].coord(k);
				if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
				if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
			}	
		}
	}



	public double getDistanceToScreen(Projector proj)
	{
		return 	proj.zCoord(centre);
	}
	
	public Vecteur calcCentre()
	{
		centre=p[0].addn(p[1]).addn(p[2]).scmul(1.0/3);
		return centre;
	}
	
	public Vecteur getCentre()
	{
		return centre;
	}

	public Vecteur calcNormal()
	{
		n= getNormal(p[0],p[1],p[2]);
		return n;
	}

	public Vecteur getNormal()
	{
		return n;
	}


	//http://math.stackexchange.com/questions/305642/how-to-find-surface-normal-of-a-triangle
	//if V = P2 - P1 and W = P3 - P1, and N is the surface normal, then:
	//Nx=(Vy∗Wz)−(Vz∗Wy)
	//Ny=(Vz∗Wx)−(Vx∗Wz)
	//Nz=(Vx∗Wy)−(Vy∗Wx)
	public static Vecteur getNormal(Vecteur p0,Vecteur p1,Vecteur p2)
	{
		Vecteur V=p1.sub(p0);
		Vecteur W=p2.sub(p0);
		double vx=V.x();
		double vy=V.y();
		double vz=V.z();
		double wx=W.x();
		double wy=W.y();
		double wz=W.z();
		double nx=vy*wz-vz*wy;
		double ny=vz*wx-vx*wz;
		double nz=vx*wy-vy*wx;
		Vecteur n= new Vecteur(nx,ny,nz);

		//Vecteur n=V.vectmul(W);

		n._normalise();
		return n;
	}




	/**
	 * check if (x,y) is in the projection of the triangle on plane (x,y)
	 * @param xr
	 * @param yr
	 * @return
	 */
	public boolean isInTriangle(double x, double y )
	{
		//vector P12
		Vecteur p12=this.p2().sub(this.p1());
		double x2=p12.x();
		double y2=p12.y();
		//vector P13:
		Vecteur p13=this.p3().sub(this.p1());
		double x3=p13.x();
		double y3=p13.y();

		//vector P: (x,y,z) in the local triangle projection on (x,y) frame:
		double xr=x-this.p1().x();
		double yr=y-this.p1().y();

		//P= X P12 + Y P13
		double X=(xr*y3-yr*x3)/(x2*y3-y2*x3);
		double Y=(xr*y2-yr*x2)/(x3*y2-y3*x2);

		//P = beta ( alpha P12 + (1-alpha) P13  )
		double alpha=1/(1+Y/X);
		double beta=X+Y;

		//System.out.println("alpha="+alpha+"  beta="+beta);
		//System.out.println("beta="+beta);

		if ((alpha>=0)&(beta>=0)&(alpha<=1)&(beta<=1)) return true;else return false;
	}

	/**
	 * intersection between line and triangle
	 * http://geomalgorithms.com/a06-_intersect-2.html
	 * @param pos a point of the line
	 * @param dir direction of the line
	 * @return
	 */
	public Vecteur intersection(Vecteur pos,Vecteur dir)
	{
		//intersection with the plane of the triangle
		Vecteur i=Plane.intersectionPlaneLine(p[0], n, pos, dir);

		//check if i is in the triangle:

		//vector P1 i
		Vecteur w=i.sub(this.p1());
		//vector P12
		Vecteur u=this.p2().sub(this.p1());
		//vector P13:
		Vecteur v=this.p3().sub(this.p1());


		//calc alpha and beta such that i = si u + ti v
		double uu=u.normeSquare();
		double vv=v.normeSquare();
		double uv=u.mul(v);
		double wu=w.mul(u);
		double wv=w.mul(v);
		double d=Math.pow(uv,2)-uu*vv;
		double si=(uv*wv-vv*wu)/d;
		double ti=(uv*wu-uu*wv)/d;

		//System.out.println(this);
		//System.out.println("si="+si);
		//System.out.println("ti="+ti);
		//

		if ((si<0)||(ti<0)||((si+ti)>1)) return null; //i is out of the triangle
		else return i;

	}


	public String getSTLencodeing()
	{
		StringBuffer sb=new StringBuffer();
		Vecteur n=this.getNormal();
		sb.append("facet normal "+n.x()+" "+n.y()+" "+n.z()+"\n");
		sb.append("outer loop\n");
		sb.append("\tvertex "+this.p1().x()+" "+this.p1().y()+" "+this.p1().z()+"\n");
		sb.append("\tvertex "+this.p2().x()+" "+this.p2().y()+" "+this.p2().z()+"\n");
		sb.append("\tvertex "+this.p3().x()+" "+this.p3().y()+" "+this.p3().z()+"\n");
		sb.append("endloop\n");
		sb.append("endfacet\n");
		return sb.toString();
	}


	public void draw(Graphics g,Projector proj)
	{
		//draw corners
		for (int j=0;j<3;j++) 
		{
			double[] pe=proj.calcCoorEcran(p[j]);
			cooScreen[j][0]=pe[0];
			cooScreen[j][1]=pe[1];
		}
		for (int j=0;j<3;j++) 
		{
			int k=j+1;
			if (k==3) k=0;
			{
				g.drawLine((int)cooScreen[j][0], (int)cooScreen[j][1], (int)cooScreen[k][0], (int)cooScreen[k][1]);
			}
		}
		//draw center:
		if (drawCenter)
		{
			double[] pe=proj.calcCoorEcran(centre);
			cooScreen[0][0]=pe[0];
			cooScreen[0][1]=pe[1];
			int dotSize=3;
			g.fillRect((int)cooScreen[0][0]-dotSize/2, (int)cooScreen[0][1]-1,dotSize,dotSize);
		}
		//draw normal:
		if (drawNormal)
		{
			double normalSize=this.maxSideSize()/2;
			double[] pe=proj.calcCoorEcran(centre.addn(n.scmul(normalSize)));
			cooScreen[1][0]=pe[0];
			cooScreen[1][1]=pe[1];
			int dotSize=3;
			g.drawLine((int)cooScreen[0][0], (int)cooScreen[0][1], (int)cooScreen[1][0], (int)cooScreen[1][1]);
		}
	}





	public String toString()
	{
		String s="Triangle:\n";
		for (Vecteur v:p) s+=v+"\n";
		s+="center="+centre.toString()+"\n";
		s=s.substring(0, s.length()-2);
		return s;
	}

	/**
	 * return the plane where this triangle is
	 * @return the plane of null if 2 sides are aligned
	 */
	public Plane getPlane()
	{
		Vecteur c1=p[1].sub(p[0]);
		Vecteur c2=p[2].sub(p[0]);
		Vecteur n=c1.vectmul(c2);
		//if norm of n null the triangle doesn't define a plane:
		if (n.normeSquare()==0) return null;
		n._normalise();
		return new Plane(p[0],n);
	}

	public double maxSideSize()
	{
		double side,max=-1e30;
		for (int i=0;i<3;i++) 
		{
			side=Vecteur.distance(p[i],p[(i+1)%3]);
			if (side>max) max=side;
		}
		return max;
	}

	/**
	 * Calc the intersection points of the triangle with a plane
	 * @param p plane to intersect
	 * @return list of intersection points
	 */
	public Vector<Vecteur> intersection(Plane plane)
	{
		Vector<Vecteur> points=new Vector<Vecteur>();	
		for (int i=0;i<3;i++)
		{
			int i2=(i+1)%3;
			Segment3D seg=new Segment3D(p[i],p[i2]);
			Vecteur intersect=seg.intersection(plane);
			if (intersect!=null) points.add(intersect);
		}
		if (points.size()==3)
		{
			//a point of the triangle belongs to the plane and is counted twice
			//remove the intersection repeated twice:
			Vecteur p_doubled=null;
			for (Vecteur p:points)
			{
				for (int i=0;i<3;i++) 
				{
					if (this.p[i].sub(p).normeSquare()<0.000001)
					{
						p_doubled=p;
						break;
					}
				}
				if (p_doubled!=null) break;
			}
			points.remove(p_doubled);
		}
		return points;
	}

	/**
	 * get a Segment3D that is the intersection of this Triangle3D with a Plane
	 * @param plane
	 * @return
	 */
	public Segment3D getIntersectedSegment(Plane plane)
	{
		Vector<Vecteur> points=	intersection(plane);
		if (points.size()==2) return new Segment3D(points.elementAt(0),points.elementAt(1));
		else return null;
	}


	@Override
	public boolean isOn(Point p, Projector proj)
	{
		double[] pt=new double[2];
		pt[0]=p.x;
		pt[1]=p.y;
		return pointInTriangle (pt,cooScreen[0], cooScreen[1], cooScreen[2]);
	}


	private double sign (double[] p1, double[] p2, double[] p3)
	{
		return (p1[0] - p3[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p3[1]);
	}

	private boolean pointInTriangle (double[] pt, double[] v1, double[] v2, double[] v3)
	{
		double d1, d2, d3;
		boolean has_neg, has_pos;

		d1 = sign(pt, v1, v2);
		d2 = sign(pt, v2, v3);
		d3 = sign(pt, v3, v1);

		has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
		has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

		return !(has_neg && has_pos);
	}



	/**
	 * get a Triangle3D with cpied data
	 * @return
	 */
	public Triangle3D copy() 
	{	
		return new Triangle3D(p1().copy(),p2().copy(),p3().copy());
	}

	public double area()
	{
		double x1 = p[0].x(), y1 = p[0].y();
		double x2 = p[1].x(), y2 = p[1].y();
		double x3 = p[2].x(), y3 = p[2].y();
		// Applying the determinant-based area formula
		double area = Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0;
		return area;
	}


	
	public static void testFrame()
	{
		TriangleMeshFrame mesh=new TriangleMeshFrame();
		Vecteur p1=new Vecteur(0.1,0.1,0);
		Vecteur p2=new Vecteur(1,1,1);
		Vecteur p3=new Vecteur(1,1,-1);
		Triangle3D t=new Triangle3D(p1,p2,p3);
		mesh.add(t);
		
		//System.out.println("Global:\n"+mesh.getGlobal().getTriangle(0).toString());
		mesh.getFrame()._translate(new Vecteur(3,0,0));
		mesh.updateGlobal();
		//System.out.println("Global:\n"+mesh.getGlobal().getTriangle(0).toString());
		
		Graph3DWindow w=new Graph3DWindow();
		w.getCJFrame().setExitAppOnExit(true);
		w.getGraph3DPanel().getJPanel().setBackground(Color.LIGHT_GRAY);
		w.getGraph3DPanel().setRender(false);
		w.getGraph3DPanel().add(mesh);
	}
	

	public static void test()
	{
		Graph3DWindow w=new Graph3DWindow();
		w.getCJFrame().setExitAppOnExit(true);
		//graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		w.getGraph3DPanel().getJPanel().setBackground(Color.LIGHT_GRAY);
		w.getGraph3DPanel().setRender(false);
		Plane plane=new Plane(new Vecteur(),new Vecteur(0,0,1));


		Vecteur p1=new Vecteur(0.1,0.1,0);
		Vecteur p2=new Vecteur(1,1,1);
		Vecteur p3=new Vecteur(1,1,-1);
		Triangle3D t=new Triangle3D(p1,p2,p3);
		Vector<Vecteur> vs=t.intersection(plane);
		for (Vecteur v:vs) System.out.println(v);
		
		t._translate(new Vecteur(3,0,0));
		
		
		w.getGraph3DPanel().add(t);

	}

	public static void main(String[] args)
	{
	testFrame();
	}


}
