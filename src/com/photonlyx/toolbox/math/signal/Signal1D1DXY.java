// Author Laurent Brunel

package com.photonlyx.toolbox.math.signal;


import java.net.URL;
import java.text.*;
import java.util.*;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.math.Sorter;
import com.photonlyx.toolbox.math.Util;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Messager;

/**
represent a one dimension signal : series of co-ordinates x,y  a StringSource son might be readen</p>
 */
public class Signal1D1DXY extends Signal1D1D
{
	//links
	//private StringSourceInterface stringsource;
	//in
	protected int dimInit;

	//internal
	protected double[] x,y;
	protected int dim=0;//the number of points

	protected int lastPointIndex = -1;

	public Signal1D1DXY()
	{
		x=new double[0];
		y=new double[0];
		dim=0;
	}

	public Signal1D1DXY(double[] x,double[] y)
	{
		this.x=x;
		this.y=y;
		dim=x.length;
	}

	public Signal1D1DXY(int _dim)
	{
		x=new double[_dim];
		y=new double[_dim];
		dim=_dim;
	}

	public Signal1D1DXY(int _dim,int bufferSize)
	{
		this.init(_dim, bufferSize);
	}


	/**allocate the signal, the buffer size is set to _dim value
@param _dim nb of points of the signal
	 */
	public void init(int _dim)
	{
		x=new double[_dim];
		y=new double[_dim];
		dim=_dim;
		lastPointIndex=-1;
	}

	/**set a new zero signal of dim _dim , allocate the buffer to bufferSize . It bufferSize is lower than _dim it is set to _dim value
@param _dim nb of points of the signal
@param bufferSize size of the array
	 */
	public void init(int _dim,int bufferSize)
	{
		if (bufferSize<_dim) bufferSize=_dim;
		x=new double[bufferSize];
		y=new double[bufferSize];
		dim=_dim;
		lastPointIndex=-1;
	}

	/**
init with a String having 2 columns (x and y values)
	 */
	public void init(String s)
	{
		double[][] data2col=StringSource.fillWithTwoFirstColumns(s);
		if (data2col==null) return;
		x=data2col[0];
		y=data2col[1];
		dim=x.length;
		lastPointIndex=-1;
	}

	/**
	 * this signal doesn't support cell type signal
	 * @return
	 */
	public boolean isPointsSignal()
	{
		return true;
	}

	/** 
	 * Tell if this signal contains the same data as another signal. 
	 * 
	 * @param signal Signal to be compared.
	 * @return <code>true</code> if the 2 signals contain the same data, <code>false</code> otherwise.
	 */
	public boolean hasSameData(Signal1D1DXY signal)
	{
		if (getDim() != signal.getDim()) return false;

		int dim = getDim();

		for (int i = 0; i < dim ;i++)
		{
			if (x(i) != signal.x(i) || y(i) != signal.y(i)) return false;
		}

		return true;
	}

	public int getDim()
	{
		//if (y!=null) return y.length; else return 0;
		return dim;
	}

	/** return the x values array */
	public double[] getXarray()
	{
		return x;
	}

	/** return the y values array */
	public double[] getYarray()
	{
		return y;
	}


	/**return the x value for the sample of index i*/
	public double x(int i)
	{
		return (x[i]);
	}

	/**
	 * return the x value for the sample of index i</p>
	 * this Signal1D1D doesn't support cell like samples
	 * */
	public double xSample(int i)
	{
		return (x[i]);
	}

	/**equalise to the signal, make a copy of data. Previous data is lost*/
	public void copy(Signal1D1D signal)
	{
		init(signal.getDim());
		for (int i=0;i<getDim();i++)
		{
			setValue(i,signal.x(i),signal.y(i));
		}
	}

	/**
return a copy of this signal
	 */
	public Signal1D1DXY getACopy()
	{
		Signal1D1DXY newsignal=new Signal1D1DXY();
		//signal.copy(this);
		newsignal.init(getDim());
		for (int i=0;i<getDim();i++) newsignal.setValue(i,x(i),value(i));
		return newsignal;
	}


	/**
return the value at the cell i. If i is out of range,return 0
	 */
	public double value(int i)
	{
		//if ((i<0)||(i>=getDim())) return 0.0;
		return y[i];
	}



	/**
return the value y at the cell i. If i is out of range,return 0
	 */
	public double y(int i)
	{
		if ((i<0)||(i>=getDim())) return 0.0;
		return y[i];
	}

	/** set a point*/
	public void setValue(int i,double vx,double vy)
	{
		//if ((i<0)||(i>=getDim())) return;
		x[i]=vx;
		y[i]=vy;

		if (i < lastPointIndex)
		{
			// If a point is inserted, all the curve will be redraw
			//
			lastPointIndex = -1;
		}
	}

	/** set a y value*/
	public void setValue(int i,double vy)
	{
		//if ((i<0)||(i>=getDim())) return;
		y[i]=vy;

		if (i < lastPointIndex)
		{
			// If a point is inserted, all the curve will be redraw
			//
			lastPointIndex = -1;
		}
	}


	/** set a x value*/
	public void setXValue(int i,double vx)
	{
		x[i]=vx;
		if (i < lastPointIndex)
		{
			// If a point is inserted, all the curve will be redraw
			//
			lastPointIndex = -1;
		}
	}


	/**return a String with x and y in 2 columns*/
	public String toString()
	{
		NumberFormat nf1 = NumberFormat.getInstance(Locale.ENGLISH);
		nf1.setMaximumFractionDigits(10);
		nf1.setMinimumFractionDigits(10);
		nf1.setGroupingUsed(false);

		StringBuffer s=new StringBuffer();
		for (int i=0;i<getDim();i++)
		{
			s.append(nf1.format(x(i))+"\t"+nf1.format(value(i))+"\n");
		}
		return s.toString();
	}




	/**return the max x value()*/
	public double xmax()
	{
		if (getDim()==0) return 0;
		double r=-1e100;
		for (int i=0;i<getDim();i++) if (x[i]>r) r=x[i];
		return r;
	}


	/**return the min x value()*/
	public double xmin()
	{
		if (getDim()==0) return 0;
		double r=1e100;
		for (int i=0;i<getDim();i++) if (x[i]<r) r=x[i];
		return r;
	}



	/**
read a text file with 2 columns containing x and y real data
	 */
	public void fillWithFile(String filename)
	{
		if (filename==null)
		{
			Messager.messErr(getClass()+" can't find file: "+filename);
			return;
		}
		String s=TextFiles.readFile(filename).toString();
		double[][] data2col=StringSource.fillWithTwoFirstColumns(s);
		x=data2col[0];
		y=data2col[1];
		dim=x.length;
		lastPointIndex = -1;
	}


	/**
read a text file with 2 columns containing x and y real data
	 */
	public void fillWithFile(String filename,String xname,String yname)
	{
		if (filename==null) return;
		String s=TextFiles.readFile(filename).toString();
		double[][] data2col;
		System.out.println(getClass()+" "+" reading columns "+xname+" and "+yname+" in file "+filename);
		if ((xname!=null)&&(yname!=null))
		{
			data2col=StringSource.fillWithTwoColumns(s,xname,yname);
		}
		else
		{
			data2col=StringSource.fillWithTwoFirstColumns(s);
		}
		if (data2col==null) return;
		x=data2col[0];
		y=data2col[1];
		lastPointIndex = -1;
		return;
	}


	/**
fill with the data of another Signal1D1D
	 */
	public void fillWith(Signal1D1D signal)
	{
		init(signal.getDim());
		for (int i=0;i<getDim();i++) setValue(i,signal.x(i),signal.y(i));
	}


	/**
read a text file with 2 columns containing x and y real data
	 */
	public void fillWithURL(URL url,boolean eliminateComments)
	{
		String s=TextFiles.readFile(url).toString();
		if (eliminateComments) s=StringSource.eliminateComments(s);
		double[][] data2col=StringSource.fillWithTwoFirstColumns(s);
		x=data2col[0];
		y=data2col[1];
		lastPointIndex = -1;
		dim=x.length;
	}

	public void fillWithURL(URL url)
	{
		fillWithURL( url, false);	
	}

	/**equalise to the signal; BEWARE : there is no copy of matrix data : just point to the same*/
	public void transfert(Signal1D1DXY signal)
	{
		x=signal.x;
		y=signal.y;
		lastPointIndex = -1;
	}


	/**
	 * @Deprecated
	 * use _log10TheX()
replace the x values by the Log (base 10) of themselves
	 */
	public void _logTheX()
	{
		double r=Math.log(10);
		for (int i=0;i<getDim();i++) x[i]=Math.log(x[i])/r;
		lastPointIndex = - 1;
	}

	/**
replace the x values by the Log (base 10) of themselves
	 */
	public void _log10TheX()
	{
		double r=Math.log(10);
		for (int i=0;i<getDim();i++) x[i]=Math.log(x[i])/r;
		lastPointIndex = - 1;
	}

	/**
replace the x values by the ln (base e) of themselves
	 */
	public void _lnTheX()
	{
		for (int i=0;i<getDim();i++) x[i]=Math.log(x[i]);
		lastPointIndex = - 1;
	}

	/**
replace the y values by the Log (base 10) of themselves
	 */
	public void  _log10TheY()
	{
		//double r=Math.log(10);
		for (int i=0;i<getDim();i++) y[i]=Math.log10(y[i]);
		lastPointIndex = -1;
		//return this;
	}

	/**
replace the y values by the ln (base e) of themselves
	 */
	public void _lnTheY() 
	{
		for (int i=0;i<getDim();i++) y[i]=Math.log(y[i]);
		lastPointIndex = -1;
		//return this;
	}

	/**
replace the x values by the power (base 10) of themselves
	 */
	public Signal1D1DXY _p10TheX()
	{
		for (int i=0;i<getDim();i++) x[i]=Math.pow(10,x[i]);
		lastPointIndex = -1;
		return this;
	}


	/**
replace the x values by the exponential (base e) of themselves
	 */
	public Signal1D1DXY _expTheX()
	{
		for (int i=0;i<getDim();i++) x[i]=Math.exp(x[i]);
		lastPointIndex = -1;
		return this;
	}
	/**
replace the x values by the exponential (base e) of themselves
	 */
	public void  _expTheY()
	{
		for (int i=0;i<getDim();i++) y[i]=Math.exp(y[i]);
		lastPointIndex = -1;
		//return this;
	}

	/**
swap x and y values
	 */
	public void _swapXY()
	{
		double[] swap=x;
		x=y;
		y=swap;
		lastPointIndex = -1;
	}

	/**
	 * sort the x values
	 */
	public void _sort()
	{
		int n=getDim();
		double[] xvalues=new double[n];
		double[] yvalues=new double[n];
		for (int i=0;i<n;i++)
		{
			xvalues[i]=x(i);
			yvalues[i]=y(i);
		}//sort the xvalues:
		int[] indices=Sorter.sort(xvalues);
		for (int i=0;i<n;i++) this.setValue(i, xvalues[i], yvalues[indices[i]]);
	}
	//
	//
	///** sort the signal with x values i ascending order.
	//Use the shell method, for large array better use quicksort !.
	//From "Numerical Recipies in C" Cambridge
	//*/
	//public void _sortShell()
	////Sorts an array a[1..n] into ascending numerical order by Shell's method (diminishing increment
	////sort).    n is input; a is replaced on output by its sorted rearrangement.
	//{
	//int i,j,inc;
	//double v,vy;
	//int n=getDim()/*-1*/;
	//inc=1;    //Determine the starting increment.
	//do {inc *= 3;inc++;} while (inc <= n);
	//do
	//	{   // Loop over the partial sorts.
	//	inc /= 3;
	////	for (i=inc+1;i<=n;i++)
	//	for (i=inc;i<n;i++)
	//		{ //   Outer loop of straight insertion.
	//		v=x[i];
	//		vy=y[i];
	//		j=i;
	//		while (x[j-inc] > v)
	//			{    //Inner loop of straight insertion.
	//			x[j]=x[j-inc];
	//			y[j]=y[j-inc];
	//			j -= inc;
	//			if (j <= inc) break;
	//			}
	//		x[j]=v;
	//		y[j]=vy;
	//		}
	//	} while (inc > 1);
	//lastPointIndex = -1;
	//}

	/**substract point to point 2 signals, BEWARE: no check of x values consistency */
	public Signal1D1DXY sub(Signal1D1DXY signal2)
	{
		Signal1D1DXY signal=new Signal1D1DXY(getDim());
		for (int i=0;i<getDim();i++) signal.setValue(i,x(i),value(i)-signal2.value(i));
		return signal;
	}
	/**add point to point 2 signals, BEWARE: no check of x values consistency */
	public Signal1D1DXY add(Signal1D1DXY signal2)
	{
		Signal1D1DXY signal=new Signal1D1DXY(getDim());
		for (int i=0;i<getDim();i++) signal.setValue(i,x(i),value(i)+signal2.value(i));
		return signal;
	}



	/**substract point to point 2 signals, BEWARE: no check of x values consistency */
	public void _sub(Signal1D1DXY signal2)
	{
		for (int i=0;i<getDim();i++) this.setValue(i,x(i),value(i)-signal2.value(i));
	}

	/**transfert to a SignalDiscret1D1D (assume a regular repartition of x value !) */
	public SignalDiscret1D1D transfertToDiscret1D1D()
	{
		SignalDiscret1D1D signal=new SignalDiscret1D1D(x(0),x(getDim()-1),getDim());
		for (int i=0;i<getDim();i++) signal.setValue(i,value(i));
		return signal;
	}

	/**
merge another signal to this one by just adding the points (no sort)
@param signal2 the signal to merge
	 */
	public void merge(Signal1D1DXY signal2)
	{
		int dimavant=getDim();
		enlargeBuffer(getDim()+signal2.getDim());
		dim=dimavant+signal2.getDim();
		for (int i=0;i<signal2.getDim();i++) setValue(dimavant+i,signal2.x(i),signal2.value(i));
	}



	/**set the  point of index i to these new co-ordinates, if possible*/
	public void setPoint(int i,double[] p)
	{
		x[i]=p[0];
		y[i]=p[1];
		if (i < lastPointIndex)
		{
			// If a point is inserted, all the curve will be redraw
			//
			lastPointIndex = -1;
		}
	}

	/**
add a new point to the signal
@param nx x  of the new point
@param ny y  of the new point
	 */
	public void addPoint(double nx,double ny)
	{
		if (getDim()==x.length) enlargeBuffer(2*getDim()+1);
		//System.out.println("buffer size"+x.length);
		x[getDim()]=nx;
		y[getDim()]=ny;
		dim++;
	}


	public void addPoint(Vecteur2D p)
	{
		if (getDim()==x.length) enlargeBuffer(2*getDim()+1);
		//System.out.println("buffer size"+x.length);
		x[getDim()]=p.x();
		y[getDim()]=p.y();
		dim++;
	}

	/**
add a new point to the signal after the index index
@param nx x  of the new point
@param ny y  of the new point
@param index index of the point after which the point is added
	 */
	public void addPointAfterIndex(double nx,double ny,int index)
	{
		/*System.out.println(this);
System.out.println("buffer size="+x.length);
System.out.println("dim="+getDim());*/
		if (index < lastPointIndex)
		{
			// If a point is inserted, all the curve will be redraw
			lastPointIndex = -1;
		}
		if (getDim()==0) enlargeBuffer(2);
		else if ((getDim()+1)>=x.length) enlargeBuffer(2*getDim()+1);
		for (int i=getDim();i>=index+2;i--)
		{
			x[i]=x[i-1];
			y[i]=y[i-1];
		}
		x[index+1]=nx;
		y[index+1]=ny;
		dim++;
		// System.out.println(this);
	}



	public void addPointBeforeIndex(double nx,double ny,int index)
	{
		// If a point is inserted, all the curve will be redraw
		lastPointIndex = -1;
		if (getDim()==0) enlargeBuffer(2);
		else if ((getDim()+1)>=x.length) enlargeBuffer(2*getDim()+1);
		for (int i=getDim();i>=index+1;i--)
		{
			x[i]=x[i-1];
			y[i]=y[i-1];
		}
		x[index]=nx;
		y[index]=ny;
		dim++;
		// System.out.println(this);
	}


	private void enlargeBuffer(int newSize)
	{
		//System.out.println(getClass()+" "+"enlarge buffer to "+newSize);
		double[] newx=new double[newSize];
		double[] newy=new double[newSize];
		for (int i=0;i<getDim();i++)
		{
			newx[i]=x[i];
			newy[i]=y[i];
		}
		x=newx;
		y=newy;
	}


	/**
add a new point to the signal
@param p array with x and y  of the new point
@param index index of the point after which the point is added
	 */
	public void addPoint(double[] p)
	{
		addPointAfterIndex(p[0],p[1],getDim()-1);
	}


	/**
add a new point to the signal
@param p array with x and y  of the new point
@param index index of the point after which the point is added
	 */
	public void addPoint(double[] p,int index)
	{
		addPointAfterIndex(p[0],p[1],index);
	}


	/**
remove a point to the signal
@param index index of the point to remove
	 */
	public void removePoint(int index)
	{
		// System.out.println(getClass()+" "+"remove point:"+index);
		if ((index<0)||(index>=getDim())) return;
		lastPointIndex = -1;
		if (index==(getDim()-1)) 
		{
			dim--;
			return;
		}
		for (int i=index;i<getDim()-1;i++)
		{
			x[i]=x[i+1];
			y[i]=y[i+1];
		}
		dim--;
	}

	/**
tranlate the signal in the x direction
@param xoffset : the offset to add to each x  value
	 */
	public void _translate(double xoffset)
	{
		for (int i=0;i<getDim();i++) x[i]+=xoffset;
		lastPointIndex = -1;
	}

	/**
update the state of the object from the data in the string s
@param s the string where is stored the object's state
	 */
	public void updateStateFromString(String s)
	{
		lastPointIndex = -1;
		if ((s==null) ||(s.compareTo("")==0))
		{
			init(0);
			return;
		}
		//drop the first line which is the name and class name
		String subs=s.substring(s.indexOf('\n'));
		//keep up to  ;
		subs=subs.substring(0,subs.indexOf(';'));
		//System.out.println("--------------------------------------");
		//System.out.println(subs);
		double[][] data2col;
		data2col=StringSource.fillWithTwoFirstColumns(subs);
		if (data2col==null) return;
		x=data2col[0];
		y=data2col[1];
		//for (int i=0;i<data2col.length;i++) System.out.println(data2col[0][i]+" "+data2col[1][i]);
		dim=data2col[0].length;
	}


	public void updateFromXML(XMLElement xml) 
	{
		if (xml==null) 
		{
			this.init(0);
			return;
		}
		String data=xml.getContent();	
		Definition def=new Definition(data); 
		//double[][] data2col;
		init(def.dim()/2);
		for (int i=0;i<getDim();i++) 
			this.setValue(i, new Double(def.word(2*i)).doubleValue(), new Double(def.word(2*i+1)).doubleValue());
		//data2col=StringSource.fillWithTwoFirstColumns(data);
		//if (data2col==null) return;
		//x=data2col[0];
		//y=data2col[1];
		//dim=data2col[0].length;
	}

	public void _annulate()
	{
		init(getDim());
	}

	public  void removeAllPoints() { init(0);}

	public void markEnd()
	{
		lastPointIndex=getDim() - 1 ;
	}

	/** 
	 * Tells if the Signal needs to be entirely redrawn or if only the last
	 * part has to be redrawn
	 * 
	 * @return true
	 */
	public boolean needsRedrawAll()
	{
		// TODO
		// On doit retourner true si l'un des nouveaux points est compris entre xmin et xmax
		//
		if (lastPointIndex == -1) 
		{
			return true;
		}

		return false;
	}

	/** 
	 * Froce the signal to be entirely redrawn the next time it will be drawn 
	 */
	public void redrawAll() {lastPointIndex = -1;}

	/** 
	 * Returns the part of the signal thas has not yet been drawn 
	 * 
	 * @return this
	 */
	public Signal1D1D getSignalEnd()
	{
		int newLastPointIndex = getDim() - 1;
		int firstPointIndex;

		if (lastPointIndex == -1) 
		{
			firstPointIndex = 0;
		}
		else
		{
			firstPointIndex = lastPointIndex;
		}

		int endSignalDim = getDim() - firstPointIndex;

		Signal1D1DXY endSignal=new Signal1D1DXY();
		//signal.copy(this);
		endSignal.init(endSignalDim);

		int dataIndex = firstPointIndex;
		for (int i=0; i < endSignalDim ;i++) 
		{
			endSignal.setValue(i,x(dataIndex),value(dataIndex));
			dataIndex++;
		}

		lastPointIndex = newLastPointIndex;

		return endSignal;
	}



	/**add a value to all values*/
	public void _add(double s)
	{
		for (int i=0;i<getDim();i++) setValue(i,value(i)+s);
	}

	/**add a value to all values*/
	public void _addY(double s)
	{
		for (int i=0;i<getDim();i++) setValue(i,value(i)+s);
	}

	/**add a value to all the x values*/
	public void _addX(double s)
	{
		for (int i=0;i<getDim();i++) setValue(i,x(i)+s,value(i));
	}

	/**multiply the x values by a factor*/
	public  void _multiply_x(double coef)
	{
		for (int i=0;i<getDim();i++) x[i]*=coef;
	}

	/**
multiply the y value by a constant
	 */
	public void _mul_y(double d)
	{
		for (int i=0;i<getDim();i++) y[i]*=d;
	}

	/**multiply the x values by a factor*/
	public  void _multiply_y(double coef)
	{
		for (int i=0;i<getDim();i++) y[i]*=coef;
	}


	/**
return the (first from start x value to right or left ) x value for this y value,
linear interpolation, 
the x values must be sorted,
if y value not found return the start value
	 */
	public double solve(double y)
	{
		return solve(y,xmin(),true);
	}


	/**
return the (first from start x value to right or left ) index  i such that
y between y(i) and y(i+1)
the x values must be sorted,
if y value not found return the start value
	 */
	public int solveIndex(double y,int startIndex,boolean toRight)
	{
		double y1,y2;
		boolean found=false;

		int i;
		//af("startIndex="+startIndex+" toRight="+toRight+" getDim "+getDim());
		if (getDim()<2) 
		{
			Util.af("solve: dim<2 ! : " + getDim());
			return 0;
		}
		if (toRight) if (y(getDim()-1)==y) return (getDim()-1);//this case would not reached after...
		//if (!toRight) if (y(0)==y) return (0);
		i=startIndex;
		if (i==getDim()-1) i--;
		do
		{
			//af(i);
			y1=value(i);
			y2=value(i+1);
			//af("i="+i+" y1="+y1+" y2="+y2+" y="+y);
			if (((y1<=y)&&(y2>y))||((y2<y)&&(y1>=y)))
			{
				//af("found");
				found=true;
				break;
			}
			if (toRight) i++;else i--;
		}
		while ((i<getDim()-1)&&(i>0));

		if (found)
		{
			return i;
		}
		else return startIndex;
	}



	/**
solve  y=f(x).linear interpolation betwen the points, the x must be sorted,
Return an vector of Double of the solutions ordered from xmin to xmax
	 */
	public Vector<Double> solveComplet(double y)
	{
		Vector<Double> v=new Vector<Double>();
		double x,x1,x2,y1,y2;
		// System.out.println(getClass()+" xmin= "+xmin+" xmax= "+xmax+" y="+y);
		// double step=sampling();
		x2=x(0);
		y2=y(0);
		for (int i=1;i<getDim();i++)
		{
			x1=x2;
			y1=y2;
			x2=x(i);
			y2=y(i);
			/*	System.out.println(getClass()+" x1= "+x1+" x2= "+x2);
	System.out.println(getClass()+" y1= "+y1+" y2= "+y2);*/
			if (((y1<y)&(y2<y))||((y1>y)&(y2>y))) continue; 
			if (((y1<y)&(y<y2))||((y2<y)&(y<y1))) 
			{
				if (y2!=y1) x= ( x1*Math.abs(y2-y) + x2*Math.abs(y1-y)  ) / Math.abs(y2-y1);
				else x=(x1+x2)/2;
				/*		System.out.println(getClass()+" x1= "+x1+" x2= "+x2);
		System.out.println(getClass()+" y1= "+y1+" y2= "+y2);
		System.out.println(getClass()+" x= "+x);*/
				v.add(new Double(x));
			}
			else if (y1==y) v.add(new Double(x1));
			else if (y2==y) v.add(new Double(x2));
		}
		return v;
	}


	/**
	 * change y into 1/y
	 */
	@Override
	public void _inverse() 
	{
		for (int i=0;i<getDim();i++) setValue(i,1/value(i));
	}


	/**keep only poitns with x between  x1 and x2 included */
	public void _truncx(double x1,double x2)
	{
		int compteur=0;
		int[] indices=new int[getDim()];
		for (int i=0;i<getDim();i++)
		{
			if ((x(i)>=x1)&&(x(i)<=x2)) 
			{
				indices[compteur]=i;
				compteur++;
			}
		}
		for (int i=0;i<compteur;i++)
		{
			setValue(i,x(indices[i]),y(indices[i]));
		}
		dim=compteur;
	}

	/**keep only poitns with y between  y1 and y2 included */
	public void _truncy(double y1,double y2)
	{
		int compteur=0;
		int[] indices=new int[getDim()];
		for (int i=0;i<getDim();i++)
		{
			if ((y(i)>=y1)&&(y(i)<=y2)) 
			{
				indices[compteur]=i;
				compteur++;
			}
		}
		for (int i=0;i<compteur;i++)
		{
			//setValue(i,newx[indices[i]],newy[indices[i]]);
			setValue(i,x(indices[i]),y(indices[i]));
		}
		dim=compteur;
	}


	/**trunc from i1 to i2 included*/
	public void _truncx(int i1,int i2)
	{
		int newindex=0;
		for (int i=i1;i<=i2;i++)
		{
			//System.out.println("x="+x(i)+"   y="+value(i));
			setValue(newindex,x(i),y(i));
			newindex++;
		}
		dim=i2-i1+1;
	}


	//
	///**
	//return a new SignalDiscret1D1D after smoothing with a uniform kernel of size kern
	//*/
	//public Signal1D1DXY smoothing(int dimkern)
	//{
	//Signal1D1DXY newsig=new Signal1D1DXY(getDim());
	//for (int i=0;i<getDim()-dimkern+1;i++)
	//	{
	//	double average=mean(i,i+dimkern-1);
	//	newsig.setValue(i,this.x(i),average);
	//	}
	////set the last values to the last mean
	//for (int i=getDim()-dimkern+1;i<getDim();i++)
	//	{
	//	newsig.setValue(i,x(getDim()-dimkern),y(getDim()-dimkern));
	//	}
	//return newsig;
	//}

	public Signal1D1DXY derivate()
	{
		Signal1D1DXY der=new Signal1D1DXY(getDim()-1);
		for (int i=0;i<getDim()-1;i++)
		{
			der.setValue(i, x(i), (y(i+1)-y(i)) /  (x(i+1)-x(i))  );
		}	
		return der;
	}



	public void _averageForSameX(boolean log)
	{
		copy(averageForSameX( log) )	;
	}


	/**
	 * average all the y values having the same x value
	 * @param log if true average the log values
	 * @return a new signal
	 */
	public Signal1D1DXY averageForSameX(boolean log) 
	{
		if (getDim()<=1) return this;
		this._sort();//sort the x values

		//count the x values and average at the same time
		double[] xx=new double[getDim()];
		double[] yy=new double[getDim()];
		int[] nb=new int[getDim()];//nb of occurences for each x
		int c=0;
		double previousx=x(0);
		xx[0]=this.x(0);
		if (!log) yy[0]=this.y(0);else yy[0]=Math.log(this.y(0));
		nb[0]=1;
		for (int i=1;i<getDim();i++)  
		{
			if (x(i)==previousx) 
			{
				if (!log) yy[c]+=this.y(i); else  yy[c]+=Math.log(this.y(i));
				nb[c]++;
				//af(i+" "+x(i)+" same as previous, c="+c+" x="+xx[c]+" y="+yy[c]+"  nb="+nb[c]);
			}
			else 
			{
				c++;
				if (!log) yy[c]=this.y(i);else yy[c]=Math.log(this.y(i));
				nb[c]=1;
				xx[c]=this.x(i);
				previousx=x(i);
				//af(i+" "+x(i)+" != as previous, c="+c+" x="+xx[c]+" y="+yy[c]+"  nb="+nb[c]);
			}
		}
		//af((c+1)+" differents x values");
		//for (int i=0;i<c+1;i++) af("xvalue "+xx[i]+" yvalue:"+yy[i]+"  nbvalues: "+nb[i]);
		//create the new signal and finish  the averaging
		Signal1D1DXY s2=new Signal1D1DXY(c+1);
		for (int i=0;i<s2.getDim();i++)  
		{
			if (!log) s2.setValue(i, xx[i], yy[i]/nb[i]); else s2.setValue(i, xx[i], Math.exp(yy[i]/nb[i]));
		}
		return s2;
	}


	public void _averageForSameXFiltering(boolean log,double  thresholdInSigma)
	{
		copy(averageForSameXFiltering( log,  thresholdInSigma) )	;
	}

	/**
	 * average all the y values having the same x value
	 * @param log if true average the log values
	 * @param thresholdInSigma nb of sigmas (rms) up to which you keep the value
	 * @return a new signal
	 */
	public Signal1D1DXY averageForSameXFiltering(boolean log,double  thresholdInSigma) 
	{
		if (getDim()<=1) return this;
		this._sort();//sort the x values
		if (log) this._lnTheY();

		//count the x values that are the same
		int[] nb=new int[getDim()];//nb of occurences for each x
		int c=0;
		double previousx=x(0);
		nb[0]=1;
		for (int i=1;i<getDim();i++)  
		{
			if (x(i)==previousx) 
			{
				nb[c]++;
				//af(i+" "+x(i)+" same as previous, c="+c+"  nb="+nb[c]);
			}
			else 
			{
				c++;
				nb[c]=1;
				previousx=x(i);
				//af(i+" "+x(i)+" != as previous, c="+c+"  nb="+nb[c]);
			}
		}
		//af((c+1)+" differents x values");



		//create the new signal and finish  the averaging
		Signal1D1DXY s2=new Signal1D1DXY(c+1);
		int imin=0;
		for (int i=0;i<s2.getDim();i++)  
		{
			int imax=imin+nb[i]-1;
			double filteredMean=this.meanFiltered(imin, imax, thresholdInSigma);
			s2.setValue(i, this.x(imin), filteredMean); 
			imin=imax+1;
		}

		if (log) s2._expTheY();

		return s2;
	}



	/**
	 * Does the signal contain a given point?
	 * @param xToFind
	 * @param yToFind
	 * @return true if the signal contains the point
	 */
	public boolean containsPoint(double xToFind, double yToFind){
		boolean containsPoint = false;
		for (int i=0; i<getDim(); i++){
			if ((x[i]==xToFind) && (y[i]==yToFind)){
				containsPoint = true;
			}
		}
		return containsPoint;
	}

	/**
	 * Does the signal contain a given point?
	 * @param xToFind
	 * @return true if the signal contains the point
	 */
	public boolean containsPointAtX(double xToFind){
		boolean containsPoint = false;
		for (int i=0; i<getDim(); i++){
			if (x[i]==xToFind){
				containsPoint = true;
			}
		}
		return containsPoint;
	}


	/**
	 * return a interpolated new signal with the samples spread in log in the x axis
	 * @param newdim
	 * @return
	 */
	public void  _resampleInLogxWithLinearInterpolation(int newdim)
	{
		Signal1D1DLogx s=resampleInLogxWithLinearInterpolation( newdim);
		this.copy(s);
	}

	/**
	 * return a interpolated new signal with the samples equally spread  in the x axis
	 * @param newdim
	 * @return
	 */
	public void  _resampleWithLinearInterpolation(int newdim)
	{
		Signal1D1D s=resampleWithLinearInterpolation( newdim);
		this.copy(s);
	}


	public void concatenate(Signal1D1D sig)
	{
		Signal1D1DXY newsig=Signal1D1D.concatenate(this, sig);
		this.copy(newsig);
	}
	/**
	 * invert x and y
	 */
	public void _transpose() 
	{
		double x,y;
		for (int i=0;i<this.getDim();i++)
		{
			x=this.x(i);
			y=y(i);
			this.setValue(i, y, x);
		}
	}
	
	/**
	 * set 1 if y>val 0 if not
	 * @param val
	 */
	public void _threshold(double val)
	{
		for (int i=0;i<this.getDim();i++)
		{
			if (y(i)>=val) this.setValue(i,1);
			else this.setValue(i,0);
		}
		
	}

}


