package com.photonlyx.toolbox.math.signal;
import java.awt.Color;

import com.photonlyx.toolbox.math.image.CImage;


public class Signal2D1DRGB
{
private Signal2D1D red=new Signal2D1D();
private Signal2D1D green=new Signal2D1D();
private Signal2D1D blue=new Signal2D1D();

/**construct the RGB  image with a java image*/
public Signal2D1DRGB()
{
_init(1,1);
}
/**construct the RGB  image with a java image*/
public Signal2D1DRGB(int w,int h)
{
_init(w,h);
}

/**construct the RGB  image with a java image*/
public Signal2D1DRGB(int[] pix,int w)
{
int h=pix.length/w;
_init(w,h);
_fillImage(pix, w);
}

/**construct the RGB  with the 3 channels*/
public Signal2D1DRGB(Signal2D1D r,Signal2D1D g,Signal2D1D b)
{
red=r;
green=g;
blue=b;
}

public void _init(int w,int h)
{
red.init(0, w, 0, h, w, h);
green.init(0, w, 0, h, w, h);
blue.init(0, w, 0, h, w, h);

}


public void _fillImage(int[] pix,int w)
{
for (int i=0;i<pix.length;i++)
	{
	Color c=new Color(pix[i]);
	int y=i/w;int x=i-w*y;//System.out.println("("+x+","+y+")");
	int y1=red.dimy()-1-y;//mirror around horizontal
	if ((x>0)&(x<red.dimx())&(y>0)&(y<red.dimy())) 
		{
		red.setValue(x, y1, c.getRed()+red.getValue(x,y1));
		green.setValue(x, y1, c.getGreen()+green.getValue(x,y1));
		blue.setValue(x, y1, c.getBlue()+blue.getValue(x,y1));
		}	
	}
}
public void _multiply(double d)
{
red._multiply(d);
green._multiply(d);
blue._multiply(d);
}
public void _multiplyWithSaturation(double val,double minValue,double maxValue)
{
red._multiplyWithSaturation(val,minValue,maxValue);
green._multiplyWithSaturation(val,minValue,maxValue);
blue._multiplyWithSaturation(val,minValue,maxValue);
}

public void _addWithSaturation(double val,double minValue,double maxValue)
{
red._addWithSaturation(val,minValue,maxValue);
green._addWithSaturation(val,minValue,maxValue);
blue._addWithSaturation(val,minValue,maxValue);
}


public void _offset(int dx,int dy)
{
red._offset(dx,dy);
green._offset(dx,dy);
blue._offset(dx,dy);
}

public void _add(Signal2D1DRGB im)
{
red._add(im.red);
green._add(im.green);
blue._add(im.blue);
}

/**
 * 
 * @param da in rad
 */
public void _rotateAroundCenter(double da)
{
red._rotateAroundCenter(da);
green._rotateAroundCenter(da);
blue._rotateAroundCenter(da);
}


public void transfertToCImage(CImage cim)
{
int w=red.dimx();
int h=red.dimy();
int[] pix=new int[w*h];
for (int i=0;i<w;i++)
	for (int j=0;j<h;j++)
		{	
		pix[(h-1-j)*w+i]=(255 << 24)| 
				((int)red.getValue(i,j) << 16) |
				((int)green.getValue(i,j) << 8) | 
				((int)blue.getValue(i,j)) ;
		}
cim.setPixelsDirect(pix, w, h);
}



public Signal2D1D getRedChannel()
{
return red;
}
public Signal2D1D getGreenChannel()
{
return red;
}
public Signal2D1D getBlueChannel()
{
return red;
}


public void _divide(Signal2D1DRGB s2)
{
red._divide(s2.getRedChannel());
green._divide(s2.getGreenChannel());
blue._divide(s2.getBlueChannel());

}

public void _setMaxToOne()
{
double max=Math.max(red.maxValue(), green.maxValue());
max=Math.max(max, blue.maxValue());
red._multiply(1/max);
green._multiply(1/max);
blue._multiply(1/max);
}

public Signal2D1DRGB copy()
{
Signal2D1DRGB s2=new Signal2D1DRGB(this.red.copy(),this.green.copy(),this.blue.copy());
return s2;
}





}
