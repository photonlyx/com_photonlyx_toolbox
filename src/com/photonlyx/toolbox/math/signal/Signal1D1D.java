// Author Laurent Brunel

package com.photonlyx.toolbox.math.signal;

import java.text.NumberFormat;
import java.util.Locale;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.Util;
import com.photonlyx.toolbox.math.analyse.FitLin;
import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.math.function.usual.Line;
import com.photonlyx.toolbox.util.Messager;


/**
represent a one dimension signal of 1D data
 */
public abstract class Signal1D1D extends Signal implements Signal1D1DSource,XMLstorable
{
	public  abstract int getDim();

	/**
	 * return the raw x value of index i
	 * if the signal is points like give the x of the point</p>
	 * if the signal is cell like, give the x of the left point of cell 
	 * */
	public abstract  double x(int i);

	/**
	 * return the x value for the sample of index i</p>
	 * if the signal is points like give the x of the point</p>
	 * if the signal is cell like, give the middle point of cell 
	 * */
	public abstract  double xSample(int i);
	/**
return the value at the cell i. If i is out of range,return 0
	 */
	public  abstract double value(int i);
	/**
return the value at the cell i. If i is out of range,return 0
	 */
	public  abstract double y(int i);

	/**
	 * get the fist value closer to tolerance else return 0
	 * @param x
	 * @param tolerance
	 * @return
	 */
	public  double value(double x,double tolerance)
	{
		for (int i=0;i<getDim();i++)
		{
			if (Math.abs(x(i)-x)<=tolerance) return value(i);
		}
		return 0;
	}

	/** set a value*/
	public  abstract void setValue(int i,double v);

	/**return the x min value*/
	public  abstract double xmin();

	/**return the x max value*/
	public  abstract double xmax();

	public abstract boolean isPointsSignal();


	/**return the max y value()*/
	public double ymax()
	{
		double r=-1e300;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i);
			if (v>r) r=v;
		}
		return r;
	}


	/**return the min y value()*/
	public double ymin()
	{
		double r=1e300;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i);
			if (v<r) r=v;
		}
		return r;
	}

	/**
return the index of the max y value()
	 */
	public int ymaxIndex()
	{
		double r=-1e300;
		int index=-1;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i);
			if (v>r)
			{
				r=v;
				index=i;
			}
		}
		return index;
	}


	/**
return the index of the min y value()
	 */
	public int yminIndex()
	{
		double r=1e300;
		double v;
		int index=-1;
		for (int i=0;i<getDim();i++)
		{
			v=y(i);
			if (v<r) 
			{
				r=v;
				index=i;
				//af(i+"   "+index+" "+v);
			}
		}
		return index;
	}

	/**return the min y positive alue()*/
	public double xminStrictPositive()
	{
		double r=1e300;
		for (int i=0;i<getDim();i++)
		{
			double v=x(i);
			if ((v<r)&&(v>0)) r=v;
		}
		return r;
	}

	/**return the min y positive alue()*/
	public double yminStrictPositive()
	{
		double r=1e300;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i);
			if ((v<r)&&(v>0)) r=v;
		}
		return r;
	}
	//public void fillWithFonction();
	//public void fillWithFile(String chemin,String filename,int dim,int start);

	/** return the x values array */
	public  abstract double[] getXarray();

	/** return the y values array */
	public  abstract double[] getYarray();

	/**
tranlate the signal in the x direction
@param xoffset : the offset to add to each x  value
	 */
	public  abstract void _translate(double xoffset);

	/**multiply the x values by a factor*/
	public abstract void _multiply_x(double coef);

	/**
	 * copy the signal if possible
	 * @param signal
	 */
	public abstract void copy(Signal1D1D signal);

	public Signal1D1D getSignal1D1D()
	{
		return this;
	}


	/**return the distance in the x axis between 2 samples*/
	public double sampling(int i)
	{
		return x(i+1)-x(i);
	}

	/**return the average y value */
	public double mean()
	{
		return meanY();
	}

	/**return the average y value,wtih filtering */
	public double meanFiltered(double thresholdInSigma)
	{
		return meanFiltered(0,getDim()-1,thresholdInSigma);
	}

	/**return the average y value */
	public double meanY()
	{
		double r=0;
		for (int i=0;i<getDim();i++) r+=value(i);
		//for (int i=0;i<getDim();i++) System.out.println(value(i));
		return r/(getDim());
	}

	/**return the average x value */
	public double meanX()
	{
		double r=0;
		for (int i=0;i<getDim();i++) r+=x(i);
		//for (int i=0;i<getDim();i++) System.out.println(value(i));
		return r/(getDim());
	}


	/**return the average value from the indexes imin to imax included*/
	public double mean(int imin,int  imax)
	{
		double r=0;
		for (int i=imin;i<=imax;i++) r+=value(i);
		return r/(imax-imin+1);
	}


	/**return the average value from the indexes imin to imax included*/
	public double mean(double xmin,double  xmax)
	{
		Signal1D1DXY s=truncx(xmin,xmax);
		if (s.getDim()!=0) return truncx(xmin,xmax).mean();
		else return (this.valueInterpol(xmin)+this.valueInterpol(xmax))/2;
	}



	/**return the average value from the indexes imin to imax included*/
	public double meanFiltered(int imin,int  imax,double thresholdInSigma)
	{
		//first calc the mean and standard deviation
		int n=imax-imin+1;
		double r1=0;
		double r2=0;
		double r;
		for (int i=imin;i<=imax;i++) 
		{
			r=value(i);
			r1+=r;
			r2+=r*r;
		}
		double mean=r1/n;
		double sigma=Math.sqrt(r2/n-mean*mean);//ecart type, standard deviation
		//af("mean="+mean+" sigma="+sigma);
		//then calc the mean removing the "exotic" values...
		r1=0;
		int c=0;
		double delta;
		for (int i=imin;i<=imax;i++)
		{
			r=value(i);
			delta=r-mean;
			if (delta<=thresholdInSigma*sigma) 
			{
				r1+=r;
				c++;
			}
		}
		double filteredMean=r1/c;
		return  filteredMean;
	}

	/**
	 * the root mean square around the mean value.
	 * @return
	 */
	public double rms()
	{
		double s=0;
		double m=mean();
		for (int i=0;i<getDim();i++)
		{
			double v=value(i)-m;
			s+=v*v;
		}
		return Math.sqrt(s/getDim());
	}

	/**
	 * the root mean square around the mean value.
	 * @param the mean value
	 * @return
	 */
	public double rms(double mean)
	{
		double s=0;
		double m=mean;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i)-m;
			s+=v*v;
		}
		return Math.sqrt(s/getDim());
	}

	/**return the distance L2 between the 2 signals (they must have the same sampling !!!!)*/
	public double D2(Signal1D1D s2)
	{
		double s=0;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i)-s2.value(i);
			s+=v*v;
		}
		return Math.sqrt(s/getDim());
	}

	/**return the distance L2 between this signal and the null signal*/
	public double D2()
	{
		double s=0;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i);
			s+=v*v;
		}
		return Math.sqrt(s/getDim());
	}


	/**gives the surface under the curve,linear approx between 2 points, assume that the x are sorted */
	public double integral()
	{
		double s=0;
		for (int i=0;i<getDim()-1;i++)
		{
			s+=(y(i)+y(i+1))*(x(i+1)-x(i));
		}
		return s/2;
	}


	/**gives the surface under the curve from indices imin to imax included , linear approx between 2 points, assume that the x are sorted */
	public double integrale(int imin,int imax)
	{
		double s=0;
		for (int i=imin;i<imax-1;i++)
		{
			s+=(y(i)+y(i+1))*(x(i+1)-x(i));
		}
		return s/2;
	}



	/**return the moment of order 1 */
	public double barycentre()
	{
		double r=0,s=0;
		for (int i=0;i<getDim();i++)
		{
			r+=x(i)*value(i);
			s+=value(i);
		}
		return r/s;
	}

	/**return the width of the profile (RMS of x values weighted by the values), center assumed to be at x=0 */
	public double calcProfileWidthUsingRMS()
	{
		double r=0,s=0;
		for (int i=0;i<getDim();i++)
		{
			r+=x(i)*x(i)*value(i);
			s+=value(i);
		}
		return Math.sqrt(r)/s;
	}

	public double xAbsoluteMinimum()
	{
		double r=+1e300;
		int index=-1;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i);
			if (v<r)
			{
				r=v;
				index=i;
			}
		}
		return x(index);
	}


	/** multiply all values by a factor*/
	public void _multiply(double s)
	{
		for (int i=0;i<getDim();i++) setValue(i,value(i)*s);
	}


	/**
multiply the y value by a function
	 */
	public void _mul_y(Function1D1D f)
	{
		for (int i=0;i<getDim();i++) setValue(i,value(i)*f.f(this.x(i)));
	}

	/**
divide the y value by a function
	 */
	public void _div_y(Function1D1D f)
	{
		for (int i=0;i<getDim();i++) setValue(i,value(i)/f.f(this.x(i)));
	}

	/**return the closest point of the signal from this point*/
	public int getNearestPoint(double[] point)
	{
		double distance2=1e30;
		int index=0;
		for (int i=0;i<getDim();i++) 
		{
			double distance2c=Math.pow(point[0]-x(i),2)+Math.pow(point[1]-y(i),2);
			if (distance2c<distance2) 
			{
				distance2=distance2c;
				index=i;
			}
		}
		return index;
	}

	/**return the closest point in x of the signal from this point*/
	public int getNearestXPoint(double[] point)
	{
		double distance2=1e30;
		int index=0;
		for (int i=0;i<getDim();i++)
		{
			double distance2c=Math.abs(point[0]-x(i));
			if (distance2c<distance2)
			{
				distance2=distance2c;
				index=i;
			}
		}
		return index;
	}

	/**return the closest point of the signal from this point*/
	public double[] getNearestExtraplatedPoint(double[] point)
	{
		double distance2=1e30;
		int index=0;
		double[] segstart = {x(0),y(0)};
		double[] segend = new double[2];
		double[] result = new double[2];
		double[] intersection = new double[2];
		for (int i=1;i<getDim();i++)
		{
			segend[0]=x(i);
			segend[1]=y(i);
			double distance2c=segdist2D(segstart,segend,point,intersection);
			if (distance2c<distance2)
			{
				distance2=distance2c;
				result[0] = intersection[0];
				result[1] = intersection[1];
			}
			segstart[0]=segend[0];
			segstart[1]=segend[1];
		}
		return result;
	}


	/**return the point that has the closest x from the given x value*/
	public int getNearestXPoint(double x)
	{
		double distance=1e30;
		int index=0;
		for (int i=0;i<getDim();i++)
		{
			double distancec=Math.abs(x-x(i));
			if (distancec<distance)
			{
				distance=distancec;
				index=i;
			}
		}
		return index;
	}

	/**return the point that has exactly  the given x value (-1 if not found)*/
	public int getPointOfY(double y)
	{
		int index=-1;
		for (int i=0;i<getDim();i++)
		{
			if (y(i)==y) index= i;
		}
	return index;
	}
	
	
	/**return the point of index i as a double array*/
	public double[] getPoint(int i)
	{
		double[] p=new double[2];
		p[0]=x(i);
		p[1]=value(i);
		return p;
	}

	/**set the  point of index i to these new co-ordinates, if possible*/
	public abstract void setPoint(int i,double[] p);





	/**trunc from x1 to x2 return a new Signal1D1D*/
	public Signal1D1DXY truncx(double x1,double x2)
	{
		int compteur=0;
		//System.out.println(getClass()+" "+getNode().getName()+" truncx x1="+x1+" x2="+x2);
		for (int i=0;i<getDim();i++)
		{
			if ((x(i)>=x1)&&(x(i)<=x2)) compteur++;
		}
		//System.out.println(getClass()+" "+getNode().getName()+" "+compteur+" are good");
		Signal1D1DXY newsig=new Signal1D1DXY(compteur);
		//System.out.println(getClass()+" "+getNode().getName()+" new dim="+compteur);
		int newindex=0;
		for (int i=0;i<getDim();i++)
		{
			if ((x(i)>=x1)&&(x(i)<=x2))
			{
				//System.out.println("x="+x(i)+"   y="+value(i));
				newsig.setValue(newindex,x(i),value(i));
				newindex++;
			}
			//else System.out.println("remove x="+x(i)+"   y="+value(i));
		}
		return newsig;
	}


	/**trunc from i1 to i2 included, retrun new signal*/
	public Signal1D1DXY truncx(int i1,int i2)
	{
		Signal1D1DXY newsig=new Signal1D1DXY(i2-i1+1);
		int newindex=0;
		for (int i=0;i<getDim();i++)
		{
			if ((i>=i1)&&(i<=i2))
			{
				//System.out.println("x="+x(i)+"   y="+value(i));
				newsig.setValue(newindex,x(i),value(i));
				newindex++;
			}
			//else System.out.println("remove x="+x(i)+"   y="+value(i));
		}
		return newsig;
	}


	/**keep the points which y is in [y1,y2]*/
	public Signal1D1DXY truncy(double y1,double y2)
	{
		int compteur=0;
		//System.out.println(getClass()+" "+getNode().getName()+" truncx x1="+x1+" x2="+x2);
		for (int i=0;i<getDim();i++)
		{
			if ((value(i)>=y1)&&(value(i)<=y2)) compteur++;
		}
		//System.out.println(getClass()+" "+getNode().getName()+" "+compteur+" are good");
		Signal1D1DXY newsig=new Signal1D1DXY(compteur);
		//System.out.println(getClass()+" "+getNode().getName()+" new dim="+compteur);
		int newindex=0;
		for (int i=0;i<getDim();i++)
		{
			if ((value(i)>=y1)&&(value(i)<=y2))
			{
				//System.out.println("x="+x(i)+"   y="+value(i));
				newsig.setValue(newindex,x(i),value(i));
				newindex++;
			}
			//else System.out.println("remove x="+x(i)+"   y="+value(i));
		}
		return newsig;
	}



	/**keep the points which y is strictly in of ]y1,y2[*/
	public Signal1D1DXY truncyStrictly (double y1,double y2)
	{
		int compteur=0;
		//System.out.println(getClass()+" "+getNode().getName()+" truncx x1="+x1+" x2="+x2);
		for (int i=0;i<getDim();i++)
		{
			if ((value(i)>y1)&&(value(i)<y2)) compteur++;
		}
		//System.out.println(getClass()+" "+getNode().getName()+" "+compteur+" are good");
		Signal1D1DXY newsig=new Signal1D1DXY(compteur);
		//System.out.println(getClass()+" "+getNode().getName()+" new dim="+compteur);
		int newindex=0;
		for (int i=0;i<getDim();i++)
		{
			if ((value(i)>y1)&&(value(i)<y2))
			{
				//System.out.println("x="+x(i)+"   y="+value(i));
				newsig.setValue(newindex,x(i),value(i));
				newindex++;
			}
			//else System.out.println("remove x="+x(i)+"   y="+value(i));
		}
		return newsig;
	}


	/** 
	 * A partir de tous les points contenus dans un rectangle donne
	 * trouve les deux points de x minimum et de x maximum et
	 * retourne un truncx entre les 2 valeurs x minimum et x maximum.
	 * 
	 * @param minx x min du rectangle
	 * @param maxx x max du rectangle 
	 * @param miny y min du rectangle 
	 * @param maxy y max du rectangle 
	 * @return vous savez quoi
	 */
	public Signal1D1DXY truncXRectangle(double minx, double maxx, double miny, double maxy)
	{
		double finalminx = maxx;
		double finalmaxx = minx;

		for (int i=0;i<getDim();i++)
		{
			if (x(i) >= minx && x(i) <= maxx && value(i) >= miny && value(i) <= maxy)
			{
				if (x(i) <= finalminx)
				{
					finalminx = x(i);
				}

				if (x(i) >= finalmaxx)
				{
					finalmaxx = x(i);
				}
			}
		}

		if (finalmaxx <= finalminx)
		{
			return new Signal1D1DXY();
		}

		return truncx(finalminx, finalmaxx);
	}


	/**
	 * concatenate s1 and s2 in the new returned signal
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static Signal1D1DXY concatenate(Signal1D1D s1,Signal1D1D s2)
	{
		Signal1D1DXY newsig=new Signal1D1DXY(s1.getDim()+s2.getDim());
		for (int i=0;i<s1.getDim();i++) newsig.setValue(i,s1.x(i),s1.value(i));
		for (int i=0;i<s2.getDim();i++) newsig.setValue(s1.getDim()+i,s2.x(i),s2.value(i));
		return newsig;
	}



	/**
update the state of the object from the data in the string s
@param s the string where is stored the object's state
	 */
	public abstract void updateStateFromString(String s);



	/**transfert to a new Signal1D1DXY */
	public Signal1D1DXY transfertTo1D1DXY()
	{
		Signal1D1DXY signal=new Signal1D1DXY(getDim());
		for (int i=0;i<getDim();i++) signal.setValue(i,x(i),value(i));
		return signal;
	}



	/**transfert to a new Signal1D2D */
	public Signal1D2D transfertTo1D2D()
	{
		Signal1D2D signal=new Signal1D2D(0,1,getDim());
		for (int i=0;i<getDim();i++) signal.setValue(i,x(i),value(i));
		return signal;
	}





	/** 
	 * Tells if the Signal needs to be entirely redrawn or if only the last
	 * part has to be redrawn
	 * 
	 * @return true
	 */
	public boolean needsRedrawAll()
	{
		return true;
	}

	/** 
	 * Froce the signal to be entirely redrawn the next time it will be drawn 
	 */
	public void redrawAll() {}

	/** 
	 * Returns the part of the signal thas has not yet been drawn 
	 * 
	 * @return this
	 */
	public Signal1D1D getSignalEnd()
	{
		return this;
	}

	public void markEnd()
	{
	}


	/**add a value to all values*/
	public abstract void _add(double s);

	/**
	 * add a linear function 
	 * @param a
	 */
	public void _addSlope(double a)
	{
		for (int i=0;i<getDim()-1;i++)
		{
			this.setValue(i,y(i)+a*x(i));
		}

	}

	/**
	 * add a signal (if having same nb of points)
	 * @param s
	 * @return
	 */
	public Signal1D1DXY add(Signal1D1D s)
	{
		if (this.getDim()!=s.getDim()) 
		{
			Messager.messErr("can't add 2 Signal1D1D having different nb of points");
			return null;
		}
		Signal1D1DXY s2=new Signal1D1DXY(s.getDim());
		for (int i=0;i<this.getDim();i++)
		{
			s2.setValue(i, s.x(i), this.y(i)+s.y(i));
		}
		return s2;
	}


	/**
return the value at the x position 
approximated with a linear interpolation,the x must be sorted
0 is returned if out of boundaries
	 */
	public double valueInterpol(double x)
	{
		double y=0;
		if (x<=xmin()) return y(0);
		if (this.getDim()==0) return 0;
		if (x>xmax()) return y(this.getDim()-1);
		for (int i=0;i<getDim()-1;i++)
		{
			double x1=x(i);
			double x2=x(i+1);
			//System.out.println(" x1= "+x1+" x2= "+x2);
			if ( ((x1<x)&(x<x2)) || ((x2<x)&(x<x1)) )
			{
				double y1=y(i);
				double y2=y(i+1);
				y=y1+(y2-y1)/(x2-x1)*(x-x1);
				//System.out.println(" y1= "+y1+" y2= "+y2);
				//System.out.println(" x= "+x+" y= "+y	);
				break;
			}
			else if (x==x1)
			{
				double y1=y(i);
				y=y1;
				break;
			}
			else if (x==x2)
			{
				double y2=y(i+1);
				y=y2;
				break;
			}
		}
		return y;
	}

	/**
	 * 
	 * @return the slope of a fitted line (BUG? to be tested...)
	 */
	public double meanSlope()
	{
		double[] abr=new double[3];
		FitLin.fitLine(getXarray(),getYarray(),abr);
		return abr[0];
	}


	/**put the max at the value 1*/
	//public void normalise()
	//{
	//double s=ymax();
	//_multiply(1/s);
	//
	//}


	/**put the max at the value 1*/
	public Signal1D1D normalise()
	{
		double s=ymax();
		return multiply(1/s);

	}


	public  Signal1D1D multiply(double d)
	{
		Signal1D1DXY s=this.transfertTo1D1DXY();
		s._multiply(d);
		return s;
	}


	public abstract void _inverse();




	/**
replace the y values by the Log (base 10) of themselves
	 */
	public void  _p10TheY()
	{
		for (int i=0;i<getDim();i++) setValue(i,com.photonlyx.toolbox.math.Util.p10(y(i)));
	}

	/**
replace the x values by the power (base e) of themselves
	 */
	public void _expTheY()
	{
		for (int i=0;i<getDim();i++) this.setValue(i, Math.exp(this.value(i)));
	}


	/**
replace the y values by the ln (base e) of themselves
@Deprecated (use log10TheY)
	 */
	public void _logTheY() 
	{
		for (int i=0;i<getDim();i++) this.setValue(i, Math.log(this.value(i)));
	}

	/**
replace the y values by the log (base10) of themselves
	 */
	public void _log10TheY() 
	{
		for (int i=0;i<getDim();i++) this.setValue(i, Math.log10(this.value(i)));
	}

	/**
	 * return a interpolated new signal
	 * @param newdim
	 * @return
	 */
	public SignalDiscret1D1D resampleWithLinearInterpolation(int newdim)
	{
		SignalDiscret1D1D s=new SignalDiscret1D1D(xmin(),xmax(),newdim);
		for (int i=0;i<s.getDim();i++)
		{
			s.setValue(i, this.valueInterpol(s.x(i)) );
		}
		return s;
	}



	/**
	 * return a interpolated new signal with the samples spread in log in the x axis
	 * @param newdim
	 * @return
	 */
	public Signal1D1DLogx resampleInLogxWithLinearInterpolation(int newdim)
	{
		Signal1D1DLogx s=new Signal1D1DLogx(xmin(),xmax(),newdim);
		for (int i=0;i<s.getDim()-1;i++)
		{
			s.setValue(i, this.valueInterpol(s.x(i)) );
		}
		if(s.getDim()>0) s.setValue(s.getDim()-1, this.y(this.getDim()-1) );
		return s;
	}


	/**
return a new SignalDiscret1D1D after smoothing with a uniform kernel of size kern
	 */
	//public abstract SignalDiscret1D1D smoothing(int dimkern);



	/**
return a new SignalDiscret1D1D after smoothing with a uniform kernel of size kern
	 */
	//public SignalDiscret1D1D smoothing(int dimkern)
	//{
	//SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
	//for (int i=0;i<getDim()-dimkern+1;i++)
	//	{
	//	double average=mean(i,i+dimkern-1);
	//	newsig.setValue(i,average);
	//	}
	////set the last values to the last mean
	//for (int i=getDim()-dimkern+1;i<getDim();i++)
	//	{
	//	newsig.setValue(i,value(getDim()-dimkern));
	//	}
	//return newsig;
	//}


	/**
return a new Signal1D1D after smoothing with a uniform kernel of size kern
	 */
	public Signal1D1D smooth(int dimkern)
	{
		Signal1D1DXY newsig=new Signal1D1DXY(getDim());
		for (int i=0;i<getDim()-dimkern+1;i++)
		{
			double average=mean(i,i+dimkern-1);
			newsig.setValue(i,this.x(i),average);
		}
		//set the last values with a kernel at the left
		for (int i=getDim()-dimkern+1;i<getDim();i++)
		{
			int imin=Math.max(0, i-dimkern+1);
			double average=mean(imin,i);
			newsig.setValue(i,this.x(i),average);
		}
		return newsig;
	}



	/**
 smooth with a uniform kernel of half size halfKern
	 */
	public void _smooth(int halfKern)
	{
		if (halfKern*2+1>getDim()) //kernel bigger 
		{
			double average=mean();
			for (int i=0;i<getDim();i++) setValue(i,average);
			return;
		}
		//Signal1D1DXY newsig=new Signal1D1DXY(getDim());
		double[] newy=new double[getDim()];
		int i1,i2;
		int n=getDim();
		for (int i=0;i<n-1;i++)
		{
			i1=i-halfKern;
			i2=i+halfKern;
			if (i1<0)i1=0;
			if (i2>=n) i2=n-1;
			double average=mean(i1,i2);
			newy[i]=average;
		}
		for (int i=0;i<newy.length;i++) setValue(i,newy[i]);
	}


	/**
smooth with a uniform kernel of size kern
	 */
	public void _smoothOLD(int dimkern)
	{
		if (dimkern>getDim())
		{
			double average=mean();
			for (int i=0;i<getDim();i++) setValue(i,average);
			return;
		}
		//Signal1D1DXY newsig=new Signal1D1DXY(getDim());
		double[] newy=new double[getDim()];
		for (int i=0;i<getDim()-dimkern+1;i++)
		{
			double average=mean(i,i+dimkern-1);
			newy[i]=average;
		}
		//set the last values with a kernel at the left
		for (int i=getDim()-dimkern+1;i<getDim();i++)
		{
			int imin=Math.max(0, i-dimkern+1);
			double average=mean(imin,i);
			newy[i]=average;
		}
		for (int i=0;i<newy.length;i++) setValue(i,newy[i]);
	}





	/**
 smooth with a uniform kernel of size kern with filtering
	 */
	public void _smoothFilteredOLD(int dimkern,double thresholdInSigma)
	{
		if (dimkern>getDim())
		{
			double average=meanFiltered(thresholdInSigma);
			for (int i=0;i<getDim();i++) setValue(i,average);
			return;
		}
		//Signal1D1DXY newsig=new Signal1D1DXY(getDim());
		double[] newy=new double[getDim()];
		for (int i=0;i<getDim()-dimkern+1;i++)
		{
			double average=meanFiltered(i,i+dimkern-1,thresholdInSigma);
			newy[i]=average;
		}
		//set the last values with a kernel at the left
		for (int i=getDim()-dimkern+1;i<getDim();i++)
		{
			int imin=Math.max(0, i-dimkern+1);
			double average=meanFiltered(imin,i,thresholdInSigma);
			newy[i]=average;
		}
		for (int i=0;i<newy.length;i++) setValue(i,newy[i]);
	}


	/**
 smooth with a uniform kernel of size kern with filtering
	 */
	public void _smoothFiltered(int dimkern,double thresholdInSigma)
	{
		if (dimkern>getDim())
		{
			double average=meanFiltered(thresholdInSigma);
			for (int i=0;i<getDim();i++) setValue(i,average);
			return;
		}
		//Signal1D1DXY newsig=new Signal1D1DXY(getDim());
		double[] newy=new double[getDim()];
		for (int i=0;i<getDim();i++)
		{
			int imin=Math.max(0,i-dimkern/2);
			int imax=imin+dimkern;
			if (imax>=getDim())
			{
				imax=getDim()-1;
				imin=imax-dimkern+1;
			}
			//af("imin:"+imin+" imax:"+imax+" n:"+getDim());
			double average=meanFiltered(imin,imax,thresholdInSigma);
			newy[i]=average;
		}
		for (int i=0;i<newy.length;i++) setValue(i,newy[i]);
	}




	/**
	 * simulate averaging of all the points preceeding: each value is replaced by the average of all previous value
	 */
	public void _progressiveAverage()
	{
		for (int i=1;i<getDim();i++)
		{
			double average=(y(i-1)*i+y(i))/(i+1);
			this.setValue(i, average);
		}
	}

	/**
	 * simulate averaging of all the points preceeding: each value is replaced by the average of all previous value
	 */
	public void _progressiveFilteredAverage(double thresholdInSigma)
	{
		double delta;
		double r2=0;
		double sigma;
		for (int i=1;i<getDim();i++)
		{
			double average=(y(i-1)*i+y(i))/(i+1);
			delta=y(i)-average;
			r2+=delta*delta;
			sigma=Math.sqrt(r2/(i+1));
			if (delta<=thresholdInSigma*sigma) this.setValue(i, average);else setValue(i,y(i-1));
		}
	}

	/**
	 *  calculate th eposition of a peak by averaging the left side and the roght side of the peak, 
	 *  use solve starting from left border and from right border
	 * @param yThreshold
	 * @return
	 */
	public double getPicXposition(double yThreshold)
	{
		Signal1D1DXY profile2=this.transfertTo1D1DXY();
		double r1=profile2.solve(yThreshold,profile2.xmin(),true);
		double r2=profile2.solve(yThreshold,profile2.xmax(),false);
		double r=(r1+r2)/2;
		return r;
	}


	/** rescale in order to set min y at 0 and max y at 1 */
	public void _normalise()
	{
		double ymin=this.ymin();
		double ymax=this.ymax();
		double delta=ymax-ymin;
		for (int i=0;i<getDim();i++) setValue(i,(y(i)-ymin)/delta);
	}



	/**
	 * fit a line and subtract the line to the data
	 */
	public void _subtractFittedLine(boolean logx,boolean logy)
	{
		Signal1D1D signal=this;
		Signal1D1DXY signalBis=signal.transfertTo1D1DXY();
		if (logx) signalBis._log10TheX();
		if (logy) signalBis._log10TheY();
		FitLin fitlin=new FitLin(signalBis);
		fitlin.work();
		Line line=fitlin.getLine();
		double a=line.a();
		double b=line.b();
		for (int i=0;i<signalBis.getDim();i++) signalBis.setValue(i, signalBis.y(i)-(a*signalBis.x(i)+b));
		if (logx) signalBis._p10TheX();
		if (logy) signalBis._p10TheY();
		for (int i=0;i<signal.getDim();i++) signal.setValue(i, signalBis.y(i));
	}



	public static double segdist2D(double[] segcoord0, double[] segcoord1, double[] point, double[] intersection)
	{
		double linemag = pointdist2D(segcoord0, segcoord1);
		double u = 
				(point[0] - segcoord0[0]) * (segcoord1[0] - segcoord0[0])
				+ (point[1] - segcoord0[1]) * (segcoord1[1] - segcoord0[1]);

		u/=linemag*linemag;

		// if intersection is not on segment
		if (u < 0.0 || u > 1.0)
		{
			double pdist1 = pointdist2D(segcoord0, point);
			double pdist2 = pointdist2D(segcoord1, point);

			if (pdist1 < pdist2) 
			{
				intersection[0] = segcoord0[0];
				intersection[1] = segcoord0[1];
				return pdist1;
			}
			else
			{
				intersection[0] = segcoord1[0];
				intersection[1] = segcoord1[1];
				return pdist2;
			}
		}

		// intersection is on segment. find distance to segment
		intersection[0] = segcoord0[0] + u*(segcoord1[0] - segcoord0[0]);
		intersection[1] = segcoord0[1] + u*(segcoord1[1] - segcoord0[1]);

		return pointdist2D(intersection, point);
	}

	public static double segdist2D(double[] segcoord0, double[] segcoord1, double[] point)
	{
		double[] intersection = new double[2];

		return segdist2D(segcoord0,segcoord1,point,intersection);
	}

	public static double pointdist2D(double[] point1, double[] point2)
	{
		double[] delta = {point1[0]-point2[0], point1[1]-point2[1]};
		return quadDist2D(delta);
	}

	public static double quadDist2D(double[] point)
	{
		return Math.sqrt(point[0]*point[0]+point[1]*point[1]);
	}



	//public String toXMLS() 
	//{
	//NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
	//nf.setMaximumFractionDigits(10);
	//nf.setMinimumFractionDigits(10);
	//nf.setGroupingUsed(false);
	//StringBuffer sb=new StringBuffer();
	//sb.append("<"+getClass()+"");
	//sb.append(" data=\"\n");
	//for (int i=0;i<this.getDim();i++)	sb.append(nf.format(x(i))+"\t"+nf.format(y(i))+"\n");
	//sb.append("\"\n");
	//sb.append("</"+getClass()+"> ");
	//return sb.toString();
	//}


	public XMLElement toXML()
	{
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(10);
		nf.setMinimumFractionDigits(10);
		nf.setGroupingUsed(false);
		StringBuffer sb=new StringBuffer();
		for (int i=0;i<this.getDim();i++)	
		{
			double x=x(i);
			if ( Double.isNaN(x))
			{
				System.err.println("NaN detected");
				x=0;
			}
			double y=y(i);
			if ( Double.isNaN(y)) 
			{
				System.err.println("NaN detected");
				y=0;
			}
			sb.append(nf.format(x)+" "+nf.format(y)+" ");
		}
		XMLElement el = new XMLElement();	
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("isPointsSignal", this.isPointsSignal());
		el.setContent(sb.toString());
		return el;	
	}

	/** put only the data in a String*/
	public String toStringJustData() 
	{
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(10);
		nf.setMinimumFractionDigits(10);
		nf.setGroupingUsed(false);
		StringBuffer sb=new StringBuffer();
		for (int i=0;i<this.getDim();i++)	
		{
			double x=x(i);
			if ( Double.isNaN(x))
			{
				System.err.println("NaN detected");
				x=0;
			}
			double y=y(i);
			if ( Double.isNaN(y)) 
			{
				System.err.println("NaN detected");
				y=0;
			}
			sb.append(nf.format(x)+"\t"+nf.format(y)+"\n");
		}
		return sb.toString();
	}

	public void fillWithFunction(Function1D1D fonction)
	{
		for (int i=0;i<getDim();i++)
		{
			this.setValue(i,fonction.f(x(i)));
		}
	}




	/**
replace the y values by the power by d of themselves
	 */

	public void _pow(double d) 
	{
		for (int i=0;i<getDim();i++) this.setValue(i, Math.pow(this.value(i),d));

	}


	/**
	 * get the integral (surface under the curve)
	 * @return the integral of the spot having this profile
	 */
	public double getIntegral()
	{
		Signal1D1D p=this;
		double r=0;
		for (int i=0;i<p.getDim()-1;i++)
		{
			double dv=(p.y(i+1)+p.y(i))/2*(p.x(i+1)-p.x(i));
			r+=dv;
			//System.out.println(getClass()+" i="+i+" dv="+dv);
		}
		return r;
	}



	/**integrate volume (having revolution symmetry) having profile this signal
	 * 

	 * @return the integral of the spot having this profile
	 */
	public double getIntegralProfile()
	{
		Signal1D1D p=this;
		double E=0;
		for (int i=0;i<p.getDim()-1;i++)
		{
			double dv=Math.PI*(p.y(i+1)+p.y(i))/2*(Math.pow(p.x(i+1),2)-Math.pow(p.x(i),2));
			E+=dv;
			//System.out.println(getClass()+" i="+i+" dv="+dv);
		}
		//add the central disk:
		E+=Math.PI*p.y(0)*Math.pow(p.x(0),2);
		return E;
	}


	/**integral volume (having revolution symmetry) for r from 0 to x
	 * The volume having as profile this signal
	 * @return the integral of the spot having this profile
	 */
	public Signal1D1D getIntegralProfileCumulator()
	{
		Signal1D1D c=this.transfertTo1D1DXY();
		double E=0;
		//the central disk:
		E+=Math.PI*y(0)*Math.pow(x(0),2);
		c.setValue(0, E);
		for (int i=0;i<getDim()-1;i++)
		{
			double dv=Math.PI*(y(i+1)+y(i))/2*(Math.pow(x(i+1),2)-Math.pow(x(i),2));
			E+=dv;
			c.setValue(i+1, E);
			//System.out.println(getClass()+" i="+i+" dv="+dv);
		}
		return c;
	}


	/**integral volume (having revolution symmetry) for r from 0 to x
	 * The volume having as profile this signal
	 * @return the integral of the spot having this profile
	 */
	public Signal1D1D getIntegralCumulator()
	{
		Signal1D1D c=this.transfertTo1D1DXY();
		double E=0;
		c.setValue(0, 0);
		for (int i=0;i<getDim()-1;i++)
		{
			double dv=(y(i+1)+y(i))/2*(x(i+1)-x(i));
			E+=dv;
			c.setValue(i+1, E);
			//System.out.println(getClass()+" i="+i+" dv="+dv);
		}
		return c;
	}

	/**
	 * return the closest line
	 * @param logx if true previously log the x values
	 * @param logy if true previously log the y values
	 * @return
	 */
	public Line getFittedLine(boolean logx,boolean logy)
	{
		Signal1D1DXY signal= this.transfertTo1D1DXY();
		if (logx) signal._log10TheX();
		if (logy) signal._log10TheY();
		FitLin fitlin=new FitLin(signal);
		fitlin.work();
		return fitlin.getLine();
	}




	/**
	 * invert the curve: swap X and Y
	 * @return
	 */
	public Signal1D1DXY invert()
	{
		Signal1D1DXY sig=new Signal1D1DXY(this.getDim());
		for (int i=0;i<getDim();i++)
		{
			sig.setValue(i, this.y(i), this.x(i));
		}
		return sig;
	}




	/**
	 *return the (first from start x value to right or left ) x value for this y value,
	 *linear interpolation, 
	 *the x values must be sorted,
	 *if y value not found return the start value
	 * 
	 * @param y
	 * @param start x value to start the search
	 * @param toRight direction of search true:towards x>0 false towards x<0
	 * @return
	 */
	public double solve(double y,double start,boolean toRight)
	{
		double x1,x2,y1,y2,x;
		boolean found=false;
		// double samp=sampling();
		//double xmin=xmin();
		int i;
		//af("start="+start+" toRight="+toRight+" getDim "+getDim());
		if (getDim()<2) 
		{
			Util.af("solve: dim<2 ! : " + getDim());
			return 0;
		}
		if (start<=xmin()) i=0;
		else if (start>=xmax()) i=getDim()-1;
		// else i=getSampleIndex(start);
		else i=getNearestXPoint(start);
		if (i==getDim()-1) i--;
		do
		{
			/*	x1=(xmin+i*samp);
	x2=(xmin+(i+1)*samp);*/
			//	af(i);
			x1=x(i);
			x2=x(i+1);
			y1=value(i);
			y2=value(i+1);
			//System.out.println("i="+i+"   x1="+x1+" x2="+x2+" y1="+y1+" y2="+y2+" y="+y);
			if (((y1<=y)&&(y2>y))||((y2<y)&&(y1>=y)))
			{
				//System.out.println("found");
				found=true;
				break;
			}
			if (toRight) i++;else i--;
		}
		while ((i<getDim()-1)&&(i>0));

		if (found)
		{
			if (y2!=y1) x= ( x1*Math.abs(y2-y) + x2*Math.abs(y1-y)  ) / Math.abs(y2-y1);
			else x=x1;
		}
		else x=start;
		return x;
	}















}//class end










