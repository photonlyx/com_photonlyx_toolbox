package com.photonlyx.toolbox.math.signal;

import java.io.*;
import java.text.*;
import java.util.*;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Messager;


/**
represent a one dimension signal with log sampling </p>
 */
public class Signal1D1DLogx extends SignalDiscret1D1DAbstract implements Signal1D1DSource
{
	//links
	//private Function1D1D fonction;

	//params
	private double xmin,xmax;

	//if true the y values correspond to positions at x values , nb y values= nb x values (default)
	//if false the y values correspond to cells between x values, nb y values +1 = nb x values 
	private boolean isPointsSignal=true;

	public double lxmax;//log10 of xmax
	public double lxmin;//log10 of xmin
	public double sampling;//sampling: size of a cell after log10 transform

	private double[] y;



	public Signal1D1DLogx()
	{
	}

	///**
	//construct a sampling form min to max with dim points
	//*/
	//public Signal1D1DLogx(int dim,double xmin,double xmax)
	//{
	//this.xmin=xmin;
	//this.xmax=xmax;
	//y=new double[dim];
	//}

	/**
construct a sampling form min to max with dim points
	 */
	public Signal1D1DLogx(double xmin,double xmax,int dim)
	{
		init(dim,xmin,xmax);
		//this.xmin=xmin;
		//this.xmax=xmax;
		//y=new double[dim];
	}

	/**
construct a sampling form min to max with dim points and fill with the function
	 */
	public Signal1D1DLogx(double xmin,double xmax,int dim,Function1D1D f)
	{
		init(dim,xmin,xmax);
		//this.xmin=xmin;
		//this.xmax=xmax;
		//y=new double[dim];
		if (f!=null)
		{
			for (int i=0;i<getDim();i++) y[i]=f.f(x(i));
		}
	}




	public void init(int dim,double xmin,double xmax)
	{
		y=new double[dim];
		this.xmin=xmin;
		this.xmax=xmax;
		//update this also
		lxmax=Math.log(xmax());
		lxmin=Math.log(xmin());
		if (isPointsSignal) 
			{
			if (dim==1) sampling=1;
			else sampling=(lxmax-lxmin)/(dim-1);//signal known by some points
			}
		else sampling=(lxmax-lxmin)/(dim);//signal known by values inside cells

	}



	public boolean isPointsSignal()
	{
		return isPointsSignal;
	}

	public void setPointsSignal(boolean isPointsSignal)
	{
		this.isPointsSignal = isPointsSignal;
		//update this also
		lxmax=Math.log(xmax());
		lxmin=Math.log(xmin());
		if (isPointsSignal) sampling=(lxmax-lxmin)/(getDim()-1);//signal known by some points
		else sampling=(lxmax-lxmin)/(getDim());//signal known by values inside cells
	}

	public int getDim()
	{
		if (y!=null) return y.length; else return 0;
	}

	/**
	 * return the raw x value of index i
	 * if the signal is points like give the x of the point</p>
	 * if the signal is cell like, give the x of the left point of cell 
	 * */
	public double x(int i)
	{
		if (i>getDim()-1) return 0;
		if (i<0) return 0;
		double lx;
		lx=lxmin+sampling*i;
		return (Math.exp(lx));
	}

	/**
	 * return the x value for the sample of index i</p>
	 * if the signal is points like give the x of the point</p>
	 * if the signal is cell like, give the middle point of cell in log scale
	 * */
	public double xSample(int i)
	{
		if (i>getDim()-1) return 0;
		if (i<0) return 0;
		double lx;
		if (isPointsSignal) lx=lxmin+sampling*i;
		else lx=lxmin+sampling*(i+0.5);
		return (Math.exp(lx));
	}



	/**
return the value at the cell i or point i. If it is out of range,return 0
	 */
	public double value(int i)
	{
		if ((i<0)||(i>=getDim())) return 0.0;
		return y[i];
	}
	/**
return the value at the cell i or point i. If i is out of range,return 0
	 */
	public double y(int i)
	{
		if ((i<0)||(i>=getDim())) return 0.0;
		return y[i];
	}

	/**
	 * return the value at the x position </p>
	 * if the signal is points like give the value of the closest point</p>
	 * if the signal is cell like, give the value of the cell containing the x value</p>
	 * 0 is returned if out of boundaries</p>
	 */
	public double value(double x)
	{
		//double v=0;
		if ((x<xmin())||(x>=xmax())) return 0;
		if (isPointsSignal) 
		{
			int i=getSampleIndex(x);
			return value(i);
		}
		else
		{
			int cell=getSampleIndexFloor(x);
			return value(cell);
		}

	}

	/** set a value*/
	public void setValue(int i,double v)
	{
		if (y!=null)
		{
			y[i]=v;
		}
	}



	/**return the x min value*/
	public double xmin()
	{
		return xmin;
	}

	/**return the x max value*/
	public double xmax()
	{
		return xmax;
	}



	/**get the sample index close to the x position*/
	public int getSampleIndex(double x)
	{
		double lx=Math.log(x);
		int i=(int) Math.round((lx-lxmin)/sampling);
		return i;
	}


	/**get the sample index close to the x position, </p>
	 * return 0 if x==xmin, (dim-1) if x=max,  -1 if out of range*/
	//public int getSampleIndexRound(double x)
	//{
	//double lx=Math.log(x);
	//int i=(int) Math.round((lx-lxmin)/sampling);
	//if (i<0) i=-1;
	//if (i>=getDim()) i=-1;
	//return i;
	//}

	/**
	 * get the xi closest to the x position</p>
	 * return 0 if x==xmin, (dim-1) if x=max,  -1 if out of range
	 * */
	public int getSampleIndexFloor(double x)
	{
		if (x<xmin) return -1;
		if (x>=xmax) return -1;
		double lx=Math.log(x);
		int i=(int) Math.floor((lx-lxmin)/sampling);
		//System.out.println("x="+x+" "+i+ " x1="+x(cell)+" x2="+x(i));
		return i;
	}

	/**return a String with x and y in 2 columns*/
	public String toString()
	{
		NumberFormat nf1 = NumberFormat.getInstance(Locale.ENGLISH);
		nf1.setMaximumFractionDigits(10);
		nf1.setMinimumFractionDigits(10); 
		nf1.setGroupingUsed(false);

		StringBuffer s=new StringBuffer();
		for (int i=0;i<getDim();i++)
		{
			s.append(nf1.format(x(i))+"\t"+nf1.format(value(i))+"\n");
		}
		return s.toString();
	}



	//*************************************************************************
	//*****************  analyse and processing   *****************************
	//*************************************************************************



	/**return the max y value()*/
	public double ymax()
	{
		double r=-1e300;
		for (int i=0;i<getDim();i++) 
		{
			double v=value(i);
			if (v>r) r=v;
		}
		return r;
	}


	/**return the min y value()*/
	public double ymin()
	{
		double r=1e300;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i);
			if (v<r) r=v;
		}
		return r;
	}

	/**return the min y positive alue()*/
	public double yminStrictPositive()
	{
		double r=1e300;
		for (int i=0;i<getDim();i++) 
		{
			double v=value(i);
			if ((v<r)&&(v>0)) r=v;
		}
		return r;
	}
	/**
	 * integral 
	 */
	public double integral()
	{
		if (isPointsSignal) 
		{
			double x1,x2,s=0;
			for (int i=0;i<getDim()-1;i++)
			{
				x1=Math.exp(lxmin+sampling*i);
				x2=Math.exp(lxmin+sampling*(i+1));
				s+=(y[i]+y[i+1])/2.0*(x2-x1);
			}
			return s;
		}
		else
		{
			double x1,x2,s=0;
			for (int i=0;i<getDim()-1;i++)
			{
				x1=Math.exp(lxmin+sampling*i);
				x2=Math.exp(lxmin+sampling*(i+1));
				s+=y[i]*(x2-x1);
			}
			//last cell goes to xmax:
			s+=y[getDim()-1]*(xmax-Math.exp(lxmin+sampling*(getDim()-1)));
			return s;
		}
	}

	/**
read a text file with 2 columns containing x and y real data
fill the x and y array
	 */
	public void fillWithFile(String filename)
	{
		if (filename==null) return;
		String s=TextFiles.readFile(filename).toString();
		fillWithString(s);
	}

	/**read a text file with 2 columns containing x and y real data
fill  y array
	 */
	public void fillWithFile(String chemin,String filename,int dim,int start)
	{
		FileReader r;
		StreamTokenizer st;

		double[] x=new double[dim];//temporary
		y=new double[dim];//the array of the signal

		if (chemin==null) chemin="";
		System.out.println("Lecture dans fichier "+chemin+filename+" de "+x.length+" �hantillons...");
		try
		{
			r= new FileReader(chemin+filename);
		}
		catch (Exception e)
		{
			Messager.messErr(Messager.getString("Probleme_ouverture_fichier")+" "+chemin+filename+"\n"+e);
			return ;
		}

		st= new StreamTokenizer(r);

		st.wordChars(0x26,0x26);
		st.wordChars(0x5F,0x5F);
		st.wordChars(0x7B,0x7B);
		st.wordChars(0x7D,0x7D);
		st.slashSlashComments(true);
		st.slashStarComments(true);
		try
		{
			for (int i=0;i<start;i++)
			{
				if (st.ttype==StreamTokenizer.TT_EOF)
				{
					Messager.messErr("Le fichier "+filename+" est trop court");
					x=null;
					return;
				}
				while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
				st.nextToken();
				while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
				st.nextToken();
			}
			for (int i=0;i<x.length;i++)
			{
				if (st.ttype==StreamTokenizer.TT_EOF)
				{
					Messager.messErr("Le fichier "+filename+" est trop court");
					x=null;
					return ;
				}
				while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
				x[i]=st.nval;
				st.nextToken();
				while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
				y[i]=st.nval;
				st.nextToken();
			}
		}
		catch (Exception e)
		{
			Messager.messErr(Messager.getString("Probleme_lecture_definition") +"\n"+e);
			return ;
		}
		xmin=x[0];
		xmax=x[x.length-1];
		System.out.println("OK");
	}


	/**
return the number of couple (x,y) in a string (c++ comments removed)
	 */
	private int NbPointInString(String s)
	{
		StringReader r;
		StreamTokenizer st;
		r= new StringReader(s);
		st= new StreamTokenizer(r);
		st.wordChars(0x26,0x26);
		st.wordChars(0x5F,0x5F);
		st.wordChars(0x7B,0x7B);
		st.wordChars(0x7D,0x7D);
		st.slashSlashComments(true);
		st.slashStarComments(true);
		int compteur=0;
		try
		{
			while (st.ttype!=StreamTokenizer.TT_EOF)
			{
				st.nextToken();
				System.out.println(compteur+" "+st.nval);
				if (st.ttype==StreamTokenizer.TT_NUMBER) compteur++;
			}
		}
		catch(Exception e) {;}
		return compteur/2;
	}


	/**read a string with 2 columns containing x and y real data
fill the x and y arrays*/
	public void fillWithString(String string)
	{
		StringReader r;
		StreamTokenizer st;

		r= new StringReader(string);
		st= new StreamTokenizer(r);
		st.wordChars(0x26,0x26);
		st.wordChars(0x5F,0x5F);
		st.wordChars(0x7B,0x7B);
		st.wordChars(0x7D,0x7D);
		st.slashSlashComments(true);
		st.slashStarComments(true);

		int dim=NbPointInString(string);
		double[] x=new double[dim];//temporary
		y=new double[dim];//the array of the signal

		try
		{
			for (int i=0;i<dim;i++)
			{
				if (st.ttype==StreamTokenizer.TT_EOF)
				{
					Messager.messErr("La chaine  est trop courte");
					xmin=x[0];
					xmax=x[i-1];
					//                        _trunc(0,i-1);
					return ;
				}
				while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
				x[i]=st.nval;
				st.nextToken();
				while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
				y[i]=st.nval;
				st.nextToken();
			}
		}
		catch (Exception e)
		{
			Messager.messErr(Messager.getString("Probleme_lecture_chaine") +"\n"+e);
			return ;
		}

		xmin=x[0];
		xmax=x[x.length-1];
	}


	/**equalise to the signal; BEWARE : there is no copy of matrix data : just point to the same*/
	public void transfert(Signal1D1DLogx signal)
	{
		y=signal.y;
		xmin=signal.xmin();
		xmax=signal.xmax();
	}


	/** return an array containing the x values*/
	public double[] getXarray()
	{
		double[] x=new double[getDim()];
		double lxmax=Math.log(xmax());
		double lxmin=Math.log(xmin());
		double sampling=(lxmax-lxmin)/(getDim()-1);
		for (int i=0;i<getDim();i++) 
		{
			double lx=lxmin+sampling*i;
			x[i]=Math.exp(lx);
		}
		return x;
	}

	/** return the y values array */
	public double[] getYarray()
	{
		return y;
	}

	/**set the  point of index i to these new co-ordinates, if possible*/
	public void setPoint(int i,double[] p)
	{
		y[i]=p[1];
	}

	/**
tranlate the signal in the x direction
@param xoffset : the offset to add to each x  value
	 */
	public void _translate(double xoffset)
	{
		xmin+=xoffset;
		xmax+=xoffset;
	}



	/**
update the state of the object from the data in the string s
@param s the string where is stored the object's state
	 */
	public void updateStateFromString(String s)
	{
		//drop the first line
		s=s.substring(s.indexOf('\n'));
		double[][] data2col;
		data2col=StringSource.fillWithTwoFirstColumns(s);
		if (data2col==null) return;
		xmin=data2col[0][0];
		xmax=data2col[0][data2col[0].length-1];
		y=data2col[1];
	}


	public void updateFromXML(XMLElement xml) 
	{
		String data=xml.getContent();	
		Definition def=new Definition(data); 
		int _dim=def.dim()/2;
		double _xmin=new Double(def.word(0)).doubleValue();
		double _xmax=new Double(def.word(2*(_dim-1))).doubleValue();
		init(_dim,_xmin,_xmax);
		for (int i=0;i<getDim();i++) 
			this.setValue(i,  new Double(def.word(2*i+1)).doubleValue());
		//read property
		isPointsSignal=xml.getBooleanAttribute("isPointsSignal","true","false",false);	
	}



	public void _annulate()
	{
		init(y.length,xmin(),xmax());
	}

	public  void removeAllPoints() { init(0,0,0);}


	/**add a value to all values*/
	public void _add(double s)
	{
		for (int i=0;i<getDim();i++) setValue(i,value(i)+s);
	}

	/**multiply the x values by a factor*/
	public  void _multiply_x(double coef)
	{
		xmin*=coef;
		xmax*=coef;
	}

	@Override
	public void _inverse() 
	{
		for (int i=0;i<getDim();i++) setValue(i,1/value(i));
	}

	/**
replace the y values by the Log (base 10) of themselves
	 */
	public void  _logTheY()
	{
		for (int i=0;i<getDim();i++) setValue(i,Math.log10(y[i]));
	}



	public void copy(Signal1D1D signal)
	{
		if (signal instanceof Signal1D1DLogx) this.copy((Signal1D1DLogx)signal);
		else
		{
			Messager.messErr(getClass()+" Atempt to copy a signal that is not a Signal1D1DLogx");
		}
	}


}




