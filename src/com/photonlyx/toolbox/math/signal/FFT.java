package com.photonlyx.toolbox.math.signal;

import com.photonlyx.toolbox.math.Complex;


/**
 * cf http://paulbourke.net/miscellaneous/dft/
 * 
 * @author laurent
 *
 */
public class FFT
{



/*-------------------------------------------------------------------------
Perform a 2D FFT inplace given a complex 2D array
The direction dir, 1 for forward, -1 for reverse
The size of the array must be powers of 2
*/
public static boolean FFT2D(Complex[][] c,int dir)
{
int i,j;
//int m,twopm;
double []real,imag;
int ny=c[0].length;
int nx=c.length;

/* Transform the rows */
//real = (double *)malloc(nx * sizeof(double));
//imag = (double *)malloc(nx * sizeof(double));
real=new double[nx];
imag=new double[nx];
//if (real == NULL || imag == NULL)   return(FALSE);
int[] m=new int[1];
int[] twopm=new int[1];
if (!powerof2(nx,m,twopm) || twopm[0] != nx)   return(false);


for (j=0;j<ny;j++) {
   for (i=0;i<nx;i++) {
      real[i] = c[i][j].re;
      imag[i] = c[i][j].im;
   }
   FFT.FFT1D(dir,m[0],real,imag);
   for (i=0;i<nx;i++) {
      c[i][j].re = real[i];
      c[i][j].im = imag[i];
   }
}
//free(real);
//free(imag);

/* Transform the columns */
//real = (double *)malloc(ny * sizeof(double));
//imag = (double *)malloc(ny * sizeof(double));
real=new double[ny];
imag=new double[ny];
//if (real == NULL || imag == NULL)   return(FALSE);
if (!powerof2(ny,m,twopm) || twopm[0] != ny)  return(false);
for (i=0;i<nx;i++) {
   for (j=0;j<ny;j++) {
      real[j] = c[i][j].re;
      imag[j] = c[i][j].im;
   }
   FFT.FFT1D(dir,m[0],real,imag);
   for (j=0;j<ny;j++) {
      c[i][j].re = real[j];
      c[i][j].im = imag[j];
   }
}
//free(real);
//free(imag);

return(true);
}

/*-------------------------------------------------------------------------
This computes an in-place complex-to-complex FFT
x and y are the real and imaginary arrays of 2^m points.
dir =  1 gives forward transform
dir = -1 gives reverse transform

  Formula: forward
               N-1
               ---
           1   \          - j k 2 pi n / N
   X(n) = ---   >   x(k) e                    = forward transform
           N   /                                n=0..N-1
               ---
               k=0

   Formula: reverse
               N-1
               ---
               \          j k 2 pi n / N
   X(n) =       >   x(k) e                    = forward transform
               /                                n=0..N-1
               ---
               k=0
*/
static boolean FFT1D(int dir,int m,double [] x,double [] y)
{
int nn,i,i1,j,k,i2,l,l1,l2;
double c1,c2,tx,ty,t1,t2,u1,u2,z;

/* Calculate the number of points */
nn = 1;
for (i=0;i<m;i++)
   nn *= 2;

/* Do the bit reversal */
i2 = nn >> 1;
j = 0;
for (i=0;i<nn-1;i++) {
   if (i < j) {
      tx = x[i];
      ty = y[i];
      x[i] = x[j];
      y[i] = y[j];
      x[j] = tx;
      y[j] = ty;
   }
   k = i2;
   while (k <= j) {
      j -= k;
      k >>= 1;
   }
   j += k;
}

/* Compute the FFT */
c1 = -1.0;
c2 = 0.0;
l2 = 1;
for (l=0;l<m;l++) {
   l1 = l2;
   l2 <<= 1;
   u1 = 1.0;
   u2 = 0.0;
   for (j=0;j<l1;j++) {
      for (i=j;i<nn;i+=l2) {
         i1 = i + l1;
         t1 = u1 * x[i1] - u2 * y[i1];
         t2 = u1 * y[i1] + u2 * x[i1];
         x[i1] = x[i] - t1;
         y[i1] = y[i] - t2;
         x[i] += t1;
         y[i] += t2;
      }
      z =  u1 * c1 - u2 * c2;
      u2 = u1 * c2 + u2 * c1;
      u1 = z;
   }
   c2 = Math.sqrt((1.0 - c1) / 2.0);
   if (dir == 1)
      c2 = -c2;
   c1 = Math.sqrt((1.0 + c1) / 2.0);
}

/* Scaling for forward transform */
if (dir == 1) {
   for (i=0;i<nn;i++) {
      x[i] /= (double)nn;
      y[i] /= (double)nn;
   }
}

return(true);
}

/*-------------------------------------------------------------------------
Calculate the closest but lower power of two of a number
twopm = 2**m <= n
Return TRUE if 2**m == n
*/
static boolean powerof2(int n,int []m,int []twopm)
{
if (n <= 1) {
   m[0] = 0;
   twopm[0] = 1;
   return(false);
}

m[0] = 1;
twopm[0] = 2;
do {
   (m[0])++;
   (twopm[0]) *= 2;
} while (2*(twopm[0]) <= n);

if (twopm[0] != n)
   return(false);
else
   return(true);
}



}