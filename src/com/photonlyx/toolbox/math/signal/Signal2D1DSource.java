// Author Laurent Brunel

package com.photonlyx.toolbox.math.signal;


public interface Signal2D1DSource
{


public Signal2D1D getSignal2D1D();

}
