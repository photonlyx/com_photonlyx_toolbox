package com.photonlyx.toolbox.math.signal;

public class Signal1D2DScattered
{
private double[] t,x,y;
protected int dim=0;//the number of points




public void addPoint(double nt,double nx,double ny)
{
if (getDim()==t.length) enlargeBuffer(2*getDim()+1);
//System.out.println("buffer size"+x.length);
t[getDim()]=nt;
x[getDim()]=nx;
y[getDim()]=ny;
dim++;
}




private void enlargeBuffer(int newSize)
{
//System.out.println(getClass()+" "+"enlarge buffer to "+newSize);
double[] newx=new double[newSize];
double[] newy=new double[newSize];
for (int i=0;i<getDim();i++)
	{
	newx[i]=x[i];
	newy[i]=y[i];
	}
x=newx;
y=newy;
}

public int getDim()
{
//if (y!=null) return y.length; else return 0;
return dim;
}
}
