// Author Laurent Brunel


package com.photonlyx.toolbox.math.image;

import java.io.File;
import javax.swing.filechooser.*;

import com.photonlyx.toolbox.io.FileFilterWithExtension;
import com.photonlyx.toolbox.io.Utils;
import com.photonlyx.toolbox.util.Messager;


/**
file filter for raw float image
*/
public class RawFloatImageFileFilter extends FileFilter implements FileFilterWithExtension
{


// Accept all directories and all gif, jpg, or tiff files.
public boolean accept(File f)
{
if (f.isDirectory())
	{
	return true;
	}

String sextension = Utils.getExtension(f);
if 	(
	(("raw").equals(sextension))
||	(("RAW").equals(sextension))
	) return true;
else
	{
	return false;
    	}

}

/**
The description of this filter
@return The description of this filter
*/
public String getDescription()
{
return Messager.getString("raw float image file");
}

/**
The extension of this filter
@return The extension of this filter
*/
public static String getExtension()
{
return "raw";
}

public String getFileExtension() 
	{
	return getExtension();
	}




}
