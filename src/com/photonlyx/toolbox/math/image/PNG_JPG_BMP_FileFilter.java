// Author Laurent Brunel


package com.photonlyx.toolbox.math.image;

import java.io.File;
import javax.swing.filechooser.*;

import com.photonlyx.toolbox.io.FileFilterWithExtension;
import com.photonlyx.toolbox.io.Utils;
import com.photonlyx.toolbox.util.Messager;


/**
file filter for png image
*/
public class PNG_JPG_BMP_FileFilter extends FileFilter implements FileFilterWithExtension
{
private boolean acceptfiles=true;


public PNG_JPG_BMP_FileFilter()
{
super();
this.acceptfiles = true;
}

public PNG_JPG_BMP_FileFilter(boolean acceptfiles)
{
super();
this.acceptfiles = acceptfiles;
}

// Accept all directories and all gif, jpg, or tiff files.
public boolean accept(File f)
{
if (f.isDirectory())
	{
	return acceptfiles;
	}

String sextension = Utils.getExtension(f);
if 	(
	(("png").equals(sextension))
||	(("PNG").equals(sextension))
||	(("JPG").equals(sextension))
||	(("jpg").equals(sextension))
||	(("BMP").equals(sextension))
||	(("bmp").equals(sextension))
	) return true;
else
	{
	return false;
    	}

}

/**
The description of this filter
@return The description of this filter
*/
public String getDescription()
{
return Messager.getString("image file");
}

/**
The extension of this filter
@return The extension of this filter
*/
public static String getExtension()
{
return "png";
}

public String getFileExtension() 
	{
	return getExtension();
	}




}
