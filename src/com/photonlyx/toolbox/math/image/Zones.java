package com.photonlyx.toolbox.math.image;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class Zones extends Vector<Zone>
{


static final Comparator<Zone> SIZE_ORDER = new Comparator<Zone>() 
{
public int compare(Zone e1, Zone e2) 
	{
	double diff=(e1.size())-(e2.size());
	int i=0;
    if (diff<0) i= 1;
    if (diff==0) i= 0;
    if (diff>0) i= -1;
    return i;
    }
};


public Zones()
{
// TODO Auto-generated constructor stub
}

public void sortBySizedescending()
{
Collections.sort(this, SIZE_ORDER);
}


}