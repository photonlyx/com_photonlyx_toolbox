package com.photonlyx.toolbox.math.image;

import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;



public interface DrawingLayer extends MouseListener,MouseMotionListener,KeyListener
{

public void draw(Graphics g,JPanel jp,ImageSource is);



}
