package com.photonlyx.toolbox.math.image;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.Vector;

import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Util;



public class CImageProcessing extends CImage
{
//int[] pix=null;
BufferedImage bim;

//private static int c;

//used in segmentation algorithm:
private static int blackCode=Color.BLACK.getRGB();
private static int blueCode=Color.BLUE.getRGB();
private static int whiteCode=Color.WHITE.getRGB();

private static int ZONE_MAX_SIZE=4000;

public CImageProcessing()
{
bim=new BufferedImage(100,100,BufferedImage.TYPE_INT_ARGB);
//int w=100;
//int h=100;
//bim.setRGB(0, 0,w,h, new int[w*h], 0, w);
//this.setImage(bim);
init(100,100);
}

public CImageProcessing(BufferedImage bim)
{
this.bim=bim;
}

public CImageProcessing(int w,int h)
{
//bim=new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
//bim.setRGB(0, 0,w,h, new int[w*h], 0, w);
//this.setImage(bim);
init(w,h);
}

public void init(int w,int h)
{
bim=new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
bim.setRGB(0, 0,w,h, new int[w*h], 0, w);
this.setImage(bim);
}


/**
 * create a new CImageProcessing copying pixels from cim
 * @param cim
 */
public CImageProcessing(CImage cim)
{
bim=new BufferedImage(cim.w(),cim.h(),BufferedImage.TYPE_INT_RGB);
bim.setRGB(0, 0,cim.w(),cim.h(), cim.getRGBImage(), 0, cim.w());
this.setImage(bim);
}

public CImageProcessing(String path,String filename,boolean lookInResources)
{
CImage cim=new CImage(path,filename,lookInResources);
bim=new BufferedImage(cim.w(),cim.h(),BufferedImage.TYPE_INT_RGB);
bim.setRGB(0, 0,cim.w(),cim.h(), cim.getRGBImage(), 0, cim.w());
this.setImage(bim);
}



public int getWidth(){if (bim==null) return 0; return bim.getWidth();}
public int getHeight(){if (bim==null) return 0; return bim.getHeight();}
public int w(){if (bim==null) return 0; else return bim.getWidth();}
public int h(){if (bim==null) return 0; return bim.getHeight();}

public BufferedImage getBufferedImage() {return bim;}
public Image getImage(){return bim;}

/**
fill the image with a list of int with RGB codes
@param pix the array of RGB codes
@param w width
@param h height
*/
public void setPixelsDirect(int[] pix,int w,int h)
{
if (w*h==0) return;
if (pix.length!=w*h) return;
bim=new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
bim.setRGB(0, 0,w,h, pix, 0, w);
setImage(bim);
}


/**
fill the image with a list of int with RGB codes
@param pix the array of RGB codes
*/
public void setPixelsDirect(int[] pix)
{
int w=w();
int h=h();	
if (pix.length!=w*h) return;
bim=new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
bim.setRGB(0, 0,w,h, pix, 0, w);
setImage(bim);
}

public void fill(Color c)
{
int[] rgb=new int[w()*h()];
this.setPixelsDirect(rgb, w(), h());
}

public CImageProcessing getAcopy()
{
CImageProcessing cimp=new CImageProcessing(this.w(),this.h());
int[] pix=bim.getRGB(0, 0, w(),h(), null, 0, w());
cimp.setPixelsDirect(pix, w(), h());
return cimp;
}

public boolean loadFile(String path,String filename,boolean lookInResources)
{
return this.loadFile(path,filename,lookInResources,false);
}

public boolean loadFile(String path,String filename,boolean lookInResources,boolean verbose)
{
CImage cim=new CImage();
if(!cim.loadFile(path, filename, lookInResources,verbose)) return false;
bim=new BufferedImage(cim.w(),cim.h(),BufferedImage.TYPE_INT_RGB);
bim.setRGB(0, 0,cim.w(),cim.h(), cim.getRGBImage(), 0, cim.w());
this.setImage(bim);
return true;
}

public boolean loadResource(String filename)
{
Image image=Util.getImage(filename, true);
if (image==null) return false;
CImage cim=new CImage(image);
BufferedImage bim=new BufferedImage(cim.w(),cim.h(),BufferedImage.TYPE_INT_RGB);
bim.setRGB(0, 0,cim.w(),cim.h(), cim.getRGBImage(), 0, cim.w());
this.setImage(bim);
return true;
}


public void setImage(BufferedImage bim)
{
this.bim=bim;
this.setImage((Image)bim);
}

/**
 * set the data of a new BufferedImage
 * @param bim
 */
public void copy(BufferedImage bim)
{
setImage(bim);
}

/**
 * copy the Signal2D1D values to the r g and b channels.
 * set negatives values as 0 and >255 value as 255
 * @param im
 */
public void copy(Signal2D1D im)
{
bim=new BufferedImage(im.dimx(),im.dimy(),BufferedImage.TYPE_INT_RGB);
this.setImage(bim);
int[] pix=new int[im.dimx()*im.dimy()];
int c;
int w=getWidth();
int h=getHeight();
for (int i=0;i<w;i++) 
	for (int j=0;j<h;j++) 
	{
	c=(int)im.getValue(i, j);
	if (c<0) c=0;
	if (c>255) c=255;
	//bim.setRGB(i, j,new Color(c,c,c).getRGB() );
	pix[i+j*w]=new Color(c,c,c).getRGB() ;
	}
bim.setRGB(0, 0, w, h, pix, 0, w);
}

/**
 * reset pixels buffer
 */
//public void resetBuffer()
//{
//pix=null;
//}

public void _desaturate()
{
float[] hsb=new float[3];
for (int i=0;i<bim.getWidth();i++) 
	for (int j=0;j<bim.getHeight();j++) 
	{
	Color c=new Color(bim.getRGB(i, j));
	int r=c.getRed();
	int g=c.getGreen();
	int b=c.getBlue();
	Color.RGBtoHSB(r,g,b,hsb);
	bim.setRGB(i, j, Color.HSBtoRGB(0,0,hsb[2]));
	}
}

public void _keepOnlyRedChannel()
{
float[] hsb=new float[3];
for (int i=0;i<bim.getWidth();i++) 
	for (int j=0;j<bim.getHeight();j++) 
	{
	Color c=new Color(bim.getRGB(i, j));
	int r=c.getRed();
	Color.RGBtoHSB(r,0,0,hsb);
	bim.setRGB(i, j, Color.HSBtoRGB(0,0,hsb[2]));
	}
}

public void _enhanceRed()
{
for (int i=0;i<bim.getWidth();i++) 
	for (int j=0;j<bim.getHeight();j++) 
	{
	Color c=new Color(bim.getRGB(i, j));
	int r2=c.getRed()+40;
	int g2=c.getGreen()-40;
	int b2=c.getBlue()-40;
	if (r2>255) r2=255;
	if (g2<0) g2=255;
	if (b2<0) b2=255;
	bim.setRGB(i, j, new Color(r2,g2,b2).getRGB());
	}
}
/**
 * set pixels to 0 if value < threshold else to 255
 * @param valueThreshold
 */
//public void _binarise(int valueThreshold)
//{
//int[] pix=this.getRawPixelsFromImage();
//int [] pix2=new int[pix.length];
//float[] hsb=new float[3];
//float th=valueThreshold/255f;
//for (int i=0;i<pix.length;i++) 
//	{
//	Color c=new Color(pix[i]);
//	int r=c.getRed();
//	int g=c.getGreen();
//	int b=c.getBlue();
//	Color.RGBtoHSB(r,g,b,hsb);
//	if (hsb[2]<th) pix2[i]=Color.black.getRGB();else pix2[i]=Color.white.getRGB();
//	}
//this.setPixelsDirect(pix2, w(), h());
//}
public void _binarise(int valueThreshold)
{
float[] hsb=new float[3];
float th=valueThreshold/255f;
int rgb;
for (int i=0;i<getWidth();i++) 
	for (int j=0;j<getHeight();j++) 
	{
	//System.out.println("i="+i+" j="+j);
	Color c=new Color(bim.getRGB(i, j));
	int r=c.getRed();
	int g=c.getGreen();
	int b=c.getBlue();
	Color.RGBtoHSB(r,g,b,hsb);
	if (hsb[2]<th) rgb=Color.black.getRGB();else rgb=Color.white.getRGB();
	bim.setRGB(i, j, rgb);
	}
}

/**
 * 
 * @param diag neigbourg pixels in diagonal are connnected
 * @param sortBySize
 * @return
 */
public Zones segmentation(boolean diag,boolean sortBySize)
{
int[] pix=bim.getRGB(0, 0, bim.getWidth(),bim.getHeight(), null, 0, bim.getWidth());
Zones zones=new Zones();
for (int i=0;i<pix.length;i++) 
	{
	double val2=new Color(pix[i]).getRed();
	if (val2==255)
		{
		Zone zone=new Zone(this.getWidth(),this.getHeight());
		zones.add(zone);
		followZone(diag,i,zone,zones,pix);
		}
	}
//repaint all zones in white
for (int k=0;k<pix.length;k++) if (pix[k]==blueCode) pix[k]=whiteCode;

bim.setRGB(0, 0, bim.getWidth(),bim.getHeight(), pix, 0, bim.getWidth());
//System.out.println(zones.size()+" zones");
if (sortBySize) zones.sortBySizedescending();
return zones;	
}


public Zones segmentation(boolean sortBySize)
{
return segmentation(true,sortBySize);
}



public Zones segmentation2(boolean diag,boolean sortBySize)
{
int[] pix=bim.getRGB(0, 0, bim.getWidth(),bim.getHeight(), null, 0, bim.getWidth());
Zones zones=new Zones();
for (int i=0;i<pix.length;i++) 
	{
	if (pix[i]==whiteCode)
		{
		Zone zone=new Zone(this.getWidth(),this.getHeight());
		zones.add(zone);
		//System.out.println("follow zone "+(zones.size()-1));
		followZone2(diag,i,zone,zones,pix);
		//System.out.println("Zone "+(zones.size()-1)+" "+zone.size()+" pixels");
		}
	}
//repaint all zones in white
for (int k=0;k<pix.length;k++) if (pix[k]==blueCode) pix[k]=whiteCode;

bim.setRGB(0, 0, bim.getWidth(),bim.getHeight(), pix, 0, bim.getWidth());
//System.out.println(zones.size()+" zones");
if (sortBySize) zones.sortBySizedescending();


return zones;	
}



public Zones segmentation2(boolean sortBySize)
{
return segmentation2(true,sortBySize);
}




/**
 * recursive method to follow the connected white pixels
 * @param v1
 */
public void followZone2(boolean diag,int k,Zone zone,Zones zones,int[] pix)
{
Vector<Integer> v;
Vector<Integer> pixels2visit=new Vector<Integer>();
pixels2visit.add(k);
while (pixels2visit.size()>0)
	{
	int i=pixels2visit.elementAt(0);
	
//	System.out.print("Pixel "+i);
//	System.out.print(": list of pixels to visit: ");
//	for (int pp:pixels2visit) System.out.print(pp+" ");
//	System.out.println();
//	
	//mark as seen:
	pix[i]=blueCode;
	//add this pixel to the list:
	//if (zone.has(i)) System.out.println("pixels "+i+" already in zone");
	zone.add(i);
	pixels2visit.remove(0);
	if (diag) v=getNeighboursWithDiags(i,pix); else v=getNeighbours(i,pix); 
	
//	System.out.print(v.size()+" neighbours: ");
//	for (int pp:v) System.out.print(pp+" ");
//	System.out.println();
	
	for (int pp:v) if (!pixels2visit.contains(pp)) pixels2visit.add(pp);

//	System.out.print("Zone: ");
//	for (int pp:zone) System.out.print(pp+" ");
//	System.out.println();

	
	}


}


public void followZone(boolean diag,int k,Zone zone,Zones zones,int[] pix)
{
//for (int k:v1) 
//	{
	//mark as seen:
	pix[k]=blueCode;
	//add this pixel to the list:
	zone.add(k);
	//System.out.println("nb zones="+zones.size()+" zone size:"+zone.size());
	//follow the white neighbours:
	Vector<Integer> v;
	if (diag) v=getNeighboursWithDiags(k,pix); else v=getNeighbours(k,pix); 
	//debug:
	//int y=k/w();int x=k-w()*y;System.out.print("("+x+","+y+")");
	//affNeighbours(v);
	//if(v.size()>1) System.out.println(v.size()+" neighbours");
	//follow from all the neighbour, create a new zone from the 2nd neighbour
	for (int i=0;i<v.size();i++)
		{
		Zone newzone;
//		if (i>=1)
//			{
//			//create and add a new zone:
//			newzone=new Vector<Integer>();
//			zones.add(newzone);
//			}
//		else 
			newzone=zone;
		//ystem.out.println("zone size="+zone.size());
		if (zone.size()<ZONE_MAX_SIZE) followZone(diag,v.elementAt(0),newzone,zones,pix);
		}
//	}
}



public Zones segmentationStrokes(boolean sortBySize)
{
int[] pix=bim.getRGB(0, 0, bim.getWidth(),bim.getHeight(), null, 0, bim.getWidth());
Zones zones=new Zones();
for (int i=0;i<pix.length;i++) 
	{
	double val2=new Color(pix[i]).getRed();
	if (val2==255)
		{
//		Vector<Integer> neighbours=new Vector<Integer>();
//		neighbours.add(i);
		Zone zone=new Zone(this.getWidth(),this.getHeight());
		zones.add(zone);
		followZoneStrokes(i,zone,zones,pix);
		//break;
		}
	}
bim.setRGB(0, 0, bim.getWidth(),bim.getHeight(), pix, 0, bim.getWidth());
System.out.println(zones.size()+" zones");
if (sortBySize) zones.sortBySizedescending();
return zones;	
}



/**
 * recursive method to follow the connected white pixels
 * @param v1
 */
public void followZoneStrokes(int k,Zone zone,Zones zones,int[] pix)
{
	//mark as seen:
	pix[k]=blueCode;
	//add this pixel to the list:
	zone.add(k);
	//System.out.println("nb zones="+zones.size()+" zone size:"+zone.size());
	//follow the white neighbours:
	Vector<Integer> v=getNeighboursWithDiags(k,pix);
	//debug:
	//int y=k/w();int x=k-w()*y;System.out.print("("+x+","+y+")");
	//affNeighbours(v);
	//if(v.size()>1) System.out.println(v.size()+" neighbours");
	//follow from all the neighbour, create a new zone from the 2nd neighbour
	if (v.size()>=1)followZoneStrokes(v.elementAt(0),zone,zones,pix);
	for (int i=1;i<v.size();i++)
		{
		//create and add a new zone:
		Zone newzone=new Zone(this.getWidth(),this.getHeight());
		zones.add(newzone);
		followZoneStrokes(v.elementAt(i),newzone,zones,pix);
		}

}





/**
 * get the pixels values arranged in one 1D array (like java do)
 * @return
 */
public int[] getRawPixels()
{
return bim.getRGB(0, 0, bim.getWidth(),bim.getHeight(), null, 0, bim.getWidth());
}


public byte[] getAsByteArray()
{
byte[] raw=new byte[bim.getWidth()*bim.getHeight()];
int[] pix=bim.getRGB(0, 0, bim.getWidth(),bim.getHeight(), null, 0, bim.getWidth());
for (int k=0;k<raw.length;k++) raw[k]=(byte)pix[k];
return raw;
}

public int getRed(int x,int y)
{
int[] pix=bim.getRGB(0, 0, bim.getWidth(),bim.getHeight(), null, 0, bim.getWidth());
int w=bim.getHeight();
return new Color(pix[(x)+w*(y)]).getRed();
}

//neighbours north south east west
private Vector<Integer> getNeighbours(int index,int[] pix)
{
Vector<Integer> v=new Vector<Integer>();
int w=bim.getWidth();
int h=bim.getHeight();
int y=index/w;
int x=index-w*y;
if (x>0) if (new Color(pix[(x-1)+w*(y)]).getRed()==255) v.add((x-1)+w*(y));
if (x<w-1) if (new Color(pix[(x+1)+w*(y)]).getRed()==255) v.add((x+1)+w*(y));
if (y>0) if (new Color(pix[(x)+w*(y-1)]).getRed()==255) v.add((x)+w*(y-1));
if (y<h-1) if (new Color(pix[(x)+w*(y+1)]).getRed()==255) v.add((x)+w*(y+1));
return v;
}

//
/**
 * get Neighbors having red canal with value 255 north south east west and diags
 * @param index pixel position
 * @param pix the image ordered in 1D array
 * @return list of neighbors positions 
 */
public Vector<Integer> getNeighboursWithDiags(int index,int[] pix)
{
Vector<Integer> v=new Vector<Integer>();
int w=bim.getWidth();
int h=bim.getHeight();
int y=index/w;
int x=index-w*y;
int _index;
if (x>0) if (new Color(pix[(x-1)+w*(y)]).getRed()==255) v.add((x-1)+w*(y));
if (x<w-1) if (new Color(pix[(x+1)+w*(y)]).getRed()==255) v.add((x+1)+w*(y));
if (y>0) if (new Color(pix[(x)+w*(y-1)]).getRed()==255) v.add((x)+w*(y-1));
if (y<h-1) if (new Color(pix[(x)+w*(y+1)]).getRed()==255) v.add((x)+w*(y+1));
//diagonal left up:
if ((x>0)&&(y>0)) 
	{
	_index=(x-1)+w*(y-1);
	if (new Color(pix[_index]).getRed()==255) v.add(_index);
	}
//diagonal right up:
if ((x<w-1)&&(y>0)) 
	{
	_index=(x+1)+w*(y-1);
	if (new Color(pix[_index]).getRed()==255) v.add(_index);
	}
//diagonal left down:
if ((x>0)&&(y<h-1)) 
	{
	_index=(x-1)+w*(y+1);
	if (new Color(pix[_index]).getRed()==255) v.add(_index);
	}
//diagonal right down:
if ((x<w-1)&&(y<h-1)) 
	{
	_index=(x+1)+w*(y+1);
	if (new Color(pix[_index]).getRed()==255) v.add(_index);
	}
return v;
}


//
/**
 * Not Neighbors north south east west and diags
 * @param index pixel position
 * @param pix the image ordered in 1D array
 * @return list of neighbors positions 
 */
public Vector<Integer> getNotNeighboursWithDiags(int index,int[] pix)
{
Vector<Integer> v=new Vector<Integer>();
int w=bim.getWidth();
int h=bim.getHeight();
int y=index/w;
int x=index-w*y;
int _index;
if (x>0) if (new Color(pix[(x-1)+w*(y)]).getRed()!=255) v.add((x-1)+w*(y));
if (x<w-1) if (new Color(pix[(x+1)+w*(y)]).getRed()!=255) v.add((x+1)+w*(y));
if (y>0) if (new Color(pix[(x)+w*(y-1)]).getRed()!=255) v.add((x)+w*(y-1));
if (y<h-1) if (new Color(pix[(x)+w*(y+1)]).getRed()!=255) v.add((x)+w*(y+1));
//diagonal left up:
if ((x>0)&&(y>0)) 
	{
	_index=(x-1)+w*(y-1);
	if (new Color(pix[_index]).getRed()!=255) v.add(_index);
	}
//diagonal right up:
if ((x<w-1)&&(y>0)) 
	{
	_index=(x+1)+w*(y-1);
	if (new Color(pix[_index]).getRed()!=255) v.add(_index);
	}
//diagonal left down:
if ((x>0)&&(y<h-1)) 
	{
	_index=(x-1)+w*(y+1);
	if (new Color(pix[_index]).getRed()!=255) v.add(_index);
	}
//diagonal right down:
if ((x<w-1)&&(y<h-1)) 
	{
	_index=(x+1)+w*(y+1);
	if (new Color(pix[_index]).getRed()!=255) v.add(_index);
	}
return v;
}




private void affNeighbours(Vector<Integer> v)
{
for (int index:v) 
	{
	int y=index/bim.getWidth();
	int x=index-bim.getWidth()*y;
	System.out.print("("+x+","+y+")");
	}
System.out.println();
}



public BufferedImage crop_(int xs,int ys,int ws,int hs)
{
if ((ws==0)||(hs==0)) return null;
if ((xs+ws)>(w())) return null;
if ((ys+hs)>(h())) return null;

BufferedImage bim2=new BufferedImage(ws,hs,BufferedImage.TYPE_INT_RGB);
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		bim2.setRGB(i, j, rgb);
		}
//return new CImageProcessing(bim2);
return bim2;
}

/**
 * return new cropped image
 * @param xs
 * @param ys
 * @param ws
 * @param hs
 * @return
 */
public CImageProcessing crop(int xs,int ys,int ws,int hs)
{
if ((ws==0)||(hs==0)) return null;
if ((xs+ws)>(w())) return null;
if ((ys+hs)>(h())) return null;

BufferedImage bim2=new BufferedImage(ws,hs,BufferedImage.TYPE_INT_RGB);
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		bim2.setRGB(i, j, rgb);
		}
return new CImageProcessing(bim2);
}

public void _crop(int xs,int ys,int ws,int hs)
{
if ((ws==0)||(hs==0)) return ;
if ((xs+ws)>(w())) return ;
if ((ys+hs)>(h())) return ;
int w=w();
int[] pix=this.getRawPixelsFromImage();
BufferedImage bim2=new BufferedImage(ws,hs,BufferedImage.TYPE_INT_RGB);
int i,j;
int[] pix2=new int[ws*hs];
for (int ii=0;ii<ws;ii++) 
	for (int jj=0;jj<hs;jj++) 
		{
		i=xs+ii;
		j=ys+jj;
		//int rgb=bim.getRGB(xs+i, ys+j);
		//bim2.setRGB(ii, jj, pix[i+w*j]);
		pix2[ii+ws*jj]= pix[i+w*j];
		}
bim2.setRGB(0, 0, ws, hs, pix2, 0, ws);
this.copy(bim2);
}



//
//
//public BufferedImage cropb(int xs,int ys,int ws,int hs)
//{
//if ((ws==0)||(hs==0)) return null;
//if ((xs+ws)>(w())) return null;
//if ((ys+hs)>(h())) return null;
//
//BufferedImage bim2=new BufferedImage(ws,hs,BufferedImage.TYPE_INT_RGB);
//for (int i=0;i<ws;i++) 
//	for (int j=0;j<hs;j++) 
//		{
//		int rgb=bim.getRGB(xs+i, ys+j);
//		bim2.setRGB(i, j, rgb);
//		}
//return bim2;
//}
//
//
//public CImageProcessing crop(int xs,int ys,int ws,int hs)
//{
//if ((ws==0)||(hs==0)) return null;
//if ((xs+ws)>(w())) return null;
//if ((ys+hs)>(h())) return null;
//
//CImageProcessing cimp=new CImageProcessing(ws,hs);
//cimp._crop(xs, ys, ws, hs);
//return cimp;
//}


/**
 * set 255 to all channels if brightness > brightness_threshold, else set all channels to 0
 * @param xs
 * @param ys
 * @param ws
 * @param hs
 * @param brightness
 * @return
 */
public CImageProcessing threshold(int xs,int ys,int ws,int hs,int brightness)
{
CImageProcessing bim2=new CImageProcessing(ws,hs);
Color c;
int bright;
float[] hsbvals=new float[3];
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		c=new Color(rgb);
		Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
		bright=(int)(hsbvals[2]*255);
		if ((bright>brightness))	bim2.setRGB(i, j, whiteCode);
		else 	bim2.setRGB(i, j, blackCode);
		}
return bim2;
}


/**
 * replace color by hue
 * @param xs
 * @param ys
 * @param ws
 * @param hs
 * @param brightness
 * @return
 */
public CImageProcessing setHue(int xs,int ys,int ws,int hs)
{
CImageProcessing bim2=new CImageProcessing(ws,hs);
Color c;
int val;
float[] hsbvals=new float[3];
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		c=new Color(rgb);
		Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
		val=(int)(hsbvals[0]*255);
		bim2.setRGB(i, j, new Color(val,val,val).getRGB());
		}
return bim2;
}


/**
 * replace color by color saturation
 * @param xs
 * @param ys
 * @param ws
 * @param hs
 * @param brightness
 * @return
 */
public CImageProcessing setSaturation(int xs,int ys,int ws,int hs)
{
CImageProcessing bim2=new CImageProcessing(ws,hs);
Color c;
int val;
float[] hsbvals=new float[3];
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		c=new Color(rgb);
		Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
		val=(int)(hsbvals[1]*255);
		bim2.setRGB(i, j, new Color(val,val,val).getRGB());
		}
return bim2;
}

/**
 * replace color by color brightness
 * @param xs
 * @param ys
 * @param ws
 * @param hs
 * @param brightness
 * @return
 */
public CImageProcessing setBrightness(int xs,int ys,int ws,int hs)
{
CImageProcessing bim2=new CImageProcessing(ws,hs);
Color c;
int val;
float[] hsbvals=new float[3];
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		c=new Color(rgb);
		Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
		val=(int)(hsbvals[2]*255);
		bim2.setRGB(i, j, new Color(val,val,val).getRGB());
		}
return bim2;
}


public void _setHue()
{
this.copy(setHue(0,0,w(),h()));
}

public CImageProcessing setHue()
{
return setHue(0,0,w(),h());
}


public CImageProcessing setSaturation()
{
return setSaturation(0,0,w(),h());
}


public void _setSaturation()
{
this.copy(setSaturation(0,0,w(),h()));
}


public void _setBrightness()
{
this.copy(setBrightness(0,0,w(),h()));
}

public CImageProcessing setBrightness()
{
return setBrightness(0,0,w(),h());
}




public CImageProcessing threshold(int brightness)
{
return this.threshold(0, 0, w(), h(),brightness);
}


/**
 * set 255 to all channels if brightness > brightness_threshold, else set all channels to 0
 * @param brightness : threshold
 * @return
 */
public void _threshold(int brightness)
{
copy(this.threshold(0, 0, w(), h(),brightness));
}


public CImageProcessing detectColor(int xs,int ys,int ws,int hs,
		int hueMin,int hueMax,int saturationMin,int brightnessMin)
{
CImageProcessing bim2=new CImageProcessing(ws,hs);
Color c;
int hue,sat,bright;
float[] hsbvals=new float[3];
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		c=new Color(rgb);
		Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
		hue=(int)(hsbvals[0]*255);
		sat=(int)(hsbvals[1]*255);
		bright=(int)(hsbvals[2]*255);
		if ((hue>=hueMin)&&(hue<=hueMax)&&(sat>saturationMin)&&(bright>brightnessMin))
				bim2.setRGB(i, j, whiteCode);
		else 	bim2.setRGB(i, j, blackCode);
		}
return bim2;
}

public CImageProcessing detectColor(int hueMin,int hueMax,int saturationMin,int brightnessMin)
{
return this.detectColor(0, 0, w(), h(), hueMin, hueMax, saturationMin, brightnessMin);
}
/**
 * 
 * @param xs
 * @param ys
 * @param ws
 * @param hs
 * @return
 */
public short[][] cropRED(int xs,int ys,int ws,int hs)
{
short[][] pix=new short[ws][hs];
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		Color c=new Color(rgb);
		pix[i][j]=(short)c.getRed();
		}
return pix;
}

/**
 * 
 * @param xs
 * @param ys
 * @param ws
 * @param hs
 * @return
 */
public short[][] cropGREEN(int xs,int ys,int ws,int hs)
{
short[][] pix=new short[ws][hs];
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		Color c=new Color(rgb);
		pix[i][j]=(short)c.getGreen();
		}
return pix;
}
/**
 * 
 * @param xs
 * @param ys
 * @param ws
 * @param hs
 * @return
 */
public short[][] cropBLUE(int xs,int ys,int ws,int hs)
{
short[][] pix=new short[ws][hs];
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		Color c=new Color(rgb);
		pix[i][j]=(short)c.getBlue();
		}
return pix;
}

/** crop the zone and put the Hue value in a short*/
public short[][] cropHUE(int xs,int ys,int ws,int hs)
{
short[][] pix=new short[ws][hs];
float[] hsbvals=new float[3];
Color c;
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		c=new Color(rgb);
		Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
		pix[i][j]=(short)(hsbvals[0]*255);
		}
return pix;
}

/** crop the zone and put the saturation value in a short*/
public short[][] cropSAT(int xs,int ys,int ws,int hs)
{
short[][] pix=new short[ws][hs];
float[] hsbvals=new float[3];
Color c;
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		c=new Color(rgb);
		Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
		pix[i][j]=(short)(hsbvals[1]*255);
		}
return pix;
}


public short[][] cropColorDetection(int xs, int ys, int ws, int hs, int hueMin,
		int hueMax, int satMin, int brightMin)
{
short[][] pix=new short[ws][hs];
float[] hsbvals=new float[3];
Color c;
int hue,sat,bright;
for (int i=0;i<ws;i++) 
	for (int j=0;j<hs;j++) 
		{
		int rgb=bim.getRGB(xs+i, ys+j);
		c=new Color(rgb);
		Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
		hue=(int)(hsbvals[0]*255);
		sat=(int)(hsbvals[1]*255);
		bright=(int)(hsbvals[2]*255);
		if ((hue>=hueMin)&&(hue<=hueMax)&&(sat>satMin)&&(bright>brightMin))
			pix[i][j]=(short)(255);
		else pix[i][j]=0;
		}
return pix;
}

/**
 * if val<thres set 0 else set 255
 * @param threshold
 */
//public void thresholdRed( int threshold )
//{
//if (pix==null) pix=this.getRawPixelsFromImage();
//for (int i=0;i<pix.length;i++) 
//	{
//	int val=new Color(pix[i]).getRed();
//	if ((val!=0)&(val!=255)) System.out.println(val);
//	if (val<threshold) pix[i]=Color.black.getRGB();else pix[i]=Color.white.getRGB();
//	}
//this.setPixelsDirect(pix, w(), h());
//}


public BufferedImage resample(int newWidth)
{
double coef= (double)newWidth/(double)this.getWidth();
int newHeight=(int)(this.getHeight()*coef);
BufferedImage bim2=new BufferedImage(newWidth,newHeight,BufferedImage.TYPE_INT_RGB);
for (int i=0;i<newWidth;i++) 
	for (int j=0;j<newHeight;j++) 
		{
		int oldi=(int)(i/coef);
		int oldj=(int)(j/coef);
		int rgb=bim.getRGB(oldi,oldj);
		bim2.setRGB(i, j, rgb);
		}
return bim2;
}

/**
 * better quality than resample
 * @param newWidth
 * @return
 */
public  BufferedImage resample2( int newWidth) 
{ 
	double coef= (double)newWidth/(double)this.getWidth();
	int newHeight=(int)(this.getHeight()*coef);
	return this.resample2(newWidth, newHeight);
	
}

/**
 * better quality than resample
 * @param newWidth
 * @return
 */
public  BufferedImage resample2( int newW, int newH) 
{ 
BufferedImage img=this.getBufferedImage();
Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_RGB);

Graphics2D g2d = dimg.createGraphics();
g2d.drawImage(tmp, 0, 0, null);
g2d.dispose();

return dimg;
}  



/**
 * set colors as negative
 */
public void _invert()
{
int r,g,b;
Color c;
int[] pix=this.getRawPixels();
for (int k=0;k<pix.length;k++) 
	{
	int code=pix[k];
	c=new Color(code);
	r=c.getRed();
	g=c.getGreen();
	b=c.getBlue();
	pix[k]=new Color(255-r,255-g,255-b).getRGB();
	}
this.setPixelsDirect(pix);
}




/**
 * get max red value
 */
public int maxRed()
{
int max=Integer.MIN_VALUE;
int r,g,b;
Color c;
for (int i=0;i<bim.getWidth();i++) 
	for (int j=0;j<bim.getHeight();j++) 
	{
	c=new Color(bim.getRGB(i, j));
	r=c.getRed();
	if (r>max) max=r;
	}
return max;
}



/**
 * get max red value
 */
public int minRed()
{
int min=Integer.MAX_VALUE;
int r,g,b;
Color c;
for (int i=0;i<bim.getWidth();i++) 
	for (int j=0;j<bim.getHeight();j++) 
	{
	c=new Color(bim.getRGB(i, j));
	r=c.getRed();
	if (r<min) min=r;
	}
return min;
}




public int getRGB(int x,int y)
{
if ((x<0)||(x>=w())||(y<0)||(y>=h())) return 0;
return bim.getRGB(x, y);
}

public void setRGB(int x,int y,int rgb){bim.setRGB(x, y,rgb);}

public void setRGB(int x,int y,int r,int g, int b)
{
int rgb=(255 << 24)| (r << 16) | (g << 8) | (b) ;
bim.setRGB(x, y,rgb);
}


/**
 * 
 * @param index i+w*h
 * @param rgb
 */
public void setRGB(int index,int rgb)
{
int y=index/w();
int x=index-y*w();
if ((x<0)||(x>w()-1)||(y<0)||(y>h()-1))
	{
	System.out.println( "setRGB error index="+index+" x="+x+" y="+y);
	return;
	}
bim.setRGB(x, y,rgb);
}

/**get the total luminosity (total of all pixels brightnesses)*/
public double calcLuminosity()
{
float[] hsb=new float[3];
int r,g,b;
Color c;
double l=0;
for (int i=0;i<getWidth();i++) 
	for (int j=0;j<getHeight();j++) 
	{
	//System.out.println("i="+i+" j="+j);
	c=new Color(bim.getRGB(i, j));
	r=c.getRed();
	g=c.getGreen();
	b=c.getBlue();
	Color.RGBtoHSB(r,g,b,hsb);
	l+=hsb[2];
	}
return l;
}

/**get the total luminosity (mean of all pixels brightnesses in all 3 color channels)*/
public double calcMeanBrightness()
{
float[] hsb=new float[3];
int r,g,b;
Color c;
double l=0;
//for (int i=0;i<getWidth();i++) 
//	for (int j=0;j<getHeight();j++) 
//	{
//	//System.out.println("i="+i+" j="+j);
//	c=new Color(bim.getRGB(i, j));
//	r=c.getRed();
//	g=c.getGreen();
//	b=c.getBlue();
//	Color.RGBtoHSB(r,g,b,hsb);
//	l+=hsb[2];
//	}
int n=this.w()*this.h();
int[] pix=this.getRawPixels();
for (int k=0;k<n;k++) 
	{
	c=new Color(pix[k]);
	r=c.getRed();
	g=c.getGreen();
	b=c.getBlue();
	Color.RGBtoHSB(r,g,b,hsb);
	l+=(r+g+b);
	}
return l/n/3.0;
}

public void _translate(int di, int dj)
{
BufferedImage bim2=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
for (int i=0;i<getWidth();i++) 
	for (int j=0;j<getHeight();j++) 
		{
		if (((i+di)>=0)&((j+dj)>=0)&((i+di)<this.getWidth())&((j+dj)<this.getHeight()))
		bim2.setRGB(i+di, j+dj, bim.getRGB(i, j));
		}
this.copy(bim2);
}

public void _flipX()
{
int w=getWidth();
int h=getHeight();
BufferedImage bim2=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
for (int i=0;i<w;i++) 
	for (int j=0;j<h;j++) 
		{
		bim2.setRGB(w-1-i, j, bim.getRGB(i, j));
		}
this.copy(bim2);
}

public void _flipY()
{
int w=getWidth();
int h=getHeight();
BufferedImage bim2=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
for (int i=0;i<w;i++) 
	for (int j=0;j<h;j++) 
		{
		bim2.setRGB(i, h-1-j, bim.getRGB(i, j));
		}
this.copy(bim2);
}


public void _turnLeft()
{
int w=getWidth();
int h=getHeight();
BufferedImage bim2=new BufferedImage(getHeight(),getWidth(),BufferedImage.TYPE_INT_RGB);
for (int i=0;i<w;i++) 
	for (int j=0;j<h;j++) 
		{
		bim2.setRGB(j,(w-1-i), bim.getRGB(i, j));
		}
this.copy(bim2);
}

public void _turnRight()
{
int w=getWidth();
int h=getHeight();
BufferedImage bim2=new BufferedImage(getHeight(),getWidth(),BufferedImage.TYPE_INT_RGB);
for (int i=0;i<w;i++) 
	for (int j=0;j<h;j++) 
		{
		bim2.setRGB((h-1-j),(i), bim.getRGB(i, j));
		}
this.copy(bim2);
}



/**
 * subtract the image and add an offset(to keep values >0)
 * @param cim2
 * @param offset
 */
public void _subtract(CImageProcessing cim2,int offset)
{
int r1,g1,b1,r2,g2,b2,r3,g3,b3;
Color c1,c2;
for (int i=0;i<getWidth();i++) 
	for (int j=0;j<getHeight();j++) 
	{
	c1=new Color(getRGB(i, j));
	r1=c1.getRed();
	g1=c1.getGreen();
	b1=c1.getBlue();
	c2=new Color(cim2.getRGB(i, j));
	r2=c2.getRed();
	g2=c2.getGreen();
	b2=c2.getBlue();
	r3=r1-r2+offset;
	g3=g1-g2+offset;
	b3=b1-b2+offset;
	
	if (r3<0) r3=0;
	if (g3<0) g3=0;
	if (b3<0) b3=0;
	if (r3>255) r3=255;
	if (g3>255) g3=255;
	if (b3>255) b3=255;
	
	bim.setRGB(i, j,new Color(r3,g3,b3).getRGB() );
	}
}


/**
 * subtract the image as Signal2D1D to r g and b channels 
 * and add an offset(to keep values >0)
 * @param im2
 * @param offset
 */
public void _subtract(Signal2D1D im2,int offset)
{
int r1,g1,b1,r3,g3,b3;
Color c1;
double c;
for (int i=0;i<getWidth();i++) 
	for (int j=0;j<getHeight();j++) 
	{
	c1=new Color(getRGB(i, j));
	r1=c1.getRed();
	g1=c1.getGreen();
	b1=c1.getBlue();
	c=im2.getValue(i, j);
	r3=(int)(r1-c+offset);
	g3=(int)(g1-c+offset);
	b3=(int)(b1-c+offset);
	
	if (r3<0) r3=0;
	if (g3<0) g3=0;
	if (b3<0) b3=0;
	if (r3>255) r3=255;
	if (g3>255) g3=255;
	if (b3>255) b3=255;
	
	bim.setRGB(i, j,new Color(r3,g3,b3).getRGB() );
	}
}

/**
 * calc the means along the columns in red channel
 * @return
 */
public  SignalDiscret1D1D _meanYRed()
{
SignalDiscret1D1D proj= new SignalDiscret1D1D();
_meanYRed(proj);
return proj;
}


/**
 * calc the means along the columns in red channel
 * @return
 */
public void _meanYRed(SignalDiscret1D1D proj)
{
Color c1;
int r1;
int w=w();
int h=h();
int[] pix=this.getRawPixelsFromImage();
double[] meanY=new double[w];
for (int i=0;i<w();i++) 
	for (int j=0;j<h;j++) 
		{
		//c1=new Color(getRGB(i, j));
		c1=new Color(pix[i+w*j]);
		r1=c1.getRed();
		meanY[i]+=r1;
		}
proj.init(w,0,w);
for (int i=0;i<w();i++) proj.setValue(i, meanY[i]/h);
}




/**
 * calc the means along the rows in red channel
 * @return
 */
public  SignalDiscret1D1D _meanXRed()
{
//Color c1;
//int r1;
//int w=w();
//int h=h();
//int[] pix=this.getRawPixelsFromImage();
//double[] meanX=new double[h];
//SignalDiscret1D1D proj=new SignalDiscret1D1D(meanX,0,h);
//for (int j=0;j<h;j++) 
//	for (int i=0;i<w;i++) 
//		{
//		c1=new Color(pix[i+w*j]);
//		r1=c1.getRed();
//		meanX[j]+=r1;
//		}
//for (int i=0;i<h();i++) meanX[i]/=w;
SignalDiscret1D1D proj=new SignalDiscret1D1D();
_meanXRed(proj);
return proj;
}



/**
 * calc the means along the rows in red channel
 * @return
 */
public  void _meanXRed(SignalDiscret1D1D proj)
{
Color c1;
int r1;
int w=w();
int h=h();
int[] pix=this.getRawPixelsFromImage();
double[] meanX=new double[h];
for (int j=0;j<h;j++) 
	for (int i=0;i<w;i++) 
		{
		c1=new Color(pix[i+w*j]);
		r1=c1.getRed();
		meanX[j]+=r1;
		}
proj.init(h, 0, h);
for (int j=0;j<h();j++) proj.setValue(j, meanX[j]/w);
}


/**
 * paste an image on this image, with offset
 * @param cim
 * @param dx horizontal offset
 * @param dy vertical offset
 */
public void _paste(CImageProcessing cim,int dx,int dy)
{
//if ((ws==0)||(hs==0)) return ;
if ((dx+cim.w())>(w()-1)) return ;
if ((dy+cim.h())>(h()-1)) return ;


int[] pix=this.getRawPixels();
int w=this.w();
int h=this.h();
int[] pixcim=cim.getRawPixels();
int wcim=cim.w();
int hcim=cim.h();
for (int i=0;i<wcim;i++) 
	for (int j=0;j<hcim;j++) 
		{
		int indexcim=j*wcim+i;
		//int rgb=cim.getRGB(i,j);
		int rgb=pixcim[indexcim];
		int index2=(dy+j)*w+(dx+i);
		pix[index2]=rgb;
		//this.setRGB(i+dx, j+dy, rgb);
		}
this.setPixelsDirect(pix, w, h);
}

/**
 * paste an image on this image, with offset
 * the result color is the mean of the 2 colors
 * @param cim
 * @param dx horizontal offset
 * @param dy vertical offset
 */
public void _pasteMean(CImageProcessing cim,int dx,int dy)
{
//if ((ws==0)||(hs==0)) return ;
if ((dx+cim.w())>(w()-1)) return ;
if ((dy+cim.h())>(h()-1)) return ;


int[] pix=this.getRawPixels();
int w=this.w();
int h=this.h();
int[] pixcim=cim.getRawPixels();
int wcim=cim.w();
int hcim=cim.h();
Color c1,c2,c3;
for (int i=0;i<wcim;i++) 
	for (int j=0;j<hcim;j++) 
		{
		int indexcim=j*wcim+i;
		//int rgb=cim.getRGB(i,j);
		int rgb2=pixcim[indexcim];
		int index2=(dy+j)*w+(dx+i);
		int rgb1=pix[index2];
		c1=new Color(rgb1);
		c2=new Color(rgb2);
		c3=new Color((c1.getRed()+c2.getRed())/2,(c1.getGreen()+c2.getGreen())/2,(c1.getBlue()+c2.getBlue())/2);
		pix[index2]=c3.getRGB();
		//this.setRGB(i+dx, j+dy, rgb);
		}
this.setPixelsDirect(pix, w, h);
}


public void _annulate()
{
int[] pix=this.getRawPixels();
int w=this.w();
int h=this.h();
for (int i=0;i<w();i++) 
	for (int j=0;j<h();j++) 
		{
		pix[j*w+i]=0;
		//this.setRGB(i, j, 0);
		}
this.setPixelsDirect(pix, w, h);
}


/**
 * return a new smoothed image by averaging with a kernel of size kernelSize*kernelSize
 * @param kernelSize
 * @return
 */
public void _smoothSquareKernel(int radius)
{
int[] pix=this.getRawPixels();
int w=this.w();
int h=this.h();
double r,g,b=0;
int c=0;
int[] pix2=new int[w*h];
int i2,j2,index2,code2;
for (int i=0;i<w;i++)
	for (int j=0;j<h;j++)
		{
		r=0;
		g=0;
		b=0;
		c=0;
		for (int ii=-radius;ii<radius;ii++)
			for (int jj=-radius;jj<radius;jj++)
				{
				i2=i+ii;
				j2=j+jj;
				index2=j2*w+i2;
				if ( (i2>=0) && (i2<w) && (j2>=0) && (j2<h) )
					{
					code2=pix[index2];
//					r+=(code2<<16)&255;
//					g+=(code2<<8)&255;
//					b+=code2&255;
					Color co=new Color(code2);
					r+=co.getRed();
					g+=co.getGreen();
					b+=co.getBlue();
					c++;
					}
				}
		r/=c;
		g/=c;
		b/=c;
//		int rgb=(255 << 24)| ((int)r << 16) | ((int)g << 8) | ((int)b) ;
//		pix2[j*w+i]=rgb;//s2.setValue(i, j, r);
		
		try
			{
			Color co=new Color((int)r,(int)g,(int)b);
			pix2[j*w+i]=co.getRGB();
			}
		catch (Exception e)
			{
			System.out.println(r+" "+g+" "+b);
			//e.printStackTrace();
			}
		}
this.setPixelsDirect(pix2, w, h);
}


/**
 * put the image in a bigger image with margin 
 * @param new_w
 * @param new_h
 * @param color_margin
 * @return
 */
public BufferedImage enlargeCanvas(int marginx, int marginy,int color_margin)
{
int w=w();
int h=h();
int nw=w()+2*marginx;
int nh=h()+2*marginy;
BufferedImage bim2=new BufferedImage(nw,nh,BufferedImage.TYPE_INT_RGB);
int[] pix=this.getRawPixels();

for (int i=0;i<nw;i++) 
	for (int j=0;j<nh;j++) 
		{
		if (i<marginx) bim2.setRGB(i, j, color_margin);
		else if (i>=w+marginx) bim2.setRGB(i, j, color_margin);
		else if (j<marginy) bim2.setRGB(i, j, color_margin);
		else if (j>=h+marginy) bim2.setRGB(i, j, color_margin);
		else bim2.setRGB(i, j, pix[(j-marginy)*w+(i-marginx)]);
		}
return bim2;
}

public void paintZone(Zone z, Color c) 
{
int[] pix=this.getRawPixels();
for (int index:z)
	{
	pix[index]=c.getRGB();
	}
this.setPixelsDirect(pix, w(), h());	
}


public void enhanceZoneBlue(Zone z) 
{
int r2=0,g2=0,b2=0,alpha2=128;
Color c;
int[] pix=this.getRawPixels();
for (int index:z)
	{
	c=new Color(pix[index]);
	r2=c.getRed();
	g2=c.getGreen();
	b2=(c.getBlue()+255)/2;
	pix[index]=new Color(r2,g2,b2,alpha2).getRGB();
	}
this.setPixelsDirect(pix, w(), h());	
}




/**
 * calc correlation (grey level) of sub image between 2 images
 * https://en.wikipedia.org/wiki/Digital_image_correlation_and_tracking
 * @param im1 image 1
 * @param im2 image 2
 * @param x0 x of top left corner of sub image
 * @param y0 y of top left corner of sub image
 * @param w0 width of sub image
 * @param h0 height of sub image
 * @param dx horizontal displacement of image 1
 * @param dy vertical displacement of image 1
 * @return
 */
public static double correlation(CImageProcessing im1,CImageProcessing im2,int x0,int y0, int w0,int h0,int dx,int dy)
{
int w=im1.w();
int h=im1.h();
double s=0,s1=0,s2=0;
Vecteur p1=new Vecteur(),p2=new Vecteur();
//int c1,c2;
Color c11,c12,c2;
int i1,j1;
float[] hsbvals11=new float[3];
float[] hsbvals12=new float[3];
float[] hsbvals2=new float[3];
double mean1=im1.meanGreyLevel(x0, y0, w0, h0);
double mean2=im2.meanGreyLevel(x0, y0, w0, h0);
for (int i=x0;i<x0+w0;i++) 
	for (int j=y0;j<y0+h0;j++) 
		{
		i1=i+dx;
		j1=j+dy;
		if ( ((i1)<0)||((i1)>(w-1))||((j1)<0)||((j1)>(w-1)) ) continue;
		c11= new Color(im1.getRGB(i,j));
		c12= new Color(im1.getRGB(i+dx,j+dy));
		c2= new Color(im2.getRGB(i, j));
		//p1.affect(c1.getRed(), c1.getGreen(), c1.getBlue());
		//p2.affect(c2.getRed(), c2.getGreen(), c2.getBlue());
		Color.RGBtoHSB(c11.getRed(), c11.getGreen(), c11.getBlue(), hsbvals11);
		Color.RGBtoHSB(c12.getRed(), c12.getGreen(), c12.getBlue(), hsbvals12);
		Color.RGBtoHSB(c2.getRed(), c2.getGreen(), c2.getBlue(), hsbvals2);
		s+=(hsbvals12[2]-mean1)*(hsbvals2[2]-mean2);
		s1+=Math.pow(hsbvals11[2]-mean1,2);
		s2+=Math.pow(hsbvals2[2]-mean2,2);
		}
return s/Math.sqrt(s1*s2);
}


/**
 * get the mean grey level in a sub image
 * @param x0 x of top left corner of sub image
 * @param y0 y of top left corner of sub image
 * @param w0 width of sub image
 * @param h0 height of sub image
 * @return
 */
public double meanGreyLevel(int x0,int y0, int w0,int h0)
{
double s=0;
Color c1,c2;
float[] hsbvals1=new float[3];
for (int i=0;i<x0+w0;i++) 
	for (int j=y0;j<y0+h0;j++) 
		{
		c1= new Color(this.getRGB(i, j));
		Color.RGBtoHSB(c1.getRed(), c1.getGreen(), c1.getBlue(), hsbvals1);
		s+=hsbvals1[2];
		}
return s/(w0*h0);
}



public static CImageProcessing convertFromByteArray(byte[] zone, int w, int h) 
{
int[] pix=new int[w*h];
int val;
for (int k=0;k<w*h;k++) 
	{
	val=zone[k]&0xFF;
	pix[k]=new Color(val,val,val).getRGB();
	}
CImageProcessing cimp=new CImageProcessing();
cimp.setPixelsDirect(pix, w, h);
return cimp;
}

/**
 * set min to 0 and max to 256
 */
public void _equaliseHistogramRed() 
{
int w=w();
int h=h();
int minRed=this.minRed();
int maxRed=this.maxRed();
double a=256.0/(maxRed-minRed);
double b=-minRed*a;
int[] pix=this.getRawPixelsFromImage();
int[] pix2=new int[pix.length];
Color c1;
int r1,r2;
for (int k=0;k<w*h;k++) 
		{
		c1=new Color(pix[k]);
		r1=c1.getRed();
		r2=(int)(a*r1+b);
		if (r2<0) r2=0;
		if (r2>255) r2=255;
		pix2[k]=new Color(r2,r2,r2).getRGB();
		}
this.setPixelsDirect(pix2, w, h);
}

/**
 * calc the sum of all the values
 * @return
 */
public double calcIntegralRed()
{
int[] pix=this.getRawPixels();
double d=0;
Color c;
for (int k=0;k<pix.length;k++) 
	{
	c=new Color(pix[k]);
	d+=c.getRed();
	}
return d;
}



}
//int rgb=(255 << 24)| (r << 16) | (g << 8) | (b) ;
