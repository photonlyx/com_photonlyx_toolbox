
// Author Laurent Brunel


package com.photonlyx.toolbox.math.image;


import java.awt.*;
import java.awt.image.*;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.photonlyx.toolbox.gui.PaletteInterface;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;

import java.io.*;

/**
provide an image that can be transformed
 */
public class CImage  implements ImageSource
{
	//links
	//internal
	private Image img=Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(320,240,new int[320*240],0,320));
	;
	//private int w=320,h=240; //size of the image

	public CImage()
	{
	}

	public CImage(int w,int h)
	{
		img=Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(w,h,new int[w*h],0,w));
	}

	public CImage(Image newimg)
	{
		this.img=newimg;
		//int w=newimg.getWidth(null);
		//int h=newimg.getHeight(null);
		//this.w=w;
		//this.h=h;
	}

	public CImage(String path,String filename,boolean lookInResources)
	{
		img=loadImage(path,filename,lookInResources,false);
	}


	public int w(){return img.getWidth(null);}
	public int h(){return img.getHeight(null);}



	/**
fill the image with a matrix of double. The son of class image.Palette is used to create the RGB codes
@param matrix array of values
	 */
	public void setPixelsIntMatrix(double[][] matrix, PaletteInterface palette)
	{
		if (palette==null)
		{
			Messager.messErr(getClass()+" "+Messager.getString("no_palette"));
		}
		int w=matrix[0].length;
		int h=matrix.length;
		int[] pix=new int[w*h];
		for (int i = 0; i < w; i++)
			for (int j = 0; j <h; j++)
			{
				double val = matrix[i][j];
				pix[w*(h-1-j)+i] = palette.getRGB(val);
			}

		img = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(w,h,pix,0,w));
	}


	/**
	 * copy "pixel to pixel" from the cim image
	 * @param cim
	 */
	public void copy(CImage cim)
	{
		this.setPixelsDirect(cim.getRawPixelsFromImage(),cim.w(), cim.h());
	}


	/**
fill the image with a list of int with RGB codes
@param pix the array of RGB codes
@param w width
@param h height
	 */
	public void setPixelsDirect(int[] pix,int w,int h)
	{
		img = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(w,h,pix,0,w));
		img.flush();

	}


	public void setPixelsFromIntImage(int[] values, int w, int h)
	{
		int[] pix=new int[w*h];
		int r,g,b,alpha=255;
		for (int k=0;k<w*h;k++)
		{
			r=values[k];
			g=values[k];
			b=values[k];
			pix[k]=(alpha << 24)| (r << 16) | (g << 8) | (b) ;
		}
		setPixelsDirect(pix, w, h)	;
	}


	public void setPixelsFromByteImage(byte[] values, int w, int h)
	{
		int[] pix=new int[w*h];
		int val,alpha=255;
		for (int k=0;k<w*h;k++)
		{				
			val=(int)values[k];
			if (val<0) val+=255;
			pix[k]=(alpha << 24)| (val << 16) | (val << 8) | (val) ;
			//System.out.println(r);
			//pix[k]=new Color(r,g,b).getRGB();
		}
		setPixelsDirect(pix, w, h)	;
	}

	public Image getImage(){return img;}

	public BufferedImage getBufferedImage()
	{
		BufferedImage bim=new BufferedImage(w(),h() ,BufferedImage.TYPE_INT_ARGB) ;
		int[] pixels=getRGBImage();
		for (int i=0;i<w();i++)
			for (int j=0;j<h();j++)
			{
				int index=i+w()*j;
				bim.setRGB(i,j,pixels[index]);
			}
		return bim;
	}


	public void setImage(Image img) 
	{
		this.img = img;
		//w=img.getWidth(null);
		//h=img.getHeight(null);
	}



	public CImage getCImage(){return this;}



	/**
get the pixels and size of an image
@param dimension the returned size: dimension[0] has the width and dimension[1] has the height
@return the pixels array in a one dimensional array 
	 */
	public  int[] getRawPixelsFromImage()
	{
		//Frame f=new Frame();
		/*try
	{
	MediaTracker tracker = new MediaTracker(null);
	tracker.addImage(img, 0);
	tracker.waitForID(0);
        }
catch (Exception e)
	{
	System.out.println(getClass()+" "+e);
	}*/
		if (img==null) return new int[0];

		int w = img.getWidth(null);
		int h = img.getHeight(null);
		int[] pixels = new int[w * h];
		PixelGrabber pg = new PixelGrabber(img, 0, 0, w, h, pixels, 0, w);
		try
		{
			pg.grabPixels();
		}
		catch (InterruptedException e)
		{
			System.err.println("interrupted waiting for pixels!");
			return null;
		}
		return pixels;
	}

	public boolean loadFile(String path,String filename,boolean lookInResources)
	{
		return this.loadFile(path, filename, lookInResources, true);
	}


	public boolean loadFile(String path,String filename,boolean lookInResources,boolean verbose)
	{
		img=loadImage(path,filename,lookInResources,verbose);	
		if (img==null) return false;else return true;
	}



	/**
	 * load an image in the current path or  in the resources
	 * @param filename
	 * @return
	 */
	public static  Image loadImage(String path,String filename)
	{
		return loadImage(path,filename,true,false);
	}

	public static  Image loadImage(String path,String filename,boolean lookInResources)
	{
		return loadImage(path,filename,true,true);
	}

	/**
	 * if not in resources use file name with path
	 * @param filename
	 * @param lookInResources
	 * @return
	 */
	public static  Image loadImage(String path,String filename,boolean lookInResources,boolean verbose)
	{
		int w=1;
		int h=1;
		//Image imag = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(w,h,new int[w*h],0,w));
		//imag.flush();

		Image imag=null;

		String s=filename;
		if (verbose)
			System.out.println("CImage loadImage"+" "+"Loading image: "+path+File.separator+s);
		if 	(
				(s.endsWith(".jpg"))
				||(s.endsWith(".JPG"))
				||(s.endsWith(".jpeg"))
				||(s.endsWith(".JPEG"))
				|| (s.endsWith(".GIF"))
				||(s.endsWith(".gif"))
				|| (s.endsWith(".PNG"))
				||(s.endsWith(".png"))
				/*|| (s.endsWith(".BMP"))
	||(s.endsWith(".bmp"))*/
				)
		{
			try
			{
				if (verbose) System.out.println("CImage"+" "+"Load  image: "+path+File.separator+filename);
				if (lookInResources) imag=Util.getImage(filename);//get the image in resources or in the Global.chemin
				else imag=Util.getImageAtAnyPath(path+File.separator+filename,false);

				if (imag==null)
				{
					if (verbose) Messager .messErr("Can't load file: "+path+File.separator+filename);
				}
				w=imag.getWidth(null);
				h=imag.getHeight(null);
				if (verbose) System.out.println("CImage"+" "+"Loaded  image: "+path+File.separator+filename+" "+w+"x"+h);
			}
			catch (Exception e)
			{
				System.out.println(Messager.getString("Erreur_chargement_image\n")+e);
				e.printStackTrace();
				return null;
			}
		}

		if 	(
				(s.endsWith(".bmp"))
				||(s.endsWith(".BMP"))
				)
		{
			//System.out.println("CImage"+" "+"Loading bmp image: "+s);
			//	BMPLoader bmpLoader=new BMPLoader();		
			FileInputStream in=null ;
			try 
			{
				in = new FileInputStream(path+File.separator+filename);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			if (in==null) return null;
			imag=BMPLoader.read(in);
			w=imag.getWidth(new JPanel());
			h=imag.getHeight(new JPanel());
			//System.out.println("CImage"+" "+"Loaded image bmp: "+s+" "+w+"x"+h);
		}	


		if (imag!=null) imag.flush();

		return imag;

	}




	/**
save the image in the Global path
	 */
	public void saveImage(String filename,int compLevel)
	{
		saveImage(Global.path,filename,compLevel);	
	}

	public void saveImage(String filename)
	{
		saveImage(Global.path,filename,1);	
	}

	public void saveImage(String path,String _filename)
	{
		saveImage( path, _filename,1);
	}
	/**
save the image in the file where its from 

	 * 
	 * @param path put / at the end
	 * @param _filename
	 * @param compLevel compression level 0 to 9
	 */
	public void saveImage(String path,String _filename,int compLevel)
	{
		img.flush();
		//if ((_filename.endsWith(".PNG"))||(_filename.endsWith(".png")))
		//	{
		//	ImageUtils.saveBufferedImageToPNG(path,_filename,img,compLevel);
		//	//System.out.println(getClass()+" "+path+_filename+" saved");
		//	}
		//else 
		if ((_filename.endsWith(".JPG"))||(_filename.endsWith(".jpg")))
		{
			try 
			{
				File outputfile = new File(path+File.separator+_filename);
				CImageProcessing cimp=new CImageProcessing(this);
				ImageIO.write(cimp.getBufferedImage(), "png", outputfile);
			} 
			catch (IOException e) 
			{
				System.err.println(e);
				e.printStackTrace();
			}   
		}
		else
		{
			String f=_filename;
			if (!f.endsWith(".png")) f+=".png";
			ImageUtils.saveBufferedImageToPNG(path,f,img);
			//System.out.println(getClass()+" "+path+_filename+".png"+" saved");
		}
	}


	/**
save the image in the file where its from 
	 */
	public void saveImageGlobal(String _fullfilename)
	{
		img.flush();
		if ((_fullfilename.endsWith(".PNG"))||(_fullfilename.endsWith(".png")))
		{
			ImageUtils.saveBufferedImageToPNG(_fullfilename,img);
			System.out.println(getClass()+" "+_fullfilename+" saved");
		}
		else 
		{
			ImageUtils.saveBufferedImageToPNG(_fullfilename+".png",img);
			System.out.println(getClass()+" "+_fullfilename+".png"+" saved");
		}
	}



	/**
return the red channel of the image only
	 */
	public byte[] getRedImage()
	{
		//int[] dimension=new int[2];
		int[] im=getCImage().getRawPixelsFromImage();
		//int n=dimension[0]*dimension[1];
		int n=w()*h();
		byte[] redImage=new byte[n];
		for (int i=0;i<n;i++) 
		{
			redImage[i]=(byte)(new Color(im[i]).getRed());
		}
		return redImage;
	}

	/**
return the red channel of the image only
	 */
	public short[] getBrightnessImageInShorts()
	{
		//int[] dimension=new int[2];
		int[] im=getCImage().getRawPixelsFromImage();
		//int n=dimension[0]*dimension[1];
		int n=w()*h();
		short[] redImage=new short[n];
		float[] hsb=new float[3];
		for (int i=0;i<n;i++) 
		{
			Color c=new Color(im[i]);
			Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
			redImage[i]=(short)(hsb[2]*255);
		}
		return redImage;
	}
	/**
return the red channel of the image only
	 */
	public short[] getRedImageInShorts()
	{
		//int[] dimension=new int[2];
		int[] im=getCImage().getRawPixelsFromImage();
		//int n=dimension[0]*dimension[1];
		int n=w()*h();
		short[] redImage=new short[n];
		for (int i=0;i<n;i++) 
		{
			redImage[i]=(short)(new Color(im[i]).getRed());
		}
		return redImage;
	}



	/**
/**
return the raw pixels in color encoded as java.awt
	 */
	public int[] getRGBImage()
	{
		//int[] dimension=new int[2];
		int[] im=getRawPixelsFromImage();
		return im;
	}

	public float[] getBrightnessImageInFloats() 
	{
		//int[] dimension=new int[2];
		int[] im=getCImage().getRawPixelsFromImage();
		int n=w()*h();
		//int n=dimension[0]*dimension[1];
		float[] imf=new float[n];
		for (int i=0;i<n;i++) 
		{
			imf[i]=(float)(new Color(im[i]).getRed());
		}
		return imf;
	}



	/**
	 * return a byte array with one of channels (R G B H S B alpha)
	 * @param channel (R G B H S B or alpha)
	 * @return  the channel in a byte array
	 */
	public   byte[]  getChannel (String channel)
	{
		return ImageUtils.getChannel(getImage(),channel);
	}


	/**
	 * keep only  one of the channels (R G B H S B alpha)
	 * @param channel (R G B H S B or alpha)
	 */
	public  void  applyChannel (String channel)
	{
		byte[] chan= ImageUtils.getChannel(getImage(),channel);
		int[] pix=new int[w()*h()];
		ImageUtils.convertByteChannelToJavaImage(chan,pix);
		this.setPixelsDirect(pix, w(), h());

	}


	public CImage duplicate()
	{
		//duplicate the raw pixels:
		int[] pix=this.getRawPixelsFromImage();
		int[] pix2=new int[w()*h()];
		for (int k=0;k<w()*h();k++) pix2[k]=pix[k];

		Image im2 = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(w(),h(),pix2,0,w()));
		im2.flush();

		CImage jim2=new CImage(im2);
		return jim2;
	}

	/**
	 * subtract each channel with the channels of jim2
	 * @param im2
	 */
	public void _sub(CImage jim2)
	{
		int[] pix1=this.getRawPixelsFromImage();
		int[] pix2=jim2.getRawPixelsFromImage();
		int[] pix=new int[w()*h()];
		int alpha=0;
		for (int k=0;k<pix1.length;k++)
		{
			int r1=ImageUtils.R(pix1[k]);
			int g1=ImageUtils.G(pix1[k]);
			int b1=ImageUtils.B(pix1[k]);
			int r2=ImageUtils.R(pix2[k]);
			int g2=ImageUtils.G(pix2[k]);
			int b2=ImageUtils.B(pix2[k]);
			int r=(r1-r2);
			int g=(g1-g2);
			int b=(b1-b2);
			if (r>255) r=255; if (r<0) r=0;
			if (g>255) g=255; if (g<0) g=0;
			if (b>255) b=255; if (b<0) b=0;
			int coul=(alpha << 24)| (r << 16) | (g << 8 | (b) );
			pix[k]=coul;
		}
		this.setPixelsDirect(pix, w(), h());
	}

	/**
	 * add to each channel (R G and B)  a  value, if >255 keep 255, if <0 keep 0
	 * @param im2
	 */
	public void _add(int v)
	{
		if (v==0) return;
		int[] pix1=this.getRawPixelsFromImage();
		int[] pix=new int[w()*h()];
		Color c1,c;
		int alpha=0;
		for (int k=0;k<pix1.length;k++)
		{
			//	int r1=Utils.R(pix1[k]);
			//	int g1=Utils.G(pix1[k]);
			//	int b1=Utils.B(pix1[k]);
			//	int alpha1=Utils.alpha(pix1[k]);
			//	int r=(byte)(r1+v);
			//	int g=(byte)(g1+v);
			//	int b=(byte)(b1+v);
			//	byte alpha=(byte)(alpha1+v);
			//	int coul=(alpha << 24)| (r << 16) | (g << 8) | (b) ;

			c1=new Color(pix1[k]);
			//int r=c1.getRed()+v;
			//int g=c1.getGreen()+v;
			//int b=c1.getBlue()+v;
			int r=ImageUtils.R(pix1[k])+v;
			int g=ImageUtils.G(pix1[k])+v;
			int b=ImageUtils.B(pix1[k])+v;
			if (r>255) r=255; if (r<0) r=0;
			if (g>255) g=255; if (g<0) g=0;
			if (b>255) b=255; if (b<0) b=0;

			//	c=new Color(r,g,b);
			//	pix[k]=c.hashCode();
			pix[k]=(alpha << 24)| (r << 16) | (g << 8) | (b) ;
		}
		this.setPixelsDirect(pix, w(), h());
	}






	/**
	 * multiply each channel (R G and B) by a  value, if >255 keep 255, if <0 keep 0
	 * @param im2
	 */
	public void _mul(double v)
	{
		if (v==1) return;
		int[] pix1=this.getRawPixelsFromImage();
		int[] pix=new int[w()*h()];
		int alpha=0;
		for (int k=0;k<pix1.length;k++)
		{
			int r=(int)(ImageUtils.R(pix1[k])*v);
			int g=(int)(ImageUtils.G(pix1[k])*v);
			int b=(int)(ImageUtils.B(pix1[k])*v);
			if (r>255) r=255; if (r<0) r=0;
			if (g>255) g=255; if (g<0) g=0;
			if (b>255) b=255; if (b<0) b=0;
			pix[k]=(alpha << 24)| (r << 16) | (g << 8) | (b) ;
		}
		this.setPixelsDirect(pix, w(), h());
	}



}




