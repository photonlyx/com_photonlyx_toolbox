package com.photonlyx.toolbox.math.function.usual3D1D;

import com.photonlyx.toolbox.math.function.Function3D1D;
import com.photonlyx.toolbox.math.geometry.Vecteur;


/**
 * f(r)=exp(-(r/sigma)²)
 * @author laurent
 *
 */
public class SphericalGauss extends Function3D1D
{
private double sigma=1;//sigma


	@Override
	public double f(double[] in)
	{
	Vecteur p=new Vecteur(in);
	//distance to (0,0,0)
	double r=p.norme();
	return Math.exp(-Math.pow(r/sigma,2));
	}

}
