package com.photonlyx.toolbox.math.function.usual3D1D;

import com.photonlyx.toolbox.math.function.Function3D1D;
import com.photonlyx.toolbox.math.geometry.Vecteur;

public class Sphere extends Function3D1D
{
private double radius=1;//radius


	@Override
	public double f(double[] in)
	{
	Vecteur p=new Vecteur(in);
	//distance to (0,0,0)
	double r=p.norme();
	if (r>radius) return 0; else return 1;
	}

}
