
// Author Laurent Brunel

package com.photonlyx.toolbox.math.function.usual;

import com.photonlyx.toolbox.math.function.Function1D1D;


/** function ax+b */
public  class Line extends Function1D1D
{

private double a,b;

protected static String defaultDefinition = null;

public Line()
{
	
}

/**
 * function ax+b
 * @param a
 * @param b
 */
public Line(double a,double b)
{
this.a=a;
this.b=b;
}
public double getA() {
	return a;
}

public void setA(double a) {
	this.a = a;
}

public double getB() {
	return b;
}

public void setB(double b) {
	this.b = b;
}


public static double getDistanceToPoint(double a, double b, double x, double y)
	{
	return Math.abs((y-b-a*x)/Math.sqrt(a*a+1));
	}

public static double getDistanceToPointSigned(double a, double b, double x, double y)
	{
	return (y-b-a*x)/Math.sqrt(a*a+1);
	}

public double getDistanceToPoint(double x, double y)
	{
	return Math.abs((y-b-a*x)/Math.sqrt(a*a+1));
	}

public double a() {return a;}
public double b() {return b;}





public  double f(double x)
{
double r;
r=a*x+b;
return r;
}

/** 
 * Computes the intersection with another line. 
 * 
 * @param line Line to intersect.
 * @return null if slopes of the 2 lines are the same, otherwise, 
 * the coordinates of the intersection.
 */
public double[] getIntersectionWithLine(Line line)
	{
	if (line.slope() == a)
		{
		return null;
		}

	if (a == 0)
		{
		double [] result = {line.reversef(b), b};
		return result;
		}

	double y = (a * line.offset() - line.slope() * b) / (a - line.slope());
	double x = (y - b)/a;

	double[] result = {x , y};
	return result;
	}

public static Line[] getBisectors(Line line1, Line line2)
	{

	if (line1.a() == line2.a())
		{
		return null;
		}

	double f1 = Math.sqrt(line1.a()*line1.a() + 1);
	double f2 = Math.sqrt(line2.a()*line2.a() + 1);

	double ratio = f1-f2;
	
	double a1 = (line2.a()*f1 - line1.a() * f2)/ratio;
	double b1 = (line2.b()*f1 - line1.b() * f2)/ratio;

	Line bisector1 = new Line(a1,b1);

	ratio = -f1-f2;
	a1 = (-line2.a()*f1 - line1.a() * f2)/ratio;
	b1 = (-line2.b()*f1 - line1.b() * f2)/ratio;

	Line bisector2 = new Line(a1,b1);

	Line[] result = new Line[2];
	result[0] = bisector1;
	result[1] = bisector2;

	return result;
	}

/** 
 * Computes the x value of the point that is on
 * the line and that has the given y value. If the 
 * line is horizontal (slope = 0), this method returns
 * null.
 * 
 * @param y y value(!)
 * @return null if slope = 0, the x value otherwise.
 */
public Double reversef(double y)
	{
	if (a == 0) return null;
	double d1 = y-b;
	double result = d1/a;
	return result;
	}

public double slope() {return a;}
public double offset() {return b;}

/** 
 * Given a rectangular area, computes the coordinates of the intersections
 * between the line and the rectangle. Returns null if there is no intersection.
 * 
 * @param xmin min x of the area
 * @param ymin min y of the area
 * @param xmax max x of the area
 * @param ymax max y of the area
 * @return null if there is no intersection, otherwise, returns an array of 4 doubles
 * representing the coordinates of the 2 intersection points (x1,y1,x2,y2)
 */
public double[] getExtremePointsInBoudary(double xmin, double ymin, double xmax, double ymax)
	{
	double y1 = f(xmin);
	Double x1;
	Double x2 = 0.0;
	double y2 = 0.0;

	if (y1 >= ymin && y1 <= ymax)
		{
		x1 = xmin;
		}
	else
		{
		if (a == 0)
			{
			x1 = null;
			}
		else
			{
			x1 = reversef(ymax);

			if (x1 >= xmin && x1 <= xmax)
				{
				y1 = ymax;
				}
			else
				{
				x1 = reversef(ymin);

				if (x1 >= xmin && x1 <= xmax)
					{
					y1 = ymin;
					}
				else
					{
					x1 = null;
					}
				}
			}
		}

	if (x1 != null)
		{
		y2 = f(xmax);
		if (y2 >= ymin && y2 <= ymax)
			{
			x2 = xmax;
			}
		else
			{
			x2 = reversef(ymax);
			if (x2 != null && x2 != x1 && y1 != ymax && x2 >= xmin && x2 <= xmax)
				{
				y2 = ymax;
				}
			else
				{
				x2 = reversef(ymin);
				y2 = ymin;
				}
			}
		}

	if (x1 == null)
		{
		return null;
		}
	else
		{
		double[] result = {x1, y1, x2, y2};
		if (result[0] > result[2])
			{
			double px = result[0];
			double py = result[1];
			result[0] = result[2];
			result[1] = result[3];
			result[2] = px;
			result[3] = py;
			}

		return result;
		}
	}

public void setParams(double a2, double b2) 
{
this.a=a2;
this.b=b2;
}

}
