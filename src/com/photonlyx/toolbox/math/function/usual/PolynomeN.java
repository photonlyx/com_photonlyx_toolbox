package com.photonlyx.toolbox.math.function.usual;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.photonlyx.toolbox.math.function.Function1D1D;



/** polynome of choosable order . 
The parameters corresponding to the coefs are automatically updated and named 
a0,a1,...,ai,...a(order) by the init method*/
public  class PolynomeN extends Function1D1D
{
//params
private int order=3;

//internal
private double[] coef={1,1,1,1};


public PolynomeN()
	{
	super();
	}

public PolynomeN(double[] _coef)
	{
	coef=new double[_coef.length];
	for(int i =0; i < _coef.length;i++)
		{
		coef[i]=_coef[i];
		}
	order=_coef.length-1;
	}




public  double f(double x)
{
double r=0;

// System.out.println(getClass());
// for (int i=0;i<=order.val;i++)
// 	System.out.print(" a"+i+"="+coef[i].val);
// System.out.println();

for (int i=0;i<=order;i++)	r+=coef[i]*Math.pow(x,i);
return r;
}


/**
@return order of the polynom
*/
public int getOrder()
{
return order;
}

/**
set the coef i of the polynom
*/
public void setCoef(int i,double c)
{
coef[i]=c;
}

public String toString()
	{
	return toString("x","0.00000E0");
	}

public String toString(String xSymbol,String format)
{
String r = "";
NumberFormat nf=new DecimalFormat(format);

for (int i=0; i<=(int)order; i++)
	{
	r += nf.format(coef[i]) ;
	if (i!=0)
		{
		r+="*"+xSymbol+"^" + i;
		}
	r+=" + ";
	}
r = r.substring(0,r.length()-3);
return r;
}

/**
 * coef of the polynom
 * @param i  from 0 to order
 * @return
 */
public double getCoef(int i)
{
	return coef[i];
}
public double[] getCoefs()
{
	return coef;
}
}