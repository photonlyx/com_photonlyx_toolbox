// Author Laurent Brunel

package com.photonlyx.toolbox.math.function.usual;

import com.photonlyx.toolbox.math.function.Function1D1D;


/** gaussian function (bell curve) */
public  class Gauss extends Function1D1D
{
private double sigma=1,surface,centre=0,coef;



/**
 * create a gaussian
 * @param sigma
 * @param centre
 * @param surface integral of the curve
 */
public Gauss(double sigma, double centre, double surface) {
	super();
	this.sigma = sigma;
	this.surface = surface;
	this.centre = centre;
	update();
}



public double getSigma() {
	return sigma;
}


/**
 * set the sigma and update
 * @param sigma
 */
public void setSigma(double sigma) 
{
	this.sigma = sigma;
	update();
}



public double getSurface() {
	return surface;
}



public void setSurface(double surface) {
	this.surface = surface;
	update();
}



public double getCentre() {
	return centre;
}



public void setCentre(double centre) {
	this.centre = centre;
}



/** set the surface according to the sigma*/
private void update()
{
coef=sigma*Math.sqrt(2*Math.PI);
}

public  double f(double x)
{
x=x-centre;
return surface*Math.exp(-x*x/(2*sigma*sigma))/(coef);
}

}
