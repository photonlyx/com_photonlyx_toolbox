package com.photonlyx.toolbox.math.function.usual2D1D;

import com.photonlyx.toolbox.math.function.Function2D1D;
import com.photonlyx.toolbox.math.matrix.FullMatrix;
import com.photonlyx.toolbox.math.matrix.LeastSquare;
import com.photonlyx.toolbox.math.signal.Signal2D1D;


/**
 * z=a*x+b*y+c
 * @author laurent
 *
 */
public class Plane2 extends Function2D1D
{
	private double a,b,c;

	public Plane2()
	{

	}
	/**
	 * 
	 * @param o a point of the plane
	 * @param n normalised normal of the plane
	 */
	public Plane2(double a,double b,double c)
	{
		this.a=a;
		this.b=b;
		this.c=c;
	}

	@Override
	public double f(double[] x)
	{
		double z;
		z=a*x[0]+b*x[1]+c;
		return z;
	}

	/**
	 * fit the plane to the data
	 * @param data
	 */
	public void fit(Signal2D1D data)
	{
		int n=data.dimx()*data.dimy();//nb of points
		int p=3;//nb of parameters (a b and c here)
		//fill the matrices:
		FullMatrix X=new FullMatrix(n,p);// design matrix 
		FullMatrix Y=new FullMatrix(n,1);// response variables matrix:
		double x,y;
		double sx=data.samplingx();
		double sy=data.samplingy();
		int index;
		for (int i=0;i<data.dimx();i++)
			for (int j=0;j<data.dimy();j++)
			{
				x=data.xmin()+i*sx;
				y=data.ymin()+j*sy;
				index=i+data.dimx()*j;
				X.affect(index,0,x);
				X.affect(index,1,y);
				X.affect(index,2,1);
				Y.affect(index,0,data.z(i, j));
			}
		//fill
		FullMatrix beta=LeastSquare.solve(X,Y,false);
		a=beta.el(0,0);
		b=beta.el(1,0);
		c=beta.el(2,0);
	}

}
