package com.photonlyx.toolbox.math.function.usual2D1D;

import com.photonlyx.toolbox.math.function.Function2D1D;

public class SphereCap extends Function2D1D
{
private double r=1,xcentre,ycentre,zcentre;

/**
 * 
 * @param r radius
 * @param xcentre
 * @param ycentre
 * @param zcentre
 */
public SphereCap(double r,double xcentre,double ycentre,double zcentre)
{
super();
this.r = r;
this.xcentre = xcentre;
this.ycentre = ycentre;
this.zcentre = zcentre;
}

@Override
public double f(double[] in)
{
double z;
double d=Math.sqrt(Math.pow(in[0]-xcentre,2)+Math.pow(in[1]-ycentre,2));
if (d>r) z=0;
else z=Math.sqrt(Math.pow(r,2)-Math.pow(d,2))+zcentre;
if (z<0) z=0;
return z;
}



}
