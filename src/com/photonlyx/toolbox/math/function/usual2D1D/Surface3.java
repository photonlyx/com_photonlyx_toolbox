package com.photonlyx.toolbox.math.function.usual2D1D;

import com.photonlyx.toolbox.math.function.Function2D1D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.matrix.FullMatrix;
import com.photonlyx.toolbox.math.matrix.LeastSquare;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.TextFiles;


/**
 * z=a*x²+b*y²+c*xy+d*x+e*y+f
 * @author laurent
 *
 */
public class Surface3 extends Function2D1D
{
	private double[] a=new double[10];

	public Surface3()
	{

	}
	
	public void load(String fullFilename,boolean alarm,boolean verbose)
	{
		StringBuffer sb=TextFiles.readFile(fullFilename, alarm, verbose);
		if (sb==null) return;
		Definition def=new Definition(sb.toString());
		int i=0;for (String s:def) a[i++]=Double.parseDouble(s);
	}

	@Override
	public double f(double[] xy)
	{
		double x,y,z;
		x=xy[0];
		y=xy[1];
		int k=0;
		z=      a[k++]*x*x*x+
				a[k++]*x*x*y+
				a[k++]*x*y*y+
				a[k++]*y*y*y+
				a[k++]*x*x+
				a[k++]*x*y+
				a[k++]*y*y+
				a[k++]*x+
				a[k++]*y+
				a[k++];
		return z;
	}

	/**
	 * fit the plane to the data
	 * @param data
	 */
	public void fit(Signal2D1D data,boolean verbose)
	{
		int n=data.dimx()*data.dimy();//nb of points
		int p=a.length;//nb of parameters (a b and c here)
		//fill the matrices:
		FullMatrix X=new FullMatrix(n,p);// design matrix 
		FullMatrix Y=new FullMatrix(n,1);// response variables matrix:
		double x,y;
		double sx=data.samplingx();
		double sy=data.samplingy();
		int index,k;
		for (int i=0;i<data.dimx();i++)
			for (int j=0;j<data.dimy();j++)
			{
				x=data.xmin()+i*sx;
				y=data.ymin()+j*sy;
				index=i+data.dimx()*j;
				k=0;
				X.affect(index,k++,x*x*x);
				X.affect(index,k++,x*x*y);
				X.affect(index,k++,x*y*y);
				X.affect(index,k++,y*y*y);
				X.affect(index,k++,x*x);
				X.affect(index,k++,x*y);
				X.affect(index,k++,y*y);
				X.affect(index,k++,x);
				X.affect(index,k++,y);
				X.affect(index,k++,1);
				Y.affect(index,0,data.z(i, j));
			}
		//fill
		FullMatrix beta=LeastSquare.solve(X,Y,verbose);
		for (k=0;k<p;k++) a[k]=beta.el(k,0);
	}

public double[] getParams() {return a;}
}
