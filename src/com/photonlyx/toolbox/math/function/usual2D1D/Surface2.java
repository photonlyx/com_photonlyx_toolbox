package com.photonlyx.toolbox.math.function.usual2D1D;

import com.photonlyx.toolbox.math.function.Function2D1D;
import com.photonlyx.toolbox.math.matrix.FullMatrix;
import com.photonlyx.toolbox.math.matrix.LeastSquare;
import com.photonlyx.toolbox.math.signal.Signal2D1D;


/**
 * z=a*x²+b*y²+c*xy+d*x+e*y+f
 * @author laurent
 *
 */
public class Surface2 extends Function2D1D
{
	private double a,b,c,d,e,f;

	public Surface2()
	{

	}

	@Override
	public double f(double[] x)
	{
		double z;
		z=a*x[0]*x[0]+b*x[1]*x[1]+c*x[0]*x[1]+d*x[0]+e*x[1]+f;
		return z;
	}

	/**
	 * fit the plane to the data
	 * @param data
	 */
	public void fit(Signal2D1D data)
	{
		int n=data.dimx()*data.dimy();//nb of points
		int p=6;//nb of parameters (a b and c here)
		//fill the matrices:
		FullMatrix X=new FullMatrix(n,p);// design matrix 
		FullMatrix Y=new FullMatrix(n,1);// response variables matrix:
		double x,y;
		double sx=data.samplingx();
		double sy=data.samplingy();
		int index;
		for (int i=0;i<data.dimx();i++)
			for (int j=0;j<data.dimy();j++)
			{
				x=data.xmin()+i*sx;
				y=data.ymin()+j*sy;
				index=i+data.dimx()*j;
				X.affect(index,0,x*x);
				X.affect(index,1,y*y);
				X.affect(index,2,x*y);
				X.affect(index,3,x);
				X.affect(index,4,y);
				X.affect(index,5,1);
				Y.affect(index,0,data.z(i, j));
			}
		//fill
		FullMatrix beta=LeastSquare.solve(X,Y,false);
		a=beta.el(0,0);
		b=beta.el(1,0);
		c=beta.el(2,0);
		d=beta.el(3,0);
		e=beta.el(4,0);
		f=beta.el(5,0);
	}


}
