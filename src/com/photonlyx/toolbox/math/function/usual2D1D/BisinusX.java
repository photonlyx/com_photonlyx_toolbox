package com.photonlyx.toolbox.math.function.usual2D1D;

import com.photonlyx.toolbox.math.function.Function2D1D;

public class BisinusX extends Function2D1D
{
private double wavelength=Math.PI*2;
private double amp=1;

public BisinusX()
{

}

public BisinusX(double wavelength)
{
this.wavelength=wavelength;
}

/**
 * 
 * @param wavelength lenght of the wave
 * @param amp amplitude of the wave
 */
public BisinusX(double wavelength,double amp)
{
this.wavelength=wavelength;
this.amp=amp;
}



@Override
public double f(double[] in)
{
return amp*Math.sin(Math.PI*2/wavelength*in[0]);
}

}
