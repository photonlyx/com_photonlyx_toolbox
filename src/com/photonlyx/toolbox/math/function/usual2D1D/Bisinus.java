package com.photonlyx.toolbox.math.function.usual2D1D;

import com.photonlyx.toolbox.math.function.Function2D1D;

public class Bisinus extends Function2D1D
{
double wavelength=Math.PI*2;

public Bisinus()
{

}

public Bisinus(double wavelength)
{
this.wavelength=wavelength;
}



@Override
public double f(double[] in)
{
return Math.sin(Math.PI*2/wavelength*in[0])*Math.sin(Math.PI*2/wavelength*in[1]);
}

}
