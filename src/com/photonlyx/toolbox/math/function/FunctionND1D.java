// Author Laurent Brunel

package com.photonlyx.toolbox.math.function;


/**
function from  N dimension to  1 dimension
*/
public abstract class FunctionND1D 
{



/**
Multidimensional minimization of the function funk(x) where x[1..ndim] is a vector in ndim
dimensions, by the downhill simplex method of Nelder and Mead. The matrix p[1..ndim+1]
[1..ndim] is input. Its ndim+1 rows are ndim-dimensional vectors which are the vertices of
the starting simplex. Also input is the vector y[1..ndim+1], whose components must be pre-
initialized to the values of funk evaluated at the ndim+1 vertices (rows) of p; and ftol the
fractional convergence tolerance to be achieved in the function value (n.b.!). On output, p and
y will have been reset to ndim+1 new points all within ftol of a minimum function value, and
nfunk gives the number of function evaluations taken.
@return the optimised vector
@param start the starting vector
@param step the starting step 
@param ndim the dimension of the vector
@param NMAX the max number of iterations
@param ftol the tolerance or precision needed on the function value to stop the iteration (convergence criterium)
@param subnet the eventual subnetwork to activate at each iteration
*/

public double[] simplex(double[] start,double[] step,int NMAX,double ftol)
{
double TINY=1e-10;
int ndim=step.length;
double sceau;
//int NMAX=5000;
int i,ihi,ilo,inhi,j,mpts=ndim+1;
double rtol,sum,swap,ysave,ytry;
//psum=vector(1,ndim);
double[] psum=new double[ndim+1];
int nfunk=0;

double[][] p=new double[ndim+2][ndim+1];
for (int row=1;row<=ndim+1;row++)
	for (int col=1;col<=ndim;col++)
		p[row][col]=start[col-1];
for (int col=1;col<=ndim;col++) p[col+1][col]+=step[col-1];

double[] y=new double[ndim+2];
for (int row=1;row<=ndim+1;row++) y[row]=fnr(p[row]);

//System.out.println("Simplexe: start"+printp(p));

//GET_PSUM
for (j=1;j<=ndim;j++)
	{
	for (sum=0.0,i=1;i<=mpts;i++) sum += p[i][j];
	psum[j]=sum;
	}
for (;;)
	{
	ilo=1;
/*First we must determine which point is the highest (worst), next-highest, and lowest
(best), by looping over the points in the simplex.*/
//	ihi = y[1]>y[2] ? (inhi=2,1) : (inhi=1,2);
	if (y[1]>y[2]) { inhi=2; ihi=1; }  else {inhi=1;ihi=2;}
	for (i=1;i<=mpts;i++)
		{
		if (y[i] <= y[ilo]) ilo=i;
		if (y[i] > y[ihi])
			{
			inhi=ihi;
			ihi=i;
			}
		else if (y[i] > y[inhi] && i != ihi) inhi=i;
		}
	rtol=2.0*Math.abs(y[ihi]-y[ilo])/(Math.abs(y[ihi])+Math.abs(y[ilo])+TINY);

//Compute the fractional range from highest to lowest and return if satisfactory.
	if (rtol < ftol)
		{  //  If returning, put best point and value in slot 1.
//		SWAP(y[1],y[ilo])
		sceau=y[1];y[1]=y[ilo];y[ilo]=sceau;
		for (i=1;i<=ndim;i++)
			//SWAP(p[1][i],p[ilo][i])
			{ sceau=p[1][i];p[1][i]=p[ilo][i];p[ilo][i]=sceau; }
		break;
		}
	
	//return if the number of iterations exceeds NMAX
	if (nfunk >= NMAX)
		{
		System.out.println(getClass()+" "+NMAX+" exceeded");
//		Messager.messErr(getName()+" "+getClass()+" "+NMAX+" exceeded");
		double[] point1=new double[p[1].length-1];
		for (int col=1;col<=point1.length;col++) point1[col-1]=p[1][col];
		return point1;
		}

	
	nfunk += 2;
//Begin a new iteration. First extrapolate by a factor -1 through the face of the simplex
//across from the high point, i.e., reflect the simplex from the high point.
	ytry=amotry(p,y,psum,ndim,ihi,-1.0);
	if (ytry <= y[ilo])
//Gives a result better than the best point, so try an additional extrapolation by a factor 2.
	ytry=amotry(p,y,psum,ndim,ihi,2.0);
	else if (ytry >= y[inhi])
		{
		//The reflected point is worse than the second-highest, so look for an intermediate
		//lower point, i.e., do a one-dimensional contraction.
		ysave=y[ihi];
		ytry=amotry(p,y,psum,ndim,ihi,0.5);
		if (ytry >= ysave)
			{   // Can't seem to get rid of that high point. Better contract around the lowest (best) point.
			for (i=1;i<=mpts;i++)
				{
				if (i != ilo)
					{
					for (j=1;j<=ndim;j++)
					p[i][j]=psum[j]=0.5*(p[i][j]+p[ilo][j]);
					y[i]=fnr(psum);
					}
				}
			nfunk += ndim; //   Keep track of function evaluations.
			//GET_PSUM   // Recompute psum.
			for (j=1;j<=ndim;j++)
				{
				for (sum=0.0,i=1;i<=mpts;i++) sum += p[i][j];
				psum[j]=sum;
				}
			}
		}
	else --(nfunk);  //  Correct the evaluation count.
	//System.out.println(nfunk+" "+printp1(p));
	}  //  Go back for the test of doneness and the next iteration.
//	free_vector(psum,1,ndim);
//System.out.println(getClass()+" simplexe number of fonction evaluations: "+nfunk);

double[] point1=new double[p[1].length-1];
for (int col=1;col<=p[1].length-1;col++) point1[col-1]=p[1][col];
//point1[p[1].length-1]=fnr(p[0]);
return point1;
}





/**Extrapolates by a factor fac through the face of the simplex across from the high point, tries
it, and replaces the high point if the new point is better.*/
double amotry(double[][] p, double y[], double psum[], int ndim, int ihi, double fac)
{
int j;
double fac1,fac2,ytry;
//ptry=vector(1,ndim);
double [] ptry=new double[ndim+1];
fac1=(1.0-fac)/ndim;
fac2=fac1-fac;
for (j=1;j<=ndim;j++) ptry[j]=psum[j]*fac1-p[ihi][j]*fac2;
ytry=fnr(ptry);   // Evaluate the function at the trial point.
if (ytry < y[ihi])
	{//    If it's better than the highest, then replace the highest.
	y[ihi]=ytry;
	for (j=1;j<=ndim;j++)
		{
		psum[j] += ptry[j]-p[ihi][j];
		p[ihi][j]= ptry[j];
		}
	}
//free_vector(ptry,1,ndim);
return ytry;
}

/** print the vector */
private String printp(double[][] p)
{
StringBuffer sb=new StringBuffer();
int ndim=p[0].length-1;
for (int row=1;row<=ndim+1;row++)
	{
	sb.append(row+" ");
	for (int col=1;col<=ndim;col++)
		{
		sb.append(p[row][col]+" ");
		}
	sb.append("z="+fnr(p[row]));
	sb.append("\n");
	}
/*double[] test=new double[2];
test[1]=
sb.append("z="+f(p[row]));
sb.append("\n");*/
return sb.toString();
}

private String printp1(double[][] p)
{
StringBuffer sb=new StringBuffer();
int ndim=p[0].length-1;
for (int col=1;col<=ndim;col++)
	{
	sb.append(p[1][col]+" ");
	sb.append("z="+fnr(p[1]));
	sb.append("\n");
	}
return sb.toString();
}

private double fnr(double[] point)
{
double[] point1=new double[point.length-1];
for (int col=1;col<=point1.length;col++) point1[col-1]=point[col];
return f(point1);
}

public abstract double f(double[] point);




}
