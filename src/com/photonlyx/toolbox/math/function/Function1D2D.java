
// Author Laurent Brunel

package com.photonlyx.toolbox.math.function;

import com.photonlyx.toolbox.math.signal.Signal1D2D;




/**function from R to R². Parametric curve (x,y)=f(t)*/
public abstract class Function1D2D   implements Function1D2DSource
{
//params
private int dim;
private double xmin,xmax;//boundaries of the sampling

public abstract double[] f(double t);


public Function1D2D getFunction1D2D() 
{
return this;
}


/**
DEPRECATED
find the minimal distance from a point to the curve. use dichotomie. TO BE TESTED!
@param p the point from which to calculate the distance
@param precision the precision needed in t (t is the curve abcsisse from 0 to 1)
*/
public double distance(double[] p,double precision,double tmin,double tmax)
{
int dim=10;//nb of tried points
double[][] pcurve=new double[dim][2];//array of points in the  curve 
double[] tcurve=new double[dim];//the curve abcsisse of each point
double[] dcurve=new double[dim];//the distance of each point
double[] tex=new double[2];//the extremes t
tex[0]=tmin;
tex[1]=tmax;

for(int j=0;j<100;j++)
	{
	calcpcurve(tex,pcurve,tcurve);
	lookForMinDistInpcurve(p,pcurve,tcurve,dcurve,tex);
	//System.out.println(" t1="+tex[0]+" t2="+tex[1]);
	if ((tex[1]-tex[0])<precision) break;
	}
//System.out.println("pcurve="+pcurve[0][0]+" "+pcurve[0][1]);
//System.out.println("distance0="+Math.sqrt(dbezier[0]));
return Math.sqrt(dcurve[0]);
}

private double dist2(double[] p1,double[] p2)
{
return Math.pow(p1[0]-p2[0],2)+Math.pow(p1[1]-p2[1],2);
}

//calc the x and y of the serie of points in the curve
private void calcpcurve(double[] tex,double[][] pcurve,double[] tcurve)
{
int dim=pcurve.length;
double s=(tex[1]-tex[0])/(dim-1);
for (int i=0;i<dim;i++) 
	{
	tcurve[i]=tex[0]+s*i;
	pcurve[i]=f(tcurve[i]);
	}
}
//look for the point with min distance and store the t1 and t2 around it in tex 
private void lookForMinDistInpcurve(double[] p,double[][] pcurve,double[] tcurve,double[] dcurve,double[] tex)
{
int dim=pcurve.length;
double d=1e30;
int index=0;
for (int i=0;i<dim;i++) 
	{
	dcurve[i]=dist2(pcurve[i],p);
	if (dcurve[i]<d) 
		{
		index=i;
		d=dcurve[i];
		}
	//System.out.println("i="+i+" d="+dcurve[i]+" x="+pcurve[i][0]);
	}
//System.out.println("index="+index);
if (index==0)
	{
	tex[0]=tcurve[0];
	tex[1]=tcurve[1];
	}
else if (index==(dim-1))
	{
	tex[0]=tcurve[dim-2];
	tex[1]=tcurve[dim-1];
	}
else
	{
	tex[0]=tcurve[index-1];
	tex[1]=tcurve[index+1];
	}
	
}





/**
get the distance from one point to the curve. Use the quadratic minimisation method for sub intervalls of [0;1]. An approximation of the absolute minimal distance is calculated first. The local minimim is calcultated iteratively with quadratic minimisation method.
See "Robust and Efficient Computation of the Closest Point on a Spline Curve", Hongling Wang, Joseph Kearney, and Kendall Atkinson.
@param P the point from which the distance must be calculated
@param nbIntervals the number of intervals to look for the absolute minimal distance.
@param topt array of one double with the curve abcisse of the closest point in the curve
@return the distance signed if the point is at one side or other of the curve
*/
public double distanceQuadraticMinimisation(double[] P,int  nbIntervals,double[] topt)
{
double t1,t2,d=0;
int dim=nbIntervals;//nb of intervals checked
double[] pnear=new double[2];//the closest point in the curve
double[] t=new double[1];//the curve abcisse of pnear

// System.out.println("look for closest interval for P=:"+P[0]+" "+P[1]);

//look for the closest interval
double dmin=1e30;
int index=0;
for (int i=0;i<dim;i++) 
	{
	t1=(double)i/(double)(dim);
	double di=dist2(P,f(t1));
	if (di<dmin) 
		{
		index=i;
		dmin=di;
		}
// 	System.out.println("i="+i+" d="+di+" s="+t1);
	}

//  System.out.println("closest interval:"+index);

if (index==0)
	{
	t1=0;
	t2=1./(double)(dim);
	}
else if (index==(dim-1))
	{
	t1=(dim-2)/(double)(dim);
	t2=(dim-1)/(double)(dim);
	}
else
	{
	t1=(index-1)/(double)(dim);
	t2=(index+1)/(double)(dim);
	}
	
d=distanceQuadraticMinimisationInterval(P,t1,t2,pnear,t);
topt[0]=t[0];

if (d==-1) 
	{
// 	System.out.println("extremity !");
	double d0=dist(P,f(0));
	double d1=dist(P,f(1));
	if (d0<d1) 
		{
		pnear=f(0);
		d=d0;
		topt[0]=0;
		} 
	else	{
		pnear=f(1);
		d=d1;
		topt[0]=1;
		} 
	}
	
double[] tangent=tangent(t[0],0.0001);
double[] pnearP=new double[2];
pnearP[0]=P[0]-pnear[0];
pnearP[1]=P[1]-pnear[1];

double pvectoriel=pnearP[0]*tangent[1]-pnearP[1]*tangent[0];
/*System.out.println("t="+t[0]);
System.out.println("tangx="+tangent[0]+" tangy="+tangent[1]);
System.out.println("pnearx="+pnear[0]+" pneary="+pnear[1]);
System.out.println("pvectoriel="+pvectoriel	);*/
if (pvectoriel<0) d=-d;
return d;
}





/**
use 2nd order polynomial aprox to find the minimum distance form a point to the curev. Iterative.
@param t1 lower boundary
@param t2 upper boundary
@param P the point from which the distance must be calculated
@param pnear point in the curve that is closest to P
@param t array of one double with the curve abcsisse of pnear
@return the distance, -1 if optimum is out of the segment [t1,t2]
*/
private double distanceQuadraticMinimisationInterval(double[] P,double t1,double t2,double[] pnear,double[] t)
{
double y12,y23,y31,s12,s23,s31;//coef for the quadratic approximation
double[] s=new double[4];//4 curve arc position
double[] d=new double[4];//the 4 corresponding distances to the point P
double r;
int index=0,j;
double previousd=1e30;
double convergenceCriterium=1e-10;

double[] s_sorted=new double[3];

s[0]=t1;
s[1]=(t1+t2)/2;
s[2]=t2;
  	
// System.out.println(getClass()+" "+"t1="+t1+" t2="+t2);

for (;;)
	{
//   	System.out.println("s1="+s[0]+" s2="+s[1]+" s3="+s[2]);
	y12=s[0]*s[0]-s[1]*s[1];
	y23=s[1]*s[1]-s[2]*s[2];
	y31=s[2]*s[2]-s[0]*s[0];
	s12=s[0]-s[1];
	s23=s[1]-s[2];
	s31=s[2]-s[0];
	d[0]=dist(f(s[0]),P);
	if (Double.isNaN(d[0]))
		{
		System.out.println("ADRIAN:d[0] = NaN!:");
		f(s[0]);
		}
	d[1]=dist(f(s[1]),P);
	d[2]=dist(f(s[2]),P);
	s[3]=0.5*(y23*d[0]+y31*d[1]+y12*d[2])/(s23*d[0]+s31*d[1]+s12*d[2]);
//   	System.out.println(f(s[0])[0]+" "+f(s[0])[1]+" "+f(s[1])[0]+" "+f(s[1])[1]+" "+f(s[2])[0]+" "+f(s[2])[1]);
// 	System.out.println(d[0]+" "+d[1]+" "+d[2]);
	
//   	System.out.println("s4="+s[3]);
	
	//if the new s is out of the interval return with error
	if ((s[3]<t1)||(s[3]>t2)) 
		{
// 		System.out.println("minimum not in this interval!");
		return -1;
		}
	

	d[3]=dist(f(s[3]),P);
	
// 	System.out.println("d4="+d[3]);
	
	//now select the point of the spline that has the bigger distance
	r=0;
	for (int i=0;i<4;i++) 
		if (d[i]>r) 
			{
			r=d[i];
			index=i;
			}
// 	System.out.println(index+" eliminated");
	
	//remove the point with the bigger distance
	j=0;
	for (int i=0;i<4;i++)
		{ 
		if (i!=index) 
			{
			s_sorted[j]=s[i];
			j++;
			}
		}
		
// 	System.out.println("avant sort s1="+s_sorted[0]+" s2="+s_sorted[1]+" s3="+s_sorted[2]);
	//sort the curve abcisse	
	int[] indexes=com.photonlyx.toolbox.math.Sorter.sort(s_sorted);
	//for (int i=0;i<3;i++) System.out.println(indexes[i]);
	for (int i=0;i<3;i++) s[i]=s_sorted[i];
// 	System.out.println("après sort s1="+s[0]+" s2="+s[1]+" s3="+s[2]);
		
	if (Math.abs(d[3]-previousd)<convergenceCriterium) break; 
	previousd=d[3];
	}
	
double[] p=f(s[1]);
pnear[0]=p[0];
pnear[1]=p[1];
t[0]=s[1];
// System.out.println("s optimal="+t[0]);
// System.out.println("x="+pnear[0]+" y="+pnear[1]);
return d[3];
}

private double dist(double[] p1,double[] p2)
{
return Math.sqrt(Math.pow(p1[0]-p2[0],2)+Math.pow(p1[1]-p2[1],2));
}


/**
gives the  tangent vector at the point of curve abcsisse t
*/
public double[] tangent(double t,double dt)
{
double[] P1,P2,D=new double[2];

P1=f(t);
P2=f(t+dt);
//af("P1[0]="+P1[0]+"  P1[1]="+P1[1]);
//af("P2[0]="+P2[0]+"  P2[1]="+P2[1]);
D[0]=(P2[0]-P1[0])/dt;
D[1]=(P2[1]-P1[1])/dt;
//af("D[0]="+D[0]+"  D[1]="+D[1]);
return D;
}


public Signal1D2D getSampling(double xmin,double xmax,int dim)
{
Signal1D2D signal=new Signal1D2D(xmin,xmax,dim);
signal.fillWithFunction1D2D(this);
return signal;
}




}
