package com.photonlyx.toolbox.math.function.usual1D2D;

import com.photonlyx.toolbox.math.function.Function1D2D;
import com.photonlyx.toolbox.math.geometry.Line2D;
import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;




public class ArcCircleCurve extends  Function1D2D
{
private double xcentre;
private double ycentre;
private double radius;
private double alpha1;
private double alpha2;

//internal


/**
 * get arc circle curve
 * @param _xcentre
 * @param _ycentre
 * @param _radius
 * @param _alpha1 start angle
 * @param _alpha2 end angle
 */
public ArcCircleCurve(double _xcentre,double _ycentre,double _radius,double _alpha1,double _alpha2)
{
this.xcentre=_xcentre;
this.ycentre=_ycentre;
this.radius=_radius;
this.alpha1=_alpha1;
this.alpha2=_alpha2;
}


/**
 * 
 * @param centre
 * @param _radius
 * @param _alpha1
 * @param _alpha2
 */
public ArcCircleCurve(Vecteur2D centre,double _radius,double _alpha1,double _alpha2)
{
this.xcentre=centre.x();
this.ycentre=centre.y();
this.radius=_radius;
this.alpha1=_alpha1;
this.alpha2=_alpha2;

}



/**
 * get arc circle curve going through  3 points
 * @param p1
 * @param p2
 * @param p3
 */
public  ArcCircleCurve (Vecteur2D p1,Vecteur2D p2,Vecteur2D p3)
{
Line2D l1=new Segment2D(p1,p2).getBisectorLine();
Line2D l2=new Segment2D(p2,p3).getBisectorLine();
Vecteur2D i=l1.getIntersectionPointWith(l2);
double _radius=p1.subn(i).norme();
double _xcentre=i.x();
double _ycentre=i.y();
double a1=Math.atan2(p1.y()-_ycentre, p1.x()-_xcentre);
double a2=Math.atan2(p2.y()-_ycentre, p2.x()-_xcentre);
double a3=Math.atan2(p3.y()-_ycentre, p3.x()-_xcentre);//check the middle point
boolean a1Ia2=a1<a2;
boolean a2Ia3=a2<a3;
if (!((a1Ia2&a2Ia3)||(!a1Ia2&!a2Ia3))) //a2 not between a1 and a3
	{
	if (a1<a3) a1+=2*Math.PI; else a3+=2*Math.PI;
	}
//if (a2<a1) a2+=2*Math.PI;
//if (a3<a1) a1+=2*Math.PI;
this.xcentre=_xcentre;
this.ycentre=_ycentre;
this.radius=_radius;
this.alpha1=a1;
this.alpha2=a2;

}



/**
 * variable from 0 to 1 to complete the circle
 */
public double[] f(double t)
{
double[] p=new double[2];
double angle=alpha1+t*(alpha2-alpha1);
p[0]=xcentre+radius*Math.cos(angle);
p[1]=ycentre+radius*Math.sin(angle);
return p;
}


}
