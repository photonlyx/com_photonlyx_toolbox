package com.photonlyx.toolbox.math.function.usual1D2D;

import com.photonlyx.toolbox.math.function.Function1D2D;
import com.photonlyx.toolbox.math.geometry.Line2D;
import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;






public class CircleCurve extends  Function1D2D
{
private double xcentre;
private double ycentre;
private double radius;

//internal



public CircleCurve(double _xcentre,double _ycentre,double _radius)
{
this.xcentre=_xcentre;
this.ycentre=_ycentre;
this.radius=_radius;

}

/**
 * get circle from 3 points
 * @param p1
 * @param p2
 * @param p3
 */
public  CircleCurve (Vecteur2D p1,Vecteur2D p2,Vecteur2D p3)
{
Line2D l1=new Segment2D(p1,p2).getBisectorLine();
Line2D l2=new Segment2D(p2,p3).getBisectorLine();
Vecteur2D i=l1.getIntersectionPointWith(l2);
double _radius=p1.subn(i).norme();
double _xcentre=i.x();
double _ycentre=i.y();
this.xcentre=_xcentre;
this.ycentre=_ycentre;
this.radius=_radius;
}


/**
 * variable from 0 to 1 to complete the circle
 */
public double[] f(double t)
{
double[] p=new double[2];
p[0]=xcentre+radius*Math.cos(t*2*Math.PI);
p[1]=ycentre+radius*Math.sin(t*2*Math.PI);
return p;
}


}
