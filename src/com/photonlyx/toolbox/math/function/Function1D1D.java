
// Author Laurent Brunel

package com.photonlyx.toolbox.math.function;

import java.util.Vector;


/**
function from R to R
*/
public abstract class Function1D1D  implements Function1D1DSource
{



public abstract double f(double x);


public Function1D1D getFunction1D1D() 
{
return this;
}

/**
 * trapeze integration
 * xmin : min boundary, xmax sup boundary,n sampling
 * */
public double integral(double xmin,double xmax,int n)
{
double r=0;
double interv=(xmax-xmin)/n;
for (int i=0;i<n;i++)
	{
	double x1=xmin+interv*i;
	double x2=x1+interv;
	r+=(f(x1)+f(x2));
	}
r=r/2*interv;
return r;
}

/**give the fourier coefficient for one frequency. 
xmin : min boundary, xmax sup boundary,n sampling,freq frequency*/
public double fourier(double xmin, double xmax,int n,double freq)
{
double r=0;
double interv=(xmax-xmin)/n;
for (int i=0;i<n;i++)
	{
	double x=xmin+interv*i;
	r+=f(x)*Math.cos(x*freq);
	}
return (r*interv);
}


/**calculate an aproximation of the derivative, the step must me small enough*/
public double derivative(double x,double step)
{
return (f(x+step)-f(x))/step;
}

/**
look for the minimum of the function between a and b with an algo using derivative
*/
public double minimiseWithDerivative(double a, double b,double stepForDerivative)
{
return MinimiseWithDerivative(a,b,stepForDerivative);
}

/**
look for the minimum of the function between a and b with an algo using derivative
@param a the left of the interval of seeking
@param b the right of the interval of seeking
@return the x value of the minimum
*/
public double MinimiseWithDerivative(double a, double b,double stepForDerivative)
{
double c,da,db,dc;
da=derivative(a,stepForDerivative);
db=derivative(b,stepForDerivative);
System.out.println("da="+da+" db="+db);
if ((da<0)&(db<0)) return b;
if ((da>0)&(db>0)) return a;
//if ((da>0)||(db<0))
//	{
//	Messager.messErr(getNode().getName()+" "+Messager.getString("can_t_complete_MinimiseWithDerivative"));
//	return ((a+b)/2);
//	}
c=(a+b)/2;

while ((b-a)>0.01)
	{
	dc=derivative(c,stepForDerivative);
	System.out.println("a="+a+" b="+b+" dc="+dc);
	if (dc<=0) a=c;
	if (dc>0) b=c;
	c=(a+b)/2;
	}
return c;
}

/**
look for the maximum of the function between a and b with an algo using derivative
@param a the left of the interval of seeking
@param b the right of the interval of seeking
@return the x value of the maximum
*/
public double maximiseWithDerivative(double a, double b,double stepForDerivative)
{
double c,da,db,dc;
da=derivative(a,stepForDerivative);
db=derivative(b,stepForDerivative);
//System.out.println("da="+da+" db="+db);
if ((da<0)&(db<0)) return a;
if ((da>0)&(db>0)) return b;
//if ((da<0)||(db>0))
//	{
//	Messager.messErr(getNode().getName()+" "+Messager.getString("can_t_complete_MaximiseWithDerivative"));
//	return ((a+b)/2);
//	}
c=(a+b)/2;

while ((b-a)>0.01)
	{
	dc=derivative(c,stepForDerivative);
	//System.out.println("a="+a+" b="+b+" dc="+dc);
	if (dc>=0) a=c;
	if (dc<0) b=c;
	c=(a+b)/2;
	}
return c;
}

/**
From Numerical recipies in C</p>
Using Ridders' method, return the root of a function func known to lie between x1 and x2.</p>
The root, returned as zriddr, will be refined to an approximate accuracy xacc.</p>
offset means that we sove f(x)=offset and not only f(x)=0 !</p>
*/
public double zriddr(double x1, double x2, double xacc,double offset)
{
int j;
int MAXIT=100;
double ans,fh,fl,fm,fnew,s,xh,xl,xm,xnew;
fl=f(x1)-offset;
fh=f(x2)-offset;
//System.out.println("zriddr: look for solution of height "+offset+" between "+x1+" and "+x2);
if ((fl > 0.0 && fh < 0.0) || (fl < 0.0 && fh > 0.0))
	{
	xl=x1;
	xh=x2;
	ans=1e30;    //Any highly unlikely value, to simplify logic
                        //below.
	for (j=1;j<=MAXIT;j++)
		{
		//System.out.println(ans);
		xm=0.5*(xl+xh);
		fm=f(xm)-offset;    //First of two function evaluations per it-
		s=Math.sqrt(fm*fm-fl*fh);    //eration.
		if (s == 0.0) return ans;
		xnew=xm+(xm-xl)*((fl >= fh ? 1.0 : -1.0)*fm/s);  //  Updating formula.
		if (Math.abs(xnew-ans) <= xacc) return ans;
		ans=xnew;
		fnew=f(ans)-offset;    //Second of two function evaluations per
		if (fnew == 0.0) return ans;   // iteration.
		if (SIGN(fm,fnew) != fm)
			{    //Bookkeeping to keep the root bracketed
			xl=xm;    //on next iteration.
			fl=fm;
			xh=ans;
			fh=fnew;
			}
		else if (SIGN(fl,fnew) != fl)
			{
			xh=ans;
			fh=fnew;
			}
		else if (SIGN(fh,fnew) != fh)
			{
			xl=ans;
			fl=fnew;
			}
		else { System.err.println("never get here.");}
		if (Math.abs(xh-xl) <= xacc) return ans;
		}
	System.err.println("zriddr exceed maximum iterations");
	}
else
	{
	if (fl == 0.0) return x1;
	if (fh == 0.0) return x2;
	if (fl > 0.0 && fh > 0.0) {if (fl<fh) return x1; else return x2;}
	if (fl < 0.0 && fh < 0.0) {if (fl<fh) return x2; else return x1;}
	//Messager.messErr("root must be bracketed in zriddr.");
	}
return 0.0;  //  Never get here.
}

private double SIGN(double a,double b)
{
if (b>=0) return Math.abs(a);  else return -Math.abs(a);
}

/**
look for the closest root with the Lagrange method
@param x: the starting abcisse of the iterative method
@param xmin: the min possible value for x
@param xmax: the max possible value for x
@param stepForDerivative: the step used to calculate the derivative function
@param nbSteps: max number of iterations
@return the abcisse of the root
*/
public double lagrange(double x,double xmin,double xmax,double stepForDerivative,int nbSteps)
{
return lagrange(x,0,xmin,xmax,stepForDerivative,nbSteps);
}

/**
look for the closest solution of y=f(x) with the Lagrange method
@param x: the starting abcisse of the iterative method
@param y: the y value of y=f(x)
@param xmin: the min possible value for x
@param xmax: the max possible value for x
@param stepForDerivative: the step used to calculate the derivative function
@param nbSteps: max number of iterations
@return the abcisse of the root
*/
public double lagrange(double x,double y,double xmin,double xmax,double stepForDerivative,int nbSteps)
{
//look for the local root with the Lagrange method
double x1=x,x0=x;
int i=0;
// System.out.println(getClass()+" ");
Function1D1D derivative=new Function1D1Dderivative(this,stepForDerivative);

for (;;)
	{
	x0=x1;
	if (x0<xmin) x0=xmin;
	if (x0>xmax) x0=xmax;
	double slope=derivative.f(x0);
	double yy=f(x0)-y;
	x1=x0-yy/slope;
//	System.out.println(getClass()+" "+i+" x0="+x0+" slope="+slope+" y="+y+" yy="+yy);
//	System.out.println(getClass()+" "+i+" x1="+x1);
//	System.out.println(getClass()+" "+" abs(x1-x0)="+Math.abs(x1-x0));
	i++;
	if (Math.abs(x1-x0)<stepForDerivative) break;
	if (i>nbSteps) break;
	}
//System.out.println("lagrange: "+i+" steps");
if (x1<xmin) x1=xmin;
if (x1>xmax) x1=xmax;
return x1;
}

/**
stupidly and robustly solve y=f(x).do lagrange root finding in each candidate interval.
Return an vector of Double of the solutions
*/
public Vector<Double> solveIntervalsLagrange(double xmin,double xmax,double y,int nbIntervals,double stepForDerivative)
{
Vector<Double> v=new Vector<Double>();
double x1,x2,y1,y2;
// System.out.println(getClass()+" xmin= "+xmin+" xmax= "+xmax+" y="+y);
double step=(xmax-xmin)/nbIntervals;
x2=xmin;
y2=f(x2);
for (int i=1;i<=nbIntervals;i++)
	{
	x1=x2;
	y1=y2;
	x2=x1+step;
	y2=f(x2);
//	System.out.println(getClass()+" x1= "+x1+" x2= "+x2);
//	System.out.println(getClass()+" y1= "+y1+" y2= "+y2);
	if (((y1<y)&(y2<y))||((y1>y)&(y2>y))) continue; 
	if (((y1<y)&(y<y2))||((y2<y)&(y<y1))) 
		{
//		System.out.println(getClass()+" x1= "+x1+" x2= "+x2);
//		System.out.println(getClass()+" y1= "+y1+" y2= "+y2);
//		System.out.println();
		v.add(new Double(lagrange(x1,y,x1,x2,stepForDerivative,3)));
		}
	else if (y1==y) v.add(new Double(x1));
	else if (y2==y) v.add(new Double(x2));
	}
return v;
}

















}
