
package com.photonlyx.toolbox.math.function;




/**
function that gives the tangent  of the son Function1D2D
*/
public class Function1D2DTangent  extends Function1D2D
{
//links
private Function1D2D function1D2D;
//params
private boolean normalised;
private double stepForDerivative;

/**construct */
public Function1D2DTangent()
{
}

/**
construct with the son
@param f the son Function1D2D
@param normalised if true the tangent is normalised
@param stepForDerivative the step used to calculate the derivative
*/
public Function1D2DTangent(Function1D2D f,boolean normalised,double stepForDerivative)
{
this.function1D2D=f;
this.normalised=normalised;
this.stepForDerivative=stepForDerivative;
}


public double[] f(double t)
{
// System.out.println(getClass()+" "+"t="+t);
double[] tang=function1D2D.tangent(t,stepForDerivative);
if (normalised) 
	{
	double norm=Math.sqrt(Math.pow(tang[0],2)+Math.pow(tang[1],2));
	tang[0]/=norm;
	tang[1]/=norm;
	}
return tang;
}





}
