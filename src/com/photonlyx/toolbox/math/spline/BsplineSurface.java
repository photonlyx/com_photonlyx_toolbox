package com.photonlyx.toolbox.math.spline;

import java.awt.Color;

import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class BsplineSurface 
{
	private Bspline bsu;
	private Bspline bsv;
	private Vecteur[][] net; //net of ctrl pts
	private double dx=1;
	private double dy=1;

	public BsplineSurface()
	{
		int m=10;
		int n=10;
		int p=2;
		int q=2;
		init(p,q,m,n,1,1);
	}

	public BsplineSurface(int p,int q,int m,int n,double dx,double dy)
	{
		init(p,q,m,n,dx,dy);
	}

	private void init(int p,int q,int m,int n,double dx,double dy)
	{
		bsu=new Bspline(p,m);
		bsv=new Bspline(q,n);
		net=new Vecteur[m+1][n+1];
		for (int i=0;i<=m;i++) 
			for (int j=0;j<=n;j++) 
			{

				double x=dx*i/(double)m;
				double y=dy*j/(double)n;
				//double z=Math.random();
				double z=0;

//				double x=(double)i/(double)m;
//				double y=(double)j/(double)m;
//				double z=Math.random()/4;

				net[i][j]=new Vecteur(x,y,z);		
			}


		update();
	}

	/**
	 * get the point of the Bspline surface for local coord u,v :
	 * @param u
	 * @param v
	 * @return
	 */
	public Vecteur p(double u,double v)
	{
		double x=0,y=0,z=0;
		double[] Nip=bsu.coef(u);
		double[] Njq=bsv.coef(v);
		for (int i=0;i<=bsu.getN();i++) 
			for (int j=0;j<=bsv.getN();j++) 
			{
				double nn=Nip[i]*Njq[j];
				x+=net[i][j].x()*nn;
				y+=net[i][j].y()*nn;
				z+=net[i][j].z()*nn;
			}
		return new Vecteur(x,y,z);
	}

	public void update()
	{
		bsu.update();
		bsv.update();
	}


	public Bspline getBsu() {
		return bsu;
	}
	public Bspline getBsv() {
		return bsv;
	}
	public Vecteur[][] getNet() {
		return net;
	}
	
	public int getN()
	{
		return bsu.getN();
	}
	public int getM()
	{
		return bsu.getN();
	}
	
	public static void main(String args[])
	{
		BsplineSurface bss=new BsplineSurface();

		//				//show basis function:
		//				Bspline bsu=new Bspline(2,10);
		//				Bspline bsv=new Bspline(2,10);
		//				ViewBasisFunction view= bss.new ViewBasisFunction(bsu,bsv);

		//show surface:
		ViewSurface view= bss.new ViewSurface(bss);



	}










	public class ViewSurface
	{
		private BsplineSurface bss;
		private int i=3;//index of the control point in u coord
		private int j=3;//index of the control point in v coord
		private	double tmin=0;
		private	double tmax=1;
		private	int mm=100;
		private	int nn=100;
		private	SignalDiscret1D1D s1=new SignalDiscret1D1D(tmin,tmax,nn);
		private Graph3DWindow g3d=new Graph3DWindow();
		private Point3DColor[][] p; //net of ctrl pts
		private Object3DSet ctrlPts=new Object3DSet();
		private Point3DColor[][] surface; //net of pts for the surface
		private Object3DSet surfacePts=new Object3DSet();


		public ViewSurface(BsplineSurface bss)
		{
			this.bss=bss;
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(bss.getBsu(),"update"));
				tl.add(new Trigger(this,"update"));
				tl.add(new Trigger(g3d.getGraph3DPanel(),"update"));
				String[] names={"p"};
				ParamsBox pb=new ParamsBox(bss.getBsu(),tl,null,names,null,null,ParamsBox.VERTICAL,180,28);
				g3d.getGraph3DPanel().getPanelSide().add(pb.getComponent());
			}
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(bss.getBsv(),"update"));
				tl.add(new Trigger(this,"update"));
				tl.add(new Trigger(g3d.getGraph3DPanel(),"update"));
				String[] names={"p"};
				String[] labels={"q"};
				ParamsBox pb=new ParamsBox(bss.getBsv(),tl,null,names,labels,null,ParamsBox.VERTICAL,180,28);
				g3d.getGraph3DPanel().getPanelSide().add(pb.getComponent());
			}
			g3d.getGraph3DPanel().getPanelSide().validate();
			//add the control points to the chart:
			int m=bss.getBsu().getN();
			int n=bss.getBsv().getN();
			p=new Point3DColor[m][n];
			for (int i=0;i<m;i++) 
				for (int j=0;j<n;j++) 
				{
					Point3DColor pc=new Point3DColor(bss.getNet()[i][j]);
					pc.setSizeOnScreen(5);
					pc.setColor(Color.RED);
					p[i][j]=pc;
					ctrlPts.add(pc);
				}
			//add the surface points to the chart:
			surface=new Point3DColor[mm][nn];
			for (int i=0;i<mm;i++) 
				for (int j=0;j<nn;j++) 
				{
					Point3DColor pc=new Point3DColor();
					pc.setSizeOnScreen(3);
					pc.setColor(Color.DARK_GRAY);
					surface[i][j]=pc;
					surfacePts.add(pc);
				}
			g3d.getGraph3DPanel().add(ctrlPts);
			g3d.getGraph3DPanel().add(surfacePts);
			g3d.getCJFrame().setExitAppOnExit(true);
			update();

		}


		public void update()
		{	
			if ((i<0)||(i>bss.getBsu().getN())) return;
			if ((j<0)||(j>bss.getBsv().getN())) return;	
			//update surface
			bss.update();
			double su=1/(double)(mm-1);
			double sv=1/(double)(nn-1);
			for (int k=0;k<mm;k++)
				for (int l=0;l<nn;l++)
				{
					double u=0+k*su;
					double v=0+l*sv;
					Vecteur p=bss.p(u, v);
					surface[k][l].affect(p);
					//System.out.println(surface[i][j]);
					//if (Nip[i]!=0) System.out.println("i="+i+" (u,v)=("+u+","+v+")"+" Nip="+Nip[i]);
					//System.out.println(surface[i][j]);
				}		
		}







	}












	public class ViewBasisFunction
	{
		private Bspline bsu;
		private Bspline bsv;
		private int i=3;//index of the control point in u coord
		private int j=3;//index of the control point in v coord
		private	double tmin=0;
		private	double tmax=1;
		private	int mm=100;
		private	int nn=100;
		private	SignalDiscret1D1D s1=new SignalDiscret1D1D(tmin,tmax,nn);
		private Graph3DWindow g3d=new Graph3DWindow();
		private Point3DColor[][] surface; //net of pts for the surface
		private Object3DSet surfacePts=new Object3DSet();


		public ViewBasisFunction(Bspline bsu,Bspline bsv)
		{
			this.bsu=bsu;
			this.bsv=bsv;
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(bsv,"update"));
				tl.add(new Trigger(g3d.getGraph3DPanel(),"update"));
				String[] names={"p"};
				ParamsBox pb=new ParamsBox(bsu,tl,null,names,null,null,ParamsBox.VERTICAL,180,28);
				g3d.getGraph3DPanel().getPanelSide().add(pb.getComponent());
			}
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(bsu,"update"));
				tl.add(new Trigger(g3d.getGraph3DPanel(),"update"));
				String[] names={"p"};
				String[] labels={"q"};
				ParamsBox pb=new ParamsBox(bsv,tl,null,names,labels,null,ParamsBox.VERTICAL,180,28);
				g3d.getGraph3DPanel().getPanelSide().add(pb.getComponent());
			}
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(this,"update"));
				tl.add(new Trigger(g3d.getGraph3DPanel(),"update"));
				String[] names={"i","j"};
				ParamsBox pb=new ParamsBox(this,tl,null,names,null,null,ParamsBox.VERTICAL,180,28);
				g3d.getGraph3DPanel().getPanelSide().add(pb.getComponent());
			}
			g3d.getGraph3DPanel().getPanelSide().validate();
			//add the surface points to the chart:
			surface=new Point3DColor[mm][nn];
			for (int i=0;i<mm;i++) 
				for (int j=0;j<nn;j++) 
				{
					Point3DColor pc=new Point3DColor();
					pc.setSizeOnScreen(3);
					pc.setColor(Color.DARK_GRAY);
					surface[i][j]=pc;
					surfacePts.add(pc);
				}
			g3d.getGraph3DPanel().add(surfacePts);
			g3d.getCJFrame().setExitAppOnExit(true);
			update();

		}


		public void update()
		{	
			if ((i<0)||(i>bsu.getN())) return;
			if ((j<0)||(j>bsv.getN())) return;	
			//update surface
			bsu.update();
			bsv.update();
			double su=1/(double)(mm-1);
			double sv=1/(double)(nn-1);
			for (int k=0;k<mm;k++)
				for (int l=0;l<nn;l++)
				{
					double u=0+k*su;
					double v=0+l*sv;
					double[] Nip=bsu.coef(u);
					double[] Njq=bsv.coef(v);
					surface[k][l].affect(u,v,Nip[i]*Njq[j]);
				}		
		}


		public int getI() {
			return i;
		}
		public void setI(int i) {
			this.i = i;
		}

		public int getJ() {
			return j;
		}

		public void setJ(int j) {
			this.j = j;
		}





	}

}
