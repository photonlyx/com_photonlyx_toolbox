// Author Laurent Brunel

package com.photonlyx.toolbox.math.spline;

import java.text.*;
import java.util.*;

import com.photonlyx.toolbox.gui.TaskButton;
import com.photonlyx.toolbox.math.analyse.LinearInterpolation2D;
import com.photonlyx.toolbox.math.matrix.*;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.thread.CTask;
import com.photonlyx.toolbox.triggering.TriggerList;


/**
Adjust a curve BSpline t->(x,y) (Function 1D2D) defined by control points to a cloud of points with the least squares method.
- normalise the data such that it becomes in a square with x form 0 to 1 and y from 0 to 1 .
- put the starting control points on a linear interpolation of the points from the first to the last data point.
- determine the curve abcisses on the curve of the closest point to the data points.
- make a least square adjustment to move the control points such that the curve's point having these abcisses gets closert to the data points.</p>
Inspired from "Fitting B-Spline Curves to Point Clouds by Squared Distance Minimization Wenping Wang1 , Helmut Pottmann2 , Yang Liu1"
*/
public  class ControlPoints1D2DFitLeastSquare2D  implements CTask
{
//links
private Signal1D1D ss;//the cloud of points
private BsplineCurve curve;//the BSPLINE function
private TriggerList everyPoint;//the subnetwork to make work at every point
private Signal1D1DXY Pk;//the closest points of the data points on the curve
//params 
private int nbIter=5;//nb of iteration for the least square
private int nbIntervals=5;//nb of itervals for the Pk finding
private int nbPtsInterpol=100;//nb of points for the linear interpolation
private boolean stopped;//to stop if stop button pressed
private boolean doFitting;//if false, don't do the fitting

/**
 * 
 * @param bspline the BSPLINE function
 * @param pointCloud the cloud of points
 */
public ControlPoints1D2DFitLeastSquare2D(BsplineCurve bspline,Signal1D1D pointsCloud)
{
this.curve=bspline;
this.ss=pointsCloud;
}

/**
 * 
 * @param bspline  the BSPLINE function
 * @param pointsCloud the cloud of points
 * @param Pk the closest points of the data points on the curve (debug)
 */
public ControlPoints1D2DFitLeastSquare2D(BsplineCurve bspline,Signal1D1D pointsCloud,Signal1D1DXY Pk)
{
this.curve=bspline;
this.ss=pointsCloud;
this.Pk=Pk;
}

public void update()
{
run();
}


//here the measure is the total distance from data points to their corresponding near point in the spline
public void run() 
{
curve.update();
Signal1D1DXY ctrl=curve.getControlPoints();

NumberFormat nf3 = NumberFormat.getInstance(Locale.ENGLISH);
nf3.setMaximumFractionDigits(3);
nf3.setMinimumFractionDigits(3);
nf3.setGroupingUsed(false);

Signal1D1D rawdata=ss.getSignal1D1D();
if (rawdata==null) return;
if (rawdata.getDim()==0) return;
// for (int i=0;i<rawdata.getDim();i++) System.out.println(rawdata.x(i)+" "+rawdata.y(i));

double[] X=new double[2];
double normeCorrection;
double[] tnear=new double[1];





// put the ctrl points on the linear interpolation
// for (int i=0;i<ctrl.getDim();i++)
// 	{
// 	double curveAbcisse=i/(double)(ctrl.getDim()-1);
// // 	System.out.println("interpol curve abcisse "+i+"= "+curveAbcisse);
// 	double[] pt=interpol.f(curveAbcisse);
// // 	System.out.println("interpol point "+i+"= "+pt[0]+" "+pt[1]);
// 	ctrl.setValue(i,pt[0],pt[1]);
// 	}




//fing the first and last point of the data cloud of points
double xstart=rawdata.x(0);
double ystart=rawdata.y(0);
double xend=rawdata.x(rawdata.getDim()-1);
double yend=rawdata.y(rawdata.getDim()-1);

//apply normalisation to x and y axis
double xmin=rawdata.xmin();
double xmax=rawdata.xmax();
double ymin=rawdata.ymin();
double ymax=rawdata.ymax();
/*System.out.println("xmin="+xmin+" xmax="+xmax);
System.out.println("ymin="+ymin+" ymax="+ymax);*/
double deltax=xmax-xmin;
double deltay=ymax-ymin;
Signal1D1DXY rawdataNormalised=rawdata.transfertTo1D1DXY();
if ((deltax!=0)&(deltay!=0))for (int i=0;i<rawdataNormalised.getDim();i++) 
	{
	//System.out.println(data.x(i)+" "+data.y(i));
	 rawdataNormalised.setValue(i,(rawdata.x(i)-xmin)/deltax,(rawdata.y(i)-ymin)/deltay);
	}
else if (deltax==0)for (int i=0;i<rawdataNormalised.getDim();i++) 
	{
	//System.out.println(data.x(i)+" "+data.y(i));
	rawdataNormalised.setValue(i,rawdata.x(i),(rawdata.y(i)-ymin)/deltay);
	}
else if (deltay==0)for (int i=0;i<rawdataNormalised.getDim();i++) 
	{
	//System.out.println(data.x(i)+" "+data.y(i));
	rawdataNormalised.setValue(i,(rawdata.x(i)-xmin)/deltax,rawdata.y(i));
	}
else if ((deltax==0)&(deltay==0))
	{
	System.out.println(getClass()+" raw data is only one point !");
	return;
	}
	
//use data resampled by linear interpolation
LinearInterpolation2D interpol=new LinearInterpolation2D(rawdataNormalised,"distance");
Signal1D1DXY dataNormalised=new Signal1D1DXY(nbPtsInterpol);
for (int i=0;i<dataNormalised.getDim();i++)
	{
	double curveAbcisse=i/(double)(dataNormalised.getDim()-1);
	double[] pt=interpol.f(curveAbcisse);
	dataNormalised.setValue(i,pt[0],pt[1]);
	}


//place the control points along a line from the first to the last point of the normalised cloud of points
double xstartNormalised=(xstart-xmin)/deltax;
double ystartNormalised=(ystart-ymin)/deltay;;
double xendNormalised=(xend-xmin)/deltax;
double yendNormalised=(yend-ymin)/deltay;;
double stepx=(xendNormalised-xstartNormalised)/(curve.getControlPoints().getDim()-1.0);
double stepy=(yendNormalised-ystartNormalised)/(curve.getControlPoints().getDim()-1.0);
for (int i=0;i<curve.getControlPoints().getDim();i++)
	{
	curve.getControlPoints().setValue(i,xstartNormalised+stepx*i,ystartNormalised+stepy*i);
	}




double[] t=new double[dataNormalised.getDim()];//the curve abcisses of the point in the spline closest to each data points


//calculate the curve abcisses of the closest point Pk in the curve
if (Pk!=null) Pk.init(3*dataNormalised.getDim());
for (int i=0;i<dataNormalised.getDim();i++)//list the data points
	{
	X[0]=dataNormalised.x(i);
	X[1]=dataNormalised.y(i);
	curve.distanceQuadraticMinimisation(X,nbIntervals,tnear);
	t[i]=tnear[0];
// 	System.out.println("tk"+i+"= "+t[i]);
	
	if (Pk!=null) 
		{
		double[] P=curve.f(t[i]);
		Pk.setValue(3*i,denormalise(xmin,deltax,P[0]),denormalise(ymin,deltay,P[1]));
		Pk.setValue(3*i+1,denormalise(xmin,deltax,X[0]),denormalise(ymin,deltay,X[1]));
		Pk.setValue(3*i+2,denormalise(xmin,deltax,P[0]),denormalise(ymin,deltay,P[1]));
		}
	
	}

	


//******************************************************************************
//do the least square adjustment	
//*******************************************************************************
int m=ctrl.getDim();//nb of control points
int n=dataNormalised.getDim();//nb of data points of the cloud used to adjust the spline
		
for (int k=0;k<nbIter;k++)
	{
	//fill the A matrix (matrix of the derivatives)
	
	
	/*
	If we note :
	Cj the jth control points (m control points j from 0 to m-1)
	Xi the ith data point (n data points i from 0 to n-1)
	Pi the closest point of Xi on the  curve
	the A matrix is a 2n*2m matrix
	(                                                      )
	(                ...                                   )
	(                                                      )
	(                                                      )
	(          dPi          dPi                            )
	(   ...   _____ x      _____ x       ...               )
	(          dCjx         dCjy                           )
	(                                                      )
	(          dPi          dPi                            )
	(   ...   _____ y      _____ y       ...               )
	(          dCjx         dCjy                           )
	(                                                      )
	(                 ...                                  )
	
	
	*/
	
	FullMatrix A=new FullMatrix(2*n,2*m);
	for (int i=0;i<n;i++)//list the data points
		{
		for (int j=0;j<m;j++)//list the control points
			{
			double[] pdx=partialDerivativex(j,t[i]);
			A.put(2*i,2*j,pdx[0]);
			A.put(2*i+1,2*j,pdx[1]);
			double[] pdy=partialDerivativey(j,t[i]);
			A.put(2*i,2*j+1,pdy[0]);
			A.put(2*i+1,2*j+1,pdy[1]);
			}
		}
		
/*	System.out.println("A");
	for (int i=0;i<2*n;i++)//list the data points
		{
		for (int j=0;j<2*m;j++)//list the control points
			{
			System.out.print(nf3.format(A.el(i,j))+"\t");
			}
		System.out.println();
		}
	System.out.println();*/
	
	
	

	//fill the F matrix. F is the vector (matrix 2n x 1) of the Xi-Pi
	/*
	
	
	   (                 )
	   (                 )
	   (                 )
	   (                 )	
	   (    ...          )
	F= (   (Xi)x-(Pi)x   )
	   (   (Xi)y-(Pi)y   )
	   (    ...          )
	   (                 )
	   (                 )
	   (                 )
	   
	*/
	
	FullMatrix F=new FullMatrix(2*n,1);
	for (int i=0;i<n;i++)//list the data points
		{
		double[] P=curve.f(t[i]);
		F.put(2*i,0,P[0]-dataNormalised.x(i));
		F.put(2*i+1,0,P[1]-dataNormalised.y(i));
		}
		
/*	System.out.println("F");
	for (int i=0;i<n;i++)//list the data points
		{
		System.out.println("F dataPt"+i+"x="+F.el(2*i,0));
		System.out.println("F dataPt"+i+"y="+F.el(2*i+1,0));
		}
	System.out.println();*/
	
	//calculate the normal matrix
	FullMatrix At=A.transpose();
	FullMatrix N=At.multiply(A);
	
	
/*	System.out.println("N");
	for (int j=0;j<2*m;j++)
		{
		for (int k=0;k<2*m;k++)
			{
			System.out.print(nf3.format(N.el(j,k))+"\t");
			}
		System.out.println();
		}
	System.out.println();*/
	
	
	FullMatrix AtF=At.multiply(F);
	
/*	for (int j=0;j<m;j++)
		{
		System.out.println("AtF ctrlPt"+j+"x="+AtF.el(2*j,0));
		System.out.println("AtF ctrlPt"+j+"y="+AtF.el(2*j+1,0));
		}
	System.out.println();*/
	
	//now solve the equation:  AtA dX = AtF
	int[] indx=new int[2*n];
	FullMatrix.decompose_lu(N,indx);//N is destroyed!
	FullMatrix.systeme_lu(N,indx,AtF);//now the solution is in AtF!
	
	//apply the correction:
	for (int j=1;j<m-1;j++)//list the control points
		{
		double x=ctrl.x(j);
		double y=ctrl.y(j);
		double dx=AtF.el(2*j,0);
		double dy=AtF.el(2*j+1,0);
// 		System.out.println("Ctrl point num "+j+" dx="+nf3.format(dx)+"\tdy="+nf3.format(dy));
		ctrl.setValue(j,x-dx,y-dy);
		}
	normeCorrection=AtF.norme();
	//if (normeCorrection<1e-3) break;
	
	//activated the subnetwork
	//if (everyPoint!=null) everyPoint.workAll();
	
	
	if (stopped)  break; 
	
	}
	
//apply the inverse of the normalisation of the data points to the control points.	
for (int i=0;i<ctrl.getDim();i++)
	{
	double xNormalised=ctrl.x(i);
	double yNormalised=ctrl.y(i);
	ctrl.setValue(i,xNormalised*deltax+xmin,yNormalised*deltay+ymin);
	}

curve.update();
	
if (everyPoint!=null) everyPoint.trig();
}
	
	
private double normalise( double min,double delta,double d ) { return ((d-min)/delta); } 
private double denormalise( double min,double delta,double d ) { return (d*delta+min); } 

/**
gives the partial derivative of the position of Pk to the point of curve abcisse tk of the spline, the variable is the x coord of the control point
@param ctrlIndex index of the control point
@param tk the curve abcisses corresponding to the data point Pk
@return the value of the derivative
*/
private double[] partialDerivativex(int ctrlIndex,double tk)
{
Signal1D1DXY ctrl=curve.getControlPoints();
double dx,absdiff;
double[] der1=new double[2];
double[] der2=new double[2];
double[] Pk0;
double[] Pk;

absdiff=1;
dx=0.001;

//get the initial position of the control point
double x0=ctrl.x(ctrlIndex);
double y0=ctrl.y(ctrlIndex);

//get the initial x coord of the Pk
Pk0=curve.f(tk);
// System.out.println("d0="+d0);

while ((absdiff>1e-5))
	{
	ctrl.setValue(ctrlIndex,x0+dx,y0);
	Pk=curve.f(tk);
	der1[0]=(Pk[0]-Pk0[0])/dx;
	der1[1]=(Pk[1]-Pk0[1])/dx;
	dx=dx/2;
	ctrl.setValue(ctrlIndex,x0+dx,y0);
	Pk=curve.f(tk);
	der2[0]=(Pk[0]-Pk0[0])/dx;
	der2[1]=(Pk[1]-Pk0[1])/dx;
//  	System.out.println(der2);
	absdiff=Math.abs(der2[0]-der1[0])+Math.abs(der2[1]-der1[1]);
	} 
// System.out.println();

//replace the control point at its initial position
ctrl.setValue(ctrlIndex,x0,y0);
return der2;
}



/**
gives the partial derivative of the position of Pk to the point of curve abcisse tk of the spline, the variable is the y coord of the control point
@param ctrlIndex index of the control point
@param tk the curve abcisses corresponding to the data point Pk
@return the value of the derivative
*/
private double[] partialDerivativey(int ctrlIndex,double tk)
{
Signal1D1DXY ctrl=curve.getControlPoints();
double dy,absdiff;
double[] der1=new double[2];
double[] der2=new double[2];
double[] Pk0;
double[] Pk;

absdiff=1;
dy=0.001;

//get the initial position of the control point
double x0=ctrl.x(ctrlIndex);
double y0=ctrl.y(ctrlIndex);

//get the initial x coord of the Pk
Pk0=curve.f(tk);
// System.out.println("d0="+d0);

while ((absdiff>1e-5))
	{
	ctrl.setValue(ctrlIndex,x0,y0+dy);
	Pk=curve.f(tk);
	der1[0]=(Pk[0]-Pk0[0])/dy;
	der1[1]=(Pk[1]-Pk0[1])/dy;
	dy=dy/2;
	ctrl.setValue(ctrlIndex,x0,y0+dy);
	Pk=curve.f(tk);
	der2[0]=(Pk[0]-Pk0[0])/dy;
	der2[1]=(Pk[1]-Pk0[1])/dy;
//  	System.out.println(der2);
	absdiff=Math.abs(der2[0]-der1[0])+Math.abs(der2[1]-der1[1]);
	} 
// System.out.println();

//replace the control point at its initial position
ctrl.setValue(ctrlIndex,x0,y0);
return der2;
}



@Override
public void setStopped(boolean b) {
	stopped=b;
	
}



@Override
public boolean isStopped() {
	return stopped;
}



@Override
public void setButton(TaskButton button) {
	// TODO Auto-generated method stub
	
}


















}