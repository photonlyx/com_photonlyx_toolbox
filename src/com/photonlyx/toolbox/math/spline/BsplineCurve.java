// Author Laurent Brunel

package com.photonlyx.toolbox.math.spline;

import java.awt.Color;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.math.signal.*;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;


/** 
Uniform B-spline  curve built with control points (son)
Eric W. Weisstein. "B-Spline." From MathWorld--A Wolfram Web Resource. http://mathworld.wolfram.com/B-Spline.html 
 */
public class BsplineCurve extends ControlPointsCurve2D 
{
	private Bspline bs;
	private Signal1D1DXY ctrlPts;//the control points

	public  BsplineCurve()
	{

	}

	/**
	 * @param nbCtrlPts the nb of control points
	 * @param p the degree of the spline
	 */
	public BsplineCurve(int n,int p)
	{
		ctrlPts=new Signal1D1DXY(n);
		bs=new Bspline(p,n);
	}


	public BsplineCurve(Signal1D1D _ctrlPts,int p)
	{
		ctrlPts=_ctrlPts.transfertTo1D1DXY();
		bs=new Bspline(p,ctrlPts.getDim()-1);
	}

	public void update()
	{
		bs.update();
	}



	public double[] f(double t)
	{
		double[] r=new double[2];
		if (t<0) 
		{
			// 	Messager.messErr(getClass()+" "+getNode().getName()+" t="+t+"<0!");
			System.out.println(getClass()+" "+" t="+t+"<0!");
			return r;
		}
		if (t>1) 
		{
			// 	Messager.messErr(getClass()+" "+getNode().getName()+" t="+t+">1!");
			System.out.println(getClass()+" "+" t="+t+">1!");
			return r;
		}
		//System.out.println(getClass()+" t="+t);
		//Signal1D1D ctrlPts=ctrlPtsS.getSignal1D1D();
		if (bs.getP()>=bs.getN()) return r;
		double[] coef=bs.coef(t);
		for (int i=0;i<=bs.getN();i++) 
		{
			//double N=N(i,(int)p.val,t);
			double N=coef[i];
			r[0]+=ctrlPts.x(i)*N;
			r[1]+=ctrlPts.y(i)*N;
			/*	System.out.println(getClass()+"f() t= "+t+" pts ctrl "+i+"  N="+N);
	System.out.println(getClass()+"f() pts ctrl x="+ctrlPts.x(i)+" pts ctrl y="+ctrlPts.y(i));*/
		}
		return r;
	}



	/**return the control points of the curve*/
	public Signal1D1DXY getControlPoints()
	{
		return ctrlPts;
	}


	public Bspline getBspline() {
		return bs;
	}

	public static void example()
	{
		Signal1D1DXY pts=new Signal1D1DXY();
		pts.addPoint(0,1);
		pts.addPoint(1,10);
		pts.addPoint(2,3);
		pts.addPoint(3,6);
		pts.addPoint(4,1);
		pts.addPoint(5,-1);
		pts.addPoint(6,20);
		BsplineCurve bs=new BsplineCurve(pts,3);
		Signal1D2D s1=new Signal1D2D(0,1,100);
		s1.fillWithFunction1D2D(bs);
		Plot p=new Plot(s1);
		ChartWindow cw=new ChartWindow(p);
		Plot pctrl=new Plot(pts);
		pctrl.setLineThickness(0);
		pctrl.setDotSize(10);
		pctrl.setColor(Color.BLUE);
		cw.getChart().add(pctrl);
		cw.update();

	}

	public static void main(String args[])
	{
		example();
	}



}


