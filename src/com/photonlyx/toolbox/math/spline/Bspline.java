package com.photonlyx.toolbox.math.spline;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class Bspline 
{
	private int p;//degree of the Bspline:p=m-n-1 with m: nb of knots and n nb of control points 
	private int n; //nb of control points -1
	//internal
	private int m; //nb of knots-1
	private double[] k;//array of knots



	public Bspline(int p,int n)
	{
		this.p=p;
		this.setN(n);
		update();
	}

	/**
update the knots
	 */
	public void update()
	{
		m=n+p+1;
		k=new double[m+1];
		//  System.out.println(getClass()+" degree="+(int)p.val);
		//  System.out.println(getClass()+" "+(m+1)+" knots");
		//  System.out.println(getClass()+" "+(n+1)+" control points");

		//the first knot and the last knot must be of multiplicity p+1. This will generate the so-called clamped B-spline curves
		int pp=p;
		if (pp>=getN()) 
		{
			System.out.println(getClass()+" Degree of Bspline too high or to few control points");
			return;
		}
		int ni=(m+1)-2*(pp);//nb of internal knots
		for (int i=0;i<pp;i++) k[i]=0;//clamped
		for (int i=0;i<ni;i++) k[i+pp]=(double)i/((double)(ni-1));;
		for (int i=m;i>m-pp;i--) k[i]=1;//clamped


		//for (int i=0;i<=m;i++) k[i]=(double)i/(double)(m);;

		//for (int i=0;i<=m;i++) System.out.println("k "+i+"="+k[i]);
		// for (int i=0;i<=n;i++)	System.out.println(getClass()+" pts ctrl x="+ctrlPts.x(i)+" pts ctrl y="+ctrlPts.y(i));

	}


	/**Cox deBoor recursion. Not used since it is time consuming*/
	private double N(int i,int p,double t)
	{
		double r=0;
		if (p==0) 
		{
			if ((t>=k[i])&&(t<k[i+1])&&(k[i]<k[i+1])) r=1.;
			else r= 0.;
		}
		else //r= ((t-k[i])/(k[i+p]-k[i])*N(i,p-1,t) + (k[i+p+1]-t)/(k[i+p+1]-k[i+1])*N(i+1,p-1,t));
		{
			double n1=N(i,p-1,t);
			double n2=N(i+1,p-1,t);
			if (n1!=0) r=(t-k[i])/(k[i+p]-k[i])*n1;
			if (n2!=0) r+=(k[i+p+1]-t)/(k[i+p+1]-k[i+1])*n2;
		}
		//System.out.println("N"+i+" "+p+"("+t+")="+r);
		return r;
	}


	/**
	 * Dr. Ching-Kuang Shene 
Associate Professor 
 Department of Computer Science 
 Michigan Technological University 
http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/notes.html
@param p 
@param t
@return the array of the coefficient N from i=0 to n for the degree p and parameter t (between 0 and 1)
	 */

	public double[] coef(double t)
	{
		double[] N=new double[n+1];
		//case when t is 0 or 1
		if (t==0) 
		{
			N[0]=1;
			return N;
		}
		if (t==1) 
		{
			N[getN()]=1;
			return N;
		}
		//look for the knot span where t is
		int kk=0;
		//System.out.println("ADRIAN:t:" + t);
		for (int i=0;i<=m-1;i++) 
		{
			//System.out.println("ADRIAN:k[" + i + "]:" + k[i] + ",k[" + (i+1) + "]:" + k[i+1]);
			if ((t>=k[i])&&(t<k[i+1])) 
			{
				kk=i;
				break;
			}
		}
		if (kk==0)
		{
			System.out.println("kk=0! n="+n+" m="+m+" t="+t);
		}
		else
		{
			N[kk]=1;// degree 0 coefficient 
			for (int d=1;d<=p;d++)
			{
				// right (south-west corner) term only 
				N[kk-d] = (k[kk+1]-t) / (k[kk+1]-k[kk-d+1])	* N[(kk-d)+1]; 
				// compute internal terms
				for (int i=kk-d+1;i<=kk-1;i++)
				{
					N[i]= (t-k[i])/(k[i+d]-k[i])*N[i] + (k[i+d+1]-t)/(k[i+d+1]-k[i+1])*N[i+1];
				}
				// left (north-west corner) term only 
				N[kk]=(t-k[kk])/(k[kk+d]-k[kk])*N[kk];
			}
		}
		return N;
	}



	public int getP() {
		return p;
	}

	public void setP(int p) {
		this.p = p;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}



	public class ViewBasisFunction
	{
		private Bspline bs;
		private int i=4;
		private int nt=100;
		private SignalDiscret1D1D sig=new SignalDiscret1D1D(0,1,nt);
		private double st=1/(double)(nt-1);		
		private	ChartWindow cw=new ChartWindow(sig);
		
		public  ViewBasisFunction(Bspline bs)
		{
			this.bs=bs;
			cw.getCJFrame().setExitAppOnExit(true);
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(bs,"update"));
				tl.add(new Trigger(this,"update"));
				tl.add(new Trigger(cw,"update"));
				String[] names={"p","n"};
				ParamsBox pb=new ParamsBox(bs,tl,null,names,null,null,ParamsBox.VERTICAL,180,28);
				cw.getChartPanel().getPanelSide().add(pb.getComponent());
			}
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(this,"update"));
				tl.add(new Trigger(cw,"update"));
				String[] names={"i"};
				ParamsBox pb=new ParamsBox(this,tl,null,names,null,null,ParamsBox.VERTICAL,180,28);
				cw.getChartPanel().getPanelSide().add(pb.getComponent());
			}
			cw.getChartPanel().getPanelSide().validate();
			update();
		}

		//show basic function for control point i
		public void update()
		{
			if ((i<0)|(i>n)) return;
			for (int k=0;k<nt;k++)
			{
				double t=0+k*st;
				double[] N=bs.coef(t);
				//for (int ii=0;ii<n;ii++) System.out.print(N[i]+" "); System.out.println();
				sig.setValue(k,N[i]);
			}
		}

		public int getI() {
			return i;
		}
		public void setI(int i) {
			this.i = i;
		}


	}



	public static void main(String args[])
	{
		int n=10;//nb of ctrl pts
		int p=2;//degree of spline
		Bspline bs=new Bspline(p,n);
		ViewBasisFunction s=bs.new ViewBasisFunction(bs);
	}




}
