
// Author Laurent Brunel

package com.photonlyx.toolbox.math.spline;

import com.photonlyx.toolbox.math.function.Function1D2D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;




/** curve which shape is controled by points (x,y) in the R2 space (like Bezier, spline, ...)*/
public abstract class ControlPointsCurve2D extends Function1D2D	
{





/**return the control points of the  curve*/
public abstract Signal1D1DXY getControlPoints();



}