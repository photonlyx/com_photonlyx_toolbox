
// Author Laurent Brunel
package com.photonlyx.toolbox.math;

import java.util.Vector;

/** A simple QuickSort for String arrays. from NIH Image*/
public class Sorter {
	
/** 
Sort the array with increasing values. 
@param a array of double to sort
@return the indices in the order of the sorted array a
*/
public static int[] sort(double[] a)
{
int[] indices=new int[a.length];
for (int i=0;i<indices.length;i++) indices[i]=i;
if (!alreadySorted(a)) sort(indices,a, 0, a.length - 1);
return indices;
}

/**
 * sort the values in "a" from indices "from" to "to". get the new indices in "indices"
 * @param indices
 * @param a
 * @param from
 * @param to
 */
private static void sort(int[] indices,double[] a, int from, int to)
	{
	int i = from, j = to;
	double center = a[ (from + to) / 2 ];
	do 
		{
		while ( i < to && ((center-a[i]) > 0) ) i++;
		while ( j > from && ((center-a[j]) < 0) ) j--;
		if (i < j) 
			{
			//swap the indices
			int temp = indices[i]; indices[i] = indices[j]; indices[j] = temp;
			//swap the doubles
			double tempd = a[i]; a[i] = a[j]; a[j] = tempd;
			}
		if (i <= j) { i++; j--; }
		} 
	while(i <= j);

	if (from < j) sort(indices,a, from, j);
	if (i < to) sort(indices,a,  i, to);
}


static boolean alreadySorted(double[] a) 
{
for ( int i=1; i<a.length; i++ ) 
	{
	if ((a[i]-a[i-1]) < 0 )	return false;
	}
return true;
}

public static void sort(Vector<Double> v)
{
double[] a=new double[v.size()];
for (int i=0;i<a.length;i++) a[i]=v.elementAt(i);
sort(a);
for (int i=0;i<a.length;i++) v.setElementAt(a[i],i);
}

/** for test ! */
public static void main(String args[])
{
double[] a={1.2,3.4,5.7,-2,-6};
System.out.println("sort ");
for (int i=0;i<a.length;i++) System.out.print(a[i]+" ");
System.out.println();
int[] indices=sort(a);
System.out.println("indices ");
for (int i=0;i<indices.length;i++) System.out.print(indices[i]+" ");
System.out.println();
for (int i=0;i<a.length;i++) System.out.print(a[i]+" ");
System.out.println();

}

}
