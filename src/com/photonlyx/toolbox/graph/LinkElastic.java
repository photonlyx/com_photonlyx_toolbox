package com.photonlyx.toolbox.graph;

public class LinkElastic
{
private Node n1,n2;
private double ka=1;//spring module

public LinkElastic(Node n1,Node n2)
{
this.n1=n1;
this.n2=n2;
}

public void getForce(double[] force)
{
double d2=(Math.pow(n1.getXreal()-n2.getXreal(),2)+Math.pow(n1.getYreal()-n1.getYreal(),2));
double forceModule=ka*Math.sqrt(d2);
force[0]=forceModule*(n2.getXreal()-n1.getXreal());
force[1]=forceModule*(n2.getYreal()-n1.getYreal());
}


}
