package com.photonlyx.toolbox.graph.gui;


import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import com.photonlyx.toolbox.graph.Network;
import com.photonlyx.toolbox.graph.Node;
import com.photonlyx.toolbox.graph.Transformation;
import com.photonlyx.toolbox.gui.ColorUtil;
import com.photonlyx.toolbox.gui.TextEditor;
import com.photonlyx.toolbox.mysql.MysqlRow;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.util.Global;

import java.util.*;
import java.text.*;


public class NetworkPanel   implements MouseListener,MouseMotionListener,MouseWheelListener
,KeyListener,ActionListener
{
	//links
	private Network network;
	protected String comment;
	protected String linkedFileName;
	protected String category;


	//internal
	private int[] pix1=new int[2];
	private int[] pix2=new int[2];
	private boolean shiftPressed=false;
	private boolean ctrlPressed=false;
	private boolean altPressed=false;
	private boolean xPressed=false;
	private boolean showMenu=false;

	protected Transformation tr;

	private Node[] selectedNodes;
	private Node clickedNode;
	private int compteur=0;
	private DateFormat df=DateFormat.getDateInstance(DateFormat.MEDIUM);
	private Calendar cal=GregorianCalendar.getInstance();
	private boolean changingTaskLength;
	private Color backgroundColor=new Color(160,160,160);
	private NumberFormat nf1;
	private boolean selecting=false;

	private JPopupMenu popmenuOnBack,popmenuOnNode;
	protected Point mouse;
	private LabelEditJTextArea labelEditJTextArea;

	private JPanel mainPanel=new JPanel();
	private DecimalFormat df3=new DecimalFormat("0.000");
	private static String ROOT_NODE_NAME="node";

	protected JPanel nodesPanel=new JPanel()
	{
		public void paintComponent(Graphics g)
		{
			JPanel jp=this;
			super.paintComponent(g);

			//paint the tasks and links of this user
			paintNodes(g);


			if (changingTaskLength)
			{
				g.setColor(Color.black);
				String len="";
				len+="Start:"+selectedNodes[0].hour()+"h"+selectedNodes[0].minute();
				len+=" length: "+nf1.format(selectedNodes[0].getRadius()/1000.0/3600.0)+" h";
				len+=" up to now:"+nf1.format(selectedNodes[0].getAllLinkedTaskTotalTimeLength()/1000.0/3600.0)+" h";
				g.drawString(len,pix1[0]+10,pix1[1]-10);
				//project name
				g.drawString(selectedNodes[0].getProjectName(),pix1[0]+10,pix1[1]-20);
			}

			if (selecting)
			{
				g.setColor(Color.black);
				g.drawRect(pix1[0], pix1[1], pix2[0]-pix1[0], pix2[1]-pix1[1]);
			}

			if (showMenu)
			{
				int x=10,y=10,dec=15;
				String s;
				g.setColor(Color.white);
				s="Menu:\n";g.drawString(s,x,y+=10);
				s="m: show/hide menu\n";g.drawString(s,x,y+=dec);
				s="f: apply forces\n";g.drawString(s,x,y+=dec);
				s="x: erase selected\n";g.drawString(s,x,y+=dec);
				s="l: link/unlink\n";g.drawString(s,x,y+=dec);
				s="z: unzoom\n";g.drawString(s,x,y+=dec);
				s="Z: zoom\n";g.drawString(s,x,y+=dec);
				s="d: nodes smaller\n";g.drawString(s,x,y+=dec);
				s="D: nodes bigger\n";g.drawString(s,x,y+=dec);
				s="h: change color hue\n";g.drawString(s,x,y+=dec);
				s="s: change color saturation\n";g.drawString(s,x,y+=dec);
				s="b: change color brightness\n";g.drawString(s,x,y+=dec);
				s="Left button drag: move\n";g.drawString(s,x,y+=dec);
				s="Right button drag: box selection\n";g.drawString(s,x,y+=dec);
			}
			else 
			{
				int x=10,y=10;
				String s;
				g.setColor(Color.white);
				s="m: show/hide menu\n";g.drawString(s,x,y+=10);
			}


		}
	};




	public  NetworkPanel( Network network,boolean putAnalysePanels)
	{
		this.network=network;

		if (putAnalysePanels)
		{
			mainPanel.setLayout(new BorderLayout());
			JTabbedPane tabPane=new JTabbedPane();
			mainPanel.add(tabPane,BorderLayout.CENTER);


			tabPane.add(nodesPanel,"Nodes");
		}
		else
		{
			mainPanel.setLayout(new BorderLayout());
			mainPanel.add(nodesPanel,BorderLayout.CENTER);
		}	


		tr=new Transformation(nodesPanel,network);
		JPanel jp=nodesPanel;
		jp.addMouseListener(this);
		jp.addMouseMotionListener(this);
		jp.addMouseWheelListener(this);
		jp.addKeyListener(this);

		nf1 = NumberFormat.getInstance(Locale.ENGLISH);
		nf1.setMaximumFractionDigits(1);//important: the index must have no figure after coma
		nf1.setMinimumFractionDigits(1);
		nf1.setGroupingUsed(false);


		popmenuOnBack=new JPopupMenu("Action");
		Vector<JMenuItem> popUpMenuItems1=new Vector<JMenuItem>();
		JMenuItem jmi;
		popUpMenuItems1.add(jmi=new JMenuItem("New node"));
		//popUpMenuItems1.add(jmi=new JMenuItem("New note"));
		//jmi.setIcon(com.cionin.util.Util.getIcon("com/cionin/chart/iconExcel.png"));
		//popUpMenuItems1.add(jmi=new JMenuItem("Delete"));
		for (JMenuItem mi:popUpMenuItems1)
		{
			popmenuOnBack.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(Global.background);
			mi.setFont(Global.font);
		}

		popmenuOnNode=new JPopupMenu("Node");

		nodesPanel.setBackground(backgroundColor);
		nodesPanel.setLayout(null);

	}

	protected void setBackground(Color c)
	{
		nodesPanel.setBackground(c);
	}



	//paint the tasks and links of this user

	protected void paintNodes(Graphics g)
	{
		//Links
		for (int i=0;i<network.size();i++)
		{
			Node n=(Node)network.getRow(i);
			//System.out.println(getClass()+" draw links of "+n.getName());
			g.setColor(new Color(130,130,130));
			Vector<Node> sons=n.getSonsVector();
			for (Node son:sons)
			{
				//System.out.println(getClass()+" draw link to son "+son.getName()+"  "+n.getXscreen(tr)+" "+n.getYscreen(tr)+" "+
				//						son.getXscreen(tr)+" "+son.getYscreen(tr));
				g.drawLine(n.getXscreen(tr),n.getYscreen(tr),son.getXscreen(tr),son.getYscreen(tr));
			}
		}

		for (int i=0;i<network.size();i++)
		{
			Node t=(Node)network.getRow(i);
			t.paint(g,tr);
		}
	}





	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();
		JMenuItem mi;
		String label;
		if (source instanceof JMenuItem)
		{
			mi=(JMenuItem)source;
			label=mi.getText();
			if (label.compareTo("New node")==0)//create a new node
			{
				Node n=new Node(network,this.findNewName());
				network.addRowAndCreateNewDBnumber(n);
				n.setXreal(tr.toRealx(mouse.x));
				n.setYreal(tr.toRealy(mouse.y));
				tr.updateRealCoord(n);
				nodesPanel.repaint();
			}

//			if (isACategory(label)) 
//			{
//				clickedNode.setCategory(label);
//			}
		}


		nodesPanel.repaint();
	}

	private void updateOnNodePopUpMenu()
	{
		popmenuOnNode.removeAll();
		JMenuItem jmi2;
		Vector<JMenuItem> popUpMenuItems2=new Vector<JMenuItem>();
		//categories
		Definition def=new Definition(network.getCategories());
		def._sortInAlphabeticOrder();
		for (int i=0;i<def.dim();i++)
		{
			popUpMenuItems2.add(jmi2=new JMenuItem(def.word(i)));
		}
		popmenuOnNode.setLayout(new GridLayout(def.dim()/3, 3));
		for (JMenuItem mi:popUpMenuItems2)
		{
			popmenuOnNode.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(Global.background);
			mi.setFont(Global.font);
		}
	}




	private boolean isACategory(String s)
	{
		Definition def=new Definition(network.getCategories());
		for (int i=0;i<def.dim();i++)
		{
			if (def.word(i).compareTo(s)==0) return true;
		}
		return false;
	}


	public void mouseClicked(MouseEvent e)
	{
		mouse=e.getPoint();
		clickedNode=getNodeOnMouse(e.getX(),e.getY());
		nodesPanel.requestFocus();
		if (e.getButton()==MouseEvent.BUTTON3) 	
		{
			if (clickedNode!=null)
			{		
				updateOnNodePopUpMenu();
				popmenuOnNode.show(nodesPanel,e.getPoint().x,e.getPoint().y);
			}
			else popmenuOnBack.show(nodesPanel,e.getPoint().x,e.getPoint().y);
		}
		if (e.getButton()==MouseEvent.BUTTON1) 
			if (clickedNode==null) this.unselectAll();

		if ((e.getButton()==MouseEvent.BUTTON1)&&(e.getClickCount()==2)&&(clickedNode!=null))
		{
			editClickedNode();
		}
		else 
		{
			quitEditClickedNode();
		}
	}



	protected void  quitEditClickedNode()
	{
		if (labelEditJTextArea!=null)
		{
			nodesPanel.remove(labelEditJTextArea);
			labelEditJTextArea=null;
			nodesPanel.validate();
		}
	}

	protected void  editClickedNode()
	{
		//edit node label:
		labelEditJTextArea= new LabelEditJTextArea(clickedNode);
		labelEditJTextArea.setBackground(Global.background);
		nodesPanel.add(labelEditJTextArea);
		//labelEditJTextArea.setLocation(e.getPoint().x,e.getPoint().y);
		labelEditJTextArea.setLocation(clickedNode.getXscreen(tr),clickedNode.getYscreen(tr));
		nodesPanel.validate();
		//System.out.println("edit");

	}


	public void mouseEntered(MouseEvent e)
	{
		if (labelEditJTextArea==null)nodesPanel.requestFocus();
	}

	public void mouseExited(MouseEvent e){}

	public void mousePressed(MouseEvent e)
	{
		// System.out.println(getClass()+" mousePressed");
		pix1[0]=e.getX();
		pix1[1]=e.getY();
		clickedNode=getNodeOnMouse(e.getX(),e.getY());
		changingTaskLength=false;

		if (e.getButton()==MouseEvent.BUTTON3) 
		{
			selecting=true;
			//System.out.println("selecting");
			return;
		}
		else selecting=false;

		if (clickedNode==null)
		{
			//	double d=tr.getDate(e.getX(),e.getY());
			//	cal.setTime(new Date((long)d)) ;
			//	label="";
			//	status="";
			//	day=cal.get(Calendar.DAY_OF_MONTH);
			//	month=cal.get(Calendar.MONTH)+1;
			//	year=cal.get(Calendar.YEAR);
			//	comment="";
			//	linkedFileName="";
			//	category="";
			//	if (e.getButton()==MouseEvent.BUTTON3)
			//		if (shiftPressed) 
			//			daysColors.add(day,month,year,color);
			//	unselectAll();
			//	launcher.trig();
		}
		else 
		{
			/*label=clickedTask.label();
	status=clickedTask.status();

	day=clickedTask.day();
	month=clickedTask.month();
	year=clickedTask.year();*/

			comment=clickedNode.getComment();
			//linkedFileName=clickedTask.linkedFileName;
			//category=clickedNode.getCategory();

			//if (!ctrlPressed) unselectAll();
			clickedNode.select(true);
			if (shiftPressed) 
			{
				unselectAll();
				Network _network=clickedNode.getNetwork();
				Node son=new Node(network,findNewName());
				_network.addRow(son);
				son.setXreal(clickedNode.getXreal());
				son.setYreal(clickedNode.getYreal());
				//clickedTask.addSon(son);
				son.setLabel(clickedNode.getLabel());
				son.setRadius(clickedNode.getRadius());
				//son.setCategory(clickedNode.getCategory());
				son.setNetwork(_network);
				son.setStatus(clickedNode.getStatus());
				son.select(true);
			}
		}
		selectedNodes=getSelectedNodes();
		nodesPanel.repaint();
	}




	public void mouseReleased(MouseEvent e)
	{
		//update mysql db:
		this.network.getDb().setEnableSQL(true); 
		for (Node t:this.getSelectedNodes()) network.updateSQL(t);

		changingTaskLength=false;
		//System.out.println(getClass()+" mouseReleased");
		//if (e.getButton()==MouseEvent.BUTTON3) return;
		//select the boxes inside the rectangle
		if (selecting)
		{
			unselectAll();
			for (int i=0;i<network.size();i++)
			{
				Node t=(Node)network.getRow(i);
				if (t.isInsideRectangle(tr,pix1[0], pix1[1], pix2[0], pix2[1]))
				{
					t.select(true);
				}
			}
			selecting=false;
			nodesPanel.repaint();
		}
		//else if (!ctrlPressed) unselectAll();
		selectedNodes=getSelectedNodes();
		if (shiftPressed) 
		{
			clickedNode=null;
			nodesPanel.repaint();
		}
	}



	public void mouseDragged(MouseEvent e)
	{
		// System.out.println(getClass()+" mouseDragged");
		if (selectedNodes==null) return;
		pix2[0]=e.getX();
		pix2[1]=e.getY();

		int dx=e.getX()-pix1[0];
		int dy=e.getY()-pix1[1];

		if (selecting)
		{
			nodesPanel.repaint();
			return;
		}

		if (clickedNode!=null) 
		{
			//block mysql db update:	
			this.network.getDb().setEnableSQL(false);
			if (selectedNodes.length==1)
			{
				Node t=selectedNodes[0];
				if (changingTaskLength) 
				{
					tr.changeTaskLength(t,dx,dy);
				}
				else tr.translateTask(t,dx,dy);
			}
			// 	if (!altPressed) dy=0;
			else for (int i=0;i<selectedNodes.length;i++) 
			{
				Node t=selectedNodes[i];
				tr.translateTask(t,dx,dy);
				/*		t.translate(dx,dy);
		tr.updateRealCoord(t);*/
			}
		}
		else 
		{
			tr.translate(dx,dy);
		}
		pix1[0]=e.getX();
		pix1[1]=e.getY();
		nodesPanel.repaint();


	}

	public void mouseMoved(MouseEvent e)
	{
		Point p=e.getPoint();
		Node no=getNodeOnMouse(e.getX(),e.getY());
		String s="no node";
		String sdp="0 0";
		if (no!=null)
		{
			s=no.getLabel();
			double dt=0.1;
			double[] dp;
			dp=no.calcDisplacement(dt);
			sdp=df3.format(dp[0])+" "+df3.format(dp[1]);
		}
		Global.setMessage(p.x+" "+p.y+" real: "+df3.format(tr.toRealx(p.x))+" "+df3.format(tr.toRealy(p.y))+" node on mouse:"+s
				+"dep:"+sdp);

	}



	public void mouseWheelMoved(MouseWheelEvent e)
	{
		int nbClicks=e.getWheelRotation();
		if (nbClicks<0) 
			for (int i=0;i<-nbClicks;i++) 
			{
				if (xPressed) tr.zoomx();else tr.zoom();
			}
		if (nbClicks>0) 
			for (int i=0;i<nbClicks;i++) 
			{
				if (xPressed) tr.unzoomx();else  tr.unzoom();
			}
		nodesPanel.repaint();
	}



	public void keyPressed(KeyEvent e)
	{
		//System.out.println(e.getKeyCode()+" "+e.getKeyChar());
		switch(e.getKeyCode())
		{
		case KeyEvent.VK_SHIFT :
			shiftPressed=true;
			break;
		case KeyEvent.VK_CONTROL :
			ctrlPressed=true;
			break;
		case KeyEvent.VK_ALT :
			altPressed=true;
			break;
		case KeyEvent.VK_X :
			xPressed=true;
			break;
		case KeyEvent.VK_DELETE:
		case KeyEvent.VK_BACK_SPACE:
			this.eraseSelected();
			nodesPanel.repaint();
			break;
		}
		switch (e.getKeyChar())
		{
		case 'x':
			this.eraseSelected();
			break;
		case 'l':
			this.link2Selected();
			break;
		case 'z':case '-':
			tr.unzoom();
			break;
		case 'Z':case '+':
			tr.zoom();
			break;
		case 'f':
			//forces effects
			network.applyForces();
			break;
		case 'D':
			for (Node n:selectedNodes) 
				{
				n.setRadius(n.getRadius()*1.1+1);
				}
			break;
		case 'd':
			for (Node n:selectedNodes) n.setRadius(n.getRadius()*0.9);
			break;
		case 'h':
			changeSelectedColorHue();
			break;
		case 's':
			changeSelectedColorSat();
			break;
		case 'b':
			this.changeSelectedColorBrigthness();
			break;
		case 'm':
			showMenu=!showMenu;
			break;
		}
		nodesPanel.repaint();
		// System.out.println(getClass()+" multiSelection="+multiSelection);
	}



	public void keyReleased(KeyEvent e)
	{
		switch(e.getKeyCode())
		{
		case KeyEvent.VK_SHIFT :
			shiftPressed=false;
			break;
		case KeyEvent.VK_CONTROL :
			ctrlPressed=false;
			break;
		case KeyEvent.VK_ALT :
			altPressed=false;
			break;
		case KeyEvent.VK_X :
			xPressed=false;
			break;
		}
		// System.out.println(getClass()+" multiSelection="+multiSelection);
	}

	public void keyTyped(KeyEvent e)
	{
		// System.out.println(e.getKeyCode());
	}



	protected Node getNodeOnMouse(int xmouse,int ymouse)
	{
		//for (Node t:network)
		for (MysqlRow l: network)
		{
			Node t=(Node)l;
			if (t.mouseIsOn(tr,xmouse,ymouse)) return t;
		}
		return null;
	}


	public void unselectAll()
	{
		for (MysqlRow l: network)
		{
			Node t=(Node)l;
			t.select(false);
		}
		selectedNodes=getSelectedNodes();
		nodesPanel.repaint();
	}



	public Node[] getSelectedNodes()
	{
		Vector<Node> list=new Vector<Node>();
		for (MysqlRow l: network)
		{
			Node t=(Node)l;
			if (t.isSelected()) list.addElement(t);
		}

		Node[] ts=new Node[list.size()];
		list.copyInto(ts);
		return (ts);
	}


	public Node getClickedTask(){return clickedNode;}




	public void link2Selected()
	{
		Node[] ts=getSelectedNodes();
		if (ts.length!=2) return;
		if (ts[0].hasSon(ts[1]) )
		{
			ts[0].removeSon(ts[1]);
			return;
		}
		if (ts[1].hasSon(ts[0]) )
		{
			ts[1].removeSon(ts[0]);
			return;
		}
		System.out.println(getClass()+" "+ts[0].getName()+" "+ts[1].getName());
		if (ts[0].olderThan(ts[1])) ts[0].addSon(ts[1]);
		else ts[1].addSon(ts[0]);
		//	System.out.println(getClass()+" "+ts[1].getSonsNames());

	}

	public void eraseSelected()
	{

		for (Node t:selectedNodes) 
		{
			if (t.getNetwork()!=null) t.getNetwork().removeRow(t);
		}
	}

	public void changeSelectedColorHue()
	{
		changeSelectedColorSat(0);//index for hue
	}
	public void changeSelectedColorSat()
	{
		changeSelectedColorSat(1);//index for hue
	}
	public void changeSelectedColorBrigthness()
	{
		changeSelectedColorSat(2);//index for brightness
	}

	private void changeSelectedColorSat(int index)
	{
		for (Node t:selectedNodes) 
		{
			Color c=ColorUtil.getColor(t.getColor());
			float[] hsbvals=new float[3];
			Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
			hsbvals[index]+=0.05;
			if (hsbvals[index]>1)hsbvals[index] -=1;
			t.setColor(ColorUtil.color2String(new Color(Color.HSBtoRGB(hsbvals[0],hsbvals[1],hsbvals[2]))));
		}
	}


	public void selectAll()
	{
		for (int i=0;i<network.size();i++) ((Node)network.getRow(i)).select(true);
	}


	public String findNewName()
	{
		if (network.size()==0) return "main";
		String name=ROOT_NODE_NAME+compteur++;
		while (lookFor(name)!=null)
		{
			name=ROOT_NODE_NAME+compteur++;
		}
		return name;
	}

	public Node lookFor(String name)
	{
		for (int i=0;i<network.size();i++)
		{
			Node t=(Node)network.getRow(i);
			String tname=t.getName();
			if (tname.compareTo(name)==0) return t;
		}
		return null;
	}


	/**
gives the dates of sunday evening at midnight between min and max
	 */
	private double[] getWeekSeparations(double min,double max,int dayOfWeek)
	{

		Date datemin=new Date((long)min);
		Date datemax=new Date((long)max);
		cal.setTime(datemin) ;

		//System.out.println(getClass()+" date min :"+df.format(datemin));
		//System.out.println(getClass()+" date max :"+df.format(datemax));

		Vector<Date> values=new Vector<Date>();
		cal.set(Calendar.DAY_OF_WEEK,dayOfWeek);
		values.add(cal.getTime());
		// System.out.println(getClass()+" date of start :"+df.format(cal.getTime()));
		do
		{
			cal.add(Calendar.WEEK_OF_YEAR,1);
			// 	System.out.println(getClass()+" date :"+df.format(cal.getTime()));
			values.add(cal.getTime());
		}
		while (cal.getTime().before(datemax));

		double[] v=new double[values.size()];
		for (int i=0;i<v.length;i++) 
		{
			Date date=(Date)values.elementAt(i);
			// 	System.out.println(getClass()+" "+df.format(date));
			v[i]=date.getTime();	
		}
		// System.out.println();
		return v;
	}

	/**
gives the dates of days evening at midnight between min and max
	 */
	private double[] getDaysSeparations(double min,double max,int hour)
	{

		Date datemin=new Date((long)min);
		Date datemax=new Date((long)max);
		cal.setTime(datemin) ;

		// System.out.println(getClass()+" date min :"+df.format(datemin));

		Vector<Date> values=new Vector<Date>();
		cal.set(Calendar.HOUR_OF_DAY,hour);
		values.add(cal.getTime());
		// System.out.println(getClass()+" date of start :"+df.format(cal.getTime()));
		do
		{
			cal.add(Calendar.DAY_OF_YEAR,1);
			// 	System.out.println(getClass()+" date :"+df.format(cal.getTime()));
			values.add(cal.getTime());
		}
		while (cal.getTime().before(datemax));

		double[] v=new double[values.size()];
		for (int i=0;i<v.length;i++) 
		{
			Date date=(Date)values.elementAt(i);
			// 	System.out.println(getClass()+" "+df.format(date));
			v[i]=date.getTime();	
		}
		// System.out.println();
		return v;
	}



	/**
gives the dates of plain hours between min and max
	 */
	private double[] getHoursSeparations(double min,double max)
	{

		Date datemin=new Date((long)min);
		Date datemax=new Date((long)max);
		cal.setTime(datemin) ;

		// System.out.println(getClass()+" date min :"+df.format(datemin));

		Vector<Date> values=new Vector<Date>();
		cal.set(Calendar.MINUTE,0);
		values.add(cal.getTime());
		// System.out.println(getClass()+" date of start :"+df.format(cal.getTime()));
		do
		{
			cal.add(Calendar.HOUR_OF_DAY,1);
			// 	System.out.println(getClass()+" date :"+df.format(cal.getTime()));
			int h=cal.get(Calendar.HOUR_OF_DAY);
			if (((h>=8)&(h<=13))||(h>=15)&(h<=20)) values.add(cal.getTime());
		}
		while (cal.getTime().before(datemax));

		double[] v=new double[values.size()];
		for (int i=0;i<v.length;i++) 
		{
			Date date=(Date)values.elementAt(i);
			// 	System.out.println(getClass()+" "+df.format(date));
			v[i]=date.getTime();	
		}
		// System.out.println();
		return v;
	}




	/**
gives the special dates between the min and max
	 */
	private  double[] getDateTicks(double min,double max)
	{
		double interval,newmin,newmax,d;


		Calendar cal=GregorianCalendar.getInstance();

		Date datemin=new Date((long)min);
		Date datemax=new Date((long)max);
		cal.setTime(datemin) ;
		int y1=cal.get(Calendar.YEAR);
		int m1=cal.get(Calendar.MONTH);
		int w1=cal.get(Calendar.WEEK_OF_YEAR);
		int d1=cal.get(Calendar.DAY_OF_MONTH);
		int h1=cal.get(Calendar.HOUR);
		// cal.setTime(new Date((long)max)) ;
		// int y2=cal.get(Calendar.YEAR);
		// int m2=cal.get(Calendar.MONTH);
		// int w2=cal.get(Calendar.WEEK_OF_YEAR);
		// int d2=cal.get(Calendar.DAY_OF_MONTH);
		// int h2=cal.get(Calendar.HOUR);

		int dh=(int)((max-min)/3600000);
		int dd=dh/24;
		int dw=dd/7;
		int dm=dd/30;
		int dy=dd/365;
		System.out.println(getClass()+" "+df.format(datemin));
		System.out.println(getClass()+" "+df.format(datemax));
		System.out.println(getClass()+" "+y1+" "+m1+" "+w1+" "+d1+" "+h1);
		System.out.println(getClass()+" "+dy+" "+dm+" "+dw+" "+dd+" "+dh);


		Vector<Date> values=new Vector<Date>();
		if (dy>=4)
		{
			cal.set(y1,0,1,0,0);//set to 1st january
			do
			{
				cal.add(Calendar.YEAR,1);
				values.add(cal.getTime());
			}
			while (cal.getTime().before(datemax));
		}

		else if (dm>=6)
		{
			cal.set(y1,m1,1,0,0);//set to 1st of month
			do
			{
				cal.add(Calendar.MONTH,1);
				values.add(cal.getTime());
			}
			while (cal.getTime().before(datemax));
		}
		else if ((dw)>=4)
		{
			cal.set(y1,m1,0,0,0);//set to 1st of month
			cal.set(Calendar.WEEK_OF_MONTH,1);
			do
			{
				cal.add(Calendar.WEEK_OF_YEAR,1);
				values.add(cal.getTime());
			}
			while (cal.getTime().before(datemax));
		}
		else if ((dd)>=14)
		{
			cal.set(y1,m1,d1,0,0);//set to 1st hour
			do
			{
				cal.add(Calendar.DAY_OF_MONTH,1);
				values.add(cal.getTime());
			}
			while (cal.getTime().before(datemax));
		}
		else if ((dh)>=24)
		{
			cal.set(y1,m1,d1,h1,0);//set to 1st minute
			do
			{
				cal.add(Calendar.HOUR,1);
				values.add(cal.getTime());
			}
			while (cal.getTime().before(datemax));
		}
		else return null;

		double[] v=new double[values.size()];
		for (int i=0;i<v.length;i++) 
		{
			Date date=(Date)values.elementAt(i);
			System.out.println(getClass()+" "+df.format(date));
			v[i]=date.getTime();	
		}
		return v;
	}




	String getMonthForInt(int m) {
		String month = "invalid";
		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();
		if (m >= 0 && m <= 11 ) {
			month = months[m];
		}
		return month;
	}

	/**
	 * return the day of week of this workshop in spanish
	 * @return
	 */
	public String getDayOfWeekEnglish(Calendar cal)
	{
		String[] namesOfDays = {"Saturday","Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
		//System.out.println(cal.getTime());
		int i=cal.get(Calendar.DAY_OF_WEEK);
		//return "test";
		return namesOfDays[i-1];

	}


	public void showTodo()
	{
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy"); 
		StringBuffer sb=new StringBuffer();
		for (Node t:network.getTodosSorted()	)
		{
			sb.append(dt.format(t.getDate())+"->"+dt.format(t.getDateEnd())+" "+t.getLabel()+"\n");
		}
		TextEditor te=new TextEditor();
		te.setTitle("TODO LIST");
		te.setText(sb.toString());
	}

	private static Color darker(Color c)
	{
		int d=10;
		return new Color(Math.max(c.getRed()-d,0),Math.max(c.getGreen()-d,0),Math.max(c.getBlue()-d,0));	
	}
	private static Color brighter(Color c)
	{
		int d=-10;
		return new Color(Math.min(c.getRed()-d,255),Math.min(c.getGreen()-d,255),Math.min(c.getBlue()-d,255));	
	}


	public JPanel getComponent(){return mainPanel;}


	public void repaint()
	{
		nodesPanel.repaint();

	}

} 

