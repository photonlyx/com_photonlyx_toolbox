package com.photonlyx.toolbox.graph.gui;

import java.awt.Dimension;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.photonlyx.toolbox.graph.Node;


public class LabelEditJTextArea extends JTextArea implements DocumentListener
{
private Node t;

public LabelEditJTextArea(Node t)
{
this.t=t;
this.getDocument().addDocumentListener(this);
this.setSize(new Dimension(300,20));
this.setText(t.getLabel());
}



@Override
public void insertUpdate(DocumentEvent e)
{
String s=this.getText();
//remove chars that can corrupt the xml storage:
s=s.replace('"','-');
s=s.replace('>','-');
s=s.replace('<','-');
s=s.replace('&','-');
t.setLabel(s);
//System.out.println(t.getLabel());

}


@Override
public void removeUpdate(DocumentEvent e)
{
t.setLabel(this.getText());

}


@Override
public void changedUpdate(DocumentEvent e)
{
t.setLabel(this.getText());

}



}
