package com.photonlyx.toolbox.graph;
	
import java.awt.*;
import javax.swing.*;

	
public class Transformation 
{
private Network net;
private JComponent comp;


public  Transformation(JComponent comp,Network a)
{
this.net=a;
this.comp=comp;
}

//
//
//public void updateScreenCoord(Node t)
//{
//int i1=toScreenx(t.getXreal());
//int i2=toScreeny(t.getYreal());
//
//	{
//	t.setxscreen(i1);
//	t.setyscreen(i2);
//	}
//
//}

/**
return the date as double correponding to this point in screen
*/
public double getyreal(int x,int y)
{
double date;
 
	{
	date=net.getyMinView()+y/(comp.getHeight()/(net.getyMaxView()-net.getyMinView()));
	}

return date;
}

/**
 * update date and  position
 * @param t
 */
public void updateRealCoord(Node t)
{

	{
	t.setXreal(net.xminView+t.getXscreen(this)/(comp.getWidth()/(net.xmaxView-net.xminView)));
	t.setYreal(net.getyMinView()+t.getYscreen(this)/(comp.getHeight()/(net.getyMaxView()-net.getyMinView())));
	}

}


public int toScreenx(double xreal)
{
return (int)((xreal-net.xminView)*(comp.getWidth()/(net.xmaxView-net.xminView)));

}
public int toScreeny(double yreal)
{
return (int)((yreal-net.getyMinView())*((double)comp.getHeight()/(double)(net.getyMaxView()-net.getyMinView())));
}

public double toRealx(int xscreen)
{
return xscreen / (comp.getWidth() /  (net.xmaxView-net.xminView)  )     + net.xminView;
}

public double toRealy(int yscreen)
{
return yscreen / (comp.getHeight() /  (net.ymaxView-net.yminView)  )     + net.yminView;
}

private void zoom(double coefx,double coefdate)
{
double xcentre=(net.xminView+net.xmaxView)/2;
double deltax=net.xmaxView-net.xminView;
deltax/=coefx;
net.xminView=xcentre-deltax/2;
net.xmaxView=xcentre+deltax/2;
//System.out.println(" taf.Transformation zoom x min et max "+a.xminView+" "+a.xmaxView);

double datecentre=(net.getyMinView()+net.getyMaxView())/2;
double deltadate=net.getyMaxView()-net.getyMinView();
deltadate/=coefdate;
net.setyMinView(datecentre-deltadate/2);
net.setyMaxView(datecentre+deltadate/2);
//System.out.println(" taf.Transformation zoom date min et max"+dateMin+" "+dateMin);
}

public void zoom()
{
zoom(1.1,1.1);
}

public void unzoom()
{
zoom(0.9,0.9);
}

public void zoomx()
{
zoom(1.1,1.0);
}

public void unzoomx()
{
zoom(0.9,1.0);
}


public void translate(int dx,int dy)
{

	{
	double dxr=dx/(comp.getWidth()/(net.xmaxView-net.xminView));
	net.xminView-=dxr;
	net.xmaxView-=dxr;
	double dyr=dy/(comp.getHeight()/(net.getyMaxView()-net.getyMinView()));
	net.setyMinView(net.getyMinView() - dyr);
	net.setyMaxView(net.getyMaxView() - dyr);
	}


}


public void translateTask(Node t,int dx,int dy)
{

	{
	double dxr=dx/(comp.getWidth()/(net.xmaxView-net.xminView));
/*	xmin.val-=dxr;
	xmax.val-=dxr;*/
	double dyr=dy/(comp.getHeight()/(net.getyMaxView()-net.getyMinView()));
/*	dateMin.val-=dyr;
	dateMax.val-=dyr;*/
	t.setXreal(t.getXreal() + dxr);
	t.setYreal(t.getYreal() + dyr);
	}


}

public void changeTaskLength(Node t,int dx,int dy)
{
// System.out.println(getClass()+" change length of task "+t.label.val);
double dxr=dx/(comp.getWidth()/(net.getyMaxView()-net.getyMinView()));
double newlength=t.getRadius()+dxr;
if (newlength >= 1800000) t.setRadius(newlength);//mini 1/2 hour
}



public Component getComponent()
{
return comp;
}


}