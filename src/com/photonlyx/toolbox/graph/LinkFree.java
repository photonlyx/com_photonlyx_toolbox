package com.photonlyx.toolbox.graph;

public class LinkFree
{
private Node n1,n2;
private double ka=0.5;//attractive coef (spring module)
private double kr=50;//repulsive coef (repulsive magnet force)

public LinkFree(Node n1,Node n2)
{
this.n1=n1;
this.n2=n2;
}

public void getForce(double[] force)
{
double d2=(Math.pow(n1.getXreal()-n2.getXreal(),2)+Math.pow(n1.getYreal()-n1.getYreal(),2));
double d=Math.sqrt(d2);
double forceModule=0;
if (d>2*n1.getRadius()) forceModule+=ka*d;
forceModule+=-kr/d2;
force[0]=forceModule*(n2.getXreal()-n1.getXreal());
force[1]=forceModule*(n2.getYreal()-n1.getYreal());
}


}
