package com.photonlyx.toolbox.graph;

import java.text.SimpleDateFormat;
import java.util.*;

import java.awt.*;

import com.photonlyx.toolbox.gui.ColorUtil;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.mysql.MysqlRow;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.util.Messager;

import nanoxml.XMLElement;

public class Node  extends MysqlRow implements XMLstorable
{
	private Network network;
	private Node father;
	//protected Vector<Node> sons=new Vector<Node>();
	
	//stored:
	private String name="";
	private double yreal; //position x
	private double xreal;// position y
	private String color="red";
	private String label="label";
	private int status=STATUS_EVENT;
	private String comment="";
	private double radius=1; //radius of the node in real coordinates
	//protected String category="project1";
	private String sonsNames="";//used when loading file

	public double mass=1;
	public boolean selected;


	private Object object;

	//internal
	//private int[] screen=new int[2];//screen coordinates
	//private int[] size=new int[2];
	private static int taskSizew=80;
	private static int taskSizeh=20;

	public static int STATUS_EVENT=0;
	public static int STATUS_NOTE=1;
	public static int STATUS_TODO=2;
	public static int STATUS_DONE=3;
	public static int STATUS_URGENT=4;


	private static SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd:HH:mm:ss");



	public Node(Network network)
	{
		this.network=network;
		//size[0]=taskSizew;
		//size[1]=taskSizeh;
	}

	public Node(Network network,String name)
	{
		this.network=network;
		this.name=name;
		//size[0]=taskSizew;
		//size[1]=taskSizeh;
	}

	//
	//public Node(Network network,String name,int xscreen,int yscreen)
	//{
	//this.network=network;
	//this.name=name;
	////size[0]=taskSizew;
	////size[1]=taskSizeh;
	////screen[0]=xscreen;
	////screen[1]=yscreen;
	//
	//}




	public void paint(Graphics g,Transformation tr)
	{
		//tr.updateScreenCoord(this);

		//if (xscreen()>tr.getComponent().getSize().width+500) return;
		//if (xscreen()<-500) return;

		int radiusPix=tr.toScreenx(this.xreal+radius)-tr.toScreenx(this.xreal);
		//Global.setMessage("radius: "+radius+" radius pix: "+radiusPix);
		//int radiusPix=100;


		g.setColor(ColorUtil.getColor(color)); 
		g.fillOval(getXscreen(tr)-radiusPix,getYscreen(tr)-radiusPix,radiusPix*2,radiusPix*2);	


		if (selected)  
		{
			g.setColor(Color.blue);
			//g.drawRect(xscreen(),yscreen(),radiusPix,h());
			//g.drawRect(xscreen(),yscreen(),radiusPix+10,h());
			//g.drawLine(xscreen(),0,xscreen(),tr.getComponent().getSize().height);
			//g.drawLine(xscreen()+radiusPix,0,xscreen()+radiusPix,tr.getComponent().getSize().height);
			int r=radiusPix+10;
			g.drawOval(getXscreen(tr)-r,getYscreen(tr)-r,r*2,r*2);	
		}

		//g.setColor(Color.lightGray);
		//g.fillOval(xscreen()-4,yscreen()+h()/2-4,8,8);	

		//label of the activity:
		if (status==STATUS_DONE) g.setColor(Color.darkGray);else g.setColor(Color.black);
		g.drawString(label,getXscreen(tr)+10,getYscreen(tr)+radiusPix/2+5);

		Font save=g.getFont();
		Font petit=save.deriveFont(save.getSize()-4);
		g.setFont(petit);
		//category:
		g.setColor(Color.red);
		//g.drawString(category,getXscreen(tr)+5,getYscreen(tr)+5);

	}


	public boolean mouseIsOn(Transformation tr,int xmouse,int ymouse)
	{
		//return ((xmouse>=xscreen())&(xmouse<=(xscreen()+w(tr)+10))&(ymouse>=yscreen())&(ymouse<=(yscreen()+h())));
//		double xrealmouse=tr.toRealx(xmouse);
//		double yrealmouse=tr.toRealy(ymouse);
//		double distmouse=Math.sqrt(Math.pow(xrealmouse-xreal,2)+Math.pow(yrealmouse-yreal,2));
//		System.out.println("radius  "+radius+"   dx "+(xrealmouse-xreal)+"  dy "+(yrealmouse-yreal)+"   distmouse "+distmouse);
//		return (distmouse<radius);
		
		int xScreen=tr.toScreenx(xreal);
		int yScreen=tr.toScreeny(yreal);
		double distmouse=Math.sqrt(Math.pow(xmouse-xScreen,2)+Math.pow(ymouse-yScreen,2));
		// System.out.println("   dx "+(xmouse-xScreen)+"  y "+(ymouse-yScreen)+"   distmouse "+distmouse);
		return (distmouse<20);
		
		
	}

	//public boolean mouseInLengthHandle(Transformation tr,int x,int y)
	//{
	//int radiusPix=tr.toScreenx(this.xreal+radius)-tr.toScreenx(this.xreal);
	//int dist2radius=
	//return (((getXscreen(tr)+w(tr))<=x)&&(x<=getXscreen(tr)+w(tr)+10)&&((getYscreen(tr))<=y)&&(y<=getYscreen(tr)+h()));
	//}

	public boolean isInsideRectangle(Transformation tr,int x1,int y1,int x2,int y2)
	{
		return ((getXscreen(tr)>=x1)&(getXscreen(tr)<=x2)&(getYscreen(tr)>=y1)&(getYscreen(tr)<=y2));
	}

	public void select(Transformation tr,int xmouse,int ymouse)
	{
		if (mouseIsOn(tr,xmouse,ymouse)) selected=true;
	}

	//public void translate(int dx,int dy)
	//{
	//setxscreen(xscreen()+dx);
	//setyscreen(yscreen()+dy);
	//}

	public void select(boolean b) { selected=b;}

	public boolean isSelected()
	{
		return (selected);
	}


	public boolean olderThan(Node t2)
	{
		return (yreal<t2.yreal);
	}


	public int getXscreen(Transformation tr)
	{ 
		return tr.toScreenx(xreal);
	}

	public int getYscreen(Transformation tr)
	{ 
		return tr.toScreeny(yreal);
	}

	//public int xscreen()
	//{ 
	//return screen[0];
	//}
	//
	//public int yscreen()
	//{ 
	//return screen[1];
	//}


	//public void setxscreen(int i) {screen[0]=i;}
	//public void setyscreen(int i) {screen[1]=i;}

	//public int[] screen(){ return screen;}
	public int w(Transformation tr)
	{
		return tr.toScreeny(yreal+radius)-tr.toScreeny(yreal);
	}

	//public int h(){ return size[1];}
	//
	//
	//public int handlex(Transformation tr){ return (xscreen()+w(tr)/2); }
	//public int handley(){ return (yscreen()); }
	//public int handlesonx(Transformation tr){  return (xscreen()+w(tr)/2); }
	//public int handlesony(){ return (yscreen()+h()); }



	public double getXreal(){return xreal;}
	public double getYreal(){return yreal;}




	public Date getDate()
	{ 
		return new Date((long)yreal);
	}


	public Date getDateEnd()
	{ 
		return new Date((long)(yreal+radius));
	}

	public String label(){return label;}

	public int minute()
	{
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date((long)yreal)) ;
		return cal.get(Calendar.MINUTE);
	}

	public int hour()
	{
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date((long)yreal)) ;
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	/**return day of month*/
	public int day()
	{
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date((long)yreal)) ;
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	/**return the month (from 1 to 12)*/
	public int month()
	{
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date((long)yreal)) ;
		return (cal.get(Calendar.MONTH)+1);
	}

	public int year()
	{
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date((long)yreal)) ;
		return cal.get(Calendar.YEAR);
	}

	/** return the ancestor of this task*/
	public Node getAncestor()
	{
		Node t=this;
		for(;;)
		{
			// 	System.out.println(getClass()+" "+t.label.val);
			Node father=(Node)t.getFather();
			if (father==null) break;
			t=(Node)father;
		}
		return t;
	}

	public void setFather(Node f)
	{
		father=f;
	}


	public void removeFather()
	{
		father=null;
	}


	//public Vector<Node> getSons(){return sons;}

	public void addSon(Node son)
	{
		//sons.add(son);
		this.sonsNames+=" "+son.getName();
		this.getTable().updateSQL(this,"sonsNames");
		son.setFather(this);
	}

	public void removeSon(Node son)
	{
		//sons.remove(son);
		this.sonsNames.replace(son.getName(),"");
	}

	public boolean hasSon(Node son)
	{
		return  (sonsNames.contains(son.getName()));
	}

	public Node son(int j)
	{
		Vector<Node> sons=this.getSonsVector();
		return sons.elementAt(j);
		
	}

	public int nbSons()
	{
		Vector<Node> sons=this.getSonsVector();
		return sons.size();
	}

	public Node getFather(){return father;}

	/** return the ancestor of this task*/
	public double  getAllLinkedTaskTotalTimeLength()
	{
		Node t=this;
		double time=this.getRadius();
		for(;;)
		{
			// 	System.out.println(getClass()+" "+t.label.val);
			Node father=t.getFather();
			if (father==null) break;
			time+=father.getRadius();
			t=(Node)father;
		}
		return time;
	}

	/** return the project name of this task (it is the label of the ancestor)*/
	public String getProjectName()
	{
		return getAncestor().label;
	}

	public boolean isANote()
	{
		return  (
				(status==STATUS_NOTE)
				//|| (status.compareTo(STATUS_TODO)==0)
				|| (status==STATUS_DONE)
				|| (status==STATUS_URGENT)
				)
				;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
		this.getTable().updateSQL(this,"name");
	}

	public void setLength(double length)
	{
		this.radius = length;
	}
	public void setRadius(double d)
	{
		this.radius = d;
		this.getTable().updateSQL(this,"radius");
	}

	public double getRadius(){return radius;}

	
	public String getLabel(){ return label;}
	//public String getCategory(){ return category;}
	public void setLabel(String s)
	{
		label=s;
		this.getTable().updateSQL(this,"label");
	}
	//public void setCategory(String s){ category=s;}
	public void setStatus(int i)
	{ 
		status=i;
		//System.out.println("Status set to "+s);
	}
	public int getStatus()
	{
		return status;
	}

	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
		this.getTable().updateSQL(this,"color");
	}

	public String getSonsNames() {
		return sonsNames;
	}

	public void setSonsNames(String sonsNames) {
		this.sonsNames = sonsNames;
		this.getTable().updateSQL(this,"sonsNames");
	}

	public void setYreal(double yreal) {
		this.yreal = yreal;
		this.getTable().updateSQL(this,"yreal");
	}

	public void setXreal(double xreal) {
		this.xreal = xreal;
		this.getTable().updateSQL(this,"xreal");
	}

	public Object getObject()
	{
		return object;
	}

	public void setObject(Object object)
	{
		this.object = object;
	}

	public void setAttributesToXML(XMLElement el )
	{
		el.setAttribute("dBnumber",this.getdBnumber());
		el.setAttribute("name",name);
		el.setAttribute("y",yreal);
		el.setDoubleAttribute("x",xreal);
		el.setAttribute("label",label);
		el.setIntAttribute("status",status);
		el.setAttribute("comment",comment);
		//el.setAttribute("linkedFileName",linkedFileName);
		el.setDoubleAttribute("length",radius);
		//el.setAttribute("category",category);
		el.setAttribute("label",label);
		el.setAttribute("color", (color));
//		String s="";
//		for (Node t:sons) s+=t.getName()+" ";
		el.setAttribute("sons",sonsNames);

	}

	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().getSimpleName().toString().replace("class ", ""));
		setAttributesToXML(el );
		return el;
	}

	@Override
	public void updateFromXML(XMLElement xml)
	{
		this.setDBnumber(xml.getIntAttribute("dbnumber",0));
		name=xml.getStringAttribute("name");
		xreal=xml.getDoubleAttribute("x");
		yreal=xml.getDoubleAttribute("y");
		label=xml.getStringAttribute("label");
		status=xml.getIntAttribute("status");

		comment=xml.getStringAttribute("comment");
		radius=xml.getDoubleAttribute("length");
		//category=xml.getStringAttribute("category");
		label=xml.getStringAttribute("label");
		color=xml.getStringAttribute("color","red");
		//sons
		sonsNames=xml.getStringAttribute("sons");
	}


	/**
	 * update the sons from the list of names
	 */
	protected void updateSons()
	{
		Definition def=new Definition(this.sonsNames);
		for (int i=0;i<def.dim();i++)
		{
			Node tson=network.getNodeOfName(def.word(i));
			if(tson==null)
			{
				Messager.messErr("Task "+this.getName()+" :can't find node of name "+tson);
			}
			else this.addSon(tson);
		}
	}

	public Vector<Node> getSonsVector()
	{
		Vector<Node> sons=new Vector<Node>();
		Definition def=new Definition(this.sonsNames);
		for (int i=0;i<def.dim();i++)
		{
			Node tson=network.getNodeOfName(def.word(i));
			if(tson==null)
			{
				Messager.messErr("Task "+this.getName()+" :can't find node of name "+tson);
			}
			else sons.add(tson);
		}
		return sons;
	}


	public String getComment()
	{
		return comment;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public Network getNetwork()
	{
		return network;
	}
	public void setNetwork(Network activity)
	{
		this.network = activity;
	}


	public double[] calcTotalForce( )
	{
		double[] f1=new double[2];
		//for (Node n:network)
		for (MysqlRow li:network)
		{
			Node n=(Node) li;
			if (n==this) continue;
			LinkFree l=new LinkFree(this,n);
			l.getForce(f1);
			f1[0]+=f1[0];
			f1[1]+=f1[1];
		} 
		Vector<Node> sons=this.getSonsVector();
		for (Node n:sons)
		{
			LinkElastic l=new LinkElastic(this,n);
			l.getForce(f1);
			f1[0]+=f1[0];
			f1[1]+=f1[1];
		}
		return f1;
	}


	public double[] calcDisplacement(double dt)
	{
		double[] dp=new double[2];
		double[] f=this.calcTotalForce();
		dp[0]=f[0]/mass*Math.pow(dt, 2);
		dp[1]=f[1]/mass*Math.pow(dt, 2);
		return dp;
	}

	public void translateReal(double[] dp)
	{
		xreal+=dp[0];
		yreal+=dp[1];
	}


}
